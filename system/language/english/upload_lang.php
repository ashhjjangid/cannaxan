<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['upload_userfile_not_set'] = 'Es wurde keine Post-Variable namens userfile gefunden.'; //Unable to find a post variable called userfile.
$lang['upload_file_exceeds_limit'] = 'Die hochgeladene Datei überschreitet die maximal zulässige Größe in Ihrer PHP-Konfigurationsdatei.'; //The uploaded file exceeds the maximum allowed size in your PHP configuration file.
$lang['upload_file_exceeds_form_limit'] = 'Die hochgeladene Datei überschreitet die im Übermittlungsformular zulässige maximale Größe.'; //The uploaded file exceeds the maximum size allowed by the submission form.
$lang['upload_file_partial'] = 'Die Datei wurde nur teilweise hochgeladen.'; //The file was only partially uploaded.
$lang['upload_no_temp_directory'] = 'Der temporäre Ordner fehlt.'; //The temporary folder is missing.
$lang['upload_unable_to_write_file'] = 'Die Datei konnte nicht auf die Festplatte geschrieben werden.'; //The file could not be written to disk.
$lang['upload_stopped_by_extension'] = 'Der Datei-Upload wurde durch Erweiterung gestoppt.'; //The file upload was stopped by extension.
$lang['upload_no_file_selected'] = 'Sie haben keine Datei zum Hochladen ausgewählt.'; //You did not select a file to upload.
$lang['upload_invalid_filetype'] = 'Der Dateityp, den Sie hochladen möchten, ist nicht zulässig. Bitte wählen Sie JPG, JPEG oder PNG'; //The filetype you are attempting to upload is not allowed.
$lang['upload_invalid_filesize'] = 'Die Datei, die Sie hochladen möchten, ist größer als die zulässige Größe.'; //The file you are attempting to upload is larger than the permitted size.
$lang['upload_invalid_dimensions'] = 'Das Bild, das Sie hochladen möchten, passt nicht in die zulässigen Abmessungen.'; //The image you are attempting to upload doesn\'t fit into the allowed dimensions.
$lang['upload_destination_error'] = 'Beim Versuch, die hochgeladene Datei an das endgültige Ziel zu verschieben, ist ein Problem aufgetreten.'; //A problem was encountered while attempting to move the uploaded file to the final destination.
$lang['upload_no_filepath'] = 'Der Upload-Pfad scheint nicht gültig zu sein.The upload path does not appear to be valid.'; //The upload path does not appear to be valid.
$lang['upload_no_file_types'] = 'Sie haben keine zulässigen Dateitypen angegeben.'; //You have not specified any allowed file types.
$lang['upload_bad_filename'] = 'Der von Ihnen übermittelte Dateiname ist bereits auf dem Server vorhanden.'; //The file name you submitted already exists on the server.
$lang['upload_not_writable'] = 'Der Upload-Zielordner scheint nicht beschreibbar zu sein.'; //The upload destination folder does not appear to be writable.
