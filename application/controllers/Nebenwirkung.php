<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nebenwirkung extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('nebenwirkung_model', 'side_effects');
		$this->load->model('patients_model', 'patients');
		$this->load->model('process_control_model', 'process_model');
	}

	function index() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}

		$output = array();
		$output['header_menu'] = 'process_control';
        $output['header_sub_menu'] = 'nebenwirkung';
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
       	
        if ($is_exist_patient) {
	        $id = $is_exist_patient->id;
	        $output['insurance_number'] = $insured_number;          
        	$data_exists = $this->side_effects->getSideEffectsTableDataById($id);
        	$current_date = date('Y-m-d');

        	if ($data_exists) {
        		$c_date = strtotime($current_date);
        		$first_visit = $this->side_effects->getFirstVisit($id);
        		$datediff = $c_date - strtotime($first_visit->datum);
				$behandungstag = round($datediff / (60 * 60 * 24));

        		$s_date = strtotime($data_exists->datum);

        		if ($s_date == $c_date) {
        			$visit_number = $data_exists->visite;
        			$is_current_record = 'Yes';
        		} else {
        			$visit_number = $data_exists->visite + 1;
        			$is_current_record = 'No';
        		}

	        	$output['datum'] = date('d.m.Y', strtotime($current_date));
	        	$output['behandungstag'] = $data_exists->behandungstag;
	        	$output['visite'] = $data_exists->visite;
	        	$output['arzneimittel'] = $data_exists->arzneimittel;
	        	$output['select_date'] = $data_exists->datum;
	        	$output['keine_kausal_bedingte'] = $data_exists->keine_kausal_bedingte;
        		$behandlunggstag_id = $data_exists->id;
        		$table_data_exists = $this->side_effects->getSideEffectsDataByBehandlunggstagId($behandlunggstag_id);
        		$output['table_data_exists'] = $table_data_exists;
        	} else {
        		$output['datum'] = date('d.m.Y', strtotime($current_date));
	        	$output['behandungstag'] = '';
	        	$output['visite'] = '';
	        	$output['select_date'] = '';
	        	$output['arzneimittel'] = 'CannaXan-THC';
	        	$output['keine_kausal_bedingte'] = '';
	        	$is_current_record = 'No';
        	}

        	// $visits = $this->process_model->getPatientQuestions($is_exist_patient->id);
        	$visits = $this->process_model->getVisitsByPatientId($is_exist_patient->id);
    		$output['visits'] = $visits;
		    // $output['visits'] = $visits;
		    $output['is_current_record'] = $is_current_record;
        } else {
        	redirect('patients');
        }


		$records = $this->side_effects->getAllData();
		//pr($records); die;
		$output['records'] = $records;


		$this->load->view('front/includes/header', $output);
		$this->load->view('front/nebenwirkung/list_page');
		$this->load->view('front/includes/footer');
	}

	function add_nebenwirkung_data() {
		$doctor = $this->session->userdata('user_id');

		$insured_number = $this->input->get('in');
		//pr($insured_number); die;

		
		if ($this->input->post()) {
			// pr($_POST); die;
			$this->form_validation->set_rules('datum', 'Datum', 'trim|required', array('required' =>'Datumsfeld ist erforderlich'));
			$this->form_validation->set_rules('behandungstag', 'Behandungstag', 'trim|required', array('required' =>'Behandungstag feld ist erforderlich'));
			$this->form_validation->set_rules('visite', 'Visite', 'trim|required', array('required' =>'Visite feld ist erforderlich'));
			$this->form_validation->set_rules('arzneimittel', 'Arzneimittel', 'trim|required', array('required' =>'Arzneimittel feld ist erforderlich'));
			$this->form_validation->set_rules('keine_kausal_bedingte', 'Kausal bedingte Nebenwirkung', 'trim|required', array('required' =>'Kausal bedingte Nebenwirkung feld ist erforderlich'));

			if ($this->form_validation->run()) {
				
				$is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

				if ($is_exist_patient) {
			        $id = $is_exist_patient->id;
			        
			        $datum = date('Y-m-d', strtotime($this->input->post('datum')));
			        $is_exist = $this->side_effects->getRecordByDate($id, $datum);

			        if ($is_exist) {
			        	
						$insert_id = $is_exist->id;
						$input['datum'] = date('Y-m-d', strtotime($this->input->post('datum')));
						$input['behandungstag'] = $this->input->post('behandungstag');
						$input['visite'] = $this->input->post('visite');
						$input['arzneimittel'] = $this->input->post('arzneimittel');
			        	$input['keine_kausal_bedingte'] = $this->input->post('keine_kausal_bedingte');
						$input['add_date'] = getDefaultToGMTDate(time());
						$this->side_effects->updatekausalbedingteData($input, $insert_id);

						$this->side_effects->deletenebenwirkungtabledata($insert_id);

						if ($this->input->post('keine_kausal_bedingte') == 'ja') {

							foreach ($_POST['data'] as $key => $value) {
								
								$dataArray['side_effects'][$key]['behandlunggstag_id'] = $insert_id;
								$dataArray['side_effects'][$key]['nebenwirkung_name'] = $value['nebenwirkung_name'];
								$dataArray['side_effects'][$key]['mild_schweregrad'] = (isset($value['mild_schweregrad']) && $value['mild_schweregrad'] == 'on'?'Yes':'No');
								$dataArray['side_effects'][$key]['moderat_schweregrad'] = (isset($value['moderat_schweregrad']) && $value['moderat_schweregrad'] == 'on'?'Yes':'No');
								$dataArray['side_effects'][$key]['schwer_schweregrad'] = (isset($value['schwer_schweregrad']) && $value['schwer_schweregrad'] == 'on'?'Yes':'No');
								$dataArray['side_effects'][$key]['lebensbedrohlich_schweregrad'] = (isset($value['lebensbedrohlich_schweregrad']) && $value['lebensbedrohlich_schweregrad'] == 'on'?'Yes':'No');
								$dataArray['side_effects'][$key]['kausalitat_zur_therapie_vorhanden'] = (isset($value['kausalitat_zur_therapie_vorhanden']) && $value['kausalitat_zur_therapie_vorhanden'] == 'on'?'ja':'nein');
								$dataArray['side_effects'][$key]['add_date'] = getDefaultToGMTDate(time());
							}
							
							$inserted_data = $this->side_effects->addnebenwirkungtabledata($dataArray['side_effects']);
							if ($inserted_data) {
								
								$success = true;
								$message = 'Nebenwirkungsdaten erfolgreich hinzugefügt';
								$data['redirectURL'] = base_url('nebenwirkung/nebenwirkungen_auswertung?in='. $insured_number);
							} else {
								$success = false;
								$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
							}
						} else {
							$success = true;
							$message = 'Nebenwirkungsdaten erfolgreich hinzugefügt';
							$data['redirectURL'] = base_url('nebenwirkung/nebenwirkungen_auswertung?in='. $insured_number);
						}


			        } else {

				        $input['doctor_id'] = $doctor;
				        $input['patient_id'] = $is_exist_patient->id;
						$input['datum'] = date('Y-m-d', strtotime($this->input->post('datum')));
						$input['behandungstag'] = $this->input->post('behandungstag');
						$input['visite'] = $this->input->post('visite');
						$input['arzneimittel'] = $this->input->post('arzneimittel');
						$input['keine_kausal_bedingte'] = $this->input->post('keine_kausal_bedingte');
						$input['add_date'] = getDefaultToGMTDate(time());

						//$insert_id = true;
						$insert_id = $this->side_effects->addkausalbedingteData($input);
				                  
						if ($insert_id) {
						
							//pr($_POST['data']); die;
							foreach ($_POST['data'] as $key => $value) {
								
								$dataArray['side_effects'][$key]['behandlunggstag_id'] = $insert_id;
								$dataArray['side_effects'][$key]['nebenwirkung_name'] = $value['nebenwirkung_name'];
								$dataArray['side_effects'][$key]['mild_schweregrad'] = (isset($value['mild_schweregrad']) && $value['mild_schweregrad'] == 'on'?'Yes':'No');
								$dataArray['side_effects'][$key]['moderat_schweregrad'] = (isset($value['moderat_schweregrad']) && $value['moderat_schweregrad'] == 'on'?'Yes':'No');
								$dataArray['side_effects'][$key]['schwer_schweregrad'] = (isset($value['schwer_schweregrad']) && $value['schwer_schweregrad'] == 'on'?'Yes':'No');
								$dataArray['side_effects'][$key]['lebensbedrohlich_schweregrad'] = (isset($value['lebensbedrohlich_schweregrad']) && $value['lebensbedrohlich_schweregrad'] == 'on'?'Yes':'No');
								$dataArray['side_effects'][$key]['kausalitat_zur_therapie_vorhanden'] = (isset($value['kausalitat_zur_therapie_vorhanden']) && $value['kausalitat_zur_therapie_vorhanden'] == 'on'?'ja':'nein');
								$dataArray['side_effects'][$key]['add_date'] = getDefaultToGMTDate(time());
							}

							$inserted_data = $this->side_effects->addnebenwirkungtabledata($dataArray['side_effects']);
							if ($inserted_data) {
								
								$success = true;
								$message = 'Nebenwirkungsdaten erfolgreich hinzugefügt';
								$data['redirectURL'] = base_url('nebenwirkung/nebenwirkungen_auswertung?in='. $insured_number);
							} else {
								$success = false;
								$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
							}

						} else {
							$success = false;
							$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
						}
			        }
					
		        } else {

		        	$message = 'Patient nicht gefunden';
		        	$success = false;
		        }
			} else {

				$success = false;
				$message = validation_errors();
			}

		}
		$data['success'] = $success;
        $data['message'] = $message;
        json_output($data);
	}

	function nebenwirkungen_auswertung() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}

		$output = array();
		$output['header_menu'] = 'process_control';
        $output['header_sub_menu'] = 'nebenwirkung';
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
       	
        if ($is_exist_patient) {
	        $id = $is_exist_patient->id;
	        $output['insurance_number'] = $insured_number;          
        	$data_exists = $this->side_effects->getSideEffectsTableDataById($id);
        	
        	$current_date = date('Y-m-d');

        	$visit_array = array();
        	
        	if ($data_exists) {
        		$patient_visits = $this->process_model->getVisitsByPatientId($is_exist_patient->id);

        		foreach ($patient_visits as $key => $value) {
        			$sideEffects = $this->side_effects->getRecordByDate($id, $value->datum);

        			if ($sideEffects) {
        				$patient_visit_data_exists = $this->side_effects->getAllVisitsSideEffectsDataByBehandlunggstagId($sideEffects->id);

        				foreach ($patient_visit_data_exists as $k => $v) {

        					$type = '';
        					$html = '';

        					if ($v->mild_schweregrad == 'Yes') {
        						$type = 'mild'; 
        						$html = '<span style="background:#2CA61E; display:block; height:20px;">'; 
        					}
				            if ($v->moderat_schweregrad == 'Yes') {
				            	$type = 'moderat'; 
				            	$html = '<span style="background:#FCAF16; display:block; height:20px;">'; 
				            }
				            if ($v->schwer_schweregrad == 'Yes') {
				            	$type = 'schwer'; 
				            	$html = '<span style="background:#F76816; display:block; height:20px;">'; 
				            }
				            if ($v->lebensbedrohlich_schweregrad == 'Yes') {
				            	$type = 'lebensbedrohlich'; 
				            	$html = '<span style="background:#D91313; display:block; height:20px;">'; 
				            }

        					// $value->nebenwirkung_name = $html;

        					$visit_array[$value->visite][$v->nebenwirkung_name] = $html;
        					// $visit_array[$value->visite][$k]['color'] = $color;
        					// $visit_array[$value->visite][$k]['type'] = $type;
        				}
	        		}
        		}
        		
        		// pr($visit_array); die;
        		$c_date = strtotime($current_date);
        		$first_visit = $this->side_effects->getFirstVisit($id);
        		$datediff = $c_date - strtotime($first_visit->datum);
				$behandungstag = round($datediff / (60 * 60 * 24));

        		$s_date = strtotime($data_exists->datum);

        		if ($s_date == $c_date) {
        			$visit_number = $data_exists->visite;
        			$is_current_record = 'Yes';
        		} else {
        			$visit_number = $data_exists->visite + 1;
        			$is_current_record = 'No';
        		}

	        	$output['datum'] = date('d.m.Y', strtotime($data_exists->datum));
	        	$output['behandungstag'] = $data_exists->behandungstag;
	        	$output['visite'] = $data_exists->visite;
	        	$output['patient_visits'] = $patient_visits;
	        	$output['arzneimittel'] = $data_exists->arzneimittel;
	        	$output['keine_kausal_bedingte'] = $data_exists->keine_kausal_bedingte;
	        	$output['visit_array'] = $visit_array;
        		$behandlunggstag_id = $data_exists->id;
        		$table_data_exists = $this->side_effects->getSideEffectsDataByBehandlunggstagId($behandlunggstag_id);
        		// pr($table_data_exists); die;
        		/*foreach ($table_data_exists as $ke => $val) {
        			$kausalitat_zur_therapie_vorhanden = $val->kausalitat_zur_therapie_vorhanden;
        			$output['kausalitat_zur_therapie_vorhanden'] = $kausalitat_zur_therapie_vorhanden;
        		}*/
        		$output['table_data_exists'] = $table_data_exists;
        		//pr($table_data_exists); die;
        	} else {
        		$output['datum'] = date('d.m.Y', strtotime($current_date));
	        	$output['behandungstag'] = '0';
	        	$output['visite'] = '1';
	        	$output['patient_visits'] = '';
	        	$output['arzneimittel'] = 'CannaXan-THC';
	        	$output['keine_kausal_bedingte'] = '';
	        	$output['visit_array'] = $visit_array;
        		$output['table_data_exists'] = '';
	        	$is_current_record = 'No';
        	}
        	// $visits = $this->process_model->getPatientQuestions($is_exist_patient->id);
        	$visits = $this->process_model->getVisitsByPatientId($is_exist_patient->id);
		    $output['visits'] = $visits;
		    $output['is_current_record'] = $is_current_record;
        } else {
        	redirect('patients');
        }
        
        $records = $this->side_effects->getAllData();
		//pr($records); die;
		$output['records'] = $records;

		$this->load->view('front/includes/header', $output);
		$this->load->view('front/nebenwirkung/nebenwirkungen-auswertung');
		$this->load->view('front/includes/footer');
	}
}
