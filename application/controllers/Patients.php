<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Patients extends CI_Controller {


    function __construct() {

        Parent::__construct();
        $this->load->model('Patients_model','patients');
        
    }


    public function index() 
    {
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }
        $output['menu'] = 'Dashboard';
        if ($this->input->post()) {
            $this->form_validation->set_rules('versichertennummer', 'versichertennummer', 'trim|required|regex_match[/^[a-zA-Z][0-9]{9}$/]', array('required' => 'Bitte geben Sie Ihre Versicherungsnummer ein.',
                'regex_match' => 'Die Krankenversicherungsnummer besteht immer aus zehn Zeichen. Das erste ist ein Buchstabe gefolgt von neun Zahlen'));  
            /*$this->form_validation->set_rules('versichertennummer', 'versichertennummer', 'trim|required');*/
            
            if ($this->form_validation->run()) {
                $versichertennummer = $this->input->post('versichertennummer');

                $is_patient_exists = $this->patients->getPatientByInsuranceNumber($versichertennummer);

                if ($is_patient_exists) {

                	$doctor_id = $this->session->userdata('user_id');
                	$patient_id = $is_patient_exists->id;
                	$doctor_patient_exists = $this->patients->checkDoctorPatientByIds($doctor_id, $patient_id);
                	$other_patient_doctor_exists = $this->patients->checkOtherDoctorPatientById($patient_id);

                	if ($doctor_patient_exists) {
                		
		                $success = true;
		                $message = false;
		                $data['redirectURL'] = base_url('patients/anlegen?in='.$versichertennummer);

                	} else if ($other_patient_doctor_exists) {
                		$success = false;
                		$message = 'Dieser Patient wird bereits von einem anderen Arzt behandelt';
                	} else {
                		/*New Register*/
                		$success = true;
		                $message = false;
		                $data['redirectURL'] = base_url('patients/anlegen?in='.$versichertennummer);
                	}         	

                } else {
                	$success = true;
                    $message = false;
                    $data['redirectURL'] = base_url('patients/anlegen?in='.$versichertennummer);
                }

                
            } else {
                $message = validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $this->load->view('front/includes/header', $output);
        $this->load->view('front/pages/patient-laden');
        $this->load->view('front/includes/footer');
    }

    public function anlegen() 
    {
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }
        $output['header_menu'] = 'patients';
        $output['header_sub_menu'] = 'anlegen';
        $output['hauptindikation_text'] = 'Hauptindikation auswählen';

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
        if ($is_exist_patient) {
            $output['geburtsdatum'] = date('d', strtotime($is_exist_patient->date_of_birth));
        } else {
            $output['geburtsdatum'] = '';
        }

        if ($is_exist_patient) {
        	//pr($is_exist_patient); die;
        	$output['date_of_birth_date'] = date('d', strtotime($is_exist_patient->date_of_birth));
        	$output['date_of_birth_month'] = date('M', strtotime($is_exist_patient->date_of_birth));
        	$output['dob_month'] = date('m', strtotime($is_exist_patient->date_of_birth));
        	$output['date_of_birth_year'] = date('Y', strtotime($is_exist_patient->date_of_birth));
        } else {
        	$output['dob_month'] = '';
        	$output['date_of_birth_date'] = '';
        	$output['date_of_birth_month'] = '';
        	$output['date_of_birth_year'] = '';
        }
        $doctor_id = $this->session->userdata('user_id');

        /*if ($is_exist_patient) {

        	$patient_id = $is_exist_patient->id;
        	$doctor_patient_exists = $this->patients->checkDoctorPatientByIds($doctor_id, $patient_id);
        	$other_patient_doctor_exists = $this->patients->checkOtherDoctorPatientById($patient_id);

        	if ($doctor_patient_exists) {
       

        	} else if ($other_patient_doctor_exists) {
        		$success = false;
        		$message = 'This patient is consulting with other doctor';
        		$data['message'] = $message;
	            $data['success'] = $success;
	            json_output($data);
        	} else {
        		
        		$success = true;
                $message = false;
                $data['redirectURL'] = base_url('patients/anlegen?in='.$insured_number);
                $data['message'] = $message;
	            $data['success'] = $success;
	            json_output($data);
        	}         	

        } else {
        	$message = 'Please enter correct versichertennummer';
        	$success = false;
        	$data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }*/

        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'name', 'trim|required', array('required' => 'Name ist ein Pflichtfeld, dass ausgefüllt werden muss'));
            //$this->form_validation->set_rules('geburtsdatum', 'geburtsdatum', 'trim|required', array('required' => 'Geburtsdatum ist ein Pflichtfeld, dass ausgefüllt werden muss'));
            $this->form_validation->set_rules('vorname', 'Vorname', 'trim|required', array('required' => 'Vorname ist ein Pflichtfeld, dass ausgefüllt werden muss'));
            $this->form_validation->set_rules('telefonnummer', 'telefonnummer', 'trim|required|min_length[6]|max_length[20]|regex_match[/^[+0-9 ]+$/]', 
                array('required' => 'Telefonnummer ist ein Pflichtfeld, dass ausgefüllt werden muss',
                'regex_match' => 'Die Eingabe entspricht nicht den Vorgaben einer Telefonnummer',
                'min_length' => 'Das Telefonnummer-Feld muss mindestens 6 Zeichen lang sein.',
                'max_length' => 'Das Telefonnummer-Feld darf nicht länger als 20 Zeichen sein.'));
            $this->form_validation->set_rules('diagnose_code', 'diagnose code', 'trim', array('required' => 'Diagnose code ist ein Pflichtfeld'));
            $this->form_validation->set_rules('hauptindikation', 'Hauptindikation', 'trim', array('required' => 'Hauptindikation ist ein Pflichtfeld'));
            $this->form_validation->set_rules('date_of_birth_date', 'Date of Birth Date', 'trim|required', array('required' => 'Geburtsdatum ist ein Pflichtfeld'));
            $this->form_validation->set_rules('date_of_birth_month', 'Date of Birth Month', 'trim|required', array('required' => 'Geburtsmonat ist ein Pflichtfeld'));
            $this->form_validation->set_rules('date_of_birth_year', 'Date of Birth Year', 'trim|required', array('required' => 'Geburtsjahr ist ein Pflichtfeld'));
            //$this->form_validation->set_rules('grad', 'Grad', 'trim|required', array('required' => 'Grad ist ein Pflichtfeld'));
            //$this->form_validation->set_rules('nicht', 'Nicht zuordenbare Hauptindikation', 'trim');
            // $this->form_validation->set_rules('is_chronic_indication', 'Chronic indication', 'required');

            if ($this->form_validation->run()) {
                
                $saveData['insured_number'] = $insured_number; 
                $saveData['first_name'] = $this->input->post('vorname'); 
                $saveData['last_name'] = $this->input->post('name'); 
                $saveData['grad'] = $this->input->post('grad');
                
                $date_of_birth_date = $this->input->post('date_of_birth_date'); 
                $date_of_birth_month = $this->input->post('date_of_birth_month'); 
                $date_of_birth_year = $this->input->post('date_of_birth_year');
                $date_of_birth = date('Y-m-d', mktime(0,0,0,$date_of_birth_month, $date_of_birth_date, $date_of_birth_year));
                //pr($date_of_birth); die;
                $saveData['date_of_birth'] = $date_of_birth; 
                $fallnummer = $this->input->post('fallnummer', true);
                $saveData['telephone_number'] = $this->input->post('telefonnummer'); 
                $saveData['diagnostic_code'] = $this->input->post('diagnose_code'); 
                $saveData['hauptindikation'] = $this->input->post('hauptindikation'); 
                //$saveData['main_indication_not_assigned'] = $this->input->post('nicht');
                //pr($saveData); die;

                if ($this->input->post('is_chronic_indication')) {
                    $saveData['is_chronic_indication'] = 'Yes'; 
                } else {
                    $saveData['is_chronic_indication'] = 'No'; 
                }
                
                $saveData['pain_scale'] = $this->input->post('pain_scale',true); 

                if ($this->input->post('nrs_scale')) {
                    $saveData['nrs_scale'] = $this->input->post('nrs_scale'); 
                }
                //$is_patient_signed = $this->input->post('is_patient_signed');
                    
                //if ($is_patient_signed == 'Yes') {

                    if ($is_exist_patient) {
                        $id = $is_exist_patient->id;
                        $response = $this->patients->update_patient($id, $saveData);
                        
                        if ($response) {
                            $success = true;
                            $message = 'Die Änderungen wurden gespeichert.';
                            $data['redirectURL'] = base_url('patients/anlegen2?in='.$insured_number);
                        } else {
                            $success = false;
                            $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
                        }

                    } else {
                        $patient_count = $this->patients->countPatientsByDoctorId($doctor_id);

                        if ($doctor_id >= "10") {
                            $next_patient_number = $patient_count + 1;
                            $fallnummer = "$doctor_id-$next_patient_number";
                            $saveData['case_number'] = $fallnummer;
                        } else {
                            $next_patient_number = $patient_count + 1;
                            $fallnummer = "$doctor_id-0$next_patient_number";
                            $saveData['case_number'] = $fallnummer;
                        }
                        $response = $this->patients->add_patient($saveData);
                        //pr($response);
                        if ($response) {
                            
                            $data = array();
                            $data['patient_id'] = $response;
                            $data['doctor_id'] = $this->session->userdata('user_id');
                            $data['add_date'] = getDefaultToGMTDate(time());
                            $this->patients->add_doctorsid($data);
                            //pr($date);die;

                            $success = true;
                            $message = 'Der neue Patient wurde angelegt.';
                            $data['redirectURL'] = base_url('patients/anlegen2?in='.$insured_number);
                        } else {
                            $success = false;
                            $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
                        }
                    }

                /*} else {
                    $success = false;
                    $message = false;
                    $data['callBackFunction'] = 'requiredPatientSigned';
                }*/
            } else {
                $message = validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        } 
        $patient_count = $this->patients->countPatientsByDoctorId($doctor_id);

        if (isset($is_exist_patient->case_number) && $is_exist_patient->case_number) {
            $output['fallnummer'] = $is_exist_patient->case_number;
            
        } else {

            if ($doctor_id >= "10") {
                $next_patient_number = $patient_count + 1;
                $fallnummer = "$doctor_id-$next_patient_number";
                $output['fallnummer'] = $fallnummer;
            } else {
                $next_patient_number = $patient_count + 1;
                $fallnummer = "$doctor_id-0$next_patient_number";
                $output['fallnummer'] = $fallnummer;
            }
        }
        $icd_code_zuordnung = $this->patients->icd_code_zuordnung();

        foreach ($icd_code_zuordnung as $key => $value) {
            if ($is_exist_patient) {
                
                if ($is_exist_patient->hauptindikation == $value->id) {
                    $output['hauptindikation_text'] = $value->indikation;
                    //pr($icd_code_zuordnung); die;
                }
            }
        }
        $output['icd_code_zuordnung'] = $icd_code_zuordnung;
        $output['insured_number'] = $insured_number;
        $output['is_exist_patient'] = $is_exist_patient;
        $this->load->view('front/includes/header', $output);
        $this->load->view('front/pages/anlegen-1');
        $this->load->view('front/includes/footer');
    }

    public function anlegen2() 
    {
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }
        $output['header_menu'] = 'patients';
        $output['header_sub_menu'] = 'laden';
        $output['name_cannabis_text'] = '';

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
        //pr($is_exist_patient); die;
        if ($this->input->post()) {
            
            if ($this->input->post('bestehendes') == 'Yes') {
                /*if (!empty($this->input->post('name_eines_nicht')) || !empty($this->input->post('tagesdosis_second')) || !empty($this->input->post('tagesdosis_mg_thc_second')) || !empty($this->input->post('dosiseinheit_second'))) {
                    $this->form_validation->set_rules('name_cannabis', 'Cannabis Medikament', 'trim');
                    $this->form_validation->set_rules('dosiseinheit_first', 'Dosiseinheit', 'trim'); 
                    $this->form_validation->set_rules('tagesdosis_first', 'Tagesdosis', 'trim');
                    $this->form_validation->set_rules('tagesdosis_mg_first', 'Tagesdosis mg', 'trim');
                    
                    if (!empty($this->input->post('name_eines_nicht'))) {
                        
                        $this->form_validation->set_rules('dosiseinheit_second', 'Dosiseinheit', 'trim|required',
                            array('required' => 'Dosiseinheit ist ein Pflichtfeld'));
                        $this->form_validation->set_rules('tagesdosis_second', 'tagesdosis', 'trim|required',
                            array('required' => 'Tagesdosis ist ein Pflichtfeld'));
                        $this->form_validation->set_rules('tagesdosis_mg_thc_second', 'tagesdosis mg thc', 'trim|required',
                            array('required' => 'Tagesdosis mg ist ein Pflichtfeld'));                    
                    } else if (!empty($this->input->post('tagesdosis_second'))) {
                        $this->form_validation->set_rules('name_eines_nicht', 'Name eines Nicht zuordenbares Cannabis Medikament', 'trim|required',
                            array('required' => 'Das Feld Name eines Nicht zuordenbares Cannabis-Verbrauchs ist erforderlich.'));
                        $this->form_validation->set_rules('dosiseinheit_second', 'Dosiseinheit', 'trim|required',
                            array('required' => 'Dosiseinheit ist ein Pflichtfeld'));
                        
                        $this->form_validation->set_rules('tagesdosis_mg_thc_second', 'tagesdosis mg thc', 'trim|required',
                        array('required' => 'Tagesdosis mg ist ein Pflichtfeld'));
                    } else if (!empty($this->input->post('dosiseinheit_second'))) {
                        $this->form_validation->set_rules('name_eines_nicht', 'Name eines Nicht zuordenbares Cannabis Medikament', 'trim|required', 
                            array('required' => 'Das Feld Name eines Nicht zuordenbares Cannabis-Verbrauchs ist erforderlich.'));
                        //$this->form_validation->set_rules('dosiseinheit_second', 'Dosiseinheit', 'trim|required');
                        $this->form_validation->set_rules('tagesdosis_second', 'tagesdosis', 'trim|required', 
                            array('required' => 'Tagesdosis ist ein Pflichtfeld'));
                        
                    } else if (!empty($this->input->post('tagesdosis_mg_thc_second'))) {
                        $this->form_validation->set_rules('name_eines_nicht', 'Name eines Nicht zuordenbares Cannabis Medikament', 'trim|required', 
                            array('required' => 'Das Feld Name eines Nicht zuordenbares Cannabis-Verbrauchs ist erforderlich.'));
                        $this->form_validation->set_rules('dosiseinheit_second', 'Dosiseinheit', 'trim|required');
                        $this->form_validation->set_rules('tagesdosis_second', 'tagesdosis', 'trim|required', 
                            array('required' => 'Tagesdosis ist ein Pflichtfeld'));
                        
                    }
                } else {*/
                    $this->form_validation->set_rules('name_cannabis', 'Cannabis Medikament', 'trim|required', array('required' => 'Das Cannabis Cannabis Feld ist erforderlich.'));
                    $this->form_validation->set_rules('dosiseinheit_first', 'Dosiseinheit', 'trim|required', 
                        array('required' => 'Dosiseinheit ist ein Pflichtfeld'));
                    $this->form_validation->set_rules('tagesdosis_first', 'Tagesdosis', 'trim|required', 
                        array('required' => 'Tagesdosis ist ein Pflichtfeld'));
                    $this->form_validation->set_rules('tagesdosis_mg_first', 'Tagesdosis mg', 'trim|required', 
                        array('required' => 'Tagesdosis mg ist ein Pflichtfeld'));

                    //$this->form_validation->set_rules('grund_fur_umstellung[]', 'grund fur umstellung', 'trim|required', array('required' => 'Bitte wählen Sie einen Grund aus, warum eine Umstellung stattfindet'));
                    if ($this->input->post('grund_fur_umstellung') || $this->input->post('anderer_grund')) {
                    	
                    } else {

                    	$this->form_validation->set_rules('anderer_grund', 'anderer grund', 'trim|required', array('required' => 'Bitte wählen Sie einen anderer grund aus, warum eine Umstellung stattfindet'));
                    }
                    
                //}

            } else {
                $success = true;
                $this->form_validation->set_rules('bestehendes', 'bestehendes', 'trim|required', array('required' => 'Das Feld bestehendes ist erforderlich.'));
            }

            if ($this->form_validation->run()) {

                $saveData['bestehendes'] = $this->input->post('bestehendes');
                $saveData['name_cannabis'] = $this->input->post('name_cannabis');
                
                if ($this->input->post('bestehendes') == 'Yes') {
                    $saveData['dosiseinheit_first'] = $this->input->post('dosiseinheit_first');
                    $saveData['tagesdosis_first'] = str_replace(',', '.', $this->input->post('tagesdosis_first'));
                    $saveData['tagesdosis_mg_first'] = str_replace(',', '.', $this->input->post('tagesdosis_mg_first'));
                    $saveData['name_eines_nicht'] = $this->input->post('name_eines_nicht');
                    $saveData['dosiseinheit_second'] = $this->input->post('dosiseinheit_second');
                    $saveData['tagesdosis_second'] = str_replace(',', '.', $this->input->post('tagesdosis_second'));
                    $saveData['tagesdosis_mg_thc_second'] = str_replace(',', '.', $this->input->post('tagesdosis_mg_thc_second'));
                    $grund_fur_umstellung = $this->input->post('grund_fur_umstellung');
                    if ($grund_fur_umstellung) {
	                    $grund_fur_umstellung_arr = implode(',', $grund_fur_umstellung);
	                    $saveData['grund_fur_umstellung'] = $grund_fur_umstellung_arr;
                    }
                    $saveData['anderer_grund'] = $this->input->post('anderer_grund');
                } else {
                    $saveData['dosiseinheit_first'] = NULL;
                    $saveData['tagesdosis_first'] = NULL;
                    $saveData['tagesdosis_mg_first'] = NULL;
                    $saveData['name_eines_nicht'] = NULL;
                    $saveData['dosiseinheit_second'] = NULL;
                    $saveData['tagesdosis_second'] = NULL;
                    $saveData['tagesdosis_mg_thc_second'] = NULL;
                    //$grund_fur_umstellung = $this->input->post('grund_fur_umstellung');
                    //pr($grund_fur_umstellung); die;
                    //$grund_fur_umstellung_arr = implode(',', $grund_fur_umstellung);
                    
                    $saveData['grund_fur_umstellung'] = NULL;
                    $saveData['anderer_grund'] = NULL;
                }
                
                $id = $is_exist_patient->id;
                $response = $this->patients->update_patient($id, $saveData);
                    
                if ($response) {
                    $success = true;
                    $message = 'Die Änderungen wurden gespeichert.';
                    /*if ($this->input->post('bestehendes') == 'Yes') {
                        $data['redirectURL'] = base_url('therapie/umstellung?in='.$insured_number);    
                    } else {

                        $data['redirectURL'] = base_url('patients/begleiterkrankungen?in='.$insured_number);
                    }*/

                    $data['redirectURL'] = base_url('patients/begleiterkrankungen?in='.$insured_number);

                } else {
                    $success = false;
                    $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
                } 
                    
                
            } else {
                $message = validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);  
        }

        $cannabis_medikament = $this->patients->getCannabisMedikament();

        foreach ($cannabis_medikament as $key => $value) {
            if ($is_exist_patient) {
                
                if ($is_exist_patient->name_cannabis == $value->id) {
                    $output['name_cannabis_text'] = $value->name;
                }
            }
        }
        if ($is_exist_patient) {
            if ($is_exist_patient->bestehendes == "Yes") {
                $output['bestehendes'] = "Ja";
            } else {
                $output['bestehendes'] = "Nein";
            }
        }
        $output['is_exist_patient'] = $is_exist_patient;
        $output['insured_number'] = $insured_number;
        $output['cannabis_medikament'] = $cannabis_medikament;
        $this->load->view('front/includes/header', $output);
        $this->load->view('front/pages/anlegen-2');
        $this->load->view('front/includes/footer');
    }

    public function begleiterkrankungen() 
    {

        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }
        $output['header_menu'] = 'patients';
        $output['header_sub_menu'] = 'begleiterkrankungen';

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        $output['is_exist_patient'] = $is_exist_patient;
        $output['insured_number'] = $insured_number;
        
        if ($this->input->post()) {
            
            $relevante = $this->input->post('relevante');

            foreach ($relevante as $key => $value) {
       
                if (!empty($value['description']) && $value['description']) {
                    
                    if ((isset($value['option']) && !empty($value['option']))) {
                        
                    } else {
                         $message = 'Bitte wählen Sie Bestehend seit für Zeile ('.$key.')';
                        $success = false;
                        $data['success'] = $success;
                        $data['message'] = $message;
                        json_output($data); die;
                    }
                } else if ((isset($value['option']) && !empty($value['option']))) {
                    
                    if (!empty($value['description']) && $value['description']) {

                    } else {
                        $message = 'Bitte geben Sie die relevante Begleiterbedingung für die Zeile ein ('.$key.')';
                        $success = false;
                        $data['success'] = $success;
                        $data['message'] = $message;
                        json_output($data); die;
                    }
                }               
            }

            if ($relevante) {
                $saveData = array();
                $i = 0;

                foreach ($relevante as $key => $value) {

                    if ($value['description'] && (isset($value['option']) && !empty($value['option']))) {
                        $saveData[$i]['patient_id'] = $is_exist_patient->id;
                        $saveData[$i]['description'] = $value['description'];
                        $saveData[$i]['bestehend_seit'] = $value['option'];
                        $saveData[$i]['add_date'] = getDefaultToGMTDate(time());
                        $i++;
                    }
                    //pr($saveData);
                }
                //die;

                if ($saveData) {
                    $this->patients->add_begleiterkrankungen($saveData, $is_exist_patient->id);

                    $id = $is_exist_patient->id;
                    $patientData['is_profile_complete'] = 'Yes'; 
                    $response = $this->patients->update_patient($id, $patientData);
                    $success = true;
                    $message = 'Die Änderungen wurden gespeichert.';

                    if ($is_exist_patient->bestehendes == 'Yes') {
                        $data['redirectURL'] = base_url('therapie/umstellung?in='.$insured_number);    
                    } else {
                        $data['redirectURL'] = base_url('therapie?in='. $insured_number);
                    }
                } else {
                    $id = $is_exist_patient->id;
                    $patientData['is_profile_complete'] = 'Yes'; 
                    $response = $this->patients->update_patient($id, $patientData);
                    $success = true;
                    $message = 'Die Änderungen wurden gespeichert.';
                    $data['redirectURL'] = base_url('therapie?in='. $insured_number);

                    if ($is_exist_patient->bestehendes == 'Yes') {
                        $data['redirectURL'] = base_url('therapie/umstellung?in='.$insured_number);    
                    } else {
                        $data['redirectURL'] = base_url('therapie?in='. $insured_number);
                    }


                }
            } else {
                $success = false;
                $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
            }

            $data['success'] = $success;
            $data['message'] = $message;
            json_output($data);
        }
        

        if ($is_exist_patient) {
            $begleiterkrankungen = $this->patients->getPatientBegleiterkrankungen($is_exist_patient->id);
            $output['begleiterkrankungen'] = $begleiterkrankungen;
        }
        // pr($output); die;
        $this->load->view('front/includes/header', $output);
        $this->load->view('front/pages/begleiterkrankung');
        $this->load->view('front/includes/footer');
    }

    public function add_begleiterkrankungen_row(){
        
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }

        $digit = $this->input->post('digit') + 1;
        $html = '<div class="row align-items-center options-rows">
                <div class="col-md-8">
                   <div class="no-input">
                      <strong>'.$digit.'</strong>
                      <input type="text" name="relevante['.$digit.'][description]" class="form-control" placeholder="">
                   </div>
                </div>
                <div class="col-md-4">
                   <div class="row list-cheak">
                      <div class="col ">
                         <div class="checkbox-custom small">
                             <input type="radio" name="relevante['.$digit.'][option]" value="3" id="me10-'.$digit.'">
                             <label for="me10-'.$digit.'"><span class="cheak"></span></label>
                          </div>
                      </div>
                      <div class="col ">
                         <div class="checkbox-custom small">
                                <input type="radio" name="relevante['.$digit.'][option]" value="6" id="me-120-'.$digit.'">
                                <label for="me-120-'.$digit.'"><span class="cheak"></span></label>
                             </div>
                      </div>
                      <div class="col ">
                         <div class="checkbox-custom small">
                                <input type="radio" name="relevante['.$digit.'][option]" value="12" id="me33-'.$digit.'">
                                <label for="me33-'.$digit.'"><span class="cheak"></span></label>
                             </div>
                      </div>
                      <div class="col cheak-width"></div>
                   </div>
                </div>
             </div>';
        $data['html'] = $html;
        json_output($data);
    }

    public function begleitmedikation() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/begleitmedikation');
        $this->load->view('front/includes/footer');
    }

    public function begleitmedikation_verlauf() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/begleitmedikation-verlauf');
        $this->load->view('front/includes/footer');
    }

    public function laborwerte() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/laborwerte');
        $this->load->view('front/includes/footer');
    }

    public function laborwerte_auswertung() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/laborwerte-auswertung');
        $this->load->view('front/includes/footer');
    }

    public function lebensqualitat() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/lebensqualitat');
        $this->load->view('front/includes/footer');
    }

    public function nebenwirkungen() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/nebenwirkungen');
        $this->load->view('front/includes/footer');
    }

    public function nebenwirkungen_auswertung() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/nebenwirkungen-auswertung');
        $this->load->view('front/includes/footer');
    }

    public function schmer() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/schmer');
        $this->load->view('front/includes/footer');
    }

    public function schlafqualitat() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/schlafqualität');
        $this->load->view('front/includes/footer');
    }
    
}