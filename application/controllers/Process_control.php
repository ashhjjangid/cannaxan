<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Process_control extends CI_Controller {


    function __construct() {

        Parent::__construct();
        $this->load->model('process_control_model', 'process_model');
        $this->load->model('patients_model', 'patients');
    }

    function add_visiteplan_row() {
	    $output = array();
	    $insured_number = $this->input->post('insured_number');
      $is_current_date_exist = $this->input->post('row_counter');

	    $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

	    if ($is_exist_patient) {
        $current_date = date('Y-m-d');
        $is_date_exist = $this->process_model->checkVisitExistByDate($is_exist_patient->id, $current_date);
	    	
        if ($is_current_date_exist) {
          $data['success'] = false;
          $data['message'] = 'Sie können nur einen Besuch pro Tag hinzufügen.';
          echo json_encode($data); die;
        } else if ($is_date_exist) {
          $data['success'] = false;
          $data['message'] = 'Sie können nur einen Besuch pro Tag hinzufügen.';
          echo json_encode($data); die;
        } else {
          $last_date_severe_pain_last_week = $this->process_model->get_last_date_severe_pain_last_week_1($is_exist_patient->id);

          if ($last_date_severe_pain_last_week) {
            $schmerz = $last_date_severe_pain_last_week->pain_after_30_min_after_thc;
          } else {
            $schmerz = $this->getSchmerz($is_exist_patient->id, 'DESC');
          }
        	$lebensqualitat = $this->getLebensqualitat($is_exist_patient->id, 'DESC');
        	$schlafqualitat = $this->getSchlafqualitat($is_exist_patient->id, 'DESC');
        	$output['schmerz'] = $schmerz;
          $output['lebensqualitat'] = $lebensqualitat;
          $output['schlafqualitat'] = $schlafqualitat;

  		    $row_nmber = $this->input->post('row_number') + 1;
  		    $output['next_row'] = $row_nmber;
  		    $html = $this->load->view('front/verlaufskontrolle/schmerzindikationen_view', $output, true);
  		    $data['success'] = true;
          $data['html'] = $html;
  		    echo json_encode($data); die;
       }

	    }
    }


    public function add_visites()
    {
    	$success = true;
    	$insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {

	    	if (isset($_POST['visit']) && !empty($_POST['visit'])) {
	    		$error_count = 0;
	    		$error_message = '';
	    		$total_visit = 0;
	    		foreach ($_POST['visit'] as $key => $value) {
	    			
	    			if (empty($value['datum'])) {
	    				$error_message.= '<p> Bitte wählen Sie das Datum des Besuchsommers '.$value['visit'].'.</p>';
	    				$error_count++; 
	    			}
	    		}

	    		if ($error_count > 0) {
	    			$success = false;
	    			$message = $error_message;
	    		} else {
	    			$datum = array();
	    			$i = 0;

	    			foreach ($_POST['visit'] as $key => $value) {
	    				$datum[$i] = $value['datum'];
	    				$i++;
		    		}

		    		$unique_visit = count(array_unique($datum));
		    		$total_visit = $i;

		    		if ($unique_visit == $total_visit) {
		    			$saveData = array();
		    			$j = 0;

		    			foreach ($_POST['visit'] as $key => $value) {
		    				$datum = str_replace('.', '-', $value['datum']);
		    				$datum = date('Y-m-d', strtotime($datum));
	    					$is_date_exist = $this->process_model->checkVisitExistByDate($is_exist_patient->id, $datum);

	    					if ($is_date_exist) {
	    						$success = false;
	    						$message = 'Das datum ('.$value['datum'].') ist bereits vorhanden.';
	    						break;
	    					}

	    					$saveData[$j]['patient_id'] = $is_exist_patient->id;
	    					$saveData[$j]['visite'] = $value['visit'];
	    					$saveData[$j]['datum'] = $datum;
	    					$saveData[$j]['nrs_schmerz'] = $value['nrs_schmerz'];
	    					$saveData[$j]['sf_12_lebensqualitat'] = $value['sf_12_lebensqualitat'];
	    					$saveData[$j]['ris_schlafqualitat'] = $value['ris_schlafqualitat'];
	    					$saveData[$j]['add_date'] = getDefaultToGMTDate(time());
	    					$j++;
			    		}

			    		if ($success) {

			    			if ($saveData) {
			    				$this->process_model->add_visites($saveData);
			    				$success = true;
	    						$message = 'Besuch hinzugefügt.';
	    						$data['redirectURL'] = base_url('process_control/visitenplan?in='.$insured_number);
			    			} else {
			    				$success = false;
	    						$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch einmal.';
			    			}
			    		}
		    		} else {
		    			$success = false;
		    			$message = 'Bitte wählen Sie ein eindeutiges Datum im Bezugsfeld.';
		    		}
		    		
	    		}

	    	} else {
	    		$success = false;
		   		$message = 'Bitte fügen Sie mindestens eine Zeile hinzu.';
	    	}

			$data['success'] = $success;
			$data['message'] = $message;
			json_output($data);
        }

    }

  	public function visitenplan() 
    {   
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
        	
          	$patient_visits = $this->process_model->getVisitsByPatientId($is_exist_patient->id);
          	
          	if ($patient_visits) {

          		foreach ($patient_visits as $key => $value) {
              		$verlaufskontrolle_laborwerte = $this->process_model->getLatestVerlaufskontrolleLaborwerte($is_exist_patient->id, $value->visite);

                  	$begleitmedikation_text = '';
                  	$begleitmedikation_detail = $this->process_model->getLatestBegleitmedikationDetail($is_exist_patient->id, $value->datum);
                  	
                  	if ($begleitmedikation_detail) {

                    	foreach ($begleitmedikation_detail as $k => $v) {
                      		
                      		if ($begleitmedikation_text) {
                        		$begleitmedikation_text = $begleitmedikation_text.$v->handelsname.'<br>';
                      		} else {
                        		$begleitmedikation_text = $v->handelsname.'<br>';
                      		}
                    	}
                  	}

              		$nebenwirkung = $this->process_model->getLatestBebenwirkung($is_exist_patient->id, $value->visite);
                  	$nebenwirkung_text = '';

                  	if ($nebenwirkung) {
                    	$nebenwirkung_detail = $this->process_model->getSideEffectsDataByBehandlunggstagId($nebenwirkung->id);
                    
                    	foreach ($nebenwirkung_detail as $k => $v) {
                      
          							if ($nebenwirkung_text) {
          								$nebenwirkung_text = $nebenwirkung_text.$v->nebenwirkung_name.'<br>';
          							} else {
          								$nebenwirkung_text = $v->nebenwirkung_name.'<br>';
          							}
                    	} 
                  	}

          			$value->nebenwirkung = $nebenwirkung_text;
                  	$value->begleitmedikation = $begleitmedikation_text;
              		$value->verlaufskontrolle_laborwerte = $verlaufskontrolle_laborwerte;
            	}
          	} else {
          		$schmerz = $this->getSchmerz($is_exist_patient->id, 'ASC');
          		$lebensqualitat = $this->getLebensqualitat($is_exist_patient->id, 'ASC');
          		$schlafqualitat = $this->getSchlafqualitat($is_exist_patient->id, 'ASC');
          		$output['schmerz'] = $schmerz;
      				$output['lebensqualitat'] = $lebensqualitat;
      				$output['schlafqualitat'] = $schlafqualitat;
          	}
          	// pr($patient_visits); die;
			$output['header_menu'] = 'process_control';
			$output['header_sub_menu'] = 'Visitenplan';
			$output['insured_number'] = $insured_number;
			$output['patient_visits'] = $patient_visits;
			$output['is_exist_patient'] = $is_exist_patient;
			$this->load->view('front/includes/header', $output);
			$this->load->view('front/verlaufskontrolle/schmerzindikationen');
			$this->load->view('front/includes/footer');
        } else {
          redirect('patients');
        }
    }

    function getSchmerz($patient_id, $order_by = 'ASC')
    {
    	$first_severe_pain_last_week = $this->process_model->first_severe_pain_last_week($patient_id, $order_by);
		$first_no_pain_reliever_severe_pain = $this->process_model->first_no_pain_reliever_severe_pain($patient_id, $order_by);

		if ($first_severe_pain_last_week) {
			$severe_pain_last_week = $first_severe_pain_last_week->severe_pain_last_week;
		} else {
			$severe_pain_last_week = 0;
		}

		if ($first_no_pain_reliever_severe_pain) {
			$no_pain_reliever_severe_pain = $first_no_pain_reliever_severe_pain->no_pain_reliever_severe_pain;
		} else {
			$no_pain_reliever_severe_pain = 0;
		}

		$total = $severe_pain_last_week + $no_pain_reliever_severe_pain;
		return $total;
    }

    function getLebensqualitat($patient_id, $order_by = 'ASC')
    {
    	$app_lebensqualitat_1 = $this->process_model->patient_app_lebensqualitat_1($patient_id, $order_by);
		$question_1_point = 0;

        if ($app_lebensqualitat_1) {
            $question_1_point = $question_1_point + getQuestionPoint_1($app_lebensqualitat_1->health_status);
        }

        $question_2_point = 0;
        $app_lebensqualitat_2 = $this->process_model->patient_app_lebensqualitat_2($patient_id, $order_by);
        
        if ($app_lebensqualitat_2) {
            $question_2_point = $question_2_point + getQuestionPoint_2($app_lebensqualitat_2->health_activities_moderate);
        }

        $question_3_point = 0;
        $app_lebensqualitat_3 = $this->process_model->patient_app_lebensqualitat_3($patient_id, $order_by);

        if ($app_lebensqualitat_3) {
            $question_3_point = $question_3_point + getQuestionPoint_3($app_lebensqualitat_3->health_activities_climbing);
        }

        $question_4_point = 0;
        $question_5_point = 0;
        $app_lebensqualitat_4 = $this->process_model->patient_app_lebensqualitat_4($patient_id, $order_by);

        if ($app_lebensqualitat_4) {        
        	$question_4_point = $question_4_point + getQuestionPoint_4($app_lebensqualitat_4->physical_health_past_4_weeks_less);
        	$question_5_point = $question_5_point + getQuestionPoint_5($app_lebensqualitat_4->physical_health_past_4_weeks_certain);
        }

        $app_lebensqualitat_5 = $this->process_model->patient_app_lebensqualitat_5($patient_id, $order_by);

        $question_6_point = 0;
        $question_7_point = 0;

        if ($app_lebensqualitat_5) {
        	$question_6_point = $question_6_point + getQuestionPoint_6($app_lebensqualitat_5->mental_health_past_4_weeks_less);
        	$question_7_point = $question_7_point + getQuestionPoint_7($app_lebensqualitat_5->mental_health_past_4_weeks_could_not_be_careful);
        }

        $question_8_point = 0;
        $app_lebensqualitat_6 = $this->process_model->patient_app_lebensqualitat_6($patient_id, $order_by);

        if ($app_lebensqualitat_6) {
            $question_8_point = $question_8_point + getQuestionPoint_8($app_lebensqualitat_6->pain_in_daily_activities_past_4_weeks);
        }

        $question_9_point = 0;
        $app_lebensqualitat_7 = $this->process_model->patient_app_lebensqualitat_7($patient_id, $order_by);

        if ($app_lebensqualitat_7) {
            $question_9_point = $question_9_point + getQuestionPoint_9($app_lebensqualitat_7->clam_in_past_4_weeks);
        }

        $question_10_point = 0;
        $app_lebensqualitat_8 = $this->process_model->patient_app_lebensqualitat_8($patient_id, $order_by);

        if ($app_lebensqualitat_8) {
            $question_10_point = $question_10_point + getQuestionPoint_10($app_lebensqualitat_8->full_energy_in_past_4_weeks);
        }

        $question_11_point = 0;
        $app_lebensqualitat_9 = $this->process_model->patient_app_lebensqualitat_9($patient_id, $order_by);

        if ($app_lebensqualitat_9) {
            $question_11_point = $question_11_point + getQuestionPoint_11($app_lebensqualitat_9->discourage_sad_in_past_4_weeks);
        }

        $question_12_point = 0;
        $app_lebensqualitat_10 = $this->process_model->patient_app_lebensqualitat_10($patient_id, $order_by);

        if ($app_lebensqualitat_10) {
            $question_12_point = $question_12_point + getQuestionPoint_12($app_lebensqualitat_10->contact_with_others);
        }

        $lebensqualitat_total = $question_1_point + $question_2_point + $question_3_point + $question_4_point + $question_5_point + $question_6_point + $question_7_point + $question_8_point + $question_9_point + $question_10_point + $question_11_point + $question_12_point;

        return $lebensqualitat_total;
    }

    function getSchlafqualitat($patient_id, $order_by = 'ASC') 
    {
    	$app_schlafqualitat_1 = $this->process_model->patient_app_schlafqualitat_1($patient_id, $order_by);
        $question_13_point = 0;

        if ($app_schlafqualitat_1) {
            $question_13_point = $question_13_point + getQuestionPoint_13($app_schlafqualitat_1->time_takes_to_fall_asleep);
        }

        $question_14_point = 0;
        $app_schlafqualitat_2 = $this->process_model->patient_app_schlafqualitat_2($patient_id, $order_by);
        
        if ($app_schlafqualitat_2) {
            $question_14_point = $question_14_point + getQuestionPoint_14($app_schlafqualitat_2->avg_hours_sleep_at_night);
        }

        $question_15_point = 0;
        $app_schlafqualitat_3 = $this->process_model->patient_app_schlafqualitat_3($patient_id, $order_by);

        if ($app_schlafqualitat_3) {
            $question_15_point = $question_15_point + getQuestionPoint_15($app_schlafqualitat_3->cannot_sleep_through);
        }

        $question_16_point = 0;
        $app_schlafqualitat_4 = $this->process_model->patient_app_schlafqualitat_4($patient_id, $order_by);

        if ($app_schlafqualitat_4) {
            $question_16_point = $question_16_point + getQuestionPoint_16($app_schlafqualitat_4->wake_up_early);
        }

        $app_schlafqualitat_5 = $this->process_model->patient_app_schlafqualitat_5($patient_id, $order_by);

        $question_17_point = 0;

        if ($app_schlafqualitat_5) {
            $question_17_point = $question_17_point + getQuestionPoint_17($app_schlafqualitat_5->wake_up_with_noise);
        }

        $question_18_point = 0;
        $app_schlafqualitat_6 = $this->process_model->patient_app_schlafqualitat_6($patient_id, $order_by);

        if ($app_schlafqualitat_6) {
            $question_18_point = $question_18_point + getQuestionPoint_18($app_schlafqualitat_6->no_eye_all_night);
        }

        $question_19_point = 0;
        $app_schlafqualitat_7 = $this->process_model->patient_app_schlafqualitat_7($patient_id, $order_by);

        if ($app_schlafqualitat_7) {
            $question_19_point = $question_19_point + getQuestionPoint_19($app_schlafqualitat_7->think_much_sleep_after);
        }

        $question_20_point = 0;
        $app_schlafqualitat_8 = $this->process_model->patient_app_schlafqualitat_8($patient_id, $order_by);

        if ($app_schlafqualitat_8) {
            $question_20_point = $question_20_point + getQuestionPoint_20($app_schlafqualitat_8->afraid_not_able_to_sleep);
        }

        $question_21_point = 0;
        $app_schlafqualitat_9 = $this->process_model->patient_app_schlafqualitat_9($patient_id, $order_by);

        if ($app_schlafqualitat_9) {
            $question_21_point = $question_21_point + getQuestionPoint_21($app_schlafqualitat_9->full_powerful);
        }

        $question_22_point = 0;
        $app_schlafqualitat_10 = $this->process_model->patient_app_schlafqualitat_10($patient_id, $order_by);

        if ($app_schlafqualitat_10) {
            $question_22_point = $question_22_point + getQuestionPoint_22($app_schlafqualitat_10->taking_sleeping_pills);
        }
        
        $schlafqualitat_total = $question_13_point + $question_14_point + $question_15_point + $question_16_point + $question_17_point + $question_18_point + $question_19_point + $question_20_point + $question_21_point + $question_22_point;

        return $schlafqualitat_total;
    }


    function calculateVisitPlanColumCalculation($patient_id) 
    {
    	$patient_visits = $this->process_model->getPatientVisits($patient_id);
      	$visits = array();
      	$series = array();
      	$column = array();
      	$line = array();
      	$date_range = array();
        	
    	if ($patient_visits) {
            $s_date = '';
            $total_visit = count($patient_visits) - 1;

            foreach ($patient_visits as $key => $value) {

              if ($total_visit > 0) {
                

                if ($s_date) {
                  $dates = array();
                  $dates[] = $s_date;
                  $dates[] = $value->datum;
                  $date_range[] = $dates;
                }

                $s_date = $value->datum;
                if ($key == $total_visit) {
                    $dates = array();
                    $dates[] = $s_date;
                    $dates[] = date('Y-m-d');
                    $date_range[] = $dates;
                }
              } else {
                $dates = array();
                $dates[] = $value->datum;
                $dates[] = date('Y-m-d');
                $date_range[] = $dates;
              }
    		}
          	
    		$schmerz = array();
    		$lebensqualitat = array();
    		$schlafqualitat = array();

            if ($date_range) {
				$i = 1;

				foreach ($date_range as $key => $value) {
					$start_date = $value[0];
					$end_date = $value[1];
					$total_severe_pain_last_week = $this->process_model->total_severe_pain_last_week($patient_id ,$start_date, $end_date);
					$total_no_pain_reliever_severe_pain = $this->process_model->total_no_pain_reliever_severe_pain($patient_id ,$start_date, $end_date);

					$total = $total_severe_pain_last_week->total_severe_pain_last_week + $total_no_pain_reliever_severe_pain->total_no_pain_reliever_severe_pain;

					$schmerz[$i] = $total;
				

					$app_lebensqualitat_1 = $this->process_model->get_app_lebensqualitat_1($patient_id, $start_date, $end_date);
		            $question_1_point = 0;

		            if ($app_lebensqualitat_1) {

			            foreach ($app_lebensqualitat_1 as $k => $v) {
			            	$question_1_point = $question_1_point + getQuestionPoint_1($v->health_status);
			            }
		            }

		            $question_2_point = 0;
		            $app_lebensqualitat_2 = $this->process_model->get_app_lebensqualitat_2($patient_id, $start_date, $end_date);
		            
		            if ($app_lebensqualitat_2) {

			            foreach ($app_lebensqualitat_2 as $k => $v) {
			            	$question_2_point = $question_2_point + getQuestionPoint_2($v->health_activities_moderate);
			            }
		            }

		            $question_3_point = 0;
		            $app_lebensqualitat_3 = $this->process_model->get_app_lebensqualitat_3($patient_id, $start_date, $end_date);

		            if ($app_lebensqualitat_3) {

			            foreach ($app_lebensqualitat_3 as $k => $v) {
			            	$question_3_point = $question_3_point + getQuestionPoint_3($v->health_activities_climbing);
			            }
		            }

		            $question_4_point = 0;
		            $question_5_point = 0;
		            $app_lebensqualitat_4 = $this->process_model->get_app_lebensqualitat_4($patient_id, $start_date, $end_date);

		            if ($app_lebensqualitat_4) {

			            foreach ($app_lebensqualitat_4 as $k => $v) {
			            	$question_4_point = $question_4_point + getQuestionPoint_4($v->physical_health_past_4_weeks_less);
			            	$question_5_point = $question_5_point + getQuestionPoint_5($v->physical_health_past_4_weeks_certain);
			            }
		            }

		            $app_lebensqualitat_5 = $this->process_model->get_app_lebensqualitat_5($patient_id, $start_date, $end_date);

		            $question_6_point = 0;
		            $question_7_point = 0;

		            if ($app_lebensqualitat_5) {

			            foreach ($app_lebensqualitat_5 as $k => $v) {
			            	$question_6_point = $question_6_point + getQuestionPoint_6($v->mental_health_past_4_weeks_less);
			            	$question_7_point = $question_7_point + getQuestionPoint_7($v->mental_health_past_4_weeks_could_not_be_careful);
			            }
		            }

		            $question_8_point = 0;
		            $app_lebensqualitat_6 = $this->process_model->get_app_lebensqualitat_6($patient_id, $start_date, $end_date);

		            if ($app_lebensqualitat_6) {

			            foreach ($app_lebensqualitat_6 as $k => $v) {
			            	$question_8_point = $question_8_point + getQuestionPoint_8($v->pain_in_daily_activities_past_4_weeks);
			            }
		            }

		            $question_9_point = 0;
		            $app_lebensqualitat_7 = $this->process_model->get_app_lebensqualitat_7($patient_id, $start_date, $end_date);

		            if ($app_lebensqualitat_7) {

			            foreach ($app_lebensqualitat_7 as $k => $v) {
			            	$question_9_point = $question_9_point + getQuestionPoint_9($v->clam_in_past_4_weeks);
			            }
		            }

		            $question_10_point = 0;
		            $app_lebensqualitat_8 = $this->process_model->get_app_lebensqualitat_8($patient_id, $start_date, $end_date);

		            if ($app_lebensqualitat_8) {

			            foreach ($app_lebensqualitat_8 as $k => $v) {
			            	$question_10_point = $question_10_point + getQuestionPoint_10($v->full_energy_in_past_4_weeks);
			            }
		            }

		            $question_11_point = 0;
		            $app_lebensqualitat_9 = $this->process_model->get_app_lebensqualitat_9($patient_id, $start_date, $end_date);

		            if ($app_lebensqualitat_9) {

			            foreach ($app_lebensqualitat_9 as $k => $v) {
			            	$question_11_point = $question_11_point + getQuestionPoint_11($v->discourage_sad_in_past_4_weeks);
			            }
		            }

		            $question_12_point = 0;
		            $app_lebensqualitat_10 = $this->process_model->get_app_lebensqualitat_10($patient_id, $start_date, $end_date);

		            if ($app_lebensqualitat_10) {

			            foreach ($app_lebensqualitat_10 as $k => $v) {
			            	$question_12_point = $question_12_point + getQuestionPoint_12($v->contact_with_others);
			            }
		            }

		            $lebensqualitat_total = $question_1_point + $question_2_point + $question_3_point + $question_4_point + $question_5_point + $question_6_point + $question_7_point + $question_8_point + $question_9_point + $question_10_point + $question_11_point + $question_12_point;

	                $lebensqualitat[$i] = $lebensqualitat_total;


	                $app_schlafqualitat_1 = $this->process_model->get_app_schlafqualitat_1($patient_id, $start_date, $end_date);
		            $question_13_point = 0;

		            if ($app_schlafqualitat_1) {

			            foreach ($app_schlafqualitat_1 as $k => $v) {
			            	$question_13_point = $question_13_point + getQuestionPoint_13($v->time_takes_to_fall_asleep);
			            }
		            }

		            $question_14_point = 0;
		            $app_schlafqualitat_2 = $this->process_model->get_app_schlafqualitat_2($patient_id, $start_date, $end_date);
		            
		            if ($app_schlafqualitat_2) {

			            foreach ($app_schlafqualitat_2 as $k => $v) {
			            	$question_14_point = $question_14_point + getQuestionPoint_14($v->avg_hours_sleep_at_night);
			            }
		            }

		            $question_15_point = 0;
		            $app_schlafqualitat_3 = $this->process_model->get_app_schlafqualitat_3($patient_id, $start_date, $end_date);

		            if ($app_schlafqualitat_3) {

			            foreach ($app_schlafqualitat_3 as $k => $v) {
			            	$question_15_point = $question_15_point + getQuestionPoint_15($v->cannot_sleep_through);
			            }
		            }

		            $question_16_point = 0;
		            $app_schlafqualitat_4 = $this->process_model->get_app_schlafqualitat_4($patient_id, $start_date, $end_date);

		            if ($app_schlafqualitat_4) {

			            foreach ($app_schlafqualitat_4 as $k => $v) {
			            	$question_16_point = $question_16_point + getQuestionPoint_16($v->wake_up_early);
			            }
		            }

		            $app_schlafqualitat_5 = $this->process_model->get_app_schlafqualitat_5($patient_id, $start_date, $end_date);

		            $question_17_point = 0;

		            if ($app_schlafqualitat_5) {

			            foreach ($app_schlafqualitat_5 as $k => $v) {
			            	$question_17_point = $question_17_point + getQuestionPoint_17($v->wake_up_with_noise);
			            }
		            }

		            $question_18_point = 0;
		            $app_schlafqualitat_6 = $this->process_model->get_app_schlafqualitat_6($patient_id, $start_date, $end_date);

		            if ($app_schlafqualitat_6) {

			            foreach ($app_schlafqualitat_6 as $k => $v) {
			            	$question_18_point = $question_18_point + getQuestionPoint_18($v->no_eye_all_night);
			            }
		            }

		            $question_19_point = 0;
		            $app_schlafqualitat_7 = $this->process_model->get_app_schlafqualitat_7($patient_id, $start_date, $end_date);

		            if ($app_schlafqualitat_7) {

			            foreach ($app_schlafqualitat_7 as $k => $v) {
			            	$question_19_point = $question_19_point + getQuestionPoint_19($v->think_much_sleep_after);
			            }
		            }

		            $question_20_point = 0;
		            $app_schlafqualitat_8 = $this->process_model->get_app_schlafqualitat_8($patient_id, $start_date, $end_date);

		            if ($app_schlafqualitat_8) {

			            foreach ($app_schlafqualitat_8 as $k => $v) {
			            	$question_20_point = $question_20_point + getQuestionPoint_20($v->afraid_not_able_to_sleep);
			            }
		            }

		            $question_21_point = 0;
		            $app_schlafqualitat_9 = $this->process_model->get_app_schlafqualitat_9($patient_id, $start_date, $end_date);

		            if ($app_schlafqualitat_9) {

			            foreach ($app_schlafqualitat_9 as $k => $v) {
			            	$question_21_point = $question_21_point + getQuestionPoint_21($v->full_powerful);
			            }
		            }

		            $question_22_point = 0;
		            $app_schlafqualitat_10 = $this->process_model->get_app_schlafqualitat_10($patient_id, $start_date, $end_date);

		            if ($app_schlafqualitat_10) {

			            foreach ($app_schlafqualitat_10 as $k => $v) {
			            	$question_22_point = $question_22_point + getQuestionPoint_22($v->taking_sleeping_pills);
			            }
		            }

		            $schlafqualitat_total = $question_13_point + $question_14_point + $question_15_point + $question_16_point + $question_17_point + $question_18_point + $question_19_point + $question_20_point + $question_21_point + $question_22_point;

	                $schlafqualitat[$i] = $schlafqualitat_total;
					$i++;
				}
            }

            $returnData['schmerz'] = $schmerz;
            $returnData['lebensqualitat'] = $lebensqualitat;
            $returnData['schlafqualitat'] = $schlafqualitat;
            return $returnData;
    	} else {
            return false;
    	}
    }

    public function schmerz() 
    {   
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
          	$visits = array();
          	$series = array();
          	$column = array();
          	$line = array();
          	$date_range = array();

        	$first_date_severe_pain_last_week = $this->process_model->get_first_date_severe_pain_last_week_1($is_exist_patient->id);
        	$last_date_severe_pain_last_week = $this->process_model->get_last_date_severe_pain_last_week_1($is_exist_patient->id);

        	if ($first_date_severe_pain_last_week && $last_date_severe_pain_last_week) {

        		$start_add_date = strtotime(getGMTDateToLocalDate($first_date_severe_pain_last_week->add_date,'Y-m-d'));
        		$end_add_date = strtotime(getGMTDateToLocalDate($last_date_severe_pain_last_week->add_date,'Y-m-d'));
        		
        		if ($start_add_date == $end_add_date) {
					$dates = array();
					$dates[] = date("Y-m-d", $start_add_date);
					$dates[] = date("Y-m-d", $end_add_date);
					$date_range[] = $dates;
				} else {
        			$s_date = '';

	        		for ($i=$start_add_date; $i<=$end_add_date; $i+=86400) {  
					    
					    if ($s_date) {
			                $dates = array();
			                $dates[] = date("Y-m-d", $s_date);
			                $dates[] = date("Y-m-d", strtotime('-1 day', $i));
			                $date_range[] = $dates;
			            }

          				$s_date = $i;
				   	} 
					
					if ($s_date < $end_add_date) {
						$dates = array();
						$dates[] = date("Y-m-d", $s_date);
						$dates[] = date("Y-m-d", $end_add_date);
						$date_range[] = $dates;
					} else if ($s_date == $end_add_date) {
						$dates = array();
						$dates[] = date("Y-m-d", strtotime('-1 day', $s_date));
						$dates[] = date("Y-m-d", $end_add_date);
						$date_range[] = $dates;
					} 
				}
    		}

          	$i = 0;
        	$j = 0;
          
          	$total_severe_pain_last_week = $this->process_model->total_severe_pain_last_week_new($is_exist_patient->id);
          	$total_no_pain_reliever_severe_pain = $this->process_model->total_no_pain_reliever_severe_pain_new($is_exist_patient->id);

			if ($total_severe_pain_last_week && $total_no_pain_reliever_severe_pain) {
				$start_pain_add_date = getGMTDateToLocalDate($total_severe_pain_last_week->add_date,'Y-m-d');
				$end_pain_add_date = getGMTDateToLocalDate($total_severe_pain_last_week->add_date,'Y-m-d');
				$thc_value = $this->calculate_thc($is_exist_patient->id ,$start_pain_add_date, $end_pain_add_date);

				$total = ($total_severe_pain_last_week->total_severe_pain_last_week + $total_no_pain_reliever_severe_pain->total_no_pain_reliever_severe_pain) / 2;

				$visits[$i]['text'] = $i;
				$visits[$i]['total_count'] = $total;

				$series[$i] = 'START';
				$column[$i] = ($total)?$total:null;
				$line[$i] = ($thc_value)?round($thc_value, 2):null;
				$i++;
				$j++;
			}

        	if ($date_range) {

            	foreach ($date_range as $key => $value) {
					$start_date = $value[0];
					$end_date = $value[1];
					$total_severe_pain_last_week_1 = $this->process_model->total_severe_pain_last_week_1($is_exist_patient->id ,$start_date, $end_date);
    					
              		$thc_value = $this->calculate_thc($is_exist_patient->id ,$start_date, $end_date);
					$total = $total_severe_pain_last_week_1->total_severe_pain_last_week;

					$visits[$i]['text'] = $i;
					$visits[$i]['total_count'] = $total;

					$series[$i] = $j;
					$column[$i] = ($total)?(int)$total:null;
					$line[$i] = ($thc_value)?round($thc_value, 2):null;
					$i++;
					$j++;
          		}
          	}
    			
          	$output['series'] = json_encode($series);
			$output['column'] = json_encode($column);
			$output['line'] = json_encode($line);
			$output['header_menu'] = 'process_control';
			$output['header_sub_menu'] = 'schmerz';
          
	        $this->load->view('front/includes/header', $output);
	        $this->load->view('front/verlaufskontrolle/schmerz');
	        $this->load->view('front/includes/footer');
        } else {
        	redirect('patients');
        }
    }

    function calculate_thc($patient_id ,$start_date, $end_date){
      $zuordnung_sps = $this->patients->getZuordnungSps();
    	$total_thc = 0;
        $app_verlaufskontrolle = $this->process_model->get_app_verlaufskontrolle($patient_id ,$start_date, $end_date);

        if ($app_verlaufskontrolle) {

        	foreach ($app_verlaufskontrolle as $k => $v) {
        		$total_thc = $total_thc + $v->morning_total_spraying + $v->lunch_total_spraying + $v->evening_total_spraying + $v->night_total_spraying;
        	}
        }

        $app_weitere_einnahme = $this->process_model->get_app_weitere_einnahme($patient_id ,$start_date, $end_date);
        

        if ($app_weitere_einnahme) {

        	foreach ($app_weitere_einnahme as $k1 => $v1) {

        		$childData = $this->process_model->get_weitere_einnahme_content($v1->id);

        		foreach ($childData as $k2 => $v3) {
        			$total_thc = $total_thc + $v3->total_spray;
        		}
        	}
        }

        if ($total_thc > 0) {
        	$thc_value = $total_thc * $zuordnung_sps->menge_thc_sps_mg;
        } else {
        	$thc_value = 0;
        }
        return $thc_value;
    }
    function test(){
      $patient_visits = $this->process_model->getPatientVisits_1(1);
      pr($patient_visits);
    }
    public function lebensqualitat() 
    {

      if (!$this->session->userdata('user_id')) {
        redirect('doctors/login');
      }

      $insured_number = $this->input->get('in');
      $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

      if ($is_exist_patient) {

        //pr($patient_visits); die;
        $visits = array();
        $series = array();
        $column = array();
        $line = array();
      	$date_range = array();
	  	
	  	$first_date_app_lebensqualitat = $this->process_model->get_first_date_app_lebensqualitat_1($is_exist_patient->id);
    	$last_date_app_lebensqualitat = $this->process_model->get_last_date_app_lebensqualitat_1($is_exist_patient->id);

    	if ($first_date_app_lebensqualitat && $last_date_app_lebensqualitat) {

    		$start_add_date = strtotime(getGMTDateToLocalDate($first_date_app_lebensqualitat->add_date,'Y-m-d'));
    		$end_add_date = strtotime(getGMTDateToLocalDate($last_date_app_lebensqualitat->add_date,'Y-m-d'));
    		
    		if ($start_add_date == $end_add_date) {
				$dates = array();
				$dates[] = date("Y-m-d", $start_add_date);
				$dates[] = date("Y-m-d", $end_add_date);
				$date_range[] = $dates;
			} else {
    			$s_date = '';

        		for ($i=$start_add_date; $i<=$end_add_date; $i+=2419200) {  
				    // echo date("Y-m-d", $i).'<br />';  
				      if ($s_date) {
                $dates = array();
                $dates[] = date("Y-m-d", $s_date);
                $dates[] = date("Y-m-d", strtotime('-1 day', $i));
                $date_range[] = $dates;
              }
  	          $s_date = $i;
				    } 
				
    				if ($s_date < $end_add_date) {
    					$dates = array();
    					$dates[] = date("Y-m-d", $s_date);
    					$dates[] = date("Y-m-d", $end_add_date);
    					$date_range[] = $dates;
    				} else if ($s_date == $end_add_date) {
    					$dates = array();
    					$dates[] = date("Y-m-d", strtotime('-1 day', $s_date));
    					$dates[] = date("Y-m-d", $end_add_date);
    					$date_range[] = $dates;
    				} 
    			}
    	 }
       
		if ($date_range) {
          $i = 0;
          $j = 1;
          foreach ($date_range as $key => $value) {
            $start_date = $value[0];
            $end_date = $value[1];

            $app_lebensqualitat_1 = $this->process_model->get_app_lebensqualitat_1($is_exist_patient->id, $start_date, $end_date);
            
            $question_1_point = 0;

            if ($app_lebensqualitat_1) {

	            foreach ($app_lebensqualitat_1 as $k => $v) {
	            	$question_1_point = $question_1_point + getQuestionPoint_1($v->health_status);
	            }
            }


            $question_2_point = 0;
            $app_lebensqualitat_2 = $this->process_model->get_app_lebensqualitat_2($is_exist_patient->id, $start_date, $end_date);
           
            
            if ($app_lebensqualitat_2) {

	            foreach ($app_lebensqualitat_2 as $k => $v) {
	            	$question_2_point = $question_2_point + getQuestionPoint_2($v->health_activities_moderate);
	            }
            }


            $question_3_point = 0;
            $app_lebensqualitat_3 = $this->process_model->get_app_lebensqualitat_3($is_exist_patient->id, $start_date, $end_date);
           

            if ($app_lebensqualitat_3) {

	            foreach ($app_lebensqualitat_3 as $k => $v) {
	            	$question_3_point = $question_3_point + getQuestionPoint_3($v->health_activities_climbing);
	            }
            }


            $question_4_point = 0;
            $question_5_point = 0;
            $app_lebensqualitat_4 = $this->process_model->get_app_lebensqualitat_4($is_exist_patient->id, $start_date, $end_date);
           

            if ($app_lebensqualitat_4) {

	            foreach ($app_lebensqualitat_4 as $k => $v) {
	            	$question_4_point = $question_4_point + getQuestionPoint_4($v->physical_health_past_4_weeks_less);
	            	$question_5_point = $question_5_point + getQuestionPoint_5($v->physical_health_past_4_weeks_certain);
	            }
            }


            $app_lebensqualitat_5 = $this->process_model->get_app_lebensqualitat_5($is_exist_patient->id, $start_date, $end_date);
           

            $question_6_point = 0;
            $question_7_point = 0;

            if ($app_lebensqualitat_5) {

	            foreach ($app_lebensqualitat_5 as $k => $v) {
	            	$question_6_point = $question_6_point + getQuestionPoint_6($v->mental_health_past_4_weeks_less);
	            	$question_7_point = $question_7_point + getQuestionPoint_7($v->mental_health_past_4_weeks_could_not_be_careful);
	            }
            }


            $question_8_point = 0;
            $app_lebensqualitat_6 = $this->process_model->get_app_lebensqualitat_6($is_exist_patient->id, $start_date, $end_date);
           

            if ($app_lebensqualitat_6) {

	            foreach ($app_lebensqualitat_6 as $k => $v) {
	            	$question_8_point = $question_8_point + getQuestionPoint_8($v->pain_in_daily_activities_past_4_weeks);
	            }
            }


            $question_9_point = 0;
            $app_lebensqualitat_7 = $this->process_model->get_app_lebensqualitat_7($is_exist_patient->id, $start_date, $end_date);
           

            if ($app_lebensqualitat_7) {

	            foreach ($app_lebensqualitat_7 as $k => $v) {
	            	$question_9_point = $question_9_point + getQuestionPoint_9($v->clam_in_past_4_weeks);
	            }
            }


            $question_10_point = 0;
            $app_lebensqualitat_8 = $this->process_model->get_app_lebensqualitat_8($is_exist_patient->id, $start_date, $end_date);
           
           //
            if ($app_lebensqualitat_8) {

	            foreach ($app_lebensqualitat_8 as $k => $v) {
	            	$question_10_point = $question_10_point + getQuestionPoint_10($v->full_energy_in_past_4_weeks);
	            }
            }


            $question_11_point = 0;
            $app_lebensqualitat_9 = $this->process_model->get_app_lebensqualitat_9($is_exist_patient->id, $start_date, $end_date);
           

            if ($app_lebensqualitat_9) {

	            foreach ($app_lebensqualitat_9 as $k => $v) {
	            	$question_11_point = $question_11_point + getQuestionPoint_11($v->discourage_sad_in_past_4_weeks);
	            }
            }


            $question_12_point = 0;
            $app_lebensqualitat_10 = $this->process_model->get_app_lebensqualitat_10($is_exist_patient->id, $start_date, $end_date);
           

            if ($app_lebensqualitat_10) {

	            foreach ($app_lebensqualitat_10 as $k => $v) {
	            	$question_12_point = $question_12_point + getQuestionPoint_12($v->contact_with_others);
	          
	            }
            }
           
            $total = $question_1_point + $question_2_point + $question_3_point + $question_4_point + $question_5_point + $question_6_point + $question_7_point + $question_8_point + $question_9_point + $question_10_point + $question_11_point + $question_12_point;

            $thc_value = $this->calculate_thc($is_exist_patient->id ,$start_date, $end_date);
            $visits[$i]['text'] = $i;
            $visits[$i]['total_count'] = $total;

            $series[$i] = $j;
            $column[$i] = ($total)?$total:null;
            $line[$i] = ($thc_value)?round($thc_value, 2):null;
            $i++;
            $j++;
          }
        }

        $output['series'] = json_encode($series);
        $output['column'] = json_encode($column);
        $output['line'] = json_encode($line);
        $output['header_menu'] = 'process_control';
        $output['header_sub_menu'] = 'lebensqualitat';
        // pr($output); die;
        $this->load->view('front/includes/header', $output);
        $this->load->view('front/verlaufskontrolle/lebensqualitat');
        $this->load->view('front/includes/footer');
      } else {
      	redirect('patients');
      }

        
    }

    public function schlafqualitat() 
    {   
       if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
			$series = array();
			$column = array();
			$line = array();
			$visits = array();
			$date_range = array();
			$first_date_app_schlafqualitat = $this->process_model->get_first_date_app_schlafqualitat_1($is_exist_patient->id);
	    	$last_date_app_schlafqualitat = $this->process_model->get_last_date_app_schlafqualitat_1($is_exist_patient->id);

	    	if ($first_date_app_schlafqualitat && $last_date_app_schlafqualitat) {

	    		$start_add_date = strtotime(getGMTDateToLocalDate($first_date_app_schlafqualitat->add_date,'Y-m-d'));
	    		$end_add_date = strtotime(getGMTDateToLocalDate($last_date_app_schlafqualitat->add_date,'Y-m-d'));
	    		
	    		if ($start_add_date == $end_add_date) {
					$dates = array();
					$dates[] = date("Y-m-d", $start_add_date);
					$dates[] = date("Y-m-d", $end_add_date);
					$date_range[] = $dates;
				} else {
	    			$s_date = '';

	        		for ($i=$start_add_date; $i<=$end_add_date; $i+=604800) {  
					    // echo date("Y-m-d", $i).'<br />';  
					    if ($s_date) {
		                  $dates = array();
		                  $dates[] = date("Y-m-d", $s_date);
		                  $dates[] = date("Y-m-d", strtotime('-1 day', $i));
		                  $date_range[] = $dates;
		                }

		                $s_date = $i;
					} 
					
					if ($s_date < $end_add_date) {
						$dates = array();
						$dates[] = date("Y-m-d", $s_date);
						$dates[] = date("Y-m-d", $end_add_date);
						$date_range[] = $dates;
					} else if ($s_date == $end_add_date) {
						$dates = array();
						$dates[] = date("Y-m-d", strtotime('-1 day', $s_date));
						$dates[] = date("Y-m-d", $end_add_date);
						$date_range[] = $dates;
					} 
				}
	    	}
        	
    		if ($date_range) {
              $i = 0;
              $j = 1;
              foreach ($date_range as $key => $value) {
                $start_date = $value[0];
                $end_date = $value[1];

	            $app_schlafqualitat_1 = $this->process_model->get_app_schlafqualitat_1($is_exist_patient->id, $start_date, $end_date);
	            $question_1_point = 0;

	            if ($app_schlafqualitat_1) {

		            foreach ($app_schlafqualitat_1 as $k => $v) {
		            	$question_1_point = $question_1_point + getQuestionPoint_13($v->time_takes_to_fall_asleep);
		            }
	            }

	            $question_2_point = 0;
	            $app_schlafqualitat_2 = $this->process_model->get_app_schlafqualitat_2($is_exist_patient->id, $start_date, $end_date);
	            
	            if ($app_schlafqualitat_2) {

		            foreach ($app_schlafqualitat_2 as $k => $v) {
		            	$question_2_point = $question_2_point + getQuestionPoint_14($v->avg_hours_sleep_at_night);
		            }
	            }

	            $question_3_point = 0;
	            $app_schlafqualitat_3 = $this->process_model->get_app_schlafqualitat_3($is_exist_patient->id, $start_date, $end_date);

	            if ($app_schlafqualitat_3) {

		            foreach ($app_schlafqualitat_3 as $k => $v) {
		            	$question_3_point = $question_3_point + getQuestionPoint_15($v->cannot_sleep_through);
		            }
	            }

	            $question_4_point = 0;
	            $app_schlafqualitat_4 = $this->process_model->get_app_schlafqualitat_4($is_exist_patient->id, $start_date, $end_date);

	            if ($app_schlafqualitat_4) {

		            foreach ($app_schlafqualitat_4 as $k => $v) {
		            	$question_4_point = $question_4_point + getQuestionPoint_16($v->wake_up_early);
		            }
	            }

	            $app_schlafqualitat_5 = $this->process_model->get_app_schlafqualitat_5($is_exist_patient->id, $start_date, $end_date);

	            $question_5_point = 0;

	            if ($app_schlafqualitat_5) {

		            foreach ($app_schlafqualitat_5 as $k => $v) {
		            	$question_5_point = $question_5_point + getQuestionPoint_17($v->wake_up_with_noise);
		            }
	            }

	            $question_6_point = 0;
	            $app_schlafqualitat_6 = $this->process_model->get_app_schlafqualitat_6($is_exist_patient->id, $start_date, $end_date);

	            if ($app_schlafqualitat_6) {

		            foreach ($app_schlafqualitat_6 as $k => $v) {
		            	$question_6_point = $question_6_point + getQuestionPoint_18($v->no_eye_all_night);
		            }
	            }

	            $question_7_point = 0;
	            $app_schlafqualitat_7 = $this->process_model->get_app_schlafqualitat_7($is_exist_patient->id, $start_date, $end_date);

	            if ($app_schlafqualitat_7) {

		            foreach ($app_schlafqualitat_7 as $k => $v) {
		            	$question_7_point = $question_7_point + getQuestionPoint_19($v->think_much_sleep_after);
		            }
	            }

	            $question_8_point = 0;
	            $app_schlafqualitat_8 = $this->process_model->get_app_schlafqualitat_8($is_exist_patient->id, $start_date, $end_date);

	            if ($app_schlafqualitat_8) {

		            foreach ($app_schlafqualitat_8 as $k => $v) {
		            	$question_8_point = $question_8_point + getQuestionPoint_20($v->afraid_not_able_to_sleep);
		            }
	            }

	            $question_9_point = 0;
	            $app_schlafqualitat_9 = $this->process_model->get_app_schlafqualitat_9($is_exist_patient->id, $start_date, $end_date);

	            if ($app_schlafqualitat_9) {

		            foreach ($app_schlafqualitat_9 as $k => $v) {
		            	$question_9_point = $question_9_point + getQuestionPoint_21($v->full_powerful);
		            }
	            }

	            $question_10_point = 0;
	            $app_schlafqualitat_10 = $this->process_model->get_app_schlafqualitat_10($is_exist_patient->id, $start_date, $end_date);

	            if ($app_schlafqualitat_10) {

		            foreach ($app_schlafqualitat_10 as $k => $v) {
		            	$question_10_point = $question_10_point + getQuestionPoint_22($v->taking_sleeping_pills);
		            }
	            }
	           
	            $total = $question_1_point + $question_2_point + $question_3_point + $question_4_point + $question_5_point + $question_6_point + $question_7_point + $question_8_point + $question_9_point + $question_10_point;
	            
	            $thc_value = $this->calculate_thc($is_exist_patient->id ,$start_date, $end_date);
                $visits[$i]['text'] = $i;
                $visits[$i]['total_count'] = $total;

                $series[$i] = $j;
                $column[$i] = ($total)?$total:null;
                $line[$i] = ($thc_value)?round($thc_value, 2):null;
                $i++;
                $j++;
              }
            }
        	
          $output['series'] = json_encode($series);
          $output['column'] = json_encode($column);
          $output['line'] = json_encode($line);
	        $output['header_menu'] = 'process_control';
	        $output['header_sub_menu'] = 'schlafqualitat';
	        $this->load->view('front/includes/header', $output);
	        $this->load->view('front/verlaufskontrolle/schlafqualitat');
	        $this->load->view('front/includes/footer');
        } else {
        	redirect('patients');
        }
       
    }

    public function laborwerte() 
    {   
      $doctor = $this->session->userdata('user_id');

      $insured_number = $this->input->get('in');
      //pr($insured_number); die;

    
      if ($this->input->post()) {
      
      $this->form_validation->set_rules('datum', 'Datum', 'trim|required', array('required' => 'Datum ist erforderlich'));
      $this->form_validation->set_rules('behandungstag', 'Behandungstag', 'trim|required', array('required' => 'Behandungstag ist erforderlich'));
      $this->form_validation->set_rules('visite', 'Visite', 'trim|required', array('required' => 'Visite ist erforderlich'));
      $this->form_validation->set_rules('arzneimittel', 'Arzneimittel', 'trim|required', array('required' => 'Arzneimittel ist erforderlich'));

      $this->form_validation->set_rules('Laborwerte1', 'Laborwerte (1)', 'trim|required',
      	array('required' => 'Das Feld Laborwerte (1) ist erforderlich'));
      $this->form_validation->set_rules('wert1', 'Wert (1)', 'trim|required',
      	array('required' => 'Dies ist ein Pflichtfeld'));
      $this->form_validation->set_rules('einheit1', 'Einheit (1)', 'trim|required', array('required' => 'Das Feld Einheit (1) ist obligatorisch'));
      $this->form_validation->set_rules('normbereich1', 'Normbereich (1)', 'trim|required',
      	array('required' => 'Bitte tragen Sie den Wert für den Normbereich ein'));

      $this->form_validation->set_rules('Laborwerte2', 'Laborwerte (2)', 'trim|required',
      	array('required' => 'Das Feld Laborwerte (2) ist erforderlich'));
      $this->form_validation->set_rules('wert2', 'Wert (2)', 'trim|required',
      	array('required' => 'Dies ist ein Pflichtfeld'));
      $this->form_validation->set_rules('einheit2', 'Einheit (2)', 'trim|required', array('required' => 'Das Feld Einheit (2) ist obligatorisch'));
      $this->form_validation->set_rules('normbereich2', 'Normbereich (2)', 'trim|required',
      	array('required' => 'Bitte tragen Sie den Wert für den Normbereich ein'));

      $this->form_validation->set_rules('Laborwerte3', 'Laborwerte (3)', 'trim|required',
      	array('required' => 'Das Feld Laborwerte (3) ist erforderlich'));
      $this->form_validation->set_rules('wert3', 'Wert (3)', 'trim|required',
      	array('required' => 'Dies ist ein Pflichtfeld'));
      $this->form_validation->set_rules('einheit3', 'Einheit (3)', 'trim|required', array('required' => 'Das Feld Einheit (3) ist obligatorisch'));
      $this->form_validation->set_rules('normbereich3', 'Normbereich (3)', 'trim|required',
      	array('required' => 'Bitte tragen Sie den Wert für den Normbereich ein'));

      $this->form_validation->set_rules('Laborwerte4', 'Laborwerte (4)', 'trim|required',
      	array('required' => 'Das Feld Laborwerte (4) ist erforderlich'));
      $this->form_validation->set_rules('wert4', 'Wert (4)', 'trim|required',
      	array('required' => 'Dies ist ein Pflichtfeld'));
      $this->form_validation->set_rules('einheit4', 'Einheit (4)', 'trim|required', array('required' => 'Das Feld Einheit (4) ist obligatorisch'));
      $this->form_validation->set_rules('normbereich4', 'Normbereich (4)', 'trim|required',
      	array('required' => 'Bitte tragen Sie den Wert für den Normbereich ein'));

      if ($this->form_validation->run()) {
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
          $visit = $this->input->post('visite',true);
          $is_record_exist = $this->process_model->getLaborwerteByVisitNumber($visit, $is_exist_patient->id);

          $id = $is_exist_patient->id;
          $input['doctor_id'] = $doctor;
          $input['patient_id'] = $is_exist_patient->id;
          $input['behandungstag'] = $this->input->post('behandungstag',true);
          $input['visite'] = $this->input->post('visite',true);
          $input['arzneimittel'] = $this->input->post('arzneimittel',true);
          $input['datum'] = date('Y-m-d', strtotime($this->input->post('datum')));
          

          if ($is_record_exist) {
            $input['add_date'] = getDefaultToGMTDate(time());
            $this->process_model->updatelaborwerte($input, $is_record_exist->id);
            
            $childData = array();
            $childData['laborwerte_id'] = $is_record_exist->id;
            $childData['laborparameter1'] = $this->input->post('Laborwerte1',true);
            $childData['laborparameter2'] = $this->input->post('Laborwerte2',true);
            $childData['laborparameter3'] = $this->input->post('Laborwerte3',true);
            $childData['laborparameter4'] = $this->input->post('Laborwerte4',true);
            $wert1 = str_replace(',', '.', $this->input->post('wert1',true));
      			$wert2 = str_replace(',', '.', $this->input->post('wert2',true));
      			$wert3 = str_replace(',', '.', $this->input->post('wert3',true));
      			$wert4 = str_replace(',', '.', $this->input->post('wert4',true));
          	$childData['wert1'] = $wert1;
      			$childData['wert2'] = $wert2;
      			$childData['wert3'] = $wert3;
      			$childData['wert4'] = $wert4;
            $childData['einheit1'] = $this->input->post('einheit1',true);
            $childData['einheit2'] = $this->input->post('einheit2',true);
            $childData['einheit3'] = $this->input->post('einheit3',true);
            $childData['einheit4'] = $this->input->post('einheit4',true);
            $childData['normbereich1'] = $this->input->post('normbereich1',true);
            $childData['normbereich2'] = $this->input->post('normbereich2',true);
            $childData['normbereich3'] = $this->input->post('normbereich3',true);
            $childData['normbereich4'] = $this->input->post('normbereich4',true);
            $this->process_model->updatelaborparameter($childData, $is_record_exist->id);

            $success = true;
            $message = 'laborwerte erfolgreich hinzugefügt';
            $data['redirectURL'] = base_url('process_control/laborwerte_auswertung?in='.$insured_number);
          } else {
            $input['add_date'] = getDefaultToGMTDate(time());
            $insert_id = $this->process_model->addlaborwerte($input);
            
            if ($insert_id) {
				$childData = array();
				$childData['laborwerte_id'] = $insert_id;
				$childData['laborparameter1'] = $this->input->post('Laborwerte1',true);
				$childData['laborparameter2'] = $this->input->post('Laborwerte2',true);
				$childData['laborparameter3'] = $this->input->post('Laborwerte3',true);
				$childData['laborparameter4'] = $this->input->post('Laborwerte4',true);
				$wert1 = str_replace(',', '.', $this->input->post('wert1',true));
				$wert2 = str_replace(',', '.', $this->input->post('wert2',true));
				$wert3 = str_replace(',', '.', $this->input->post('wert3',true));
				$wert4 = str_replace(',', '.', $this->input->post('wert4',true));
              	$childData['wert1'] = $wert1;
				$childData['wert2'] = $wert2;
				$childData['wert3'] = $wert3;
				$childData['wert4'] = $wert4;
				$childData['einheit1'] = $this->input->post('einheit1',true);
				$childData['einheit2'] = $this->input->post('einheit2',true);
				$childData['einheit3'] = $this->input->post('einheit3',true);
				$childData['einheit4'] = $this->input->post('einheit4',true);
				$childData['normbereich1'] = $this->input->post('normbereich1',true);
				$childData['normbereich2'] = $this->input->post('normbereich2',true);
				$childData['normbereich3'] = $this->input->post('normbereich3',true);
				$childData['normbereich4'] = $this->input->post('normbereich4',true);


				$response = $this->process_model->addlaborparameter($childData);
              
				if ($response) {
					$success = true;
					$message = 'laborwerte erfolgreich hinzugefügt';
					$data['redirectURL'] = base_url('process_control/laborwerte_auswertung?in='.$insured_number);
				} else {
					$success = false;
					$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
				}
            } else {
                $success = false;
                $message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
            }
          }
          
        } else {

          $message = 'Patient nicht gefunden';
          $success = false;
        }
      } else {

        $success = false;
        $message = validation_errors();
      }

    }
    $data['success'] = $success;
        $data['message'] = $message;
        json_output($data);
    }

      function index() {
        
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }

        $output = array();
        $output['header_menu'] = 'process_control';
        $output['header_sub_menu'] = 'laborwerte';
        $insured_number = $this->input->get('in');
        
        if ($this->input->get('qid')) {
          $qid = $this->input->get('qid');
        } else {
          $qid = '';
        }

        $output['qid'] = $qid;
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
        
        if ($is_exist_patient) {
          $id = $is_exist_patient->id;  
          $output['insurance_number'] = $insured_number;

          /*if ($qid) {
            $data_exists = $this->process_model->getProcessDataByQuestionId($id, $qid);

            if ($data_exists) {
              
            } else {
              $data_exists = $this->process_model->getprocessDataById($id);
            }

          } else {
          
          }*/ 

          	$data_exists = $this->process_model->getprocessDataById($id);
          	//pr($data_exists); die;
          	if ($data_exists) {
	            $output['select_date'] = $data_exists->datum;
	            $output['add_date'] = date('d.m.yy', strtotime($data_exists->add_date));
	            $output['behandungstag'] = $data_exists->behandungstag;
	            $output['visite'] = $data_exists->visite;
	            $output['arzneimittel'] = $data_exists->arzneimittel;
	            $wert1_record = $data_exists->wert1;
      				$wert2_record = $data_exists->wert2;
      				$wert3_record = $data_exists->wert3;
      				$wert4_record = $data_exists->wert4;
	            $output['wert1'] = str_replace('.', ',', $wert1_record);
	            $output['wert2'] = str_replace('.', ',', $wert2_record);
	            $output['wert3'] = str_replace('.', ',', $wert3_record);
	            $output['wert4'] = str_replace('.', ',', $wert4_record);
	            $output['normbereich1'] = $data_exists->normbereich1;
	            $output['normbereich2'] = $data_exists->normbereich2;
	            $output['normbereich3'] = $data_exists->normbereich3;
	            $output['normbereich4'] = $data_exists->normbereich4;
	            $output['einheit1'] = $data_exists->einheit1;
	            $output['einheit2'] = $data_exists->einheit2;
	            $output['einheit3'] = $data_exists->einheit3;
	            $output['einheit4'] = $data_exists->einheit4;
	            $output['laborparameter1'] = $data_exists->laborparameter1;
	            $output['laborparameter2'] = $data_exists->laborparameter2;
	            $output['laborparameter3'] = $data_exists->laborparameter3;
	            $output['laborparameter4'] = $data_exists->laborparameter4;
	            $laborwerte_id = $data_exists->id;
              // pr($output); die;
          	} else {
	            $output['select_date'] = '';
	            $output['add_date'] = '';
	            $output['behandungstag'] = '';
	            $output['visite'] = '';
	            $output['arzneimittel'] = '';
	            $output['laborparameter1'] = '';
	            $output['laborparameter2'] = '';
	            $output['laborparameter3'] = '';
	            $output['laborparameter4'] = '';
	            $output['wert1'] = '';
	            $output['wert2'] = '';
	            $output['wert3'] = '';
	            $output['wert4'] = '';
	            $output['einheit1'] = '';
	            $output['einheit2'] = '';
	            $output['einheit3'] = '';
	            $output['einheit4'] = '';
	            $output['normbereich1'] = '';
	            $output['normbereich2'] = '';
	            $output['normbereich3'] = '';
	            $output['normbereich4'] = '';
          	}

    		$visits = $this->process_model->getVisitsByPatientId($is_exist_patient->id);
        // pr($visits); die;
    		$output['visits'] = $visits;
        } else {
          redirect('patients');
        }


   // pr($output); die;
   // $output['records'] = $records;

    $this->load->view('front/includes/header', $output);
   	$this->load->view('front/verlaufskontrolle/laborwerte');
    $this->load->view('front/includes/footer');
  }


/*    function add_nebenwirkung_data() {
    $doctor = $this->session->userdata('user_id');

    $insured_number = $this->input->get('in');
    //pr($insured_number); die;

    
    if ($this->input->post()) {
      
      $this->form_validation->set_rules('datum', 'Datum', 'trim|required');
      $this->form_validation->set_rules('behandungstag', 'Behandungstag', 'trim|required');
      $this->form_validation->set_rules('visite', 'Visite', 'trim|required');
      $this->form_validation->set_rules('arzneimittel', 'Arzneimittel', 'trim|required');

      if ($this->form_validation->run()) {
        
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
              $id = $is_exist_patient->id;
              
              $input['doctor_id'] = $doctor;
              $input['patient_id'] = $is_exist_patient->id;
              $input['behandungstag'] = $this->input->post('behandungstag',true);
              $input['visite'] = $this->input->post('visite',true);
              $input['arzneimittel'] = $this->input->post('arzneimittel',true);
              $input['add_date'] = date('Y-m-d', strtotime(getDefaultToGMTDate(time())));
            pr($input); die;
            $insert_id = $this->process_model->addlaborwerte($input);
                        
          if ($insert_id) {
          
            //pr($_POST['data']); die;
             $data = array();

              $data['laborwerte_id'] = $insert_id;
              $data['laborparameter1'] = $this->input->post('Laborwerte1',true);
              $data['laborparameter2'] = $this->input->post('Laborwerte2',true);
              $data['laborparameter3'] = $this->input->post('Laborwerte3',true);
              $data['laborparameter4'] = $this->input->post('Laborwerte4',true);
              $data['wert1'] = $this->input->post('wert1',true);
              $data['wert2'] = $this->input->post('wert2',true);
              $data['wert3'] = $this->input->post('wert3',true);
              $data['wert4'] = $this->input->post('wert4',true);
              $data['einheit1'] = $this->input->post('einheit1',true);
              $data['einheit2'] = $this->input->post('einheit2',true);
              $data['einheit3'] = $this->input->post('einheit3',true);
              $data['einheit4'] = $this->input->post('einheit4',true);
              $data['normbereich1'] = $this->input->post('normbereich1',true);
              $data['normbereich2'] = $this->input->post('normbereich2',true);
              $data['normbereich3'] = $this->input->post('normbereich3',true);
              $data['normbereich4'] = $this->input->post('normbereich4',true);

              //pr($data);die;
              $response = $this->process_model->addlaborparameter($data);
              if ($response) {
              
              $success = true;
              $message = 'Nebenwirkungsdaten erfolgreich hinzugefügt';
            } else {
              $success = false;
              $message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
            }
          } else {
              $success = false;
              $message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
          }
          
            } else {

              $message = 'Patient nicht gefunden';
              $success = false;
            }
      } else {

        $success = false;
        $message = validation_errors();
      }

    }
    $data['success'] = $success;
        $data['message'] = $message;
        json_output($data);
  }*/

    public function begleitmedikation() 
    {  
        $output = array();
        $output['header_menu'] = 'process_control';
        $output['header_sub_menu'] = 'begleitmedikation';
        $insured_number = $this->input->get('in'); 
        //pr($insured_number);
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
        
        if ($is_exist_patient) {
            $patient_id = $is_exist_patient->id;
            $doctor_id = $this->session->userdata('user_id');
            //$patient_record = $this->process_model->getPatientDataRecords($patient_id, $doctor_id);
            //pr($patient_record); die;
            $patients_medication = $this->process_model->getPatientsMedicationsDetails($is_exist_patient->id);
            
            $output['patients_medication'] = $patients_medication;
            /*if ($patient_record) {
                                    
              $output['visite'] = $patient_record->visite;
               //  $patients_medication = $this->process_model->getPatientsMedications($patient_record->id);
                //pr($patients_medication); die;

                if ($patients_medication) {
                    $output['medication'] = $patient_record;
                    
                }
		        
		        $visits = $this->process_model->getPatientQuestions($is_exist_patient->id);
		    	$output['visits'] = $visits;
            }*/
        } else {
            redirect('patients');
        }


        $output['id'] = $patient_id;
        $output['insurance_number'] = $insured_number;
        $output['digit'] = '';          

        //pr($output); die;
        
        $this->load->view('front/includes/header', $output);
        $this->load->view('front/verlaufskontrolle/begleitmedikation');
        $this->load->view('front/includes/footer');
    }

    function add_new_begleithmedikation_row() {
    	//die('here');
    	 $digit = $this->input->post('digit');

       //pr($digit); die;		
    	$html = '<tr class="table-row-length filled-row">
                     <th>'. $digit.'</th>
                     <td><input type="text" name="data['.$digit.'][wriksoff]" class="form-control"/></td>
                     <td><input type="text" name="data['.$digit.'][handelsname]" class="form-control"/></td>
                     <td><input type="text" name="data['.$digit.'][starke]" class="form-control"/></td>
                     <td>
                        <div class="dropdown select-box select_eiheit_dropdown_box">
		                     <button class="dropdown-select eiheit_values_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                     
		                       <span><i class="fas fa-chevron-down"></i></span>
		                     </button>
		                     <ul class="dropdown-menu select-form_1" aria-labelledby="eiheit_values_btn">
                         <li value="Sprühstoß" onclick="select_eiheit_dropdown_box(this)">Sprühstoß</li>
		                      <li value="mg" onclick="select_eiheit_dropdown_box(this)">mg</li>
		                      <li value="mL" onclick="select_eiheit_dropdown_box(this)">mL</li>
                          <li value="µg" onclick="select_eiheit_dropdown_box(this)">µg</li>
                        <li value="IE" onclick="select_eiheit_dropdown_box(this)">IE</li>
                        <li value="Tropfen" onclick="select_eiheit_dropdown_box(this)">Tropfen</li>
                        <li value="Hub" onclick="select_eiheit_dropdown_box(this)">Hub</li>
                        <li value="Tablette" onclick="select_eiheit_dropdown_box(this)">Tablette</li>
		                     </ul>
		                  </div>
		                  <input type="hidden" name="data['.$digit.'][eiheit]" class="eiheit_values-field" value="">
                     </td>
                     <td>
                        <div class="dropdown select-box select_form_dropdown_box">
	                     <button class="dropdown-select form_values_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                     
	                       <span><i class="fas fa-chevron-down"></i></span>
	                     </button>
		                     <ul class="dropdown-menu" id="select-form_1" aria-labelledby="form_values_btn">
		                      <li value="Tablette" onclick="select_form_dropdown_box(this)">Tablette</li>
		                      <li value="Lösung" onclick="select_form_dropdown_box(this)">Lösung</li>
		                      <li value="Tropfen" onclick="select_form_dropdown_box(this)">Tropfen</li>
		                      <li value="Pflaster" onclick="select_form_dropdown_box(this)">Pflaster</li>
		                      <li value="Zäpfchen" onclick="select_form_dropdown_box(this)">Zäpfchen</li>
                          <li value="Aerosol" onclick="select_form_dropdown_box(this)">Aerosol</li>
		                     </ul>
		                  </div>
		                  <input type="hidden" name="data['.$digit.'][form]" class="form_values-field" value="">
                     </td>
                     <td><input type="text" name="data['.$digit.'][morgens]" class="form-control morgens_field" onkeyup="total_tagodosis(this)"/></td>
                     <td><input type="text" name="data['.$digit.'][mittags]" class="form-control mittags_field" onkeyup="total_tagodosis(this)"/></td>
                     <td><input type="text" name="data['.$digit.'][abends]" class="form-control abends_field" onkeyup="total_tagodosis(this)"/></td>
                     <td><input type="text" name="data['.$digit.'][zur_nacht]" class="form-control zur_nacht_field" onkeyup="total_tagodosis(this)"/></td>
                     <td><input type="text" name="data['.$digit.'][einheit]" class="form-control einheit"/></td>
                     <td><input type="text" name="data['.$digit.'][tagesdosis]" class="form-control total_tagodosis" readonly/></td>
                     <td>
                        
                        <div class="dropdown select-box select_grund_dropdown_box">
	                     <button class="dropdown-select grund_values_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                     
	                       <span><i class="fas fa-chevron-down"></i></span>
	                     </button>
	                     <ul class="dropdown-menu select-form_1" aria-labelledby="grund_values_btn">
	                      <li value="Schmerz" onclick="select_grund_dropdown_box(this)">Schmerz</li>
	                      <li value="Angststörung" onclick="select_grund_dropdown_box(this)">Angststörung</li>
	                      <li value="Schlafstörung" onclick="select_grund_dropdown_box(this)">Schlafstörung</li>
	                      <li value="Depression" onclick="select_grund_dropdown_box(this)">Depression</li>
	                      <li value="Hypertonie" onclick="select_grund_dropdown_box(this)">Hypertonie</li>
	                     </ul>
	                  </div>
	                  <input type="hidden" name="data['.$digit.'][grund]" class="grund_values-field" value="">
                     </td>
                     <td><input type="text" name="data['.$digit.'][startdatum]" class="form-control startdate" autocomplete="off"/></td>
                     <td><input type="text" name="data['.$digit.'][stopdatum]" class="form-control stopdate"  autocomplete="off"/></td>
                   </tr>';
        $data['html'] = $html;
        json_output($data);

    }

    function addNewBegleithmedikationData() {

      if ($this->input->post()) {
        $insured_number = $this->input->get('in');
        $patientData = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($patientData) {
          $saveData = array();
          
          if ($_POST['data']) {
            $i = 0;
            foreach ($_POST['data'] as $key => $value) {
              $saveData[$i]['patient_id'] = $patientData->id;
              $saveData[$i]['wriksoff'] = $_POST['data'][$key]['wriksoff'];                    
              $saveData[$i]['handelsname'] = $_POST['data'][$key]['handelsname'];
              $saveData[$i]['starke'] = $_POST['data'][$key]['starke'];
              $saveData[$i]['eiheit'] = $_POST['data'][$key]['eiheit'];
              $saveData[$i]['form'] = $_POST['data'][$key]['form'];
              $saveData[$i]['morgens'] = str_replace(',', '.', $_POST['data'][$key]['morgens']);
              $saveData[$i]['mittags'] = str_replace(',', '.', $_POST['data'][$key]['mittags']);
              $saveData[$i]['abends'] = str_replace(',', '.', $_POST['data'][$key]['abends']);
              $saveData[$i]['zur_nacht'] = str_replace(',', '.', $_POST['data'][$key]['zur_nacht']);
              $saveData[$i]['einheit'] = $_POST['data'][$key]['einheit'];
              $saveData[$i]['tagesdosis'] = $_POST['data'][$key]['tagesdosis'];
              $saveData[$i]['grund'] = $_POST['data'][$key]['grund'];
              $saveData[$i]['startdatum'] = ($_POST['data'][$key]['startdatum'])?date('Y-m-d', strtotime($_POST['data'][$key]['startdatum'])):date('Y-m-d');

              if ($_POST['data'][$key]['stopdatum']) {
              	$saveData[$i]['stopdatum'] = date('Y-m-d', strtotime($_POST['data'][$key]['stopdatum']));
              } else {

              	$saveData[$i]['stopdatum'] = null;
              }
              $i++;
            }
            
            $this->process_model->deletepreviousbegleithmedikationdatabypatientid($patientData->id);
            $insert_id = $this->process_model->addbegleithmedikationdata($saveData);

            if ($insert_id) {
                
                $message = 'Begleithmedikation daten hinzugefügt';
                $success = true;
                $data['redirectURL'] = base_url('process_control/begleitmedikation_verlauf?in='.$insured_number);
            } else {
                $message = 'Etwas ist schief gelaufen! Bitte versuche es erneut...';
                $success = false;
            }
          } else {
            $message = 'Etwas ist schief gelaufen! Bitte versuche es erneut...';
            $success = false;
          }
        } else {
          $message = 'Patientendaten nicht gefunden';
          $success = false;
        }
        
        $data['success'] = $success;
        $data['message'] = $message;
        json_output($data);
      } else {
        $data['success'] = false;
        $data['message'] = 'Bitte fügen Sie mindestens eine Zeile hinzu';
        json_output($data);
      }
    }

    // function addNewBegleithmedikationData() {

    // 	if ($this->input->post()) {
                
    //         $insured_number = $this->input->get('in');
            
    //         $patientData = $this->patients->getPatientByInsuredNumber($insured_number);

    //         if ($patientData) {
	   //            $input['data'] = array();

    //             //$begleithmedikation_data_exits = $this->process_model->getPatientDataRecords($patientData->id);

    //             /*if ($begleithmedikation_data_exits) {
                
    //             //$this->process_model->deletepreviousbegleithmedikationdatabypatientid($patientData->id);
    //             }*/

    //             $addData['doctor_id'] = $this->session->userdata('user_id');
    //             $addData['patient_id'] = $patientData->id;
    //            // $addData['visite_date'] = $this->input->post('visite_date');
    //            // $addData['visite'] = $this->input->post('visite');
    //             $addData['add_date'] = getDefaultToGMTDate(time());
    //             //pr($addData); die;

    //             $inserted = $this->process_model->addbegleithmedikationparentData($addData);

    //             if ($inserted) {
                
    //             	//pr($_POST); die;
    //                 foreach ($_POST['data'] as $key => $value) {

    //                     //pr($value); die;
                        
    //   	                $input['data'][$key]['begleitmedikation_id'] = $inserted;
    //   	                $input['data'][$key]['wriksoff'] = $_POST['data'][$key]['wriksoff'];                    
    //   	                $input['data'][$key]['handelsname'] = $_POST['data'][$key]['handelsname'];
    //   	                $input['data'][$key]['starke'] = $_POST['data'][$key]['starke'];
    //                     //pr($input['data']); die;
    //   	                $input['data'][$key]['eiheit'] = $_POST['data'][$key]['eiheit'];
    //   	                $input['data'][$key]['form'] = $_POST['data'][$key]['form'];
    //   	                $input['data'][$key]['morgens'] = $_POST['data'][$key]['morgens'];
    //   	                $input['data'][$key]['mittags'] = $_POST['data'][$key]['mittags'];
    //   	                $input['data'][$key]['abends'] = $_POST['data'][$key]['abends'];
    //   	                $input['data'][$key]['zur_nacht'] = $_POST['data'][$key]['zur_nacht'];
    //   	                $input['data'][$key]['einheit'] = $_POST['data'][$key]['einheit'];
    //   	                $input['data'][$key]['tagesdosis'] = $_POST['data'][$key]['tagesdosis'];
    //                     $input['data'][$key]['grund'] = $_POST['data'][$key]['grund'];
    //                     $input['data'][$key]['startdatum'] = date('Y-m-d', strtotime($_POST['data'][$key]['startdatum']));
    //                     $input['data'][$key]['stopdatum'] = date('Y-m-d', strtotime($_POST['data'][$key]['stopdatum']));

    //                 }
    //                 // pr($input['data']); die;
    //                 $insert_id = $this->process_model->addbegleithmedikationdata($input['data']);

    //                 if ($insert_id) {
                        
    //                     $message = 'Begleithmedikation daten hinzugefügt';
    //                     $success = true;
    //                     // $data['redirectURL'] = base_url('process_control/begleitmedikation_verlauf');
    //                 } else {
    //                     $message = 'Etwas ist schief gelaufen! Bitte versuche es erneut...';
    //                     $success = false;
    //                 }
    //             } else {
    //                 $message = 'Etwas ist schief gelaufen! Bitte versuche es erneut...';
    //                 $success = false;
    //                 }
    //         } else {
    //         	$message = 'Patientendaten nicht gefunden';
    //         	$success = false;
    //         }
            
    //         $data['success'] = $success;
    //         $data['message'] = $message;
    //         json_output($data);
    //     } else {
    //       $data['success'] = false;
    //         $data['message'] = 'Bitte fügen Sie mindestens eine Zeile hinzu';
    //         json_output($data);
    //     }
    // }

    public function nebenwirkungen_auswertung() 
    {   
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }

       // $output['header_menu'] = '';
        $this->load->view('front/includes/header');
        $this->load->view('front/verlaufskontrolle/nebenwirkung-auswertung');
        $this->load->view('front/includes/footer');
    }
    public function begleitmedikation_verlauf() 
    {   
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }
        //$output['header_menu'] = '';
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
            $visits = array();
            $series = array();
            $column = array();
            $line = array();
            $date_range = array();

            $first_date_severe_pain_last_week = $this->process_model->get_first_date_severe_pain_last_week_1($is_exist_patient->id);
            $last_date_severe_pain_last_week = $this->process_model->get_last_date_severe_pain_last_week_1($is_exist_patient->id);

          if ($first_date_severe_pain_last_week && $last_date_severe_pain_last_week) {

            $start_add_date = strtotime(getGMTDateToLocalDate($first_date_severe_pain_last_week->add_date,'Y-m-d'));
            $end_add_date = strtotime(getGMTDateToLocalDate($last_date_severe_pain_last_week->add_date,'Y-m-d'));
            
            if ($start_add_date == $end_add_date) {
              $dates = array();
              $dates[] = date("Y-m-d", $start_add_date);
              $dates[] = date("Y-m-d", $end_add_date);
              $date_range[] = $dates;
            } else {
              $s_date = '';

              for ($i=$start_add_date; $i<=$end_add_date; $i+=86400) {  
                // echo date("Y-m-d", $i).'<br />';  
                if ($s_date) {
                  $dates = array();
                  $dates[] = date("Y-m-d", $s_date);
                  $dates[] = date("Y-m-d", strtotime('-1 day', $i));
                  $date_range[] = $dates;
                }

                $s_date = $i;
              } 
          
              if ($s_date < $end_add_date) {
                $dates = array();
                $dates[] = date("Y-m-d", $s_date);
                $dates[] = date("Y-m-d", $end_add_date);
                $date_range[] = $dates;
              } else if ($s_date == $end_add_date) {
                $dates = array();
                $dates[] = date("Y-m-d", strtotime('-1 day', $s_date));
                $dates[] = date("Y-m-d", $end_add_date);
                $date_range[] = $dates;
              } 
            }
          }

          $i = 0;
          $j = 0;
          
          $total_severe_pain_last_week = $this->process_model->total_severe_pain_last_week_new($is_exist_patient->id);
          $total_no_pain_reliever_severe_pain = $this->process_model->total_no_pain_reliever_severe_pain_new($is_exist_patient->id);

          if ($total_severe_pain_last_week && $total_no_pain_reliever_severe_pain) {

            $start_pain_add_date = getGMTDateToLocalDate($total_severe_pain_last_week->add_date,'Y-m-d');
            $end_pain_add_date = getGMTDateToLocalDate($total_severe_pain_last_week->add_date,'Y-m-d');
            $thc_value = $this->calculate_thc($is_exist_patient->id ,$start_pain_add_date, $end_pain_add_date);
           
            $total = ($total_severe_pain_last_week->total_severe_pain_last_week + $total_no_pain_reliever_severe_pain->total_no_pain_reliever_severe_pain) / 2;

            $visits[$i]['text'] = $i;
            $visits[$i]['total_count'] = $total;

            $series[$i] = 'START';
            $column[$i] = ($total)?(int)$total:null;
            $line[$i] = ($thc_value)?round($thc_value, 2):null;
            $i++;
            $j++;
          }
          


          if ($date_range) {
            
             
            foreach ($date_range as $key => $value) {
              $start_date = $value[0];
              $end_date = $value[1];
              $total_severe_pain_last_week_1 = $this->process_model->total_severe_pain_last_week_1($is_exist_patient->id ,$start_date, $end_date);
              
              $thc_value = $this->calculate_thc($is_exist_patient->id ,$start_date, $end_date);
              $total = $total_severe_pain_last_week_1->total_severe_pain_last_week;

              $visits[$i]['text'] = $i;
              $visits[$i]['total_count'] = $total;

              $series[$i] = $j;
              $column[$i] = ($total)?(int)$total:null;
              $line[$i] = ($thc_value)?round($thc_value, 2):null;
              $i++;
              $j++;
            }
          }
          
         
          $date_range_medicine = array();

          $first_date_weitere_schmerzmedikamente = $this->process_model->get_first_date_weitere_schmerzmedikamente($is_exist_patient->id);
          $last_date_weitere_schmerzmedikamente = $this->process_model->get_last_date_weitere_schmerzmedikamente($is_exist_patient->id);



          if ($first_date_weitere_schmerzmedikamente && $last_date_weitere_schmerzmedikamente) {

            $start_add_date = strtotime(getGMTDateToLocalDate($first_date_weitere_schmerzmedikamente->add_date,'Y-m-d'));
            

            if ($last_date_severe_pain_last_week) {
              $end_add_date_1 = strtotime(getGMTDateToLocalDate($last_date_severe_pain_last_week->add_date,'Y-m-d')); 

              $end_add_date_2 = strtotime(getGMTDateToLocalDate($last_date_weitere_schmerzmedikamente->add_date,'Y-m-d'));

              if ($end_add_date_2 >= $end_add_date_1) {
                $end_add_date = $end_add_date_2;
              } else {
                $end_add_date = $end_add_date_1;
              }

            } else {
              $end_add_date = strtotime(getGMTDateToLocalDate($last_date_weitere_schmerzmedikamente->add_date,'Y-m-d'));
            }
            if ($start_add_date == $end_add_date) {
              $dates = array();
              $dates[] = date("Y-m-d", $start_add_date);
              $dates[] = date("Y-m-d", $end_add_date);
              $date_range_medicine[] = $dates;
            } else {
              $s_date = '';

              for ($i=$start_add_date; $i<=$end_add_date; $i+=86400) {  
                // echo date("Y-m-d", $i).'<br />';  
                if ($s_date) {
                  $dates = array();
                  $dates[] = date("Y-m-d", $s_date);
                  $dates[] = date("Y-m-d", strtotime('-1 day', $i));
                  $date_range_medicine[] = $dates;
                }
                $s_date = $i;
              } 
          
              if ($s_date < $end_add_date) {
                $dates = array();
                $dates[] = date("Y-m-d", $s_date);
                $dates[] = date("Y-m-d", $end_add_date);
                $date_range_medicine[] = $dates;
              } else if ($s_date == $end_add_date) {
                $dates = array();
                $dates[] = date("Y-m-d", strtotime('-1 day', $s_date));
                $dates[] = date("Y-m-d", $end_add_date);
                $date_range_medicine[] = $dates;
              } 
            }
          }

          $patients_medication = $this->process_model->getPatientsMedicationsDetails($is_exist_patient->id, true);

          $patient_medicines_graph = array();
            
          if ($patients_medication) {
            $kk = 0;
                
            foreach ($patients_medication as $key => $value) {
              $medicine_series = array();
              $medicine_column = array();
              $medicine_line = array();

              $i = 0;
              $j = 0;
              $total_severe_pain_last_week = $this->process_model->total_severe_pain_last_week_new($is_exist_patient->id);
              $total_no_pain_reliever_severe_pain = $this->process_model->total_no_pain_reliever_severe_pain_new($is_exist_patient->id);

              if ($total_severe_pain_last_week && $total_no_pain_reliever_severe_pain) {

                $start_pain_add_date = getGMTDateToLocalDate($total_severe_pain_last_week->add_date,'Y-m-d');
                $end_pain_add_date = getGMTDateToLocalDate($total_severe_pain_last_week->add_date,'Y-m-d');
                $medicine_name = $value->handelsname;
                // $thc_value = $this->calculate_thc($is_exist_patient->id ,$start_date, $end_date);
                $weitere_schmerzmedikamente = $this->process_model->get_weitere_schmerzmedikamente($is_exist_patient->id, $start_pain_add_date, $end_pain_add_date, $medicine_name);
                $multiple_starke = ($value->starke == '')?1:$value->starke;
                
                if ($weitere_schmerzmedikamente) {
                  $total_tagodosis = ($weitere_schmerzmedikamente->total_further_morning_pain_drug + $weitere_schmerzmedikamente->total_further_lunch_pain_drug + $weitere_schmerzmedikamente->total_further_evening_pain_drug + $weitere_schmerzmedikamente->total_further_night_pain_drug) * $multiple_starke;
                } else {
                  $total_tagodosis = null;
                }

                $medicine_series[$j] = $j + 1;
                $medicine_column[$j] = ($total)?(int)$total:null;
                $medicine_line[$j] = $total_tagodosis;
                $j++;

                /*$patient_medicines_graph[$kk]['medicine_name'] = $value->handelsname;
                $patient_medicines_graph[$kk]['medicine_series'] = json_encode($medicine_series);
                $patient_medicines_graph[$kk]['medicine_column'] = json_encode($medicine_column);
                // $patient_medicines_graph[$kk]['thc_value'] = json_encode($thc_value);
                $patient_medicines_graph[$kk]['medicine_line'] = json_encode($medicine_line);
                $kk++;*/
              }

              
              if ($date_range_medicine) {

                foreach ($date_range_medicine as $k => $v) {
                  $start_date = $v[0];
                  $end_date = $v[1];

                  // $total_severe_pain_last_week = $this->process_model->total_severe_pain_last_week($is_exist_patient->id ,$start_date, $end_date);
                  // $total_no_pain_reliever_severe_pain = $this->process_model->total_no_pain_reliever_severe_pain($is_exist_patient->id ,$start_date, $end_date);

                  $total_severe_pain_last_week_1 = $this->process_model->total_severe_pain_last_week_1($is_exist_patient->id ,$start_date, $end_date);

                  $total = $total_severe_pain_last_week_1->total_severe_pain_last_week;

                  $medicine_name = $value->handelsname;
                  // $thc_value = $this->calculate_thc($is_exist_patient->id ,$start_date, $end_date);
                  $weitere_schmerzmedikamente = $this->process_model->get_weitere_schmerzmedikamente($is_exist_patient->id, $start_date, $end_date, $medicine_name);
                  $multiple_starke = ($value->starke == '')?1:$value->starke;

                  if ($weitere_schmerzmedikamente) {
                    $total_tagodosis = ($weitere_schmerzmedikamente->total_further_morning_pain_drug + $weitere_schmerzmedikamente->total_further_lunch_pain_drug + $weitere_schmerzmedikamente->total_further_evening_pain_drug + $weitere_schmerzmedikamente->total_further_night_pain_drug) * $multiple_starke;
                  } else {
                    $total_tagodosis = null;
                  }

                  $medicine_series[$j] = $j + 1;

                  $medicine_column[$j] = ($total)?(int)$total:null;
                  $medicine_line[$j] = $total_tagodosis;
                  $j++;
                }
              }

              $patient_medicines_graph[$kk]['medicine_name'] = $value->handelsname;
              $patient_medicines_graph[$kk]['eiheit'] = $value->handelsname.' ['.$value->eiheit.']';
              $patient_medicines_graph[$kk]['medicine_series'] = json_encode($medicine_series);
              $patient_medicines_graph[$kk]['medicine_column'] = json_encode($medicine_column);
              // $patient_medicines_graph[$kk]['thc_value'] = json_encode($thc_value);
              $patient_medicines_graph[$kk]['medicine_line'] = json_encode($medicine_line);
              $kk++;
            }
          }


          $begleitmedikation_count = count($patient_medicines_graph);
          $output['patient_medicines_graph'] = $patient_medicines_graph;
          $output['begleitmedikation_count'] = $begleitmedikation_count;
          $output['series'] = json_encode($series);
          $output['column'] = json_encode($column);
          $output['line'] = json_encode($line);
          $output['header_menu'] = 'process_control';
          $output['header_sub_menu'] = 'begleitmedikation';
          $this->load->view('front/includes/header', $output);
          $this->load->view('front/verlaufskontrolle/begleitmedikation-verlauf');
          $this->load->view('front/includes/footer');
        } else {
          redirect('patients');
        }
    }

    public function laborwerte_auswertung() 
    {   
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
          	$patient_laborwertes = $this->process_model->getPatientLaborwertes($is_exist_patient->id);
			$visits_1 = array();
			$series_1 = array();
			$column_1 = array();
			$line_1 = array();

			$visits_2 = array();
			$series_2 = array();
			$column_2 = array();
			$line_2 = array();

			$visits_3 = array();
			$series_3 = array();
			$column_3 = array();
			$line_3 = array();

			$visits_4 = array();
			$series_4 = array();
			$column_4 = array();
			$line_4 = array();
          	
			if ($patient_laborwertes) {
			  	
			  	$patient_visits = $this->process_model->getPatientVisits($is_exist_patient->id);
        		$date_range = array();
        		
	        	if ($patient_visits) {
		            $s_date = '';
		            $date_range = array();
		            $total_visit = count($patient_visits) - 1;

		            foreach ($patient_visits as $key => $value) {

						if ($total_visit > 0) {

							if ($s_date) {
							  $dates = array();
							  $dates[] = $s_date;
							  $dates[] = date('Y-m-d', strtotime('-1 day', strtotime($value->datum)));
							  $date_range[] = $dates;
							}

							$s_date = $value->datum;

							if ($key == $total_visit) {
							    $dates = array();
							    $dates[] = $s_date;
							    $dates[] = date('Y-m-t');
							    $date_range[] = $dates;
							}

						} else {
							$dates = array();
							$dates[] = $value->datum;
							$dates[] = date('Y-m-d');
							$date_range[] = $dates;
						}
	        		}
	        	}

			  	$i = 0;
			  	$j = 1;

				foreach ($patient_laborwertes as $key => $value) {

					if ($date_range && isset($date_range[$key])) {
						$thc_value = $this->calculate_thc($is_exist_patient->id ,$date_range[$key][0], $date_range[$key][1]);
					} else {
						$thc_value = 0;
					}
					$labor_parameter = $this->process_model->getLaborwerteLaborParameter($value->id);
          
          if ($labor_parameter) {
            
            $visits_1[$i]['text'] = $labor_parameter->laborparameter1;
				    $visits_1[$i]['total_count'] = $labor_parameter->wert1;
				    $series_1[$i] = $j;
				    $column_1[$i] = ($labor_parameter->wert1)?(float)$labor_parameter->wert1:null;
				    $line_1[$i] = ($thc_value)?round($thc_value, 2):null;

				    $visits_2[$i]['text'] = $labor_parameter->laborparameter2;
				    $visits_2[$i]['total_count'] = $labor_parameter->wert2;
				    $series_2[$i] = $j;
				    $column_2[$i] = ($labor_parameter->wert2)?(float)$labor_parameter->wert2:null;
				    $line_2[$i] = ($thc_value)?round($thc_value, 2):null;

				    $visits_3[$i]['text'] = $labor_parameter->laborparameter3;
				    $visits_3[$i]['total_count'] = $labor_parameter->wert3;
				    $series_3[$i] = $j;
				    $column_3[$i] = ($labor_parameter->wert3)?(float)$labor_parameter->wert3:null;
				    $line_3[$i] = ($thc_value)?round($thc_value, 2):null;

				    $visits_4[$i]['text'] = $labor_parameter->laborparameter4;
				    $visits_4[$i]['total_count'] = $labor_parameter->wert4;
				    $series_4[$i] = $j;
				    $column_4[$i] = ($labor_parameter->wert4)?(float)$labor_parameter->wert4:null;
				    $line_4[$i] = ($thc_value)?round($thc_value, 2):null;
				    $i++;
				    $j++;
          }
				}
			}

          $output['series_1'] = json_encode($series_1);
          $output['column_1'] = json_encode($column_1);
          $output['line_1'] = json_encode($line_1);
          $output['einheit_1'] = $labor_parameter->einheit1;

          $output['series_2'] = json_encode($series_2);
          $output['column_2'] = json_encode($column_2);
          $output['line_2'] = json_encode($line_2);
          $output['einheit_2'] = $labor_parameter->einheit2;

          $output['series_3'] = json_encode($series_3);
          $output['column_3'] = json_encode($column_3);
          $output['line_3'] = json_encode($line_3);
          $output['einheit_3'] = $labor_parameter->einheit3;

          $output['series_4'] = json_encode($series_4);
          $output['column_4'] = json_encode($column_4);
          $output['line_4'] = json_encode($line_4);
          $output['einheit_4'] = $labor_parameter->einheit4;
          // pr($output); die;
          $output['header_menu'] = 'process_control';
          $output['header_sub_menu'] = 'laborwerte-auswertung';
          //pr($output); die;
        }
        $this->load->view('front/includes/header', $output);
        $this->load->view('front/verlaufskontrolle/laborwerte-auswertung');
        $this->load->view('front/includes/footer');
    }
}