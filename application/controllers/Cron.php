<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('cron_model', 'cron_model');
	}

	public function send_weekly_notification()
	{
		$current_time = date('H:i:s');
		$patients_data = $this->cron_model->getNotificationReminders();

		if ($patients_data) {
			$current_date = date('Y-m-d H:i:s');
			$current_time = strtotime($current_date);
			
			foreach ($patients_data as $key => $value) {
				$local_add_date = getGMTDateToLocalDate($value->add_date, 'Y-m-d');
				$local_add_time = strtotime($local_add_date);
				$days = ($current_time - $local_add_time)/60/60/24;
				
				if ($days % 7 == 0) {

					$patient_notification_data = $this->cron_model->get_patient_send_notification($value->patient_id, 'Weekly', $current_date);

					if ($patient_notification_data) {

					} else {

						// $notification_time = getGMTDateToLocalDate($value->weekly_reminder, 'Y-m-d H:i:s');
						$timezone = 'GMT';
						date_default_timezone_set($timezone);
						$notification_time = strtotime($value->weekly_reminder);
						
						if (time() >= strtotime($notification_time)) {
							$timezone = 'Europe/Berlin';
							date_default_timezone_set($timezone);
							$notification_send_date = date('Y-m-d');
							$input['patient_id'] = $value->patient_id;
							$input['notification_send_date'] = $notification_send_date;
							$input['notification_type'] = 'Weekly';
							$input['notification_send_datetime'] = date('Y-m-d H:i:s');
							$this->cron_model->add_notification($input);

							$patient_info = $this->cron_model->getPatientById($value->patient_id);
							$deviceToken = $patient_info->device_token;
							$payload['notification_title'] = 'Fragebogen';
							$payload['notification_message'] = 'Schlafqualität';
							$payload['notification_type'] = 'Weekly';
							sendPushToAndroid($deviceToken, $payload);
						} 
					}
				}
			}
		}
	}

	public function send_monthly_notification()
	{
		$current_time = date('H:i:s');
		$patients_data = $this->cron_model->getNotificationReminders();

		if ($patients_data) {
			$current_date = date('Y-m-d');
			$current_time = strtotime($current_date);
			
			foreach ($patients_data as $key => $value) {
				$local_add_date = getGMTDateToLocalDate($value->add_date, 'Y-m-d');
				$local_add_time = strtotime($local_add_date);
				$days = ($current_time - $local_add_time)/60/60/24;
				
				if ($days % 28 == 0) {

					$patient_notification_data = $this->cron_model->get_patient_send_notification($value->patient_id, 'Monthly', $current_date);

					if ($patient_notification_data) {

					} else {
						// $notification_time = getGMTDateToLocalDate($value->monthly_reminder, 'Y-m-d H:i:s');
						$timezone = 'GMT';
						date_default_timezone_set($timezone);
						$notification_time = strtotime($value->monthly_reminder);
						
						if (time() >= $notification_time) {
							$timezone = 'Europe/Berlin';
							date_default_timezone_set($timezone);
							$notification_send_date = date('Y-m-d');
							$input['patient_id'] = $value->patient_id;
							$input['notification_send_date'] = $notification_send_date;
							$input['notification_type'] = 'Monthly';
							$input['notification_send_datetime'] = date('Y-m-d H:i:s');
							$this->cron_model->add_notification($input);

							$patient_info = $this->cron_model->getPatientById($value->patient_id);
							$deviceToken = $patient_info->device_token;
							$payload['notification_title'] = 'Fragebogen';
							$payload['notification_message'] = 'Lebensqualität';
							$payload['notification_type'] = 'Monthly';
							sendPushToAndroid($deviceToken, $payload);
						} 
					}
				}
			}
		}
	}

	function send_daily_basis_notification()
	{
		$current_time = date('H:i:s');
		$patients_data = $this->cron_model->getNotificationReminders();

		if ($patients_data) {
			$current_date = date('Y-m-d');
			$current_time = strtotime($current_date);
			
			foreach ($patients_data as $key => $value) {
				$patient_notification_data = $this->cron_model->get_patient_send_notification($value->patient_id, 'Daily', $current_date);

				if ($patient_notification_data) {

				} else {

					//$notification_time = getGMTDateToLocalDate($value->daily_reminder, 'Y-m-d H:i:s');
					$timezone = 'GMT';
					date_default_timezone_set($timezone);
					$notification_time = strtotime($value->daily_reminder);

					if (time() >= $notification_time) {
						$timezone = 'Europe/Berlin';
						date_default_timezone_set($timezone);
						$notification_send_date = date('Y-m-d');
						$input['patient_id'] = $value->patient_id;
						$input['notification_send_date'] = $notification_send_date;
						$input['notification_type'] = 'Daily';
						$input['notification_send_datetime'] = date('Y-m-d H:i:s');
						$this->cron_model->add_notification($input);

						$patient_info = $this->cron_model->getPatientById($value->patient_id);
						$deviceToken = $patient_info->device_token;
						$payload['notification_title'] = 'Tagesdosis';
						$payload['notification_message'] = 'Schmerzwert & Begleitmedikamente';
						$payload['notification_type'] = 'Daily';
						sendPushToAndroid($deviceToken, $payload);
					} 
				}
			}
		}
	}
	function send_daily_notification() 
	{
		$patients_data = $this->cron_model->get_app_verlaufskontrolle_patients();

		if ($patients_data) {

			foreach ($patients_data as $key => $value) {

				$is_exist = $this->cron_model->getNotificationReminders($value->patient_id);

				if ($is_exist) {

				} else {
					
					$patient_info = $this->cron_model->getPatientById($value->patient_id);
					// pr($patient_info);
					if ($patient_info && $patient_info->device_token) {
						$deviceToken = $patient_info->device_token;
						$payload['notification_title'] = 'Tagesdosis';
						$payload['notification_message'] = 'Schmerzwert & Begleitmedikamente';
						$payload['notification_type'] = 'Daily_type';
						sendPushToAndroid($deviceToken, $payload);
					}
				}
			}
		}
	} 

	function send_alert_of_password_change() {

		$current_date = date('Y-m-d');

		$doctors_data = $this->cron_model->getDoctors($current_date);

		if ($doctors_data) {
			
			foreach ($doctors_data as $key => $value) {

				$doctor_detail = $this->cron_model->getDoctorById($value->id);

				if ($doctor_detail) {
					
					$this->load->model('mailsending_model');
					$this->mailsending_model->SendUserPasswordChangeReminderMail($value->id);

				}
				
			}
		}
	}

	function send_alert_before_one_week_of_password_change() {
		
		$current_date = date('Y-m-d');
		$end_date = time() + (7*24*60*60);
		$next_week_date = date('Y-m-d', $end_date);


		$doctors_data = $this->cron_model->getDoctors($next_week_date);

		if ($doctors_data) {
			
			foreach ($doctors_data as $key => $value) {

				$doctor_detail = $this->cron_model->getDoctorById($value->id);

				if ($doctor_detail) {
					
					$this->load->model('mailsending_model');
					$this->mailsending_model->SendUserPasswordChangeReminderMail($value->id);

				}
				
			}
		}
	}
}
