<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Name:  Testpdf
*
* Version: 1.0.0
*
* Author: Pedro Ruiz Hidalgo
*		  ruizhidalgopedro@gmail.com
*         @pedroruizhidalg
*
* Location: application/controllers/Testpdf.php
*
* Created:  208-02-27
*
* Description:  This demonstrates pdf library is working.
*
* Requirements: PHP5 or above
*
*/


/*class Testpdf extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->add_package_path( APPPATH . 'third_party/fpdf');
        $this->load->library('pdf');
    }

	public function index()
	{
        $html = $this->load->view('front/therapie/folgeverordnung-tabel', true);
        $filename = '1599223866-folgeverordnung.pdf'; 
        $pdfpath = 'assets/uploads/pdf/'.$filename;
        $this->pdf = new Pdf();
        $this->pdf->setSourceFile($pdfpath);
        $this->pdf->Add_Page('P','A4',0);
        $this->pdf->AliasNbPages();
        
        $this->pdf->Output( 'page.pdf' , 'D' );
	}
}
*/

require(APPPATH . 'third_party/fpdf/libraries/fpdf_merge.php');
require(APPPATH . 'third_party/FPDI/src/autoload.php');
include APPPATH . 'third_party/fpdf/libraries/PDFMerger.php';
//use setasign\Fpdi\Fpdi;
//use setasign\Fpdi\PdfReader;

class Testpdf extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->add_package_path( APPPATH . 'third_party/fpdf');
        $this->load->library('pdf');
    }

    public function index()
    {	
        /*$merge = new FPDF_Merge();
        $merge->add('assets/uploads/pdf/Arztfragebogen_Probant_31082020.pdf');
        $merge->add('assets/uploads/pdf/Arztfragebogen_sdkjfh_sjdf_20102020.pdf');
        $merge->output();*/

    	/*$pdf = new \setasign\Fpdi\Fpdi();
        $pageCount = $pdf->setSourceFile('assets/uploads/pdf/Arztfragebogen_Probant_31082020.pdf');
		$pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
		$pdf->addPage();
		$pdf->useImportedPage($pageId, 10, 10, 100);
		$pdf->SetXY(30, 30);
		for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
    		// import a page
		    $templateId = $pdf->importPage($pageNo);

		    $pdf->AddPage();
		    $pdf->getTemplateSize('L');
		    // use the imported page and adjust the page size
		    $pdf->useTemplate($templateId, ['adjustPageSize' => true]);

		    //$pdf->SetFont('Helvetica');
		    //$pdf->SetXY(5, 5);
		    //$pdf->Write(8, 'A complete document imported with FPDI');
		}
		$pdf->Output('I', 'generated.pdf');*/

		$pdf = new \Clegginabox\PDFMerger\PDFMerger;

        $pdf->addPDF('assets/uploads/pdf/Folgeverordnung_asd_asd_03112020.pdf');
        $pdf->addPDF('assets/uploads/pdf/Folgeverordnung_Tablesdjkfhs_sjdfh_12112020.pdf');
        $pdf->merge('F', 'TEST2.pdf');
    }
}


