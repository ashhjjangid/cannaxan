<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/rest/REST_Controller.php';

class Registration extends REST_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api/user_model', 'user');
	}

	public function login_post() {
		$_POST = $this->post();
		if($this->input->post()) {
			$success = false;
			$message = '';
			$data = array();

			$this->form_validation->set_rules('insurance_number', 'Insurance Number', 'trim|required',
				array('required' => 'Die Krankenversicherungsnummer ist ein Pflichtfeld'));
			$this->form_validation->set_rules('date_of_birth', 'Date of Birth', 'trim|required',
				array('required' => 'Das Geburtsdatum ist ein Pflichtfeld'));
			$this->form_validation->set_rules('device_type', 'Device Type', 'trim|required',
				array('required' => 'Das Feld Gerätetyp ist erforderlich'));
			$this->form_validation->set_rules('device_token', 'device token', 'trim|required',
				array('required' => 'Das Feld Gerätetoken ist erforderlich'));

			if ($this->form_validation->run()) {
				//pr($_POST); die;
				
				$insurance_number = $this->input->post('insurance_number');
				$date_of_birth = date('Y-m-d', strtotime($this->input->post('date_of_birth')));

				$valid_user = $this->user->getLoginUserDetails($insurance_number, $date_of_birth);

				if ($valid_user) {
					$unique_id = md5(uniqid(rand(), true));
					$updateData = array();

					if ($this->input->post('name')) {
						$updateData['last_name'] = $this->input->post('name');
					}
					
					if ($this->input->post('street')) {
						$updateData['street'] = $this->input->post('street');
					}

					if ($this->input->post('haus_nr')) {
						$updateData['haus_nr'] = $this->input->post('haus_nr');
					}

					if ($this->input->post('plz')) {
						$updateData['plz'] = $this->input->post('plz');
					}

					if ($this->input->post('ort')) {
						$updateData['ort'] = $this->input->post('ort');
					}


					$updateData['device_type'] = $this->input->post('device_type');
					$updateData['device_token'] = $this->input->post('device_token'); 
					$updateData['token'] = $unique_id; 
					$updateData['update_date'] = getDefaultToGMTDate(time());
					$id = $valid_user->id;
					$this->user->update_user($id, $updateData);
					$valid_user->token = $unique_id;
					$valid_user->device_token = $this->input->post('device_token');
					$data = $valid_user;
					$success = true;
					$message = 'Erfolgreiche Anmeldung.';
				} else {
					$success = false;
					$message = 'Ihre Anmeldedaten sind ungültig';
				}
			} else {
				$message = validation_errors();
				$success = false;
			}

			$response = array('status' => $success, 'message' => $message, 'data' => $data);
			$this->response($response, 200);
		}

	}
	public function patient_detail_post() 
	{
        $data = array();
		$success = 'error';
		$message = '';
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$userdata = $this->getUserDetailByToken($token);
		$data = $userdata;
		$message = 'Patient gefunden.';
		$success = 'success'; 
		$response = array('status'=>$success,'message' => $message,'data' => $data);
    	$this->response($response, 200);
	}


	function check_token_exists_in_header() {
		if (isset($_SERVER['HTTP_TOKEN']) && $_SERVER['HTTP_TOKEN']) {

		} else {
			$response = array('status'=>'error','message' => "Token fehlt", 'data' => array());
			$this->response($response, 200);
		}
	}
	
	function getUserDetailByToken($token) {
		$user_detail = $this->user->getRecordByToken($token);
		
		if ($user_detail) {
			return $user_detail;
			
		} else {
			$response = array('status'=>'error','message' => 'Sitzung abgelaufen.','error_code' => 'delete_user','data'=>array());
			$this->response($response, 200);
		}
	}

	/*function patient_detail_post() {

		if ($this->input->post()) {
			$success = false;
			$message = '';
			$data = array();
			
			$this->form_validation->set_rules('device_token', 'Device Token', 'trim|required');

			if ($this->form_validation->run()) {
				
				$device_token = $this->input->post('device_token');

				$valid_user = $this->user->getUserByDeviceToken($device_token);

				if ($valid_user) {

					$data = $valid_user;

					$success = true;
					$message = 'Data found.';
				} else {
					$success = false;
					$message = 'Data not found';
				}
			} else {
				$message = validation_errors();
				$success = false;
			}

			$response = array('status' => $success, 'message' => $message, 'data' => $data);
			$this->response($response, 200);
		}
	}*/


	/*public function register_post() {
		$_POST = $this->post();
		$insert_id = '';
		if ($this->input->post()) {
			
			$success = false;
			$message = '';
			$data = array();

			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run()) {
				
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				$encrypt_password = encrpty_password($password);

				$is_user_exists = $this->user->checkUserExists($email);

				if (!$is_user_exists) {

					$input['email'] = $email;
					$input['password'] = $encrypt_password;

					$insert_id = $this->user->addNewUser($input);

					if ($insert_id) {
						
						$success = true;
						$message = 'You are successfully registered.';
					} else {

						$success = false;
						$message = 'Something went wrong! Try Again...';
					}					
				} else {
					$message = 'This Email address is already Registered';
					$success = false;
				}

			} else {
				$message = validation_errors();
				$success = false;
			}

			$response = array('status' => $success, 'message' => $message, 'data' => $insert_id);
			$this->response($response, 200);
		}

	}

	public function forgot_password_post()
	{
		$_POST = $this->post();
		if ($this->input->post()) {
			
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

			if ($this->form_validation->run()) {
				$email = $this->input->post('email');

				$user = $this->user->getUserByEmail($email);

				if ($user) {
					$user_id = $user->id;
					$forgotpasswordkey = get_random_key(30);
					$expiry_time = getExpireTime();
					//$resetURL = '<a href="'.site_url(''). '"></a>';
					$resetURL = '<a href="' . base_url('reset-password/' . $forgotpasswordkey) . '">Reset Password</a>';
					$input = array();
					$input['auth_key'] = $forgotpasswordkey;
                    $input['user_id'] = $user_id;
                    $input['key_type'] = 'Forget';
                    $input['used'] = 'No';
                    $input['expire_time'] = getDefaultToGMTDate($expiry_time);
					//pr($input); die;
                    $this->user->setPasswordKey($user_id, $input);
					$this->load->model('mailsending_model', 'mailsend');
					$this->mailsend->SetForgetPasswordMail($user_id, $forgotpasswordkey, $resetURL);

					$message = 'Please check email and reset password.';
                    $success = true;
                    
				} else {
					$message = 'Email Address incorrect';
					$success = false;
				}
			} else {
				$message = validation_errors();
				$success = false;
			}

			$response = array('status'=>$success, 'message' => $message, 'data' => array());
			$this->response($response, 200);
		}
	}*/
}
