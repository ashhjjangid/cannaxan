<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Mail_templates class.
 * 
 * @extends CI_Controller
 */
class Mail_templates extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

	function __construct() {
		Parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		$this->load->model('admin/mail_templates_model', 'mail_templates');
		$this->load->model('admin/user_model', 'user');
		$this->user_id = $this->session->userdata('admin_id');
	}

    /**
     * index function.
     * 
     * @access public
     * @return record of all templates
     */

	function index() {
		$output['page_title'] = 'List of E-mail Templates';
		$output['left_menu'] = 'Mail_templates';
		$output['left_submenu'] = 'Mail_templates_list';

		$templates = $this->mail_templates->getAllTemplates();
		$users = $this->user->getAllUsers();
		$output['templates'] = $templates;
		$output['users_list'] = $users;

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/mail_templates/mail_templates_list');
		$this->load->view('admin/includes/footer');
	}

    /**
     * changeStatus function.
     * 
     * @access public
     * @return null
     */

	function changeStatus() {
		$id = $this->input->get('id',true);
		$status = $this->input->get('status',true);
		$this->mail_templates->changeStatusById($id,$status);
		$data['success'] = true;
		$data['message'] = 'Record updated Successfully';
		json_output($data);
	} 	

    /**
     * multiTaskOperation function.
     * 
     * @access public
     * @return null
     */	

	function multiTaskOperation() {
		$task = $this->input->post('task',true);
		$ids = $this->input->post('ids',true);
		$dataIds = explode(',',$ids);
		foreach ($dataIds as $key => $value) 
		{
			if($task=='Active' || $task=='Inactive')
			{
				$this->mail_templates->changeStatusById($value,$task);			
				$message = 'Status of selected records changed successfully.';
			}
		}		
		$data['ids'] = $ids;
		$data['success'] = true;
		$data['message'] = $message;
		$data['callBackFunction'] = 'callBackCommonDelete';
		json_output($data);
	}

    /**
     * update function.
     * 
     * @access public
     * @param mixed $id
     * @return null
     */

	function update($id) {
		$output['page_title'] = 'Update E-mail Template';
		$output['left_menu'] = 'Mail_templates';
		$output['left_submenu'] = 'Mail_templates_add';
		$output['message'] ='';
		$emailTemplate = $this->mail_templates->getSingleTemplateById($id);
		$output['subject'] = $emailTemplate->subject; 
		$output['content'] = $emailTemplate->content;
		$output['name'] = $emailTemplate->name;
		
		if (isset($_POST) && !empty($_POST)) {	
			$this->form_validation->set_rules('name', 'template name', 'trim|required');			
			$this->form_validation->set_rules('subject', 'subject', 'trim|required');
			$this->form_validation->set_rules('content', 'content', 'trim|required');
			
			if ($this->form_validation->run()) {
				$input = array();
				$input['name'] = $this->input->post('name',true);
				$input['subject'] = $this->input->post('subject',true);
				$input['content'] = $this->input->post('content',true);
				$this->mail_templates->setEmailTemplate($id, $input);
				$message = 'Record Updated Successfully';
				$this->session->set_flashdata('message', $message);
				$success = true;
				$data['redirectURL'] = site_url('admin/mail_templates');
			
			} else {
				$success = false;
				$message = validation_errors();
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);
		}
		$constants = array();
		$constantsB = explode(',',$emailTemplate->constants);
		
		foreach ($constantsB as $key => $value) {
			$constants[$value] = '{{'.$value.'}}';
		}
		
		$output['constants'] = $constants;
		$output['id'] = $id;
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/mail_templates/add_form');
		$this->load->view('admin/includes/footer');
	}
}