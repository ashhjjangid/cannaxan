<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Email_cron class.
 * 
 * @extends CI_Controller
 */

class Email_cron extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        Parent::__construct();
        $this->load->model('admin/mail_cron_model', 'mail_cron');
        $this->load->model('admin/mail_schedule_model', 'mail_schedule');
        $this->load->model('mailsending_model', 'mail_send');
        $this->load->model('admin/user_model', 'user');
    }


    /**
     * index function.
     * 
     * @access public
     * @return null
     */

    public function index() {
        $record = $this->mail_cron->getAllPendingScheduleEmails();

        foreach($record as $scheduled_data) {
            $newsletter_detail  = $this->newsletter->get_newsletter_by_id($record->template_id);
            $newsletter_message = $newsletter_detail->message;
            $newsletter_subject = $newsletter_detail->subject;
            $selected_users = $this->mail_schedule->getAllScheduleUsers($scheduled_data->id);

            foreach($selected_users as $users) {
                $user_email = $value->email;
                $userEmailId = base64_encode($user_email);
                $this->mailsend->sendNewsletterMailToSubcriberByAdmin($newsletter_message,$newsletter_subject,$user_email);
            }
            $this->mail_cron->setSendSucessMail($scheduled_data->id);
        }
    }
}
