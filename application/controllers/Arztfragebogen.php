<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arztfragebogen extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('patients_model', 'patients');
		$this->load->model('arztfragebogen_model', 'arztfragebogen');
	}

	function index() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}

		$output['header_menu'] = 'arztfragebogen';
		$insured_number = $this->input->get('in');
		$output['insured_number'] = $insured_number;
		$doctor_id = $this->session->userdata('user_id');
		$is_exists_patient = $this->patients->getPatientByInsuranceNumber($insured_number);
		$random_string = get_random_key(6);
		$question_id = ($_GET && isset($_GET['qid']) && !empty($_GET['qid']))?$this->input->get('qid'):'';

		if ($is_exists_patient) {			
			$patient_id = $is_exists_patient->id;
			$questiondata = $this->arztfragebogen->getWebQuestionDataStart($patient_id, $doctor_id, $question_id);

			if ($this->input->post()) {
				//pr($_POST); die('hee');

				$this->form_validation->set_rules('37b_sgb_v', 'Erfolgt die Verordnung im Rahmen der genehmigten Versorgung nach § 37b SGB V (spezialisierte ambulante Palliativversorgung)?', 'trim|required', array('required' => 'Bitte markieren Sie ja oder nein.'));
				$this->form_validation->set_rules('behandlungsziel[]', 'Wie lautet das Behandlungsziel?', 'trim', array('required' => 'Bitte geben Sie eine Option bei Behandlungsziel ein.'));

				if (!$this->input->post('behandlungsziel')) {
					
					$this->form_validation->set_rules('behandlungsziel_text_field', 'Wie lautet das Behandlungsziel?', 'trim|required', array('required' => 'Bitte geben Sie eine Option bei Behandlungsziel ein.'));
				}

				if ($this->form_validation->run()) {

					if ($questiondata && $question_id) {
						$insert_id = $questiondata->id;
						$random_string = $question_id;
					} else {
						$input['doctor_id'] = $doctor_id;
						$input['patient_id'] = $patient_id;
						$input['is_process_complete'] = 'No';
						$input['qid'] = $random_string;
						$input['add_date'] = getDefaultToGMTDate(time());
						$insert_id = $this->arztfragebogen->addingWebQuestionOfPatientStart($input);
					}

					if ($insert_id) {

						$this->arztfragebogen->deleteWebQuestion_1($insert_id);						
						$sgb_v = $this->input->post('37b_sgb_v');
						$question_1['web_patient_question_id'] = $insert_id;
						$question_1['answer'] = $sgb_v;
						$question_1['question_no'] = 'First';
						$question_1['add_date'] = getDefaultToGMTDate(time());

						$this->arztfragebogen->addWebQuestion_1($question_1);

						$behandlungsziel = $this->input->post('behandlungsziel');

						if ($behandlungsziel) {
							
							foreach ($behandlungsziel as $key => $value) {
								
								$question_2['web_patient_question_id'] = $insert_id;
								$question_2['answer'] = $value;
								$question_2['question_no'] = 'Second';
								$question_2['add_date'] = getDefaultToGMTDate(time());
								//pr($question_2);
								$this->arztfragebogen->addWebQuestion_1($question_2);
							}
						}

						if ($this->input->post('behandlungsziel_text_field')) {

							$question_2_text_field['web_patient_question_id'] = $insert_id;
							$question_2_text_field['answer'] = NULL;
							$question_2_text_field['behandlungsziel_text_field'] = $this->input->post('behandlungsziel_text_field');
							$question_2_text_field['question_no'] = 'Second';
							$question_2_text_field['add_date'] = getDefaultToGMTDate(time());
							//pr($question_2_text_field); die;
							$this->arztfragebogen->addWebQuestion_1($question_2_text_field);
						}

						$message = 'Der Fragebogen wurde erfolgreich gespeichert.';
						$success = true;
						$data['redirectURL'] = base_url('kostenubernahmeantrag/step-2?in='.$insured_number.'&qid='.$random_string);
						

					} else {
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
						$success = false;
					}

				} else {
					$message = validation_errors();
					$success = false;
				}

				$data['message'] = $message;
				$data['success'] = $success;
				json_output($data);
			}

			if ($questiondata) {	
				$recorded_answer_1 = $this->arztfragebogen->get_first_step_WebQuestion_1_Data($questiondata->id);
				$recorded_answer_2 = $this->arztfragebogen->get_first_step_WebQuestion_2_Data($questiondata->id);
				//pr($recorded_answer_2); die;
				$output['question_1_data'] = $recorded_answer_1;
				$output['question_2_data'] = $recorded_answer_2;
			} else {
				$output['question_1_data'] = '';
				$output['question_2_data'] = '';
			}
			
			$output['qid'] = $question_id;
			//pr($recorded_answer_1);
			//pr($recorded_answer_2); die;
			$this->load->view('front/includes/header', $output);
			$this->load->view('front/arztfragebogen/erfolgt-die-verordnung');
			$this->load->view('front/includes/footer');

		} else {
			redirect('patients');
		}
	}

	function arztfragebogen_step_2() {

		/*pdf page no 2 */
		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}
		$success = '';
		$message = '';
		$output['header_menu'] = 'arztfragebogen';
		$question_added_id = $this->input->get('qid');
		$success = true;
		$output['qid'] = $question_added_id;
		$insured_number = $this->input->get('in');
		$output['insured_number'] = $insured_number;

		$doctor_id = $this->session->userdata('user_id');


		$is_exists_patient = $this->patients->getPatientByInsuranceNumber($insured_number);
		if ($is_exists_patient) {
			$patient_id = $is_exists_patient->id;
			$question_data_exists = $this->arztfragebogen->checkQuestionStartDataExistsById($question_added_id);
			//pr($question_data_exists); die;
			if ($question_data_exists) {
				
				if ($this->input->post()) {
					//pr($_POST); die();
					
					if ($this->input->post('erkrankung_schwerwiegend')) {
						
						if ($this->input->post('erkrankung_schwerwiegend') == 'Yes') {

							if ($this->input->post('endgradig_chronifizierte_number') || $this->input->post('aufhebung_der_schlafarchitektur') || $this->input->post('depressive_entwicklung') || $this->input->post('latente_suizidalitat') || $this->input->post('zunehmende_bettlaerigkeit') || $this->input->post('beruflichen_tatigkeit') || $this->input->post('bettlagerigkeit_aufgrund_schmerzen') || $this->input->post('die_gesamtsymptomatik') || $this->input->post('die_lebenserwartung')) {
								
								$success = true;
								$message = '';
								/*if ($this->input->post('zunehmende_bettlaerigkeit')) {
									
									$success = true;
									$message = '';
									//$this->form_validation->set_rules('zunehmende_bettlaerigkeit', 'Zunehmende Bettlägerigkeit aufgrund Schmerzen Kurze Beschreibung der Grunderkrankung', 'trim|required');
								} else {
									$message = 'Bitte beschreiben Sie kurz die Grunderkrankung.';
									$success = false;
									$data['success'] = $success;
									$data['message'] = $message;
									json_output($data);	 die;
								}*/

								if ($this->input->post('endgradig_chronifizierte_number')) {
									if (!$this->input->post('nrs_skala')) {
										$message = 'Bitte geben Sie einen Wert auf der NRS Skala von 0 bis 10 an.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data);	 die;
									} else {

										$success = true;
										$message = '';
									}
									
									//$this->form_validation->set_rules('nrs_skala', 'Endgradig chronifizierte', 'trim|numeric|required');
								}
									
								if ($this->input->post('beruflichen_tatigkeit')) {
									if ($this->input->post('beruflichen')) {
										//die('here');
										$success = true;
										$message = '';
									} else {
										$message = 'Bitte wählen Sie eine Option.';
										$success = false;	
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
									//$this->form_validation->set_rules('beruflichen', 'Einschränkung der beruflichen Tätigkeit', 'trim|required');
								}
								
								
									
								if ($this->input->post('die_lebenserwartung')) {
									if (!$this->input->post('lebenserwartung_patienten')) {
										$message = 'Bitte geben Sie einen Wert bei Lebenserwartung an.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data);	 die;
									} else {
										$success = true;
										$message = '';
									}
									//$this->form_validation->set_rules('lebenserwartung_patienten', 'Die Lebenserwartung', 'trim|numeric|required');
								}
							} else {

								$success = false;
								$message = 'Bitte markieren Sie ja oder nein.';
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}
						}
					} else {
						$success = false;
						$message = 'Bitte markieren Sie ja oder nein.';
						$data['success'] = $success;
						$data['message'] = $message;
						json_output($data); die;
					}

					if ($success == true) {

						$input['web_patient_question_id'] = $question_data_exists->id;
						if ($this->input->post('erkrankung_schwerwiegend') == 'Yes') {
							$input['answer'] = $this->input->post('erkrankung_schwerwiegend');
							$input['add_date'] = getDefaultToGMTDate(time());	
							if ($this->input->post('endgradig_chronifizierte_number')) {
													
								$input['endgradig_chronifizierte_number'] = $this->input->post('nrs_skala');
							}					
							$input['aufhebung_der_schlafarchitektur'] = $this->input->post('aufhebung_der_schlafarchitektur');
							$input['depressive_entwicklung'] = $this->input->post('depressive_entwicklung');
							$input['latente_suizidalitat'] = $this->input->post('latente_suizidalitat');
							$input['beruflichen_tatigkeit'] = $this->input->post('beruflichen');
							$input['bettlagerigkeit_aufgrund_schmerzen'] = $this->input->post('zunehmende_bettlaerigkeit');
							$input['die_gesamtsymptomatik'] = $this->input->post('die_gesamtsymptomatik');
							if ($this->input->post('die_lebenserwartung')) {
								
								$input['die_lebenserwartung'] = $this->input->post('lebenserwartung_patienten');
							}
							
						} else {
							$input['answer'] = $this->input->post('erkrankung_schwerwiegend');
							$input['add_date'] = getDefaultToGMTDate(time());
							$input['endgradig_chronifizierte_number'] = NULL;
							$input['aufhebung_der_schlafarchitektur'] = NULL;
							$input['depressive_entwicklung'] = NULL;
							$input['latente_suizidalitat'] = NULL;
							$input['beruflichen_tatigkeit'] = NULL;
							$input['bettlagerigkeit_aufgrund_schmerzen'] = NULL;
							$input['die_gesamtsymptomatik'] = NULL;
							$input['die_lebenserwartung'] = NULL;
						}

						 //pr($input); die;
						$this->arztfragebogen->deleteWebQuestion_2($question_data_exists->id);
						$question_2_data_added = $this->arztfragebogen->addWebQuestion_2($input);

						if ($question_2_data_added) {
							$success = true;
							$message = 'Der Fragebogen wurde erfolgreich gespeichert.';
							$data['redirectURL'] = base_url('kostenubernahmeantrag/step-3?in='.$insured_number.'&qid='.$question_added_id);
						} else {
							$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
						}
					}
					$data['success'] = $success;
					$data['message'] = $message;
					json_output($data);
				}
				
				$recorded_answer = $this->arztfragebogen->get_second_step_question_data($question_data_exists->id);

				if ($recorded_answer) {
					
					$output['recorded_answer'] = $recorded_answer;
					$output['erkrankung_schwerwiegend'] = $recorded_answer->answer;
					$output['endgradig_chronifizierte_number'] = $recorded_answer->endgradig_chronifizierte_number;
					$output['aufhebung_der_schlafarchitektur'] = $recorded_answer->aufhebung_der_schlafarchitektur;
					$output['depressive_entwicklung'] = $recorded_answer->depressive_entwicklung;
					$output['latente_suizidalitat'] = $recorded_answer->latente_suizidalitat;
					$output['beruflichen_tatigkeit'] = $recorded_answer->beruflichen_tatigkeit;
					$output['bettlagerigkeit_aufgrund_schmerzen'] = $recorded_answer->bettlagerigkeit_aufgrund_schmerzen;
					$output['die_gesamtsymptomatik'] = $recorded_answer->die_gesamtsymptomatik;
					$output['die_lebenserwartung'] = $recorded_answer->die_lebenserwartung;
				} else {

					$last_process_complete_question = $this->arztfragebogen->get_last_process_complete_answer($patient_id, $doctor_id);

					if ($last_process_complete_question) {
						$recorded_answer = $this->arztfragebogen->get_second_step_question_data($last_process_complete_question->id);
						$output['recorded_answer'] = $recorded_answer;
						$output['erkrankung_schwerwiegend'] = $recorded_answer->answer;
						$output['endgradig_chronifizierte_number'] = $recorded_answer->endgradig_chronifizierte_number;
						$output['aufhebung_der_schlafarchitektur'] = $recorded_answer->aufhebung_der_schlafarchitektur;
						$output['depressive_entwicklung'] = $recorded_answer->depressive_entwicklung;
						$output['latente_suizidalitat'] = $recorded_answer->latente_suizidalitat;
						$output['beruflichen_tatigkeit'] = $recorded_answer->beruflichen_tatigkeit;
						$output['bettlagerigkeit_aufgrund_schmerzen'] = $recorded_answer->bettlagerigkeit_aufgrund_schmerzen;
						$output['die_gesamtsymptomatik'] = $recorded_answer->die_gesamtsymptomatik;
						$output['die_lebenserwartung'] = $recorded_answer->die_lebenserwartung;
					} else {
						$output['recorded_answer'] = '';
						$output['erkrankung_schwerwiegend'] = '';
						$output['endgradig_chronifizierte_number'] = '';
						$output['aufhebung_der_schlafarchitektur'] = '';
						$output['depressive_entwicklung'] = '';
						$output['latente_suizidalitat'] = '';
						$output['beruflichen_tatigkeit'] = '';
						$output['bettlagerigkeit_aufgrund_schmerzen'] = '';
						$output['die_gesamtsymptomatik'] = '';
						$output['die_lebenserwartung'] = '';
					}
				}
				
				$this->load->view('front/includes/header', $output);
				$this->load->view('front/arztfragebogen/erkrankung-schwerwiegend');
				$this->load->view('front/includes/footer');
			} else {
				
				redirect('arztfragebogen/step-1?in='.$insured_number);
			}

		} else {
			redirect('patients');
		}

	}

	function arztfragebogen_step_3() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}

		$output['header_menu'] = 'arztfragebogen';
		$doctor_id = $this->session->userdata('user_id');
		$insured_number = $this->input->get('in');
		$output['insured_number'] = $insured_number;
		$is_exists_patient = $this->patients->getPatientByInsuranceNumber($insured_number);
		$question_added_id = $this->input->get('qid');
		//pr($question_added_id); die;
		$success = true;
		$message = '';
		$output['qid'] = $question_added_id;

		/*pdf page no 3 */

		if ($is_exists_patient) {
			$question_data_exists = $this->arztfragebogen->checkQuestionStartDataExistsById($question_added_id);

			if ($question_data_exists) {
				
				if ($this->input->post()) {
					
					//$this->form_validation->set_rules('arztlicher_einschatzung', 'Es ist eine medizinische Versorgung (ärztliche Behandlung, Arzneimitteltherapie) erforderlich, ohne die nach ärztlicher Einschä tzung', 'trim');

					if ($this->input->post('arztlicher_einschatzung')) {

						if ($this->input->post('lebensbedrohliche_verschlimmerung') || $this->input->post('verminderung_der_lebenserwartu') || $this->input->post('dauerhafte_beeintrachtigung')) {
							$success = true;
						} else {
							$message = 'Bitte wählen Sie eine Option.';
							//$this->form_validation->set_message('required', 'Select any one option');
							$success = false;
						}

					}
					if ($success == true) {

						$input['web_patient_question_id'] = $question_data_exists->id;
						if ($this->input->post('arztlicher_einschatzung') == 'Yes') {
							
							$input['answer'] = $this->input->post('arztlicher_einschatzung');
							$input['eine_lebensbedrohliche_verschlimmerung'] = $this->input->post('lebensbedrohliche_verschlimmerung');
							$input['eine_verminderung_der_lebenserwartung'] = $this->input->post('verminderung_der_lebenserwartu');
							$input['dauerhafte_beeintrachtigung'] = $this->input->post('dauerhafte_beeintrachtigung');
							$input['add_date'] = getDefaultToGMTDate(time());
						} else {
							$input['answer'] = 'No';
							$input['eine_lebensbedrohliche_verschlimmerung'] = NULL;
							$input['eine_verminderung_der_lebenserwartung'] = NULL;
							$input['dauerhafte_beeintrachtigung'] = NULL;
							$input['add_date'] = getDefaultToGMTDate(time());
						}
						//pr($input); die;
						$this->arztfragebogen->deleteWebQuestion_3($question_data_exists->id);
						$question_3_data_added = $this->arztfragebogen->addWebQuestion_3($input);

						if ($question_3_data_added) {
							$success = true;
							$message = 'Der Fragebogen wurde erfolgreich gespeichert.';
							$data['redirectURL'] = base_url('kostenubernahmeantrag/step-4?in='.$insured_number.'&qid='.$question_added_id);
						} else {
							$success = false;
							$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
						}

					}

					$data['message'] = $message;
					$data['success'] = $success;
					json_output($data);
				}
				$recorded_answer = $this->arztfragebogen->get_third_step_question_data($question_data_exists->id);
				//pr($recorded_answer); die;

				if ($recorded_answer) {
					$output['recorded_answer'] = $recorded_answer;
					$output['arztlicher_einschatzung'] = $recorded_answer->answer;
					$output['eine_lebensbedrohliche_verschlimmerung'] = $recorded_answer->eine_lebensbedrohliche_verschlimmerung;
					$output['eine_verminderung_der_lebenserwartung'] = $recorded_answer->eine_verminderung_der_lebenserwartung;
					$output['dauerhafte_beeintrachtigung'] = $recorded_answer->dauerhafte_beeintrachtigung;
				} else {
					$last_process_complete_question = $this->arztfragebogen->get_last_process_complete_answer($is_exists_patient->id, $doctor_id);

					if ($last_process_complete_question) {
						$recorded_answer = $this->arztfragebogen->get_third_step_question_data($last_process_complete_question->id);
						$output['recorded_answer'] = $recorded_answer;
						$output['arztlicher_einschatzung'] = $recorded_answer->answer;
						$output['eine_lebensbedrohliche_verschlimmerung'] = $recorded_answer->eine_lebensbedrohliche_verschlimmerung;
						$output['eine_verminderung_der_lebenserwartung'] = $recorded_answer->eine_verminderung_der_lebenserwartung;
						$output['dauerhafte_beeintrachtigung'] = $recorded_answer->dauerhafte_beeintrachtigung;
					} else {
						$output['recorded_answer'] = '';
						$output['arztlicher_einschatzung'] = '';
						$output['eine_lebensbedrohliche_verschlimmerung'] = '';
						$output['eine_verminderung_der_lebenserwartung'] = '';
						$output['dauerhafte_beeintrachtigung'] = '';
					}
				}
				$this->load->view('front/includes/header', $output);
				$this->load->view('front/arztfragebogen/medizinische-versorgung');
				$this->load->view('front/includes/footer');
			} else {
				redirect('arztfragebogen/step-2?in='.$insured_number.'&qid='.$question_added_id);
			}
			
		} else {
			redirect('patients');
		}

	}

	function arztfragebogen_step_4() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}

		$output['header_menu'] = 'arztfragebogen';
		$success = true;
		$message = '';
		$doctor_id = $this->session->userdata('user_id');
		$insured_number = $this->input->get('in');
		$output['insured_number'] = $insured_number;
		$is_exists_patient = $this->patients->getPatientByInsuranceNumber($insured_number);
		$question_added_id = $this->input->get('qid');
		//pr($question_added_id); die;
		$output['qid'] = $question_added_id;


		$doctor_id = $this->session->userdata('user_id');

		if ($is_exists_patient) {
			
			/*pdf page no 4 */
			/*question no 2 on pdf page no 4 option are till page no 16 */

			$question_data_exists = $this->arztfragebogen->checkQuestionStartDataExistsById($question_added_id);

			if ($question_data_exists) {
				$question_2_recorded_answers = $this->arztfragebogen->get_forth_step_question_2_data($question_data_exists->id);
				//pr($question_2_recorded_answers); die;
				if ($question_2_recorded_answers) {
					$who_1_patient_answer = $this->arztfragebogen->get_who_1_data($question_2_recorded_answers->id);
					//if ($who_1_patient_answer) {
						
					$nichtPatientAnswers = $this->arztfragebogen->getwho1Nichtrecords($question_2_recorded_answers->id, 'Nicht-Opioidanalgetika');
					if ($nichtPatientAnswers) {
						$output['nichtPatientAnswers'] = $nichtPatientAnswers;
					} else {
						$output['nichtPatientAnswers'] = '';
					}

					$steroidePatientAnswers = $this->arztfragebogen->getwho1Steroiderecords($question_2_recorded_answers->id, 'Steroide');
						//pr($steroidePatientAnswers); die;
					if ($steroidePatientAnswers) {
						$output['steroidePatientAnswers'] = $steroidePatientAnswers;
					} else {
						$output['steroidePatientAnswers'] = '';
					}
					$neuroleptikaPatientAnswers = $this->arztfragebogen->getwho1Neuroleptikarecords($question_2_recorded_answers->id, 'Neuroleptika');
					if ($neuroleptikaPatientAnswers) {
						$output['neuroleptikaPatientAnswers'] = $neuroleptikaPatientAnswers;
					} else {
						$output['neuroleptikaPatientAnswers'] = '';
					}
					$antidepressivaPatientAnswers = $this->arztfragebogen->getwho1Antidepressivarecords($question_2_recorded_answers->id, 'Antidepressiva');
					if ($antidepressivaPatientAnswers) {
						$output['antidepressivaPatientAnswers'] = $antidepressivaPatientAnswers;
					} else {
						$output['antidepressivaPatientAnswers'] = '';
					}
					$sedativaPatientAnswers = $this->arztfragebogen->getwho1Sedativarecords($question_2_recorded_answers->id, 'Sedativa');
					if ($sedativaPatientAnswers) {
						$output['sedativaPatientAnswers'] = $sedativaPatientAnswers;
					} else {
						$output['sedativaPatientAnswers'] = '';
					}
					$antikonvulsivaPatientAnswers = $this->arztfragebogen->getwho1Antikonvulsivarecords($question_2_recorded_answers->id, 'Antikonvulsiva');
					if ($antikonvulsivaPatientAnswers) {
						$output['antikonvulsivaPatientAnswers'] = $antikonvulsivaPatientAnswers;
					} else {
						$output['antikonvulsivaPatientAnswers'] = '';
					}
					$antiemetikaPatientAnswers = $this->arztfragebogen->getwho1Antiemetikarecords($question_2_recorded_answers->id, 'Antiemetika');
					if ($antiemetikaPatientAnswers) {
						$output['antiemetikaPatientAnswers'] = $antiemetikaPatientAnswers;
					} else {
						$output['antiemetikaPatientAnswers'] = '';
					}
				//}



					$output['who_1_patient_answer'] = $who_1_patient_answer;
					
					$output['who_2_patient_answer'] = $this->arztfragebogen->get_who_2_data($question_2_recorded_answers->id);
					$output['who_3_patient_answer'] = $this->arztfragebogen->get_who_3_data($question_2_recorded_answers->id);
					$output['spasmolytika_patient_answer'] = $this->arztfragebogen->get_spasmolytika_data($question_2_recorded_answers->id);
					//pr($output); die;
				} else {

					$last_process_complete_question = $this->arztfragebogen->get_last_process_complete_answer($is_exists_patient->id, $doctor_id);

					if ($last_process_complete_question) {
						$question_2_recorded_answers = $this->arztfragebogen->get_forth_step_question_2_data($last_process_complete_question->id);
						
						if ($question_2_recorded_answers) {
							$who_1_patient_answer = $this->arztfragebogen->get_who_1_data($question_2_recorded_answers->id);
							//if ($who_1_patient_answer) {
								
							$nichtPatientAnswers = $this->arztfragebogen->getwho1Nichtrecords($question_2_recorded_answers->id, 'Nicht-Opioidanalgetika');
							if ($nichtPatientAnswers) {
								$output['nichtPatientAnswers'] = $nichtPatientAnswers;
							} else {
								$output['nichtPatientAnswers'] = '';
							}

							$steroidePatientAnswers = $this->arztfragebogen->getwho1Steroiderecords($question_2_recorded_answers->id, 'Steroide');
								//pr($steroidePatientAnswers); die;
							if ($steroidePatientAnswers) {
								$output['steroidePatientAnswers'] = $steroidePatientAnswers;
							} else {
								$output['steroidePatientAnswers'] = '';
							}
							$neuroleptikaPatientAnswers = $this->arztfragebogen->getwho1Neuroleptikarecords($question_2_recorded_answers->id, 'Neuroleptika');
							if ($neuroleptikaPatientAnswers) {
								$output['neuroleptikaPatientAnswers'] = $neuroleptikaPatientAnswers;
							} else {
								$output['neuroleptikaPatientAnswers'] = '';
							}
							$antidepressivaPatientAnswers = $this->arztfragebogen->getwho1Antidepressivarecords($question_2_recorded_answers->id, 'Antidepressiva');
							if ($antidepressivaPatientAnswers) {
								$output['antidepressivaPatientAnswers'] = $antidepressivaPatientAnswers;
							} else {
								$output['antidepressivaPatientAnswers'] = '';
							}
							$sedativaPatientAnswers = $this->arztfragebogen->getwho1Sedativarecords($question_2_recorded_answers->id, 'Sedativa');
							if ($sedativaPatientAnswers) {
								$output['sedativaPatientAnswers'] = $sedativaPatientAnswers;
							} else {
								$output['sedativaPatientAnswers'] = '';
							}
							$antikonvulsivaPatientAnswers = $this->arztfragebogen->getwho1Antikonvulsivarecords($question_2_recorded_answers->id, 'Antikonvulsiva');

							if ($antikonvulsivaPatientAnswers) {
								$output['antikonvulsivaPatientAnswers'] = $antikonvulsivaPatientAnswers;
							} else {
								$output['antikonvulsivaPatientAnswers'] = '';
							}
							$antiemetikaPatientAnswers = $this->arztfragebogen->getwho1Antiemetikarecords($question_2_recorded_answers->id, 'Antiemetika');
							if ($antiemetikaPatientAnswers) {
								$output['antiemetikaPatientAnswers'] = $antiemetikaPatientAnswers;
							} else {
								$output['antiemetikaPatientAnswers'] = '';
							}
						//}



							$output['who_1_patient_answer'] = $who_1_patient_answer;
							
							$output['who_2_patient_answer'] = $this->arztfragebogen->get_who_2_data($question_2_recorded_answers->id);
							$output['who_3_patient_answer'] = $this->arztfragebogen->get_who_3_data($question_2_recorded_answers->id);
							$output['spasmolytika_patient_answer'] = $this->arztfragebogen->get_spasmolytika_data($question_2_recorded_answers->id);
							// pr($output); die;
						} else {
							$output['who_1_patient_answer'] = '';
							$output['who_2_patient_answer'] = '';
							$output['who_3_patient_answer'] = '';
							$output['spasmolytika_patient_answer'] = '';
						}
					} else {
						$output['who_1_patient_answer'] = '';
						$output['who_2_patient_answer'] = '';
						$output['who_3_patient_answer'] = '';
						$output['spasmolytika_patient_answer'] = '';
					}
				}
				//pr($question_2_recorded_answers); die;
				if ($this->input->post()) {
					//pr($_POST); die;

					if ($this->input->post('physiotherapie_1') || $this->input->post('interventionelle_verfahren') || $this->input->post('psychotherapie_2') || $this->input->post('tens') || $this->input->post('akupunktur') || $this->input->post('keine')) {
						
						$success = true;
					} else {
						$message = 'Bitte wählen Sie eine Option.';
						$success = false;
						$data['success'] = $success;
						$data['message'] = $message;
						json_output($data); die;
					}

					if ($this->input->post('schmerztherapie_durchgefuhrt')) {
						$success = true;
					} else {
						$message = 'Eine multimodale Schmerztherapie ist erforderlich.';
						$success = false;
						$data['success'] = $success;
						$data['message'] = $message;
						json_output($data); die;
					}

					if ($this->input->post('multimodale_schmerztherapie')) {

						if ($this->input->post('multimodale_schmerztherapie') == 'nein') {

							if ($this->input->post('multimodale_schmerztherapie_text')) {
								$success = true;
							} else {
								$message = 'Erklären Sie, dass eine multimodale Schmerztherapie aus medizinischer Sicht erforderlich ist.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}
						} else {
							$success = true;
						}
					} else {
						$message = 'Eine multimodale Schmerztherapie aus medizinischer Sicht ist erforderlich.';
						$success = false;
						$data['success'] = $success;
						$data['message'] = $message;
						json_output($data); die;
					}

					if ($this->input->post('who_stufe1') || $this->input->post('who_stufe2') || $this->input->post('who_stufe3') || $this->input->post('spasmolytika')) {
							$success = true;
						} else {
							$success = false;
							$message = 'Bitte wählen Sie eine Option.';
							$data['success'] = $success;
							$data['message'] = $message;
							json_output($data);
						}	

					if ($this->input->post('who_stufe1') == 'Yes') {

						if ($this->input->post('nicht_opioidanalgetika') || $this->input->post('steroide') || $this->input->post('neuroleptika') || $this->input->post('antidepressiva') || $this->input->post('sedativa') || $this->input->post('antikonvulsiva') || $this->input->post('antiemetika')) {
							$success = true;
						} else {
							$message = 'Bitte wählen Sie eine Option.';
							$success = false;
							$data['success'] = $success;
							$data['message'] = $message;
							json_output($data);
						}

						if ($this->input->post('nicht_opioidanalgetika')  == 'Nicht-Opioidanalgetika') {
						
							if ($this->input->post('nicht_opioidanalgetika_input_1') || $this->input->post('nicht_opioidanalgetika_input_2') || $this->input->post('nicht_opioidanalgetika_input_3')) {
								$success = true;
								if ($this->input->post('nicht_opioidanalgetika_input_1') == 'Freifeld') {
									
									if ($this->input->post('nicht_medikament_free_text_first')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Nicht-Opioidanalgetika Medikament';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('nicht_opioidanalgetika_input_2') == 'Freifeld') {
									
									if ($this->input->post('nicht_medikament_free_text_second')) {
										$success = true;
									} else {
										$message = 'Bitte wählen Sie eine Option bei Nicht-Opioidanalgetika Medikament.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('nicht_opioidanalgetika_input_3') == 'Freifeld') {
									
									if ($this->input->post('nicht_medikament_free_text_third')) {
										$success = true;
									} else {
										$message = 'Nicht-Opioidanalgetika Medikament Text field is required';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option bei Nicht-Opioidanalgetika Medikament.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
								
							}

							if ($this->input->post('nicht_therapieeflog')) {
								$success = true;
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}

							if ($this->input->post('nicht_gastrointestinal_input')) {
								$success = true;
								if ($this->input->post('nicht_gastrointestinal_input') == 'Freifeld') {
									
									if ($this->input->post('nicht_gastrointestinal_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Nicht-Opioidanalgetika Gastrointestinal ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('nicht_kardial_input')) {
								$success = true;
								if ($this->input->post('nicht_kardial_input') == 'Freifeld') {
									
									if ($this->input->post('nicht_kardial_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Nicht-Opioidanalgetika Kardial ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('nicht_nephrologisch_input')) {
								$success = true;
								if ($this->input->post('nicht_nephrologisch_input') == 'Freifeld') {
									
									if ($this->input->post('nicht_nephrologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Nicht-Opioidanalgetika Nephrologisch  ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('nicht_neurologisch_input')) {
								$success = true;
								if ($this->input->post('nicht_neurologisch_input') == 'Freifeld') {
									
									if ($this->input->post('nicht_neurologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Nicht-Opioidanalgetika Neurologisch/psychotrop ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}								
							}
						}

						if ($this->input->post('steroide')  == 'Steroide') {
						
							if ($this->input->post('steroide_input_1') || $this->input->post('steroide_input_2') || $this->input->post('steroide_input_3')) {
								$success = true;
								if ($this->input->post('steroide_input_1') == 'Freifeld') {
									
									if ($this->input->post('steroide_medikament_free_text_first')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Steroide Medikament 1 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('steroide_input_2') == 'Freifeld') {
									
									if ($this->input->post('steroide_medikament_free_text_second')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Steroide Medikament 2 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('steroide_input_3') == 'Freifeld') {
									
									if ($this->input->post('steroide_medikament_free_text_third')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Steroide Medikament 3 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option bei Steroide Medikament.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
								
							}



							if ($this->input->post('steroide_therapieeflog')) {
								$success = true;
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}

							if ($this->input->post('steroide_gastrointestinal_input')) {
								$success = true;
								if ($this->input->post('steroide_gastrointestinal_input') == 'Freifeld') {
									
									if ($this->input->post('steroide_gastrointestinal_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Steroide Gastrointestinal ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('steroide_kardial_input')) {
								$success = true;
								if ($this->input->post('steroide_kardial_input') == 'Freifeld') {
									
									if ($this->input->post('steroide_kardial_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Steroide Kardial ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('steroide_nephrologisch_input')) {
								$success = true;
								if ($this->input->post('steroide_nephrologisch_input') == 'Freifeld') {
									
									if ($this->input->post('steroide_nephrologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Steroide Nephrologisch  ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('steroide_neurologisch_input')) {
								$success = true;
								if ($this->input->post('steroide_neurologisch_input') == 'Freifeld') {
									
									if ($this->input->post('steroide_neurologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Steroide Neurologisch/psychotrop ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}								
							}
						}

						if ($this->input->post('neuroleptika')  == 'Neuroleptika') {
						
							if ($this->input->post('neuroleptika_input_1') || $this->input->post('neuroleptika_input_2') || $this->input->post('neuroleptika_input_3')) {
								$success = true;
								if ($this->input->post('neuroleptika_input_1') == 'Freifeld') {
									
									if ($this->input->post('neuroleptika_medikament_free_text_first')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Neuroleptika Medikament 1 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('neuroleptika_input_2') == 'Freifeld') {
									
									if ($this->input->post('neuroleptika_medikament_free_text_second')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Neuroleptika Medikament 2 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('neuroleptika_input_3') == 'Freifeld') {
									
									if ($this->input->post('neuroleptika_medikament_free_text_third')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Neuroleptika Medikament 3 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option bei Neuroleptika Medikament.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
								
							}

							if ($this->input->post('neuroleptika_therapieeflog')) {
								$success = true;
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}

							if ($this->input->post('neuroleptika_gastrointestinal_input')) {
								$success = true;
								if ($this->input->post('neuroleptika_gastrointestinal_input') == 'Freifeld') {
									
									if ($this->input->post('neuroleptika_gastrointestinal_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Neuroleptika Gastrointestinal ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('neuroleptika_kardial_input')) {
								$success = true;
								if ($this->input->post('neuroleptika_kardial_input') == 'Freifeld') {
									
									if ($this->input->post('neuroleptika_kardial_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Neuroleptika Kardial ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('neuroleptika_nephrologisch_input')) {
								$success = true;
								if ($this->input->post('neuroleptika_nephrologisch_input') == 'Freifeld') {
									
									if ($this->input->post('neuroleptika_nephrologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Neuroleptika Nephrologisch  ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('neuroleptika_neurologisch_input')) {
								$success = true;
								if ($this->input->post('neuroleptika_neurologisch_input') == 'Freifeld') {
									
									if ($this->input->post('neuroleptika_neurologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Neuroleptika Neurologisch/psychotrop ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}								
							}
						}

						if ($this->input->post('antidepressiva')  == 'Antidepressiva') {
						
							if ($this->input->post('antidepressiva_input_1') || $this->input->post('antidepressiva_input_2') || $this->input->post('antidepressiva_input_3')) {
								$success = true;
								if ($this->input->post('antidepressiva_input_1') == 'Freifeld') {
									
									if ($this->input->post('antidepressiva_medikament_free_text_first')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antidepressiva Medikament 1 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('antidepressiva_input_2') == 'Freifeld') {
									
									if ($this->input->post('antidepressiva_medikament_free_text_second')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antidepressiva Medikament 2 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('antidepressiva_input_3') == 'Freifeld') {
									
									if ($this->input->post('antidepressiva_medikament_free_text_third')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antidepressiva Medikament 3 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option bei Antidepressiva Medikament.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
								
							}

							if ($this->input->post('antidepressiva_therapieeflog')) {
								$success = true;
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}

							if ($this->input->post('antidepressiva_gastrointestinal_input')) {
								$success = true;
								if ($this->input->post('antidepressiva_gastrointestinal_input') == 'Freifeld') {
									
									if ($this->input->post('antidepressiva_gastrointestinal_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antidepressiva Gastrointestinal ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('antidepressiva_kardial_input')) {
								$success = true;
								if ($this->input->post('antidepressiva_kardial_input') == 'Freifeld') {
									
									if ($this->input->post('antidepressiva_kardial_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antidepressiva Kardial ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('antidepressiva_nephrologisch_input')) {
								$success = true;
								if ($this->input->post('antidepressiva_nephrologisch_input') == 'Freifeld') {
									
									if ($this->input->post('antidepressiva_nephrologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antidepressiva Nephrologisch  ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('antidepressiva_neurologisch_input')) {
								$success = true;
								if ($this->input->post('antidepressiva_neurologisch_input') == 'Freifeld') {
									
									if ($this->input->post('antidepressiva_neurologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antidepressiva Neurologisch/psychotrop ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}
						}

						if ($this->input->post('sedativa')  == 'Sedativa') {
						
							if ($this->input->post('sedativa_input_1') || $this->input->post('sedativa_input_2') || $this->input->post('sedativa_input_3')) {
								$success = true;
								if ($this->input->post('sedativa_input_1') == 'Freifeld') {
									
									if ($this->input->post('sedativa_medikament_free_text_first')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Sedativa Medikament 1 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('sedativa_input_2') == 'Freifeld') {
									
									if ($this->input->post('sedativa_medikament_free_text_second')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Sedativa Medikament 2 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('sedativa_input_3') == 'Freifeld') {
									
									if ($this->input->post('sedativa_medikament_free_text_third')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Sedativa Medikament 3 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option bei Sedativa Medikament 1.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
								
							}

							if ($this->input->post('sedativa_therapieeflog')) {
								$success = true;
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}

							if ($this->input->post('sedativa_gastrointestinal_input')) {
								$success = true;
								if ($this->input->post('sedativa_gastrointestinal_input') == 'Freifeld') {
									
									if ($this->input->post('sedativa_gastrointestinal_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Sedativa Gastrointestinal ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('sedativa_kardial_input')) {
								$success = true;
								if ($this->input->post('sedativa_kardial_input') == 'Freifeld') {
									
									if ($this->input->post('sedativa_kardial_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Sedativa Kardial ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('sedativa_nephrologisch_input')) {
								$success = true;
								if ($this->input->post('sedativa_nephrologisch_input') == 'Freifeld') {
									
									if ($this->input->post('sedativa_nephrologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Sedativa Nephrologisch  ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('sedativa_neurologisch_input')) {
								$success = true;
								if ($this->input->post('sedativa_neurologisch_input') == 'Freifeld') {
									
									if ($this->input->post('sedativa_neurologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Sedativa Neurologisch/psychotrop ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}
						}

						if ($this->input->post('antikonvulsiva')  == 'Antikonvulsiva') {
						
							if ($this->input->post('antikonvulsiva_input_1') || $this->input->post('antikonvulsiva_input_2') || $this->input->post('antikonvulsiva_input_3')) {
								$success = true;
								if ($this->input->post('antikonvulsiva_input_1') == 'Freifeld') {
									
									if ($this->input->post('antikonvulsiva_medikament_free_text_first')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antikonvulsiva Medikament 1 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('antikonvulsiva_input_2') == 'Freifeld') {
									
									if ($this->input->post('antikonvulsiva_medikament_free_text_second')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antikonvulsiva Medikament 2 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('antikonvulsiva_input_3') == 'Freifeld') {
									
									if ($this->input->post('antikonvulsiva_medikament_free_text_third')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antikonvulsiva Medikament 3 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option bei Antikonvulsiva Medikament 1.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
								
							}

							if ($this->input->post('antikonvulsiva_therapieeflog')) {
								$success = true;
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}

							if ($this->input->post('antikonvulsiva_gastrointestinal_input')) {
								$success = true;
								if ($this->input->post('antikonvulsiva_gastrointestinal_input') == 'Freifeld') {
									
									if ($this->input->post('antikonvulsiva_gastrointestinal_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antikonvulsiva Gastrointestinal ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('antikonvulsiva_kardial_input')) {
								$success = true;
								if ($this->input->post('antikonvulsiva_kardial_input') == 'Freifeld') {
									
									if ($this->input->post('antikonvulsiva_kardial_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antikonvulsiva Kardial ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('antikonvulsiva_nephrologisch_input')) {
								$success = true;
								if ($this->input->post('antikonvulsiva_nephrologisch_input') == 'Freifeld') {
									
									if ($this->input->post('antikonvulsiva_nephrologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antikonvulsiva Nephrologisch  ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('antikonvulsiva_neurologisch_input')) {
								$success = true;
								if ($this->input->post('antikonvulsiva_neurologisch_input') == 'Freifeld') {
									
									if ($this->input->post('antikonvulsiva_neurologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antikonvulsiva Neurologisch/psychotrop ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}
						}

						if ($this->input->post('antiemetika')  == 'Antiemetika') {
						
							if ($this->input->post('antiemetika_input_1') || $this->input->post('antiemetika_input_2') || $this->input->post('antiemetika_input_3')) {
								$success = true;
								if ($this->input->post('antiemetika_input_1') == 'Freifeld') {
									
									if ($this->input->post('antiemetika_medikament_free_text_first')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antiemetika Medikament 1 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('antiemetika_input_2') == 'Freifeld') {
									
									if ($this->input->post('antiemetika_medikament_free_text_second')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antiemetika Medikament 2 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('antiemetika_input_3') == 'Freifeld') {
									
									if ($this->input->post('antiemetika_medikament_free_text_third')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antiemetika Medikament 3 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option bei Antiemetika Medikament.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
								
							}

							if ($this->input->post('antiemetika_therapieeflog')) {
								$success = true;
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}

							if ($this->input->post('antiemetika_gastrointestinal_input')) {
								$success = true;
								if ($this->input->post('antiemetika_gastrointestinal_input') == 'Freifeld') {
									
									if ($this->input->post('antiemetika_gastrointestinal_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antiemetika Gastrointestinal ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('antiemetika_kardial_input')) {
								$success = true;
								if ($this->input->post('antiemetika_kardial_input') == 'Freifeld') {
									
									if ($this->input->post('antiemetika_kardial_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antiemetika Kardial ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('antiemetika_nephrologisch_input')) {
								$success = true;
								if ($this->input->post('antiemetika_nephrologisch_input') == 'Freifeld') {
									
									if ($this->input->post('antiemetika_nephrologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antiemetika Nephrologisch  ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('antiemetika_neurologisch_input')) {
								$success = true;
								if ($this->input->post('antiemetika_neurologisch_input') == 'Freifeld') {
									
									if ($this->input->post('antiemetika_neurologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Antiemetika Neurologisch/psychotrop ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}
						}					
					}

					if ($this->input->post('who_stufe2') == 'Yes') {

						if ($this->input->post('schwach_wirksame_opioide')) {
							$success = true;
						} else {
							$message = 'Select option from WHO Stufe 2';
							$success = false;
							$data['success'] = $success;
							$data['message'] = $message;
							json_output($data);
						}

						if ($this->input->post('schwach_wirksame_opioide')  == 'Schwach wirksame Opioide') {
						
							if ($this->input->post('schwach_input_1') || $this->input->post('schwach_input_2') || $this->input->post('schwach_input_3')) {
								$success = true;
								if ($this->input->post('schwach_input_1') == 'Freifeld') {
									
									if ($this->input->post('schwach_medikament_free_text_first')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Schwach wirksame Opioide Medikament 1 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('schwach_input_2') == 'Freifeld') {
									
									if ($this->input->post('schwach_medikament_free_text_second')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Schwach wirksame Opioide Medikament 2 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('schwach_input_3') == 'Freifeld') {
									
									if ($this->input->post('schwach_medikament_free_text_third')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Schwach wirksame Opioide Medikament 3 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option bei Schwach wirksame Opioide Medikament.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;								
							}

							if ($this->input->post('schwach_therapieeflog')) {
								$success = true;
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}

							if ($this->input->post('schwach_gastrointestinal_input')) {
								$success = true;
								if ($this->input->post('schwach_gastrointestinal_input') == 'Freifeld') {
									
									if ($this->input->post('schwach_gastrointestinal_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Schwach wirksame Opioide Gastrointestinal ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('schwach_kardial_input')) {
								$success = true;
								if ($this->input->post('schwach_kardial_input') == 'Freifeld') {
									
									if ($this->input->post('schwach_kardial_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Schwach wirksame Opioide Kardial ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('schwach_nephrologisch_input')) {
								$success = true;
								if ($this->input->post('schwach_nephrologisch_input') == 'Freifeld') {
									
									if ($this->input->post('schwach_nephrologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Schwach wirksame Opioide Nephrologisch  ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('schwach_neurologisch_input')) {
								$success = true;
								if ($this->input->post('schwach_neurologisch_input') == 'Freifeld') {
									
									if ($this->input->post('schwach_neurologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Schwach wirksame Neurologisch/psychotrop ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}
						}
					}

					if ($this->input->post('who_stufe3') == 'Yes') {

						if ($this->input->post('stark_wirksame_opioide')) {
							$success = true;
						} else {
							$message = 'Select option from WHO Stufe 3';
							$success = false;
							$data['success'] = $success;
							$data['message'] = $message;
							json_output($data);
						}

						if ($this->input->post('stark_wirksame_opioide')  == 'Stark wirksame Opioide') {
						
							if ($this->input->post('stark_input_1') || $this->input->post('stark_input_2') || $this->input->post('stark_input_3')) {
								$success = true;
								if ($this->input->post('stark_input_1') == 'Freifeld') {
									
									if ($this->input->post('stark_medikament_free_text_first')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Stark wirksame Opioide Medikament 1 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('stark_input_2') == 'Freifeld') {
									
									if ($this->input->post('stark_medikament_free_text_second')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Stark wirksame Opioide Medikament 2 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('stark_input_3') == 'Freifeld') {
									
									if ($this->input->post('stark_medikament_free_text_third')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Stark wirksame Opioide Medikament 3 ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option bei Stark wirksame Opioide Medikament.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
								
							}

							if ($this->input->post('stark_therapieeflog')) {
								$success = true;
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}

							if ($this->input->post('stark_gastrointestinal_input')) {
								$success = true;
								if ($this->input->post('stark_gastrointestinal_input') == 'Freifeld') {
									
									if ($this->input->post('stark_gastrointestinal_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Stark wirksame Opioide Gastrointestinal ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('stark_kardial_input')) {
								$success = true;
								if ($this->input->post('stark_kardial_input') == 'Freifeld') {
									
									if ($this->input->post('stark_kardial_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Stark wirksame Opioide Kardial ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('stark_nephrologisch_input')) {
								$success = true;
								if ($this->input->post('stark_nephrologisch_input') == 'Freifeld') {
									
									if ($this->input->post('stark_nephrologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Stark wirksame Opioide Nephrologisch ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}

							if ($this->input->post('stark_neurologisch_input')) {
								$success = true;
								if ($this->input->post('stark_neurologisch_input') == 'Freifeld') {
									
									if ($this->input->post('stark_neurologisch_text')) {
										$success = true;
									} else {
										$message = 'Bitte geben Sie einen Namen bei Stark wirksame Opioide Neurologisch/psychotrop ein.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							}
						}
					}

					if ($this->input->post('spasmolytika') == 'Yes') {
	
						if ($this->input->post('spasmolytika_input_1') || $this->input->post('spasmolytika_input_2') || $this->input->post('spasmolytika_input_3')) {
							$success = true;
							if ($this->input->post('spasmolytika_input_1') == 'Freifeld') {
								
								if ($this->input->post('spasmolytika_medikament_free_text_first')) {
									$success = true;
								} else {
									$message = 'Bitte geben Sie einen Namen bei Spasmolytika Medikament 1 ein.';
									$success = false;
									$data['success'] = $success;
									$data['message'] = $message;
									json_output($data); die;
								}
							}

							if ($this->input->post('spasmolytika_input_2') == 'Freifeld') {
								
								if ($this->input->post('spasmolytika_medikament_free_text_second')) {
									$success = true;
								} else {
									$message = 'Bitte geben Sie einen Namen bei Spasmolytika Medikament 2 ein.';
									$success = false;
									$data['success'] = $success;
									$data['message'] = $message;
									json_output($data); die;
								}
							}

							if ($this->input->post('spasmolytika_input_3') == 'Freifeld') {
								
								if ($this->input->post('spasmolytika_medikament_free_text_third')) {
									$success = true;
								} else {
									$message = 'Bitte geben Sie einen Namen bei Spasmolytika Medikament 3 ein.';
									$success = false;
									$data['success'] = $success;
									$data['message'] = $message;
									json_output($data); die;
								}
							}
							
						} else {
							$message = 'Bitte wählen Sie eine Option bei Spasmolytika Medikament.';
							$success = false;
							$data['success'] = $success;
							$data['message'] = $message;
							json_output($data); die;
							
						}

						if ($this->input->post('spasmolytika_therapieeflog')) {
							$success = true;
						} else {
							$message = 'Bitte wählen Sie eine Option.';
							$success = false;
							$data['success'] = $success;
							$data['message'] = $message;
							json_output($data); die;
						}

						if ($this->input->post('spasmolytika_gastrointestinal_input')) {
							$success = true;
							if ($this->input->post('spasmolytika_gastrointestinal_input') == 'Freifeld') {
								
								if ($this->input->post('spasmolytika_gastrointestinal_text')) {
									$success = true;
								} else {
									$message = 'Bitte geben Sie einen Namen bei Spasmolytika Gastrointestinal ein.';
									$success = false;
									$data['success'] = $success;
									$data['message'] = $message;
									json_output($data); die;
								}
							}
							
						}

						if ($this->input->post('spasmolytika_kardial_input')) {
							$success = true;
							if ($this->input->post('spasmolytika_kardial_input') == 'Freifeld') {
								
								if ($this->input->post('spasmolytika_kardial_text')) {
									$success = true;
								} else {
									$message = 'Bitte geben Sie einen Namen bei Spasmolytika Kardial ein.';
									$success = false;
									$data['success'] = $success;
									$data['message'] = $message;
									json_output($data); die;
								}
							}
							
						}

						if ($this->input->post('spasmolytika_nephrologisch_input')) {
							$success = true;
							if ($this->input->post('spasmolytika_nephrologisch_input') == 'Freifeld') {
								
								if ($this->input->post('spasmolytika_nephrologisch_text')) {
									$success = true;
								} else {
									$message = 'Bitte geben Sie einen Namen bei Spasmolytika Nephrologisch  ein.';
									$success = false;
									$data['success'] = $success;
									$data['message'] = $message;
									json_output($data); die;
								}
							}
							
						}

						if ($this->input->post('spasmolytika_neurologisch_input')) {
							$success = true;
							if ($this->input->post('spasmolytika_neurologisch_input') == 'Freifeld') {
								
								if ($this->input->post('spasmolytika_neurologisch_text')) {
									$success = true;
								} else {
									$message = 'Bitte geben Sie einen Namen bei Spasmolytika Neurologisch/psychotrop ein.';
									$success = false;
									$data['success'] = $success;
									$data['message'] = $message;
									json_output($data); die;
								}
							}
							
						}
					}

					if ($success == true) {						

						$input['web_patient_question_id'] = $question_data_exists->id;
						$input['physiotherapie_1'] = $this->input->post('physiotherapie_1');
						$input['interventionelle_verfahren'] = $this->input->post('interventionelle_verfahren');
						$input['psychotherapie_2'] = $this->input->post('psychotherapie_2');
						$input['tens'] = $this->input->post('tens');
						$input['akupunktur'] = $this->input->post('akupunktur');
						$input['keine'] = $this->input->post('keine');
						$input['schmerztherapie_durchgefuhrt'] = $this->input->post('schmerztherapie_durchgefuhrt');
						$input['multimodale_schmerztherapie'] = $this->input->post('multimodale_schmerztherapie');
						$input['multimodale_schmerztherapie_text'] = $this->input->post('multimodale_schmerztherapie_text');
						$input['add_date'] = getDefaultToGMTDate(time());
						//pr($input); die;

						$input_question_5['web_patient_question_id'] = $question_data_exists->id;
						$who_stufe1 = $this->input->post('who_stufe1');
						$who_stufe2 = $this->input->post('who_stufe2');
						$who_stufe3 = $this->input->post('who_stufe3');
						$spasmolytika = $this->input->post('spasmolytika');
						//$this->arztfragebogen->clearwhodata($question_2_recorded_answers->web_patient_question_id);
						$this->arztfragebogen->deleteWebQuestion_5($question_data_exists->id);
						$question_data_added = $this->arztfragebogen->addWebQuestion_5($input_question_5);

						if ($question_data_added) {
							//$this->arztfragebogen->deletewho1data($question_2_recorded_answers->id);
							//$this->arztfragebogen->deletewho2data($question_2_recorded_answers->id);
							//$this->arztfragebogen->deletewho3data($question_2_recorded_answers->id);
							//$this->arztfragebogen->deletespasmolytikadata($question_2_recorded_answers->id);

							if (isset($who_stufe1) && $who_stufe1 == 'Yes') {

								$input_question_5['who_stufe_id'] = $who_stufe1;
								$nicht_opioidanalgetika = $this->input->post('nicht_opioidanalgetika');
								$steroide = $this->input->post('steroide');
								$neuroleptika = $this->input->post('neuroleptika');
								$antidepressiva = $this->input->post('antidepressiva');
								$sedativa = $this->input->post('sedativa');
								$antikonvulsiva = $this->input->post('antikonvulsiva');
								$antiemetika = $this->input->post('antiemetika');


								if (isset($nicht_opioidanalgetika) && $nicht_opioidanalgetika == 'Nicht-Opioidanalgetika') {
									
									$nichtOpioidanalgetikaSaveData['who_stufe_id'] = $question_data_added;	
									$nichtOpioidanalgetikaSaveData['option_name'] = $nicht_opioidanalgetika;
								
									$nichtOpioidanalgetikaSaveData['medikament_dropdown_1'] = $this->input->post('nicht_opioidanalgetika_input_1');
									$nichtOpioidanalgetikaSaveData['medikament_dropdown_1_text_field'] = $this->input->post('nicht_medikament_free_text_first');
									$nichtOpioidanalgetikaSaveData['medikament_dropdown_2'] = $this->input->post('nicht_opioidanalgetika_input_2');
									$nichtOpioidanalgetikaSaveData['medikament_dropdown_2_text_field'] = $this->input->post('nicht_medikament_free_text_second');
									$nichtOpioidanalgetikaSaveData['medikament_dropdown_3'] = $this->input->post('nicht_opioidanalgetika_input_3');
									$nichtOpioidanalgetikaSaveData['medikament_dropdown_3_text_field'] = $this->input->post('nicht_medikament_free_text_third');

									$nichtOpioidanalgetikaSaveData['therapieerfolg'] = $this->input->post('nicht_therapieeflog');
									$nichtOpioidanalgetikaSaveData['gastrointestinal_dropdown'] = $this->input->post('nicht_gastrointestinal_input');
									$nichtOpioidanalgetikaSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('nicht_gastrointestinal_text');
									$nichtOpioidanalgetikaSaveData['kardial_dropdown'] = $this->input->post('nicht_kardial_input');
									$nichtOpioidanalgetikaSaveData['kardial_dropdown_text_field'] = $this->input->post('nicht_kardial_text');
									$nichtOpioidanalgetikaSaveData['nephrologisch_dropdown'] = $this->input->post('nicht_nephrologisch_input');
									$nichtOpioidanalgetikaSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('nicht_nephrologisch_text');
									$nichtOpioidanalgetikaSaveData['neurologisch_dropdown'] = $this->input->post('nicht_neurologisch_input');
									$nichtOpioidanalgetikaSaveData['neurologisch_dropdown_text_field'] = $this->input->post('nicht_neurologisch_text');

									$this->arztfragebogen->addWho1fields($nichtOpioidanalgetikaSaveData);	
									
								}
								if (isset($steroide) && $steroide == 'Steroide') {
									$steroideSaveData['who_stufe_id'] = $question_data_added;
									$steroideSaveData['option_name'] = $steroide;
							
									$steroideSaveData['medikament_dropdown_1'] = $this->input->post('steroide_input_1');
									$steroideSaveData['medikament_dropdown_1_text_field'] = $this->input->post('steroide_medikament_free_text_first');
									$steroideSaveData['medikament_dropdown_2'] = $this->input->post('steroide_input_2');
									$steroideSaveData['medikament_dropdown_2_text_field'] = $this->input->post('steroide_medikament_free_text_second');
									$steroideSaveData['medikament_dropdown_3'] = $this->input->post('steroide_input_3');
									$steroideSaveData['medikament_dropdown_3_text_field'] = $this->input->post('steroide_medikament_free_text_third');

									$steroideSaveData['therapieerfolg'] = $this->input->post('steroide_therapieeflog');
									$steroideSaveData['gastrointestinal_dropdown'] = $this->input->post('steroide_gastrointestinal_input');
									$steroideSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('steroide_gastrointestinal_text');
									$steroideSaveData['kardial_dropdown'] = $this->input->post('steroide_kardial_input');
									$steroideSaveData['kardial_dropdown_text_field'] = $this->input->post('steroide_kardial_text');
									$steroideSaveData['nephrologisch_dropdown'] = $this->input->post('steroide_nephrologisch_input');
									$steroideSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('steroide_nephrologisch_text');
									$steroideSaveData['neurologisch_dropdown'] = $this->input->post('steroide_neurologisch_input');
									$steroideSaveData['neurologisch_dropdown_text_field'] = $this->input->post('steroide_neurologisch_text');

									$this->arztfragebogen->addWho1fields($steroideSaveData);

								}
								if (isset($neuroleptika) && $neuroleptika == 'Neuroleptika') {
									$neuroleptikaSaveData['who_stufe_id'] = $question_data_added;
									$neuroleptikaSaveData['option_name'] = $neuroleptika;
							
									$neuroleptikaSaveData['medikament_dropdown_1'] = $this->input->post('neuroleptika_input_1');
									$neuroleptikaSaveData['medikament_dropdown_1_text_field'] = $this->input->post('neuroleptika_medikament_free_text_first');
									$neuroleptikaSaveData['medikament_dropdown_2'] = $this->input->post('neuroleptika_input_2');
									$neuroleptikaSaveData['medikament_dropdown_2_text_field'] = $this->input->post('neuroleptika_medikament_free_text_second');
									$neuroleptikaSaveData['medikament_dropdown_3'] = $this->input->post('neuroleptika_input_3');
									$neuroleptikaSaveData['medikament_dropdown_3_text_field'] = $this->input->post('neuroleptika_medikament_free_text_third');

									$neuroleptikaSaveData['therapieerfolg'] = $this->input->post('neuroleptika_therapieeflog');
									$neuroleptikaSaveData['gastrointestinal_dropdown'] = $this->input->post('neuroleptika_gastrointestinal_input');
									$neuroleptikaSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('neuroleptika_gastrointestinal_text');
									$neuroleptikaSaveData['kardial_dropdown'] = $this->input->post('neuroleptika_kardial_input');
									$neuroleptikaSaveData['kardial_dropdown_text_field'] = $this->input->post('neuroleptika_kardial_text');
									$neuroleptikaSaveData['nephrologisch_dropdown'] = $this->input->post('neuroleptika_nephrologisch_input');
									$neuroleptikaSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('neuroleptika_nephrologisch_text');
									$neuroleptikaSaveData['neurologisch_dropdown'] = $this->input->post('neuroleptika_neurologisch_input');
									$neuroleptikaSaveData['neurologisch_dropdown_text_field'] = $this->input->post('neuroleptika_neurologisch_text');

									$this->arztfragebogen->addWho1fields($neuroleptikaSaveData);

								}
								if (isset($antidepressiva) && $antidepressiva == 'Antidepressiva') {
									$antidepressivaSaveData['who_stufe_id'] = $question_data_added;
									$antidepressivaSaveData['option_name'] = $antidepressiva;
							
									$antidepressivaSaveData['medikament_dropdown_1'] = $this->input->post('antidepressiva_input_1');
									$antidepressivaSaveData['medikament_dropdown_1_text_field'] = $this->input->post('antidepressiva_medikament_free_text_first');
									$antidepressivaSaveData['medikament_dropdown_2'] = $this->input->post('antidepressiva_input_2');
									$antidepressivaSaveData['medikament_dropdown_2_text_field'] = $this->input->post('antidepressiva_medikament_free_text_second');
									$antidepressivaSaveData['medikament_dropdown_3'] = $this->input->post('antidepressiva_input_3');
									$antidepressivaSaveData['medikament_dropdown_3_text_field'] = $this->input->post('antidepressiva_medikament_free_text_third');

									$antidepressivaSaveData['therapieerfolg'] = $this->input->post('antidepressiva_therapieeflog');
									$antidepressivaSaveData['gastrointestinal_dropdown'] = $this->input->post('antidepressiva_gastrointestinal_input');
									$antidepressivaSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('antidepressiva_gastrointestinal_text');
									$antidepressivaSaveData['kardial_dropdown'] = $this->input->post('antidepressiva_kardial_input');
									$antidepressivaSaveData['kardial_dropdown_text_field'] = $this->input->post('antidepressiva_kardial_text');
									$antidepressivaSaveData['nephrologisch_dropdown'] = $this->input->post('antidepressiva_nephrologisch_input');
									$antidepressivaSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('antidepressiva_nephrologisch_text');
									$antidepressivaSaveData['neurologisch_dropdown'] = $this->input->post('antidepressiva_neurologisch_input');
									$antidepressivaSaveData['neurologisch_dropdown_text_field'] = $this->input->post('antidepressiva_neurologisch_text');

									$this->arztfragebogen->addWho1fields($antidepressivaSaveData);
									
								}
								if (isset($sedativa) && $sedativa == 'Sedativa') {
									$sedativaSaveData['who_stufe_id'] = $question_data_added;
									$sedativaSaveData['option_name'] = $sedativa;
							
									$sedativaSaveData['medikament_dropdown_1'] = $this->input->post('sedativa_input_1');
									$sedativaSaveData['medikament_dropdown_1_text_field'] = $this->input->post('sedativa_medikament_free_text_first');
									$sedativaSaveData['medikament_dropdown_2'] = $this->input->post('sedativa_input_2');
									$sedativaSaveData['medikament_dropdown_2_text_field'] = $this->input->post('sedativa_medikament_free_text_second');
									$sedativaSaveData['medikament_dropdown_3'] = $this->input->post('sedativa_input_3');
									$sedativaSaveData['medikament_dropdown_3_text_field'] = $this->input->post('sedativa_medikament_free_text_third');

									$sedativaSaveData['therapieerfolg'] = $this->input->post('sedativa_therapieeflog');
									$sedativaSaveData['gastrointestinal_dropdown'] = $this->input->post('sedativa_gastrointestinal_input');
									$sedativaSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('sedativa_gastrointestinal_text');
									$sedativaSaveData['kardial_dropdown'] = $this->input->post('sedativa_kardial_input');
									$sedativaSaveData['kardial_dropdown_text_field'] = $this->input->post('sedativa_kardial_text');
									$sedativaSaveData['nephrologisch_dropdown'] = $this->input->post('sedativa_nephrologisch_input');
									$sedativaSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('sedativa_nephrologisch_text');
									$sedativaSaveData['neurologisch_dropdown'] = $this->input->post('sedativa_neurologisch_input');
									$sedativaSaveData['neurologisch_dropdown_text_field'] = $this->input->post('sedativa_neurologisch_text');

									$this->arztfragebogen->addWho1fields($sedativaSaveData);
									
								}
								if (isset($antikonvulsiva) && $antikonvulsiva == 'Antikonvulsiva') {
									$antikonvulsivaSaveData['who_stufe_id'] = $question_data_added;
									$antikonvulsivaSaveData['option_name'] = $antikonvulsiva;
							
									$antikonvulsivaSaveData['medikament_dropdown_1'] = $this->input->post('antikonvulsiva_input_1');
									$antikonvulsivaSaveData['medikament_dropdown_1_text_field'] = $this->input->post('antikonvulsiva_medikament_free_text_first');
									$antikonvulsivaSaveData['medikament_dropdown_2'] = $this->input->post('antikonvulsiva_input_2');
									$antikonvulsivaSaveData['medikament_dropdown_2_text_field'] = $this->input->post('antikonvulsiva_medikament_free_text_second');
									$antikonvulsivaSaveData['medikament_dropdown_3'] = $this->input->post('antikonvulsiva_input_3');
									$antikonvulsivaSaveData['medikament_dropdown_3_text_field'] = $this->input->post('antikonvulsiva_medikament_free_text_third');

									$antikonvulsivaSaveData['therapieerfolg'] = $this->input->post('antikonvulsiva_therapieeflog');
									$antikonvulsivaSaveData['gastrointestinal_dropdown'] = $this->input->post('antikonvulsiva_gastrointestinal_input');
									$antikonvulsivaSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('antikonvulsiva_gastrointestinal_text');
									$antikonvulsivaSaveData['kardial_dropdown'] = $this->input->post('antikonvulsiva_kardial_input');
									$antikonvulsivaSaveData['kardial_dropdown_text_field'] = $this->input->post('antikonvulsiva_kardial_text');
									$antikonvulsivaSaveData['nephrologisch_dropdown'] = $this->input->post('antikonvulsiva_nephrologisch_input');
									$antikonvulsivaSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('antikonvulsiva_nephrologisch_text');
									$antikonvulsivaSaveData['neurologisch_dropdown'] = $this->input->post('antikonvulsiva_neurologisch_input');
									$antikonvulsivaSaveData['neurologisch_dropdown_text_field'] = $this->input->post('antikonvulsiva_neurologisch_text');
									
									$this->arztfragebogen->addWho1fields($antikonvulsivaSaveData);
								}
								if (isset($antiemetika) && $antiemetika == 'Antiemetika') {
									$antiemetikaSaveData['who_stufe_id'] = $question_data_added;
									$antiemetikaSaveData['option_name'] = $antiemetika;
							
									$antiemetikaSaveData['medikament_dropdown_1'] = $this->input->post('antiemetika_input_1');
									$antiemetikaSaveData['medikament_dropdown_1_text_field'] = $this->input->post('antiemetika_medikament_free_text_first');
									$antiemetikaSaveData['medikament_dropdown_2'] = $this->input->post('antiemetika_input_2');
									$antiemetikaSaveData['medikament_dropdown_2_text_field'] = $this->input->post('antiemetika_medikament_free_text_second');
									$antiemetikaSaveData['medikament_dropdown_3'] = $this->input->post('antiemetika_input_3');
									$antiemetikaSaveData['medikament_dropdown_3_text_field'] = $this->input->post('antiemetika_medikament_free_text_third');

									$antiemetikaSaveData['therapieerfolg'] = $this->input->post('antiemetika_therapieeflog');
									$antiemetikaSaveData['gastrointestinal_dropdown'] = $this->input->post('antiemetika_gastrointestinal_input');
									$antiemetikaSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('antiemetika_gastrointestinal_text');
									$antiemetikaSaveData['kardial_dropdown'] = $this->input->post('antiemetika_kardial_input');
									$antiemetikaSaveData['kardial_dropdown_text_field'] = $this->input->post('antiemetika_kardial_text');
									$antiemetikaSaveData['nephrologisch_dropdown'] = $this->input->post('antiemetika_nephrologisch_input');
									$antiemetikaSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('antiemetika_nephrologisch_text');
									$antiemetikaSaveData['neurologisch_dropdown'] = $this->input->post('antiemetika_neurologisch_input');
									$antiemetikaSaveData['neurologisch_dropdown_text_field'] = $this->input->post('antiemetika_neurologisch_text');

									$this->arztfragebogen->addWho1fields($antiemetikaSaveData);
									
								}
							}
							if (isset($who_stufe2) && $who_stufe2 == 'Yes'){
								
								$input_question_5['who_stufe_id'] = $who_stufe2;
								$schwach_wirksame_opioide = $this->input->post('schwach_wirksame_opioide');

								if (isset($schwach_wirksame_opioide) && $schwach_wirksame_opioide == 'Schwach wirksame Opioide')
								{

									$schwachWirksameOpioideSaveData['who_stufe_id'] = $question_data_added;
									$schwachWirksameOpioideSaveData['option_name'] = $schwach_wirksame_opioide;
								
									$schwachWirksameOpioideSaveData['medikament_dropdown_1'] = $this->input->post('schwach_input_1');
									$schwachWirksameOpioideSaveData['medikament_dropdown_1_text_field'] = $this->input->post('schwach_medikament_free_text_first');
									$schwachWirksameOpioideSaveData['medikament_dropdown_2'] = $this->input->post('schwach_input_2');
									$schwachWirksameOpioideSaveData['medikament_dropdown_2_text_field'] = $this->input->post('schwach_medikament_free_text_second');
									$schwachWirksameOpioideSaveData['medikament_dropdown_3'] = $this->input->post('schwach_input_3');
									$schwachWirksameOpioideSaveData['medikament_dropdown_3_text_field'] = $this->input->post('schwach_medikament_free_text_third');

									$schwachWirksameOpioideSaveData['therapieerfolg'] = $this->input->post('schwach_therapieeflog');
									$schwachWirksameOpioideSaveData['gastrointestinal_dropdown'] = $this->input->post('schwach_gastrointestinal_input');
									$schwachWirksameOpioideSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('schwach_gastrointestinal_text');
									$schwachWirksameOpioideSaveData['kardial_dropdown'] = $this->input->post('schwach_kardial_input');
									$schwachWirksameOpioideSaveData['kardial_dropdown_text_field'] = $this->input->post('schwach_kardial_text');
									$schwachWirksameOpioideSaveData['nephrologisch_dropdown'] = $this->input->post('schwach_nephrologisch_input');
									$schwachWirksameOpioideSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('schwach_nephrologisch_text');
									$schwachWirksameOpioideSaveData['neurologisch_dropdown'] = $this->input->post('schwach_neurologisch_input');
									$schwachWirksameOpioideSaveData['neurologisch_dropdown_text_field'] = $this->input->post('schwach_neurologisch_text');	
									//pr($input_question_5); die('here');

									$response = $this->arztfragebogen->addWho2fields($schwachWirksameOpioideSaveData);					
									//pr($response); die;
								}
							}
							if (isset($who_stufe3) && $who_stufe3 == 'Yes'){

								$input_question_5['who_stufe_id'] = $who_stufe3;
								$stark_wirksame_opioide = $this->input->post('stark_wirksame_opioide');

								if (isset($stark_wirksame_opioide) && $stark_wirksame_opioide == 'Stark wirksame Opioide')
								{
									$starkWirksameOpioideSaveData['who_stufe_id'] = $question_data_added;
									$starkWirksameOpioideSaveData['option_name'] = $stark_wirksame_opioide;
								
									$starkWirksameOpioideSaveData['medikament_dropdown_1'] = $this->input->post('stark_input_1');
									$starkWirksameOpioideSaveData['medikament_dropdown_1_text_field'] = $this->input->post('stark_medikament_free_text_first');
									$starkWirksameOpioideSaveData['medikament_dropdown_2'] = $this->input->post('stark_input_2');
									$starkWirksameOpioideSaveData['medikament_dropdown_2_text_field'] = $this->input->post('stark_medikament_free_text_second');
									$starkWirksameOpioideSaveData['medikament_dropdown_3'] = $this->input->post('stark_input_3');
									$starkWirksameOpioideSaveData['medikament_dropdown_3_text_field'] = $this->input->post('stark_medikament_free_text_third');

									$starkWirksameOpioideSaveData['therapieerfolg'] = $this->input->post('stark_therapieeflog');
									$starkWirksameOpioideSaveData['gastrointestinal_dropdown'] = $this->input->post('stark_gastrointestinal_input');
									$starkWirksameOpioideSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('stark_gastrointestinal_text');
									$starkWirksameOpioideSaveData['kardial_dropdown'] = $this->input->post('stark_kardial_input');
									$starkWirksameOpioideSaveData['kardial_dropdown_text_field'] = $this->input->post('stark_kardial_text');
									$starkWirksameOpioideSaveData['nephrologisch_dropdown'] = $this->input->post('stark_nephrologisch_input');
									$starkWirksameOpioideSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('stark_nephrologisch_text');
									$starkWirksameOpioideSaveData['neurologisch_dropdown'] = $this->input->post('stark_neurologisch_input');
									$starkWirksameOpioideSaveData['neurologisch_dropdown_text_field'] = $this->input->post('stark_neurologisch_text');	
									//pr($starkWirksameOpioideSaveData); die('here');

									$response = $this->arztfragebogen->addWho3fields($starkWirksameOpioideSaveData);					

								}
							}
							if (isset($spasmolytika) && $spasmolytika == 'Yes'){
								$spasmolytikaSaveData['who_stufe_id'] = $question_data_added;
								
								$spasmolytikaSaveData['option_name'] = 'Spasmolytika';
							
								$spasmolytikaSaveData['medikament_dropdown_1'] = $this->input->post('spasmolytika_input_1');
								$spasmolytikaSaveData['medikament_dropdown_1_text_field'] = $this->input->post('spasmolytika_medikament_free_text_first');
								$spasmolytikaSaveData['medikament_dropdown_2'] = $this->input->post('spasmolytika_input_2');
								$spasmolytikaSaveData['medikament_dropdown_2_text_field'] = $this->input->post('spasmolytika_medikament_free_text_second');
								$spasmolytikaSaveData['medikament_dropdown_3'] = $this->input->post('spasmolytika_input_3');
								$spasmolytikaSaveData['medikament_dropdown_3_text_field'] = $this->input->post('spasmolytika_medikament_free_text_third');

								$spasmolytikaSaveData['therapieerfolg'] = $this->input->post('spasmolytika_therapieeflog');
								$spasmolytikaSaveData['gastrointestinal_dropdown'] = $this->input->post('spasmolytika_gastrointestinal_input');
								$spasmolytikaSaveData['gastrointestinal_dropdown_text_field'] = $this->input->post('spasmolytika_gastrointestinal_text');
								$spasmolytikaSaveData['kardial_dropdown'] = $this->input->post('spasmolytika_kardial_input');
								$spasmolytikaSaveData['kardial_dropdown_text_field'] = $this->input->post('spasmolytika_kardial_text');
								$spasmolytikaSaveData['nephrologisch_dropdown'] = $this->input->post('spasmolytika_nephrologisch_input');
								$spasmolytikaSaveData['nephrologisch_dropdown_text_field'] = $this->input->post('spasmolytika_nephrologisch_text');
								$spasmolytikaSaveData['neurologisch_dropdown'] = $this->input->post('spasmolytika_neurologisch_input');
								$spasmolytikaSaveData['neurologisch_dropdown_text_field'] = $this->input->post('spasmolytika_neurologisch_text');	
								//pr($input_question_5); die('here');		
								$this->arztfragebogen->addspasmolytikafields($spasmolytikaSaveData);			
								
							}
						} else {
							$success = false;
							$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
						}
						//pr($input_question_5); die;
						$this->arztfragebogen->deleteWebQuestion_4($question_data_exists->id);
						$question_4_data_added = $this->arztfragebogen->addWebQuestion_4($input);

						if ($question_4_data_added) {
							$success = true;
							$message = 'Der Fragebogen wurde erfolgreich gespeichert.';
							$data['redirectURL'] = base_url('kostenubernahmeantrag/step-5?in='.$insured_number.'&qid='.$question_added_id);
						} else {
							$success = false;
							$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
						}
					}

					$data['message'] = $message;
					$data['success'] = $success;
					json_output($data);
				}
				$recorded_answer = $this->arztfragebogen->get_forth_step_question_data($question_data_exists->id);
				//pr($recorded_answer); die;

				if ($recorded_answer) {
				
					$output['recorded_answer'] = $recorded_answer;
					$output['physiotherapie_1'] = $recorded_answer->physiotherapie_1;
					$output['interventionelle_verfahren'] = $recorded_answer->interventionelle_verfahren;
					$output['psychotherapie_2'] = $recorded_answer->psychotherapie_2;
					$output['tens'] = $recorded_answer->tens;
					$output['akupunktur'] = $recorded_answer->akupunktur;
					$output['keine'] = $recorded_answer->keine;
					$output['schmerztherapie_durchgefuhrt'] = $recorded_answer->schmerztherapie_durchgefuhrt;
					$output['multimodale_schmerztherapie'] = $recorded_answer->multimodale_schmerztherapie;
					$output['multimodale_schmerztherapie_text'] = $recorded_answer->multimodale_schmerztherapie_text;
				} else {

					$last_process_complete_question = $this->arztfragebogen->get_last_process_complete_answer($is_exists_patient->id, $doctor_id);

					if ($last_process_complete_question) {
						$recorded_answer = $this->arztfragebogen->get_forth_step_question_data($last_process_complete_question->id);
						
						if ($recorded_answer) {
				
							$output['recorded_answer'] = $recorded_answer;
							$output['physiotherapie_1'] = $recorded_answer->physiotherapie_1;
							$output['interventionelle_verfahren'] = $recorded_answer->interventionelle_verfahren;
							$output['psychotherapie_2'] = $recorded_answer->psychotherapie_2;
							$output['tens'] = $recorded_answer->tens;
							$output['akupunktur'] = $recorded_answer->akupunktur;
							$output['keine'] = $recorded_answer->keine;
							$output['schmerztherapie_durchgefuhrt'] = $recorded_answer->schmerztherapie_durchgefuhrt;
							$output['multimodale_schmerztherapie'] = $recorded_answer->multimodale_schmerztherapie;
							$output['multimodale_schmerztherapie_text'] = $recorded_answer->multimodale_schmerztherapie_text;
						} else {
							$output['recorded_answer'] = '';
							$output['physiotherapie_1'] = '';
							$output['interventionelle_verfahren'] = '';
							$output['psychotherapie_2'] = '';
							$output['tens'] = '';
							$output['akupunktur'] = '';
							$output['keine'] = '';
							$output['schmerztherapie_durchgefuhrt'] = '';
							$output['multimodale_schmerztherapie'] = '';
							$output['multimodale_schmerztherapie_text'] = '';
						}
					} else {
						$output['recorded_answer'] = '';
						$output['physiotherapie_1'] = '';
						$output['interventionelle_verfahren'] = '';
						$output['psychotherapie_2'] = '';
						$output['tens'] = '';
						$output['akupunktur'] = '';
						$output['keine'] = '';
						$output['schmerztherapie_durchgefuhrt'] = '';
						$output['multimodale_schmerztherapie'] = '';
						$output['multimodale_schmerztherapie_text'] = '';
					}
				}
				$this->load->view('front/includes/header', $output);
				$this->load->view('front/arztfragebogen/welche-bisherige-therapie');
				$this->load->view('front/includes/footer');
			} else {
				redirect('arztfragebogen/step-1?in='.$insured_number);
			}

		} else {
			redirect('patients');
		}

	}

	function arztfragebogen_step_5() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}
		/*pdf page no 17 */

		$output['header_menu'] = 'arztfragebogen';
		$doctor_id = $this->session->userdata('user_id');
		$insured_number = $this->input->get('in');
		$output['insured_number'] = $insured_number;
		$is_exists_patient = $this->patients->getPatientByInsuranceNumber($insured_number);
		$question_added_id = $this->input->get('qid');
		$success = true;
		$message = '';
		
		$output['qid'] = $question_added_id;

		if ($is_exists_patient) {
			$question_data_exists = $this->arztfragebogen->checkQuestionStartDataExistsById($question_added_id);

			if ($question_data_exists) {

				if ($this->input->post()) {
					
					$this->form_validation->set_rules('kontraindikation','Ist eine Kontraindikation bei der Verwendung von CannaXan während der Therapie zu erwarten?', 'required|trim');

					if ($this->input->post('kontraindikation')) {
						if ($this->input->post('kontraindikation') == 'Yes') {
							
							if ($this->input->post('relative_kontraindikation') || $this->input->post('absolute_kontraindikation')) {

								if ($this->input->post('relative_kontraindikation') == 'Relative Kontraindikation') {
									
									if ($this->input->post('allergien_gegen') || $this->input->post('nierenerkrankung')) {
										$success = true;
									} else {
										$message = 'Bitte wählen Sie eine Option bei Relative Kontraindikation.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}

								if ($this->input->post('absolute_kontraindikation') == 'Absolute Kontraindikation') {
									
									if ($this->input->post('schwere_personlichkeitsstorung') || $this->input->post('schwere_kardiovaskulare') || $this->input->post('kinder') || $this->input->post('schwangerschaft')) {
										$success = true;
									} else {
										$message = 'Bitte wählen Sie eine Option bei Absolute Kontraindikation.';
										$success = false;
										$data['success'] = $success;
										$data['message'] = $message;
										json_output($data); die;
									}
								}
								
							} else {
								$message = 'Bitte wählen Sie eine Option.';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;
							}
						}
					} else {
						$message = 'Bitte markieren Sie ja oder nein.';
						$success = false;
						$data['success'] = $success;
						$data['message'] = $message;
						json_output($data); die;
					}

					if ($this->input->post('who_stufe_2_medikamente') || $this->input->post('who_stufe_2_medikamente_keiner') || $this->input->post('who_stufe_3_medikamente') || $this->input->post('who_stufe_3_medikamente_keiner') || $this->input->post('weitere_begrundung')) {
						$success = true;

						if ($this->input->post('weitere_begrundung')) {

							if ($this->input->post('weitere_begrundung_text')) {

							} else {
								$message = 'Warum werden alternative Therapien in der vorliegenden Diagnose nicht verwendet und die Anamnese ist erforderlich?';
								$success = false;
								$data['success'] = $success;
								$data['message'] = $message;
								json_output($data); die;		
							}

						}
					} else {
						$message = 'Warum werden alternative Therapien in der vorliegenden Diagnose nicht verwendet und die Anamnese ist erforderlich?';
						$success = false;
						$data['success'] = $success;
						$data['message'] = $message;
						json_output($data); die;
					}

					if ($this->form_validation->run()) {
						
						$input['web_patient_question_id'] = $question_data_exists->id;
						if ($this->input->post('kontraindikation') == 'Yes') {
							$input['kontraindikation'] = $this->input->post('kontraindikation');
							if ($this->input->post('relative_kontraindikation') == 'Relative Kontraindikation') {
								
								$input['relative_kontraindikation'] = $this->input->post('relative_kontraindikation');
								$input['allergien_gegen'] = $this->input->post('allergien_gegen');
								$input['nierenerkrankung'] = $this->input->post('nierenerkrankung');
							}
							if ($this->input->post('absolute_kontraindikation') == 'Absolute Kontraindikation') {
								
								$input['absolute_kontraindikation'] = $this->input->post('absolute_kontraindikation');
								$input['schwere_personlichkeitsstorung'] = $this->input->post('schwere_personlichkeitsstorung');
								$input['schwere_kardiovaskulare'] = $this->input->post('schwere_kardiovaskulare');
								$input['kinder'] = $this->input->post('kinder');
								$input['schwangerschaft'] = $this->input->post('schwangerschaft');
								$input['add_date'] = getDefaultToGMTDate(time());
							}

						} else {
							//$input['web_patient_question_id'] = $question_data_exists->id;							
							$input['kontraindikation'] = $this->input->post('kontraindikation');
							$input['relative_kontraindikation'] = NULL;
							$input['allergien_gegen'] = NULL;
							$input['nierenerkrankung'] = NULL;
							$input['absolute_kontraindikation'] = NULL;
							$input['schwere_personlichkeitsstorung'] = NULL;
							$input['schwere_kardiovaskulare'] = NULL;
							$input['kinder'] = NULL;
							$input['schwangerschaft'] = NULL;
							$input['add_date'] = getDefaultToGMTDate(time());
						}

						if ($this->input->post('who_stufe_2_medikamente')) {
							$input['who_stufe_2_medikamente'] = $this->input->post('who_stufe_2_medikamente');
						} else {
							$input['who_stufe_2_medikamente'] = '';
						}
						if ($this->input->post('who_stufe_2_medikamente_keiner')) {
							$input['who_stufe_2_medikamente_keiner'] = $this->input->post('who_stufe_2_medikamente_keiner');
						} else {
							$input['who_stufe_2_medikamente_keiner'] = '';
						}
						if ($this->input->post('who_stufe_3_medikamente')) {
							$input['who_stufe_3_medikamente'] = $this->input->post('who_stufe_3_medikamente');
						} else {
							$input['who_stufe_3_medikamente'] = '';
						}
						if ($this->input->post('who_stufe_3_medikamente_keiner')) {
							$input['who_stufe_3_medikamente_keiner'] = $this->input->post('who_stufe_3_medikamente_keiner');
						} else {
							$input['who_stufe_3_medikamente_keiner'] = '';
						}
						
						if ($this->input->post('weitere_begrundung')) {
							$input['weitere_begrundung'] = $this->input->post('weitere_begrundung');
							$input['weitere_begrundung_text'] = $this->input->post('weitere_begrundung_text');
						} else {
							$input['weitere_begrundung'] = '';
							$input['weitere_begrundung_text'] = '';
						}

						
						//pr($input); die;
						$this->arztfragebogen->deleteWebQuestion_6($question_data_exists->id);
						$question_6_data_added = $this->arztfragebogen->addWebQuestion_6($input);

						if ($question_6_data_added) {
							$success = true;
							$message = 'Der Fragebogen wurde erfolgreich gespeichert.';
							$data['redirectURL'] = base_url('kostenubernahmeantrag/step-6?in='.$insured_number.'&qid='.$question_added_id);
						} else {
							$success = false;
							$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
						}
					} else {
						$message = validation_errors();
						$success = false;
					}

					$data['message'] = $message;
					$data['success'] = $success;
					json_output($data);
				}
				$recorded_answer = $this->arztfragebogen->get_fifth_step_question_data($question_data_exists->id);
				//pr($recorded_answer); die;

				if ($recorded_answer) {
					
					$output['recorded_answer'] = $recorded_answer;
					$output['kontraindikation'] = $recorded_answer->kontraindikation;
					$output['relative_kontraindikation'] = $recorded_answer->relative_kontraindikation;
					$output['allergien_gegen'] = $recorded_answer->allergien_gegen;
					$output['nierenerkrankung'] = $recorded_answer->nierenerkrankung;
					$output['absolute_kontraindikation'] = $recorded_answer->absolute_kontraindikation;
					$output['schwere_personlichkeitsstorung'] = $recorded_answer->schwere_personlichkeitsstorung;
					$output['schwere_kardiovaskulare'] = $recorded_answer->schwere_kardiovaskulare;
					$output['kinder'] = $recorded_answer->kinder;
					$output['schwangerschaft'] = $recorded_answer->schwangerschaft;
					$output['who_stufe_2_medikamente'] = $recorded_answer->who_stufe_2_medikamente;
					$output['who_stufe_2_medikamente_keiner'] = $recorded_answer->who_stufe_2_medikamente_keiner;
					$output['who_stufe_3_medikamente'] = $recorded_answer->who_stufe_3_medikamente;
					$output['who_stufe_3_medikamente_keiner'] = $recorded_answer->who_stufe_3_medikamente_keiner;
					$output['weitere_begrundung'] = $recorded_answer->weitere_begrundung;
					$output['weitere_begrundung_text'] = $recorded_answer->weitere_begrundung_text;
				} else {

					$last_process_complete_question = $this->arztfragebogen->get_last_process_complete_answer($is_exists_patient->id, $doctor_id);

					if ($last_process_complete_question) {
						$recorded_answer = $this->arztfragebogen->get_fifth_step_question_data($last_process_complete_question->id);

						if ($recorded_answer) {
							$output['recorded_answer'] = $recorded_answer;
							$output['kontraindikation'] = $recorded_answer->kontraindikation;
							$output['relative_kontraindikation'] = $recorded_answer->relative_kontraindikation;
							$output['allergien_gegen'] = $recorded_answer->allergien_gegen;
							$output['nierenerkrankung'] = $recorded_answer->nierenerkrankung;
							$output['absolute_kontraindikation'] = $recorded_answer->absolute_kontraindikation;
							$output['schwere_personlichkeitsstorung'] = $recorded_answer->schwere_personlichkeitsstorung;
							$output['schwere_kardiovaskulare'] = $recorded_answer->schwere_kardiovaskulare;
							$output['kinder'] = $recorded_answer->kinder;
							$output['schwangerschaft'] = $recorded_answer->schwangerschaft;
							$output['who_stufe_2_medikamente'] = $recorded_answer->who_stufe_2_medikamente;
							$output['who_stufe_2_medikamente_keiner'] = $recorded_answer->who_stufe_2_medikamente_keiner;
							$output['who_stufe_3_medikamente'] = $recorded_answer->who_stufe_3_medikamente;
							$output['who_stufe_3_medikamente_keiner'] = $recorded_answer->who_stufe_3_medikamente_keiner;
							$output['weitere_begrundung'] = $recorded_answer->weitere_begrundung;
							$output['weitere_begrundung_text'] = $recorded_answer->weitere_begrundung_text;
						} else {
							$output['recorded_answer'] = '';
							$output['kontraindikation'] = '';
							$output['relative_kontraindikation'] = '';
							$output['allergien_gegen'] = '';
							$output['nierenerkrankung'] = '';
							$output['absolute_kontraindikation'] = '';
							$output['schwere_personlichkeitsstorung'] = '';
							$output['schwere_kardiovaskulare'] = '';
							$output['kinder'] = '';
							$output['schwangerschaft'] = '';
							$output['who_stufe_2_medikamente'] = '';
							$output['who_stufe_2_medikamente_keiner'] = '';
							$output['who_stufe_3_medikamente'] = '';
							$output['who_stufe_3_medikamente_keiner'] = '';
							$output['weitere_begrundung'] = '';
							$output['weitere_begrundung_text'] = '';
						}
					} else {
						$output['recorded_answer'] = '';
						$output['kontraindikation'] = '';
						$output['relative_kontraindikation'] = '';
						$output['allergien_gegen'] = '';
						$output['nierenerkrankung'] = '';
						$output['absolute_kontraindikation'] = '';
						$output['schwere_personlichkeitsstorung'] = '';
						$output['schwere_kardiovaskulare'] = '';
						$output['kinder'] = '';
						$output['schwangerschaft'] = '';
						$output['who_stufe_2_medikamente'] = '';
						$output['who_stufe_2_medikamente_keiner'] = '';
						$output['who_stufe_3_medikamente'] = '';
						$output['who_stufe_3_medikamente_keiner'] = '';
						$output['weitere_begrundung'] = '';
						$output['weitere_begrundung_text'] = '';
					}
				}
				$this->load->view('front/includes/header', $output);
				$this->load->view('front/arztfragebogen/kontraindikation');
				$this->load->view('front/includes/footer');
			} else {
				redirect('arztfragebogen/step-5?in='.$insured_number.'&qid='. $question_added_id);
			}
			
		} else {
			redirect('patients');
		}

	}

	function arztfragebogen_step_6() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}
		/*pdf page no 18 */

		$output['header_menu'] = 'arztfragebogen';
		$doctor_id = $this->session->userdata('user_id');
		$insured_number = $this->input->get('in');
		$output['insured_number'] = $insured_number;
		$is_exists_patient = $this->patients->getPatientByInsuranceNumber($insured_number);
		$question_added_id = $this->input->get('qid');
		$output['qid'] = $question_added_id;

		if ($is_exists_patient) {
			$question_data_exists = $this->arztfragebogen->checkQuestionStartDataExistsById($question_added_id);

			if ($question_data_exists) {
				if ($this->input->post()) {
					
					$this->form_validation->set_rules('klinischen_prufung','Erfolgt die Therapie im Rahmen einer klinischen Prüfung?','trim|required', array('required' => 'Bitte markieren Sie ja oder nein.'));
					$this->form_validation->set_rules('therapieoptionen_haben','Sind die Therapieoptionen, die für die vorliegende Indikation zur Reduktion der Symptomatik aus ärztlicher Sicht durchzuführen sind, ausgeschöpft?','trim|required', array('required' => 'Sind die Therapieoptionen Sie ja oder nein?.'));

					if ($this->form_validation->run()) {
						$input['web_patient_question_id'] = $question_data_exists->id;
						$input['klinischen_prufung'] = $this->input->post('klinischen_prufung');
						$input['therapieoptionen_haben'] = $this->input->post('therapieoptionen_haben');
						$input['add_date'] = getDefaultToGMTDate(time());

						$patient_process['is_process_complete'] = 'Yes';
						$patient_process['update_date'] = getDefaultToGMTDate(time());
						$this->arztfragebogen->deleteWebQuestion_7($question_data_exists->id);
						$question_7_data_added = $this->arztfragebogen->addWebQuestion_7($input);

						if ($question_7_data_added) {
							$process_completed = $this->arztfragebogen->updateWebQuestionOfPatientStart($question_data_exists->id, $patient_process);
							//pr($input); die;
							if ($process_completed) {
								
								$success = true;
								$message = 'Der Fragebogen wurde erfolgreich gespeichert.';
								$data['redirectURL'] = base_url('arztfragebogen/arztfragebogen_pdf?in='.$insured_number.'&qid='.$question_added_id);
							} else {
								$success = false;
								$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';	
							}
						} else {
							$success = false;
							$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
						}

					} else {
						$success = false;
						$message = validation_errors();
					}
					$data['success'] = $success;
					$data['message'] = $message;
					json_output($data);
				}
				$recorded_answer = $this->arztfragebogen->get_sixth_step_question_data($question_data_exists->id);

				if ($recorded_answer) {
					
					$output['recorded_answer'] = $recorded_answer;
					$output['klinischen_prufung'] = $recorded_answer->klinischen_prufung;
					$output['therapieoptionen_haben'] = $recorded_answer->therapieoptionen_haben;
					
				} else {
					$last_process_complete_question = $this->arztfragebogen->get_last_process_complete_answer($is_exists_patient->id, $doctor_id);

					if ($last_process_complete_question) {
						$recorded_answer = $this->arztfragebogen->get_sixth_step_question_data($last_process_complete_question->id);

						if ($recorded_answer) {
							$output['recorded_answer'] = $recorded_answer;
							$output['klinischen_prufung'] = $recorded_answer->klinischen_prufung;
							$output['therapieoptionen_haben'] = $recorded_answer->therapieoptionen_haben;
						} else {
							$output['recorded_answer'] = '';
							$output['klinischen_prufung'] = '';
							$output['therapieoptionen_haben'] = '';
						}
					} else {
						$output['recorded_answer'] = '';
						$output['klinischen_prufung'] = '';
						$output['therapieoptionen_haben'] = '';
					}
				}
				$this->load->view('front/includes/header', $output);
				$this->load->view('front/arztfragebogen/klinischen-prufung');
				$this->load->view('front/includes/footer');
			} else {
				redirect('arztfragebogen/step-5?in='.$insured_number.'&qid='. $question_added_id);
			}
			
		} else {
			redirect('patients');
		}

	}

	function arztfragebogen_pdf() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}

		$insured_number = $this->input->get('in');
		$output['insured_number'] = $insured_number;

		$doctor_id = $this->session->userdata('user_id');
		$doctor_details = $this->arztfragebogen->getLoggedInUserById($doctor_id);
		//pr($doctor_details); die;
		if ($doctor_details) {
			
			$output['doctor_details'] = $doctor_details;
			$output['title'] = $doctor_details->title;
			$output['vorname'] = $doctor_details->first_name;
			$output['nachname'] = $doctor_details->name;
			$output['arztpraxis'] = $doctor_details->name_doctor_practice;
			$output['strabe'] = $doctor_details->road;
			$output['plzl'] = $doctor_details->plzl;
			$output['ort'] = $doctor_details->ort;
			$output['house_no'] = $doctor_details->hausnr;
			$output['unterschrift'] = $doctor_details->unterschrift;
			$output['datum'] = date('d.m.Y');
		} else {
			$output['doctor_details'] = '';
			$output['title'] = '';
			$output['vorname'] = '';
			$output['nachname'] = '';
			$output['plzl'] = '';
			$output['strabe'] = '';
			$output['arztpraxis'] = '';
			$output['ort'] = '';
			$output['house_no'] = '';
			$output['unterschrift'] = '';
			$output['datum'] = date('d.m.Y');
		}
		$is_exists_patient = $this->patients->getPatientByInsuranceNumber($insured_number);

		if ($is_exists_patient) {

			$qid = $this->input->get('qid');
			$question_group = $this->arztfragebogen->getQuestionByQid($qid, $is_exists_patient->id);

			if ($question_group) {
				$question_1_part_first = $this->arztfragebogen->getFirstQuestionByWebPatientQuestionId($question_group->id, true);
				$question_1_part_second = $this->arztfragebogen->getFirstQuestionByWebPatientQuestionId($question_group->id, false);

				// pr($question_1_part_second); die;
				$question_2 = $this->arztfragebogen->getSecondQuestionByWebPatientQuestionId($question_group->id);
				$question_3 = $this->arztfragebogen->getThirdQuestionByWebPatientQuestionId($question_group->id);
				$question_4 = $this->arztfragebogen->getForthQuestionByWebPatientQuestionId($question_group->id);

				$question_5 = $this->arztfragebogen->getFifthQuestionByWebPatientQuestionId($question_group->id);

				$question_5_WHO_1_nicht = $this->arztfragebogen->getFifthQuestionWHO1NichtById($question_5->id, 'Nicht-Opioidanalgetika');
				//pr($question_5_WHO_1_nicht);

				$question_5_WHO_1_steroide = $this->arztfragebogen->getFifthQuestionWHO1SteroideById($question_5->id, 'Steroide');
				//pr($question_5_WHO_1_steroide);

				$question_5_WHO_1_neuroleptika = $this->arztfragebogen->getFifthQuestionWHO1NeuroleptikaById($question_5->id, 'Neuroleptika');
				//pr($question_5_WHO_1_neuroleptika);

				$question_5_WHO_1_antidepressiva = $this->arztfragebogen->getFifthQuestionWHO1AntidepressivaById($question_5->id, 'Antidepressiva');
				//pr($question_5_WHO_1_antidepressiva);

				$question_5_WHO_1_sedativa = $this->arztfragebogen->getFifthQuestionWHO1SedativaById($question_5->id, 'Sedativa');
				//pr($question_5_WHO_1_sedativa);

				$question_5_WHO_1_antikonvulsiva = $this->arztfragebogen->getFifthQuestionWHO1AntikonvulsivaById($question_5->id, 'Antikonvulsiva');
				//pr($question_5_WHO_1_antikonvulsiva);

				$question_5_WHO_1_antiemetika = $this->arztfragebogen->getFifthQuestionWHO1AntiemetikaById($question_5->id, 'Antiemetika');
				//pr($question_5_WHO_1_antiemetika); die;

				$question_5_WHO_2 = $this->arztfragebogen->getFifthQuestionWHO2ById($question_5->id);
				//pr($question_5_WHO_2);

				$question_5_WHO_3 = $this->arztfragebogen->getFifthQuestionWHO3ById($question_5->id);
				//pr($question_5_WHO_3); die;
				$question_5_Spasmolytika = $this->arztfragebogen->getFifthQuestionSpasmolytikaById($question_5->id);

				$question_6 = $this->arztfragebogen->getSixthQuestionByWebPatientQuestionId($question_group->id);
				//pr($question_6); die;

				$question_7 = $this->arztfragebogen->getSevenQuestionByWebPatientQuestionId($question_group->id);

				// $this->load->model('Patients_model','patients');

				$begleiterkrankungen = $this->patients->getPatientBegleiterkrankungen($is_exists_patient->id);

				$begleitmedikation = $this->arztfragebogen->getbegleitmedikationDataByPatientId($is_exists_patient->id);

				$hauptindikation = $this->arztfragebogen->getHauptindikationById($is_exists_patient->hauptindikation);

				$book_group = $this->arztfragebogen->getBookGroupIdByICDCode($is_exists_patient->hauptindikation);

				if ($book_group) {
					$books = $this->arztfragebogen->getBooksByGroupId($book_group->book_group_id);
				} else {
					$books = array();
				}
				
				// pr($is_exists_patient); die;
				
				$last_therapie_access = $is_exists_patient->last_therapie_access;

				if ($last_therapie_access == 'Titration'){
					$therapieplan = $this->patients->getPatientArztlicherTherapieplan($is_exists_patient->id);
					$summe_cannaxan_thc = $therapieplan->total_cannaxan_thc;
					$summe_cannaxan_thc = str_replace('.', ',', $summe_cannaxan_thc);
					$rezepturarzneimittel = $therapieplan->rezepturarzneimittel;
					$wirkstoff = $therapieplan->wirkstoff;
				} else if ($last_therapie_access == 'Umstellung'){
					$therapieplan = $this->patients->getPatientTherapyPlanUmstellung($is_exists_patient->id);
					$typUmstellung = $this->patients->getPatientTypUmstellung($therapieplan->id);
					$table = $this->patients->arztlicher_therapieplan_table($therapieplan->anzahl_therapietage);
					$total_summe = 0;
					$zuordnung_sps = $this->patients->getZuordnungSps();

					foreach ($table as $key => $value) {
						$total = ($typUmstellung[0]->morgens_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg) + ($typUmstellung[0]->mittags_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg) + ($typUmstellung[0]->abends_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg) + ($typUmstellung[0]->nachts_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg);
						$total_summe = $total_summe + $total;
					}
					$konzentration_thc_mg_ml = $zuordnung_sps->konzentration_thc_mg_ml;
    				$summe_cannaxan_thc = round(($total_summe / $konzentration_thc_mg_ml), 1);
    				$summe_cannaxan_thc = str_replace('.', ',', $summe_cannaxan_thc);
    				$rezepturarzneimittel = $therapieplan->rezepturarzneimittel;
					$wirkstoff = $therapieplan->wirkstoff;
				} else if ($last_therapie_access == 'Folgeverordnung'){
					$therapieplan = $this->patients->getPatientThherpieplanFolgeverord($is_exists_patient->id);
					$summe_cannaxan_thc = str_replace('.', ',', $therapieplan->total_cannaxan_thc);
					$rezepturarzneimittel = $therapieplan->rezepturarzneimittel;
					$wirkstoff = $therapieplan->wirkstoff;
				} else {
					$summe_cannaxan_thc = '0,00';
					$rezepturarzneimittel = 'CannaXan-THC';
					$wirkstoff = 'CannaXan-701-1.1';
				}
				$output['is_exists_patient'] = $is_exists_patient;
				$output['summe_cannaxan_thc'] = $summe_cannaxan_thc;
				$output['rezepturarzneimittel'] = $rezepturarzneimittel;
				$output['wirkstoff'] = $wirkstoff;
				$output['question_1_part_first'] = $question_1_part_first;
				$output['question_1_part_second'] = $question_1_part_second;
				$output['question_2'] = $question_2;
				$output['question_3'] = $question_3;
				$output['question_4'] = $question_4;
				$output['question_5'] = $question_5;
				$output['question_5_WHO_1_nicht'] = $question_5_WHO_1_nicht;
				$output['question_5_WHO_1_steroide'] = $question_5_WHO_1_steroide;
				$output['question_5_WHO_1_neuroleptika'] = $question_5_WHO_1_neuroleptika;
				$output['question_5_WHO_1_antidepressiva'] = $question_5_WHO_1_antidepressiva;
				$output['question_5_WHO_1_sedativa'] = $question_5_WHO_1_sedativa;
				$output['question_5_WHO_1_antikonvulsiva'] = $question_5_WHO_1_antikonvulsiva;
				$output['question_5_WHO_1_antiemetika'] = $question_5_WHO_1_antiemetika;
				$output['question_5_WHO_2'] = $question_5_WHO_2;
				$output['question_5_WHO_3'] = $question_5_WHO_3;
				$output['question_5_Spasmolytika'] = $question_5_Spasmolytika;
				$output['question_6'] = $question_6;
				$output['question_7'] = $question_7;
				$output['begleiterkrankungen'] = $begleiterkrankungen;
				$output['begleitmedikation'] = $begleitmedikation;
				$output['hauptindikation'] = $hauptindikation;
				$output['books'] = $books;
				// pr($output); die;
			}

			//$html = $this->load->view('front/arztfragebogen/arztfragebogen_pdf_page', $output);
			$html = $this->load->view('front/arztfragebogen/arztfragebogen_pdf_page', $output, true);
			
			//$this->load->view('front/arztfragebogen/arztfragebogen_pdf_page', $output);
			$header_html = $this->load->view('front/arztfragebogen/arztfragebogen_pdf_header', $output, true);
			$footer_html = $this->load->view('front/arztfragebogen/arztfragebogen_pdf_footer', $output, true);

			$pdf_name = 'Kostenübernahmeantrag_'.str_replace(' ', '', $is_exists_patient->first_name).'_'.str_replace(' ', '', $is_exists_patient->last_name).'_'.date('dmY');
			// echo $html; die;

			$this->load->library('pdf');
			$this->pdf->loadHtml($html);
			$this->pdf->set_paper("a4", "portrait" );
			$this->pdf->render();
			$this->pdf->set_option( "is_php_enabled" , true );
			// $canvas = $this->pdf->get_canvas(); 
			// $font = $this->pdf->getFontMetrics()->get_font("helvetica", "bold");
			// $font = Font_Metrics::get_font("helvetica", "bold"); 
			// $canvas->page_text(0, 512, "Page {PAGE_NUM} of {PAGE_COUNT}",$font, 8, array(0,0,0)); 
			ob_end_clean();
			$this->pdf->stream($pdf_name.".pdf");
            /*$new_pdf_name = time().".pdf.";
            file_put_contents(ASSETSPATH. 'uploads/pdf/'.$pdf_name.".html", $html);

            $pdf_header = time().'-header';
            file_put_contents(ASSETSPATH. 'uploads/pdf/'.$pdf_header.".html", $header_html);

            $pdf_footer = time().'-footer';
            file_put_contents(ASSETSPATH. 'uploads/pdf/'.$pdf_footer.".html", $footer_html);
            // pr (base_url('assets/uploads/pdf/').$pdf_header.'.html'); die;	
            $newpdf_path = ASSETSPATH. 'uploads/pdf/'.$pdf_name.".pdf";
            exec ('/usr/local/bin/wkhtmltopdf --print-media-type --margin-top 25mm --margin-bottom 25mm --header-html '.base_url('assets/uploads/pdf/').$pdf_header.'.html --footer-html '.base_url('assets/uploads/pdf/').$pdf_footer.'.html --no-stop-slow-scripts '. base_url('assets/uploads/pdf/').$pdf_name.'.html '.$newpdf_path." 2>&1 ", $res);

            $file_url = base_url().'assets/uploads/pdf/'.$pdf_name.'.pdf';

            header('Content-Type: *');
            header("Content-Transfer-Encoding: *"); 
            header("Content-disposition: attachment; filename=\"".$pdf_name.".pdf\""); 
            readfile($file_url);
*/
            /*$this->load->library('pdf');
			$this->pdf->loadHtml($html);
			$this->pdf->render();
			$this->pdf->stream($pdf_name.".pdf", array("Attachment"=>0));*/
            exit;

		} else {
			redirect('patients');
		}
	}
}
