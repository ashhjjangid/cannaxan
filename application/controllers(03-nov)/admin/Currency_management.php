<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Currency_management class.
 * 
 * @extends CI_Controller
 */
class Currency_management extends CI_Controller {

    /**
    * __construct function.
    * 
    * @access public
    * @return void
    */
       
    function __construct() {
        Parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/Currency_management_model', 'currency');
    }

   /**
    * index function.
    * 
    * @access public
    * @return list of all currency
    */

    public function index() {
        $output['page_title'] = 'Currency Management';
        $output['left_menu'] = 'Currency_management';
        $output['left_submenu'] = 'Currency_list';
        /* get list of Currency management*/
        $currency = $this->currency->get_currency();
        $output['currency'] = $currency;
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/currency/list');
        $this->load->view('admin/includes/footer');
    }

   /**
    * add function.
    * 
    * @access public
    * @return null
    */

    public function add() {
        $output['page_title'] = 'Add Currency';
        $output['left_menu'] = 'Currency Management';
        $output['left_submenu'] = 'Currency Management list';
        $output['message'] = '';
        $output['task'] = 'add';
        $output['status'] = 'Active';
        $output['id'] = '';
        $output['currency_name'] = '';
        
        if ($this->input->post()) {
            $success = true;
            $message = '';
            /* set validation rules */
            $this->form_validation->set_rules('currency_name', 'Currency name', 'trim|required');
            

            if ($this->form_validation->run()) {
                $banner_image='';
                $input = array();
                $error = 0;
                
                               
                if (isset($_FILES['icon']['name']) && $_FILES['icon']['name']) {
                    $directory = './assets/uploads/currencies/'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'gif|jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if ($this->upload->do_upload('icon')) {
                        $image_data = $this->upload->data();
                        $file_name = $image_data['file_name'];
                        $input['icon'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                    }
                }

                if ($success) {
                    
                   $currency_name = $this->input->post('currency_name',true);

                    $input['currency_name'] = ucfirst($this->input->post('currency_name',true));
                    $input['status'] = $this->input->post('status',true);
                    /*insert as currency management */
                    $insert_id = $this->currency->addcurrency($input);
                    //print_r($insert_id);die;
                    if ($insert_id) {
        
                        $success = true;
                        $message = 'Currency Management added successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = base_url('admin/Currency_management');
                    
                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
                }
            
            } else {
                $message.= validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        } 
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/currency/form');
        $this->load->view('admin/includes/footer');
    }

   /**
    * update function.
    * 
    * @access public
    * @param mixed $id     
    * @return null
    */

    public function update($id) {
        $output['page_title'] = 'Update Currency';
        $output['left_menu'] = 'Currency Management';
        $output['left_submenu'] = 'Currency Management list';
        $output['task'] = 'edit';
        $output['message'] = '';
        /* get customer record*/
        $record = $this->currency->getcurrencyById($id);
        checkRequestedDataExists($record);
        /* assign values */
        $output['record'] = $record;
        $output['status'] = $record->status;
        $output['id'] = $record->id;
        $output['currency_name'] = $record->currency_name;
        
        $output['status'] = $record->status;

        if ($this->input->post()) {
            $message = '';
            $success = true;
            /* set validation rules */
            $this->form_validation->set_rules('currency_name', 'Currency name', 'alpha_numeric_spaces|trim|required');
            
                $update_arr = array();
                $error  = 0;

                if (isset($_FILES['icon']['name']) && $_FILES['icon']['name']) {
                    $directory = './assets/uploads/currencies/'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'gif|jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if ($this->upload->do_upload('icon')) {
                        $image_data = $this->upload->data();
                        $file_name = $image_data['file_name'];
                        $input['icon'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                    }
                    
                }

                if ($success) {
                    $input['currency_name'] = ucfirst($this->input->post('currency_name',true));
                    
                    $input['status'] = $this->input->post('status',true);
                    /* update records */
                    $response = $this->currency->setcurrencyById($id,$input);

                    if ($response) {
                        $success = true;
                        $message = 'Currency Management updated successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = base_url('admin/Currency_management');
                    
                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
                }
            
             else {
                $success = false;
                $message = validation_errors();     
            }   
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/currency/form');
        $this->load->view('admin/includes/footer');
    }

   /**
    * view function.
    * 
    * @access public
    * @param mixed $id      
    */

    public function view($id) {
        $output['page_title'] = 'View Currency management';
        $output['left_menu'] = 'Currency management';
        $output['left_submenu'] = 'Currency management list';
        /* get record by id */
        $record = $this->currency->getcurrencyById($id);
        checkRequestedDataExists($record);
        $output['currency'] = $record; 

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/currency/view');
        $this->load->view('admin/includes/footer');
    }

   /**
    * changeStatus function.
    * 
    * @access public
    * @return null
    */

    public function changeStatus()    {
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
        $this->currency->changeStatusById($id,$status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }
   
   /**
    * multiTaskOperation function.
    * 
    * @access public
    * @return null
    */

    public function multiTaskOperation() {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        
        foreach ($dataIds as $key => $value) {
            if ($task == 'Delete') {
                $this->currency->deletecurrencyById($value);
                $message = 'Currency management has been deleted successfully.';
            
            }else if 
            ($task=='Active' || $task=='Inactive') {
                $this->currency->changeStatusById($value,$task);            
                $message = 'Status of selected currency management has been changed successfully.';
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data); 
    }

   /**
    * delete function.
    * 
    * @access public
    * @return null
    */
    public function delete() {
        $id = $this->input->post('record_id',true);
        $this->currency->deletecurrencyById($id);

        $data['success'] = true;
        $data['message'] = 'Currency management has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }
}