<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cannabis_medikament extends CI_Controller {


    function __construct() {

        Parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/cannabismedikament_model','cannabismedikament');
        
    }

    public function index() {
        $output['page_title'] = 'cannabis medikament';
        $output['left_menu'] = 'cannabismedikament_management';
        $output['left_submenu'] = 'cannabismedikament_list';

        $record = $this->cannabismedikament->getcannabis_medikament();
        checkRequestedDataExists($record);
        $output['record'] = $record;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/cannabis_medikament/list');
        $this->load->view('admin/includes/footer');
    }

    public function add() 
    {  

        $output['page_title'] = 'Add cannabis medikament';
        $output['left_menu'] = 'cannabismedikament_management';
        $output['left_submenu'] = 'cannabismedikament_add';
        $output['message'] = '';
        $output['tag_name'] = '';
        $output['id'] = '';
        $output['name'] = '';
        $output['status'] = 'Active';
        
        if ($this->input->post()) {
            $message = '';
            $success = true;

            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            if ($this->form_validation->run()) {

                 $input = array();
                 $success =  true;
                 $message = '';
                
                    $input['name'] = $this->input->post('name', true);
                    $input['status'] = $this->input->post('status', true);
                    $input['add_date'] = getDefaultToGMTDate(time());
                    //pr($input); die;
                    $insert_id = $this->cannabismedikament->add_new_cannabis_medikament($input);



                    if ($insert_id) {

                        $success = true;
                        $message = 'Details of cannabis medikament added successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = base_url('admin/cannabis_medikament');

                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
            
            } else {
                $success = false;
                $message = validation_errors();        
            }    
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/cannabis_medikament/form');
        $this->load->view('admin/includes/footer');
    }

    public function update($id) 
    {   

        $record = $this->cannabismedikament->getcannabis_medikamentById($id);
        checkRequestedDataExists($record);

        $output['page_title'] = 'Update cannabis medikament';
        $output['left_menu'] = 'cannabismedikament_management';
        $output['left_submenu'] = 'cannabismedikament_update';
        $output['message'] = '';
        $output['record'] = $record;
        $output['tag_name'] = $record->name;
        $output['id'] = $record->id;
        $output['status'] = $record->status;
        
        if ($this->input->post()) {
            $message = '';
            $success = true;

            $this->form_validation->set_rules('name', 'Name', 'trim|required');

            if ($this->form_validation->run()) {

                 $input = array();
                 $success =  true;
                 $message = '';
                
                    $input['name'] = $this->input->post('name', true);
                    $input['status'] = $this->input->post('status', true);
                    $input['update_date'] = getDefaultToGMTDate(time());
                    //pr($input); die;
                    $response = $this->cannabismedikament->setcannabis_medikamentById($id, $input);

                    if ($response) {

                        $success = true;
                        $message = 'Details of cannabis medikament updated successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = site_url('admin/cannabis_medikament');

                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
            
            } else {
                $success = false;
                $message = validation_errors();        
            }    
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/cannabis_medikament/form');
        $this->load->view('admin/includes/footer');
    }

      public function view($id) {
        $output['page_title'] = 'View details';
        $output['left_menu'] = 'cannabismedikament_management';
        $output['left_submenu'] = 'cannabismedikament_view';
        /* get record by id */
        $record = $this->cannabismedikament->getcannabis_medikamentById($id);
        //pr($record); die;
        checkRequestedDataExists($record);
        $output['record'] = $record; 

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/cannabis_medikament/view');
        $this->load->view('admin/includes/footer');
    }    

    public function changeStatus()    {
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
        $this->cannabismedikament->changeStatusById($id,$status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    /*public function delete() {
        $id = $this->input->post('record_id',true);
        $this->deals->getUsersById($id);
        $data['success'] = true;
        $data['message'] = 'Users has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }*/

    public function multiTaskOperation() {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        
        foreach ($dataIds as $key => $value) {
            
            if ($task == 'Delete') {
                $this->deals->deleteUsersById($value);
                $message = 'Users has been deleted successfully.';
            
            } else  if ($task=='Active' || $task=='Inactive') {
                $this->cannabismedikament->changeStatusById($value,$task);            
                $message = 'Status of selected users has been changed successfully.';
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data); 
    }

    
}