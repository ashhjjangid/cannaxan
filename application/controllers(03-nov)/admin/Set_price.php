<?php 

/**
 * 
 */
class Set_price extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
    {
        $output['page_title'] = "Set Price";
        $output['left_menu'] = 'Site_options';
        $output['left_submenu'] = 'set_price';

        if ($this->input->post()) 
        {
        	$this->form_validation->set_rules('');
        	$this->form_validation->set_rules();
        }

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/price/prices');
        $this->load->view('admin/includes/footer');
    }
}
?>