<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Rating class. 
 * @extends CI_Controller
 */

class Rating extends CI_Controller {

   /**
    * __construct function
    * @access public
    * @return void
   */
       
    function __construct() {
        parent:: __construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/Ratings_model', 'rating');
        $this->load->model('admin/riders_model', 'user');
        $this->load->model('admin/scooter_model', 'scooter');
        
    }

    /**
     * index function.
     * @access public
     * @return null
     */

  public function index()
  {
    $output['page_title']="Reviews";
    $output['left_menu']="rating_management";
    $output['left_submenu']="Rating_list";

    $list = $this->rating->get_list_of_reviews();
    $output['rating_records']=$list;
    //pr($output['rating']);die;
    $this->load->view('admin/includes/header',$output);
    $this->load->view('admin/review/review_page_list');
    $this->load->view('admin/includes/footer');
  }


    /**
     * add function.
     * @access public
     * @return null
     */

    public function add() {
        $output['page_title'] = 'Add Review';
        $output['left_menu'] = 'rating_management';
        $output['left_submenu'] = 'Rating_list';
        $users = $this->user->get_all_riders();
        $output['users'] = $users;
        $scooters = $this->scooter->get_all_scooters();
        $output['scooters'] = $scooters;        
        $output['message'] = '';
        $output['task'] = 'add';
        $output['user_name'] = '';
        $output['maker_name'] = '';
        $output['rating'] = '';
        $output['review'] = '';
        $output['status'] = 'Active';
        $output['id'] = '';
        $output['add_date'] = getDefaultToGMTDate(time());
       

        if ($this->input->post()) {
            $success = true;
            $message = '';
            
            $this->form_validation->set_rules('user_name', 'User name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('rating','Rating','trim|required');
            $this->form_validation->set_rules('review','Review','trim|required');
     

            if ($this->form_validation->run()) {
                $input = array();
                $error = 0;
                

                if ($success) {
                    $input['scooter_id'] = $this->input->post('scooter_name',true);
                    $input['user_id'] = $this->input->post('user_name',true);
                    $input['rating'] = $this->input->post('rating',true);
                    $input['review'] = $this->input->post('review',true);
                    $input['status'] = $this->input->post('status',true);
                    /*insert as customer user */
                    $insert_id = $this->rating->addrating($input);
                   if ($insert_id) {
                        
                        $success = true;
                        $message = 'Review added successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = base_url('admin/rating');
                    
                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
                }
            
            } else {
                $message.= validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        } 
            $this->load->view('admin/includes/header',$output);
            $this->load->view('admin/review/form');
            $this->load->view('admin/includes/footer');
        
    
        }
    /**
     * update function
     * @access public
     * @param mixed $id     
     * @return null
     */

    public function update($id) {
        $records = $this->rating->get_rating_for_update($id);
        //pr($records); die;
        $output['records'] = $records;
        $output['page_title'] = 'Update Review';
        $output['left_menu'] = 'rating_management';
        $output['left_submenu'] = 'Rating_list';
        $users = $this->user->get_all_riders();
        //pr($users);die;
        $output['users'] = $users;
        $scooters = $this->scooter->get_all_scooters();
        //pr($scooters);die;
        $output['scooters'] = $scooters;
        $output['task'] = 'edit';
        $output['message'] = '';
        /* assign values */
        $output['id'] = $records->rating_id;
        $output['user_name'] = $records->user_id;
        $output['scooter_name'] = $records->scooter_id;
        $output['status'] = $records->status;
        $output['rating'] = $records->rating;
        $output['review'] = $records->review;

        if ($this->input->post()) {
            $message = '';
            $success = true;
            /* set validation rules */
            $this->form_validation->set_rules('user_name', 'name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('rating','rating','trim|required');
            $this->form_validation->set_rules('review','review','trim|required');
            
            if ($this->form_validation->run()) {
              
                $update = array();
                $error  = 0;

                if ($success) {
                   
                    $input['user_id'] = $this->input->post('user_name',true);
                    $input['scooter_id'] = $this->input->post('scooter_name',true);
                    $input['rating'] = ucfirst($this->input->post('rating',true));
                    $input['review'] = ucfirst($this->input->post('review',true));
                    $input['status'] = $this->input->post('status',true);
                    /* update records */
                    $response = $this->rating->set_updated_rating($id,$input);

                    if ($response) {
                        $success = true;
                        $message = 'Review updated successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = base_url('admin/rating');
                    
                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
                }
            
            } else {
                $success = false;
                $message = validation_errors();     
            }   
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/review/form');
        $this->load->view('admin/includes/footer');
    }

    /**
     * change_status function.
     * @access public
     * @param mixed $id
     */

    public function change_status()
    {
        $id = $this->input->get('id', true);
        $status = $this->input->get('status', true);
        //print_r($id);
        $this->rating->change_status_by_id($id, $status);

        $data['success'] = true;
        $data['message'] = 'Status of Review updated Successfully';
        json_output($data);
    }
    /**
     * multi_task_operation function
     * @access public
     * @return null
     */

    public function multi_task_operation()
    {
        $task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $reviewsids = explode(',', $ids);
        //pr($reviewsids); die;
        foreach ($reviewsids as $value) 
        {
            if ($task == 'Delete') 
            {
                //pr($value);die('here');
                $this->rating->delete_rating($value);
                $message = 'Selected Reviews deleted Successfully';
            } 
            else if ($task == 'Active' || $task == 'Inactive') 
            {   
                $this->rating->change_status_by_id($value, $task);
                $message = 'Status of selected Reviews has been Changed Successfully';
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }
    
    public function delete() {
        $id = $this->input->post('record_id',true);
        $this->rating->delete_rating($id);

        $data['success'] = true;
        $data['message'] = 'Rating has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }

    public function view($id)
    {        
        $output['page_title'] = 'View Review Details';
        $output['left_menu'] = 'rating_management';
        $output['left_submenu'] = 'Rating_list';

        $review_records = $this->rating->view_single_review($id);
        //pr($review_records);die;
        $output['review_records'] = $review_records;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/review/view_user_review');
        $this->load->view('admin/includes/footer');
    } 

}