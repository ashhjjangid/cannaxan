<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Manage_ride class.
 * @extends CI_Controller
 */

class Manage_ride extends CI_Controller {

   /**
    * __construct function.
    * @access public
    * @return void
   */
       
    function __construct() {
        Parent::__construct();
        checkAdminLogin();
        $this->load->model('admin/Manage_rides_model', 'rides');
        $this->userId = $this->session->userdata('admin_id');
    }

    /**
     * index function.
     * @access public
     * @return static page list
     */

    function index() { 
        $output['page_title'] = 'Rides In Progress';
        $output['left_menu'] = 'rides_management';
        $output['left_submenu'] = 'manage_rides_list';
        
        $rides = $this->rides->get_rides();
        $output['rides'] = $rides;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/manage_rides/list');
        $this->load->view('admin/includes/footer');
    }
     

    public function view($id) {
        $output['page_title'] = 'Manage Rides Details';
        $output['left_menu'] = 'rides_management';
        $output['left_submenu'] = 'manage_rides_list';
        /* get record by id */
        $record = $this->rides->view_ride_details($id);
        //print_r($record);
        checkRequestedDataExists($record);
        $output['userdata'] = $record; 

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/manage_rides/view_ride_details');
        $this->load->view('admin/includes/footer');
    }
       

    /**
     * changeStatus function.
     * @access public
     * @return null
     */

    function changeStatus() {
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
       //print_r($status);die;
        $this->rides->change_status_by_id($id,$status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    /**
     * multiTaskOperation function.
     * @access public
     * @return null
     */
    
    function multiTaskOperation() {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        $message = '';

        foreach ($dataIds as $key => $value) {

            if ($task == 'Delete') {
                $this->static_page->delete_ride_page_by_id($value);
                $message = 'Selected pages has been deleted successfully.';
            
            } else if ($task=='Active' || $task=='Inactive') {
                $this->rides->change_status_by_id($value,$task);            
                $message = 'Status of Selected records changed successfully.';
            }
        }        
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }  
    
}