<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Users class.
 * 
 */
class Users extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		$this->load->model('admin/users_model', 'users');
		$this->load->model('mailsending_model', 'mailsend');
	}

	public function index()
	{
		$output['page_title'] = "Doctors";
		$output['left_menu'] = 'doctors_management';
		$output['left_submenu'] = 'doctors_list';

		//$output['records'] = $this->users->get_all_users();

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/users/users_list');
		$this->load->view('admin/includes/footer');
	}

	public function unapproved_users()
	{
		$output['page_title'] = "Unapproved Doctors";
		$output['left_menu'] = 'doctors_management';
		$output['left_submenu'] = 'unapproved_doctors_list';

		//$output['records'] = $this->users->get_all_users();

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/users/unapproved_user_list');
		$this->load->view('admin/includes/footer');
	}

	public function get_ajax_list()
	{
		$this->load->library('pagination');
    	$success = true;
        $html = false;
        $load_prev_link = false;
        $load_next_link = false;
		$keyword = $this->input->get('keyword');
		$page_limit = $this->input->get('page_limit', true);
		$column_name = $this->input->get('column_name',true);
		$sorting_order = $this->input->get('sorting_order',true);
        $page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
        $page_no_index = ($page_no - 1) * $page_limit;
        $sQuery = '';

		if($keyword)
		{
			$sQuery = $sQuery.'&keyword='.$keyword;
		}
		if($page_limit)
		{
			$sQuery = $sQuery.'&page_limit='.$page_limit;		
		}
		if ($column_name) 
		{
			$sQuery = $sQuery. '&column_name=' .$column_name;
		}
		if ($sorting_order) 
		{
			$sQuery = $sQuery. '&sorting_order=' .$sorting_order;
		}
		$per_page = $this->input->get('page_limit', true);
		$searchA['search_index'] = $page_no_index;
		$searchA['limit'] = $per_page;		
		$searchA['keyword'] = $keyword;
		$searchA['column_name'] = $column_name;
		$searchA['sorting_order'] = $sorting_order;
        $config['base_url'] = site_url('admin/users/get_ajax_list?' .$sQuery);
        $config['total_rows'] = $this->users->count_users($searchA);
        $config['per_page'] = $per_page;

        $this->pagination->initialize($config);
        $paging = $this->pagination->create_links();
        $records = $this->users->get_all_users($searchA);
        $output['records'] = $records; 
        //pr($output); die;
        $html = $this->load->view('admin/users/ajax_list',$output, true);
        // pr($html); die;
        $data['html'] = $html;
        $data['paging'] = $paging;
        $data['success']= true;
        json_output($data); 

	}

	public function unapproved_get_ajax_list()
	{
		$this->load->library('pagination');
    	$success = true;
        $html = false;
        $load_prev_link = false;
        $load_next_link = false;
		$keyword = $this->input->get('keyword');
		$page_limit = $this->input->get('page_limit', true);
		$column_name = $this->input->get('column_name',true);
		$sorting_order = $this->input->get('sorting_order',true);
        $page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
        $page_no_index = ($page_no - 1) * $page_limit;
        $sQuery = '';

		if($keyword)
		{
			$sQuery = $sQuery.'&keyword='.$keyword;
		}
		if($page_limit)
		{
			$sQuery = $sQuery.'&page_limit='.$page_limit;		
		}
		if ($column_name) 
		{
			$sQuery = $sQuery. '&column_name=' .$column_name;
		}
		if ($sorting_order) 
		{
			$sQuery = $sQuery. '&sorting_order=' .$sorting_order;
		}
		$per_page = $this->input->get('page_limit', true);
		$searchA['search_index'] = $page_no_index;
		$searchA['limit'] = $per_page;		
		$searchA['keyword'] = $keyword;
		$searchA['column_name'] = $column_name;
		$searchA['sorting_order'] = $sorting_order;
        $config['base_url'] = site_url('admin/users/unapproved_get_ajax_list?' .$sQuery);
        $config['total_rows'] = $this->users->count_unapproved_users($searchA);
        $config['per_page'] = $per_page;

        $this->pagination->initialize($config);
        $paging = $this->pagination->create_links();
        $records = $this->users->get_all_unapproved_users($searchA);
        $output['records'] = $records; 
        //pr($output); die;
        $html = $this->load->view('admin/users/unapproved_ajax_list',$output, true);
        // pr($html); die;
        $data['html'] = $html;
        $data['paging'] = $paging;
        $data['success']= true;
        json_output($data); 

	}

	public function add()
	{

			
		$output['page_title'] = 'Add Doctor';
		$output['left_menu'] = 'doctors_management';
		$output['left_submenu'] = 'add_doctors_list';
		$output['task'] = 'add';
		$output['id'] = '';
		$output['lecture'] = '';
		$output['first_name'] = '';
		$output['name'] = '';
		$output['road'] = '';
		$output['plzl'] = '';
		$output['datenspeicherung'] = '';
		$output['email'] = '';
		$output['ort'] = '';
		$output['hausnr'] = '';
		$output['phonenumber'] = '';
		$output['fax'] = '';
		$output['www'] = '';
		$output['name_doctor_practice'] = '';
		$output['subject'] = '';
		$output['lanr'] = '';
		$output['title'] = '';
		$output['videosprechstunde'] = '';
		$output['status'] = 'Active';


		if ($this->input->post()) {
			$success = true;
            $message = '';
			/*set validation rules*/
			$this->form_validation->set_rules('title', 'titel', 'trim|required');
			$this->form_validation->set_rules('first_name', 'Vorname', 'trim|required');
			$this->form_validation->set_rules('name', 'Nachname', 'trim|required');
			$this->form_validation->set_rules('road', 'Straße', 'trim|required');
			$this->form_validation->set_rules('hausnr', 'hausnr', 'trim|required');
			$this->form_validation->set_rules('plzl', 'Plzl', 'trim|required|numeric|max_length[5]');
			$this->form_validation->set_rules('ort', 'ort', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_members.email]');
			$this->form_validation->set_rules('www', 'WWW', 'trim|valid_url');
			$this->form_validation->set_rules('name_doctor_practice', 'Name Arztpraxis', 'trim|required');
			$this->form_validation->set_rules('subject', 'Fachrichtung', 'trim|required');
			$this->form_validation->set_rules('lanr', 'Lanr', 'trim|required|numeric|max_length[10]');
			$this->form_validation->set_rules('phonenumber', 'telenumber', 'trim|required|regex_match[^[0-9\-\+]{9,15}$^]');
			$this->form_validation->set_rules('fax', 'Fax', 'trim|numeric');
			$this->form_validation->set_rules('videosprechstunde', 'Videosprechstunde', 'trim');
			$this->form_validation->set_rules('datenspeicherung', 'Datenspeicherung', 'trim|required');

			if ($this->form_validation->run()) 
			{
				$input = array();
				$success = true;

				if (isset($_FILES['unterschrift']['tmp_name']) && $_FILES['unterschrift']['tmp_name']) {
                    $directory = './assets/uploads/unterschrift-images'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if ($this->upload->do_upload('unterschrift')) {
                        $image = $this->upload->data();
                        $file_name = $image['file_name'];
                        $input['unterschrift'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                        //pr($this->upload->display_errors());
                    }
                }/* else {
                    $message = 'Please select the Unterschrift';
                    $success = false;
                }*/

                if ($success) {

                	$complete_string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+=*&^%$@!"';
                	$setrandompassword = substr(str_shuffle($complete_string), 0, 10);
                	
					$input['lecture'] = $this->input->post('lecture', true);
					$input['name_doctor_practice'] = $this->input->post('name_doctor_practice', true);
					$input['name'] = ucfirst($this->input->post('name',true));
					$input['first_name'] =$this->input->post('first_name',true);
					//pr($input['first_name']);die;
					$input['subject'] = $this->input->post('subject', true);
					$input['lanr'] = $this->input->post('lanr', true);
					$input['title'] = $this->input->post('title', true);
					$input['road'] = $this->input->post('road', true);
					$input['hausnr'] = $this->input->post('hausnr', true);
					$input['email'] = $this->input->post('email', true);
					$new_random_password = encrpty_password($setrandompassword);
					$input['password'] = $new_random_password;
					$input['plzl'] = $this->input->post('plzl', true);
					$input['videosprechstunde'] = $this->input->post('videosprechstunde', true);
					$input['ort'] = $this->input->post('ort', true);
					$input['www'] = $this->input->post('www', true);
					$input['phonenumber'] = $this->input->post('phonenumber', true);
					$input['fax'] = $this->input->post('fax', true);
					$input['datenspeicherung'] = $this->input->post('datenspeicherung', true);
					$input['status'] = $this->input->post('status', true);
					$input['is_approved'] = 'Yes';
					$input['is_register_completed'] = 'Yes';
					$input['add_date'] = getDefaultToGMTDate(time());
					//pr($input); die;

					$insert_id = $this->users->add_doctors($input);

					if ($insert_id) {
						$success = true;
						$message = 'Doctor added Succesfully. A link is sent to the email with default password can be changed with given link in the mail';
						//$setpasswordURL = '<a href="'. base_url('doctors/set_new_password/'. $setpasswordkey) .'">Set Password</a>';

						$this->mailsend->SetUserPasswordMail($insert_id, $setrandompassword);
						$data['redirectURL'] = base_url('admin/users');
					}else
					{
						$success = false;
						$message = "Technical Error! Please Try Again later...";
					}
                }

			}
			else
			{
				$message = validation_errors();
				$success = false;
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);
		}

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/users/form');
		$this->load->view('admin/includes/footer');

	}

	public function check_email_exists($value, $id)
	{
		if($this->users->does_email_exists($value, $id))
		{
		$this->form_validation->set_message('check_email_exists', 'This email is already taken. Try some unqiue');
		return false;
		}
		return true;
	}

	public function update($id)
	{
		$records = $this->users->get_doctors_for_update($id);
		//pr($records); die;
		$output['records'] = $records;

		$output['page_title'] = "Edit Doctors";
		$output['left_menu'] = 'doctors_management';
		$output['left_submenu'] = 'update_doctors_list';
		$output['task'] = "edit";
		$output['id'] = $records;
		$output['lecture'] = $records->lecture;
		$output['first_name'] = $records->first_name;
		$output['name'] = $records->name;
		$output['road'] = $records->road;
		$output['plzl'] = $records->plzl;
		$output['email'] = $records->email;
		$output['hausnr'] = $records->hausnr;
		$output['datenspeicherung'] = $records->datenspeicherung;
		$output['ort'] = $records->ort;
		$output['phonenumber'] = $records->phonenumber;
		$output['fax'] = $records->fax;
		$output['www'] = $records->www;
		$output['name_doctor_practice'] = $records->name_doctor_practice;
		$output['subject'] = $records->subject;
		$output['lanr'] = $records->lanr;
		$output['title'] = $records->title;
		$output['status'] = $records->status;
		$output['videosprechstunde'] = $records->videosprechstunde;
		//$output['password'] = $records->password;

		if ($this->input->post())
		{
			$success = true;
            $message = '';
			/*set validation rules*/
			$this->form_validation->set_rules('title', 'titel', 'trim|required');
			$this->form_validation->set_rules('first_name', 'Vorname', 'trim|required');
			$this->form_validation->set_rules('name', 'Nachname', 'trim|required');
			$this->form_validation->set_rules('road', 'Straße', 'trim|required');
			$this->form_validation->set_rules('hausnr', 'hausnr', 'trim|required');
			$this->form_validation->set_rules('plzl', 'Plzl', 'trim|required|numeric');
			$this->form_validation->set_rules('ort', 'ort', 'trim|required');
			$this->form_validation->set_rules('name_doctor_practice', 'Name Arztpraxis', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('fax', 'Fax', 'trim|numeric');
			$this->form_validation->set_rules('www', 'WWW', 'trim|valid_url');
			$this->form_validation->set_rules('phonenumber', 'telenumber', 'trim|required');
			$this->form_validation->set_rules('lanr', 'Lanr', 'trim|required');
			$this->form_validation->set_rules('videosprechstunde', 'Videosprechstunde', 'trim');

			if ($this->form_validation->run()) 
			{
				$input = array();

				if (isset($_FILES['unterschrift']['tmp_name']) && $_FILES['unterschrift']['tmp_name']) {
                    $directory = './assets/uploads/unterschrift-images'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if ($this->upload->do_upload('unterschrift')) {
                        $image = $this->upload->data();
                        $file_name = $image['file_name'];
                        $input['unterschrift'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                        //pr($this->upload->display_errors());
                    }
                }

                if ($success) {
                	
					//$setpasswordkey = get_random_key(30);
					$input['first_name'] =$this->input->post('first_name',true);
					//pr($input['first_name']);die;
					$input['name'] = ucfirst($this->input->post('name',true));
					$input['road'] = $this->input->post('road', true);
					$input['email'] = $this->input->post('email', true);
					$input['phonenumber'] = $this->input->post('phonenumber', true);
					$input['plzl'] = $this->input->post('plzl', true);
					//$input['set_password_key'] = $setpasswordkey;
					$input['status'] = $this->input->post('status', true);
					$input['ort'] = $this->input->post('ort', true);
					$input['fax'] = $this->input->post('fax', true);
					$input['www'] = $this->input->post('www', true);
					$input['datenspeicherung'] = $this->input->post('datenspeicherung', true);
					$input['subject'] = $this->input->post('subject', true);
					$input['name_doctor_practice'] = $this->input->post('name_doctor_practice', true);
					$input['lanr'] = $this->input->post('lanr', true);
					$input['title'] = $this->input->post('title', true);
					$input['hausnr'] = $this->input->post('hausnr', true);
					$input['lecture'] = $this->input->post('lecture', true);
					$input['videosprechstunde'] = $this->input->post('videosprechstunde', true);
					//pr($input); die;

					$update_record = $this->users->set_updated_doctors_data($id, $input);

					if ($update_record) {
						$success = true;
						$message = 'Doctors Updated Succesfully';
						//$setpasswordURL = '<a href="'. base_url('doctors/set_new_password/'. $setpasswordkey) .'">Set Password</a>';

							//$this->mailsend->SetUserPasswordMail($id, $setpasswordkey, $setpasswordURL);
						$data['redirectURL'] = base_url('admin/users');
					} else {
						$success = false;
	                    $message = 'Technical error. Please try again later.';
					}
                }
			}
			else
			{
				$message = validation_errors();
				$success = false;
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);

		}

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/users/form');
		$this->load->view('admin/includes/footer');
	}

	function download_contract($id) {

		$records = $this->users->get_doctors_for_update($id);


		if ($records->contract_file) {
			$file_name = $records->contract_file.'.pdf';
			
			$this->load->helper('download');
	        $data = file_get_contents('assets/uploads/pdf/'.$file_name);

	        force_download($file_name, $data);
		} else {
			redirect('admin/users');
		}

	}

	public function delete()
	{
		$id = $this->input->post('record_id', true);
		$this->users->delete_user($id);
		$data['success'] = true;
		$data['message'] = "Record Deleted Successfully";
		$data['callBackFunction'] = 'callBackCommonDelete';
		$data['redirectURL'] = base_url('admin/users');
		json_output($data);
	}

	public function view($id)
	{
		$output['page_title'] = "View Details";
		$output['left_menu'] = 'doctors_management';
		$output['left_submenu'] = 'view_doctors_list';

		$view_record = $this->users->view_user($id);
		//pr($view_record); die;
		$output['usersdata'] = $view_record;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/users/view_user');
		$this->load->view('admin/includes/footer');
	}

	public function change_status()
	{
		$id = $this->input->get('id', true);
		$status = $this->input->get('status', true);
		$this->users->change_status_by_id($id, $status);

		$data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
	}

	public function change_approval_status()
	{
		$id = $this->input->get('id', true);
		$status = $this->input->get('status', true);
		$this->users->change_approval_status_by_id($id, $status);

		$setrandompassword = get_random_key(8);
		$new_random_password = encrpty_password($setrandompassword);
		$input['password'] = $new_random_password;
		$update_record = $this->users->set_updated_doctors_data($id, $input);

		if ($update_record) {
			
			$this->mailsend->SetUserPasswordMail($id, $setrandompassword);
			$data['success'] = true;
	        $data['message'] = 'Doctor is Approved Successfully';
	        json_output($data);
		} else {

			$data['success'] = false;
	        $data['message'] = 'Doctor was not Approved';
	        json_output($data); die;

		}

	}

	public function multi_task_operation()
    {
    	$task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $data_ids = explode(',',$ids);

        //print_r($ids); die;

        foreach ($data_ids as $key => $value) {
        	
        	if ($task == 'Delete') {        	
        	
        		$this->users->delete_user($value);
        		$message = "Selected rows deleted";
        	}
        	else if ($task == 'Active' || $task == 'Inactive') {
        		$this->users->change_status_by_id($value, $task);
        		$message = "Status changed of the selected Rows";
        		
        	} else if ($task == 'Yes') {

        		$this->users->change_approval_status_by_id($value, $task);
        		$message = "selected Doctors Approved";

        		$setrandompassword = get_random_key(8);
				$new_random_password = encrpty_password($setrandompassword);
				$input['password'] = $new_random_password;
				$update_record = $this->users->set_updated_doctors_data($value, $input);
				
				$this->mailsend->SetUserPasswordMail($value, $setrandompassword);
        	}

        }
    	$data['ids'] = $ids;
    	$data['success'] = true;
    	$data['message'] = $message;
    	$data['callBackFunction'] = 'callBackCommonDelete';
    	json_output($data);

    }
}

?>