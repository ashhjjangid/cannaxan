<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Zuordnung_sps extends CI_Controller {


    function __construct() {

        Parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/Zuordnung_model','zuordnung');
        
    }

   /*public function index() {
        $output['page_title'] = 'Zuordnung_sps';
        $output['left_menu'] = 'Zuordnung_sps_management';
        $output['left_submenu'] = 'Zuordnung_sps_list';

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/zuordnung/index');
        $this->load->view('admin/includes/footer');
    }

    public function get_ajax_list() {
        $this->load->library('pagination');
        $keyword = $this->input->get('keyword',true);
        $column_name = $this->input->get('column_name',true);
        $sorting_order = $this->input->get('sorting_order',true);
        $is_admin_approve = $this->input->get('is_admin_approve',true);
        $page_limit = $this->input->get('page_limit',true);
        $status = $this->input->get('status',true);
        $page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
        $page_no_index = ($page_no - 1) * $page_limit;
        $sQuery = '';

        if ($keyword) {
            $sQuery = $sQuery.'&keyword='.$keyword;
        }

        if ($column_name) {
            $sQuery = $sQuery.'&column_name='.$column_name;
        }

        if ($sorting_order) {
            $sQuery = $sQuery.'&sorting_order='.$sorting_order;
        }

        if ($page_limit) {
            $sQuery = $sQuery.'&page_limit='.$page_limit;
        }

        if ($status) {
            $sQuery = $sQuery.'&status='.$status;
        }
        if ($is_admin_approve) {
            $sQuery = $sQuery.'&is_admin_approve='.$is_admin_approve;
        }

        $searchData['search_index'] = $page_no_index;
        $searchData['limit']  = $page_limit;
        $searchData['keyword'] = $keyword;
        $searchData['column_name'] = $column_name;
        $searchData['sorting_order'] = $sorting_order;
        $searchData['status'] = $status;
        $config['base_url'] = site_url('admin/zuordnung/get_ajax_list?' . $sQuery);

        $total_rows = $this->zuordnung->count_zuordnung($searchData);
        $config['total_rows'] = $total_rows;
        $config["per_page"] = $page_limit;

        $this->pagination->initialize($config);
        $paging = $this->pagination->create_links();
        $records = $this->zuordnung->getAllzuordnung($searchData);
        $output['records'] = $records;
        $html = $this->load->view('admin/zuordnung/ajax_list', $output, true);
        $data['success'] = true;
        $data['html'] = $html;
        $data['paging'] = $paging;
        json_output($data);

    }*/

    function index() { 
        $output['page_title'] = 'Zuordnung SPA';
        $output['left_menu'] = 'Zuordnung_sps_management';
        $output['left_submenu'] = 'Zuordnung_sps_list';
        
        $static_pages = $this->zuordnung->getzuordnung();
        //pr($static_pages);die;
        $output['record'] = $static_pages;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/zuordnung/list');
        $this->load->view('admin/includes/footer');
    }

    public function update($id) 
    {   

        $record = $this->zuordnung->getzuordnungById($id);
        //pr($record);die;
        checkRequestedDataExists($record);

        $output['page_title'] = 'Zuordnung sps';
        $output['left_menu'] = 'Zuordnung_sps_management';
        $output['left_submenu'] = 'Zuordnung_sps_list';
        $output['task'] = 'edit';
        $output['message'] = '';
        $output['record'] = $record;
        $output['id'] = $record->id;
        $output['wirkstoff'] = $record->wirkstoff;
        $output['konzentration_thc_mg_ml'] = $record->konzentration_thc_mg_ml;
        $output['konzentration_cbd_mg_ml'] = $record->konzentration_cbd_mg_ml;
        $output['spruhstobe'] = $record->spruhstobe;
        $output['volumen_sps_ml'] = $record->volumen_sps_ml;
        $output['menge_thc_sps_mg'] = $record->menge_thc_sps_mg;
        $output['menge_cbd_sps_mg'] = $record->menge_cbd_sps_mg;
        $output['sps_20_ml_flasche'] = $record->sps_20_ml_flasche;
        $output['menge_thc_20_ml'] = $record->menge_thc_20_ml;
        $output['menge_cbd_20_ml'] = $record->menge_cbd_20_ml;
        $output['anzahl_spruhstobe_durchgang'] = $record->anzahl_spruhstobe_durchgang;
        
        if ($this->input->post()) {
            $message = '';
            $success = true;

            $this->form_validation->set_rules('wirkstoff', 'wirkstoff', 'trim|required');
            $this->form_validation->set_rules('konzentration_thc_mg_ml', 'konzentration_thc_mg_ml', 'trim|required');
            $this->form_validation->set_rules('konzentration_cbd_mg_ml', 'konzentration_cbd_mg_ml', 'trim|required');
            $this->form_validation->set_rules('spruhstobe', 'spruhstobe', 'required');
            $this->form_validation->set_rules('volumen_sps_ml', 'volumen_sps_ml', 'required');
            $this->form_validation->set_rules('menge_thc_sps_mg', 'menge_thc_sps_mg', 'trim|required');
            $this->form_validation->set_rules('menge_cbd_sps_mg', 'menge_cbd_sps_mg', 'trim|required');
            $this->form_validation->set_rules('sps_20_ml_flasche', 'sps_20_ml_flasche', 'trim|required');
            $this->form_validation->set_rules('menge_thc_20_ml', 'menge_thc_20_ml', 'trim|required');
            $this->form_validation->set_rules('menge_cbd_20_ml', 'menge_cbd_20_ml', 'trim|required');
            $this->form_validation->set_rules('anzahl_spruhstobe_durchgang', 'anzahl_spruhstobe_durchgang', 'trim|required');

            if ($this->form_validation->run()) {

                 $input = array();
                 $success =  true;
                 $message = '';
                
                    $input['wirkstoff'] = $this->input->post('wirkstoff', true);
                    $input['konzentration_thc_mg_ml'] = $this->input->post('konzentration_thc_mg_ml');
                    $input['konzentration_cbd_mg_ml'] = $this->input->post('konzentration_cbd_mg_ml', true);
                    $input['spruhstobe'] = $this->input->post('spruhstobe', true);
                    $input['volumen_sps_ml'] = $this->input->post('volumen_sps_ml', true);
                    $input['menge_thc_sps_mg'] = $this->input->post('menge_thc_sps_mg', true);
                    $input['menge_cbd_sps_mg'] = ucfirst($this->input->post('menge_cbd_sps_mg', true));
                    $input['sps_20_ml_flasche'] = ucfirst($this->input->post('sps_20_ml_flasche', true));
                    $input['menge_thc_20_ml'] = $this->input->post('menge_thc_20_ml', true);
                    $input['menge_cbd_20_ml'] = $this->input->post('menge_cbd_20_ml', true);
                    $input['anzahl_spruhstobe_durchgang'] = $this->input->post('anzahl_spruhstobe_durchgang', true);
                    $input['update_date'] = getDefaultToGMTDate(time());
                    //pr($input); die;
                    $response = $this->zuordnung->setzuordnungById($id, $input);

                    if ($response) {

                        $success = true;
                        $message = 'Details of Zuordnung_sps updated successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = site_url('admin/zuordnung_sps');

                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
            
            } else {
                $success = false;
                $message = validation_errors();        
            }    
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/zuordnung/form');
        $this->load->view('admin/includes/footer');
    }

    
}