<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Faq class
 */
class Promotion_email_user extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		$this->load->model('admin/Promotion_email_user_model', 'useremail');
	}

	public function index()
	{
		$output['page_title'] = "List of promotion email user";
		$output['left_menu'] = "promotion email user management";
		$output['left_submenu'] = "promotion_email_user_list";
		$list = $this->useremail->get_data();
		//print_r($list);die;
        $output['records'] = $list;
		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/pro_email_user/list');
		$this->load->view('admin/includes/footer');
	}  
 
	public function add()
	{

		$list = $this->useremail->get_email_user();
		//print_r($list);die;
		$output['get_data'] = $list;	
		$output['page_title'] = "add promotion email user";
		$output['left_menu'] = "promotion email user management";
		$output['left_submenu'] = "promotion_email_user_list";
		$output['task'] = 'add';
		$output['id'] = '';
		$output['promotion_email_id'] = '';
		$output['status'] = 'Active';
		$output['user_id'] = ''; 

        if ($this->input->post()) {
			$success = true;
            $message = '';
			/*set validation rules*/
			$this->form_validation->set_rules('user_id','User_id', 'trim|required');
			$this->form_validation->set_rules('promotion_email_id', 'promotion_email_id', 'trim|required');
			if ($this->form_validation->run()) 
			{
				$input = array();
				$input['user_id'] = $this->input->post('user_id', true);
				$input['promotion_email_id'] = $this->input->post('promotion_email_id', true);
				$input['status'] = $this->input->post('status', true);
				$insert_id = $this->useremail->add_pro_email_user($input);

				if ($insert_id) {
					$success = true;
					$message = 'Promotion email user added Succesfully';
					$data['redirectURL'] = base_url('admin/Promotion_email_user');
				} else {
					$success = false;
					$message = "Technical Error! Please Try Again later.";
				}

			}
			else
			{
				$message = validation_errors();
				$success = false;
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);
		}

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/pro_email_user/email_form');
		$this->load->view('admin/includes/footer');

	}

	public function update($id)
	{
		$records = $this->useremail->get_email_for_update($id);
		//pr($records); die;
		$output['records'] = $records;
		$output['page_title'] = "update promotion email user";
		$output['left_menu'] = "promotion email user management";
		$output['left_submenu'] = "promotion_email_user_list";
		$output['task'] = "edit";
		$output['id'] = $records->id; 
		$output['user_id'] = $records->user_id; 
		$output['promotion_email_id'] = $records->promotion_email_id;
		$output['status'] = $records->status;
		$list = $this->useremail->get_email_user();
		$output['get_data'] = $list;	
		if ($this->input->post())
		{
			$success = true;
            $message = '';
			/*set validation rules*/
			$this->form_validation->set_rules('user_id', 'User id', 'trim|required');
			$this->form_validation->set_rules('promotion_email_id', 'promotion_email_id', 'trim|required');
			if ($this->form_validation->run()) 
			 {
				$input = array();
				$input['user_id'] = $this->input->post('user_id', true);
				$input['promotion_email_id'] = $this->input->post('promotion_email_id', true);
				$input['status'] = $this->input->post('status', true);

				$update_record = $this->useremail->set_updated_email($id, $input);

				if ($update_record) {
					$success = true;
					$message = 'Promotion email user  Updated Succesfully';
					$data['redirectURL'] = base_url('admin/Promotion_email_user');
				} else {
					$success = false;
					$message = 'Technical error. Please try again.';
				}

			}
			else
			{
				$message = validation_errors();
				$success = false;
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);

		}

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/pro_email_user/email_form');
		$this->load->view('admin/includes/footer');
	}

	public function view($id)
	{
		$output['page_title'] = "View promotion email user";
		$output['left_menu'] = "promotion email user management";
		$output['left_submenu'] = "promotion_email_user_list";
		$view_record = $this->useremail->view_users($id);
		//print_r($view_record); die;

		$output['usersdata'] = $view_record;
		

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/pro_email_user/view');
		$this->load->view('admin/includes/footer');
	}

	public function change_status()
	{
		$id = $this->input->get('id', true);
		$status = $this->input->get('status', true);
		$this->useremail->change_status_by_id($id, $status);

		$data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
	}
	

	public function multi_task_operation()
    {
    	$task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $data_ids = explode(',',$ids);

        foreach ($data_ids as $key => $value) {
        	
        	if ($task == 'Delete') {        	
        	
        	    $this->useremail->delete_user($value);
        		$message = "Selected rows deleted";
        	}
        	else if ($task == 'Active' || $task == 'Inactive') {
        		$this->useremail->change_status_by_id($value, $task);
        		$message = "Status changed of the selected Rows";
        	}

        }
    	$data['ids'] = $ids;
    	$data['success'] = true;
    	$data['message'] = $message;
    	$data['callBackFunction'] = 'callBackCommonDelete';
    	json_output($data);

    }
}

?>
