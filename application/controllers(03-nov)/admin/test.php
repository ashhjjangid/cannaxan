<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users class.
 * 
 * @extends CI_Controller
 */

class Users extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */
       
    function __construct() {
        Parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/users_model', 'usersmodel');
    }

    /**
     * index function.
     * 
     * @access public
     * @return null
     */

    function index() {
        $output['page_title'] = 'Users';
        $output['left_menu'] = 'user_management';
        $output['left_submenu'] = 'users_list';

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/users/users_list');
        $this->load->view('admin/includes/footer');
    }

    /**
     * get_ajax_list function.
     * 
     * @access public
     * @return list of all customers
     */

    public function get_ajax_list()
    {
        $this->load->library('pagination');

        $keyword = $this->input->get('keyword',true);
        $column_name = $this->input->get('column_name',true);
        $sorting_order = $this->input->get('sorting_order',true);
        $is_delete = $this->input->get('is_delete', true);
        //$is_admin_approve = $this->input->get('is_admin_approve',true);
        $page_limit = $this->input->get('page_limit',true);
        $status = $this->input->get('status',true);
        $page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
        $page_no_index = ($page_no - 1) * $page_limit;
        $sQuery = '';

        if ($keyword) {
            $sQuery = $sQuery.'&keyword='.$keyword;
        }

        if ($column_name) {
            $sQuery = $sQuery.'&column_name='.$column_name;
        }

        if ($sorting_order) {
            $sQuery = $sQuery.'&sorting_order='.$sorting_order;
        }

        if ($status) {
            $sQuery = $sQuery.'&status='.$status;
        }
        /*if ($is_delete) {
            $sQuery = $sQuery.'&is_delete='.$is_delete;
        }*/

        $searchData['search_index'] = $page_no_index;
        $searchData['limit']  = $page_limit;
        $searchData['keyword'] = $keyword;
        $searchData['column_name'] = $column_name;
        $searchData['sorting_order'] = $sorting_order;
        $searchData['status'] = $status;
        $config['base_url'] = base_url('admin/users/get_ajax_list?' . $sQuery);
       
        $total_rows = $this->usersmodel->countUsers($searchData);
        $config['total_rows'] = $total_rows;
        $this->pagination->initialize($config);

        $paging = $this->pagination->create_links();

        $records = $this->usersmodel->get_all_users($searchData);
        $output['users'] = $records;
        $html = $this->load->view('admin/users/ajax_list', $output, true);
        $data['success'] = true;
        $data['html'] = $html;
        $data['paging'] = $paging;
        // pr($data); die;

        json_output($data);
    }

    /**
     * add function.
     * 
     * @access public
     * @return null
     */

    function add() {
        $output['page_title'] = 'Add User';
        $output['left_menu'] = 'user_management';
        $output['left_submenu'] = 'users_list';
        $output['message'] = '';
        $output['task'] = 'add';
        $output['status'] = 'Active';
        $output['gender'] = 'male';
        $output['id'] = '';
        $output['first_name'] = '';
        $output['last_name'] = '';
        $output['username'] = '';
        $output['email'] = '';
        $output['phonenumber'] = '';
        $output['is_delete'] = 'No';

        if ($this->input->post()) {
            $success = true;
            $message = '';
            /* set validation rules */
            $this->form_validation->set_rules('first_name', 'first name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('last_name', 'last name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('username', 'User name', 'alpha_numeric_spaces|trim|required|is_unique[tbl_users.username]');
            $this->form_validation->set_rules('email', 'email address', 'trim|required|valid_email|is_unique[tbl_users.email]');
            $this->form_validation->set_rules('phonenumber', 'phone number', 'trim|min_length[6]|max_length[20]|regex_match[/^[+0-9]+$/]');
            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]|max_length[20]');
            //$this->form_validation->set_rules('latitude', 'valid address latitude', 'required');
            //$this->form_validation->set_rules('longitude', 'valid address longitude', 'required');

            if ($this->form_validation->run()) {
                $banner_image='';
                $input = array();
                $error = 0;
                               
                if (isset($_FILES['profile']['name']) && $_FILES['profile']['name']) {
                    $directory = './assets/uploads/user_images/'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'gif|jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if ($this->upload->do_upload('profile')) {
                        $image_data = $this->upload->data();
                        $file_name = $image_data['file_name'];
                        $input['image'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                    }
                }

                if ($success) {
                    $email = $this->input->post('email');
                    $first_name = ucfirst($this->input->post('first_name',true));
                    $last_name = ucfirst($this->input->post('last_name',true));
                    $username = $this->input->post('username', true);

                    $password = $this->input->post('password',true);
                    $ency_password = encrpty_password($password);
                    $input['password'] = $ency_password;

                    $input['first_name'] = ucfirst($this->input->post('first_name',true));
                    $input['last_name'] = ucfirst($this->input->post('last_name',true));
                    $input['username'] = $this->input->post('username', true);
                    $input['email'] = $this->input->post('email');
                    $input['phone_number'] = $this->input->post('phonenumber',true);
                    $input['gender'] = $this->input->post('gender',true);
                    $input['status'] = $this->input->post('status',true);
                    $input['is_delete'] = $this->input->post('is_delete', true);
                    /*insert as customer user */
                    $insert_id = $this->usersmodel->add_user($input);
                    
                    if ($insert_id) {
                        $success = true;
                        $message = 'New User added successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = site_url('admin/users');
                    
                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
                }
            
            } else {
                $message.= validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        } 
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/users/form');
        $this->load->view('admin/includes/footer');
    }

    /**
     * check_email_exist function.
     * 
     * @access public
     * @param mixed $value
     * @param mixed $id
     * @return true or false
     */

    public function check_email_exist($value, $id = '') {
        
        if ($this->usersmodel->doesEmailExist( $value, $id )) {
            $this->form_validation->set_message('check_email_exist', 'This E-Mail address already register on our server.');
            return false;
        }
        return true;
    }

    /**
     * check_username_exist function.
     * 
     * @access public
     * @param mixed $value
     * @param mixed $id     
     * @return true or false
     */

    public function check_username_exist($value, $id = '') {
        
        if ($this->usersmodel->doesUsernameExist( $value, $id )) {
            $this->form_validation->set_message('check_email_exist', 'This User name already register on our server.');
            return false;
        }
        return true;
    }

    /**
     * update function.
     * 
     * @access public
     * @param mixed $id     
     * @return null
     */

    public function update($id) {
        $output['page_title'] = 'Update users';
        $output['left_menu'] = 'user_management';
        $output['left_submenu'] = 'users_list';
        $output['task'] = 'edit';
        $output['message'] = '';
        /* get customer record*/
        $record = $this->usersmodel->get_user_by_id($id);
        checkRequestedDataExists($record);
        /* assign values */
        $output['record'] = $record;
        $output['status'] = $record->status;
        $output['id'] = $record->id;
        $output['first_name'] = $record->first_name;
        $output['last_name'] = $record->last_name;
        $output['username'] = $record->username;
        $output['email'] = $record->email;
        $output['phonenumber'] = $record->phone_number;
        $output['gender'] = $record->gender;
        $output['password'] = $record->password;
        $output['status'] = $record->status;
        $output['is_delete'] = $record->is_delete;

        if ($this->input->post()) {
            $message = '';
            $success = true;
            /* set validation rules */
            $this->form_validation->set_rules('first_name', 'first name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('last_name', 'last name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('username', 'Username', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('email', 'email address', 'trim|required|valid_email|callback_check_email_exist['.$id.']');
            $this->form_validation->set_rules('phonenumber', 'Phone Number', 'trim|min_length[6]|max_length[20]|regex_match[/^[+0-9]+$/]');
            //$this->form_validation->set_rules('latitude', 'valid address latitude', 'required');
            //$this->form_validation->set_rules('longitude', 'valid address longitude', 'required');
        
            if ($this->form_validation->run()) {

                $update_arr = array();
                $error  = 0;

                if (isset($_FILES['profile']['name']) && $_FILES['profile']['name']) {
                    $directory = './assets/uploads/user_images/'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'gif|jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if ($this->upload->do_upload('profile')) {
                        $image_data = $this->upload->data();
                        $file_name = $image_data['file_name'];
                        $input['image'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                    }
                    
                }

                if ($success) {
                    $input['first_name'] = ucfirst($this->input->post('first_name',true));
                    $input['last_name'] = ucfirst($this->input->post('last_name',true));
                    $input['username'] = $this->input->post('username', true);
                    $input['email'] = $this->input->post('email',true);
                    $input['phone_number'] = $this->input->post('phonenumber',true);
                    $input['gender'] = $this->input->post('gender',true);
                    $input['status'] = $this->input->post('status',true);
                    $input['is_delete'] = $this->input->post('is_delete', true);
                    /* update records */
                    $response = $this->usersmodel->set_user_by_id($id,$input);

                    if ($response) {
                        $success = true;
                        $message = 'Users updated successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = site_url('admin/users');
                    
                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
                }
            
            } else {
                $success = false;
                $message = validation_errors();     
            }   
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/users/form');
        $this->load->view('admin/includes/footer');
    }

    /**
     * view function.
     * 
     * @access public
     * @param mixed $id      
     * @return row of customer type user
     */

    public function view($id) {
        $output['page_title'] = 'View Users';
        $output['left_menu'] = 'user_management';
        $output['left_submenu'] = 'users_list';
        /* get record by id */
        $record = $this->usersmodel->get_user_by_id($id);
        checkRequestedDataExists($record);
        $output['usersdata'] = $record; 

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/users/view_user');
        $this->load->view('admin/includes/footer');
    }


    /**
     * changeStatus function.
     * 
     * @access public
     * @return null
     */

    public function change_status()  {
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
        $this->usersmodel->change_status_by_id($id,$status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    
    /**
     * delete function.
     * 
     * @access public
     * @return null
     */

    public function delete() {


        $id = $this->input->post('record_id',true);
        $this->usersmodel->delete_user_by_id($id);

        $data['success'] = true;
        $data['message'] = 'Users has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }



    /**
     * multiTaskOperation function.
     * 
     * @access public
     * @return null
     */

    public function multi_task_operation() 
    {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        
        foreach ($dataIds as $key => $value) {
            
            if ($task == 'Delete') {
                $this->usersmodel->delete_user_by_id($value);
                $message = 'Selected Users has been deleted successfully.';
            
            } else  if ($task=='Active' || $task=='Inactive') {
                $this->usersmodel->change_status_by_id($value,$task);            
                $message = 'Status of selected Users has been changed successfully.';
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data); 
    }

    /**
     * check_space function.
     * 
     * @access public
     * @param mixed $value 
     * @return true or false
     */

    public function check_space($value){
        
        if ( ! preg_match("/^[a-zA-Z'0-9_@#$^&%*)(_+}{;:?\/., -]*$/", $value) ) {
           $this->form_validation->set_message('check_space', 'The %s field should contain only letters, numbers or periods');
           return false;
        }
        else
        return true;
    }

}
