<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Mail_schedule class.
 * 
 * @extends CI_Controller
 */
class Mail_schedule extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

	function __construct() {
		Parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		$this->load->model('admin/mail_schedule_model', 'mail_schedule');
		$this->load->model('admin/mail_templates_model', 'mail_templates');
		$this->load->model('admin/user_model', 'user');
		$this->user_id = $this->session->userdata('admin_id');
	}

    /**
     * index function.
     * 
     * @access public
     * @return record of all templates
     */

	function index($id) {
		$emailTemplate = $this->mail_templates->getSingleTemplateById($id);

        if(!$emailTemplate) {
        	redirect('admin/mail_templates/');
        }

		$output['page_title'] = 'List of Schedule Emails';
		$output['left_menu'] = 'Mail_templates';
		$output['left_submenu'] = 'Mail_templates_list';

		$schedule = $this->mail_schedule->getAllScheduleEmails($id);
		$output['schedule'] = $schedule;
		$output['template_id'] = $id;
		
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/mail_schedule/index');
		$this->load->view('admin/includes/footer');
	}

    /**
     * changeStatus function.
     * 
     * @access public
     * @return null
     */

	function changeStatus() {
		$id = $this->input->get('id',true);
		$status = $this->input->get('status',true);
		$this->mail_schedule->changeStatusById($id,$status);
		$data['success'] = true;
		$data['message'] = 'Record updated Successfully';
		json_output($data);
	} 	

    /**
     * multiTaskOperation function.
     * 
     * @access public
     * @return null
     */	

	function multiTaskOperation() {
		$task = $this->input->post('task',true);
		$ids = $this->input->post('ids',true);
		$dataIds = explode(',',$ids);

		foreach ($dataIds as $key => $value) 
		{
            if ($task == 'Delete') {
                $this->mail_schedule->deleteSchdeuleById($value);
                $message = 'Selected Schedules has been deleted successfully.';
            
            } else if($task=='Active' || $task=='Inactive'){
				$this->mail_schedule->changeStatusById($value,$task);			
				$message = 'Status of selected records changed successfully.';
			}
		}	

		$data['ids'] = $ids;
		$data['success'] = true;
		$data['message'] = $message;
		$data['callBackFunction'] = 'callBackCommonDelete';
		json_output($data);
	}

    /**
     * Sendemail function.
     * 
     * @access public
     * @param mixed $id
     * @return null
     */	

	function Send_email($id) {
		$output['page_title'] = 'List of Schedulled Emails';
		$output['left_menu'] = 'Mail_templates';
		$output['left_submenu'] = 'Mail_templates_list';

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/mail_schedule//send_email_list');
		$this->load->view('admin/includes/footer');
	}

	/**
     * view function.
     * 
     * @access public
     * @param mixed $id      
     * @return row of scheduled email
     */

    public function view($id) {
		$output['page_title'] = 'List of Schedule Emails';
		$output['left_menu'] = 'Mail_templates';
		$output['left_submenu'] = 'Mail_templates_list';
        /* get record by id */
        $record = $this->mail_schedule->getSingleScheduleById($id);
        checkRequestedDataExists($record);
        $selected_users = $this->mail_schedule->getAllScheduleUsers($id);

        $output['records'] = $record; 
        $output['selected_users'] = $selected_users;
        $output['template_id'] = $record->template_id; 

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/mail_schedule/view');
		$this->load->view('admin/includes/footer');
    }

    /**
     * update function.
     * 
     * @access public
     * @param mixed $id
     * @return null
     */

	function update($id) {

		$output['page_title'] = 'Update Email Schedule';
		$output['left_menu'] = 'Mail_templates';
		$output['left_submenu'] = 'Mail_templates_add';
		$output['message'] ='';
		$users = $this->user->getAllUsers();

        $record = $this->mail_schedule->getSingleScheduleById($id);
        checkRequestedDataExists($record);
        $users = $this->user->getAllUsers();

		$output['users_list'] = $users;
		$output['template_id'] = $record->template_id;

		$selected_users = $this->mail_schedule->getAllScheduleUsers($id);
        $all_users = array();

		if($selected_users) {
			 foreach($selected_users as $users) {
			 	$all_users[] = $users->user_id;
			 }
		}

		$output['selected_users'] = $all_users;
		$output['time_slot'] = $record->scheduled_time;
		$output['id'] = $id;

		if (isset($_POST) && !empty($_POST)) {	
			$this->form_validation->set_rules('time_slot', 'time slot', 'trim|required');			
			$this->form_validation->set_rules('users[]', 'users', 'trim|required');
			
			if ($this->form_validation->run()) {

				$time_slot = $this->input->post('time_slot',true);
				$result = $this->mail_schedule->setTimeSchedule($id,$time_slot);

				if($result) {
                    $this->mail_schedule->deleteEmailScheduleUsers($id);
                    /* get user array */
					$users = $this->input->post('users',true);

					foreach($users as $user_id) {
						$input_user = array();
						$input_user['user_id'] = $user_id;
						$input_user['schedule_id'] = $id;

						$this->mail_schedule->addEmailScheduleUsers($input_user);
				    }

					$message = 'Record Updated Successfully';
					$this->session->set_flashdata('message', $message);
					$success = true;
					$data['redirectURL'] = site_url('admin/mail_schedule/'.$record->template_id);

				} else {
					$success = false;
					$message = 'Technical error! please try again';					
				}
			} else {
				$success = false;
				$message = validation_errors();
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);
		}

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/mail_schedule/form');
		$this->load->view('admin/includes/footer');
	}

    /**
     * add function.
     * 
     * @access public
     * @param mixed $id
     * @return null
     */

	function add($id) {
		$output['page_title'] = 'Add Email Schedule';
		$output['left_menu'] = 'Mail_templates';
		$output['left_submenu'] = 'Mail_templates_add';
		$output['message'] ='';
		$output['id'] ='';
		
        $emailTemplate = $this->mail_templates->getSingleTemplateById($id);

        if(!$emailTemplate) {
        	redirect('admin/mail_templates/');
        }

		$users = $this->user->getAllUsers();
		$output['users_list'] = $users;
		$output['template_id'] = $id;
		$output['selected_users'] = ''; 
		$output['time_slot'] = '';
		
		if (isset($_POST) && !empty($_POST)) {	
			$this->form_validation->set_rules('time_slot', 'time slot', 'trim|required');			
			$this->form_validation->set_rules('users[]', 'users', 'trim|required');
			
			if ($this->form_validation->run()) {

				$input = array();
				$input['scheduled_time'] = $this->input->post('time_slot',true);
				$input['template_id'] = $id;

				$insert_id = $this->mail_schedule->addEmailSchedule($input);

				if($insert_id) {

					$users = $this->input->post('users',true);

					foreach($users as $user_id) {
						$input_user = array();
						$input_user['user_id'] = $user_id;
						$input_user['schedule_id'] = $insert_id;

						$this->mail_schedule->addEmailScheduleUsers($input_user);
				    }

				} else {
					$success = false;
					$message = 'Technical error! please try again';					
				}

				$message = 'Record added Successfully';
				$this->session->set_flashdata('message', $message);
				$success = true;
				$data['redirectURL'] = site_url('admin/mail_schedule/'.$id);
			
			} else {
				$success = false;
				$message = validation_errors();
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);
		}

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/mail_schedule/form');
		$this->load->view('admin/includes/footer');
	}

	/**
     * delete function.
     * 
     * @access public
     * @return null
     */

    public function delete() {
        $id = $this->input->post('record_id',true);
        $deleted = $this->mail_schedule->deleteSchdeuleById($id);
        /* delete scheduled users */
        if($deleted) {
            $this->mail_schedule->deleteEmailScheduleUsers($id);	
        }

        $data['success'] = true;
        $data['message'] = 'Schedule has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }	
}