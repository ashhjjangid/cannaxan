<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ride_history class. 
 * @extends CI_Controller
 */

class Ride_history extends CI_Controller {

   /**
    * __construct function
    * @access public
    * @return void
   */
       
    function __construct() {
        parent:: __construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/Ride_history_model', 'history');
                
    }

    /**
     * index function.
     * @access public
     * @return null
     */

  public function index()
  {
    $output['page_title']="Ride History";
    $output['left_menu']="Ride_management";
    $output['left_submenu']="Ride_list";

    $list = $this->history->get_list_of_rides();
    $output['records']=$list;
    //pr($output['rating_records']);die;
    $this->load->view('admin/includes/header',$output);
    $this->load->view('admin/history/history_page_list');
    $this->load->view('admin/includes/footer');
  }

    /**
     * changeStatus function.
     * @access public
     * @param mixed $id
     */

    public function change_status()
    {
        $id = $this->input->get('id', true);
        $status = $this->input->get('status', true);
        //print_r($id);
        $this->history->change_status_by_id($id, $status);

        $data['success'] = true;
        $data['message'] = 'Status of Review updated Successfully';
        json_output($data);
    }
    /**
     * multi_task_operation function
     * @access public
     * @return null
     */

    public function multi_task_operation()
    {
        $task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $reviewsids = explode(',', $ids);
        //pr($reviewsids); die;
        foreach ($reviewsids as $value) 
        {
            if ($task == 'Delete') 
            {
                //pr($value);die('here');
                $this->history->delete_rating($value);
                $message = 'Selected Reviews deleted Successfully';
            } 
            else if ($task == 'Active' || $task == 'Inactive') 
            {   
                $this->history->change_status_by_id($value, $task);
                $message = 'Status of selected Reviews has been Changed Successfully';
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }
    
    public function delete() {
        $id = $this->input->post('record_id',true);
        $this->history->delete_rating($id);

        $data['success'] = true;
        $data['message'] = 'Rating has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }

    public function view($id)
    {        
        $output['page_title']="Ride History";
        $output['left_menu']="Ride_management";
        $output['left_submenu']="Ride_list";

        $review_records = $this->history->view_single_review($id);
        $output['review_records'] = $review_records;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/history/view_user');
        $this->load->view('admin/includes/footer');
    } 

}