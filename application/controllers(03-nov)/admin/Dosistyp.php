<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Dosistyp extends CI_Controller {


    function __construct() {

        Parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/Dosistyp_model','dosistyp');
        
    }

    public function index() {
        $output['page_title'] = 'Dosistyp';
        $output['left_menu'] = 'dosistyp_management';
        $output['left_submenu'] = 'dosistyp_list';

        $record = $this->dosistyp->getdosistyp();
        checkRequestedDataExists($record);
        $output['record'] = $record;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/dosistyp/list');
        $this->load->view('admin/includes/footer');
    }

    public function add() 
    {  

        $output['page_title'] = 'Add Dosistyp';
        $output['left_menu'] = 'dosistyp_management';
        $output['left_submenu'] = 'dosistyp_add';
        $output['message'] = '';
        $output['tag_name'] = '';
        $output['id'] = '';
        $output['morgens_anzahl_durchgange'] = '';
        $output['morgens_anzahl_sps'] = '';
        $output['mittags_anzahl_durchgange'] = '';
        $output['mittags_anzahl_sps'] = '';
        $output['abends_anzahl_durchgange'] = '';
        $output['abends_anzahl_sps'] = '';
        $output['nachts_anzahl_durchgange'] = '';
        $output['nachts_anzahl_sps'] = '';
        $output['thc_morgens_mg'] = '';
        $output['thc_mittags_mg'] = '';
        $output['thc_abends_mg'] = '';
        $output['thc_nachts_mg'] = '';
        $output['summe_thc_mg'] = '';
        $output['display_order'] = '';
        $output['status'] = 'Active';
        
        if ($this->input->post()) {
            $message = '';
            $success = true;

            $this->form_validation->set_rules('tag_name', 'tag_name', 'trim|required');
            $this->form_validation->set_rules('morgens_anzahl_durchgange', 'morgens_anzahl_durchgange', 'trim|required');
            $this->form_validation->set_rules('morgens_anzahl_sps', 'morgens_anzahl_sps', 'trim|required');
            $this->form_validation->set_rules('mittags_anzahl_durchgange', 'mittags_anzahl_durchgange', 'required');
            $this->form_validation->set_rules('mittags_anzahl_sps', 'mittags_anzahl_sps', 'required');
            $this->form_validation->set_rules('abends_anzahl_durchgange', 'abends_anzahl_durchgange', 'trim|required');
            $this->form_validation->set_rules('abends_anzahl_sps', 'abends_anzahl_sps', 'trim|required');
            $this->form_validation->set_rules('nachts_anzahl_durchgange', 'nachts_anzahl_durchgange', 'trim|required');
            $this->form_validation->set_rules('nachts_anzahl_sps', 'nachts_anzahl_sps', 'trim|required');
            $this->form_validation->set_rules('thc_morgens_mg', 'thc_morgens_mg', 'trim|required');
            $this->form_validation->set_rules('thc_abends_mg', 'thc_abends_mg', 'trim|required|is_natural');

            if ($this->form_validation->run()) {

                 $input = array();
                 $success =  true;
                 $message = '';
                
                    $input['tag_name'] = $this->input->post('tag_name', true);
                    $input['morgens_anzahl_durchgange'] = $this->input->post('morgens_anzahl_durchgange');
                    $input['morgens_anzahl_sps'] = $this->input->post('morgens_anzahl_sps', true);
                    $input['mittags_anzahl_durchgange'] = $this->input->post('mittags_anzahl_durchgange', true);
                    $input['mittags_anzahl_sps'] = $this->input->post('mittags_anzahl_sps', true);
                    $input['abends_anzahl_durchgange'] = $this->input->post('abends_anzahl_durchgange', true);
                    $input['abends_anzahl_sps'] = ucfirst($this->input->post('abends_anzahl_sps', true));
                    $input['nachts_anzahl_durchgange'] = ucfirst($this->input->post('nachts_anzahl_durchgange', true));
                    $input['nachts_anzahl_sps'] = $this->input->post('nachts_anzahl_sps', true);
                    $input['thc_morgens_mg'] = $this->input->post('thc_morgens_mg', true);
                    $input['thc_abends_mg'] = $this->input->post('thc_abends_mg', true);
                    $input['thc_nachts_mg'] = ucfirst($this->input->post('thc_nachts_mg', true));
                    $input['thc_mittags_mg'] = $this->input->post('thc_mittags_mg', true);
                    $input['summe_thc_mg'] = $this->input->post('summe_thc_mg', true);
                    $input['display_order'] = $this->input->post('display_order', true);
                    $input['status'] = $this->input->post('status', true);
                    $input['add_date'] = getDefaultToGMTDate(time());
                    //pr($input); die;
                    $insert_id = $this->dosistyp->add_new_dosistyp($input);



                    if ($insert_id) {

                        $success = true;
                        $message = 'Details of dosistyp added successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = base_url('admin/dosistyp');

                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
            
            } else {
                $success = false;
                $message = validation_errors();        
            }    
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/dosistyp/form');
        $this->load->view('admin/includes/footer');
    }

    public function update($id) 
    {   

        $record = $this->dosistyp->getdosistypById($id);
        checkRequestedDataExists($record);

        $output['page_title'] = 'Update Dosistyp';
        $output['left_menu'] = 'dosistyp_management';
        $output['left_submenu'] = 'dosistyp_update';
        $output['message'] = '';
        $output['record'] = $record;
        $output['tag_name'] = $record->tag_name;
        $output['id'] = $record->id;
        $output['morgens_anzahl_durchgange'] = $record->morgens_anzahl_durchgange;
        $output['morgens_anzahl_sps'] = $record->morgens_anzahl_sps;
        $output['mittags_anzahl_durchgange'] = $record->mittags_anzahl_durchgange;
        $output['mittags_anzahl_sps'] = $record->mittags_anzahl_sps;
        $output['abends_anzahl_durchgange'] = $record->abends_anzahl_durchgange;
        $output['abends_anzahl_sps'] =$record->abends_anzahl_sps;
        $output['nachts_anzahl_durchgange'] = $record->nachts_anzahl_durchgange;
        $output['nachts_anzahl_sps'] = $record->nachts_anzahl_sps;
        $output['thc_morgens_mg'] =  $record->thc_morgens_mg ;
        $output['thc_mittags_mg'] = $record->thc_mittags_mg;
        $output['thc_abends_mg'] = $record->thc_abends_mg;
        $output['thc_nachts_mg'] = $record->thc_nachts_mg;
        $output['summe_thc_mg'] = $record->summe_thc_mg;
        $output['display_order'] = $record->display_order;
        $output['status'] = $record->status;
        
        if ($this->input->post()) {
            $message = '';
            $success = true;

            $this->form_validation->set_rules('tag_name', 'tag_name', 'trim|required');
            $this->form_validation->set_rules('morgens_anzahl_durchgange', 'morgens_anzahl_durchgange', 'trim|required');
            $this->form_validation->set_rules('morgens_anzahl_sps', 'morgens_anzahl_sps', 'trim|required');
            $this->form_validation->set_rules('mittags_anzahl_durchgange', 'mittags_anzahl_durchgange', 'required');
            $this->form_validation->set_rules('mittags_anzahl_sps', 'mittags_anzahl_sps', 'required');
            $this->form_validation->set_rules('abends_anzahl_durchgange', 'abends_anzahl_durchgange', 'trim|required');
            $this->form_validation->set_rules('abends_anzahl_sps', 'abends_anzahl_sps', 'trim|required');
            $this->form_validation->set_rules('nachts_anzahl_durchgange', 'nachts_anzahl_durchgange', 'trim|required');
            $this->form_validation->set_rules('nachts_anzahl_sps', 'nachts_anzahl_sps', 'trim|required');
            $this->form_validation->set_rules('thc_morgens_mg', 'thc_morgens_mg', 'trim|required');
            $this->form_validation->set_rules('thc_abends_mg', 'thc_abends_mg', 'trim|required');
             $this->form_validation->set_rules('summe_thc_mg', 'summe_thc_mg', 'trim|required');
            $this->form_validation->set_rules('display_order', 'display_order', 'trim|required');

            if ($this->form_validation->run()) {

                 $input = array();
                 $success =  true;
                 $message = '';
                
                    $input['tag_name'] = $this->input->post('tag_name', true);
                    $input['morgens_anzahl_durchgange'] = $this->input->post('morgens_anzahl_durchgange');
                    $input['morgens_anzahl_sps'] = $this->input->post('morgens_anzahl_sps', true);
                    $input['mittags_anzahl_durchgange'] = $this->input->post('mittags_anzahl_durchgange', true);
                    $input['mittags_anzahl_sps'] = $this->input->post('mittags_anzahl_sps', true);
                    $input['abends_anzahl_durchgange'] = $this->input->post('abends_anzahl_durchgange', true);
                    $input['abends_anzahl_sps'] = ucfirst($this->input->post('abends_anzahl_sps', true));
                    $input['nachts_anzahl_durchgange'] = ucfirst($this->input->post('nachts_anzahl_durchgange', true));
                    $input['nachts_anzahl_sps'] = $this->input->post('nachts_anzahl_sps', true);
                    $input['thc_morgens_mg'] = $this->input->post('thc_morgens_mg', true);
                    $input['thc_abends_mg'] = $this->input->post('thc_abends_mg', true);
                    $input['thc_nachts_mg'] = ucfirst($this->input->post('thc_nachts_mg', true));
                    $input['summe_thc_mg'] = $this->input->post('summe_thc_mg', true);
                    $input['thc_mittags_mg'] = $this->input->post('thc_mittags_mg', true);
                    $input['display_order'] = $this->input->post('display_order', true);
                    $input['status'] = $this->input->post('status', true);
                    $input['update_date'] = getDefaultToGMTDate(time());
                    //pr($input); die;
                    $response = $this->dosistyp->setdosistypById($id, $input);

                    if ($response) {

                        $success = true;
                        $message = 'Details of dosistyp updated successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = site_url('admin/dosistyp');

                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
            
            } else {
                $success = false;
                $message = validation_errors();        
            }    
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/dosistyp/form');
        $this->load->view('admin/includes/footer');
    }

      public function view($id) {
        $output['page_title'] = 'View details';
        $output['left_menu'] = 'dosistyp_management';
        $output['left_submenu'] = 'dosistyp_view';
        /* get record by id */
        $record = $this->dosistyp->getdosistypById($id);
        //pr($record); die;
        checkRequestedDataExists($record);
        $output['record'] = $record; 

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/dosistyp/view');
        $this->load->view('admin/includes/footer');
    }    

    public function changeStatus()    {
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
        $this->dosistyp->changeStatusById($id,$status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    /*public function delete() {
        $id = $this->input->post('record_id',true);
        $this->deals->getUsersById($id);
        $data['success'] = true;
        $data['message'] = 'Users has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }*/

    public function multiTaskOperation() {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        
        foreach ($dataIds as $key => $value) {
            
            if ($task == 'Delete') {
                $this->deals->deleteUsersById($value);
                $message = 'Users has been deleted successfully.';
            
            } else  if ($task=='Active' || $task=='Inactive') {
                $this->dosistyp->changeStatusById($value,$task);            
                $message = 'Status of selected users has been changed successfully.';
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data); 
    }

    
}