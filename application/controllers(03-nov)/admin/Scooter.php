<?php
if (!defined('BASEPATH')) exit ('No direct script access allowed');

/**
 * 
 */
class Scooter extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/scooter_model', 'scooter');
	}

	public function index()
	{
		$output['page_title'] = 'E-scooters';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'scooter_list';


		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/scooter_list');
		$this->load->view('admin/includes/footer');
	}

	public function get_ajax_list()
	{
		$this->load->library('pagination');

        $success = true;
        $html = false;
        $load_prev_link = false;
        $load_next_link = false;
        $keyword = $this->input->get('keyword', true);
        $page_limit = $this->input->get('page_limit', true);
        $column_name = $this->input->get('column_name', true);
        $sorting_order = $this->input->get('sorting_order', true);
        $status = $this->input->get('status',true);         
        $page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
        $page_no_index = ($page_no - 1) * $page_limit;
        $sQuery = '';
        if($keyword)
        {
            $sQuery = $sQuery.'&keyword='.$keyword;
        }
        if($page_limit)
        {
            $sQuery = $sQuery.'&page_limit='.$page_limit;       
        }
        if ($column_name) 
        {
            $sQuery = $sQuery.'&column_name='. $column_name;
        }
        if ($sorting_order) 
        {
            $sQuery = $sQuery.'&sorting_order='. $sorting_order;
        }
        $per_page = $this->input->get('page_limit', true);
        $searchdata['search_index'] = $page_no_index;
        $searchdata['limit'] = $per_page;      
        $searchdata['keyword'] = $keyword;
        $searchdata['column_name'] = $column_name;
        $searchdata['sorting_order'] = $sorting_order;
        $searchdata['status'] = $status; 
        $config['base_url'] = site_url('admin/scooter/get_ajax_list?'.$sQuery);
        $config['total_rows'] = $this->scooter->count_scooter($searchdata);
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);

        $data['paging'] = $this->pagination->create_links();

        $records = $this->scooter->get_scooters($searchdata);
        $output['all_scooters'] = $records; 
        //pr($records); die;
        $html = $this->load->view('admin/scooter/ajax_list',$output, true);
        $data['html'] = $html;
        $data['success']= true;

        json_output($data);
	}

	public function add()
	{
		$output['page_title'] = 'Add E-scooter';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'scooter_list';
		$output['task'] = 'Add';
		$output['id'] = '';
		$output['name'] = '';
		$output['maker_name'] = '';
		$output['year_of_made'] = '';
		$output['scooter_image'] = '';
		$output['status'] = 'Active';

		if ($this->input->post()) 
		{
			$success = true;
			$message = '';

			$this->form_validation->set_rules('name', 'Name of Scooter', 'trim|required');
			$this->form_validation->set_rules('maker_name', 'Maker Name', 'trim|required');
			$this->form_validation->set_rules('year_of_made', 'Year Made', 'trim|required');

			if ($this->form_validation->run()) 
			{
				$input = array();
				$input_image = array();

				if (isset($_FILES['image_name']['tmp_name']) && $_FILES['image_name']['tmp_name']) 
				{
					$count = count($_FILES['image_name']['tmp_name']);

					for ($i=0; $i < $count ; $i++) 
					{ 
						$_FILES['file']['name'] = $_FILES['image_name']['name'][$i];
						$_FILES['file']['type'] = $_FILES['image_name']['type'][$i];
						$_FILES['file']['tmp_name'] = $_FILES['image_name']['tmp_name'][$i];
						$_FILES['file']['size'] = $_FILES['image_name']['size'][$i];
						$_FILES['file']['error'] = $_FILES['image_name']['error'][$i];
						$directory = './assets/uploads/scooter_images/';
						@mkdir($directory, 0777);
						@chmod($directory, 0777);
						$config['upload_path'] = $directory;
						$config['allowed_types'] = 'jpg|jpeg|png|gif';
						$config['encrypt_name'] = TRUE;
						$this->load->library('upload');
						$this->upload->initialize($config);

						if ($this->upload->do_upload('file')) 
						{
							$image_data = $this->upload->data();
							$file_name[$i] = $image_data['file_name'];
							$images = implode(',', $file_name);
							$input_image['image_name'] = $images;
						}
						else 
						{
							$message = $this->upload->display_errors();
							$success = false;
						}
					}
				}

				if ($success) 
				{
					$name = ucfirst($this->input->post('name'));
					$maker_name = ucfirst($this->input->post('maker_name'));
					$year_of_made = $this->input->post('year_of_made');
					$status = $this->input->post('status');
					$input['name'] = $name;
					$input['maker_name'] = $maker_name;
					$input['year_of_made'] = $year_of_made;
					$input['status'] = $status;
					//pr($input_image); die;
					$insert_id = $this->scooter->add_new_scooter($input);

					if ($insert_id) 
					{
						if(!empty($images)) 
						{
							$input_image['scooter_id'] = $insert_id;
							$input_image['image_name'] = $images;
							//pr($input_image); die;
							$this->scooter->add_scooter_image($input_image);							
						}						
						$success = true;
						$data['redirectURL'] = site_url('admin/scooter');
						$this->session->set_flashdata('message', $message);
						$message = 'New Scooter added Successfully';
					}
					else
					{
						$message = 'Technical Error!, Please Try Again';
						$success = false;
					}
				}
			}
			else
			{
				$message = validation_errors();
				$success = false;
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);
		}
		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/form');
		$this->load->view('admin/includes/footer');
	}

	public function update($id)
	{
		$output['page_title'] = 'Update E-scooter';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'scooter_list';
		$output['task'] = 'edit';

		$records = $this->scooter->get_scooter_by_id($id);
		$output['records'] = $records;
		$image_data = $this->scooter->get_image_by_id($id);
		$output['image_data'] = $image_data;
		//pr($output); die;
		$output['id'] = $records->id;
		$output['name'] = $records->name;
		$output['maker_name'] = $records->maker_name;
		$output['year_of_made'] = $records->year_of_made;
		//$output['scooter_image'] = '';
		$output['status'] = $records->status;

		if ($this->input->post()) 
		{
			$success = true;
			$message = '';

			$this->form_validation->set_rules('name', 'Name of Scooter', 'trim|required');
			$this->form_validation->set_rules('maker_name', 'Maker Name', 'trim|required');
			$this->form_validation->set_rules('year_of_made', 'Year Made', 'trim|required');

			if ($this->form_validation->run()) 
			{
				$input = array();
				$input_image = array();

				if (isset($_FILES['image_name']['tmp_name']) && $_FILES['image_name']['tmp_name']) 
				{
					$count = count($_FILES['image_name']['tmp_name']);

					for ($i=0; $i < $count ; $i++) 
					{ 
						$_FILES['file']['name'] = $_FILES['image_name']['name'][$i];
						$_FILES['file']['type'] = $_FILES['image_name']['type'][$i];
						$_FILES['file']['tmp_name'] = $_FILES['image_name']['tmp_name'][$i];
						$_FILES['file']['size'] = $_FILES['image_name']['size'][$i];
						$_FILES['file']['error'] = $_FILES['image_name']['error'][$i];
						$directory = './assets/uploads/scooter_images/';
						@mkdir($directory, 0777);
						@chmod($directory, 0777);
						$config['upload_path'] = $directory;
						$config['allowed_types'] = 'jpg|jpeg|png|gif';
						$config['encrypt_name'] = TRUE;
						$this->load->library('upload');
						$this->upload->initialize($config);

						if ($this->upload->do_upload('file')) 
						{
							$image_data = $this->upload->data();
							$file_name[$i] = $image_data['file_name'];
							$images = implode(',', $file_name);
							$input_image['image_name'] = $images;
						}
						else 
						{
							$message = $this->upload->display_errors();
							$success = false;
						}
					}
				}

				if ($success) 
				{
					$name = ucfirst($this->input->post('name'));
					$maker_name = ucfirst($this->input->post('maker_name'));
					$year_of_made = $this->input->post('year_of_made');
					$status = $this->input->post('status');
					$input['name'] = $name;
					$input['maker_name'] = $maker_name;
					$input['year_of_made'] = $year_of_made;
					$input['status'] = $status;
					//pr($input); die;
					$response = $this->scooter->update_scooter_details($id, $input);
					
					if ($response) 
					{
						//die('here');
						if(!empty($images))
						{
							//die('here2');
							$input_image['scooter_id'] = $id;
							$input_image['image_name'] = $images;
							//pr($input_image); die;
							$this->scooter->update_scooter_image($id, $input_image);							
						}						
						$success = true;
						$data['redirectURL'] = site_url('admin/scooter');
						$this->session->set_flashdata('message', $message);
						$message = 'Scooter Updated Successfully';
					}
					else
					{
						$message = 'Technical Error!, Please Try Again';
						$success = false;
					}
				}
			}
			else
			{
				$message = validation_errors();
				$success = false;
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);
		}
		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/form');
		$this->load->view('admin/includes/footer');
	}

	public function view($id)
	{
		$output['page_title'] = 'View E-scooter Details';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'scooter_list';

		$scooter_details = $this->scooter->view_scooter_details($id);
		$output['scooter_details'] = $scooter_details;
		//pr($scooter_details); die;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/view_scooter');
		$this->load->view('admin/includes/footer');
	}

	public function delete()
	{
		$id = $this->input->post('record_id', true);
		$this->scooter->delete_scooter($id);

		$data['success'] = true;
		$data['message'] = 'Scooter has been deleted Successfully';
		$data['callBackFunction'] = 'callBackCommonDelete';
		json_output($data);
	}

	public function change_status()
	{
		$id = $this->input->get('id', true);
		$status = $this->input->get('status', true);
		//print_r($id);
		$this->scooter->change_status_by_id($id, $status);

		$data['success'] = true;
		$data['message'] = 'Status of Scooter updated Successfully';
		json_output($data);
	}

	public function multi_task_operation()
	{
		$task = $this->input->post('task', true);
		$ids = $this->input->post('ids', true);
		$scooterids = explode(',', $ids);

		foreach ($scooterids as $value) 
		{
			if ($task == 'Delete') 
			{
				$this->scooter->delete_scooter($value);
				$message = 'Selected Scooters deleted Successfully';
			} 
			else if ($task == 'Active' || $task == 'Inactive') 
			{
				$this->scooter->change_status_by_id($value, $task);
				$message = 'Status of selected Scooters has been Changed Successfully';
			}
		}
		$data['ids'] = $ids;
		$data['success'] = true;
		$data['message'] = $message;
		$data['callBackFunction'] = 'callBackCommonDelete';
		json_output($data);
	}

	public function not_charged()
	{
		$output['page_title'] = 'E-scooters for Charging';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'for_charging';

		$records = $this->scooter->list_scooters_for_charging();
		$output['records'] = $records;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/scooters_for_charging');
		$this->load->view('admin/includes/footer');
	}

	public function view_not_charged_scooter($id)
	{
		$output['page_title'] = 'View Charging Details';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'for_charging';

		$scooter_records = $this->scooter->view_scooters_for_charging($id);
		//pr($scooter_records); die;
		$output['records'] = $scooter_records;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/view_charging_details');
		$this->load->view('admin/includes/footer');	
	}

	public function list_of_fully_charged_scooters()
	{
		$output['page_title'] = 'Fully Charged E-scooters';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'fully_charged';

		$charged_scooters = $this->scooter->list_scooters_full_charged();
		$output['charged_scooters'] = $charged_scooters;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/scooters_fully_charged');
		$this->load->view('admin/includes/footer');	
	}

	public function view_charged_scooter($id)
	{
		$output['page_title'] = 'View Charged E-scooter Details';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'fully_charged';

		$scooter_records = $this->scooter->view_scooters_charged($id);
		//pr($scooter_records); die;
		$output['records'] = $scooter_records;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/view_scooters_full_charged');
		$this->load->view('admin/includes/footer');	
	}

	public function reserved_scooter()
	{
		$output['page_title'] = 'Reserved E-scooters';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'reserved_scooters';

		$reserved = $this->scooter->list_reserved_scooters();
		//pr($reserved); die;
		$output['reserved_scooters'] = $reserved;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/reserved_scooters');
		$this->load->view('admin/includes/footer');	
	}

	public function view_reserved_scooter_details($id)
	{
		$output['page_title'] = 'View Reserved E-scooter Details';
		$output['left_menu'] = 'scooter_management';
		$output['left_submenu'] = 'reserved_scooters';

		$scooter_records = $this->scooter->view_scooters_reserved($id);
		//pr($scooter_records); die;
		$output['records'] = $scooter_records;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/scooter/view_reserved_scooter');
		$this->load->view('admin/includes/footer');	
	}

	public function delete_charging()
	{
		$id = $this->input->post('record_id', true);
		$this->scooter->delete_scooter_from_charging($id);
		//$this->scooter->delete_scooter($id);

		$data['success'] = true;
		$data['message'] = 'Scooter Removed from Charging';
		$data['callBackFunction'] = 'callBackCommonDelete';
		json_output($data);
	}
}