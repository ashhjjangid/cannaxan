<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Faq class
 */
class Faq extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		$this->load->model('admin/Faq_model', 'faq');
	}

	public function index()
	{
		$output['page_title'] = "List of FAQ";
		$output['left_menu'] = "FAQ_management";
		$output['left_submenu'] = "FAQ_list";
		$list = $this->faq->get_data();
        $output['records'] = $list;
		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/faq/list');
		$this->load->view('admin/includes/footer');
	}  
 
	public function add()
	{

		$list = $this->faq->get_category();
		$output['get_data'] = $list;	
		$output['page_title'] = 'Add FAQ';
		$output['left_menu'] = 'FAQ_management';
		$output['left_submenu'] = 'FAQ_list';
		$output['task'] = 'add';
		$output['id'] = '';
		$output['question'] = '';
		$output['answer'] = '';
		$output['status'] = 'Active';
		$output['category_id'] = ''; 

        if ($this->input->post()) {
			$success = true;
            $message = '';
			/*set validation rules*/
			$this->form_validation->set_rules('category_id', 'Category', 'trim|required');
			$this->form_validation->set_rules('question', 'Question', 'trim|required');
			$this->form_validation->set_rules('answer', 'Answer', 'trim|required');
			if ($this->form_validation->run()) 
			{
				$input = array();
				$input['category_id'] = $this->input->post('category_id', true);
				$input['question'] = $this->input->post('question', true);
				$input['answer'] = $this->input->post('answer', true);
				$input['status'] = $this->input->post('status', true);
				$insert_id = $this->faq->add_new_faq($input);

				if ($insert_id) {
					$success = true;
					$message = 'faq added Succesfully';
					$data['redirectURL'] = base_url('admin/faq');
				} else {
					$success = false;
					$message = "Technical Error! Please Try Again later.";
				}

			}
			else
			{
				$message = validation_errors();
				$success = false;
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);
		}

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/faq/form');
		$this->load->view('admin/includes/footer');

	}

	public function update($id)
	{
		$records = $this->faq->get_faq_for_update($id);
		//pr($records); die;
		$output['records'] = $records;
		$output['page_title'] = "Edit FAQ";
		$output['left_menu'] = 'FAQ_management';
		$output['left_submenu'] = 'FAQ_list';
		$output['task'] = "edit";
		$output['id'] = $records->id; 
		$output['category_id'] = $records->category_id; 
		$output['question'] = $records->question;
		$output['answer'] = $records->answer;
		$output['status'] = $records->status;
		$list = $this->faq->get_category();
		$output['get_data'] = $list;	
		if ($this->input->post())
		{
			$success = true;
            $message = '';
			/*set validation rules*/
			$this->form_validation->set_rules('category_id', 'Category', 'trim|required');
			$this->form_validation->set_rules('question', 'Question', 'trim|required');
			$this->form_validation->set_rules('answer', 'Answer', 'trim|required');
			if ($this->form_validation->run()) 
			{
				$input = array();
				$input['category_id'] = $this->input->post('category_id', true);
				$input['question'] = $this->input->post('question', true);
				$input['answer'] = $this->input->post('answer', true);
				$input['status'] = $this->input->post('status', true);

				$update_record = $this->faq->set_updated_faq($id, $input);

				if ($update_record) {
					$success = true;
					$message = 'FAQ Updated Succesfully';
					$data['redirectURL'] = base_url('admin/faq');
				} else {
					$success = false;
					$message = 'Technical error. Please try again.';
				}

			}
			else
			{
				$message = validation_errors();
				$success = false;
			}
			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);

		}

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/faq/form');
		$this->load->view('admin/includes/footer');
	}

	public function delete()
	{
		$id = $this->input->post('record_id', true);
		$this->faq->delete_faq($id);
		$data['success'] = true;
		$data['message'] = "FAQ Deleted Successfully";
		$data['callBackFunction'] = 'callBackCommonDelete';
		$data['redirectURL'] = base_url('admin/faq');
		json_output($data);
	}

	public function view($id)
	{
		$output['page_title'] = "FAQ Details";
		$output['left_menu'] = 'FAQ_management';
		$output['left_submenu'] = 'FAQ_list';
		$view_record = $this->faq->view_faq($id);
		//print_r($view_record); die;

		$output['usersdata'] = $view_record;
		

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/faq/view');
		$this->load->view('admin/includes/footer');
	}

	public function change_status()
	{
		$id = $this->input->get('id', true);
		$status = $this->input->get('status', true);
		$this->faq->change_status_by_id($id, $status);

		$data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
	}

	public function multi_task_operation()
    {
    	$task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $data_ids = explode(',',$ids);

        foreach ($data_ids as $key => $value) {
        	
        	if ($task == 'Delete') {        	
        	
        		$this->faq->delete_faq($value);
        		$message = "Selected rows deleted";
        	}
        	else if ($task == 'Active' || $task == 'Inactive') {
        		$this->faq->change_status_by_id($value, $task);
        		$message = "Status changed of the selected Rows";
        	}

        }
    	$data['ids'] = $ids;
    	$data['success'] = true;
    	$data['message'] = $message;
    	$data['callBackFunction'] = 'callBackCommonDelete';
    	json_output($data);

    }
}

?>
