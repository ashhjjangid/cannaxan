<?php
if(!defined('BASEPATH')) {
  exit('No direct script access allowed');
}

/**
 * Website class.
 * 
 * @extends CI_Controller
 */

class Website extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    public function __construct() {
        Parent::__construct();
        checkAdminLogin();
        $this->load->model('admin/website_model', 'website');
    }

    /**
     * index function.
     * 
     * @access public
     * @return website setting record
     */

    public function index() {
        $output['page_title'] = "Website Setting";
        $output['left_menu'] = 'Site_options';
        $output['left_submenu'] = 'Website_setting';
        $output['message'] = '';

        if ($_POST) {
            /* set validation rule */
            $this->form_validation->set_rules('smtp_host', 'SMTP Host', 'trim|required');
            $this->form_validation->set_rules('smtp_port', 'SMTP Post Number', 'trim|required');
            $this->form_validation->set_rules('smtp_user', 'SMTP Username', 'trim|required');
            $this->form_validation->set_rules('smtp_pass', 'SMTP Password', 'trim|required');            

            if ($this->form_validation->run()) 
            {
                $options = $_POST;

                $this->website->updateOptionsSetting($options);            
                $message = 'Record Updated Successfully';
                $success = true;            
            }
            else
            {                
                $success = false;
                $message = validation_errors();
            }
            $output['message'] = $message;
            $output['success'] = $success;
            echo json_encode($output); die;
        }    

        $fields = array(                   
                'smtp_host',
                'smtp_port',
                'smtp_user',
                'smtp_pass',
                'commission',
                'google_analytics',
                'copyright_text',
                'mailchimp_api_key',
                'mailchimp_list_id',
                'mailchimp_datacenter',
                'contact_phone',
                'contact_address',
                'google_site_key',
                'google_secret_key',
                'facebook_link',
                'twitter_link',
                'linkedin_link',
                'google_plus_link',
                'single_job_price',
                'you_tube_link',
                'rss_feed_link',
                'contact_email',
                'paypal_business_email',
                'latitude',
                'longitude',
                'suggestion_mail_to_users',
                'kvk_number',
                'mission_statement_string',
                'site_logo_name_front',
                'site_logo_name_admin',
                'allow_free_job_post',
                'expire_job_in_days',
                'allow_paid_job_post',
                'achievement_success',
                'contact_us_address',
                'contact_us_tel_no',
                'contact_us_fax_no',
                'lock_unlock_price',
                'rate_per_min'
            );

        $options_data = array();
        
        if(!empty($fields))
        {
            foreach ($fields as $value) 
            {            
                $options_data[$value] = $this->website->getValueBySlug($value, true);
            }
        }

        $output['copyright_text'] = getOptionValue('copyright_text');
        $output['options_content'] = (object) $options_data;

        if ($options_data['site_logo_name_front']) {
            $output['front_logo_image_url'] = site_url('/assets/uploads/logo/'.$options_data['site_logo_name_front']);
        }

        if ($options_data['site_logo_name_admin']) {
            $output['admin_logo_image_url'] = site_url('/assets/uploads/logo/'.$options_data['site_logo_name_admin']);
        }
        
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/setting/index');
        $this->load->view('admin/includes/footer');
    }   
}
