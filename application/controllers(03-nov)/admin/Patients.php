<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Hunters class.
 * 
 * @extends CI_Controller
 */

class Patients extends CI_Controller {

   /**
    * __construct function.
    * @access public 
    */
       
    function __construct() {
        Parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/patients_model', 'patients');
        $this->load->model('admin/users_model', 'doctors');
        $this->load->model('admin/Nebenwirkung_model','nebenwirkung');
    }

    /**
     * index function.
     * 
     * @access public
     * @return null
     */

    function index() {
        $output['page_title'] = 'Patients';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'patient_list';

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/users_list');
        $this->load->view('admin/includes/footer');
    }

    /**
     * get_ajax_list function.
     * 
     * @access public
     * @return list of all hunters
     */

    public function get_ajax_list()
    {
        $this->load->library('pagination');
        $success = true;
        $html = false;
        $load_prev_link = false;
        $load_next_link = false;
        $keyword = $this->input->get('keyword');
        $page_limit = $this->input->get('page_limit', true);
        $column_name = $this->input->get('column_name',true);
        $sorting_order = $this->input->get('sorting_order',true);
        $page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
        $page_no_index = ($page_no - 1) * $page_limit;
        $status = $this->input->get('status',true);
        $sQuery = '';

       if($keyword)
        {
            $sQuery = $sQuery.'&keyword='.$keyword;
        }
        if($page_limit)
        {
            $sQuery = $sQuery.'&page_limit='.$page_limit;       
        }
        if ($column_name) 
        {
            $sQuery = $sQuery. '&column_name=' .$column_name;
        }
        $per_page = $this->input->get('page_limit', true);
        $searchData['search_index'] = $page_no_index;
        $searchData['limit']  = $page_limit;
        $searchData['keyword'] = $keyword;
        $searchData['column_name'] = $column_name;
        $searchData['sorting_order'] = $sorting_order;
        $searchData['status'] = $status;
        $config['base_url'] = base_url('admin/patients/get_ajax_list?' . $sQuery);
        $total_rows = $this->patients->count_patients($searchData);
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);

        $paging = $this->pagination->create_links();

        $records = $this->patients->getAllPatients($searchData);
        //pr($records); die;
        $output['records'] = $records;
        $html = $this->load->view('admin/patients/ajax_list', $output, true);
        $data['success'] = true;
        $data['html'] = $html;
        $data['paging'] = $paging;

        json_output($data);
    }

    /**
     * add function.
     * 
     * @access public
     * @return null
     */

    function add() {

        $doctor_id = $this->input->get('doctor');
        $doctors = $this->doctors->get_doctors();
        //pr($doctors); die;

        $icd_code_zuordnung = $this->patients->icd_code_zuordnung();
        //pr($icd_code_zuordnung); die;
        $output['all_indications'] = $icd_code_zuordnung;
        $output['doctors'] = $doctors;
        $output['selected_doctor'] = '';
        $output['selected_indication'] = '';
        $output['page_title'] = 'Add New Patient';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'add_patient_list';
        $output['message'] = '';
        $output['task'] = 'add';
        $output['status'] = 'Active';
        $output['gender'] = 'Male';
        $output['pain_scale'] = 'Not Applicable';
        $output['id'] = '';
        $output['first_name'] = '';
        $output['last_name'] = '';       
        $output['date_of_birth'] = '';
        $output['selected_doctors'] = '';


        $output['insured_number'] = '';
        $output['selected_cannabis_medikament'] = '';
        $output['case_number'] = '';
        $output['telephone_number'] = '';
        $output['diagnostic_code'] = '';
        $output['weight'] = ''; /* in KG */
        $output['size'] = ''; /* in CM */
        //$output['main_indication'] = '';
        //$output['main_indication_not_assigned'] = '';
        $output['consent_form'] = 'No';
        $output['dose_unit'] = 'Capsules';
        $output['daily_dose'] = '';
        $output['daily_dose_mg'] = '';
        //$output['unassigned_dose_unit'] = '';
        //$output['unassigned_daily_dose'] = '';
        //$output['unassigned_daily_dose_mg'] = '';
        //$output['unassigned_drug'] = '';
        $output['other_reason'] = '';
        $output['cannabis_drug'] = '';
        $output['existing_cannabis_med'] = 'ja';
        $output['selected_grund_umstellung_reason'] = '';

        if ($this->input->post()) {
            //pr($_POST); die;
            $success = true;
            $message = '';
            /* set validation rules */
            $this->form_validation->set_rules('first_name', 'first name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('last_name', 'last name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('date_of_birth', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('telephone_number', 'Telephone Number', 'trim|min_length[6]|max_length[20]|regex_match[/^[+0-9]+$/]');
            $this->form_validation->set_rules('insured_number', 'Insured Number', 'alpha_numeric_spaces|trim|required|is_unique[tbl_patients.insured_number]|regex_match[/^[a-zA-Z][0-9]{9}$/]', array('regex_match' => 'versichertennummer first character is always a letter and then following nine numbers'));
            $this->form_validation->set_rules('case_number', 'Case Number', 'trim|required');
            $this->form_validation->set_rules('diagnostic_code', 'Diagnostice Code', 'trim|required');
            $this->form_validation->set_rules('weight', 'Weight(In KG)', 'numeric|trim|required');
            $this->form_validation->set_rules('size', 'Size', 'numeric|trim|required');
            $this->form_validation->set_rules('main_indication', 'Main Indication', 'alpha_numeric_spaces|trim|required');
            //$this->form_validation->set_rules('main_indication_not_assigned', 'Main Indication Not Assigned', 'trim|required');
            if ($this->input->post('existing_cannabis_med') == 'ja') {
                $this->form_validation->set_rules('cannabis_drug', 'Cannabis Medikament', 'trim|required');
                $this->form_validation->set_rules('dose_unit', 'Dose Unit', 'trim|required');
                $this->form_validation->set_rules('daily_dose', 'Daily Dose', 'numeric|trim|required');
                $this->form_validation->set_rules('daily_dose_mg', 'Daily Dose(MG THC)', 'numeric|trim|required');
                /*if ($this->input->post('unassigned_drug')) {
                    
                    //$this->form_validation->set_rules('unassigned_drug', 'Unassigned Drug', 'alpha_numeric_spaces|trim|required');
                    $this->form_validation->set_rules('unassigned_daily_dose', 'Unassigned Daily Dose', 'numeric|trim|required');
                    $this->form_validation->set_rules('unassigned_daily_dose_mg', 'Unassigned Daily Dose(MG THC)', 'numeric|trim|required');
                } else if ($this->input->post('unassigned_daily_dose')) {
                    $this->form_validation->set_rules('unassigned_drug', 'Unassigned Drug', 'alpha_numeric_spaces|trim|required');
                    //$this->form_validation->set_rules('unassigned_daily_dose', 'Unassigned Daily Dose', 'numeric|trim|required');
                    $this->form_validation->set_rules('unassigned_daily_dose_mg', 'Unassigned Daily Dose(MG THC)', 'numeric|trim|required');
                } else if ($this->input->post('unassigned_daily_dose_mg')) {
                    $this->form_validation->set_rules('unassigned_drug', 'Unassigned Drug', 'alpha_numeric_spaces|trim|required');
                    $this->form_validation->set_rules('unassigned_daily_dose', 'Unassigned Daily Dose', 'numeric|trim|required');
                    //$this->form_validation->set_rules('unassigned_daily_dose_mg', 'Unassigned Daily Dose(MG THC)', 'numeric|trim|required');
                }*/
            } else {

                $this->form_validation->set_rules('change_reason[]', 'GRUND FÜR UMSTELLUNG', 'trim|required');
                $this->form_validation->set_rules('other_reason', 'Anderer Grund', 'trim|required');
            }
            $this->form_validation->set_rules('doctors_id', 'Doctor', 'trim|required');

            
            if ($this->form_validation->run()) {
                
                $first_name = ucfirst($this->input->post('first_name',true));
                $last_name = ucfirst($this->input->post('last_name',true));
                $username = $this->input->post('username', true);
               
                $doctors_id = $this->input->post('doctors_id', true);
                //pr($input['doctors_id']);
                $input['insured_number'] = $this->input->post('insured_number', true);
                $input['case_number'] = $this->input->post('case_number', true);
                $input['first_name'] = ucfirst($this->input->post('first_name',true));
                $input['last_name'] = ucfirst($this->input->post('last_name',true));
                $input['telephone_number'] = $this->input->post('telephone_number',true);
                $input['date_of_birth'] = date('Y-m-d',strtotime($this->input->post('date_of_birth', true)));
                $input['diagnostic_code'] = $this->input->post('diagnostic_code',true);
                $input['weight'] =$this->input->post('weight',true);
                $input['size'] =$this->input->post('size',true);
                $input['gender'] =$this->input->post('gender',true);
                $input['main_indication'] = $this->input->post('main_indication',true);
                $input['pain_scale'] = $this->input->post('pain_scale',true);
                //$input['consent_form'] = $this->input->post('consent_form',true);
                $input['existing_cannabis_med'] = $this->input->post('existing_cannabis_med',true);
                $input['cannabis_drug'] = $this->input->post('cannabis_drug',true);
                //$input['main_indication_not_assigned'] = $this->input->post('main_indication_not_assigned', true);
                $input['dose_unit'] = $this->input->post('dose_unit',true);
                $input['daily_dose'] = $this->input->post('daily_dose',true);
                $input['daily_dose_mg'] = $this->input->post('dose_unit',true);
                //$input['unassigned_drug'] = $this->input->post('unassigned_drug',true);
                //$input['unassigned_dose_unit'] = $this->input->post('dose_unit',true);
                //$input['unassigned_daily_dose'] = $this->input->post('unassigned_daily_dose',true);
                //$input['unassigned_daily_dose_mg'] = $this->input->post('daily_dose',true);
                $change_reason[] = $this->input->post('change_reason', true);
                foreach ($change_reason as $key => $value) {
                    $selected_umstellung_reasons = implode(',', $value);
                }
                $input['change_reason'] = $selected_umstellung_reasons;
                $input['other_reason'] = $this->input->post('other_reason', true);

                $input['status'] = $this->input->post('status',true);
                //pr($input); die;
                
                /*insert as hunters */
                $insert_id = $this->patients->add_patients($input);
                
                if ($insert_id) {
                    $doctor_patient_data = array();
                    $success = true;
                    $message = 'New Patient added successfully';
                    $doctor_patient_data['patient_id'] = $insert_id;
                    $doctor_patient_data['doctor_id'] = $doctors_id;
                    $doctor_patient_data['add_date'] = getDefaultToGMTDate(time());
                    $this->patients->add_doctorsid($doctor_patient_data);

                    $this->session->set_flashdata('message', $message);
                    $data['redirectURL'] = site_url('admin/patients');
                
                } else {
                    $success = false;
                    $message = 'Technical error. Please try again later.';
                }           
            } else {
                $message.= validation_errors();
                $success = false;
            }

            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        } 

        if ($this->input->get('doctor')) {
            
            //pr($doctor_id);die;
            $patient_count = $this->patients->countPatientsByDoctorId($doctor_id);

            if ($doctor_id >= "10") {
                $next_patient_number = $patient_count + 1;
                $fallnummer = "$doctor_id-$next_patient_number";
                //$fallnummerdata['case_number'] = $fallnummer;

                json_output($fallnummer);
            } else {
                $next_patient_number = $patient_count + 1;
                $fallnummer = "$doctor_id-0$next_patient_number";
                //$fallnummerdata['case_number'] = $fallnummer;

                json_output($fallnummer);
            }
        }
        $cannabis_medikament = $this->patients->getCannabisMedikament();
        $output['cannabis_medikament'] = $cannabis_medikament;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/form');
        $this->load->view('admin/includes/footer');
    }

    /**
     * update function.
     * 
     * @access public
     * @param mixed $id     
     * @return null
     */

    public function update($id) 
    {
        $doctors = $this->doctors->get_doctors();
        $selected_doctor = $this->patients->checkOtherDoctorPatientById($id);
        //pr($selected_doctor); die;
        if ($selected_doctor) {
            
            $output['selected_doctor'] = $selected_doctor->doctor_id;
        } else {
            $output['selected_doctor'] = '';
        }
        $output['doctors'] = $doctors;
        $record = $this->patients->get_patients_by_id($id);
        //pr($record);die;
        $icd_code_zuordnung = $this->patients->icd_code_zuordnung();
        //pr($icd_code_zuordnung); die;
        $cannabis_medikament = $this->patients->getCannabisMedikament();
        $output['cannabis_medikament'] = $cannabis_medikament;
        $output['all_indications'] = $icd_code_zuordnung;
        checkRequestedDataExists($record);
        $output['page_title'] = 'Update Patient';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'update_patient_list';
        $output['task'] = 'edit';
        $output['record'] = $record;
        $output['message'] = '';
        $output['status'] = $record->status;
        $output['gender'] = $record->gender;
        $output['pain_scale'] = $record->pain_scale;
        $output['id'] = $record->id;
        $output['first_name'] = $record->first_name;
        $output['last_name'] = $record->last_name;       
        $output['date_of_birth'] = date('d-m-Y',strtotime($record->date_of_birth));
        $output['selected_grund_umstellung_reason'] = $record->change_reason;
        $output['insured_number'] = $record->insured_number;
        $output['case_number'] = $record->case_number;
        $output['telephone_number'] = $record->telephone_number;
        $output['diagnostic_code'] = $record->diagnostic_code;
        $output['weight'] = $record->weight; /* in KG */
        $output['size'] = $record->size; /* in CM */
        $output['selected_indication'] = $record->main_indication;
        //pr($output['selected_indication']); die;
        //$output['main_indication_not_assigned'] = $record->main_indication_not_assigned;
        //$output['consent_form'] = $record->consent_form;
        $output['dose_unit'] = $record->dose_unit;
        $output['daily_dose'] = $record->daily_dose;
        $output['daily_dose_mg'] = $record->daily_dose_mg;
        //$output['unassigned_dose_unit'] = $record->unassigned_dose_unit;
        //$output['unassigned_daily_dose'] = $record->unassigned_daily_dose;
        //$output['unassigned_daily_dose_mg'] = $record->unassigned_daily_dose_mg;
        //$output['unassigned_drug'] = $record->unassigned_drug;
        $output['other_reason'] = $record->other_reason;
        //$output['cannabis_drug'] = $record->cannabis_drug;
        $output['selected_cannabis_medikament'] = $record->cannabis_drug;
        $output['existing_cannabis_med'] = $record->existing_cannabis_med;
        
        if ($this->input->post()) 
        {
            $message = '';
            $success = true;
            /* set validation rules */
            $this->form_validation->set_rules('first_name', 'first name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('last_name', 'last name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('date_of_birth', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('telephone_number', 'Telephone Number', 'trim|min_length[6]|max_length[20]|regex_match[/^[+0-9]+$/]');
            $this->form_validation->set_rules('insured_number', 'Insured Number', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('case_number', 'Case Number', 'trim|required');
            $this->form_validation->set_rules('diagnostic_code', 'Diagnostice Code', 'trim|required');
            $this->form_validation->set_rules('weight', 'Weight(In KG)', 'numeric|trim|required');
            $this->form_validation->set_rules('size', 'Size', 'numeric|trim|required');
            $this->form_validation->set_rules('main_indication', 'Main Indication', 'alpha_numeric_spaces|trim|required');
            //$this->form_validation->set_rules('main_indication_not_assigned', 'Main Indication Not Assigned', 'trim|required');
            if ($this->input->post('existing_cannabis_med') == 'ja') {
                $this->form_validation->set_rules('cannabis_drug', 'Cannabis Medikament', 'trim|required');
                $this->form_validation->set_rules('dose_unit', 'Dose Unit', 'trim|required');
                $this->form_validation->set_rules('daily_dose', 'Daily Dose', 'numeric|trim|required');
                $this->form_validation->set_rules('daily_dose_mg', 'Daily Dose(MG THC)', 'numeric|trim|required');
               /* if ($this->input->post('unassigned_drug')) {
                    
                    //$this->form_validation->set_rules('unassigned_drug', 'Unassigned Drug', 'alpha_numeric_spaces|trim|required');
                    $this->form_validation->set_rules('unassigned_daily_dose', 'Unassigned Daily Dose', 'numeric|trim|required');
                    $this->form_validation->set_rules('unassigned_daily_dose_mg', 'Unassigned Daily Dose(MG THC)', 'numeric|trim|required');
                } else if ($this->input->post('unassigned_daily_dose')) {
                    $this->form_validation->set_rules('unassigned_drug', 'Unassigned Drug', 'alpha_numeric_spaces|trim|required');
                    //$this->form_validation->set_rules('unassigned_daily_dose', 'Unassigned Daily Dose', 'numeric|trim|required');
                    $this->form_validation->set_rules('unassigned_daily_dose_mg', 'Unassigned Daily Dose(MG THC)', 'numeric|trim|required');
                } else if ($this->input->post('unassigned_daily_dose_mg')) {
                    $this->form_validation->set_rules('unassigned_drug', 'Unassigned Drug', 'alpha_numeric_spaces|trim|required');
                    $this->form_validation->set_rules('unassigned_daily_dose', 'Unassigned Daily Dose', 'numeric|trim|required');
                    //$this->form_validation->set_rules('unassigned_daily_dose_mg', 'Unassigned Daily Dose(MG THC)', 'numeric|trim|required');
                }*/
            } else {

                $this->form_validation->set_rules('change_reason[]', 'GRUND FÜR UMSTELLUNG', 'trim|required');
                $this->form_validation->set_rules('other_reason', 'Anderer Grund', 'trim|required');
            }
            $this->form_validation->set_rules('doctors_id', 'Doctor', 'trim|required');            

            if ($this->form_validation->run()) {

                $first_name = ucfirst($this->input->post('first_name',true));
                $last_name = ucfirst($this->input->post('last_name',true));
                $username = $this->input->post('username', true);
                $doc_ids = $this->input->post('doctors_id', true);



                $input['doctors_id'] = $doc_ids;
                //pr($input['doctors_id']);
                $input['insured_number'] = $this->input->post('insured_number', true);
                $input['case_number'] = $this->input->post('case_number', true);
                $input['first_name'] = ucfirst($this->input->post('first_name',true));
                $input['last_name'] = ucfirst($this->input->post('last_name',true));
                $input['telephone_number'] = $this->input->post('telephone_number',true);
                $input['date_of_birth'] = date('Y-m-d',strtotime($this->input->post('date_of_birth', true)));
                $input['diagnostic_code'] = $this->input->post('diagnostic_code',true);
                $input['weight'] =$this->input->post('weight',true);
                $input['size'] =$this->input->post('size',true);
                $input['gender'] =$this->input->post('gender',true);
                $input['main_indication'] = $this->input->post('main_indication',true);
                $input['pain_scale'] = $this->input->post('pain_scale',true);
                //$input['consent_form'] = $this->input->post('consent_form',true);
                $input['existing_cannabis_med'] = $this->input->post('existing_cannabis_med',true);
                //$input['main_indication_not_assigned'] = $this->input->post('main_indication_not_assigned', true);
                //$input['unassigned_drug'] = $this->input->post('unassigned_drug',true);
                //$input['unassigned_dose_unit'] = $this->input->post('dose_unit',true);
                //$input['unassigned_daily_dose'] = $this->input->post('unassigned_daily_dose',true);
                //$input['unassigned_daily_dose_mg'] = $this->input->post('daily_dose',true);
                $change_reason[] = $this->input->post('change_reason', true);
                //pr($change_reason);
                foreach ($change_reason as $key => $value) {
                    //pr($value);
                    $selected_umstellung_reasons = implode(',', $value);
                }
                //pr($selected_umstellung_reasons);die;
                if ($this->input->post('existing_cannabis_med') == 'ja') {
                    
                    $input['cannabis_drug'] = $this->input->post('cannabis_drug',true);
                    $input['dose_unit'] = $this->input->post('dose_unit',true);
                    $input['daily_dose'] = $this->input->post('daily_dose',true);
                    $input['daily_dose_mg'] = $this->input->post('daily_dose_mg',true);
                    $input['change_reason'] = '';
                    $input['other_reason'] = '';
                } else {
                    $input['cannabis_drug'] = '';
                    $input['dose_unit'] = '';
                    $input['daily_dose'] = '';
                    $input['daily_dose_mg'] = '';
                    $input['change_reason'] = $selected_umstellung_reasons;
                    $input['other_reason'] = $this->input->post('other_reason', true);
                }

                $input['status'] = $this->input->post('status',true);
                    
                $response = $this->patients->update_patients_details($id,$input);

                    if ($response) {
                        $success = true;
                        $message = 'Patients updated successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = site_url('admin/patients');                    
                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
        

            } else {
                $success = false;
                $message = validation_errors();     
            }   
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/form');
        $this->load->view('admin/includes/footer');
    }

    /**
     * view function.
     * 
     * @access public
     * @param mixed $id      
     */
    public function view_laborwerte($id)
    {
        $output['page_title'] = 'View laborwerte';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'view_laborwerte_list';
        
        $laborwerte_data = $this->patients->getPatientlaborwerteData($id);
        checkRequestedDataExists($laborwerte_data);

        if ($laborwerte_data) {
            
            $patient_laborwerte_data = $this->patients->getPaitentlaborwerteDataByPatientId($laborwerte_data->patients_laborwerte_id);
            $output['patient_laborwerte_data'] = $patient_laborwerte_data;
            $output['laborwerte_data'] = $laborwerte_data;
        } else {
            $output['patient_laborwerte_data'] = '';
            $output['laborwerte_data'] = '';
        }
        //pr($output['laborwerte_data']);
        //pr($output['patient_laborwerte_data']);die;
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/laborwerte_view');
        $this->load->view('admin/includes/footer');
    }

    public function view_begleitmedikation($id)
    {
        $output['page_title'] = 'View begleitmedikation';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'view_begleitmedikation_list';
        
        $begleitmedikation_data = $this->nebenwirkung->getPatientbegleitmedikationData($id);
        //pr($begleitmedikation_data);
        //checkRequestedDataExists($begleitmedikation_data);
        $output['begleitmedikation_data'] = $begleitmedikation_data;
        if($begleitmedikation_data)
        {

        $patient_begleitmedikation_data = $this->nebenwirkung->getPaitentbegleitmedikationDataByPatientId($begleitmedikation_data->patient_begleitmedikation_id);
        //pr($patient_begleitmedikation_data);
        $output['patient_begleitmedikation_data'] = $patient_begleitmedikation_data;
    } else {
        $output['patient_begleitmedikation_data'] = '';
    }
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/begleitmedikation_view');
        $this->load->view('admin/includes/footer');
    }


      public function view_arztlichertherapieplan()
    {
        $output['page_title'] = 'View arztlicher therapieplan';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'arztlichertherapieplan_list_data';

        $arztlichertherapieplan = $this->patients->getarztlicherTherapieplandata();
        //pr($arztlichertherapieplan);die;
        $output['arztlichertherapieplan'] = $arztlichertherapieplan;
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/arztlichertherapieplan_list');
        $this->load->view('admin/includes/footer');
    }

     public function view_arztlichertherapieplandata($id)
    {
        $output['page_title'] = 'View arztlicher therapieplan';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'view_arztlichertherapieplan_list';
        $record = $this->patients->getPatientAllInformation($id);
        //pr($record);die;
        $output['record'] = $record;
        $output['patients_dosistyp_data'] = '';
        
        $arztlichertherapieplan_data = $this->nebenwirkung->getPatientarztlichertherapieplanData($id);
        //pr($arztlichertherapieplan_data); die;
        if ($arztlichertherapieplan_data->dosistyp == "dosistyp_1") {
            
            $patient_arztlichertherapieplan_data_dosis_typ_1 = $this->nebenwirkung->getpatient_dosistyp_1($arztlichertherapieplan_data->id);
            //pr($patient_arztlichertherapieplan_data_dosis_typ_1); die;
           $output['patients_dosistyp_data'] = $patient_arztlichertherapieplan_data_dosis_typ_1;
        } else {

            $patient_arztlichertherapieplan_data_dosis_typ_2 = $this->nebenwirkung->getpatient_dosistyp_2($arztlichertherapieplan_data->id);
                
            $output['patients_dosistyp_data'] = $patient_arztlichertherapieplan_data_dosis_typ_2;
            
        }
        $output['arztlicher_therapieplan'] = $arztlichertherapieplan_data;
        
        $icd_code_zuordnung = $this->nebenwirkung->geticd_code_zuordnung();
        //pr($icd_code_zuordnung); die;
        foreach ($icd_code_zuordnung as $key => $value) {
            if ($record) {
                if ($record->hauptindikation == $value->id) {
                    $output['hauptindikation_name'] = $value->indikation;
                } else {
                    $output['hauptindikation_name'] = '';
                }
            }
        }

        $output['icd_code_zuordnung'] = $icd_code_zuordnung;
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/arztlichertherapieplan_view');
        $this->load->view('admin/includes/footer');
    }

     public function view_umstellung($id)
    {
        $output['page_title'] = 'View umstellung';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'umstellung_list';
        
        $umstellung = $this->patients->getumstellungData();
        //pr($umstellung);
        $output['umstellungdata'] = $umstellung;   
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/umstellung_list');
        $this->load->view('admin/includes/footer');
    }

    public function view_umstellungdata($id)
    {
        $output['page_title'] = 'View umstellung';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'umstellung_view';
        
        $umstellung_data = $this->patients->getPatientumstellungData($id);
        //pr($umstellung_data);
        $output['umstellung_data'] = $umstellung_data;
        if($umstellung_data)
        {
        $patient_umstellung_data = $this->patients->getPaitentumstellungDataByPatientId($umstellung_data->therapy_plan_id);
        //pr($patient_umstellung_data);
            $output['patient_umstellung_data'] = $patient_umstellung_data;

        } else
        {

        //pr($patient_umstellung_data);
            $output['patient_umstellung_data'] = '';
        }
        
        $cannabis_medikament = $this->patients->getcannabis_medikament();
        //pr($cannabis_medikament);
        $output['cannabis_medikament'] = $cannabis_medikament;

        
         
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/umstellung_view');
        $this->load->view('admin/includes/footer');
    }

    public function view_folgeverord($id)
    {
        $output['page_title'] = 'View folgeverord';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'view_folgeverord_list';
        
        $folgeverord = $this->patients->getfolgeverordData();
        //pr($folgeverord);
        $output['folgeverorddata'] = $folgeverord;
        
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/folgeverord_list');
        $this->load->view('admin/includes/footer');
    }

    public function view_folgeverorddata($id)
    {
        $output['page_title'] = 'View folgeverord';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'view_folgeverord_view';
        
        $icd_code_zuordnung = $this->nebenwirkung->geticd_code_zuordnung();
        //pr($icd_code_zuordnung);
        $output['icd_code_zuordnung'] = $icd_code_zuordnung;
        
        $folgeverord_data = $this->patients->getPatientfolgeverordData($id);
        //pr($folgeverord_data);
        $output['folgeverord_data'] = $folgeverord_data;
        if($folgeverord_data)
        {
            $patient_folgeverord_data = $this->patients->getPaitentfolgeverordDataByPatientId($folgeverord_data->patient_folgeverord_id);
            $output['patient_folgeverord_data'] = $patient_folgeverord_data;
           //pr($patient_folgeverord_data); 
        } else {
            $output['patient_folgeverord_data'] = '';
        }
        
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/folgeverord_view');
        $this->load->view('admin/includes/footer');
    }

    public function view_nebenwirkung($id)
    {
        $output['page_title'] = 'View nebenwirkung';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'view_nebenwirkung_list';
        $record = $this->patients->getPatientAllInformation($id);
        $output['record'] = $record;
        
        $nebenwirkung_data = $this->patients->getPatientnebenwirkungData($id);
        //pr($nebenwirkung_data); die;
        if ($nebenwirkung_data) {
            $output['nebenwirkung_data'] = $nebenwirkung_data;
            $get_nebenwirkung_data = $this->patients->getnebenwirkung_data($nebenwirkung_data->patient_nebenwirkung_id);
            //pr($get_nebenwirkung_data); 
            $output['get_nebenwirkung_data'] = $get_nebenwirkung_data;
            
        } else {
           $output['nebenwirkung_data'] = ''; 
           $output['get_nebenwirkung_data'] = '';
        }
        $get_nebenwirkung = $this->patients->getnebenwirkung();
        //pr($get_nebenwirkung);
        $output['get_nebenwirkung'] = $get_nebenwirkung; 
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/nebenwirkung_view');
        $this->load->view('admin/includes/footer');
    }

    public function view($id) {
        $output['page_title'] = 'View Patient';
        $output['left_menu'] = 'patient_management';
        $output['left_submenu'] = 'view_patient_list';
        /* get record by id */
        $output['patient_weitere_data'] = '';
        $record = $this->patients->getPatientAllInformation($id);
        //pr($record); die;
        checkRequestedDataExists($record);
        $output['usersdata'] = $record; 
        $question_data_records = $this->patients->getPatientAppQuestionsData($id);
        //pr($question_data_records); die;
        if ($question_data_records) {
            $patient_weitere_data = $this->patients->get_patientweitere_cannaxan_dosis($question_data_records->patient_id);
            if ($patient_weitere_data) {
                $output['patient_weitere_data'] = $patient_weitere_data;
            }
            
            $output['question_data_records'] = $question_data_records;
        } else {
            $output['question_data_records'] = '';
        }
        //pr($question_data_records); die;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/patients/view_user');
        $this->load->view('admin/includes/footer');
    }

    


    /**
     * change_status function.
     * 
     * @access public
     * @return null
     */

    public function change_status()  {
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
        $this->patients->change_status_by_id($id,$status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    
    /**
     * delete function.
     * 
     * @access public
     * @return null
     */

  public function delete() {
        $id = $this->input->post('record_id',true);
        $this->patients->delete_patients($id);
        $data['success'] = true;
        $data['message'] = 'records has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }



    /**
     * multi_task_operation function.
     * 
     * @access public
     * @return null
     */

    public function multi_task_operation() 
    {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        
        foreach ($dataIds as $key => $value) {
            
            if ($task == 'Delete') {
                $this->patients->delete_patients($value);
                $message = 'Selected records has been deleted successfully.';
            
            } else  if ($task=='Active' || $task=='Inactive') {
                $this->patients->change_status_by_id($value,$task);            
                $message = 'Status of selected records has been changed successfully.';
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data); 
    }
}