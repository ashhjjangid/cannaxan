<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Static_page_mobile class.
 * @extends CI_Controller
 */

class Static_page_mobile extends CI_Controller {

   /**
    * __construct function.
    * @access public
    * @return void
   */
       
    function __construct() {
        Parent::__construct();
        checkAdminLogin();
        $this->load->model('admin/static_page_mobile_model', 'static_page');
        $this->userId = $this->session->userdata('admin_id');
    }

    /**
     * index function.
     * @access public
     * @return static page list
     */

    function index() { 
        $output['page_title'] = 'Static page mobile';
        $output['left_menu'] = 'Static_Pages';
        $output['left_submenu'] = 'static_page_mobile_list';
        
        $static_pages = $this->static_page->get_all_static_pages();
        $output['static_page'] = $static_pages;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/static_page_mobile/static_page_list');
        $this->load->view('admin/includes/footer');
    }

    /**
     * update function.
     * @access public
     * @param mixed $id
     * @return null
     */     

    function update($id) {
        $output['page_title'] = 'Update Static Page mobile';
        $output['left_menu'] = 'Static_Pages';
        $output['left_submenu'] = 'static_page_mobile_list';
        $output['message'] = '';

        $staticPage = $this->static_page->get_static_page_by_id($id);
        checkRequestedDataExists($staticPage);
        
        $output['id'] = $staticPage->id;
        $output['record'] = $staticPage;
        $output['title'] = $staticPage->title;
        $output['content'] = $staticPage->content;
        
        if ($this->input->post()) {
            $success = true;
            $this->form_validation->set_rules('title', 'title', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('content', 'content', 'trim|required');
            
            if ($this->form_validation->run()) {
                $input = array();
                $input['edited_by'] = $this->userId;
                $input['status'] = $this->input->post('status',true);
                $input['title'] =  $this->input->post('title',true);
                $input['content'] = $this->input->post('content',true);
                $result = $this->static_page->set_static_page_by_id($id,$input);

                if ($result) {
                    $this->session->set_flashdata('message', 'Record updated successfully.');
                    $data['redirectURL'] = site_url('admin/static_page_mobile');
                    $message = 'Static page updated successfully.';
                    $success = true;
                
                } else {
                    $message = 'Technical error. Please try again.';
                    $success = false;
                }
            
            } else {
                $success = false;
                $message = validation_errors();
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $output['title'] = $staticPage->title;
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/static_page_mobile/static_page_form');
        $this->load->view('admin/includes/footer');
    }

    /**
     * changeStatus function.
     * @access public
     * @return null
     */

    function changeStatus() {
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
       //print_r($status);die;
        $this->static_page->change_status_by_id($id,$status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    /**
     * multiTaskOperation function.
     * @access public
     * @return null
     */
    
    function multiTaskOperation() {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        $message = '';

        foreach ($dataIds as $key => $value) {

            if ($task == 'Delete') {
                $this->static_page->delete_static_page_by_id($value);
                $message = 'Selected pages has been deleted successfully.';
            
            } else if ($task=='Active' || $task=='Inactive') {
                $this->static_page->change_status_by_id($value,$task);            
                $message = 'Status of Selected records changed successfully.';
            }
        }        
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }

    
    
}