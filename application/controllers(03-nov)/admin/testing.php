<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

   

    function __construct() 
    {
        Parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/users_model');
        $this->user_id = $this->session->userdata('admin_id');
    }

    public function add() 
    {
        $message = " ";
        $success = true;
        $output['page_title'] = 'User management';
        $output['left_menu'] = 'user details';
        $output['left_submenu'] = 'user details';

        //$templates = $this->mail_templates->getAllTemplates();
        //$users = $this->user->getAllUsers();
        //$output['templates'] = $templates;
        //$output['users_list'] = $users;


          

        if($this->input->post())
        {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('firstname','First Name', 'trim|required');
        $this->form_validation->set_rules('lastname','Last Name','trim|required');
        $this->form_validation->set_rules('username','User Name','trim|required|is_unique[tbl_user.username]');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email|is_unique[tbl_user.email]');
        $this->form_validation->set_rules('password','Password','trim|required');
        $this->form_validation->set_rules('mobile','mobile','trim|required');

        }
        if($this->form_validation->run())
        {
        
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $mobile = $this->input->post('mobile');
            $gender = $this->input->post('gender');
            $status = $this->input->post('status');

            $input = array();
            $error = 0;

                if (isset($_FILES['image']['name']) && $_FILES['image']['name']) 
                {
                    $directory = './assets/uploads/'; 
                                      
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'gif|jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    print_r($this->upload->do_upload('image'));
                    if ($this->upload->do_upload('image')) 
                    {
                        $image_data = $this->upload->data();
                        $file_name = $image_data['file_name'];
                        $input['image'] = $file_name;
                        //print_r($file_name); die;
                    } else 
                    {
                        $message = $this->upload->display_errors();
                        $success = false;
                    }
                }
                $add_data = $this->users_model->insert($firstname, $lastname, $username, $email, $password, $mobile, $file_name, $gender, $status);     
                redirect('admin/user');
                if($add_data)
                {
                    echo"Your record insert successfully";
                }
                else
                {
                    echo" unsuccessfully";
                }
            }
              
        
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/user/user_form');
        $this->load->view('admin/includes/footer');
    }
    public function index()
    {
        $output['page_title'] = 'User management';
        $output['left_menu'] = 'user details';
        $output['left_submenu'] = 'user details';
        $this->load->view('admin/includes/header',$output);
        $this->load->model('Users_model');
        $data['new']=$this->Users_model->display();
        //print_r($data);die;
        $this->load->view('admin/user/userlist',$data);
        $this->load->view('admin/includes/footer');
    }
    public function display_view()
    {
        $output['page_title'] = 'User management';
        $output['left_menu'] = 'user details';
        $output['left_submenu'] = 'user details';
        $this->load->model('Users_model');
        $id=$this->input->get('id');
        $data['new']=$this->Users_model->get($id);
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/user/view',$data);
        $this->load->view('admin/includes/footer');
    }
    public function cancel()
    {
        $id=$this->input->get('id');
        $this->load->model('users_model');
        $data=$this->users_model->delete($id);
        redirect('admin/User');
    }
    public function edit()
    {   
        $output['page_title'] = 'User management';
        $output['left_menu'] = 'user details';
        $output['left_submenu'] = 'user details';
        $this->load->view('admin/includes/header',$output);
        $id = $this->input->get('id');
        $this->load->model('Users_model');
        $rowdata['row'] = $this->Users_model->get($id);
        $this->load->view('admin/user/update', $rowdata);

        if($this->input->post())
        {
            $this->form_validation->set_rules('firstname','First Name','trim|required');
            $this->form_validation->set_rules('lastname','Last Name','trim|required');
            $this->form_validation->set_rules('username','User Name','trim|required|is_unique[tbl_user.username]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|is_unique[tbl_user.email]');
            $this->form_validation->set_rules('password','Password','trim|requird');
            $this->form_validation->set_rules('mobile','mobile','trim|requird');
            
        }
        if($this->form_validation->run())
        {
            $id = $this->input->post('id');
            $firstname=$this->input->post('firstname');
            $lastname=$this->input->post('lastname');
            $username=$this->input->post('username');
            $email=$this->input->post('email');
            $password=$this->input->post('password');
            $mobile=$this->input->post('mobile');
            $gender=$this->input->post('gender');
            $status=$this->input->post('status');

            $data=$this->users_model->update($id,$firstname,$lastname,$username,$email,$password,$mobile,$gender,$status);
            print_r($data);die;
            redirect('admin/user');
        }

    }
        public function changeStatus()
        {
            $id = $this->input->get('id');
            $status = $this->input->get('status');

            $this->users_model->changeStatusById($id, $status);

            $data['success'] = true;
            $data['message'] = "Record Update successfully";
            json_output($data);
        }
}      
?>
