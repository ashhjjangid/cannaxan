<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Users class.
 * 
 */
require_once APPPATH.'libraries/gemini/autoload.php';
use Samrap\Gemini\Gemini;

class Gemini_api extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		/*$this->key = 'master-67kcc70IiGNP1RotOrzc';
		$this->secret = '3gpWo781wvVwLLKcKnShM3HRFKJH';*/

		$this->key = 'account-ptfODETv56Bf2sGs9Iqh';
		$this->secret = 'YaRfwXHwRHBan6JsHCjud12fdVh';
	}

	public function exchange($symbol = null)
	{
		if (!$symbol) {
			redirect('admin');
		}
		$output['page_title'] = "Exchange Rate (".$symbol.")";
		$output['left_menu'] = "gemini_management";
		$output['left_submenu'] = "gemini_list";
		$exchange_currencies = $this->_getCurrencies(strtoupper($symbol));
		
		if (!$symbol) {
			redirect('admin');
		}
		$currencies = array();
		foreach ($exchange_currencies as $key => $value) {
			$ticker = $this->_getTicker($value);
			
			$currencies[$key]['name'] = $value;
			$currencies[$key]['bid'] = $ticker['bid'];
			$currencies[$key]['ask'] = $ticker['ask'];
			$currencies[$key]['date'] = $ticker['volume']['timestamp'];
		}
		
		$output['currencies'] = $currencies;
		$output['symbol'] = strtolower($symbol);
		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/gemini/exchange_list');
		$this->load->view('admin/includes/footer');
	}

	private function _getCurrencies($symbol){
		$currencies = array();

		if ($symbol == "USD") {
			$currencies[] = 'BTCUSD';
			$currencies[] = 'ETHUSD';
			$currencies[] = 'BCHUSD';
			$currencies[] = 'LTCUSD';
			$currencies[] = 'ZECUSD';
		} else if ($symbol == "BTC") {
			$currencies[] = 'BTCUSD';
			$currencies[] = 'ETHBTC';
			$currencies[] = 'BCHBTC';
			$currencies[] = 'LTCBTC';
			$currencies[] = 'ZECBTC';
		} else if ($symbol == "ETH") {
			$currencies[] = 'ETHBTC';
			$currencies[] = 'ETHUSD';
			$currencies[] = 'BCHETH';
			$currencies[] = 'LTCETH';
			$currencies[] = 'ZECETH';
		} else if ($symbol == "BCH") {
			$currencies[] = 'BCHUSD';
			$currencies[] = 'BCHBTC';
			$currencies[] = 'BCHETH';
			$currencies[] = 'LTCBCH';
			$currencies[] = 'ZECBCH';
		} else if ($symbol == "LTC") {
			$currencies[] = 'LTCUSD';
			$currencies[] = 'LTCBTC';
			$currencies[] = 'LTCETH';
			$currencies[] = 'LTCBCH';
			$currencies[] = 'ZECLTC';
		} else if ($symbol == "ZEC") {
			$currencies[] = 'ZECUSD';
			$currencies[] = 'ZECBTC';
			$currencies[] = 'ZECETH';
			$currencies[] = 'ZECBCH';
			$currencies[] = 'ZECLTC';
		}

		return $currencies;
	}

	private function _getTicker($symbol){
		$gemini = new Gemini($this->key, $this->secret);
		$ticker = $gemini->ticker($symbol);
		return $ticker;
	} 


	function past_orders() {
		$symbol = ($_GET && $_GET['s'])?$_GET['s']:'';
		$exchange_symbol = ($_GET && $_GET['es'])?$_GET['es']:'';

		if ($symbol && $exchange_symbol) {
			$orderBook = $this->_getPastOrders($exchange_symbol);
			$output['page_title'] = "Past Orders (".$symbol.")";
			$output['left_menu'] = "gemini_management";
			$output['left_submenu'] = "gemini_list";
			$output['orders'] = $orderBook;
			$this->load->view('admin/includes/header', $output);
			$this->load->view('admin/gemini/past_orders');
			$this->load->view('admin/includes/footer');
		} else {
			redirect('admin');
		}
	}

	function active_orders() {
		$symbol = ($_GET && $_GET['s'])?$_GET['s']:'';
		$exchange_symbol = ($_GET && $_GET['es'])?$_GET['es']:'';

		if ($symbol && $exchange_symbol) {
			$orderBook = $this->_getActiveOrders($exchange_symbol);
			$output['page_title'] = "Active Orders (".$symbol.")";
			$output['left_menu'] = "gemini_management";
			$output['left_submenu'] = "gemini_list";
			$output['orders'] = $orderBook;
			$this->load->view('admin/includes/header', $output);
			$this->load->view('admin/gemini/past_orders');
			$this->load->view('admin/includes/footer');
		} else {
			redirect('admin');
		}
	}

	private function _getPastOrders($symbol){
		$gemini = new Gemini($this->key, $this->secret);
		$payload = array();
		$payload['symbol'] = $symbol;
		$orders = $gemini->getPastTrades($payload);
		return $orders;
	}

	private function _getActiveOrders($symbol){
		$gemini = new Gemini($this->key, $this->secret);
		$payload = array();
		$payload['symbol'] = $symbol;
		$orders = $gemini->getPastTrades($payload);
		return $orders;
	} 
}