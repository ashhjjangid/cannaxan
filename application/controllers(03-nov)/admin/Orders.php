<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Users class.
 * 
 */
require_once APPPATH.'libraries/gemini/autoload.php';
use Samrap\Gemini\Gemini;

class Orders extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		$this->master_key = 'master-7WY7n3T4ObG5mXMhn4v9';
		$this->master_secret = '2n64u4PWtaAkM5xzVKeCaKi2KNF6';

		$this->key = 'account-ptfODETv56Bf2sGs9Iqh';
		$this->secret = 'YaRfwXHwRHBan6JsHCjud12fdVh';
	}


	function completed() {	
		$symbols = $this->_getSymbols();
		$orders = array();
		$i = 0; 

		foreach ($symbols as $key => $value) {

			$orderBook = $this->_getPastOrders($value);
			
			if ($orderBook) {

				foreach ($orderBook as $k => $v) {
					$orders[$i]['order_id'] = $v['order_id']; 
					$orders[$i]['price'] = $v['price']; 
					$orders[$i]['amount'] = $v['amount']; 
					$orders[$i]['timestamp'] = $v['timestamp']; 
					$orders[$i]['type'] = $v['type']; 
					$orders[$i]['fee_currency'] = $v['fee_currency']; 
					$orders[$i]['fee_amount'] = $v['fee_amount']; 	
					$orders[$i]['orderbook'] = strtoupper($value); 	
					$i++;
				}
			}
			sleep(1);
		}
		$output['page_title'] = "Completed Orders";
		$output['left_menu'] = "order_management";
		$output['left_submenu'] = "completed_order_list";
		$output['orders'] = $orders;
		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/orders/completed');
		$this->load->view('admin/includes/footer');
	}

	function active() {
		$output['page_title'] = "Active Orders";
		$output['left_menu'] = "order_management";
		$output['left_submenu'] = "active_order_list";
		$orderBook = $this->_getActiveOrders();
		$output['orders'] = $orderBook;
		// pr($output['orders']); die;
		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/orders/active');
		$this->load->view('admin/includes/footer');
	}

	private function _getSymbols(){
		$gemini = new Gemini($this->key, $this->secret);
		$orders = $gemini->symbols();
		return $orders;
	}

	private function _getPastOrders($symbol){
		$gemini = new Gemini($this->key, $this->secret);
		$payload = array();
		$payload['symbol'] = $symbol;
		$orders = $gemini->getPastTrades($payload);
		return $orders;
	}

	private function _getActiveOrders(){
		$gemini = new Gemini($this->key, $this->secret);
		$orders = $gemini->getActiveOrders();
		return $orders;
	} 

	public function add_order(){
		$output['page_title'] = "Add Order";
		$output['left_menu'] = "order_management";
		$output['left_submenu'] = "add_order";

		if ($this->input->post()) {
			$this->form_validation->set_rules('symbol', 'Symbol', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			$this->form_validation->set_rules('price', 'Price', 'trim|required');
			$this->form_validation->set_rules('side', 'Side', 'trim|required');
			$this->form_validation->set_rules('order_type', 'Order type', 'trim|required');

			if ($this->form_validation->run()) {
				$symbol = $this->input->post('symbol');
				$amount = $this->input->post('amount');
				$price = $this->input->post('price');
				$side = $this->input->post('side');
				$order_type = $this->input->post('order_type');
				$sendData = array();
				$date = date('Y-m-d');
				$date = str_replace('-', '', $date);
				$unique_string = str_shuffle("1234567890");
				$sendData['client_order_id'] = $date.'-'.$unique_string;
				$sendData['symbol'] = $symbol;
				$sendData['amount'] = $amount;
				$sendData['price'] = $price;
				$sendData['side'] = $side;
				$sendData['type'] = 'exchange limit';
				$order = $this->_addOrder($sendData);
				$data['redirectURL'] = base_url('admin/orders/add_order');
				$message = 'Your limit buy order has been placed.';
				$success = true;
			} else {
				$message = validation_errors();
				$success = false;
			}

			$data['message'] = $message;
			$data['success'] = $success;
			json_output($data);
		}

		$symbols = $this->_getSymbols();
		$output['symbols'] = $symbols;
		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/orders/add_order');
		$this->load->view('admin/includes/footer');
	}

	private function _addOrder($data){
		$gemini = new Gemini($this->key, $this->secret);
		$orders = $gemini->newOrder($data);
		return $orders;
	} 
}