<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Users class.
 * 
 */
class Team_management extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/team_management_model', 'team');
    }

    public function index()
    {
        $output['page_title'] = "Teams";
        $output['left_menu'] = "team_management";
        $output['left_submenu'] = "teams";

        $output['usersdata'] = $this->team->get_all_team_members();

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/team_management/list');
        $this->load->view('admin/includes/footer');
    }

    public function add()
    {
        $output['page_title'] = 'Add Team Member';
        $output['left_menu'] = 'team_management';
        $output['left_submenu'] = 'add_teams';
        $output['task'] = 'add';
        $output['id'] = '';
        $output['name'] = '';
        $output['designation'] = '';
        $output['status'] = 'Active';

        if ($this->input->post()) {
            $success = true;
            $message = '';
            /*set validation rules*/
            $this->form_validation->set_rules('name', 'Name', 'trim|alpha_numeric_spaces|required');
            $this->form_validation->set_rules('designation', 'Designation', 'trim|required');
            

            if ($this->form_validation->run()) 
            {
                $input = array();

                if (isset($_FILES['profile']['tmp_name']) && $_FILES['profile']['tmp_name']) {
                    $directory = './assets/uploads/images';
                    @mkdir($directory, 0777);
                    @chmod($directory, 0777);
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('profile')) {
                        $image = $this->upload->data();
                        $file_name = $image['file_name'];
                        $input['image'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                    }
                }
                if ($success) {

                    $name = ucfirst($this->input->post('name',true));
                    $designation = $this->input->post('designation', true);
                    $status = $this->input->post('status', true);

                    $input['name'] = $name;
                    $input['designation'] = $designation;               
                    $input['status'] = $status;
                    
                    //pr($input); die;

                    $insert_id = $this->team->add_member($input);

                    if ($insert_id) {
                        $success = true;
                        $message = 'Member added Succesfully';
                        $data['redirectURL'] = base_url('admin/team_management');
                    }else
                    {
                        $success = false;
                        $message = "Technical Error! Please Try Again later...";
                    }
                }
            }
            else
            {
                $message = validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/team_management/form');
        $this->load->view('admin/includes/footer');

    }

    public function update($id)
    {
        $records = $this->team->get_member_for_update($id);
        //pr($records); die;
        $output['records'] = $records;

        $output['page_title'] = "Edit Team Member";
        $output['left_menu'] = 'team_management';
        $output['left_submenu'] = 'edit_teams';
        $output['task'] = "edit";
        $output['id'] = $records->id; 
        $output['name'] = $records->name;
        $output['designation'] = $records->designation;
        $output['status'] = $records->status;
        //$output['password'] = $records->password;

        if ($this->input->post())
        {
            $success = true;
            $message = '';
            /*set validation rules*/
            $this->form_validation->set_rules('name', 'Name', 'trim|required|alpha_numeric_spaces');
            $this->form_validation->set_rules('designation', 'Designation', 'trim|required');

            if ($this->form_validation->run()) 
            {
                $input = array();

                if (isset($_FILES['profile']['tmp_name']) && $_FILES['profile']['tmp_name']) {
                    $directory = './assets/uploads/images';
                    @mkdir($directory, 0777);
                    @chmod($directory, 0777);
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('profile')) {
                        $image = $this->upload->data();
                        $file_name = $image['file_name'];
                        $input['image'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                    }
                }

                if ($success) {

                    $name = ucfirst($this->input->post('name',true));
                    $designation = ucfirst($this->input->post('designation',true));
                    $status = $this->input->post('status', true);

                    $input['name'] = $name;
                    $input['designation'] = $designation;
                    $input['status'] = $status;
                    //$input['last_ip'] = $this->input->ip_address();
                    //pr($input); die;

                    $update_record = $this->team->update_member_data($id, $input);

                    if ($update_record) {
                        $success = true;
                        $message = 'Member Updated Succesfully';
                        $data['redirectURL'] = base_url('admin/team_management');
                    }
                }
            }
            else
            {
                $message = validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/team_management/form');
        $this->load->view('admin/includes/footer');
    }

    public function delete()
    {
        $id = $this->input->post('record_id', true);
        $this->team->delete_member($id);
        $data['success'] = true;
        $data['message'] = "User Deleted Successfully";
        $data['callBackFunction'] = 'callBackCommonDelete';
        $data['redirectURL'] = base_url('admin/team_management');
        json_output($data);
    }

    public function view($id)
    {
        $output['page_title'] = "View Member Details";
        $output['left_menu'] = 'team_management';
        $output['left_submenu'] = 'view_teams';

        $view_record = $this->team->view_member($id);
        $output['usersdata'] = $view_record;

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/team_management/view');
        $this->load->view('admin/includes/footer');
    }

    public function change_status()
    {
        $id = $this->input->get('id', true);
        $status = $this->input->get('status', true);
        $this->team->change_status_by_id($id, $status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    public function multi_task_operation()
    {
        $task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $data_ids = explode(',',$ids);

        //print_r($ids); die;

        foreach ($data_ids as $key => $value) {
            
            if ($task == 'Delete') {            
            
                $this->team->delete_member($value);
                $message = "Selected rows deleted";
            }
            else if ($task == 'Active' || $task == 'Inactive') {
                $this->team->change_status_by_id($value, $task);
                $message = "Status changed of the selected Rows";
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }
}

?>