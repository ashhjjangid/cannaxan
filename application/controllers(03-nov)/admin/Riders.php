<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users class.
 * 
 * @extends CI_Controller
 */

class Riders extends CI_Controller {

   /**
    * __construct function.
    * @access public 
    */
       
    function __construct() {
        Parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/riders_model', 'riders');
    }

    /**
     * index function.
     * 
     * @access public
     * @return null
     */

    function index() {
        $output['page_title'] = 'Riders';
        $output['left_menu'] = 'user_management';
        $output['left_submenu'] = 'rider_list';

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/riders/rider_list');
        $this->load->view('admin/includes/footer');
    }

    /**
     * get_ajax_list function.
     * 
     * @access public
     * @return list of all customers
     */

    public function get_ajax_list()
    {      

        $this->load->library('pagination');

        $success = true;
        $html = false;
        $load_prev_link = false;
        $load_next_link = false;
        $keyword = $this->input->get('keyword', true);
        $page_limit = $this->input->get('page_limit', true);
        $column_name = $this->input->get('column_name', true);
        $sorting_order = $this->input->get('sorting_order', true);
        $status = $this->input->get('status',true);         
        $page_no = $this->input->get('page_no') ? $this->input->get('page_no') : 1;
        $page_no_index = ($page_no - 1) * $page_limit;
        $sQuery = '';
        if($keyword)
        {
            $sQuery = $sQuery.'&keyword='.$keyword;
        }
        if($page_limit)
        {
            $sQuery = $sQuery.'&page_limit='.$page_limit;       
        }
        if ($column_name) 
        {
            $sQuery = $sQuery.'&column_name='. $column_name;
        }
        if ($sorting_order) 
        {
            $sQuery = $sQuery.'&sorting_order='. $sorting_order;
        }
        $per_page = $this->input->get('page_limit', true);
        $searchA['search_index'] = $page_no_index;
        $searchA['limit'] = $per_page;      
        $searchA['keyword'] = $keyword;
        $searchA['column_name'] = $column_name;
        $searchA['sorting_order'] = $sorting_order;
        $searchA['status'] = $status; 
        $config['base_url'] = site_url('admin/riders/get_ajax_list?'.$sQuery);
        $config['total_rows'] = $this->riders->countRiders($searchA);
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);

        $data['paging'] = $this->pagination->create_links();

        $records = $this->riders->getAllRiders($searchA);
        $output['users'] = $records; 
        $html = $this->load->view('admin/riders/ajax_list',$output, true);
        $data['html'] = $html;
        $data['success']= true;

        json_output($data);
    }

    /**
     * add function.
     * 
     * @access public
     * @return null
     */

    function add() {
        $output['page_title'] = 'Add Rider';
        $output['left_menu'] = 'user_management';
        $output['left_submenu'] = 'rider_list';
        $output['message'] = '';
        $output['task'] = 'add';
        $output['status'] = 'Active';
        $output['gender'] = 'Male';
        $output['id'] = '';
        $output['first_name'] = '';
        $output['last_name'] = '';
        $output['username'] = '';
        $output['email'] = '';
        $output['phonenumber'] = '';        
        $output['date_of_birth'] = '';
        $output['address'] = '';

        if ($this->input->post()) {
            $success = true;
            $message = '';
            /* set validation rules */
            $this->form_validation->set_rules('first_name', 'first name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('last_name', 'last name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('date_of_birth', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('email', 'email address', 'trim|required|valid_email|is_unique[tbl_users.email]');
            $this->form_validation->set_rules('phonenumber', 'phone number', 'trim|min_length[6]|max_length[20]|regex_match[/^[+0-9]+$/]');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]|max_length[20]');            
            
            if ($this->form_validation->run()) {
                $banner_image='';
                $input = array();
                $error = 0;
                               
                if (isset($_FILES['profile']['name']) && $_FILES['profile']['name']) {
                    $directory = './assets/uploads/rider_images/'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'gif|jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if ($this->upload->do_upload('profile')) {
                        $image = $this->upload->data();
                        $file_name = $image['file_name'];
                        $input['profile'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                    }
                }

                if ($success) {
                    $email = $this->input->post('email');
                    $first_name = ucfirst($this->input->post('first_name',true));
                    $last_name = ucfirst($this->input->post('last_name',true));
                    $username = $this->input->post('username', true);

                    $password = $this->input->post('password',true);
                    $ency_password = encrpty_password($password);
                    $input['password'] = $ency_password;

                    $input['first_name'] = ucfirst($this->input->post('first_name',true));
                    $input['last_name'] = ucfirst($this->input->post('last_name',true));
                    $input['date_of_birth'] = date('Y-m-d',strtotime($this->input->post('date_of_birth', true)));
                    $input['email'] = $this->input->post('email');
                    $input['address'] = $this->input->post('address', true);
                    $input['phonenumber'] = $this->input->post('phonenumber',true);
                    $input['gender'] = $this->input->post('gender',true);
                    $input['status'] = $this->input->post('status',true);
                    
                    /*insert as customer user */
                    $insert_id = $this->riders->add_new_rider($input);
                    
                    if ($insert_id) {
                        $success = true;
                        $message = 'New Rider added successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = site_url('admin/riders');
                    
                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
                }            
            } else {
                $message.= validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        } 
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/riders/form');
        $this->load->view('admin/includes/footer');
    }

    /**
     * check_email_exist function.
     * 
     * @access public
     * @param mixed $value
     */

    public function check_email_exist($value, $id = '') {
        
        if ($this->riders->doesEmailExist( $value, $id )) {
            $this->form_validation->set_message('check_email_exist', 'This E-Mail address already register on our server.');
            return false;
        }
        return true;
    }

    /**
     * check_username_exist function.
     * 
     * @access public
     * @param mixed $value
     * @param mixed $id     
     * @return true or false
     */

    public function check_username_exist($value, $id = '') {
        
        if ($this->riders->doesUsernameExist( $value, $id )) {
            $this->form_validation->set_message('check_email_exist', 'This User name already register on our server.');
            return false;
        }
        return true;
    }

    /**
     * update function.
     * 
     * @access public
     * @param mixed $id     
     * @return null
     */

    public function update($id) 
    {
        $output['page_title'] = 'Update Rider';
        $output['left_menu'] = 'user_management';
        $output['left_submenu'] = 'rider_list';
        $output['task'] = 'edit';
        $output['message'] = '';

        /* get customer record*/
        $record = $this->riders->get_rider_by_id($id);
        checkRequestedDataExists($record);
        /* assign values */

        $output['record'] = $record;
        $output['status'] = $record->status;
        $output['id'] = $record->id;
        $output['first_name'] = $record->first_name;
        $output['last_name'] = $record->last_name;
        $output['date_of_birth'] = date('M d Y', strtotime($record->date_of_birth));
        $output['email'] = $record->email;
        $output['phonenumber'] = $record->phonenumber;
        $output['gender'] = $record->gender;
        $output['status'] = $record->status;
        $output['address'] = $record->address;
        //$output['password'] = $record->password;
        
        if ($this->input->post()) 
        {
            $message = '';
            $success = true;
            /* set validation rules */
            $this->form_validation->set_rules('first_name', 'first name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('last_name', 'last name', 'alpha_numeric_spaces|trim|required');
            $this->form_validation->set_rules('date_of_birth', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('email', 'email address', 'trim|required|valid_email|callback_check_email_exist['.$id.']');
            $this->form_validation->set_rules('phonenumber', 'Phone Number', 'trim|min_length[6]|max_length[20]|regex_match[/^[+0-9]+$/]');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]|max_length[20]');
            $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required|matches[password]');
            //$this->form_validation->set_rules('latitude', 'valid address latitude', 'required');
            //$this->form_validation->set_rules('longitude', 'valid address longitude', 'required');
        
            if ($this->form_validation->run()) {

                $input = array();
                $error  = 0;
                if (isset($_FILES['profile']['name']) && $_FILES['profile']['name']) {
                    $directory = './assets/uploads/rider_images/'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'gif|jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if ($this->upload->do_upload('profile')) {
                        $image_data = $this->upload->data();
                        $file_name = $image_data['file_name'];
                        $input['profile'] = $file_name;
                        //pr($input); die;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                    }                    
                }

                if ($success) {
                    $password = $this->input->post('password', true);
                    $ency_password = encrpty_password($password);
                    $input['password'] = $ency_password;

                    $input['first_name'] = ucfirst($this->input->post('first_name',true));
                    $input['last_name'] = ucfirst($this->input->post('last_name',true));
                    $input['date_of_birth'] = date('Y-m-d', strtotime($this->input->post('date_of_birth', true)));
                    $input['email'] = $this->input->post('email',true);
                    $input['phonenumber'] = $this->input->post('phonenumber',true);
                    $input['gender'] = $this->input->post('gender',true);
                    $input['status'] = $this->input->post('status',true);
                    $input['address'] = $this->input->post('address', true);
                    //pr($input); die;

                    /* update records */
                    $response = $this->riders->update_rider_details($id,$input);

                    if ($response) {
                        $success = true;
                        $message = 'Rider updated successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = site_url('admin/riders');                    
                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
                }            
            } else {
                $success = false;
                $message = validation_errors();     
            }   
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/riders/form');
        $this->load->view('admin/includes/footer');
    }

    /**
     * view function.
     * 
     * @access public
     * @param mixed $id      
     */

    public function view($id) {
        $output['page_title'] = 'View Rider Details';
        $output['left_menu'] = 'user_management';
        $output['left_submenu'] = 'rider_list';
        /* get record by id */
        $record = $this->riders->get_rider_by_id($id);
        checkRequestedDataExists($record);
        $output['ridersdata'] = $record; 

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/riders/view_rider');
        $this->load->view('admin/includes/footer');
    }


    /**
     * change_status function.
     * 
     * @access public
     * @return null
     */

    public function change_status()  {
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
        $this->riders->change_status_by_id($id,$status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    
    /**
     * delete function.
     * 
     * @access public
     * @return null
     */

    public function delete() {


        $id = $this->input->post('id',true);
        $this->riders->delete_user_by_id($id);

        $data['success'] = true;
        $data['message'] = 'Riders has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }



    /**
     * multi_task_operation function.
     * 
     * @access public
     * @return null
     */

    public function multi_task_operation() 
    {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        
        foreach ($dataIds as $key => $value) {
            
            if ($task == 'Delete') {
                $this->riders->delete_user_by_id($value);
                $message = 'Selected Riders has been deleted successfully.';
            
            } else  if ($task=='Active' || $task=='Inactive') {
                $this->riders->change_status_by_id($value,$task);            
                $message = 'Status of selected Riders has been changed successfully.';
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data); 
    }

    /**
     * check_space function.
     * 
     * @access public
     * @param mixed $value 
     * @return true or false
     */

    public function check_space($value){
        
        if ( ! preg_match("/^[a-zA-Z'0-9_@#$^&%*)(_+}{;:?\/., -]*$/", $value) ) {
           $this->form_validation->set_message('check_space', 'The %s field should contain only letters, numbers or periods');
           return false;
        }
        else
        return true;
    }
}
