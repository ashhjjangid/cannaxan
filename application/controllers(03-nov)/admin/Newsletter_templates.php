<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Newsletter_templates class.
 * 
 * @extends CI_Controller
 */

class Newsletter_templates extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */	

	function __construct() {
		parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		$this->load->model('admin/Newsletter_templates_model', 'newsletter');
		$this->load->model('Mailsending_model', 'mailsend');
		$this->config->set_item('language', 'english');
	}

    /**
     * index function.
     * 
     * @access public
     * @return null
     */

	function index() {
		$all_templates = $this->newsletter->get_all_templates();

		$output['page_title'] = 'Newsletter Templates';
		$output['left_menu'] = 'Templates';
		$output['left_submenu'] = 'newsletter_templates_list';
		$output['templates'] = $all_templates;

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/newsletter_templates/templates_list');
		$this->load->view('admin/includes/footer');
	}

    /**
     * add function.
     * 
     * @access public
     * @return null
     */

	function add() {
		$output['page_title'] = 'Add newsletter templates';
		$output['left_menu'] = 'Templates';
		$output['left_submenu'] = 'newsletter_templates_add';
		$output['message'] = '';
		$output['id'] = '';
		$output['subject'] = '';
		$output['content'] = '';
		$output['status'] = 'Active';
		$output['created_at'] = '';
		
		if ($this->input->post()) {
			$this->form_validation->set_rules('subject', 'subject', 'trim|required');
			$this->form_validation->set_rules('message', 'message', 'trim|required');
			$this->form_validation->set_rules('status', 'status', 'trim|required');

			if ($this->form_validation->run()) {
					$inesrt_arr = array();
					$inesrt_arr['subject'] = $this->input->post('subject');
					$inesrt_arr['message'] = $this->input->post('message');
					$inesrt_arr['status'] = $this->input->post('status');
					$inesrt_arr['created_at'] = getDefaultToGMTDate(time());
					$insert_id = $this->newsletter->insert_templates($inesrt_arr);

					if ($insert_id) {
						$success = true;
						$message = 'News letter templates added successfully';
						$data['redirectURL']	= site_url('admin/newsletter_templates');
					
					} else {
						$message = 'Technical error. Please try again later';
						$success = false;
					}
						
			} else {
				$success = false;
				$message = validation_errors();
			}
			$data['message'] = $message;
			$data['success'] = $success;
			echo json_encode($data);die;
		}
		$constants = array();
		$constantsA = 'Website_URL,Logo,Unsubscribe_Newsletter_Link';
		$constantsB = explode(',',$constantsA);
		foreach ($constantsB as $key => $value) {
			$constants[$value] = '{{'.$value.'}}';
		}
		$output['constants'] = $constants;
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/newsletter_templates/templates_form');
		$this->load->view('admin/includes/footer');
	}

    /**
     * update function.
     * 
     * @access public
     * @param mixed $id
     * @return null
     */

	function update($id){
		$output['page_title'] = 'Newsletter templates';
		$output['left_menu'] = 'Templates';
		$output['left_submenu'] = 'templates';
		$output['success'] = '';
		$output['message'] = '';
		$output['task'] = 'edit';
		$record = $this->newsletter->get_templates_by_id($id);
		
		if (!$record) {
			redirect('admin/newsletter_templates');
		}
		$output['id'] = $record->id;
		$output['subject'] = $record->subject;
		$output['content'] = $record->message;
		$output['status'] = $record->status;
		 		
		if ($this->input->post()) {
			$message = '';
			$success = true;
			$this->form_validation->set_rules('subject', 'subject', 'trim|required');
			$this->form_validation->set_rules('message', 'message', 'trim|required');
			$this->form_validation->set_rules('status', 'status', 'trim|required');
			
			if ($this->form_validation->run()) {
				$update_arr = array();
				$update_arr['subject'] = $this->input->post('subject');
				$update_arr['message'] = $this->input->post('message');
				$update_arr['status'] = $this->input->post('status');
				$update_arr['updated_at'] = getDefaultToGMTDate(time());

				$response = $this->newsletter->update_templates($id, $update_arr);

				
				if ($response) {
					$success = true;
					$message = 'News letter templates updated successfully';
					$data['redirectURL'] = site_url('admin/newsletter_templates');
				
				} else {
					$message = 'Technical error. Please try again later';
					$success = false;
				}
			
			} else {
				$success = false;
				$message = validation_errors();
			}
			$data['message'] = $message;
			$data['success'] = $success;
			echo json_encode($data);die;
		}
		$constants = array();
		$constantsA = 'Website_URL,Log,Unsubscribe_Newsletter_Link';
		$constantsB = explode(',',$constantsA);
		foreach ($constantsB as $key => $value) {
			$constants[$value] = '{{'.$value.'}}';
		}
		$output['constants'] = $constants;
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/newsletter_templates/templates_form');
		$this->load->view('admin/includes/footer');
	}

    /**
     * view function.
     * 
     * @access public
     * @param mixed $id
     * @return null
     */

	function view($id){
		$output['page_title'] = 'View newsletter templates';
		$output['left_menu'] = 'Templates';
		$output['left_submenu'] = 'templates';
        $single_template = $this->newsletter->get_templates_by_id($id);
        $output['templates'] = $single_template;
        $this->load->view('admin/includes/header',$output);
		$this->load->view('admin/newsletter_templates/view_tamplates');
		$this->load->view('admin/includes/footer');
	}

    /**
     * multiTaskOperation function.
     * 
     * @access public
     * @return null
     */

	function multiTaskOperation() {
		$task = $this->input->post('task');
		$ids = $this->input->post('ids');
		$dataIds = explode(',',$ids);
		
		foreach ($dataIds as $key => $value) {
			
			if ($task == 'Delete') {
				$this->newsletter->delete_templates($value);
				$message = 'Selected record deleted successfully.';
			
			} else if ($task=='Active' || $task=='Inactive') {
				$this->newsletter->change_status_by_id($value,$task);			
				$message = 'Status of Selected records changed successfully.';
			}
		}		
		$data['ids'] = $ids;
		$data['success'] = true;
		$data['message'] = $message;
		$data['callBackFunction'] = 'callBackCommonDelete';
		echo json_encode($data); die;
	}

    /**
     * change_status function.
     * 
     * @access public
     * @return null
     */
    
	function change_status() {
		$id = $this->input->get('id');
		$status = $this->input->get('status');
		$this->newsletter->change_status_by_id($id,$status);
		$data['success'] = true;
		$data['message'] = 'Record updated successfully';
		echo json_encode($data);
	}

}