<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Contact_us class.
 * 
 * @extends CI_Controller
 */

class Contact_us extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */
   
    function __construct() {
        Parent::__construct();
        checkAdminLogin();
        $this->load->model('admin/contact_us_model', 'contact_us');
        $this->load->model('mailsending_model', 'mailsend');
    }
    
    /**
     * index function.
     * 
     * @access public
     * @return contact user list
     */

    public function index() {
        $output['page_title'] = 'Contact us';
        $output['left_menu'] = 'Contact';
        $output['left_submenu'] = 'Contact_list';
        $records = $this->contact_us->getAllContactQueries();
        $output['allContact'] = $records;
        
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/contact_us/contact_list');
        $this->load->view('admin/includes/footer');
    }

    /**
     * reply function.
     * 
     * @access public
     * @param mixed $id
     * @return contact user record
     */


    public function reply($id) {
        $output['page_title'] = 'Reply contact us';
        $output['left_menu'] = 'Contact';
        $output['left_submenu'] = 'Contact_list';
        $input = array();
        $input['is_viewed'] = 'Yes';
        $this->contact_us->setContactQuery($id,$input);
        $contactQueries = $this->contact_us->getSignleRecordById($id);
        
        if (isset($_POST) && !empty($_POST)) {
            $this->form_validation->set_rules('reply_message', 'Reply message', 'trim|required');
            
            if ($this->form_validation->run()) {
                $input = array();

                $input['reply_message'] = $this->input->post('reply_message',true);
                $this->contact_us->setContactQuery($id,$input);

                $replyMessage = $this->input->post('reply_message',true);
                $this->mailsend->replyContactQuery($replyMessage,$id);
                
                $message = 'Reply Message Successfully Sent';
                $success = true;
                $data['redirectURL'] = site_url('admin/contact_us');
            
            } else {
                $success = false;
                $message = validation_errors();
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }
        $output['contactUserQueries'] = $contactQueries;        
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/contact_us/view_user_contact');
        $this->load->view('admin/includes/footer');
    }    

    /**
     * multiTaskOperation function.
     * 
     * @access public
     * @return void
     */

    function multiTaskOperation() {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        /*foreach ($dataIds as $key => $value) {
        }*/        
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }
}

