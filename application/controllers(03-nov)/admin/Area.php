<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Area class.
 * 
 */
class Area extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/Area_model', 'area');
    }

  public function index()
    {
        $output['page_title'] = "Area";
        $output['left_menu'] = "area_selection";
        $output['left_submenu'] = "area_list";
        $area= $this->area->get_all_area_data();
        $data= $this->area->get_area_data();
       // print_r($data);die;
        $output['area']=$area;
        $output['data']=$data;

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/area/list');
        $this->load->view('admin/includes/footer');
    }



    public function change_status()
    {
        $id = $this->input->get('id', true);
        $status = $this->input->get('status', true);
        $this->area->change_status_by_id($id, $status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    public function multi_task_operation()
    {
        $task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $data_ids = explode(',',$ids);

        //print_r($ids); die;

        foreach ($data_ids as $key => $value) {
            
            if ($task == 'Delete') {            
            
                $this->area->delete_member($value);
                $message = "Selected rows deleted";
            }
            else if ($task == 'Active' || $task == 'Inactive') {
                $this->area->change_status_by_id($value, $task);
                $message = "Status changed of the selected Rows";
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
      }

    public function changestatus()
    {
        $id = $this->input->get('id', true);
        $status = $this->input->get('status', true);
        $this->area->change_status_by_id_marker($id, $status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }
     public function multitaskoperation()
    {
        $task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $data_ids = explode(',',$ids);

        //print_r($ids); die;

        foreach ($data_ids as $key => $value) {
            
            if ($task == 'Delete') {            
            
                $this->area->delete_member($value);
                $message = "Selected rows deleted";
            }
            else if ($task == 'Active' || $task == 'Inactive') {
                $this->area->change_status_by_id_marker($value, $task);
                $message = "Status changed of the selected Rows";
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
      }

   public function view($id)
    {        
        $output['page_title'] = 'View Area';
        $output['left_menu'] = "area_selection";
        $output['left_submenu'] = "area_list";

        $records = $this->area->get_area_data($id);
        $output['records'] = $records;
       //print_r($output['records']);die;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/area/view');
        $this->load->view('admin/includes/footer');
    } 

}
?>