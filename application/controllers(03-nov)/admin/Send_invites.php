<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Send_invites class.
 * 
 * @extends CI_Controller
 */

class Send_invites extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */	

	function __construct() {
		parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		$this->load->model('admin/Send_invites_model', 'invites');
		$this->load->model('mailsending_model', 'mailsend');
		$this->config->set_item('language', 'english');
	}

    /**
     * index function.
     * 
     * @access public
     * @return null
     */

	function index() {

		$output['page_title'] = 'Invites';
		$output['left_menu'] = 'Invites';
		$output['left_submenu'] = 'send_invites_list';
		$all_data = $this->invites->get_all_data();
		//print_r($all_data);die;
		$output['data_invites'] = $all_data;

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/send_invites/email_list');
		$this->load->view('admin/includes/footer');
	}
    
}