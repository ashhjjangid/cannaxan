<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Admin class.
 * 
 * @extends CI_Controller
 */

class Admin extends CI_Controller
{
    /**
     * __construct function.
     * 
     * @access public
     * @return void
    */

    public function __construct()
    {
        Parent::__construct();
        checkAdminLogin();
        $this->load->model('admin/user_model', 'users');
        $this->load->model('admin/dashboard_model', 'dashboard');
        $this->load->model('mailsending_model', 'mailsend');
        $this->load->model('admin/Users_model', 'user');
        $this->load->model('admin/Patients_model', 'patients');
        $this->admin_id = $this->session->userdata('admin_id');
    }

    /**
     * index function.
     * 
     * @access public
     * @return void
     */

    public function index()
    {
        $output['page_title'] = "Dashboard";
        $output['left_menu']  = 'Dashboard';

        $users = $this->user->Countnumberofdoctors();
        $output['users'] = $users;
        //pr($output['users']);
        $patients = $this->patients->Countnumberofpatients();
        $output['patients'] = $patients;
        //pr($output['patients']);die;
        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/admin/index');
        $this->load->view('admin/includes/footer');
    }

    /**
     * check_email_exist function.
     * 
     * @access public
     * @param mixed $value
     * @param mixed $id
     * @return true or false
     */

    public function check_email_exist($value, $id = '') {
        
        if ($this->users->doesEmailExist( $value, $id )) {
            $this->form_validation->set_message('check_email_exist', 'This E-Mail address already register on our server.');
            return false;
        }
        return true;
    }


    /**
     * updateProfile function.
     * 
     * @access public
     * @return null
     */

    public function updateProfile()
    {
        $output['page_title'] = 'Update profile';
        $output['left_menu']  = 'Members';
        $output['message'] = '';
        $user = $this->users->getUserDetails($this->admin_id);
        $userId = $this->admin_id;

        if (isset($_POST) && !empty($_POST)) {

            $this->form_validation->set_rules('username', 'User name', 'trim|required|min_length[6]|max_length[50]|regex_match[/^[-A-Za-z0-9_ ]+$/]');
            $this->form_validation->set_rules('email', 'Email address', 'trim|required|valid_email|callback_check_email_exist['.$userId.']');

            if ($this->form_validation->run()) {
                $input             = array();
                $input['username'] = strtolower($this->input->post('username',true));
                $input['email']    = $this->input->post('email');

                $this->users->setUserDetails($userId, $input);
                $this->session->set_userdata(array('admin_name' => $this->input->post('username',true)));
                
                $message               = 'Profile updated successfully';
                $success               = true;
                $output['redirectURL'] = site_url('admin/update-profile');

            } else {
                $success = false;
                $message = validation_errors();
            }
            
            $output['message'] = $message;
            $output['success'] = $success;
            json_output($output);
        }

        $output['first_name'] = $user->first_name;
        $output['last_name']  = $user->last_name;
        $output['username']   = $user->username;
        $output['email']      = $user->email;
        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/users/update_profile');
        $this->load->view('admin/includes/footer');
    }

    /**
     * changePasswordForAdmin function.
     * 
     * @access public
     * @return null
     */

    public function changePasswordForAdmin()
    {
        $output['page_title'] = 'Change password';
        $output['left_menu']  = 'Members';
        $output['message']    = '';

        if (isset($_POST) && !empty($_POST)) {
            /* set validation rule*/
            $this->form_validation->set_rules('current_password', 'Current password', 'trim|required');
            $this->form_validation->set_rules('new_password', 'New password', 'trim|required|matches[re_new_password]|min_length[6]|max_length[50]');
            $this->form_validation->set_rules('re_new_password', 'Confirmation password', 'trim|required|min_length[6]|max_length[50]');

            if ($this->form_validation->run()) {
                $password = $this->input->post('current_password',true);
                $ency_password = get_encrypted_password($password);
                $userId = $this->admin_id;
                $user = $this->users->checkUserExistByOldPassword($ency_password, $userId);

                if ($user) {
                    $newPassword = $this->input->post('new_password',true);
                    $encryptedPassword = get_encrypted_password($newPassword);
                    $this->users->setUserPassword($userId, $encryptedPassword);
                    
                    $message = 'Password changed successfully';
                    $success = true;
                    $output['resetForm'] = true;
                    $output['redirectURL'] = site_url('admin/');
                } else {
                    $message = 'Current password incorrect';
                    $success = false;
                }
            } else {
                $success = false;
                $message = validation_errors();
            }
            $output['message'] = $message;
            $output['success'] = $success;
            json_output($output);
        }
        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/users/change_password');
        $this->load->view('admin/includes/footer');
    }


}
