<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DosistypII extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('admin/dosistypII_model', 'dosistyp2');
	}

	public function index()
	{
		$output['page_title'] = 'DosisTyp II';
		$output['left_menu'] = 'dosistyp_management';
		$output['left_submenu'] = 'dosistyp';
		
		$record = $this->dosistyp2->getAllDosisTypII();
		$output['records'] = $record;

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/dosistypII/list');
		$this->load->view('admin/includes/footer');
	}

	function add() {

		$output['page_title'] = 'Add New DosisTyp II';
		$output['left_menu'] = 'dosistyp_management';
		$output['left_submenu'] = 'add_dosistyp';
		$output['id'] = '';
		$output['tag_name'] = '';
		$output['morgens_anzahl_durchgange'] = '';
		$output['mittags_anzahl_durchgange'] = '';
		$output['abends_anzahl_durchgange'] = '';
		$output['nachts_anzahl_durchgange'] = '';

		$output['morgens_anzahl_sps'] = '';
		$output['mittags_anzahl_sps'] = '';
		$output['abends_anzahl_sps'] = '';
		$output['nachts_anzahl_sps'] = '';
		
		$output['thc_morgens_mg'] = '';
		$output['thc_mittags_mg'] = '';
		$output['thc_abends_mg'] = '';
		$output['thc_nachts_mg'] = '';
		
		$output['summe_thc_mg'] = '';
		$output['display_order'] = '';
		$output['status'] = 'Active';

		if ($this->input->post()) {
			
			$this->form_validation->set_rules('tag_name','Tag Name','trim|required');
			$this->form_validation->set_rules('morgens_anzahl_durchgange','Morgens Anzahl Durchgänge','trim|required');
			$this->form_validation->set_rules('mittags_anzahl_durchgange','Mittags Anzahl Durchgänge','trim|required');
			$this->form_validation->set_rules('abends_anzahl_durchgange','Abends Anzahl Durchgänge','trim|required');
			$this->form_validation->set_rules('nachts_anzahl_durchgange','Nachts Anzahl Durchgänge','trim|required');

			$this->form_validation->set_rules('morgens_anzahl_sps','Morgens Anzahl SPS','trim|required');
			$this->form_validation->set_rules('mittags_anzahl_sps','Mittags Anzahl SPS','trim|required');
			$this->form_validation->set_rules('abends_anzahl_sps','Abends Anzahl SPS','trim|required');
			$this->form_validation->set_rules('nachts_anzahl_sps','Nachts Anzahl SPS','trim|required');

			$this->form_validation->set_rules('thc_morgens_mg','THC morgens [mg]','trim|required');
			$this->form_validation->set_rules('thc_mittags_mg','THC mittags [mg]','trim|required');
			$this->form_validation->set_rules('thc_abends_mg','THC abends [mg]','trim|required');
			$this->form_validation->set_rules('thc_nachts_mg','THC nachts [mg]','trim|required');

			$this->form_validation->set_rules('summe_thc_mg','Summe THC [mg]','trim|required');
			$this->form_validation->set_rules('display_order','Display Order','trim|required');

			if ($this->form_validation->run()) {
				
				$input = array();
				$input['tag_name'] = ucfirst($this->input->post('tag_name'));
				$input['morgens_anzahl_durchgange'] = $this->input->post('morgens_anzahl_durchgange');
				$input['mittags_anzahl_durchgange'] = $this->input->post('mittags_anzahl_durchgange');
				$input['abends_anzahl_durchgange'] = $this->input->post('abends_anzahl_durchgange');
				$input['nachts_anzahl_durchgange'] = $this->input->post('nachts_anzahl_durchgange');

				$input['morgens_anzahl_sps'] = $this->input->post('morgens_anzahl_sps');
				$input['mittags_anzahl_sps'] = $this->input->post('mittags_anzahl_sps');
				$input['abends_anzahl_sps'] = $this->input->post('abends_anzahl_sps');
				$input['nachts_anzahl_sps'] = $this->input->post('nachts_anzahl_sps');

				$thc_morgens_mg = $this->input->post('thc_morgens_mg');
				$thc_mittags_mg = $this->input->post('thc_mittags_mg');
				$thc_abends_mg = $this->input->post('thc_abends_mg');
				$thc_nachts_mg = $this->input->post('thc_nachts_mg');

				//pr($summe_thc_mg); die;

				$input['thc_morgens_mg'] = $thc_morgens_mg;
				$input['thc_mittags_mg'] = $thc_mittags_mg;
				$input['thc_abends_mg'] = $thc_abends_mg;
				$input['thc_nachts_mg'] = $thc_nachts_mg;

				$summe_thc_mg = $this->input->post('summe_thc_mg');
				//$summe_thc_mg = $thc_morgens_mg + $thc_mittags_mg + $thc_abends_mg + $thc_nachts_mg;

				$input['summe_thc_mg'] = $summe_thc_mg;
				$input['display_order'] = $this->input->post('display_order');

				$input['status'] = $this->input->post('status');
				$input['add_date'] = getDefaultToGMTDate(time());
				//pr($input); die;

				$insert_id = $this->dosistyp2->addNewDosistyp2($input);

				if ($insert_id) {
					
					//$message = 'Neuer DosistypII erfolgreich hinzugefügt';
					$message = 'New DosisTypII added succesfully';
					$success = true;
					$data['redirectURL'] = base_url('admin/dosistypII');
				} else {
					//$message = 'Etwas ist schief gelaufen! Bitte versuche es erneut...';
					$message = 'Something went wrong! Try Again...';
					$success = false;
				}
			} else {
				$message = validation_errors();
				$success = false;
			}

			$data['success'] = $success;
			$data['message'] = $message;
			json_output($data);
		}

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/dosistypII/form');
		$this->load->view('admin/includes/footer');		
	}

	function update($id) {

		$record = $this->dosistyp2->getDosisTypIIById($id);
		checkRequestedDataExists($record);
		$output['page_title'] = 'Edit DosisTyp II';
		$output['left_menu'] = 'dosistyp_management';
		$output['left_submenu'] = 'update_dosistyp';
		$output['id'] = $record->id;
		$output['tag_name'] = $record->tag_name;
		$output['morgens_anzahl_durchgange'] = $record->morgens_anzahl_durchgange;
		$output['mittags_anzahl_durchgange'] = $record->mittags_anzahl_durchgange;
		$output['abends_anzahl_durchgange'] = $record->abends_anzahl_durchgange;
		$output['nachts_anzahl_durchgange'] = $record->nachts_anzahl_durchgange;

		$output['morgens_anzahl_sps'] = $record->morgens_anzahl_sps;
		$output['mittags_anzahl_sps'] = $record->mittags_anzahl_sps;
		$output['abends_anzahl_sps'] = $record->morgens_anzahl_sps;
		$output['nachts_anzahl_sps'] = $record->morgens_anzahl_sps;
		
		$output['thc_morgens_mg'] = $record->thc_morgens_mg;
		$output['thc_mittags_mg'] = $record->thc_mittags_mg;
		$output['thc_abends_mg'] = $record->thc_abends_mg;
		$output['thc_nachts_mg'] = $record->thc_nachts_mg;
		
		$output['summe_thc_mg'] = $record->summe_thc_mg;
		$output['display_order'] = $record->display_order;
		$output['status'] = $record->status;

		if ($this->input->post()) {
			
			$this->form_validation->set_rules('tag_name','Tag Name','trim|required');
			$this->form_validation->set_rules('morgens_anzahl_durchgange','Morgens Anzahl Durchgänge','trim|required');
			$this->form_validation->set_rules('mittags_anzahl_durchgange','Mittags Anzahl Durchgänge','trim|required');
			$this->form_validation->set_rules('abends_anzahl_durchgange','Abends Anzahl Durchgänge','trim|required');
			$this->form_validation->set_rules('nachts_anzahl_durchgange','Nachts Anzahl Durchgänge','trim|required');

			$this->form_validation->set_rules('morgens_anzahl_sps','Morgens Anzahl SPS','trim|required');
			$this->form_validation->set_rules('mittags_anzahl_sps','Mittags Anzahl SPS','trim|required');
			$this->form_validation->set_rules('abends_anzahl_sps','Abends Anzahl SPS','trim|required');
			$this->form_validation->set_rules('nachts_anzahl_sps','Nachts Anzahl SPS','trim|required');

			$this->form_validation->set_rules('thc_morgens_mg','THC morgens [mg]','trim|required');
			$this->form_validation->set_rules('thc_mittags_mg','THC mittags [mg]','trim|required');
			$this->form_validation->set_rules('thc_abends_mg','THC abends [mg]','trim|required');
			$this->form_validation->set_rules('thc_nachts_mg','THC nachts [mg]','trim|required');

			$this->form_validation->set_rules('summe_thc_mg','Summe THC [mg]','trim|required');
			$this->form_validation->set_rules('display_order','Display Order','trim|required');

			if ($this->form_validation->run()) {
				
				$edit_arr = array();
				$edit_arr['tag_name'] = ucfirst($this->input->post('tag_name'));
				$edit_arr['morgens_anzahl_durchgange'] = $this->input->post('morgens_anzahl_durchgange');
				$edit_arr['mittags_anzahl_durchgange'] = $this->input->post('mittags_anzahl_durchgange');
				$edit_arr['abends_anzahl_durchgange'] = $this->input->post('abends_anzahl_durchgange');
				$edit_arr['nachts_anzahl_durchgange'] = $this->input->post('nachts_anzahl_durchgange');

				$edit_arr['morgens_anzahl_sps'] = $this->input->post('morgens_anzahl_sps');
				$edit_arr['mittags_anzahl_sps'] = $this->input->post('mittags_anzahl_sps');
				$edit_arr['abends_anzahl_sps'] = $this->input->post('abends_anzahl_sps');
				$edit_arr['nachts_anzahl_sps'] = $this->input->post('nachts_anzahl_sps');

				$thc_morgens_mg = $this->input->post('thc_morgens_mg');
				$thc_mittags_mg = $this->input->post('thc_mittags_mg');
				$thc_abends_mg = $this->input->post('thc_abends_mg');
				$thc_nachts_mg = $this->input->post('thc_nachts_mg');

				//pr($summe_thc_mg); die;

				$edit_arr['thc_morgens_mg'] = $thc_morgens_mg;
				$edit_arr['thc_mittags_mg'] = $thc_mittags_mg;
				$edit_arr['thc_abends_mg'] = $thc_abends_mg;
				$edit_arr['thc_nachts_mg'] = $thc_nachts_mg;


				//$summe_thc_mg = $thc_morgens_mg + $thc_mittags_mg + $thc_abends_mg + $thc_nachts_mg;
				$summe_thc_mg = $this->input->post('summe_thc_mg');

				$edit_arr['summe_thc_mg'] = $summe_thc_mg;
				$edit_arr['display_order'] = $this->input->post('display_order');

				$edit_arr['status'] = $this->input->post('status');
				$edit_arr['update_date'] = getDefaultToGMTDate(time());

				$insert_id = $this->dosistyp2->editDosistyp2ById($id, $edit_arr);

				if ($insert_id) {
					
					//$message = 'Details von DosistypII erfolgreich aktualisiert';
					$message = 'Details of DosistypII has been updated';
					$success = true;
					$data['redirectURL'] = base_url('admin/dosistypII');
				} else {
					//$message = 'Etwas ist schief gelaufen! Bitte versuche es erneut...';
					$message = 'Something went wrong! Try Again...';
					$success = false;
				}
			} else {
				$message = validation_errors();
				$success = false;
			}

			$data['success'] = $success;
			$data['message'] = $message;
			json_output($data);
		}

		$this->load->view('admin/includes/header', $output);
		$this->load->view('admin/dosistypII/form');
		$this->load->view('admin/includes/footer');		
	}

	function change_status() {

		$id = $this->input->get('id', true);
		$status = $this->input->get('status', true);

		$this->dosistyp2->changeStatusById($id, $status);

		$data['success'] = true;
		$data['message'] = 'Status of DosisTypII has been changed';
		json_output($data);
	}

    function multi_task_operation() {

        $task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $user_ids = explode(',', $ids);

        foreach ($user_ids as $key => $value) {
            
            if ($task == 'Active' || $task == 'Inactive') {
                $this->dosistyp2->changeStatusById($value, $task);
                $message = 'Status of selected ICD Codes has been changed';
            } else if ($task == 'Delete') {
                $this->icd_codes->deleteDosisTypIIById($value);
                $message = 'Selected ICD Codes has been deleted';
            }
        }

        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }

    function view($id) {

    	$output['page_title'] = 'View DosisTypII';
    	$output['left_menu'] = 'dosistyp_management';
    	$output['left_submenu'] = 'dosistyp_view';

    	$record = $this->dosistyp2->getDosisTypIIById($id);
    	checkRequestedDataExists($record);
    	$output['records'] = $record;

    	$this->load->view('admin/includes/header', $output);
    	$this->load->view('admin/dosistypII/view');
    	$this->load->view('admin/includes/footer');
    }
}
