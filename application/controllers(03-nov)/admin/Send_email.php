<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Send_email class.
 * 
 * @extends CI_Controller
 */

class Send_email extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */	

	function __construct() {
		parent::__construct();
		checkAdminLogin();
		checkLoginAdminStatus();
		$this->load->model('admin/Send_email_model', 'proemail');
		$this->load->model('mailsending_model', 'mailsend');
		$this->config->set_item('language', 'english');
	}

    /**
     * index function.
     * 
     * @access public
     * @return null
     */

	function index() {

		$output['page_title'] = 'Send Email';
		$output['left_menu'] = 'promotion';
		$output['left_submenu'] = 'send_email_list';
		$all_data = $this->proemail->get_all_data();
		//print_r($all_data);die;
		$output['email_templates'] = $all_data;

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/send_email/email_list');
		$this->load->view('admin/includes/footer');
	}

    /**
     * add function.
     * 
     * @access public
     * @return null
     */

	function add() {
		$list = $this->proemail->get_user();
		//print_r($list);die;
		//$data = array();
		$output['get_data']=$list;
		$output['page_title'] = 'Send Email';
		$output['left_menu'] = 'promotion';
		$output['left_submenu'] = 'send_email_list';
		$output['message'] = '';
		$output['id'] = '';
		$output['subject'] = '';
		$output['content'] = '';
		$output['user_id']='';
		$output['add_date'] = '';
		
		//$output['data']=$send;
		
		if ($this->input->post()) {
			$this->form_validation->set_rules('user_id[]','User id', 'trim|required');
			$this->form_validation->set_rules('subject', 'subject', 'trim|required');
			$this->form_validation->set_rules('message', 'message', 'trim|required');

			if ($this->form_validation->run()) {
					$insert_arr = array();
					$user_id = $this->input->post('user_id');
					$value = implode(" ,",$user_id);
					$insert_arr['user_id']=$value;
					$subject= $this->input->post('subject');
					$insert_arr['subject']=$subject;
					$message = $this->input->post('message');
					$insert_arr['message']=$message;
					$insert_arr['add_date'] = getDefaultToGMTDate(time());
					$insert_id = $this->proemail->insert_email($insert_arr);
			  		//print_r($insert_id); die;

					if($insert_id)
					//print_r($insert_id);die;
					{    
						$data = $this->proemail->get_email($value);
						//print_r($data);die; 

						foreach($user_id as $id) 
						{
                              $data = $this->proemail->getUserDetails($id);

                              if($data) {
	                              $user_first_name = $data->first_name;
	                              $user_last_name = $data->last_name;
	                              $user_email = $data->email;
	                              //print_r($user_email);die;

								  $send = $this->mailsend->sendPromotionTypeMail($user_first_name, $user_last_name, $user_email, $subject, $message);
	                              //pr($send); die;
                              }
							}
				
						$success = true;
						$message = 'Send email added successfully';
						$data->redirectURL = base_url('admin/Send_email');
					} else {
						$message = 'Technical error. Please try again later';
						$success = false;
					}
						
			} else {
				$success = false;
				$message = validation_errors();
			}
			$data->message = $message;
			$data->success = $success;
			json_output($data);
		}
		$constants = array();
		$constantsA = 'Website_URL,Logo,Unsubscribe_Newsletter_Link';
		$constantsB = explode(',',$constantsA);
		foreach ($constantsB as $key => $value) {
			$constants[$value] = '{{'.$value.'}}';
		}
		$output['constants'] = $constants;
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/send_email/email_form');
		$this->load->view('admin/includes/footer');
	}

    /**
     * view function.
     * 
     * @access public
     * @param mixed $id
     * @return null
     */

	function view($id){
		$output['page_title'] = 'View Send Email';
		$output['left_menu'] = 'promotion';
		$output['left_submenu'] = 'send_email_list';

        $data = $this->proemail->get_user_view($id);
        //print_r($data);die;
        $user_ids = $data->user_id;
        $exploded_ids = explode(',',$user_ids);

        foreach($exploded_ids as $userid) {
         	$userdata[] = $this->proemail->getUserDetails($userid);
        } 

       $output['email_details'] = $data;
       $output['user_details'] = $userdata;

        //pr($output); die;

        $this->load->view('admin/includes/header',$output);
		$this->load->view('admin/send_email/view_email');
		$this->load->view('admin/includes/footer');
	}

}