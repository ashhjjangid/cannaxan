<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Users class.
 * 
 */
class Faq_category extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/faq_category_model', 'faq_model');
    }

    public function index()
    {
        $output['page_title'] = "FAQ";
        $output['left_menu'] = "FAQ_management";
        $output['left_submenu'] = "faq_category";

        $output['categories'] = $this->faq_model->get_all_categories();

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/faq_category/list');
        $this->load->view('admin/includes/footer');
    }

    public function add()
    {
        $output['page_title'] = 'Add FAQ Category';
        $output['left_menu'] = 'FAQ_management';
        $output['left_submenu'] = 'faq_category';
        $output['task'] = 'add';
        $output['id'] = '';
        $output['category_name'] = '';
        $output['status'] = 'Active';


        if ($this->input->post()) {
            $success = true;
            $message = '';
            /*set validation rules*/
            $this->form_validation->set_rules('category_name', 'Category Name', 'trim|alpha_numeric_spaces|required');            

            if ($this->form_validation->run()) 
            {
                $input = array();

                $category_name = ucfirst($this->input->post('category_name',true));             
                $status = $this->input->post('status', true);

                $input['category_name'] = $category_name;
                $input['status'] = $status;
                
                //pr($input); die;

                $insert_id = $this->faq_model->add_new_category($input);

                if ($insert_id) {
                    $success = true;
                    $message = 'FAQ category added Succesfully';
                    $data['redirectURL'] = base_url('admin/faq_category');
                }else
                {
                    $success = false;
                    $message = "Technical Error! Please Try Again later...";
                }
            }
            else
            {
                $message = validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/faq_category/form');
        $this->load->view('admin/includes/footer');

    }

    public function update($id)
    {
        $records = $this->faq_model->get_member_for_update($id);
        //pr($records); die;
        $output['records'] = $records;

        $output['page_title'] = "Edit FAQ Category";
        $output['left_menu'] = 'FAQ_management';
        $output['left_submenu'] = 'faq_category';
        $output['task'] = "edit";
        $output['id'] = $records->id;
        $output['category_name'] = $records->category_name;
        $output['status'] = $records->status;

        if ($this->input->post())
        {
            $success = true;
            $message = '';
            /*set validation rules*/
            $this->form_validation->set_rules('category_name', 'Category Name', 'trim|required|alpha_numeric_spaces');            

            if ($this->form_validation->run()) 
            {
                $input = array();                

                $category_name = ucfirst($this->input->post('category_name',true));
                
                $status = $this->input->post('status', true);

                $input['category_name'] = $category_name;
                $input['status'] = $status;
                //$input['last_ip'] = $this->input->ip_address();
                //pr($input); die;

                $update_record = $this->faq_model->update_faq_category($id, $input);

                if ($update_record) {
                    $success = true;
                    $message = 'FAQ category Updated Succesfully';
                    $data['redirectURL'] = base_url('admin/faq_category');
                }
            }
            else
            {
                $message = validation_errors();
                $success = false;
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/faq_category/form');
        $this->load->view('admin/includes/footer');
    }

    public function delete()
    {
        $id = $this->input->post('record_id', true);
        $this->faq_model->delete_category($id);
        $data['success'] = true;
        $data['message'] = "FAQ category Deleted Successfully";
        $data['callBackFunction'] = 'callBackCommonDelete';
        $data['redirectURL'] = base_url('admin/team_management');
        json_output($data);
    }

    public function view($id)
    {
        $output['page_title'] = "View FAQ";
        $output['left_menu'] = 'FAQ_management';
        $output['left_submenu'] = 'faq_category';

        $view_record = $this->faq_model->view_faq_category($id);
        $output['categorydata'] = $view_record;

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/faq_category/view');
        $this->load->view('admin/includes/footer');
    }

    public function change_status()
    {
        $id = $this->input->get('id', true);
        $status = $this->input->get('status', true);
        $this->faq_model->change_status_by_id($id, $status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    public function multi_task_operation()
    {
        $task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $data_ids = explode(',',$ids);

        //print_r($ids); die;

        foreach ($data_ids as $key => $value) {
            
            if ($task == 'Delete') {            
            
                $this->faq_model->delete_category($value);
                $message = "Selected rows deleted";
            }
            else if ($task == 'Active' || $task == 'Inactive') {
                $this->faq_model->change_status_by_id($value, $task);
                $message = "Status changed of the selected Rows";
            }

        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }
}

?>