<?php
if(!defined('BASEPATH')) {
  exit('No direct script access allowed');
}

/**
 * Area_selection class.
 * 
 * @extends CI_Controller
 */

class Area_selection extends CI_Controller {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    public function __construct() {
        Parent::__construct();
        checkAdminLogin();
        $this->load->model('admin/area_model', 'area');
    }

    /**
     * index function.
     * 
     * @access public
     * @return website setting record
     */

    public function index() {
        $output['page_title'] = "Area Selection";
        $output['left_menu'] = 'area_selection';
        $output['left_submenu'] = 'add_area';
        $output['message'] = '';        
        
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/map_area/map');
        $this->load->view('admin/includes/footer');
    }   

    public function save_area() 
    {
//pr($this->input->post());

        if($this->input->post())   
        {
            $input = array();
                
                $area_name = $this->input->post('markerarea');
                $polygonlat = $this->input->post('polygonlat');
                $polygonlong = $this->input->post('polygonlong');
                $markerlat = $this->input->post('markerlat');
                $markerlong = $this->input->post('markerlong');

                $count = count($polygonlat);


                $input['area_name'] = $area_name;                     
                for ($i=0; $i < $count; $i++) { 

                    if ($polygonlat[$i] && $polygonlong[$i]) 
                    {
                        $polylat = implode(',', $polygonlat);   
                        $input['latitude'] = $polylat;
                        $polylong = implode(',', $polygonlong);
                        $input['longitude'] = $polylong;
                    }
                }            
                    $insert_id = $this->area->add_polygon_data($input);
                    if ($insert_id) {                            

                        $insert['area_id'] = $insert_id;
                        $count1 = count($markerlat);

                        for ($j=0; $j < $count1; $j++) {

                            if ($markerlat[$j] && $markerlong[$j]) 
                            {
                                $insert['marker_lat'] = $markerlat[$j];
                                $insert['marker_long'] = $markerlong[$j];
                    //pr($input); die;
                                $insert_id = $this->area->add_marker_data($insert);

                                if($insert_id) {
                                    $message = "Area Added Successfully.";
                                    $status = true;
                                } else {
                                    $message = "Technical Error. Please try again.";
                                    $status = false;
                                }
                            }
                        } 
                    } else {
                        $message = "Technical Error. Area cannot added Please again later.";
                        $status = false;
                    }
            $data['message'] = $message;
            $data['status'] = $status;
            json_output($data);
        }
    }
}
