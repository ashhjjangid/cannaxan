<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Registration class.
 * 
 * @extends CI_Controller
 */

class Registration extends CI_Controller
{
   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */
   
    public function __construct()
    {
        Parent::__construct();
        $this->load->model('admin/user_model', 'user');
        $this->load->model('mailsending_model', 'mailsend');
        $this->load->helper('string_helper');

    }

    /**
     * login function.
     * 
     * @access public
     * @return void
     */

    public function login()
    {
        if (is_logged_in()) {
            redirect('admin');
        }

        if (isset($_POST)) {
            $success = true;
            $failure = false;
            $message = false;
            
            /* set validation */
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run()) {
                $password = $this->input->post('password',true);
                $ency_password = encrpty_password($password);
                $user = array(
                    'email' => $this->input->post('email',true),
                    'password' => $ency_password,
                );
                $valid_user = $this->user->check_valid_user($user);

                if ($valid_user) {
                    $user = $this->user->getUserDetails($valid_user->id);
                    $this->session->set_userdata(array('admin_id' => $user->id, 'admin_name' => $user->username, 'admin_last_name' => $user->last_name));
                    $message = 'You are successfully login.';

                } else {
                    $message = 'Incorrect login credentials';
                    $failure = true;
                }

            } else {
                $message = validation_errors();
                $failure = true;
            }

            if ($failure) {
                $data['success'] = false;

            } else {
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['redirectURL'] = site_url('admin');
            }

            $data['message'] = $message;

            if ($this->input->is_ajax_request()) {
                
                json_output($data);
            }
        }
        $this->load->view('admin/register/login');
    }

    /**
     * logout function.
     * 
     * @access public
     * @return void
     */

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('admin/login');
    }

    /**
     * forgotPassword function.
     * 
     * @access public
     * @return void
     */

    public function forgotPassword()
    {
        if (is_logged_in()) {
            redirect('admin');
        }

        $output['message'] = '';

        if (isset($_POST) && !empty($_POST)) {
            $this->form_validation->set_rules('email_id', 'Email address', 'trim|required|valid_email');

            if ($this->form_validation->run()) {
                $emailId = $this->input->post('email_id',true);
                $user = $this->user->getUserByEmailId($emailId);

                if ($user) {
                    $userid = $user->id;
                    $forgotPasswordKey = get_random_key(30);
                    $expire_time =  getExpireTime();

                    $resetURL = '<a href="' . site_url('admin/reset-password/' . $forgotPasswordKey) . '">Reset Password</a>';
                    $input = array();
                    $input['auth_key'] = $forgotPasswordKey;
                    $input['user_id'] = $userid;
                    $input['key_type'] = 'Forget';
                    $input['used'] = 'No';
                    $input['expire_time'] = getDefaultToGMTDate($expire_time);

                    $this->user->setPasswordKey($userid, $input);
                    $this->mailsend->SetForgetPasswordMail($userid, $forgotPasswordKey, $resetURL);
                    $message = 'Please check email and reset password.';
                    $output['redirectURL'] = base_url('admin/login');
                    $success = true;
                    $output['resetForm'] = true;
                } else {
                    $message = 'Email address incorrect.';
                    $success = false;
                }
            } else {
                $success = false;
                $message = validation_errors();
            }
            $output['message'] = $message;
            $output['success'] = $success;
            json_output($output);
        }
        $this->load->view('admin/register/forgot_password');
    }

    /**
     * resetPassword function.
     * 
     * @access public
     * @param mixed $key
     * @return void
     */

    public function resetPassword($key = '')
    {
        $forgotPasswordKey = $key;
        $user = $this->user->getPasswordKey($forgotPasswordKey);


        if ($user) {
            //$emailAddress = $user->email;
            $id = $user->user_id;
            $current_time = getDefaultToGMTDate(time());
            $checkValidURL = $this->user->getPasswordExpireTime($id, $current_time, $forgotPasswordKey);
            

            if ($checkValidURL) {

                if (isset($_POST) && !empty($_POST)) {
                    $this->form_validation->set_rules('new_password', 'New password', 'required|min_length[6]|max_length[50]');
                    $this->form_validation->set_rules('confirm_password', 'Re- enter password', 'required|matches[new_password]');

                    if ($this->form_validation->run()) {
                        $password = $this->input->post('new_password',true);
                        $ency_password = get_encrypted_password($password);
                        $responseData = $this->user->setUserPassword($id,$ency_password);
                       // pr($responseData);die;

                        if($responseData) {
                            $this->user->setExpireUserKey($forgotPasswordKey);
                            $message = 'Password successfully updated.';
                            $this->session->set_flashdata('success_message', $message);
                            $success = true;
                            $output['redirectURL'] = site_url('admin/login');
                        } else {
                            $success = false;
                            $message = 'Technical error! please try again';                            
                        }
                    } else {
                        $success = false;
                        $message = validation_errors();
                    }
                    $output['message'] = $message;
                    $output['success'] = $success;
                    json_output($output);
                }
                $this->load->view('admin/register/reset_password');
            } else {
                $output['message'] = 'Your reset password link is expired.';
                $this->load->view('admin/register/errors', $output);
            }
        } else {
            $output['message'] = 'Your reset password link is expired.';
            $this->load->view('admin/register/errors', $output);
        }
    }

}
