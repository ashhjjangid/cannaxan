<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Nebenwirkung extends CI_Controller {


    function __construct() {

        Parent::__construct();
        checkAdminLogin();
        checkLoginAdminStatus();
        $this->load->model('admin/Nebenwirkung_model','nebenwirkung');
        
    }

    public function index() 
    {
        $output['page_title'] = 'Nebenwirkung';
        $output['left_menu'] = 'nebenwirkung_management';
        $output['left_submenu'] = 'nebenwirkung_list';

        $record = $this->nebenwirkung->getnebenwirkung();
        checkRequestedDataExists($record);
        $output['record'] = $record;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/nebenwirkung/list');
        $this->load->view('admin/includes/footer');
    }

   /* public function add() 
    {  

        $output['page_title'] = 'Add nebenwirkung';
        $output['left_menu'] = 'nebenwirkung_management';
        $output['left_submenu'] = 'nebenwirkungt_add';
        $output['message'] = '';
        $output['id'] = '';
        $output['nebenwirkung_name'] = '';
        
        if ($this->input->post()) {
            $message = '';
            $success = true;
            $this->form_validation->set_rules('nebenwirkung_name', 'nebenwirkung_name', 'trim|required');
    
            if ($this->form_validation->run()) {

                 $input = array();
                 $success =  true;
                 $message = '';
                    $input['nebenwirkung_name'] = $this->input->post('nebenwirkung_name', true);
                    $input['datum'] = getDefaultToGMTDate(time());
                    //pr($input); die;
                    $insert_id = $this->nebenwirkung->add_nebenwirkung($input);



                    if ($insert_id) {

                        $success = true;
                        $message = 'Nebenwirkung added successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = base_url('admin/nebenwirkung');

                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
            
            } else {
                $success = false;
                $message = validation_errors();        
            }    
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/nebenwirkung/form');
        $this->load->view('admin/includes/footer');
    }*/

    public function update($id) 
    {   

        $record = $this->nebenwirkung->getnebenwirkungById($id);
        checkRequestedDataExists($record);

        $output['page_title'] = 'Update nebenwirkung';
        $output['left_menu'] = 'nebenwirkung_management';
        $output['left_submenu'] = 'nebenwirkung_update';
        $output['message'] = '';
        $output['id'] = $record->id;
        $output['record'] = $record;
        $output['nebenwirkung_name'] = $record->nebenwirkung_name;
        
        if ($this->input->post()) {
            $message = '';
            $success = true;
            $this->form_validation->set_rules('nebenwirkung_name', 'nebenwirkung_name', 'trim|required');
   
            if ($this->form_validation->run()) {

                 $input = array();
                 $success =  true;
                 $message = '';
                    $input['nebenwirkung_name'] = $this->input->post('nebenwirkung_name', true);
                    $input['update_date'] = getDefaultToGMTDate(time());
                    //pr($input); die;
                    $response = $this->nebenwirkung->setnebenwirkungById($id, $input);
                    if ($response) {

                        $success = true;
                        $message = 'Nebenwirkung updated successfully';
                        $this->session->set_flashdata('message', $message);
                        $data['redirectURL'] = site_url('admin/nebenwirkung');

                    } else {
                        $success = false;
                        $message = 'Technical error. Please try again later.';
                    }
            
            } else {
                $success = false;
                $message = validation_errors();        
            }    
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/nebenwirkung/form');
        $this->load->view('admin/includes/footer');
    }

      public function view($id) {
        $output['page_title'] = 'View details';
        $output['left_menu'] = 'nebenwirkung_management';
        $output['left_submenu'] = 'nebenwirkung_view';
        /* get record by id */
        $record = $this->nebenwirkung->getnebenwirkungById($id);
        //pr($record); die;
        checkRequestedDataExists($record);
        $output['record'] = $record; 

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/nebenwirkung/view');
        $this->load->view('admin/includes/footer');
    }    

    public function changeStatus()    {
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
        $this->nebenwirkung->changeStatusById($id,$status);

        $data['success'] = true;
        $data['message'] = 'Record Updated Successfully';
        json_output($data);
    }

    /*public function delete() {
        $id = $this->input->post('record_id',true);
        $this->deals->getUsersById($id);
        $data['success'] = true;
        $data['message'] = 'Users has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }*/

    public function multiTaskOperation() {
        $task = $this->input->post('task',true);
        $ids = $this->input->post('ids',true);
        $dataIds = explode(',',$ids);
        
        foreach ($dataIds as $key => $value) {
            
            if ($task == 'Delete') {
                $this->nebenwirkung->deleteUsersById($value);
                $message = 'Users has been deleted successfully.';
            
            } else  if ($task=='Active' || $task=='Inactive') {
                $this->nebenwirkung->changeStatusById($value,$task);            
                $message = 'Status of selected users has been changed successfully.';
            }
        }
        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data); 
    }

    public function umstellung($id)
    {
      // $record = $this->nebenwirkung->getumstellung();
       //pr($record);die;

       //$data = $this->nebenwirkung->getPatientfolgeverordData($id);
       //pr($data);die;

       //$data = $this->nebenwirkung->getPatientlaborwerteData($id);
       //pr($data);die; 

        $data = $this->nebenwirkung->getPatientbegleitmedikationData($id);
         pr($data);die;
          $data = $this->nebenwirkung->getPaitentbegleitmedikationDataByPatientId($id);
         pr($data);die;
    }

    
}