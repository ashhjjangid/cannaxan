<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Icd_codes extends CI_Controller
{
    public function __construct()
    {
        Parent::__construct();
        checkAdminLogin();
        $this->load->model('admin/icd_code_model', 'icd_codes');
    }

    function index() {

        $output['page_title'] = 'ICD Code';
        $output['left_menu'] = 'icd_code_management';
        $output['left_sub_menu'] = 'icd_code_list';

        $records = $this->icd_codes->getAllIcdCodes();
        $output['records'] = $records;

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/icd_codes/index');
        $this->load->view('admin/includes/footer');
    }

    function add() {
        $output['page_title'] = 'ICD Code';
        $output['left_menu'] = 'icd_code_management';
        $output['left_submenu'] = 'add_icd_code';
        $output['id'] = '';
        $output['indikation'] = '';
        $output['icd_10'] = '';
        $output['status'] = 'Active';

        if ($this->input->post()) {
            
            $this->form_validation->set_rules('indikation', 'Indikation', 'trim|required');
            $this->form_validation->set_rules('icd_10', 'ICD 10', 'trim|required');

            if ($this->form_validation->run()) {
                
                $input = array();
                $input['indikation'] = $this->input->post('indikation', true);
                $input['icd_10'] = $this->input->post('icd_10', true);
                $input['status'] = $this->input->post('status');
                $input['add_date'] = getDefaultToGMTDate(time());
                //pr($input); die;

                $insert_id = $this->icd_codes->addNewIcdCode($input);

                if ($insert_id) {
                    
                    //$message = 'Neuer Icd-Code erfolgreich hinzugefügt';
                    $message = 'New ICD Code added successfully';
                    $success = true;
                    $data['redirectURL'] = base_url('admin/icd_codes');
                } else {
                    //$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
                    $message = 'SomeThing went wrong! Try Again...';
                    $success = false;
                }
            } else {
                $message = validation_errors();
                $success = false;
            }

            $data['success'] = $success;
            $data['message'] = $message;
            json_output($data);
        }

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/icd_codes/form');
        $this->load->view('admin/includes/footer');
    }

    function update($id) {
        $output['page_title'] = 'ICD Code';
        $output['left_menu'] = 'icd_code_management';
        $output['left_sub_menu'] = 'update_icd_code';

        $record = $this->icd_codes->getIcdCodeById($id);
        $output['id'] = $record->id;
        $output['indikation'] = $record->indikation;
        $output['icd_10'] = $record->icd_10;
        $output['status'] = $record->status;

        if ($this->input->post()) {
            
            $this->form_validation->set_rules('indikation', 'Indikation', 'trim|required');
            $this->form_validation->set_rules('icd_10', 'ICD 10', 'trim|required');

            if ($this->form_validation->run()) {
                
                $input = array();
                $input['indikation'] = $this->input->post('indikation', true);
                $input['icd_10'] = $this->input->post('icd_10', true);
                $input['status'] = $this->input->post('status');
                $input['update_date'] = getDefaultToGMTDate(time());
                //pr($input); die;

                $insert_id = $this->icd_codes->updateIcdCode($id, $input);

                if ($insert_id) {
                    
                    //$message = 'Icd-Code-Details erfolgreich aktualisiert';
                    $message = 'Details of ICD Code has been updated successfully';
                    $success = true;
                    $data['redirectURL'] = base_url('admin/icd_codes');
                } else {
                    //$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
                    $message = 'SomeThing went wrong! Try Again...';
                    $success = false;
                }
            } else {
                $message = validation_errors();
                $success = false;
            }

            $data['success'] = $success;
            $data['message'] = $message;
            json_output($data);
        }

        $this->load->view('admin/includes/header', $output);
        $this->load->view('admin/icd_codes/form');
        $this->load->view('admin/includes/footer');
    }

    function change_status() {

        $id = $this->input->get('id', true);
        $status = $this->input->get('status', true);

        $this->icd_codes->changeStatusById($id, $status);

        $data['message'] = 'Status of ICD Code has been changed';
        $data['success'] = true;
        json_output($data);
    }

    function delete() {
        $id = $this->input->post('record_id',true);
        //pr($id); die;
        $this->icd_codes->deleteIcdCodeById($id);

        $data['success'] = true;
        $data['message'] = 'ICD Code has been deleted successfully';
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }

    function multi_task_operation() {

        $task = $this->input->post('task', true);
        $ids = $this->input->post('ids', true);
        $user_ids = explode(',', $ids);

        foreach ($user_ids as $key => $value) {
            
            if ($task == 'Active' || $task == 'Inactive') {
                $this->icd_codes->changeStatusById($value, $task);
                $message = 'Status of selected ICD Codes has been changed';
            } else if ($task == 'Delete') {
                $this->icd_codes->deleteIcdCodeById($value);
                $message = 'Selected ICD Codes has been deleted';
            }
        }

        $data['ids'] = $ids;
        $data['success'] = true;
        $data['message'] = $message;
        $data['callBackFunction'] = 'callBackCommonDelete';
        json_output($data);
    }
}
