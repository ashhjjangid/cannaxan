<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Credits class.
 * @extends CI_Controller
 */

class Credits extends CI_Controller {

   /**
    * __construct function.
    * @access public
    * @return void
   */
       
    function __construct() {
        Parent::__construct();
        checkAdminLogin();
        $this->load->model('admin/Credits_model', 'credit');
        $this->userId = $this->session->userdata('admin_id');
    }

    /**
     * index function.
     * @access public
     * @return static page list
     */

    function index() { 
        $output['page_title'] = 'Credits';
        $output['left_menu'] = 'Invites';
        $output['left_submenu'] = 'credits_list';
        
        $credits = $this->credit->get_credits_data();
        //print_r($credits); die;
        $output['credits'] = $credits;

        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/credit/list');
        $this->load->view('admin/includes/footer');
    }

    /**
     * update function.
     * @access public
     * @param mixed $id
     * @return null
     */     

    function update($id) {
        $output['page_title'] = 'Update Credits';
        $output['left_menu'] = 'Invites';
        $output['left_submenu'] = 'credits_list';
        $output['message'] = '';

        $credits = $this->credit->get_credits_page_by_id($id);
        //pr($credits);die;
        checkRequestedDataExists($credits);
        
        $output['id'] = $credits->id;
        $output['record'] = $credits;
        $output['recevier_mail'] = $credits->recevier_mail;
        $output['credits'] = $credits->credits;
        
        if ($this->input->post()) {
            $success = true;
            $this->form_validation->set_rules('credits', 'credits', 'trim|required');
            
            if ($this->form_validation->run()) {
                $input = array();
                $input['edited_by'] = $this->userId;
                $input['content'] = $this->input->post('content',true);
                $result = $this->credit->set_static_page_by_id($id,$input);

                if ($result) {
                    $this->session->set_flashdata('message', 'Record updated successfully.');
                    $data['redirectURL'] = site_url('admin/Credits');
                    $message = 'Credits updated successfully.';
                    $success = true;
                
                } else {
                    $message = 'Technical error. Please try again.';
                    $success = false;
                }
            
            } else {
                $success = false;
                $message = validation_errors();
            }
            $data['message'] = $message;
            $data['success'] = $success;
            json_output($data);
        }

        //$output['title'] = $staticPage->title;
        $this->load->view('admin/includes/header',$output);
        $this->load->view('admin/credit/form');
        $this->load->view('admin/includes/footer');
    }    
    
}