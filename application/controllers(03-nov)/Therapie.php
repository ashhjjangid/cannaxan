<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Therapie extends CI_Controller {


    function __construct() {
        Parent::__construct();
        $this->load->model('Patients_model','patients');
        $this->load->model('doctors_model','doctors');
    }

    public function index() 
    {   
    	if(!$this->session->userdata('user_id')) {
    		redirect('doctors/login');
    	}

        $output['header_menu'] ='therapie';
        $output['header_sub_menu'] ='titration';
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
        $output['dosistyp'] = "Dosistyp 1";
        //pr($is_exist_patient);
        if ($is_exist_patient) {

            if (isset($is_exist_patient->hauptindikation) && $is_exist_patient->hauptindikation) {

                $icd_code_zuordnung = $this->patients->icd_code_zuordnung();
                $zuordnung_sps = $this->patients->getZuordnungSps();
                //pr($zuordnung_sps); die;
                $arztlicher_therapieplan = $this->patients->getPatientArztlicherTherapieplan($is_exist_patient->id);
                //pr($arztlicher_therapieplan); die;
                if ($arztlicher_therapieplan) {
                    
                    if ($arztlicher_therapieplan->dosistyp == "dosistyp_1") {
                        
                        $output['dosistyp'] = "Dosistyp 1";
                    } else if($arztlicher_therapieplan->dosistyp == "dosistyp_2") {
                        $output['dosistyp'] = "Dosistyp 2";
                    }
                } else {
                    
                }
                $output['icd_code_zuordnung'] = $icd_code_zuordnung;
                $output['is_exist_patient'] = $is_exist_patient;
                $output['zuordnung_sps'] = $zuordnung_sps;
                $output['insured_number'] = $insured_number;
                $output['arztlicher_therapieplan'] = $arztlicher_therapieplan;

                $this->load->view('front/includes/header', $output);
                $this->load->view('front/therapie/therapieplan_titration');
                $this->load->view('front/includes/footer');

            } else {
                
                redirect('patients/anlegen?in='.$insured_number);
            }
        } else {
            redirect('patients');
        }

    }

    function getDosisTypeTableFirst(){
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
            $arztlicher_therapieplan = $this->patients->getPatientArztlicherTherapieplan($is_exist_patient->id);

            if ($arztlicher_therapieplan && $arztlicher_therapieplan->dosistyp == 'dosistyp_1') {
                $dosistyp_1 = $this->patients->getPatientDosistyp_1($arztlicher_therapieplan->id);    
            } else {
                $dosistyp_1 = $this->patients->getDosistyp_1();    
            }
            
            if ($dosistyp_1) {
                $output['dosistyp'] = $dosistyp_1;
                $html = $this->load->view('front/therapie/dosistyp_1', $output, true);
                $success = true;
                $message = false;
                $data['html'] = $html;
            } else {
                $success = false;
                $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
            }
            
        } else {
            $success = false;
            $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
        }


        $data['success'] = $success;
        $data['message'] = $message;
        echo json_encode($data); die;
    }

    function add_therapie()
    {

        if ($this->input->post()) {
            $insured_number = $this->input->get('in');
            $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

            if ($is_exist_patient) {
                $saveData['patient_id'] = $is_exist_patient->id;
                $saveData['doctor_id'] = $this->session->userdata('user_id');
                $saveData['hauptindikation'] = $this->input->post('hauptindikation');
                $saveData['rezepturarzneimittel'] = $this->input->post('rezepturarzneimittel');
                $saveData['wirkstoff'] = $this->input->post('wirkstoff');
                $saveData['dosistyp'] = $this->input->post('dosistyps');
                $saveData['konzentration_thc_mg_ml'] = str_replace(',', '.', $this->input->post('konzentration_thc_mg_ml'));
                $saveData['total_summe'] = $this->input->post('total_summe');
                $saveData['total_cannaxan_thc'] = $this->input->post('total_cannaxan_thc');
                $saveData['verordnete_anzahl'] = $this->input->post('verordnete_anzahl');
                $saveData['rechnerische_restmenge'] = $this->input->post('rechnerische_restmenge');
                $saveData['add_date'] = getDefaultToGMTDate(time());

                $arztlicher_therapieplan_id = $this->patients->addPatientArztlicherTherapieplan($saveData);

                if ($arztlicher_therapieplan_id) {

                    $dosistyp_tages = $this->input->post('dosistyp');
                    $dosistyp_tages = $dosistyp_tages['dosistyp'];

                    foreach ($dosistyp_tages as $key => $value) {
                        $tagData = array();
                        $tagData['arztlicher_therapieplan_id'] = $arztlicher_therapieplan_id;
                        $tagData['morgens_anzahl_durchgange'] = $value['morgens_anzahl_durchgange'];
                        $tagData['morgens_anzahl_sps'] = $value['morgens_anzahl_sps'];
                        $tagData['mittags_anzahl_durchgange'] = $value['mittags_anzahl_durchgange'];
                        $tagData['mittags_anzahl_sps'] = $value['mittags_anzahl_sps'];
                        $tagData['abends_anzahl_durchgange'] = $value['abends_anzahl_durchgange'];
                        $tagData['abends_anzahl_sps'] = $value['abends_anzahl_sps'];
                        $tagData['nachts_anzahl_durchgange'] = $value['nachts_anzahl_durchgange'];
                        $tagData['nachts_anzahl_sps'] = $value['nachts_anzahl_sps'];
                        $tagData['thc_morgens_mg'] = $value['thc_morgens_mg'];
                        $tagData['thc_mittags_mg'] = $value['thc_mittags_mg'];
                        $tagData['thc_abends_mg'] = $value['thc_abends_mg'];
                        $tagData['thc_nachts_mg'] = $value['thc_nachts_mg'];
                        $tagData['summe_thc_mg'] = $value['summe_thc_mg'];
                        $tagData['add_date'] = getDefaultToGMTDate(time());

                        if ($this->input->post('dosistyps') == 'dosistyp_1') {
                            $this->patients->addPatientDosistyp_1($tagData);
                        } else {
                            $this->patients->addPatientDosistyp_2($tagData);
                        }
                    }

                    $patientData['last_therapie_access'] = 'Titration';
                    $this->patients->update_patient($is_exist_patient->id, $patientData);

                    $success = true;
                    $message = 'Die Änderungen wurden gespeichert.';
                    $data['redirectURL'] = base_url('therapie/rezepturarzneimittel_titration?in='.$insured_number);

                } else {
                    $success = false;
                    $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
                }


            } else {
                $data['redirectURL'] = base_url('patients');
                $success = true;
                $message = false;
            }

            $data['success'] = $success;
            $data['message'] = $message;
            echo json_encode($data); die;
        }   
    }

    function getDosisTypeTableSecond(){

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
            $arztlicher_therapieplan = $this->patients->getPatientArztlicherTherapieplan($is_exist_patient->id);

            if ($arztlicher_therapieplan && $arztlicher_therapieplan->dosistyp == 'dosistyp_2') {
                $dosistyp_2 = $this->patients->getPatientDosistyp_2($arztlicher_therapieplan->id);    
            } else {
                $dosistyp_2 = $this->patients->getDosistyp_2();    
            }
            
            if ($dosistyp_2) {
                $output['dosistyp'] = $dosistyp_2;
                $html = $this->load->view('front/therapie/dosistyp_2', $output, true);
                $success = true;
                $message = false;
                $data['html'] = $html;
            } else {
                $success = false;
                $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
            }
            
        } else {
            $success = false;
            $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
        }

        /*$dosistyp_2 = $this->patients->getDosistyp_2();

        if ($dosistyp_2) {
            $output['dosistyp'] = $dosistyp_2;
            $html = $this->load->view('front/therapie/dosistyp_2', $output, true);
            $success = true;
            $message = false;
            $data['html'] = $html;
        } else {
            $success = false;
            $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
        }*/

        $data['success'] = $success;
        $data['message'] = $message;
        echo json_encode($data); die;
    }

    function getTypUmstellungTable(){
        
    }


    public function umstellung() 
    {    
        $output['header_menu'] ='therapie';
        $output['header_sub_menu'] ='umstellung';
        $output['name_cannabis_text'] = '';
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {

            if (isset($is_exist_patient->hauptindikation) && $is_exist_patient->hauptindikation) {

                $zuordnung_sps = $this->patients->getZuordnungSps();
                $cannabis_medikament = $this->patients->getCannabisMedikament();
                $patientTherapyPlanUmstellung = $this->patients->getPatientTherapyPlanUmstellung($is_exist_patient->id);
                //pr($is_exist_patient);
                //pr($patientTherapyPlanUmstellung); die;
                if (isset($is_exist_patient) && $is_exist_patient->bestehendes == 'Yes') {
                    
                    foreach ($cannabis_medikament as $key => $value) {
                            
                        if ($is_exist_patient->name_cannabis == $value->id) {
                            $output['name_cannabis_text'] = $value->name;
                            $output['cannabis_medikament_id'] = $value->id;
                        }
                    }

                    $dosiseinheit = ($is_exist_patient)?$is_exist_patient->dosiseinheit_first:'';
                    $tagesdosis_bisheriges = ($is_exist_patient)?$is_exist_patient->tagesdosis_first:'';
                    $tagesdosis = ($is_exist_patient)?$is_exist_patient->tagesdosis_mg_first:'';
                } else {

                    $output['name_cannabis_text'] = '';
                    $output['cannabis_medikament_id'] = '';

                    $dosiseinheit = '';
                    $tagesdosis_bisheriges = '';
                    $tagesdosis = '';
                }
                $output['dosiseinheit'] = $dosiseinheit;
                $output['tagesdosis_bisheriges'] = $tagesdosis_bisheriges;
                $output['tagesdosis'] = $tagesdosis;
                
                $output['cannabis_medikament'] = $cannabis_medikament;
                $output['is_exist_patient'] = $is_exist_patient;
                $output['zuordnung_sps'] = $zuordnung_sps;
                $output['insured_number'] = $insured_number;
                $output['patientTherapyPlanUmstellung'] = $patientTherapyPlanUmstellung;
                $this->load->view('front/includes/header', $output);
                $this->load->view('front/therapie/umstellung');
                $this->load->view('front/includes/footer');

            } else {
                redirect('patients/anlegen?in='.$insured_number);
            }
        } else {
            redirect('patients');
        }
    }

    public function typumstelung()
    {

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
            $id = $this->input->post('id');
            $tagesdosis = str_replace(',', '.', $this->input->post('tagesdosis'));
            $output = array(); 
            $output['id'] = $id;

            $patientTherapyPlanUmstellung = $this->patients->getPatientTherapyPlanUmstellung($is_exist_patient->id);

            if ($id == 1) { // Sativex
                
                if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 1) {

                    if ($patientTherapyPlanUmstellung->tagesdosis == $tagesdosis) {

                        $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                    } else {
                        $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                    }
                } else {
                    $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                }

                $html = $this->load->view('front/therapie/aquivalente_sativex_dosis_mg_tag', $output, true);

            } else if ($id == 2) { // Dronabinol (0,83mg Dronabinol/Tropfen)

                if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 2) {
                    
                    if ($patientTherapyPlanUmstellung->tagesdosis == $tagesdosis) {

                        $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                    } else {
                        $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                    }
                    // pr($output);die;
                } else {
                    $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                }

                $html = $this->load->view('front/therapie/aquivalente_dronabinol_dosis_mg_tag', $output, true);

            } else if ($id == 3) { // Blüten THC hoch (z.B. Bedrocan, Pedanios 22/1 etc

                if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 3) {
                    
                    if ($patientTherapyPlanUmstellung->tagesdosis == $tagesdosis) {

                        $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                    } else {
                        $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                    }
                } else {
                    $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                }

               // pr($output['typUmstellung']); die;

                $html = $this->load->view('front/therapie/aquivalenter_blutendosis_g_tag', $output, true);

            } else if ($id == 4) { // Tilray 25 Extrakt

                if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 4) {
                    if ($patientTherapyPlanUmstellung->tagesdosis == $tagesdosis) {

                        $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                    } else {
                        $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                    }
                } else {
                    $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                }

                $html = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
            } else if ($id == 5) { // Tilray 10/10 Extrakt

                if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 5) {
                    if ($patientTherapyPlanUmstellung->tagesdosis == $tagesdosis) {

                        $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                    } else {
                        $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                    }
                } else {
                    
                    $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                }

                $html = $this->load->view('front/therapie/aquivalente_thc_extrakt_10_by_10_dosis_mg_tag', $output, true);
            } else if ($id == 6) { // Tilray 10/10 Extrakt

                if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 6) {
                    if ($patientTherapyPlanUmstellung->tagesdosis == $tagesdosis) {

                        $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                    } else {
                        $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                    }
                } else {
                    
                    $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                }

                $html = $this->load->view('front/therapie/aquivalente_thc_5_by_20_extrakt_dosis_mg_tag', $output, true);
            } else if ($id == 7) { // Tilray 10/10 Extrakt

                if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 7) {
                    if ($patientTherapyPlanUmstellung->tagesdosis == $tagesdosis) {

                        $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                    } else {
                        $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                    }
                } else {
                    
                    $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                }

                $html = $this->load->view('front/therapie/vertanical_thc_50_extrakt_dosis_mg_tag', $output, true);
            } else if ($id == 8) { // Tilray 10/10 Extrakt

                if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 8) {
                    if ($patientTherapyPlanUmstellung->tagesdosis == $tagesdosis) {

                        $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                    } else {
                        $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                    }
                } else {
                    
                    $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                }

                $html = $this->load->view('front/therapie/vertanical_cbd_thc_extrakt_dosis_mg_tag', $output, true);
            } else if ($id == 9) { // Tilray 10/10 Extrakt

                if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 9) {
                    if ($patientTherapyPlanUmstellung->tagesdosis == $tagesdosis) {

                        $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                    } else {
                        $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                    }
                } else {
                    
                    $output['typUmstellung'] = $this->patients->getMainTypUmstellung($id, $tagesdosis);
                }

                $html = $this->load->view('front/therapie/pedanios_5_by_1_thc_extrakt_dosis_mg_tag', $output, true);
            } 

            $data['html'] = $html;
            $success = true;
            $message = false;
        } else {
            $success = false;
            $message = false;
        }

        $data['success'] = $success;
        $data['message'] = $message;
        echo json_encode($data); die;
    }

    public function add_typumstelung()
    {

        if ($this->input->post()) {


            $insured_number = $this->input->get('in');
            $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

            if ($is_exist_patient) {
                $this->form_validation->set_rules('tagesdosis_bisheriges', 'Tagesdosis bisheriges', 'trim|required', array('required' => 'Tagesdosis bisheriges ist erforderlich'));
                $this->form_validation->set_rules('tagesdosis', 'Tagesdosis (MG THC)', 'trim|required', array('required' => 'Tagesdosis (MG THC) ist erforderlich'));
                $this->form_validation->set_rules('anzahl_therapietage', 'Anzahl therapietage', 'trim|required|less_than_equal_to[30]', array('required' => 'Anzahl therapietage ist erforderlich',
                    'less_than_equal_to' => 'Anzahl therapietage sollte gleich 30 oder weniger als 30 sein'));

                if ($this->form_validation->run()) {
                    $cannabis_medikament_id = $this->input->post('cannabis_medikament_id');
                    $dosiseinheit = $this->input->post('dosiseinheit');
                    $tagesdosis_bisheriges = str_replace(',', '.', $this->input->post('tagesdosis_bisheriges'));
                    $tagesdosis = str_replace(',', '.', $this->input->post('tagesdosis'));
                    $rezepturarzneimittel = $this->input->post('rezepturarzneimittel');
                    $wirkstoff = $this->input->post('wirkstoff');
                    $anzahl_therapietage = $this->input->post('anzahl_therapietage');
                    $typumstelung = $this->input->post('typumstelung');

                    $saveData['cannabis_medikament_id'] = $cannabis_medikament_id;
                    $saveData['dosiseinheit'] = $dosiseinheit;
                    $saveData['tagesdosis_bisheriges'] = $tagesdosis_bisheriges;
                    $saveData['tagesdosis'] = $tagesdosis;
                    $saveData['rezepturarzneimittel'] = $rezepturarzneimittel;
                    $saveData['wirkstoff'] = $wirkstoff;
                    $saveData['anzahl_therapietage'] = $anzahl_therapietage;
                    $saveData['patient_id'] = $is_exist_patient->id;
                    $patient_medicine['bestehendes'] = 'Yes';
                    $patient_medicine['name_cannabis'] = $cannabis_medikament_id;
                    $patient_medicine['dosiseinheit_first'] = $dosiseinheit;
                    $patient_medicine['tagesdosis_first'] = $tagesdosis_bisheriges;
                    $patient_medicine['tagesdosis_mg_first'] = $tagesdosis;

                    /*$is_exist = $this->patients->getPatientTherapyPlanUmstellung($is_exist_patient->id);

                    if ($is_exist) {
                        $saveData['update_date'] = getDefaultToGMTDate(time());
                        $this->patients->updatePatientTherapyPlanUmstellung($is_exist->id, $saveData);
                        $save_id = $is_exist->id;
                    } else {
                        $saveData['add_date'] = getDefaultToGMTDate(time());
                        $save_id = $this->patients->addPatientTherapyPlanUmstellung($saveData);
                    }*/

                    $this->patients->update_patient($is_exist_patient->id, $patient_medicine);
                    $saveData['add_date'] = getDefaultToGMTDate(time());
                    $save_id = $this->patients->addPatientTherapyPlanUmstellung($saveData);

                    if ($typumstelung) {

                        //$this->patients->deletePatientTypUmstellung($save_id);

                        foreach ($typumstelung as $key => $value) {
                            $tagData = array();
                            $tagData['patient_therapy_plan_umstellung_id'] = $save_id;
                            $tagData['tag_name'] = $value['tag_name'];
                            
                            if (isset($value['tag_description'])) { 
                                $tagData['tag_description'] = $value['tag_description'];
                            }

                            $tagData['morgens_anzahl_durchgange'] = str_replace(',', '.', $value['morgens_anzahl_durchgange']);
                            $tagData['morgens_anzahl_sps'] = str_replace(',', '.', $value['morgens_anzahl_sps']);
                            $tagData['mittags_anzahl_durchgange'] = str_replace(',', '.', $value['mittags_anzahl_durchgange']);
                            $tagData['mittags_anzahl_sps'] = str_replace(',', '.', $value['mittags_anzahl_sps']);
                            $tagData['abends_anzahl_durchgange'] = str_replace(',', '.', $value['abends_anzahl_durchgange']);
                            $tagData['abends_anzahl_sps'] = str_replace(',', '.', $value['abends_anzahl_sps']);
                            $tagData['nachts_anzahl_durchgange'] = str_replace(',', '.', $value['nachts_anzahl_durchgange']);
                            $tagData['nachts_anzahl_sps'] = str_replace(',', '.', $value['nachts_anzahl_sps']);
                            $tagData['transferfaktor'] = str_replace(',', '.', $value['eintitration']);
                            $tagData['start_tgl_cannaxan_thc_dosis_mg_thc'] = $value['transferfaktor'];
                            $tagData['eintitration'] = 'nein, sofortige Umstellung auf CannaXan-THC möglich. Reduktion der Dosis innerhalb von 5 Wochen Therapie möglich.';
                            $tagData['add_date'] = getDefaultToGMTDate(time());

                            $this->patients->addPatientTypUmstellung($tagData);
                        }
                    }


                    $patientData['last_therapie_access'] = 'Umstellung';
                    $this->patients->update_patient($is_exist_patient->id, $patientData);

                    $success = true;
                    $message = 'Die Änderungen wurden gespeichert.';
                    $data['redirectURL'] = base_url('therapie/rezepturarzneimittel_umstellung?in='.$insured_number);
                } else {
                    $success = false;
                    $message = validation_errors();
                }

            } else {
                $data['redirectURL'] = base_url('patients');
                $success = true;
                $message = false;
            }

            $data['success'] = $success;
            $data['message'] = $message;
            echo json_encode($data); die;
        }

    }

    public function folgeverordnung() 
    {
        $output['header_menu'] ='therapie';
        $output['header_sub_menu'] ='folgeverordnung';
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {

            if (isset($is_exist_patient->hauptindikation) && $is_exist_patient->hauptindikation) {
                $icd_code_zuordnung = $this->patients->icd_code_zuordnung();
                $zuordnung_sps = $this->patients->getZuordnungSps();
                $is_exist = $this->patients->getPatientThherpieplanFolgeverord($is_exist_patient->id);
                //pr($is_exist); die;
                $output['icd_code_zuordnung'] = $icd_code_zuordnung;
                $output['is_exist_patient'] = $is_exist_patient;
                $output['zuordnung_sps'] = $zuordnung_sps;
                $output['insured_number'] = $insured_number;
                $output['folgeverordnung'] = $is_exist;
                $this->load->view('front/includes/header', $output);
                $this->load->view('front/therapie/folgeverordnung');
                $this->load->view('front/includes/footer');
            } else {
                redirect('patients/anlegen?in='.$insured_number);
            }
        } else {
            redirect('patients');
        }
    }

    function getFolgeverordnungCalculation()
    {
        $tagesdosis_thc_mg = trim($this->input->post('tagesdosis_thc_mg'));
        $therapiedauer = trim($this->input->post('therapiedauer'));

        if ($tagesdosis_thc_mg && $therapiedauer) {
            $zuordnung_sps = $this->patients->getZuordnungSps();
            $menge_thc_sps_mg = $zuordnung_sps->menge_thc_sps_mg;
            $tagesdosis_thc_mg = str_replace(',', '.', $tagesdosis_thc_mg);
            $morgens = $tagesdosis_thc_mg * $menge_thc_sps_mg;
            $mittags = $tagesdosis_thc_mg * $menge_thc_sps_mg;
            $abends = $tagesdosis_thc_mg * $menge_thc_sps_mg;
            $nachts = $tagesdosis_thc_mg * $menge_thc_sps_mg;
            $summe = $morgens + $mittags + $abends + $nachts;

            $total_summe = $therapiedauer * $summe;
        } else {
            $total_summe = 0;
        }

        $data['total_summe'] = $total_summe;
        echo json_encode($data); die;
    }
    function add_folgeverordnung()
    {
        if ($this->input->post()) {
            $insured_number = $this->input->get('in');
            $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
            
            if ($is_exist_patient) {
                $saveData['patient_id'] = $is_exist_patient->id;
                $saveData['hauptindikation'] = $this->input->post('hauptindikation');
                $saveData['rezepturarzneimittel'] = $this->input->post('rezepturarzneimittel');
                $saveData['wirkstoff'] = $this->input->post('wirkstoff');
                $saveData['thc_konzentration_mg_ml'] = str_replace(',', '.', $this->input->post('thc_konzentration_mg_ml'));
                //$saveData['tagesdosis_thc_mg'] = str_replace(',', '.', $this->input->post('tagesdosis_thc_mg'));

                if ($this->input->post('gleichbleibende_tagesdosis')) {
                    $saveData['gleichbleibende_tagesdosis'] = 'Yes';
                } else {
                    $saveData['gleichbleibende_tagesdosis'] = 'No';
                }

                $saveData['therapiedauer'] = $this->input->post('therapiedauer');
                $saveData['tagesdosis_cbd'] = $this->input->post('tagesdosis_cbd');
                $saveData['total_summe'] = $this->input->post('total_summe');
                $saveData['total_cannaxan_thc'] = $this->input->post('total_cannaxan_thc');
                $saveData['verordnete_anzahl'] = $this->input->post('verordnete_anzahl');
                $saveData['rechnerische_restmenge'] = $this->input->post('rechnerische_restmenge');

                $saveData['morgens_anzahl_durchgange'] = $this->input->post('morgens_anzahl_durchgange');
                $saveData['morgens_anzahl_sps'] = $this->input->post('morgens_anzahl_sps');
                $saveData['mittags_anzahl_durchgange'] = $this->input->post('mittags_anzahl_durchgange');
                $saveData['mittags_anzahl_sps'] = $this->input->post('mittags_anzahl_sps');
                $saveData['abends_anzahl_durchgange'] = $this->input->post('abends_anzahl_durchgange');
                $saveData['abends_anzahl_sps'] = $this->input->post('abends_anzahl_sps');
                $saveData['nachts_anzahl_durchgange'] = $this->input->post('nachts_anzahl_durchgange');
                $saveData['nachts_anzahl_sps'] = $this->input->post('nachts_anzahl_sps');
                $saveData['transferfaktor'] = $this->input->post('transferfaktor');
                //pr($saveData); die;

                /*$is_exist = $this->patients->getPatientThherpieplanFolgeverord($is_exist_patient->id);

                if ($is_exist) {
                    $saveData['update_date'] = getDefaultToGMTDate(time());
                    $this->patients->updatePatientThherpieplanFolgeverord($is_exist->id, $saveData);
                    $save_id = $is_exist->id;
                } else {
                    $saveData['add_date'] = getDefaultToGMTDate(time());
                    $save_id = $this->patients->addPatientThherpieplanFolgeverord($saveData);
                }*/
                $saveData['add_date'] = getDefaultToGMTDate(time());
                $save_id = $this->patients->addPatientThherpieplanFolgeverord($saveData);
                /*$thherpieplan_folgeverord = $this->input->post('thherpieplan_folgeverord');

                if ($thherpieplan_folgeverord) {

                    //$this->patients->deletePatientThherpieplanFolgeverordOption($save_id);

                    foreach ($thherpieplan_folgeverord as $key => $value) {
                        $tagData = array();
                        $tagData['patient_thherpieplan_folgeverord_id'] = $save_id;
                        $tagData['tag_name'] = $value['tag_name'];
                        $tagData['anzahl_spruhstobe_morgens'] = $value['anzahl_spruhstobe_morgens'];
                        $tagData['anzahl_spruhstobe_mittags'] = $value['anzahl_spruhstobe_mittags'];
                        $tagData['anzahl_spruhstobe_abends'] = $value['anzahl_spruhstobe_abends'];
                        $tagData['anzahl_spruhstobe_nachts'] = $value['anzahl_spruhstobe_nachts'];
                        $tagData['thc_morgens_mg'] = $value['thc_morgens_mg'];
                        $tagData['thc_mittags_mg'] = $value['thc_mittags_mg'];
                        $tagData['thc_abends_mg'] = $value['thc_abends_mg'];
                        $tagData['thc_nachts_mg'] = $value['thc_nachts_mg'];
                        $tagData['summe_thc_mg'] = $value['summe_thc_mg'];

                        $this->patients->addPatientThherpieplanFolgeverordOption($tagData);
                    }
                }*/
                $therapiedauer = $this->input->post('therapiedauer');
                $transferfaktor = $this->input->post('transferfaktor');
                $total_summe = $transferfaktor;
                $thc_value = $transferfaktor / 4;
                // $zuordnung_sps = $this->patients->getZuordnungSps();

                // $menge_thc_sps_mg = $zuordnung_sps->menge_thc_sps_mg;
                // $anzahl_spruhstobe_durchgang = $zuordnung_sps->anzahl_spruhstobe_durchgang;
                // $tagesdosis_thc_mg = str_replace(',', '.', $this->input->post('tagesdosis_thc_mg'));
                // $total_summe = $tagesdosis_thc_mg;

                // $thc_value = $total_summe / 4;
                // $sps_value = round($thc_value / $menge_thc_sps_mg);
                // $ang_value = round($sps_value / $anzahl_spruhstobe_durchgang);

                /*$anzahl_sps_morgens = $tagesdosis_thc_mg * $anzahl_spruhstobe_durchgang;
                $anzahl_sps_mittags = $tagesdosis_thc_mg * $anzahl_spruhstobe_durchgang;
                $anzahl_sps_abends = $tagesdosis_thc_mg * $anzahl_spruhstobe_durchgang;
                $anzahl_sps_nachts = $tagesdosis_thc_mg * $anzahl_spruhstobe_durchgang;

                $morgens = $anzahl_sps_morgens * $menge_thc_sps_mg;
                $mittags = $anzahl_sps_mittags * $menge_thc_sps_mg;
                $abends = $anzahl_sps_abends * $menge_thc_sps_mg;
                $nachts = $anzahl_sps_nachts * $menge_thc_sps_mg;
                $summe = $morgens + $mittags + $abends + $nachts;*/

                if ($therapiedauer > 0) {

                    for ($i=1; $i <= $therapiedauer; $i++) {
                        $tagData = array();
                        $tagData['patient_thherpieplan_folgeverord_id'] = $save_id;
                        $tagData['tag_name'] = $i;
                        $tagData['anzahl_spruhstobe_morgens'] = $this->input->post('morgens_anzahl_durchgange');
                        $tagData['anzahl_sps_morgens'] = $this->input->post('morgens_anzahl_sps');
                        $tagData['anzahl_spruhstobe_mittags'] =  $this->input->post('mittags_anzahl_durchgange');
                        $tagData['anzahl_sps_mittags'] = $this->input->post('mittags_anzahl_sps');
                        $tagData['anzahl_spruhstobe_abends'] =  $this->input->post('abends_anzahl_durchgange');
                        $tagData['anzahl_sps_abends'] = $this->input->post('abends_anzahl_sps');
                        $tagData['anzahl_spruhstobe_nachts'] = $this->input->post('nachts_anzahl_durchgange');
                        $tagData['anzahl_sps_nachts'] = $this->input->post('nachts_anzahl_sps');
                        $tagData['thc_morgens_mg'] = $thc_value;
                        $tagData['thc_mittags_mg'] = $thc_value;
                        $tagData['thc_abends_mg'] = $thc_value;
                        $tagData['thc_nachts_mg'] = $thc_value;
                        $tagData['summe_thc_mg'] = $total_summe;
                        $this->patients->addPatientThherpieplanFolgeverordOption($tagData);
                    }
                    $patientData['last_therapie_access'] = 'Folgeverordnung';
                    $this->patients->update_patient($is_exist_patient->id, $patientData);
                    $success = true;
                    $message = 'Die Änderungen wurden gespeichert.';
                    $data['redirectURL'] = base_url('therapie/rezepturarzneimittel_folgeverordnung?in='. $insured_number);
                    
                } else {
                    $message = 'Therapiedauer für Die konstante Tagesdosis sollte min. 1';
                    $success = false;
                }

            } else {
                $data['redirectURL'] = base_url('patients');
                $success = true;
                $message = false;
            }

            $data['success'] = $success;
            $data['message'] = $message;
            echo json_encode($data); die;
        }
    }
    public function getFolgeverordnung()
    {

        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
        
        if ($is_exist_patient) {
            $output = array();
            $is_exist = $this->patients->getPatientThherpieplanFolgeverord($is_exist_patient->id);

            if ($is_exist) {
                $thherpieplan_folgeverord = $this->patients->getPatientThherpieplanFolgeverordOption($is_exist->id);

                if(!$thherpieplan_folgeverord) {
                    $thherpieplan_folgeverord = $this->patients->getThherpieplanFolgeverord();
                }

            } else {
                $thherpieplan_folgeverord = $this->patients->getThherpieplanFolgeverord();
            }

            if ($thherpieplan_folgeverord) {
                $output['thherpieplan_folgeverord'] = $thherpieplan_folgeverord;
                $html = $this->load->view('front/therapie/folgeverordnung_view', $output, true);
                $success = true;
                $message = false;
                $data['html'] = $html;
            } else {
                $success = false;
                $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
            } 
        } else {
            $success = false;
            $message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
        }

        $data['success'] = $success;
        $data['message'] = $message;
        echo json_encode($data); die;       
    }

    public function rezepturarzneimittel_titration() 
    {
    	if ($this->input->get('in')) {
    		
	    	$insured_number = $this->input->get('in');
	    	$doctor_id = $this->session->userdata('user_id');
	    	$doctor_record = $this->doctors->getDoctorRecordById($doctor_id);
	    	//pr($doctor_record); die;
	    	$is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

	    	if ($is_exist_patient) {
	    		/*Patient data*/
	    		$output['versichertennr'] = $insured_number;
	    		$output['name'] = $is_exist_patient->last_name;
	    		$output['vorname'] = $is_exist_patient->first_name;
	    		$output['gedatum'] = date('d.m.Y',strtotime($is_exist_patient->date_of_birth));
	    		$output['telefone'] = $is_exist_patient->telephone_number;

	    		/*Doctor Data*/
	    		$output['lanr'] = $doctor_record->lanr;
	    		$output['title'] = $doctor_record->title;
	    		$output['first_name'] = $doctor_record->first_name;
                $output['last_name'] = $doctor_record->name;
                $output['strabe'] = $doctor_record->road;
                $output['hausnr'] = $doctor_record->hausnr;
                $output['plzl'] = $doctor_record->plzl;
                $output['ort'] = $doctor_record->ort;
                $output['unterschrift'] = $doctor_record->unterschrift;
	    		
	    		$patients_therapie_plan = $this->patients->getPatientandDoctorTherapieplan($is_exist_patient->id, $doctor_id);

	    		if ($patients_therapie_plan) {
	    			
		    		//pr($patients_therapie_plan); die;
		    		$output['rezepturarzneimittel'] = $patients_therapie_plan->rezepturarzneimittel;
		    		$output['wirkstoff'] = $patients_therapie_plan->wirkstoff;
		    		$output['tag_name'] = '';
		    		$output['sum_thc_mg'] = $patients_therapie_plan->total_summe;
		    		$output['sum_cannaxan_thc'] = $patients_therapie_plan->total_cannaxan_thc;
		    		$output['verordnete_anzahl'] = $patients_therapie_plan->verordnete_anzahl;
		    		$output['rechnerische_restmenge'] = $patients_therapie_plan->rechnerische_restmenge;

		    		$dosistyp = $patients_therapie_plan->dosistyp;
		    		if ($dosistyp == 'dosistyp_1') {
		    			$output['dosistyp'] = 'DosisTyp 1';
		    		} else {
		    			$output['dosistyp'] = 'DosisTyp 2';
		    		}
		    		$output['datum'] = date('d.m.Y', time());
		    		$output['therapietyp'] = '';


		    		if ($patients_therapie_plan && $patients_therapie_plan->dosistyp == 'dosistyp_1') {

		                $dosistyp_1 = $this->patients->getPatientDosistyp_1($patients_therapie_plan->id);    
		            	$output['dosistyp_data'] = $dosistyp_1;
		            } else if ($patients_therapie_plan && $patients_therapie_plan->dosistyp == 'dosistyp_2') {
		                $dosistyp_2 = $this->patients->getPatientDosistyp_2($patients_therapie_plan->id);    
		            	$output['dosistyp_data'] = $dosistyp_2;
		            } else {

		                $dosistyp_2 = $this->patients->getDosistyp_1();    
		                $output['dosistyp_data'] = $dosistyp_2;
		            }
			        $this->load->view('front/includes/header', $output);
			        $this->load->view('front/therapie/rezepturarzneimittel-titration');
			        $this->load->view('front/includes/footer');
	    		} else {
	    			redirect('therapie?in='.$insured_number);
	    		}
	    	}
    	} else {
    		redirect('patients');
    	}
    	
    }

    function generate_pdf() {

    	if ($this->input->get('in')) {
    		
	    	$insured_number = $this->input->get('in');
	    	$doctor_id = $this->session->userdata('user_id');
	    	$doctor_record = $this->doctors->getDoctorRecordById($doctor_id);
	    	//pr($doctor_record); die;
	    	$is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

	    	if ($is_exist_patient) {
	    		/*Patient data*/
	    		$output['versichertennr'] = $insured_number;
	    		$output['name'] = $is_exist_patient->last_name;
	    		$output['vorname'] = $is_exist_patient->first_name;
	    		$output['gedatum'] = date('d.m.Y',strtotime($is_exist_patient->date_of_birth));
	    		$output['telefone'] = $is_exist_patient->telephone_number;

	    		/*Doctor Data*/
	    		$output['lanr'] = $doctor_record->lanr;
	    		$output['title'] = $doctor_record->title;
	    		$output['first_name'] = $doctor_record->first_name;
                $output['last_name'] = $doctor_record->name;
                $output['hausnr'] = $doctor_record->hausnr;
                $output['plzl'] = $doctor_record->plzl;
                $output['ort'] = $doctor_record->ort;
                $output['strabe'] = $doctor_record->road;
                $output['unterschrift'] = $doctor_record->unterschrift;
	    		
	    		$patients_therapie_plan = $this->patients->getPatientandDoctorTherapieplan($is_exist_patient->id, $doctor_id);

	    		if ($patients_therapie_plan) {
	    			
		    		//pr($patients_therapie_plan); die;
		    		$output['rezepturarzneimittel'] = $patients_therapie_plan->rezepturarzneimittel;
		    		$output['wirkstoff'] = $patients_therapie_plan->wirkstoff;
		    		$output['tag_name'] = '';
		    		$output['sum_thc_mg'] = $patients_therapie_plan->total_summe;
		    		$output['sum_cannaxan_thc'] = $patients_therapie_plan->total_cannaxan_thc;
		    		$output['verordnete_anzahl'] = $patients_therapie_plan->verordnete_anzahl;
		    		$output['rechnerische_restmenge'] = $patients_therapie_plan->rechnerische_restmenge;

		    		$dosistyp = $patients_therapie_plan->dosistyp;
		    		if ($dosistyp == 'dosistyp_1') {
		    			$output['dosistyp'] = 'DosisTyp 1';
		    		} else {
		    			$output['dosistyp'] = 'DosisTyp 2';
		    		}
		    		$output['datum'] = date('d.m.Y', time());
		    		$output['therapietyp'] = '';


		    		if ($patients_therapie_plan && $patients_therapie_plan->dosistyp == 'dosistyp_1') {

		                $dosistyp_1 = $this->patients->getPatientDosistyp_1($patients_therapie_plan->id);    
		            	$output['dosistyp_data'] = $dosistyp_1;
		            } else if ($patients_therapie_plan && $patients_therapie_plan->dosistyp == 'dosistyp_2') {

		                $dosistyp_2 = $this->patients->getPatientDosistyp_2($patients_therapie_plan->id);    
		            	$output['dosistyp_data'] = $dosistyp_2;
		            } else {

		                $dosistyp_2 = $this->patients->getDosistyp_2();    
		                $output['dosistyp_data'] = $dosistyp_2;
		            }
			        //$this->load->view('front/therapie/tabel', $output);
    				$header_html = $this->load->view('front/therapie/tabel_header', $output, true);
                    $html = $this->load->view('front/therapie/tabel', $output, true);

                    //$pdf_name = time();
                    $pdf_name = 'Therapieplan_'.$is_exist_patient->first_name.'_'.$is_exist_patient->last_name.'_'.date('dmY');
                    $new_pdf_name = time().".pdf.";
                    file_put_contents(ASSETSPATH. 'uploads/pdf/'.$pdf_name.".html", $html);

                    $pdf_header = time().'-header';
                    file_put_contents(ASSETSPATH.'uploads/pdf/'. $pdf_header.".html", $header_html);
                    //pr(base_url('assets/uploads/pdf/').$pdf_header.'.html'); die;

                    $newpdf_path = ASSETSPATH. 'uploads/pdf/'.$pdf_name.".pdf";
                    exec ('/usr/local/bin/wkhtmltopdf --header-html ' .base_url('assets/uploads/pdf/').$pdf_header.'.html --no-stop-slow-scripts '. base_url('assets/uploads/pdf/').$pdf_name.'.html '.$newpdf_path." 2>&1 ", $response);

                    //echo 'https://xsdemo.com/cannaxan/assets/uploads/pdf/'.$pdf_name.'.html';
                    //echo "<br>";
                    //echo 'https://xsdemo.com/cannaxan/assets/uploads/pdf/'.$pdf_name.'.pdf';

                    $file_url = base_url().'assets/uploads/pdf/'.$pdf_name.'.pdf';
                    header('Content-Type: *');
                    header("Content-Transfer-Encoding: *"); 
                    header("Content-disposition: attachment; filename=\"".$pdf_name."\""); 
                    readfile($file_url);
                    exit;
                    //$this->load->helper('download');
                    //force_download($new_pdf_name, $html);
			        
	    		} else {
	    			redirect('therapie?in='.$insured_number);
	    		}
	    	}
    	} else {
    		redirect('patients');
    	}
    }

    public function rezepturarzneimittel_umstellung() 
    {
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
        $doctor_id = $this->session->userdata('user_id');
        $doctor_record = $this->doctors->getDoctorRecordById($doctor_id);
        
        if ($is_exist_patient) {
            /*Patient data*/
            $output['versichertennr'] = $insured_number;
            $output['name'] = $is_exist_patient->last_name;
            $output['vorname'] = $is_exist_patient->first_name;
            $output['gedatum'] = date('d.m.Y',strtotime($is_exist_patient->date_of_birth));
            $output['telefone'] = $is_exist_patient->telephone_number;

            /*Doctor Data*/
            $output['unterschrift'] = $doctor_record->unterschrift;
            $output['lanr'] = $doctor_record->lanr;
            $output['title'] = $doctor_record->title;
            $output['first_name'] = $doctor_record->first_name;
            $output['last_name'] = $doctor_record->name;
            $output['strabe'] = $doctor_record->road;
            $output['hausnr'] = $doctor_record->hausnr;
            $output['plzl'] = $doctor_record->plzl;
            $output['ort'] = $doctor_record->ort;
            $output['unterschrift'] = $doctor_record->unterschrift;
            $output['dosistyp'] = 'Umstellung';
            $output['datum'] = date('d.m.Y', time());

            $patientTherapyPlanUmstellung = $this->patients->getPatientTherapyPlanUmstellung($is_exist_patient->id);
           // pr($patientTherapyPlanUmstellung); die;

            if ($patientTherapyPlanUmstellung) {
                $output['anzahl_therapietage'] = $patientTherapyPlanUmstellung->anzahl_therapietage;
                $cannabis_medikament = $this->patients->getCannabisMedikament();

                foreach ($cannabis_medikament as $key => $value) {
                    
                    if ($value->id == 1) { // Sativex
                
                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 1) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_sativex_dosis_mg_tag', $output, true);

                    } else if ($value->id == 2) { // Dronabinol (0,83mg Dronabinol/Tropfen)

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 2) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                            // pr($output);die;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_dronabinol_dosis_mg_tag', $output, true);

                    } else if ($value->id == 3) { // Blüten THC hoch (z.B. Bedrocan, Pedanios 22/1 etc

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 3) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalenter_blutendosis_g_tag', $output, true);

                    } else if ($value->id == 4) { // Tilray 25 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 4) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 5) { // Tilray 10/10 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 5) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 6) { // Tilray 5/20 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 6) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 7) { // Vertanical THC 50 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 7) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 8) { // Vertanical THC/CBD Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 8) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 9) { // PEDANIOS 5/1 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 9) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    }

                } 

                
                $output['table'] = $this->patients->arztlicher_therapieplan_table($patientTherapyPlanUmstellung->anzahl_therapietage);
                $zuordnung_sps = $this->patients->getZuordnungSps();
                $output['zuordnung_sps'] = $zuordnung_sps;
            } else {
                redirect('therapie/umstellung?in='.$insured_number);
            }
        } else {
            redirect('patients');
        }
        //pr($output); die;
        $this->load->view('front/includes/header', $output);
        $this->load->view('front/therapie/rezepturarzneimittel-umstellung');
        $this->load->view('front/includes/footer');
    }

    function generate_umstellung_pdf() {

        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }

        $insured_number = $this->input->get('in');
        $output['sum_thc_mg'] = $this->input->get('summethc');
        $output['sum_cannaxan_thc'] = $this->input->get('totalcannthc');
        $output['verordnete_anzahl'] = $this->input->get('verordneteanzahl');
        $output['rechnerische_restmenge'] = $this->input->get('rechnerische');
        /*pr($summethc);
        pr($totalcannthc);
        pr($verordneteanzahl);
        pr($rechnerische);*/
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
        $doctor_id = $this->session->userdata('user_id');
        $doctor_record = $this->doctors->getDoctorRecordById($doctor_id);
        if ($is_exist_patient) {
            /*Patient data*/
            $output['versichertennr'] = $insured_number;
            $output['name'] = $is_exist_patient->last_name;
            $output['vorname'] = $is_exist_patient->first_name;
            $output['gedatum'] = date('d.m.Y',strtotime($is_exist_patient->date_of_birth));
            $output['telefone'] = $is_exist_patient->telephone_number;

            /*Doctor Data*/
            $output['lanr'] = $doctor_record->lanr;
            $output['title'] = $doctor_record->title;
            $output['first_name'] = $doctor_record->first_name;
            $output['last_name'] = $doctor_record->name;
            $output['hausnr'] = $doctor_record->hausnr;
            $output['plzl'] = $doctor_record->plzl;
            $output['ort'] = $doctor_record->ort;
            $output['strabe'] = $doctor_record->road;
            $output['unterschrift'] = $doctor_record->unterschrift;
            $output['dosistyp'] = 'Umstellung';
            $output['datum'] = date('d.m.Y', time());

            $patientTherapyPlanUmstellung = $this->patients->getPatientTherapyPlanUmstellung($is_exist_patient->id);
            //pr($patientTherapyPlanUmstellung); die;

            if ($patientTherapyPlanUmstellung) {

                $output['patientTherapyPlanUmstellung'] = $patientTherapyPlanUmstellung;
                $output['anzahl_therapietage'] = $patientTherapyPlanUmstellung->anzahl_therapietage;
                $cannabis_medikament = $this->patients->getCannabisMedikament();
                $output['cannabis_medikament'] = $cannabis_medikament;

                foreach ($cannabis_medikament as $key => $value) {
                    
                    if ($value->id == 1) { // Sativex
                
                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 1) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_sativex_dosis_mg_tag', $output, true);

                    } else if ($value->id == 2) { // Dronabinol (0,83mg Dronabinol/Tropfen)

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 2) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                            // pr($output);die;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_dronabinol_dosis_mg_tag', $output, true);

                    } else if ($value->id == 3) { // Blüten THC hoch (z.B. Bedrocan, Pedanios 22/1 etc

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 3) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalenter_blutendosis_g_tag', $output, true);

                    } else if ($value->id == 4) { // Tilray 25 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 4) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 5) { // Tilray 10/10 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 5) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 6) { // Tilray 5/20 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 6) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 7) { // Tilray 25 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 7) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 8) { // Tilray 25 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 8) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    } else if ($value->id == 9) { // Tilray 25 Extrakt

                        if ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->cannabis_medikament_id == 9) {
                            $output['cannabis_medikament_id'] = $value->name;
                            $output['dosiseinheit'] = $patientTherapyPlanUmstellung->dosiseinheit;
                            $output['tagesdosis_bisheriges'] = $patientTherapyPlanUmstellung->tagesdosis_bisheriges;
                            $output['tagesdosis'] = $patientTherapyPlanUmstellung->tagesdosis;
                            $output['typUmstellung'] = $this->patients->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
                            $output['wirkstoff'] = $patientTherapyPlanUmstellung->wirkstoff;
                            $output['rezepturarzneimittel'] = $patientTherapyPlanUmstellung->rezepturarzneimittel;
                        }

                        // $output['html'] = $this->load->view('front/therapie/aquivalente_thc_extrakt_dosis_mg_tag', $output, true);
                    }
                    $output['table'] = $this->patients->arztlicher_therapieplan_table($patientTherapyPlanUmstellung->anzahl_therapietage);
                    $zuordnung_sps = $this->patients->getZuordnungSps();
                    $output['zuordnung_sps'] = $zuordnung_sps;

                } 
            } else {
                redirect('therapie/umstellung?in='.$insured_number);
                }
        } else {
            redirect('patients');
        }
        
        //$this->load->view('front/therapie/umstellung-tabel', $output);

        $html = $this->load->view('front/therapie/umstellung-tabel', $output, true);
        $header_html = $this->load->view('front/therapie/tabel_header', $output, true);

        //$pdf_name = time().'-umstellung';
        $pdf_name = 'Umstellung_'.$is_exist_patient->first_name.'_'.$is_exist_patient->last_name.'_'.date('dmY');
        file_put_contents(ASSETSPATH. 'uploads/pdf/'.$pdf_name.".html", $html);

        $pdf_header = time().'-header';
        file_put_contents(ASSETSPATH.'uploads/pdf/'. $pdf_header.".html", $header_html);

        $newpdf_path = ASSETSPATH. 'uploads/pdf/'.$pdf_name.".pdf";
        exec ('/usr/local/bin/wkhtmltopdf --margin-top 20mm --header-html ' .base_url('assets/uploads/pdf/').$pdf_header.'.html --no-stop-slow-scripts '. base_url('assets/uploads/pdf/').$pdf_name.'.html '.$newpdf_path." 2>&1 ", $response);

        $file_url = base_url().'assets/uploads/pdf/'.$pdf_name.'.pdf';
        header('Content-Type: *');
        header("Content-Transfer-Encoding: *"); 
        header("Content-disposition: attachment; filename=\"".$pdf_name."\""); 
        readfile($file_url);
        exit;
    }  

    public function rezepturarzneimittel_folgeverordnung() 
    {
        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }
        $insured_number = $this->input->get('in');
        $doctor_id = $this->session->userdata('user_id');
        $doctor_record = $this->doctors->getDoctorRecordById($doctor_id);

        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
        //pr($is_exist_patient); die;
        if ($is_exist_patient) {
            $output['versichertennr'] = $insured_number;
            $output['name'] = $is_exist_patient->last_name;
            $output['vorname'] = $is_exist_patient->first_name;
            $output['gedatum'] = date('d.m.Y',strtotime($is_exist_patient->date_of_birth));
            $output['telefone'] = $is_exist_patient->telephone_number;

            /*Doctor Data*/
            $output['unterschrift'] = $doctor_record->unterschrift;
            $output['lanr'] = $doctor_record->lanr;
            $output['title'] = $doctor_record->title;
            $output['first_name'] = $doctor_record->first_name;
            $output['last_name'] = $doctor_record->name;
            $output['hausnr'] = $doctor_record->hausnr;
            $output['plzl'] = $doctor_record->plzl;
            $output['ort'] = $doctor_record->ort;
            $output['strabe'] = $doctor_record->road;
            $output['unterschrift'] = $doctor_record->unterschrift;
            $output['dosistyp'] = 'Folgeverordnung';
            $output['datum'] = date('d.m.Y', time());

            $is_exist = $this->patients->getPatientThherpieplanFolgeverord($is_exist_patient->id);
            //pr($is_exist);

            if ($is_exist) {
                $icd_code_zuordnung = $this->patients->icd_code_zuordnung();
                $zuordnung_sps = $this->patients->getZuordnungSps();
                $output['wirkstoff'] = $is_exist->wirkstoff;
                $output['thc_konzentration_mg_ml'] = $is_exist->thc_konzentration_mg_ml;
                $output['tagesdosis_thc_mg'] = $is_exist->tagesdosis_thc_mg;
                $output['rezepturarzneimittel'] = $is_exist->rezepturarzneimittel;
                $gleichbleibende_tagesdosis = $is_exist->gleichbleibende_tagesdosis;
                /*if ($gleichbleibende_tagesdosis == 'Yes') {
                    $output['gleichbleibende_tagesdosis'] = "gleichbleibende Dosis";
                } else {
                    $output['gleichbleibende_tagesdosis'] = '';
                }*/
                $output['tagesdosis_cbd'] = $is_exist->tagesdosis_cbd;
                $output['therapiedauer'] = $is_exist->therapiedauer;
                $output['total_summe'] = $is_exist->total_summe;
                $output['total_cannaxan_thc'] = $is_exist->total_cannaxan_thc;
                $output['verordnete_anzahl'] = $is_exist->verordnete_anzahl;
                $output['rechnerische_restmenge'] = $is_exist->rechnerische_restmenge;

                //pr($zuordnung_sps); die;

                foreach ($icd_code_zuordnung as $key => $value) {
                    
                    if ($value->id == $is_exist_patient->hauptindikation) {
                        
                        $output['hauptindikation'] = $value->indikation;
                        //pr($output['hauptindikation']);
                    }
                }

                $thherpieplan_folgeverord = $this->patients->getPatientThherpieplanFolgeverordOption($is_exist->id);
                //pr($thherpieplan_folgeverord); die;
                $output['thherpieplan_folgeverord'] = $thherpieplan_folgeverord;
                if(!$thherpieplan_folgeverord) {
                    $output['thherpieplan_folgeverord'] = $this->patients->getThherpieplanFolgeverord();
                }

            } else {
                redirect('therapie/folgeverordnung?in='. $insured_number);
            }
            
            $this->load->view('front/includes/header', $output);
            $this->load->view('front/therapie/rezepturarzneimittel-folgeverordnung');
            $this->load->view('front/includes/footer');
        } else {
            redirect('patients');
        }
    }

    function generate_folgeverordnung_pdf() {

        if (!$this->session->userdata('user_id')) {
            redirect('doctors/login');
        }
        $insured_number = $this->input->get('in');
        $doctor_id = $this->session->userdata('user_id');
        $doctor_record = $this->doctors->getDoctorRecordById($doctor_id);

        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

        if ($is_exist_patient) {
            $output['versichertennr'] = $insured_number;
            $output['name'] = $is_exist_patient->last_name;
            $output['vorname'] = $is_exist_patient->first_name;
            $output['gedatum'] = date('d.m.Y',strtotime($is_exist_patient->date_of_birth));
            $output['telefone'] = $is_exist_patient->telephone_number;

            /*Doctor Data*/
            $output['lanr'] = $doctor_record->lanr;
            $output['title'] = $doctor_record->title;
            $output['first_name'] = $doctor_record->first_name;
            $output['last_name'] = $doctor_record->name;
            $output['hausnr'] = $doctor_record->hausnr;
            $output['plzl'] = $doctor_record->plzl;
            $output['ort'] = $doctor_record->ort;
            $output['strabe'] = $doctor_record->road;
            $output['unterschrift'] = $doctor_record->unterschrift;
            $output['dosistyp'] = 'Folgeverordnung';
            $output['datum'] = date('d.m.Y', time());

            $is_exist = $this->patients->getPatientThherpieplanFolgeverord($is_exist_patient->id);
            //pr($is_exist);

            if ($is_exist) {
                $icd_code_zuordnung = $this->patients->icd_code_zuordnung();
                $zuordnung_sps = $this->patients->getZuordnungSps();
                $output['wirkstoff'] = $is_exist->wirkstoff;
                $output['thc_konzentration_mg_ml'] = $is_exist->thc_konzentration_mg_ml;
                $output['tagesdosis_thc_mg'] = $is_exist->tagesdosis_thc_mg;
                $output['rezepturarzneimittel'] = $is_exist->rezepturarzneimittel;
                $output['gleichbleibende_tagesdosis'] = $is_exist->gleichbleibende_tagesdosis;
                $output['tagesdosis_cbd'] = $is_exist->tagesdosis_cbd;
                $output['therapiedauer'] = $is_exist->therapiedauer;
                $output['total_summe'] = $is_exist->total_summe;
                $output['total_cannaxan_thc'] = $is_exist->total_cannaxan_thc;
                $output['verordnete_anzahl'] = $is_exist->verordnete_anzahl;
                $output['rechnerische_restmenge'] = $is_exist->rechnerische_restmenge;

                //pr($zuordnung_sps); die;

                foreach ($icd_code_zuordnung as $key => $value) {
                    
                    if ($value->id == $is_exist_patient->hauptindikation) {
                        
                        $output['hauptindikation'] = $value->indikation;
                        //pr($output['hauptindikation']);
                    }
                }

                $thherpieplan_folgeverord = $this->patients->getPatientThherpieplanFolgeverordOption($is_exist->id);
                //pr($thherpieplan_folgeverord); die;
                $output['thherpieplan_folgeverord'] = $thherpieplan_folgeverord;
                if(!$thherpieplan_folgeverord) {
                    $output['thherpieplan_folgeverord'] = $this->patients->getThherpieplanFolgeverord();
                }

            } else {
                redirect('therapie/folgeverordnung?in='. $insured_number);
            }
            
            
            //$this->load->view('front/therapie/folgeverordnung-tabel', $output);

            $html = $this->load->view('front/therapie/folgeverordnung-tabel', $output, true);
            $header_html = $this->load->view('front/therapie/tabel_header', $output, true);

            $pdf_name = 'Folgeverordnung_'.$is_exist_patient->first_name.'_'.$is_exist_patient->last_name.'_'.date('dmY');
            file_put_contents(ASSETSPATH. 'uploads/pdf/'.$pdf_name.".html", $html);

            $pdf_header = time().'-header';
            file_put_contents(ASSETSPATH.'uploads/pdf/'. $pdf_header.".html", $header_html);

            $newpdf_path = ASSETSPATH. 'uploads/pdf/'.$pdf_name.".pdf";
            exec ('/usr/local/bin/wkhtmltopdf --margin-top 20mm --header-html ' .base_url('assets/uploads/pdf/').$pdf_header.'.html --no-stop-slow-scripts '. base_url('assets/uploads/pdf/').$pdf_name.'.html '.$newpdf_path." 2>&1 ", $response);

            $file_url = base_url().'assets/uploads/pdf/'.$pdf_name.'.pdf';
            header('Content-Type: *');
            header("Content-Transfer-Encoding: *"); 
            header("Content-disposition: attachment; filename=\"".$pdf_name."\""); 
            readfile($file_url);
            exit;
        } else {
            redirect('patients');
        }
    }  
}