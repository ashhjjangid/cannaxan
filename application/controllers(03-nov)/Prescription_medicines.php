<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Prescription_medicines extends CI_Controller {


    function __construct() {

        Parent::__construct();
        
    }


    public function index() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/rezepturarzneimittel-titration');
        $this->load->view('front/includes/footer');
    }

    public function umstellung() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/rezepturarzneimittel-umstellung');
        $this->load->view('front/includes/footer');
    }

    public function folgeverordnung() 
    {
        $this->load->view('front/includes/header');
        $this->load->view('front/pages/rezepturarzneimittel-folgeverordnung');
        $this->load->view('front/includes/footer');
    }

    
}