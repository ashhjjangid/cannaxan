<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nebenwirkung extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('nebenwirkung_model', 'side_effects');
		$this->load->model('patients_model', 'patients');
		$this->load->model('process_control_model', 'process_model');
	}

	function index() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}

		$output = array();
		$output['header_menu'] = 'process_control';
        $output['header_sub_menu'] = 'nebenwirkung';
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
       	
        if ($is_exist_patient) {
	        $id = $is_exist_patient->id;
	        $output['insurance_number'] = $insured_number;          
        	$data_exists = $this->side_effects->getSideEffectsTableDataById($id);
        	$current_date = date('Y-m-d');

        	if ($data_exists) {
        		$c_date = strtotime($current_date);
        		$first_visit = $this->side_effects->getFirstVisit($id);
        		$datediff = $c_date - strtotime($first_visit->datum);
				$behandungstag = round($datediff / (60 * 60 * 24));

        		$s_date = strtotime($data_exists->datum);

        		if ($s_date == $c_date) {
        			$visit_number = $data_exists->visite;
        			$is_current_record = 'Yes';
        		} else {
        			$visit_number = $data_exists->visite + 1;
        			$is_current_record = 'No';
        		}

	        	$output['datum'] = date('d.m.Y', strtotime($current_date));
	        	$output['behandungstag'] = $behandungstag;
	        	$output['visite'] = $visit_number;
	        	$output['arzneimittel'] = $data_exists->arzneimittel;
	        	$output['keine_kausal_bedingte'] = $data_exists->keine_kausal_bedingte;
        		$behandlunggstag_id = $data_exists->id;
        		$table_data_exists = $this->side_effects->getSideEffectsDataByBehandlunggstagId($behandlunggstag_id);
        		$output['table_data_exists'] = $table_data_exists;
        	} else {
        		$output['datum'] = date('d.m.Y', strtotime($current_date));
	        	$output['behandungstag'] = '0';
	        	$output['visite'] = '1';
	        	$output['arzneimittel'] = 'CannaXan-THC';
	        	$output['keine_kausal_bedingte'] = '';
	        	$is_current_record = 'No';
        	}
        	$visits = $this->process_model->getPatientQuestions($is_exist_patient->id);
		    $output['visits'] = $visits;
		    $output['is_current_record'] = $is_current_record;
        } else {
        	redirect('patients');
        }


		$records = $this->side_effects->getAllData();
		//pr($records); die;
		$output['records'] = $records;


		$this->load->view('front/includes/header', $output);
		$this->load->view('front/nebenwirkung/list_page');
		$this->load->view('front/includes/footer');
	}

	function add_nebenwirkung_data() {
		$doctor = $this->session->userdata('user_id');

		$insured_number = $this->input->get('in');
		//pr($insured_number); die;

		
		if ($this->input->post()) {
			
			$this->form_validation->set_rules('datum', 'Datum', 'trim|required');
			$this->form_validation->set_rules('behandungstag', 'Behandungstag', 'trim|required');
			$this->form_validation->set_rules('visite', 'Visite', 'trim|required');
			$this->form_validation->set_rules('arzneimittel', 'Arzneimittel', 'trim|required');
			$this->form_validation->set_rules('keine_kausal_bedingte', 'Keine kausal bedingte Nebenwirkung', 'trim|required');

			if ($this->form_validation->run()) {
				
				$is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);

				if ($is_exist_patient) {
			        $id = $is_exist_patient->id;
			        
			        if ($this->input->post('is_exist') == 'Yes') {
			        	$success = false;
			        	$message = 'Sie haben den Besuch für dieses Datum bereits ausgefüllt.';
			        } else {

				        $input['doctor_id'] = $doctor;
				        $input['patient_id'] = $is_exist_patient->id;
						$input['datum'] = date('Y-m-d', strtotime($this->input->post('datum')));
						$input['behandungstag'] = $this->input->post('behandungstag');
						$input['visite'] = $this->input->post('visite');
						$input['arzneimittel'] = $this->input->post('arzneimittel');
						$input['keine_kausal_bedingte'] = $this->input->post('keine_kausal_bedingte');
						$input['add_date'] = getDefaultToGMTDate(time());

						//$insert_id = true;
						$insert_id = $this->side_effects->addkausalbedingteData($input);
				                  
						if ($insert_id) {
						
							//pr($_POST['data']); die;
							foreach ($_POST['data'] as $key => $value) {
								
								$dataArray['side_effects'][$key]['behandlunggstag_id'] = $insert_id;
								$dataArray['side_effects'][$key]['nebenwirkung_name'] = $value['nebenwirkung_name'];
								$dataArray['side_effects'][$key]['mild_schweregrad'] = $value['mild_schweregrad'];
								$dataArray['side_effects'][$key]['moderat_schweregrad'] = $value['moderat_schweregrad'];
								$dataArray['side_effects'][$key]['schwer_schweregrad'] = $value['schwer_schweregrad'];
								$dataArray['side_effects'][$key]['lebensbedrohlich_schweregrad'] = $value['lebensbedrohlich_schweregrad'];
								$dataArray['side_effects'][$key]['kausalitat_zur_therapie_vorhanden'] = $value['kausalitat_zur_therapie_vorhanden'];
								$dataArray['side_effects'][$key]['add_date'] = getDefaultToGMTDate(time());
							}

							$inserted_data = $this->side_effects->addnebenwirkungtabledata($dataArray['side_effects']);
							if ($inserted_data) {
								
								$success = true;
								$message = 'Nebenwirkungsdaten erfolgreich hinzugefügt';
								$data['redirectURL'] = base_url('nebenwirkung/nebenwirkungen_auswertung?in='. $insured_number);
							} else {
								$success = false;
								$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
							}
						} else {
								$success = false;
								$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
						}
			        }
					
		        } else {

		        	$message = 'Patient nicht gefunden';
		        	$success = false;
		        }
			} else {

				$success = false;
				$message = validation_errors();
			}

		}
		$data['success'] = $success;
        $data['message'] = $message;
        json_output($data);
	}

	function nebenwirkungen_auswertung() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}

		$output = array();
		$output['header_menu'] = 'process_control';
        $output['header_sub_menu'] = 'nebenwirkung';
        $insured_number = $this->input->get('in');
        $is_exist_patient = $this->patients->getPatientByInsuredNumber($insured_number);
       	
        if ($is_exist_patient) {
	        $id = $is_exist_patient->id;
	        $output['insurance_number'] = $insured_number;          
        	$data_exists = $this->side_effects->getSideEffectsTableDataById($id);
        	$current_date = date('Y-m-d');

        	$visit_array = array();
        	
        	if ($data_exists) {

        		$patients_visits = $this->side_effects->getSideEffectsTableVisitsDataById($id);
        		//pr($patients_visits); die;

        		if ($patients_visits) {
        			foreach ($patients_visits as $key => $value) {
        				$patient_visit_data_exists = $this->side_effects->getAllVisitsSideEffectsDataByBehandlunggstagId($value->id);
        				

        				foreach ($patient_visit_data_exists as $k => $v) {

        					$type = '';
        					$html = '';

        					if ($v->mild_schweregrad == 'Yes') {
        						$type = 'mild'; 
        						$html = '<span style="background:#2CA61E; display:block; height:20px;">'; 
        					}
				            if ($v->moderat_schweregrad == 'Yes') {
				            	$type = 'moderat'; 
				            	$html = '<span style="background:#FCAF16; display:block; height:20px;">'; 
				            }
				            if ($v->schwer_schweregrad == 'Yes') {
				            	$type = 'schwer'; 
				            	$html = '<span style="background:#F76816; display:block; height:20px;">'; 
				            }
				            if ($v->lebensbedrohlich_schweregrad == 'Yes') {
				            	$type = 'lebensbedrohlich'; 
				            	$html = '<span style="background:#D91313; display:block; height:20px;">'; 
				            }

        					$visit_array[$value->visite][$v->nebenwirkung_name] = $html;
        					// $visit_array[$value->visite][$k]['color'] = $color;
        					// $visit_array[$value->visite][$k]['type'] = $type;
        				}
        			
        			}
        		}
        		// pr($visit_array); die;
        		$c_date = strtotime($current_date);
        		$first_visit = $this->side_effects->getFirstVisit($id);
        		$datediff = $c_date - strtotime($first_visit->datum);
				$behandungstag = round($datediff / (60 * 60 * 24));

        		$s_date = strtotime($data_exists->datum);

        		if ($s_date == $c_date) {
        			$visit_number = $data_exists->visite;
        			$is_current_record = 'Yes';
        		} else {
        			$visit_number = $data_exists->visite + 1;
        			$is_current_record = 'No';
        		}

	        	$output['datum'] = date('d.m.Y', strtotime($current_date));
	        	$output['behandungstag'] = $behandungstag;
	        	$output['visite'] = $visit_number;
	        	$output['arzneimittel'] = $data_exists->arzneimittel;
	        	$output['keine_kausal_bedingte'] = $data_exists->keine_kausal_bedingte;
	        	$output['visit_array'] = $visit_array;
        		$behandlunggstag_id = $data_exists->id;
        		$table_data_exists = $this->side_effects->getSideEffectsDataByBehandlunggstagId($behandlunggstag_id);
        		$output['table_data_exists'] = $table_data_exists;
        	} else {
        		$output['datum'] = date('d.m.Y', strtotime($current_date));
	        	$output['behandungstag'] = '0';
	        	$output['visite'] = '1';
	        	$output['arzneimittel'] = 'CannaXan-THC';
	        	$output['keine_kausal_bedingte'] = '';
	        	$output['visit_array'] = $visit_array;
	        	$is_current_record = 'No';
        	}
        	$visits = $this->process_model->getPatientQuestions($is_exist_patient->id);
		    $output['visits'] = $visits;
		    $output['is_current_record'] = $is_current_record;
        } else {
        	redirect('patients');
        }
        
        $records = $this->side_effects->getAllData();
		//pr($records); die;
		$output['records'] = $records;

		$this->load->view('front/includes/header', $output);
		$this->load->view('front/nebenwirkung/nebenwirkungen-auswertung');
		$this->load->view('front/includes/footer');
	}
}
