<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function add_book()
	{
		$saveData[0]["year"] = '2018';
		$saveData[0]["book_group_id"] = 14;
		$saveData[0]["book_name"] = "Baron EP (2018) Medicinal Properties of Cannabinoids, Terpenes, and Flavonoids in Cannabis, and Benefits in Migraine, Headache, and Pain: An Update on Current Evidence and Cannabis Science. Headache 58 (7):1139-1186. doi:10.1111/head.13345";

		$saveData[1]["year"] = '2018';
		$saveData[1]["book_group_id"] = 14;
		$saveData[1]["book_name"] = "Greco R, Demartini C, Zanaboni AM, Piomelli D, Tassorelli C (2018) Endocannabinoid System and Migraine Pain: An Update. Front Neurosci 12:172. doi:10.3389/fnins.2018.00172";

		$saveData[2]["year"] = '2016';
		$saveData[2]["book_group_id"] = 14;
		$saveData[2]["book_name"] = "Rhyne DN, Anderson SL, Gedde M, Borgelt LM (2016) Effects of Medical Marijuana on Migraine Headache Frequency in an Adult Population. Pharmacotherapy 36 (5):505-510. doi:10.1002/phar.1673";

		$saveData[3]["year"] = '2010';
		$saveData[3]["book_group_id"] = 14;
		$saveData[3]["book_name"] = "Collin C, Ehler E, Waberzinek G, Alsindi Z, Davies P, Powell K, Notcutt W, O'Leary C, Ratcliffe S, Novakova I, Zapletalova O, Pikova J, Ambler Z (2010) A double-blind, randomized, placebo-controlled, parallel-group study of Sativex, in subjects with symptoms of spasticity due to multiple sclerosis. Neurological research 32 (5):451-459. doi:10.1179/016164109x12590518685660";

		$saveData[4]["year"] = '2010';
		$saveData[4]["book_group_id"] = 14;
		$saveData[4]["book_name"] = "Greco R, Gasperi V, Maccarrone M, Tassorelli C. The endocannabinoid system and migraine. Exp Neurol 2010 07;224(1090-2430; 0014-4886; 1):85-91. ";

		$saveData[5]["year"] = '2009';
		$saveData[5]["book_group_id"] = 14;
		$saveData[5]["book_name"] = "Villalon CM, Olesen J. The role of CGRP in the pathophysiology of migraine and efficacy of CGRP receptor antagonists as acute antimigraine drugs. Pharmacol Ther 2009 12;124(1879-016; 0163-7258; 3):309-23.";

		$saveData[6]["year"] = '2008';
		$saveData[6]["book_group_id"] = 14;
		$saveData[6]["book_name"] = "Cupini LM, Costa C, Sarchielli P, Bari M, Battista N, Eusebi P, Calabresi P, Maccarrone M. Degradation of endocannabinoids in chronic migraine and medication overuse headache. Neurobiol Dis 2008 05;30(1095-953; 0969-9961; 2):186-9. ";

		$saveData[7]["year"] = '2007';
		$saveData[7]["book_group_id"] = 14;
		$saveData[7]["book_name"] = "Sarchielli P, Pini LA, Coppola F, Rossi C, Baldi A, Mancini ML, Calabresi P. Endocannabinoids in chronic migraine: CSF findings suggest a system failure. Neuropsychopharmacology 2007 06;32(0893-133; 0006-3223; 6):1384-90.";

		$saveData[8]["year"] = '2004';
		$saveData[8]["book_group_id"] = 14;
		$saveData[8]["book_name"] = "Russo EB. Clinical endocannabinoid deficiency (CECD): Can this concept explain therapeutic benefits of cannabis in migraine, fibromyalgia, irritable bowel syndrome and other treatment-resistant conditions? Neuro.Endocrinol.Lett. 2004 02;25(0172-780; 0172-780; 1-2):31-9.";

		/*$saveData[9]["year"] = '2012';
		$saveData[9]["book_group_id"] = 13;
		$saveData[9]["book_name"] = "Corey-Bloom J, Wolfson T, Gamst A, Jin S, Marcotte TD, Bentley H, Gouaux B (2012) Smoked cannabis for spasticity in multiple sclerosis: a randomized, placebo-controlled trial. CMAJ 184 (10):1143-1150. doi:10.1503/cmaj.110837";

		$saveData[10]["year"] = '2012';
		$saveData[10]["book_group_id"] = 13;
		$saveData[10]["book_name"] = "Baker D, Pryce G, Jackson SJ, Bolton C, Giovannoni G. The biology that underpins the therapeutic potential of cannabis-based medicines for the control of spasticity in multiple sclerosis. Mult Scler Relat Disord 2012 Apr;1(2):64-75.";

		$saveData[11]["year"] = '2012';
		$saveData[11]["book_group_id"] = 13;
		$saveData[11]["book_name"] = "Zajicek JP, Hobart JC, Slade A, Barnes D, Mattison PG. MUltiple sclerosis and extract of cannabis: Results of the MUSEC trial. J Neurol Neurosurg Psychiatr 2012 11;83(1468-330; 0022-3050; 11):1125-32. ";

		$saveData[12]["year"] = '2011';
		$saveData[12]["book_group_id"] = 13;
		$saveData[12]["book_name"] = "Novotna A, Mares J, Ratcliffe S, Novakova I, Vachova M, Zapletalova O, Gasperini C, Pozzilli C, Cefaro L, Comi G, Rossi P, Ambler Z, Stelmasiak Z, Erdmann A, Montalban X, Klimek A, Davies P (2011) A randomized, double-blind, placebo-controlled, parallel-group, enriched-design study of nabiximols* (Sativex((R)) ), as add-on therapy, in subjects with refractory spasticity caused by multiple sclerosis. Eur J Neurol 18 (9):1122-1131. doi:10.1111/j.1468-1331.2010.03328.x";

		$saveData[13]["year"] = '2011';
		$saveData[13]["book_group_id"] = 13;
		$saveData[13]["book_name"] = "Honarmand K, Tierney MC, O'Connor P, Feinstein A. Effects of cannabis on cognitive function in patients with multiple sclerosis. Neurology 2011 03/29;76(1526-632; 0028-3878; 13):1153-60.";

		$saveData[14]["year"] = '2010';
		$saveData[14]["book_group_id"] = 13;
		$saveData[14]["book_name"] = "Collin C, Ehler E, Waberzinek G, Alsindi Z, Davies P, Powell K, Notcutt W, O'Leary C, Ratcliffe S, Novakova I, Zapletalova O, Pikova J, Ambler Z (2010) A double-blind, randomized, placebo-controlled, parallel-group study of Sativex, in subjects with symptoms of spasticity due to multiple sclerosis. Neurological research 32 (5):451-459. doi:10.1179/016164109x12590518685660";

		$saveData[15]["year"] = '2010';
		$saveData[15]["book_group_id"] = 13;
		$saveData[15]["book_name"] = "Rossi S, Bernardi G, Centonze D. The endocannabinoid system in the inflammatory and neurodegenerative processes of multiple sclerosis and of amyotrophic lateral sclerosis. Exp Neurol 2010 07;224(1090-2430; 0014-4886; 1):92-102. ";

		$saveData[16]["year"] = '2010';
		$saveData[16]["book_group_id"] = 13;
		$saveData[16]["book_name"] = "Karst M, Wippermann S, Ahrens J. Role of cannabinoids in the treatment of pain and (painful) spasticity. Drugs 2010 12/24;70(0012-6667; 0012-6667; 18):2409-38. ";

		$saveData[17]["year"] = '2009';
		$saveData[17]["book_group_id"] = 13;
		$saveData[17]["book_name"] = "Aragona M, Onesti E, Tomassini V, Conte A, Gupta S, Gilio F, Pantano P, Pozzilli C, Inghilleri M. Psychopathological and cognitive effects of therapeutic cannabinoids in multiple sclerosis: A double-blind, placebo controlled, crossover study. Clin Neuropharmacol 2009 01;32(1537-162; 0362-5664; 1):41-7. ";

		$saveData[18]["year"] = '2008';
		$saveData[18]["book_group_id"] = 13;
		$saveData[18]["book_name"] = "Deutsch SI, Rosse RB, Connor JM, Burket JA, Murphy ME, Fox FJ. Current status of cannabis treatment of multiple sclerosis with an illustrative case presentation of a patient with MS, complex vocal tics, paroxysmal dystonia, and marijuana dependence treated with dronabinol. CNS Spectr 2008 May;13(5):393-403.";

		$saveData[19]["year"] = '2007';
		$saveData[19]["book_group_id"] = 13;
		$saveData[19]["book_name"] = "Collin C, Davies P, Mutiboko IK, Ratcliffe S. Randomized controlled trial of cannabis-based medicine in spasticity caused by multiple sclerosis. Eur J Neurol 2007 03;14(1468-1331; 3):290-6. ";

		$saveData[20]["year"] = '2007';
		$saveData[20]["book_group_id"] = 13;
		$saveData[20]["book_name"] = "Rog DJ, Nurmikko TJ, Young CA. Oromucosal delta9-tetrahydrocannabinol/cannabidiol for neuropathic pain associated with multiple sclerosis: An uncontrolled, open-label, 2-year extension trial. Clin Ther 2007 09;29(0149-2918; 0149-2918; 9):2068-79.";

		$saveData[21]["year"] = '2007';
		$saveData[21]["book_group_id"] = 13;
		$saveData[21]["book_name"] = "Centonze D, Rossi S, Finazzi-Agro A, Bernardi G, Maccarrone M. The (endo)cannabinoid system in multiple sclerosis and amyotrophic lateral sclerosis. Int Rev Neurobiol 2007;82(0074-7742; 0074-7742):171-86.";

		$saveData[22]["year"] = '2007';
		$saveData[22]["book_group_id"] = 13;
		$saveData[22]["book_name"] = "Pertwee RG. Cannabinoids and multiple sclerosis. Mol Neurobiol 2007 08;36(0893-7648; 0893-7648; 1):45-59. ";

		$saveData[23]["year"] = '2006';
		$saveData[23]["book_group_id"] = 13;
		$saveData[23]["book_name"] = "Wade DT, Makela PM, House H, Bateman C, Robson P. Long-term use of a cannabis-based medicine in the treatment of spasticity and other symptoms in multiple sclerosis. Mult Scler 2006 10;12(1352-4585; 5):639-45. ";

		$saveData[24]["year"] = '2006';
		$saveData[24]["book_group_id"] = 13;
		$saveData[24]["book_name"] = "Chong MS, Wolff K, Wise K, Tanton C, Winstock A, Silber E. Cannabis use in patients with multiple sclerosis. Mult Scler 2006 10;12(1352-4585; 1352-4585; 5):646-51. ";

		$saveData[25]["year"] = '2006';
		$saveData[25]["book_group_id"] = 13;
		$saveData[25]["book_name"] = "Freeman RM, Adekanmi O, Waterfield MR, Waterfield AE, Wright D, Zajicek J. The effect of cannabis on urge incontinence in patients with multiple sclerosis: A multicentre, randomised placebo-controlled trial (CAMS-LUTS). Int  Urogynecol J Pelvic Floor Dysfunct 2006 11;17(0937-3462; 0937-3462; 6):636-41. ";

		$saveData[26]["year"] = '2006';
		$saveData[26]["book_group_id"] = 13;
		$saveData[26]["book_name"] = "Page SA, Verhoef MJ. Medicinal marijuana use: Experiences of people with multiple sclerosis. Can Fam Physician 2006 01;52(0008-350; 0008-350):64-5. ";

		$saveData[27]["year"] = '2005';
		$saveData[27]["book_group_id"] = 13;
		$saveData[27]["book_name"] = "Rog DJ, Nurmikko TJ, Friede T, Young CA (2005) Randomized, controlled trial of cannabis-based medicine in central pain in multiple sclerosis. Neurology 65 (6):812-819. doi:10.1212/01.wnl.0000176753.45410.8b";

		$saveData[28]["year"] = '2005';
		$saveData[28]["book_group_id"] = 13;
		$saveData[28]["book_name"] = "Zajicek JP, Sanders HP, Wright DE, Vickery PJ, Ingram WM, Reilly SM, Nunn AJ, Teare LJ, Fox PJ, Thompson AJ. Cannabinoids in multiple sclerosis (CAMS) study: Safety and efficacy data for 12 months follow up. J Neurol Neurosurg Psychiatr 2005 12;76(0022-3050; 12):1664-9.";

		$saveData[29]["year"] = '2004';
		$saveData[29]["book_group_id"] = 13;
		$saveData[29]["book_name"] = "Svendsen KB, Jensen TS, Bach FW (2004) Does the cannabinoid dronabinol reduce central pain in multiple sclerosis? Randomised double blind placebo controlled crossover trial. BMJ 329 (7460):253. doi:10.1136/bmj.38149.566979.AE";

		$saveData[30]["year"] = '2004';
		$saveData[30]["book_group_id"] = 13;
		$saveData[30]["book_name"] = "Clark AJ, Ware MA, Yazer E, Murray TJ, Lynch ME. Patterns of cannabis use among patients with multiple sclerosis. Neurology 2004 06/08;62(1526-632; 11):2098-100. ";

		$saveData[31]["year"] = '2004';
		$saveData[31]["book_group_id"] = 13;
		$saveData[31]["book_name"] = "Wade DT, Makela P, Robson P, House H, Bateman C. Do cannabis-based medicinal extracts have general or specific effects on symptoms in multiple sclerosis? A double-blind, randomized, placebo-controlled study on 160 patients. Mult Scler 2004 08;10(1352-4585; 4):434-41. ";

		$saveData[32]["year"] = '2004';
		$saveData[32]["book_group_id"] = 13;
		$saveData[32]["book_name"] = "Vaney C, Heinzel-Gutenbrunner M, Jobin P, Tschopp F, Gattlen B, Hagen U, Schnelle M, Reif M. Efficacy, safety and tolerability of an orally administered cannabis extract in the treatment of spasticity in patients with multiple sclerosis: A randomized, double-blind, placebo-controlled, crossover study. Mult Scler 2004 08;10(1352-4585; 4):417-24.";

		$saveData[33]["year"] = '2003';
		$saveData[33]["book_group_id"] = 13;
		$saveData[33]["book_name"] = "Zajicek J, Fox P, Sanders H, Wright D, Vickery J, Nunn A, Thompson A (2003) Cannabinoids for treatment of spasticity and other symptoms related to multiple sclerosis (CAMS study): multicentre randomised placebo-controlled trial. Lancet 362 (9395):1517-1526. doi:10.1016/s0140-6736(03)14738-1";

		$saveData[34]["year"] = '2003';
		$saveData[34]["book_group_id"] = 13;
		$saveData[34]["book_name"] = "Page SA, Verhoef MJ, Stebbins RA, Metz LM, Levy JC. Cannabis use as described by people with multiple sclerosis. Can J Neurol Sci 2003 08;30(0317-1671; 0317-1671; 3):201-5.";

		$saveData[35]["year"] = '2000';
		$saveData[35]["book_group_id"] = 13;
		$saveData[35]["book_name"] = "Baker D, Pryce G, Croxford JL, Brown P, Pertwee RG, Huffman JW, Layward L. Cannabinoids control spasticity and tremor in a multiple sclerosis model. Nature 2000 03/02;404(0028-0836; 0028-0836; 6773):84-7. ";

		$saveData[36]["year"] = '2002';
		$saveData[36]["book_group_id"] = 5;
		$saveData[36]["book_name"] = "Iversen L, Chapman V (2002) Cannabinoids: a real prospect for pain relief? Curr Opin Pharmacol 2 (1):50-55";*/


		$this->db->insert_batch('tbl_icd_code_books', $saveData);
	}
}
