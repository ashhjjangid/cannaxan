<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 
 */
class Doctors extends CI_Controller
{

	
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('doctors_model', 'doctors_model');
		$this->load->model('admin/user_model','user');
		$this->load->model('mailsending_model', 'mailsend');
	}

	function login() {

		$output['menu'] = 'ANMELDUNG';

		if ($this->session->userdata('user_id')) {
			
			$doctor_id = $this->session->userdata('user_id');
			$valid_doctor = $this->doctors_model->getUserById($doctor_id);

			if ($valid_doctor->is_logged_in == 'Yes') {						

				if ($this->input->is_ajax_request()) {
					$message = false;
					$success = true;
					$data['redirectURL'] = base_url('patients');
					json_output($data);
				} else {
					redirect('patients');
				}
			} else {
				if ($this->input->is_ajax_request()) {
					$message = false;
					$success = true;
					$data['redirectURL'] = base_url('doctors/set_new_password');
					json_output($data);
				} else {
					redirect('doctors/set_new_password');
				}
			}
		}
		$output['key'] = null;

		if ($this->input->post()) {
			
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', 
				array('required' => 'E-Mail ist ein Pflichtfeld, dass ausgefüllt werden muss',
					'valid_email' => 'Das E-Mail-Feld muss eine gültige E-Mail-Adresse enthalten',
			));
			$this->form_validation->set_rules('password', 'Password', 'trim|required', 
				array('required' => 'Passwort ist ein Pflichtfeld, dass ausgefüllt werden muss'));

			if ($this->form_validation->run()) {

				$current_date = date('Y-m-d');
				$email = $this->input->post('email');
				$password = $this->input->post('password');

				$encrypt_password = encrpty_password($password);

				$valid_doctor = $this->doctors_model->loginCredentials($email, $encrypt_password);

				if ($valid_doctor) {

					$user_id = $valid_doctor->id;
					$name = $valid_doctor->name;
					$email = $valid_doctor->email;

					
					$this->session->set_userdata(array('user_id' => $user_id, 'name' => $name, 'email' => $email));

					if ($valid_doctor->is_logged_in == 'Yes') {						
					
						if ($current_date >= $valid_doctor->next_password_change) {
							$message = 'Bitte ändern Sie Ihr Passwort';
							$success = true;
							$data['redirectURL'] = base_url('doctors/set_new_password');
							$data['success'] = $success;
							$data['message'] = $message;
							json_output($data); die;
						}

						$message = 'Erfolgreich angemeldet';
						$success = true;
						$data['redirectURL'] = base_url('patients');

					} else {

						$message = 'Erfolgreich angemeldet';
						$success = true;
						$data['redirectURL'] = base_url('doctors/set_new_password');

					}
				} else {

					$success = false;
					$message = 'Falscher Benutzername oder falsches Passwort oder steht noch zur Genehmigung an';
				}
			} else {
				$success = false;
				$message = validation_errors();
			}

			$data['success'] = $success;
			$data['message'] = $message;
			json_output($data);

		}

		$this->load->view('front/includes/header', $output);
		$this->load->view('front/doctors/index');
		$this->load->view('front/includes/footer');
	}

	/*function first_step_registration() {


		$this->load->view('front/includes/header');
		$this->load->view('front/doctors/first_step_register');
		$this->load->view('front/includes/footer');
	}*/

	function registration_step_one() {

		$output['menu'] = 'REGISTRIERUNG';
		$success = '';
		$message = '';

		if ($this->session->userdata('user_id')) {
			redirect('patients');
		}

		if ($this->input->post()) {
			//pr($_POST); die;
			$this->form_validation->set_rules('first_name', 'Vor Name', 'trim|required', array('required' => 'Vorname ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('last_name', 'Name', 'trim|required', array('required' => 'Name ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('strabe', 'STRAßE', 'trim|required', array('required' => 'STRAßE ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('plzl', 'plz', 'trim|required|numeric|max_length[5]',
				array('required' => 'Postleitzahl ist ein Pflichtfeld, dass ausgefüllt werden muss',
					'numeric' => 'Das Postleitzahl-Feld darf nur Zahlen enthalten',
					'max_length' => 'Das Postleitzahl-Feld darf nicht länger als 5 Zeichen sein'));
			$this->form_validation->set_rules('house_no', 'Hausnr', 'trim|required', array('required' => 'Hausnr ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('ort', 'ORT', 'trim|required', array('required' => 'ORT ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('title', 'Titel', 'trim|required', array('required' => 'Titel ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_members.email]', 
				array('required' => 'E-Mail ist ein Pflichtfeld, dass ausgefüllt werden muss',
					'valid_email' => 'Das E-Mail-Feld muss eine gültige E-Mail-Adresse enthalten',
					'is_unique' => 'Diese E-Mailadresse ist bereits registriert. Bitte geben Sie eine andere Adresse an oder wenden Sie sich an info@cannaxan.de',
			));
			$this->form_validation->set_rules('vertarg', 'Vertarg', 'trim|required', array('required' => 'Vertarg ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('verstanden_vertarg', 'Verstanden vertarg', 'trim|required', array('required' => 'Verstanden vertarg ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('akzeptieren_vertarg', 'Akzeptieren vertarg', 'trim|required', array('required' => 'Akzeptieren vertarg ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('salutation', 'Anrede', 'trim|required', array(
				'required' => 'Das Anredefeld ist erforderlich'));

			if ($this->form_validation->run()) {
				
				$title = $this->input->post('title');
				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				$strabe = $this->input->post('strabe');
				$plzl = $this->input->post('plzl');
				$house_no = $this->input->post('house_no');
				$anedre = $this->input->post('salutation');
				$email = $this->input->post('email');
				$ort = $this->input->post('ort');
				$vertarg = $this->input->post('vertarg');
				$verstanden_vertarg = $this->input->post('verstanden_vertarg');
				$akzeptieren_vertarg = $this->input->post('akzeptieren_vertarg');
				/*pr($vertarg);
				pr($verstanden_vertarg);
				pr($akzeptieren_vertarg); die;*/

				$data['redirectURL'] = base_url('doctors/register/step-2?title='.$title.'&anedre='.$anedre.'&first_name='.$first_name.'&last_name='.$last_name.'&email='.$email.'&strabe='.$strabe.'&plzl='.$plzl.'&hausnr='.$house_no.'&ort='.$ort.'&vertarg='.$vertarg.'&verstanden_vertarg='.$verstanden_vertarg.'&akzeptieren_vertarg='.$akzeptieren_vertarg);
				$success = true;
			} else {
				$message = validation_errors();
				$success = false;
			}
			$data['success'] = $success;
			$data['message'] = $message;
			json_output($data);
		}

		$this->load->view('front/includes/header', $output);
		$this->load->view('front/doctors/first_step_register');
		$this->load->view('front/includes/footer');
	}

	function registration_step_two() {

		$output['menu'] = 'REGISTRIERUNG';
		$output['title'] = $this->input->get('title');
		$output['first_name'] = $this->input->get('first_name');
		$output['last_name'] = $this->input->get('last_name');
		$output['strabe'] = $this->input->get('strabe');
		$output['plzl'] = $this->input->get('plzl');
		$output['house_no'] = $this->input->get('hausnr');
		$output['ort'] = $this->input->get('ort');
		$output['anedre'] = $this->input->get('anedre');
		$output['email'] = $this->input->get('email');
		$output['vertarg'] = $this->input->get('vertarg');
		$output['verstanden_vertarg'] = $this->input->get('verstanden_vertarg');
		$output['akzeptieren_vertarg'] = $this->input->get('akzeptieren_vertarg');
		//pr($output); die;

		if ($this->session->userdata('user_id')) {
			redirect('patients');
		}

		if ($this->input->post()) {
			
			$input = array();
			$success = true;

			$this->form_validation->set_rules('salutation', 'Anrede', 'trim|required', array(
				'required' => 'Das Anredefeld ist erforderlich'));
			$this->form_validation->set_rules('name_doctor_practice', 'Name Arztpraxis', 'trim|required',
				array('required' => 'Name der Arztpraxis ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('name', 'Name', 'trim|required', array('required' => 'Name ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('first_name', 'Vorname', 'trim|required', array('required' => 'Vorname ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('specialization', 'Fachrichtung', 'trim|required', array('required' => 'Fachrichtung ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('lanr', 'Lanr', 'trim|required|numeric|exact_length[9]|is_unique[tbl_members.lanr]',
				array('required' => 'Bitte geben Sie hier Ihre zehnstellige Arztnummer ein.',
					'numeric' => 'Das lanr-Feld darf nur Zahlen enthalten.',
					'exact_length' => 'Das LARN-Feld muss genau 9 Zeichen lang sein.',
					'is_unique' => 'Diese Nummer wurde bereits angelegt.'));
			$this->form_validation->set_rules('title', 'Titel', 'trim|required', 
				array('required' => 'Titel ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('ort', 'ORT', 'trim|required', 
				array('required' => 'ORT ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('road', 'Straße', 'trim|required', 
				array('required' => 'Straße ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('house_no', 'Hausnr', 'trim|required',
				array('required' => 'Hausnr ist ein Pflichtfeld, dass ausgefüllt werden muss'));
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_members.email]', 
				array('required' => 'E-Mail ist ein Pflichtfeld, dass ausgefüllt werden muss',
					'valid_email' => 'Das E-Mail-Feld muss eine gültige E-Mail-Adresse enthalten',
					'is_unique' => 'Diese E-Mailadresse ist bereits registriert. Bitte geben Sie eine andere Adresse an oder wenden Sie sich an info@cannaxan.de',
			));
			//$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('plzl', 'Postleitzahl', 'trim|required|numeric|max_length[5]',
				array('required' => 'Postleitzahl ist ein Pflichtfeld, dass ausgefüllt werden muss',
					'numeric' => 'Das Postleitzahl-Feld darf nur Zahlen enthalten',
					'max_length' => 'Das Postleitzahl-Feld darf nicht länger als 5 Zeichen sein'));
			$this->form_validation->set_rules('website', 'www', 'trim|valid_url',
				array('valid_url' => 'Das Feld www muss eine gültige URL enthalten'));
			$this->form_validation->set_rules('phonenumber', 'Tel. nr.', 'trim|required|regex_match[/^[+0-9 ]+$/]', array('required' => 'Telefonnummer ist ein Pflichtfeld, dass ausgefüllt werden muss',
				'regex_match' => 'Die Eingabe entspricht nicht den Vorgaben einer Telefonnummer'));
			$this->form_validation->set_rules('fax', 'Fax', 'trim');
			$this->form_validation->set_rules('videosprechstunde', 'Videosprechstunde', 'trim');
			//$this->form_validation->set_rules('data_storage', 'DatenSpeicherung', 'trim|required', array('required' => 'Daten Speicherung ist ein Pflichtfeld, dass ausgefüllt werden muss'));

			
			if ($this->form_validation->run()) {

				if (isset($_FILES['signature']['tmp_name']) && $_FILES['signature']['tmp_name']) {
                    $directory = './assets/uploads/unterschrift-images'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if ($this->upload->do_upload('signature')) {
                        $image = $this->upload->data();
                        $file_name = $image['file_name'];
                        $input['unterschrift'] = $file_name;
                    } else {
                        $message = $this->upload->display_errors();
                        $success = false;
                        //pr($this->upload->display_errors());
                    }
                }/* else {
                    $message = 'Bitte laden Sie eine Unterschrift hoch';
                    $success = false;
                }*/

                if ($success) {
                	
                	$complete_string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+=*&^%$@!"';
                	$verification_string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                	$setrandompassword = substr(str_shuffle($complete_string), 0, 10);
                	$verification_key = substr(str_shuffle($verification_string), 0, 10);
					//$setrandompassword = get_random_key(11);
					
					//pr($setrandompassword); die;
					/*$password = $this->input->post('password', true);
					$encrypt_password = encrpty_password($password);*/

					$current_date = date('Y-m-d');

					$end_date = time() + (84*24*60*60);
					$next_date = date('Y-m-d', $end_date);
					//pr($next_date); die;
					$salutation = $this->input->post('salutation');
					$input['lecture'] = $salutation;
					$input['name_doctor_practice'] = $this->input->post('name_doctor_practice');
					$last_name = $this->input->post('name');
					$input['name'] = $last_name;
					$first_name = $this->input->post('first_name');
					$input['first_name'] = $first_name;
					$input['subject'] = $this->input->post('specialization');
					$input['lanr'] = $this->input->post('lanr');
					$title = $this->input->post('title');
					$input['title'] = $title;
					$input['road'] = $this->input->post('road');
					$input['hausnr'] = $this->input->post('house_no');
					$email = $this->input->post('email');
					$input['email'] = $email;
					$input['is_logged_in'] = 'No';
					$input['is_approved'] = 'No';
					$input['is_approved'] = 'No';
					$input['first_time_login'] = 'Yes';
					$new_random_password = encrpty_password($setrandompassword);
					$input['password'] = $new_random_password;
					$input['next_password_change'] = $next_date;
					$input['plzl'] = $this->input->post('plzl');
					$input['videosprechstunde'] = $this->input->post('videosprechstunde');
					$input['ort'] = $this->input->post('ort');
					$input['www'] = $this->input->post('website');
					$input['phonenumber'] = $this->input->post('phonenumber');
					$input['fax'] = $this->input->post('fax');
					//$input['datenSpeicherung'] = $this->input->post('data_storage');
					$input['vertarg'] = $this->input->post('vertarg');
					$input['verstanden_vertarg'] = $this->input->post('verstanden_vertarg');
					$input['akzeptieren_vertarg'] = $this->input->post('akzeptieren_vertarg');
					$input['verification_key'] = $verification_key;
					$input['add_date'] = getDefaultToGMTDate(time());
					$pdf_name = 'DSGVO_mit_CannaXan_GmbH'.'_'.date('Ymd');
					$input['contract_file'] = $pdf_name;

					$html = $this->load->view('front/doctors/doctor_info_pdf', $input, true);
					file_put_contents(ASSETSPATH. 'uploads/pdf/'.$pdf_name.".html", $html);
			        $newpdf_path = ASSETSPATH. 'uploads/pdf/'.$pdf_name.".pdf";
			        exec ('/usr/local/bin/wkhtmltopdf '. base_url('assets/uploads/pdf/').$pdf_name.'.html '.$newpdf_path." 2>&1 ", $response);

					//echo $newpdf_path; die;
					$insert_id = $this->doctors_model->registerNewDoctor($input);

					if ($insert_id) {
						
						//$message = 'Sie haben sich erfolgreich registriert. Bitte überprüfen Sie, ob der Passwort-Link per E-Mail gesendet wurde.';
						$message = 'Sie haben sich erfolgreich in unserem Portal registriert. Bitte warten Sie, bis Ihr Konto vom Administrator genehmigt wurde.';
						//$setpasswordURL = '<a href="'. base_url('doctors/set_new_password/'. $setpasswordkey) .'">Set Password</a>';

						//$this->mailsend->SetUserPasswordMail($insert_id, $setrandompassword);
						$this->mailsend->sendEmailToAdminToVerifyDoctor($insert_id, $email);
						$this->mailsend->sendContractToUser($insert_id, $email, $pdf_name, $verification_key);
						$success = true;
						$data['redirectURL'] = base_url('doctors/register/success?name='.$last_name.'&title='.$title.'&andere='.$salutation);
					} else {
						$success = false;
						$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
					}
                }
			} else {
				$success = false;
				$message = validation_errors();
			}

			$data['success'] = $success;
			$data['message'] = $message;
			json_output($data);
		}

		$this->load->view('front/includes/header', $output);
		$this->load->view('front/doctors/doctor_register');
		$this->load->view('front/includes/footer');
	}

	function register_success() {

		$output['herr'] = $this->input->get('andere');

		if ($this->input->get('title') == 'keine') {			
			$output['title'] = '';
		} else {
			$output['title'] = ' '.$this->input->get('title');
		}
		$output['dr_name'] = $this->input->get('name');

		$this->load->view('front/includes/header', $output);
		$this->load->view('front/doctors/welcome-page');
		$this->load->view('front/includes/footer');

	}

	function set_new_password() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
			
		} else {

			$doctor_id = $this->session->userdata('user_id');
			$user = $this->doctors_model->getUserById($doctor_id);
			//pr($doctor_id); die;

			if ($user) {
				
				$id = $user->id;
				$email = $user->email;
				$name = $user->name;
				$current_time = getDefaultToGMTDate(time());

				//$resetkey['key'] = $setnewpasswordkey;
				
				if (isset($_POST) && !empty($_POST)) {
					
					$this->form_validation->set_rules('set_new_password', 'New Password', 'required|trim|regex_match[/^(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W_]).{8}/]',
						array('required' => 'Dies ist ein Pflichtfeld, bitte geben Sie Ihr neues Passwort ein',
						'regex_match' => 'Das Passwort sollte aus mindestens 8 Zeichen bestehen und Buchstaben mit mindestens 1 Zahl und mindestens 1 Sonderzeichen enthalten'));
					$this->form_validation->set_rules('set_confirm_password', 'Confirm Password', 'required|trim|matches[set_new_password]', 
						array('required' => 'Dies ist ein Pflichtfeld, bitte wiederholen Sie Ihr neues Passwort',
							'matches' => 'Das Feld Passwort bestätigen stimmt nicht mit dem Feld Neues Passwort überein.'
					));

					if ($this->form_validation->run()) {
												
						$password = $this->input->post('set_new_password', true);
						$encrypt_password = encrpty_password($password);
						$end_date = time() + (84*24*60*60);
						$next_date = date('Y-m-d', $end_date);

						$input['next_password_change'] = $next_date;
						$input['password'] = $encrypt_password;
						$input['is_logged_in'] = 'Yes';
						$input['first_time_login'] = 'No';

						$input['update_date'] = getDefaultToGMTDate(time());

						$response = $this->doctors_model->setNewDoctorPassword($id, $input);

						if ($response) {
							$this->session->set_userdata(array('user_id'=> $id, 'email' => $email, 'name' =>$name));
							$message = 'Ihr Passwort wurde geändert';
							$success = true;
							
							$output['redirectURL'] = base_url('patients');
							

						} else {
							$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
							$success = false;
						}
					} else {
						$success = false;
						$message = validation_errors();
					}
					$output['message'] = $message;
	                $output['success'] = $success;
	                json_output($output);
				}
				$this->load->view('front/includes/header');
				$this->load->view('front/doctors/forget_password');
				$this->load->view('front/includes/footer'); 
			} else {
				$output['message'] = 'Ihr Passwort-Link ist abgelaufen.';
	            $this->load->view('front/doctors/index', $output);
			}
		}

	}

	/*function set_new_password($key = '') {

		if ($this->session->userdata('user_id')) {
			redirect('dashboard');
		}

		$setnewpasswordkey = $key;
		$user = $this->doctors_model->getUserBypasswordkey($setnewpasswordkey);

		if ($user) {
			
			$id = $user->id;
			$email = $user->email;
			$name = $user->name;
			$current_time = getDefaultToGMTDate(time());

			$resetkey['key'] = $setnewpasswordkey;
			
			if (isset($_POST) && !empty($_POST)) {
				
				$this->form_validation->set_rules('new_password', 'New Password', 'required|trim',
					array('required' => 'Dies ist ein Pflichtfeld, bitte geben Sie Ihr neues Passwort ein'));
				$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|trim|matches[new_password]', 
					array('required' => 'Dies ist ein Pflichtfeld, bitte wiederholen Sie Ihr neues Passwort',
						'matches' => 'Das Feld Passwort bestätigen stimmt nicht mit dem Feld Neues Passwort überein.'
				));

				if ($this->form_validation->run()) {
											
					$password = $this->input->post('new_password', true);
					$encrypt_password = encrpty_password($password);
					$response = $this->doctors_model->setNewPassword($id, $encrypt_password);

					if ($response) {
						$this->session->set_userdata(array('user_id'=> $id, 'email' => $email, 'name' =>$name));
						$message = 'Ihr Passwort wurde geändert';
						$success = true;
						$output['redirectURL'] = base_url('dashboard');

					} else {
						$message = 'Etwas ist schief gelaufen! Versuch es noch einmal...';
						$success = false;
					}
				} else {
					$success = false;
					$message = validation_errors();
				}
				$output['message'] = $message;
                $output['success'] = $success;
                json_output($output);
			}
			$this->load->view('front/includes/header', $resetkey);
			$this->load->view('front/doctors/set_password');
			$this->load->view('front/includes/footer'); 
		} else {
			$output['message'] = 'Ihr Passwort-Link ist abgelaufen.';
            $this->load->view('front/doctors/index', $output);
		}
	}*/

	function forgetpassword() {

		if ($this->session->userdata('user_id')) {
			redirect('patients');
		}

		if ($this->session->userdata('user_id')) {
			redirect('doctors');
		}

		if (isset($_POST) && !empty($_POST)) {

			$this->form_validation->set_rules('email', 'Email address', 'trim|required|valid_email', 
				array('required' => 'E-Mail ist ein Pflichtfeld, dass ausgefüllt werden muss',
					'valid_email' => 'Das E-Mail-Feld muss eine gültige E-Mail-Adresse enthalten',
			));

			if ($this->form_validation->run()) {
				
				$email = $this->input->post('email', true);
				//pr($email); die;

				$valid_user = $this->doctors_model->getUserByEmail($email);

				if ($valid_user) {

					$user_id = $valid_user->id;

					$complete_string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+=*&^%$@!"';
                	$setrandompassword = substr(str_shuffle($complete_string), 0, 10);

                	$input_data['password'] = encrpty_password($setrandompassword);
					$end_date = time() + (84*24*60*60);
					$next_date = date('Y-m-d', $end_date);
					$input_data['next_password_change'] = $next_date;
					$input_data['is_logged_in'] = 'No';
					$input_data['update_date'] = getDefaultToGMTDate(time());
					//pr($input_data); die;

					$response = $this->doctors_model->setNewDoctorPassword($user_id, $input_data);

					$forgotpasswordkey = get_random_key(30);
					$expire_time = getExpireTime();

					//$resetURL = '<a href="'. base_url('doctors/reset_password/'. $forgotpasswordkey) .'">Reset Password</a>';
					$resetURL = '<a href="'. base_url('doctors/set_new_password') .'">hier</a>';

					$input = array();
					$input['auth_key'] = $forgotpasswordkey;
					$input['user_id'] = $user_id;
					$input['key_type'] = 'Forget';
					$input['used'] = 'No';
					$input['expire_time'] = getDefaultToGMTDate($expire_time);

					$this->user->setpasswordkey($user_id, $input);
					$this->mailsend->SetUserForgetPasswordMail($user_id, $setrandompassword, $resetURL);
					//$this->mailsend->SetDoctorForgetPasswordMail($user_id, $setrandompassword);

					$message = 'Bitte überprüfen Sie Ihre E-Mail-Adresse und setzen Sie Ihr Passwort zurück.';
                    $output['redirectURL'] = base_url('doctors/set_new_password');
                    $success = true;
                    $output['resetForm'] = true;
				} else {
                    $message = 'E-Mail-Adresse nicht korrekt oder steht noch zur Genehmigung an.';
                    $success = false;
                }
			} else {
                $success = false;
                $message = validation_errors();
            }
            $output['message'] = $message;
            $output['success'] = $success;
            json_output($output);
		}

		$this->load->view('front/includes/header');
		$this->load->view('front/doctors/forget_password');
		$this->load->view('front/includes/footer');
	}

	function reset_password($key = '') {

		if ($this->session->userdata('user_id')) {
			redirect('patients');
		}

		$forgotpasswordkey = $key;
		$user = $this->user->getPasswordKey($forgotpasswordkey);

		if ($user) {
			
			$id = $user->user_id;
			$current_time = getDefaultToGMTDate(time());
			$checkValidURL = $this->user->getPasswordExpireTime($id, $current_time, $forgotpasswordkey);
			//pr($checkValidURL); die;

			if ($checkValidURL) {
				$resetkey['key'] = $forgotpasswordkey;
				
				if (isset($_POST) && !empty($_POST)) {
					
					$this->form_validation->set_rules('new_password', 'New Password', 'required|trim|regex_match[/^(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W_]).{8}/]', 
						array('required' => 'Dies ist ein Pflichtfeld, bitte geben Sie Ihr neues Passwort ein',
							'regex_match' => 'Das Passwort sollte aus mindestens 8 Zeichen bestehen und Buchstaben mit mindestens 1 Zahl und mindestens 1 Sonderzeichen enthalten'));
					$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|trim|matches[new_password]', 
						array('required' => 'Dies ist ein Pflichtfeld, bitte wiederholen Sie Ihr neues Passwort',
							'matches' => 'Das Feld Passwort bestätigen stimmt nicht mit dem Feld Neues Passwort überein.'
					));

					if ($this->form_validation->run()) {
												
						$password = $this->input->post('new_password', true);
						$encrypt_password = encrpty_password($password);

						$end_date = time() + (84*24*60*60);
						$next_date = date('Y-m-d', $end_date);

						$input['next_password_change'] = $next_date;
						$input['password'] = $encrypt_password;
						$response = $this->doctors_model->setNewDoctorPassword($id, $input);

						if ($response) {
							
							$message = 'Ihr Passwort hat sich geändert';
							$success = true;
							$output['redirectURL'] = base_url('patients');

						} else {
							$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch mal...';
							$success = false;
						}
					} else {
						$success = false;
						$message = validation_errors();
					}
					$output['message'] = $message;
                    $output['success'] = $success;
                    json_output($output);
				}
				$this->load->view('front/includes/header', $resetkey);
				$this->load->view('front/doctors/index');
				$this->load->view('front/includes/footer');
			} else {
                $output['message'] = 'Ihr Link zum Zurücksetzen des Passworts ist abgelaufen.';
                $this->load->view('front/doctors/index', $output);
            } 
		} else {
			$output['message'] = 'Ihr Link zum Zurücksetzen des Passworts ist abgelaufen.';
            $this->load->view('front/doctors/index', $output);
		}
	} 

	/*function info_pdf() {

		$this->load->view('front/includes/header');
		$this->load->view('front/doctors/doctor_info_pdf');
		$this->load->view('front/includes/footer');
	}*/

	function download_contract() {

		if ($this->session->userdata('user_id')) {
			
		} else {

			$input['name'] = $this->input->post('last_name');
			$first_name = $this->input->post('first_name');
			$input['first_name'] = $first_name;
			
			$input['title'] = $this->input->post('title');
			$input['road'] = $this->input->post('strabe');
			$input['hausnr'] = $this->input->post('house_no');
			
			$input['plzl'] = $this->input->post('plzl');
			$input['ort'] = $this->input->post('ort');
			
			$input['vertarg'] = $this->input->post('vertarg');
			$input['verstanden_vertarg'] = $this->input->post('verstanden_vertarg');
			$input['akzeptieren_vertarg'] = $this->input->post('akzeptieren_vertarg');
			$pdf_name = 'DSGVO_mit_CannaXan_GmbH'.'_'.date('Ymd');

			$html = $this->load->view('front/doctors/doctor_info_pdf', $input, true);
			file_put_contents(ASSETSPATH. 'uploads/pdf/'.$pdf_name.".html", $html);
	        $newpdf_path = ASSETSPATH. 'uploads/pdf/'.$pdf_name.".pdf";
	        exec ('/usr/local/bin/wkhtmltopdf '. base_url('assets/uploads/pdf/').$pdf_name.'.html '.$newpdf_path." 2>&1 ", $response);
	        $file_url = base_url().'assets/uploads/pdf/'.$pdf_name.'.pdf';
	        $data['file_name'] = $pdf_name.'.pdf';
	        $data['file_url'] = $file_url;
	        echo json_encode($data);
		}
	}

	function verify(){

		if ($this->session->userdata('user_id')) {
			redirect('patients');
		}

		$key = $this->input->get('key');

		if ($key) {
			$doctor = $this->doctors_model->getDoctorByVerificationKey($key);

			if ($doctor) {
				$output['dr_name'] = $doctor->name;
				$output['title'] = $doctor->title;
				$output['herr'] = $doctor->lecture;
				$output['key'] = $key;
				$this->load->view('front/includes/header', $output);
				$this->load->view('front/doctors/doctor_verify');
				$this->load->view('front/includes/footer');
			} else {
				redirect('doctors/login');
			}
		}
	}

	function doctor_verification(){
		$key = $this->input->get('key');

		$doctor = $this->doctors_model->getDoctorByVerificationKey($key);

		if ($doctor) {
			$user_id = $doctor->id;
			$updateData['verification_key'] = '';
			$updateData['is_register_completed'] = 'Yes';
			$updateData['update_date'] = getDefaultToGMTDate(time());
			$this->doctors_model->updateDoctor($user_id, $updateData);
			$success = true;
			$message = 'Erfolgreich überprüft.';
			$data['redirectURL'] = base_url('doctors/login');
		} else {
			$success = false;
			$message = 'Doktor nicht gefunden.';
			// $data['redirectURL'] = base_url('doctors/login');
		}

		$data['success'] = $success;
		$data['message'] = $message;
		json_output($data);
	}

	function logout() {

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}
		$this->session->sess_destroy();
		redirect('doctors/login');
	}
}