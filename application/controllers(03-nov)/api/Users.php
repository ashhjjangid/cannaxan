<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/rest/REST_Controller.php';
class Users extends REST_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('api/user_model','users');
	}

	public function patient_detail_post() 
	{
        $data = array();
		$success = 'error';
		$message = '';
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$userdata = $this->getUserDetailByToken($token);
		$data = $userdata;
		$message = 'Patient found.';
		$success = 'success'; 
		$response = array('status'=>$success,'message' => $message,'data' => $data);
    	$this->response($response, 200);
	}


	function check_token_exists_in_header() {
		if (isset($_SERVER['HTTP_TOKEN']) && $_SERVER['HTTP_TOKEN']) {

		} else {
			$response = array('status'=>'error','message' => "Token fehlt", 'data' => array());
			$this->response($response, 200);
		}
	}
	
	function getUserDetailByToken($token) {
		$user_detail = $this->users->getRecordByToken($token);
		
		if ($user_detail) {
			return $user_detail;
			
		} else {
			$response = array('status'=>'error','message' => 'Sitzung abgelaufen.','error_code' => 'delete_user','data'=>array());
			$this->response($response, 200);
		}
	}

	public function logout_post() {
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$userdata = $this->getUserDetailByToken($token);
		$updateData['token'] = null;
		$updateData['device_token'] = null;
		$updateData['device_type'] = null;

		$updateData['update_date'] = getDefaultToGMTDate(time());
		$update_record = $this->users->update_user_by_id($updateData,$userdata->id);

		if ($update_record) {
			$success = "success";
			$message = "Abmelden erfolgreich";
		
		} else {
			$success = "error";
			$message = "Technischer Fehler. Bitte versuchen Sie es erneut.";
		}
		$response = array('status'=>$success, 'message' => $message, 'data' => array());
		$this->response($response, 200);
	}	

}