<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/rest/REST_Controller.php';
class Faq extends REST_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('api/faq_model','faq');
	}

	public function get_faq_post() {
		$data = array();
		$success = 'error';
		$message = '';
		$_POST = $this->post();

		$faq_list = $this->faq->get_faq_categories();
		
		if ($faq_list) {
			
			foreach ($faq_list as $value) {
				$faqs = $this->faq->get_faq_by_category_id($value->id);
			    $value->faq = $faqs; 
			}	
			
			$success ='success';
		    $message = 'FAQ found.';
		    $data = $faq_list;
		} else {
			$success ='error';
		    $message = 'FAQ not found.';
		}

		$response = array('status'=>$success, 'message' => $message, 'data' => $data);
	    $this->response($response, 200);
	}
}