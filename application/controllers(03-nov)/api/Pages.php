<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/rest/REST_Controller.php';
class Pages extends REST_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('api/pages_model','page');
	}

	public function get_terms_condition_post() {
		$data = array();
		$success = 'error';
		$message = '';
		$_POST = $this->post();

		$terms_condition = $this->page->getTermCondition();
		if ($terms_condition) {
			$data = $terms_condition ;
			$success = 'success';
			$message = 'Term & Condition Found';
		} else {
			$success = 'success';
			$message = 'Term & Condition not Found';
		}
		$response = array('status'=>$success, 'message' => $message, 'data' => $data);
	    $this->response($response, 200);
	}
}