<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('dashboard_model', 'dashboard');
	}

	public function index()
	{
		$output['menu'] = 'Dashboard';

		if (!$this->session->userdata('user_id')) {
			redirect('doctors/login');
		}

		$id = $this->session->userdata('user_id');
		$record = $this->dashboard->getuserById($id);
		//pr($record); die;
		$output['dr_name'] = $record->name;
		$output['title'] = $record->title;
		$output['herr'] = $record->lecture;


		$this->load->view('front/includes/header', $output);
		$this->load->view('front/dashboard/willkommen');
		$this->load->view('front/includes/footer');
	}

	/*function loadpatient() {

		if ($this->input->post()) {
			
			$this->form_validation->set_rules('insurance_number', 'VERSICHERTENNUMMER', 'trim|required');

			if ($this->form_validation->run()) {

				$insured_number = $this->input->post('insurance_number');
				$patient_record = $this->dashboard->getPatientByInsuranceNumber($insured_number);

				if ($$patient_record) {
					
					$message = 'Patient found';
					$success = true;
				}

			}
		}

		$this->load->view('front/includes/header');
		$this->load->view('front/dashboard/patient-lladen');
		$this->load->view('front/includes/footer');
	}*/
}
