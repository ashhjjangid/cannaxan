<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/rest/REST_Controller.php';

class Patient extends REST_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api/user_model', 'user');
		$this->load->model('api/patient_model', 'patient');
	}

	function check_token_exists_in_header() {
		if (isset($_SERVER['HTTP_TOKEN']) && $_SERVER['HTTP_TOKEN']) {

		} else {
			$response = array('status'=>'error','message' => "Token fehlt", 'data' => array());
			$this->response($response, 200);
		}
	}

	function getUserDetailByToken($token) {
		$user_detail = $this->user->getUserByToken($token);
		
		if ($user_detail) {
			return $user_detail;
			
		} else {
			$response = array('status'=>'error','message' => 'Sitzung abgelaufen.','error_code' => 'delete_user','data'=>array());
			$this->response($response, 200);
		}
	}

	function step_1_post() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);

		if ($this->input->post()) {
			$this->form_validation->set_rules('weight', 'Weight', 'trim|required', array('required' => 'Bitte geben Sie Ihr Gewicht ein.'));
			$this->form_validation->set_rules('size', 'size', 'trim|required', array('required' => 'Bitte geben Sie Ihre Größe ein.'));
			$this->form_validation->set_rules('gender', 'Gender', 'trim|required', array('required' => 'Bitte geben Sie Ihr Geschlecht an.'));

			if ($this->form_validation->run()) {
				$id = $user_record['id'];
				$input = array();
				
				$input['weight'] = $this->input->post('weight');
				$input['size'] = $this->input->post('size');
				$input['gender'] = ucfirst($this->input->post('gender'));
				$input['update_date'] = getDefaultToGMTDate(time());

				$record_updated = $this->user->update_user($id, $input);

				if ($record_updated) {
					$success = true;
					$message = 'Ihre Angaben wurden aktualisiert';
				} else {

					$success = false;
					$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch mal...';
				}
			} else {
				$success = false;
				$message = validation_errors();
			}
			$response = array('status' => $success, 'message' => $message);
			$this->response($response, 200);
		}
	}

	function step_1_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);

		if ($user_record) {
			
			$users_data['user_id'] = $user_record['id'];
			$users_data['weight'] = $user_record['weight'];
			$users_data['size'] = $user_record['size'];
			$users_data['gender'] = $user_record['gender'];
			$users_data['is_first_visit_complete'] = $user_record['is_first_visit_complete'];
			$data = $users_data;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);

		//$question_data = $this->user->getUserQuestionsAnswered($id);
	}

	function is_first_visit_complete_post() {
		$data = array();
		$success = '';
		$message = '';
		$_GET = $this->get();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);

		if ($user_exists) {
			$data['is_first_visit_complete'] = $user_exists['is_first_visit_complete'];
			$success = true;
			$message = "Daten gefunden";
		} else {
			$success = false;
			$message = "Daten wurden nicht gefunden";
		}

		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);
	}

	function step_2_post() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);

		if ($this->input->post()) {

			$this->form_validation->set_rules('severe_pain_last_week', 'Severe Pain Last Week', 'required|trim', array('required' => 'Bitte geben Sie die Schmerzstärke ein.'));
			$this->form_validation->set_rules('pain_bearable', 'Is Pain_bearable', 'required|trim', array('required' => 'Bitte geben Sie an, ob der Schmerz erträglich ist.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkDailyRecordExist('tbl_app_schmerzstarke', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Ihr täglicher Fragebogen wurden bereits beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['severe_pain_last_week'] = $this->input->post('severe_pain_last_week');
					$saveData['pain_bearable'] = $this->input->post('pain_bearable');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_schmerzstarke($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['severe_pain_last_week'] = $this->input->post('severe_pain_last_week');
						$data['pain_bearable'] = $this->input->post('pain_bearable');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}

			} else {
				$message = validation_errors();
				$success = false;
			}

			$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_schmerzstarke', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_2_data_get() {
		$data = array();
		$success = '';
		$message = '';
		$current_screen_number = 1;
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		$schmerzstarke = $this->patient->get_schmerzstarke($id);

		if ($schmerzstarke) {	
			$data = $schmerzstarke;
			$data->is_first_visit_complete = $user_record['is_first_visit_complete'];
			$success = true;
			$message = "Daten gefunden";
		} else {	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_schmerzstarke', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_3_post() {

		$data = array();
		$input_step3 =array();
		$success = '';
		$message = '';
		$current_screen_number = 2;
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);

		if ($this->input->post()) {
			$this->form_validation->set_rules('no_pain_reliever_severe_pain', 'Severe Pain when pain relieve taken', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];

				
				$is_exist_today_data = $this->patient->checkDailyRecordExist('tbl_app_keine_schmerzmittel', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Ihr täglicher Fragebogen wurden bereits beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['no_pain_reliever_severe_pain'] = $this->input->post('no_pain_reliever_severe_pain');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_keine_schmerzmittel($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['no_pain_reliever_severe_pain'] = $this->input->post('no_pain_reliever_severe_pain');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_keine_schmerzmittel', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_3_data_get() 
	{
		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 2;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		$keine_schmerzmittel = $this->patient->get_keine_schmerzmittel($id);

		if ($keine_schmerzmittel) {
			$is_lock = true;	
			$data = $keine_schmerzmittel;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_keine_schmerzmittel', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_4_post() {

		$data = array();
		$input_step4 =array();
		$success = '';
		$message = '';
		$current_screen_number = 3;
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);

		if ($this->input->post()) {	
			$token = $_SERVER['HTTP_TOKEN'];
			$this->form_validation->set_rules('health_status', 'General Health Status', 'required|trim', array('required' => 'Bitte wählen Sie eine Option für den Gesundheitszustand aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_1', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['health_status'] = $this->input->post('health_status');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_1($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['health_status'] = $this->input->post('health_status');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_1', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_4_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 3;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_lebensqualitat_1 = $this->patient->get_app_lebensqualitat_1($id);

		if ($app_lebensqualitat_1) {
			$is_lock = true;	
			$data = $app_lebensqualitat_1;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_1', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_5_post() {

		$data = array();
		$input_step5 =array();
		$success = '';
		$message = '';
		$current_screen_number = 4;
		$_POST = $this->post();
		$this->check_token_exists_in_header();

		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);

		if ($this->input->post()) {
			$this->form_validation->set_rules('health_activities_moderate', 'General Health Moderate Activities', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];

				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_2', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['health_activities_moderate'] = $this->input->post('health_activities_moderate');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_2($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['health_activities_moderate'] = $this->input->post('health_activities_moderate');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}

			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_2', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_5_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 4;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_lebensqualitat_2 = $this->patient->get_app_lebensqualitat_2($id);

		if ($app_lebensqualitat_2) {
			$is_lock = true;	
			$data = $app_lebensqualitat_2;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}
		

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_2', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_6_post() {

		$data = array();
		$input_step6 =array();
		$success = '';
		$message = '';
		$current_screen_number = 5;
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);

		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('health_activities_climbing', 'General Health Climbing Activities', 'required|trim', array('Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_3', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['health_activities_climbing'] = $this->input->post('health_activities_climbing');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_3($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['health_activities_climbing'] = $this->input->post('health_activities_climbing');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_3', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_6_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 5;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		$app_lebensqualitat_3 = $this->patient->get_app_lebensqualitat_3($id);

		if ($app_lebensqualitat_3) {
			$is_lock = true;	
			$data = $app_lebensqualitat_3;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_3', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_7_post() {

		$data = array();
		$input_step7 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 6;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);

		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('physical_health_past_4_weeks_less', 'Physical Health in past 4 weeks did less
than wanted', 'required|trim', array('required' => 'Bitte wählen Sie JA oder NEIN aus.'));
			$this->form_validation->set_rules('physical_health_past_4_weeks_certain', 'Physical Health in past 4 weeks could only do certain doing things', 'trim|required', array('required' => 'Bitte wählen Sie JA oder NEIN aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];

				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_4', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['physical_health_past_4_weeks_less'] = $this->input->post('physical_health_past_4_weeks_less');
					$saveData['physical_health_past_4_weeks_certain'] = $this->input->post('physical_health_past_4_weeks_certain');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_4($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['physical_health_past_4_weeks_less'] = $this->input->post('physical_health_past_4_weeks_less');
						$data['physical_health_past_4_weeks_certain'] = $this->input->post('physical_health_past_4_weeks_certain');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_4', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_7_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 6;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_lebensqualitat_4 = $this->patient->get_app_lebensqualitat_4($id);

		if ($app_lebensqualitat_4) {
			$is_lock = true;	
			$data = $app_lebensqualitat_4;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_4', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_8_post() {

		$data = array();
		$input_step6 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 7;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);			
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];

			$this->form_validation->set_rules('mental_health_past_4_weeks_less', 'Mental Health in past 4 weeks did less
than wanted', 'required|trim', array('required' => 'Bitte wählen Sie JA oder NEIN aus.'));
			$this->form_validation->set_rules('mental_health_past_4_weeks_could_not_be_careful', 'Mental Health in past 4 weeks could not be so careful', 'trim|required', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_5', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['mental_health_past_4_weeks_less'] = $this->input->post('mental_health_past_4_weeks_less');
					$saveData['mental_health_past_4_weeks_could_not_be_careful'] = $this->input->post('mental_health_past_4_weeks_could_not_be_careful');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_5($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['mental_health_past_4_weeks_less'] = $this->input->post('mental_health_past_4_weeks_less');
						$data['mental_health_past_4_weeks_could_not_be_careful'] = $this->input->post('mental_health_past_4_weeks_could_not_be_careful');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_5', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_8_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_GET = $this->get();
		$current_screen_number = 7;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_lebensqualitat_5 = $this->patient->get_app_lebensqualitat_5($id);

		if ($app_lebensqualitat_5) {
			$is_lock = true;	
			$data = $app_lebensqualitat_5;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_5', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_9_post() {

		$data = array();
		$input_step9 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 8;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('pain_in_daily_activities_past_4_weeks', 'Pain in Daily exercise Activities in past 4 weeks', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_6', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['pain_in_daily_activities_past_4_weeks'] = $this->input->post('pain_in_daily_activities_past_4_weeks');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_6($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['pain_in_daily_activities_past_4_weeks'] = $this->input->post('pain_in_daily_activities_past_4_weeks');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}

			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_6', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_9_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 8;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_lebensqualitat_6 = $this->patient->get_app_lebensqualitat_6($id);

		if ($app_lebensqualitat_6) {
			$is_lock = true;	
			$data = $app_lebensqualitat_6;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_6', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_10_post() {

		$data = array();
		$input_step10 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 9;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('clam_in_past_4_weeks', 'Clam and relaxed in past 4 weeks', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_7', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['clam_in_past_4_weeks'] = $this->input->post('clam_in_past_4_weeks');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_7($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['clam_in_past_4_weeks'] = $this->input->post('clam_in_past_4_weeks');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}

			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_7', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_10_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 9;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_lebensqualitat_7 = $this->patient->get_app_lebensqualitat_7($id);

		if ($app_lebensqualitat_7) {
			$is_lock = true;	
			$data = $app_lebensqualitat_7;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_7', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_11_post() {

		$data = array();
		$input_step11 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 10;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('full_energy_in_past_4_weeks', 'Full of energy in past 4 weeks', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_8', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['full_energy_in_past_4_weeks'] = $this->input->post('full_energy_in_past_4_weeks');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_8($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['full_energy_in_past_4_weeks'] = $this->input->post('full_energy_in_past_4_weeks');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}

			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_8', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_11_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 10;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_lebensqualitat_8 = $this->patient->get_app_lebensqualitat_8($id);

		if ($app_lebensqualitat_8) {
			$is_lock = true;	
			$data = $app_lebensqualitat_8;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_8', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_12_post() {

		$data = array();
		$input_step12 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 11;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('discourage_sad_in_past_4_weeks', 'Dismissed and sad in past 4 weeks', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_9', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['discourage_sad_in_past_4_weeks'] = $this->input->post('discourage_sad_in_past_4_weeks');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_9($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['discourage_sad_in_past_4_weeks'] = $this->input->post('discourage_sad_in_past_4_weeks');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_9', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_12_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 11;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_lebensqualitat_9 = $this->patient->get_app_lebensqualitat_9($id);

		if ($app_lebensqualitat_9) {
			$is_lock = true;	
			$data = $app_lebensqualitat_9;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_9', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_13_post() {

		$data = array();
		$input_step13 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 12;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('contact_with_others', 'Contact with other people in past 4 weeks', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkMonthlyRecordExist('tbl_app_lebensqualitat_10', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Sie haben diesen Fragebogen diesen Monat bereits ausgefüllt';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['contact_with_others'] = $this->input->post('contact_with_others');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_lebensqualitat_10($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['contact_with_others'] = $this->input->post('contact_with_others');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_10', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_13_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 12;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_lebensqualitat_10 = $this->patient->get_app_lebensqualitat_10($id);

		if ($app_lebensqualitat_10) {
			$is_lock = true;	
			$data = $app_lebensqualitat_10;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkMonthlyScreenLocked('tbl_app_lebensqualitat_10', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_14_post() {

		$data = array();
		$input_step14 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			$token = $_SERVER['HTTP_TOKEN'];
			$this->form_validation->set_rules('sleep_timing_from', 'Usual Bed times', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));
			$this->form_validation->set_rules('sleep_timing_to', 'Usual Bed times', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_1', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['sleep_timing_from'] = $this->input->post('sleep_timing_from');
					$saveData['sleep_timing_to'] = $this->input->post('sleep_timing_to');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_1($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['sleep_timing_from'] = $this->input->post('sleep_timing_from');
						$data['sleep_timing_to'] = $this->input->post('sleep_timing_to');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_1', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_14_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 13;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_schlafqualitat_1 = $this->patient->get_app_schlafqualitat_1($id);

		if ($app_schlafqualitat_1) {
			$is_lock = true;	
			$data = $app_schlafqualitat_1;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}
		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_1', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_15_post() {

		$data = array();
		$input_step15 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 14;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('time_takes_to_fall_asleep', 'Minutes taken to fall asleep', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];

				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_2', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['time_takes_to_fall_asleep'] = $this->input->post('time_takes_to_fall_asleep');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_2($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['time_takes_to_fall_asleep'] = $this->input->post('time_takes_to_fall_asleep');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_2', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_15_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 14;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		$app_schlafqualitat_2 = $this->patient->get_app_schlafqualitat_2($id);

		if ($app_schlafqualitat_2) {
			$is_lock = true;	
			$data = $app_schlafqualitat_2;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_2', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_16_post() {

		$data = array();
		$input_step16 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 15;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			$this->form_validation->set_rules('avg_hours_sleep_at_night', 'Average sleep at night', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				// $user_exists = $this->user->getUserByToken($token);
				$id = $user_exists['id'];
				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_3', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['avg_hours_sleep_at_night'] = $this->input->post('avg_hours_sleep_at_night');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_3($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['avg_hours_sleep_at_night'] = $this->input->post('avg_hours_sleep_at_night');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}

				
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_3', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_16_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 15;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_schlafqualitat_3 = $this->patient->get_app_schlafqualitat_3($id);

		if ($app_schlafqualitat_3) {
			$is_lock = true;	
			$data = $app_schlafqualitat_3;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_3', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_17_post() {

		$data = array();
		$input_step17 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 16;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('cannot_sleep_through', "Can't sleep through", 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_4', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['cannot_sleep_through'] = $this->input->post('cannot_sleep_through');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_4($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['cannot_sleep_through'] = $this->input->post('cannot_sleep_through');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_4', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_17_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 16;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_schlafqualitat_4 = $this->patient->get_app_schlafqualitat_4($id);

		if ($app_schlafqualitat_4) {
			$is_lock = true;	
			$data = $app_schlafqualitat_4;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_4', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_18_post() {

		$data = array();
		$input_step18 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 17;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('wake_up_early', 'Wake up early', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_5', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['wake_up_early'] = $this->input->post('wake_up_early');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_5($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['wake_up_early'] = $this->input->post('wake_up_early');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_5', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_18_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 17;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		$app_schlafqualitat_5 = $this->patient->get_app_schlafqualitat_5($id);

		if ($app_schlafqualitat_5) {
			$is_lock = true;	
			$data = $app_schlafqualitat_5;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_5', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_19_post() {

		$data = array();
		$input_step19 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 18;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		
		if ($this->input->post()) {
			$token = $_SERVER['HTTP_TOKEN'];
			$this->form_validation->set_rules('wake_up_with_noise', 'Wake up due to sound', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				// $user_exists = $this->user->getUserByToken($token);
				$id = $user_exists['id'];

				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_6', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['wake_up_with_noise'] = $this->input->post('wake_up_with_noise');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_6($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['wake_up_with_noise'] = $this->input->post('wake_up_with_noise');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}

			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_6', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_19_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 18;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		$app_schlafqualitat_6 = $this->patient->get_app_schlafqualitat_6($id);

		if ($app_schlafqualitat_6) {
			$is_lock = true;	
			$data = $app_schlafqualitat_6;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_6', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_20_post() {

		$data = array();
		$input_step20 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 19;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('no_eye_all_night', 'No eye all night', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				// $user_exists = $this->user->getUserByToken($token);
				$id = $user_exists['id'];

				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_7', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['no_eye_all_night'] = $this->input->post('no_eye_all_night');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_7($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['no_eye_all_night'] = $this->input->post('no_eye_all_night');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
			} else {
				$message = validation_errors();
				$success = false;
			}

			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_7', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_20_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 19;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		
		$app_schlafqualitat_7 = $this->patient->get_app_schlafqualitat_7($id);

		if ($app_schlafqualitat_7) {
			$is_lock = true;	
			$data = $app_schlafqualitat_7;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_7', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_21_post() {

		$data = array();
		$input_step21 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 20;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('think_much_sleep_after', 'Think alot about sleep', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				// $user_exists = $this->user->getUserByToken($token);
				$id = $user_exists['id'];

				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_8', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['think_much_sleep_after'] = $this->input->post('think_much_sleep_after');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_8($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['think_much_sleep_after'] = $this->input->post('think_much_sleep_after');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}

			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_8', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_21_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 20;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		

		$app_schlafqualitat_8 = $this->patient->get_app_schlafqualitat_8($id);

		if ($app_schlafqualitat_8) {
			$is_lock = true;	
			$data = $app_schlafqualitat_8;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_8', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_22_post() {

		$data = array();
		$input_step22 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 21;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('afraid_not_able_to_sleep', 'Afraid of not able to sleep', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				// $user_exists = $this->user->getUserByToken($token);
				$id = $user_exists['id'];

				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_9', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['afraid_not_able_to_sleep'] = $this->input->post('afraid_not_able_to_sleep');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_9($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['afraid_not_able_to_sleep'] = $this->input->post('afraid_not_able_to_sleep');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_9', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_22_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 21;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$app_schlafqualitat_9 = $this->patient->get_app_schlafqualitat_9($id);

		if ($app_schlafqualitat_9) {
			$is_lock = true;	
			$data = $app_schlafqualitat_9;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_9', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_23_post() {

		$data = array();
		$input_step23 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 22;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('full_powerful', 'Fully Capable', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				// $user_exists = $this->user->getUserByToken($token);
				$id = $user_exists['id'];

				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_10', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['full_powerful'] = $this->input->post('full_powerful');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_10($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['full_powerful'] = $this->input->post('full_powerful');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}

			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_10', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_23_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 22;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		$app_schlafqualitat_10 = $this->patient->get_app_schlafqualitat_10($id);

		if ($app_schlafqualitat_10) {
			$is_lock = true;	
			$data = $app_schlafqualitat_10;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_10', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_24_post() {

		$data = array();
		$input_step24 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 23;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];
			$this->form_validation->set_rules('taking_sleeping_pills', 'sleeping pills to fall asleep', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));

			if ($this->form_validation->run()) {
			
				// $user_exists = $this->user->getUserByToken($token);
				$id = $user_exists['id'];

				$is_exist_today_data = $this->patient->checkWeeklyRecordExist('tbl_app_schlafqualitat_11', $id);

				if ($is_exist_today_data) {
					$success = true;
					$message = 'Der wöchentliche Fragebogen wurden schon beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['taking_sleeping_pills'] = $this->input->post('taking_sleeping_pills');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_app_schlafqualitat_11($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['afraid_not_able_to_sleep'] = $this->input->post('afraid_not_able_to_sleep');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_11', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_24_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 23;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		$app_schlafqualitat_11 = $this->patient->get_app_schlafqualitat_11($id);

		if ($app_schlafqualitat_11) {
			$is_lock = true;	
			$data = $app_schlafqualitat_11;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkWeeklyScreenLocked('tbl_app_schlafqualitat_11', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_25_post() {

		$data = array();
		$input_step25 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 25;
		$current_screen_number = 24;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		
		if ($this->input->post()) {
			$token = $_SERVER['HTTP_TOKEN'];
			$this->form_validation->set_rules('daily_reminder', 'daily reminder', 'required|trim', array('required' => 'Bitte geben Sie hier eine Uhrzeit ein.'));
			$this->form_validation->set_rules('weekly_reminder', 'weekly reminder', 'required|trim', array('required' => 'Bitte geben Sie hier eine Uhrzeit ein.'));
			$this->form_validation->set_rules('monthly_reminder', 'monthly reminder', 'required|trim', array('required' => 'Bitte geben Sie hier eine Uhrzeit ein.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				$saveData['patient_id'] = $id;
				$saveData['daily_reminder'] = getDefaultToGMTDate(strtotime($this->input->post('daily_reminder')), 'H:i:s');
				$saveData['weekly_reminder'] = getDefaultToGMTDate(strtotime($this->input->post('weekly_reminder')), 'H:i:s');
				$saveData['monthly_reminder'] = getDefaultToGMTDate(strtotime($this->input->post('monthly_reminder')), 'H:i:s');
				$saveData['daily_reminder_text'] = $this->input->post('daily_reminder');
				$saveData['weekly_reminder_text'] = $this->input->post('weekly_reminder');
				$saveData['monthly_reminder_text'] = $this->input->post('monthly_reminder');
				$check_record_exist = $this->patient->get_patient_reminder($id);

				if ($check_record_exist) {
					$current_date = date('Y-m-d'); 

					if ($check_record_exist->daily_reminder_text != $saveData['daily_reminder_text']) {
						$this->patient->delete_send_notification($id, $current_date, 'Daily');
					}

					if ($check_record_exist->weekly_reminder_text != $saveData['weekly_reminder_text']) {
						$this->patient->delete_send_notification($id, $current_date, 'Weekly');
					}

					if ($check_record_exist->monthly_reminder_text != $saveData['monthly_reminder_text']) {
						$this->patient->delete_send_notification($id, $current_date, 'Monthly');
					}


					$saveData['update_date'] = getDefaultToGMTDate(time());
					$this->patient->update_reminder($saveData, $check_record_exist->id);
					$success = true;
					$message = 'Reminder updated.';
				} else {
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$this->patient->add_reminder($saveData);
					$success = true;
					$message = 'Reminder added.';
				}

				$pData['is_first_visit_complete'] = 'Yes';
				$pData['update_date'] = getDefaultToGMTDate(time());
				$this->user->update_user($id, $pData);

				$data['daily_reminder'] = $this->input->post('daily_reminder');
				$data['weekly_reminder'] = $this->input->post('weekly_reminder');
				$data['monthly_reminder'] = $this->input->post('monthly_reminder');

			} else {
				$message = validation_errors();
				$success = false;
			}


			$response = array('status' => $success, 'message' => $message, 'data' => $data);
			$this->response($response, 200);
		}
	}

	function step_25_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 24;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$patient_reminder = $this->patient->get_patient_reminder($id);
		$is_lock = false;	
		
		if ($patient_reminder) {
			$data = $patient_reminder;
			$success = true;
			$message = "Daten gefunden";
		} else {	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock);
		$this->response($response, 200);
	}

	function step_26_post() {

		$data = array();
		$input_step26 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 25;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		
		if ($this->input->post()) {
			$this->form_validation->set_rules('morning_no_of_passages', 'Number of Passages in Morning', 'required|trim', array('required' => 'Bitte geben Sie die Anzahl Durchgänge MORGENS ein.'));
			$this->form_validation->set_rules('lunch_no_of_passages', 'Number of Passages at Lunch', 'required|trim', array('required' => 'Bitte geben Sie die Anzahl Durchgänge MITTAGS ein.'));
			$this->form_validation->set_rules('evening_no_of_passages', 'Number of Passages in Evening', 'required|trim', array('required' => 'Bitte geben Sie die Anzahl Durchgänge ABENDS ein.'));
			$this->form_validation->set_rules('night_no_of_passages', 'Number of Passages at Night', 'required|trim', array('required' => 'Bitte geben Sie die Anzahl Durchgänge NACHTS ein.'));
			$this->form_validation->set_rules('morning_spray_continuity', 'Spray/Continuity in Morning', 'required|trim', array('required' => 'Bitte geben Sie die Zahl Spraystoß/Durchgang MORGENS ein.'));
			$this->form_validation->set_rules('lunch_spray_continuity', 'Spray/Continuity at Lunch', 'required|trim', array('required' => 'Bitte geben Sie die Zahl Spraystoß/Durchgang MITTAGS ein.'));
			$this->form_validation->set_rules('evening_spray_continuity', 'Spray/Continuity in Evening', 'required|trim', array('required' => 'Bitte geben Sie die Zahl Spraystoß/Durchgang ABENDS ein.'));
			$this->form_validation->set_rules('night_spray_continuity', 'Spray/Continuity at Night', 'required|trim', array('required' => 'Bitte geben Sie die Zahl Spraystoß/Durchgang NACHTS ein.'));
			$this->form_validation->set_rules('morning_total_spraying', 'Total Spraying in Morning', 'required|trim', array('required' => 'Bitte geben Sie die Summe Sprühstöße MORGENS ein.'));
			$this->form_validation->set_rules('lunch_total_spraying', 'Total Spraying at Lunch', 'required|trim', array('required' => 'Bitte geben Sie die Summe Sprühstöße MITTAGS ein.'));
			$this->form_validation->set_rules('evening_total_spraying', 'Total Spraying in Evening', 'required|trim', array('required' => 'Bitte geben Sie die Summe Sprühstöße ABENDS ein.'));
			$this->form_validation->set_rules('night_total_spraying', 'Total Spraying at Night', 'required|trim', array('required' => 'Bitte geben Sie die Summe Sprühstöße NACHTS ein.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkDailyRecordExist('tbl_app_verlaufskontrolle', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Ihr täglicher Fragebogen wurden bereits beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['morning_no_of_passages'] = $this->input->post('morning_no_of_passages');
					$saveData['lunch_no_of_passages'] = $this->input->post('lunch_no_of_passages');
					$saveData['evening_no_of_passages'] = $this->input->post('evening_no_of_passages');
					$saveData['night_no_of_passages'] = $this->input->post('night_no_of_passages');
					$saveData['morning_spray_continuity'] = $this->input->post('morning_spray_continuity');
					$saveData['lunch_spray_continuity'] = $this->input->post('lunch_spray_continuity');
					$saveData['evening_spray_continuity'] = $this->input->post('evening_spray_continuity');
					$saveData['night_spray_continuity'] = $this->input->post('night_spray_continuity');
					$saveData['morning_total_spraying'] = $this->input->post('morning_total_spraying');
					$saveData['lunch_total_spraying'] = $this->input->post('lunch_total_spraying');
					$saveData['evening_total_spraying'] = $this->input->post('evening_total_spraying');
					$saveData['night_total_spraying'] = $this->input->post('night_total_spraying');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_verlaufskontrolle($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['morning_no_of_passages'] = $this->input->post('morning_no_of_passages');
						$data['lunch_no_of_passages'] = $this->input->post('lunch_no_of_passages');
						$data['evening_no_of_passages'] = $this->input->post('evening_no_of_passages');
						$data['night_no_of_passages'] = $this->input->post('night_no_of_passages');
						$data['morning_spray_continuity'] = $this->input->post('morning_spray_continuity');
						$data['lunch_spray_continuity'] = $this->input->post('lunch_spray_continuity');
						$data['evening_spray_continuity'] = $this->input->post('evening_spray_continuity');
						$data['night_spray_continuity'] = $this->input->post('night_spray_continuity');
						$data['morning_total_spraying'] = $this->input->post('morning_total_spraying');
						$data['lunch_total_spraying'] = $this->input->post('lunch_total_spraying');
						$data['evening_total_spraying'] = $this->input->post('evening_total_spraying');
						$data['night_total_spraying'] = $this->input->post('night_total_spraying');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}

			} else {
				$message = validation_errors();
				$success = false;
			}
			$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_verlaufskontrolle', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_26_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 25;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$verlaufskontrolle = $this->patient->get_verlaufskontrolle($id);
		$record_date = '';

		if ($verlaufskontrolle) {
			$is_lock = true;
			$data["id"] = $verlaufskontrolle->id;
	        $data["patient_id"] = $verlaufskontrolle->patient_id;
	        $data["morning_no_of_passages"] = $verlaufskontrolle->morning_no_of_passages;
	        $data["lunch_no_of_passages"] = $verlaufskontrolle->lunch_no_of_passages;
	        $data["evening_no_of_passages"] = $verlaufskontrolle->evening_no_of_passages;
	        $data["night_no_of_passages"] = $verlaufskontrolle->night_no_of_passages;
	        $data["morning_spray_continuity"] = $verlaufskontrolle->morning_spray_continuity;
	        $data["lunch_spray_continuity"] = $verlaufskontrolle->lunch_spray_continuity;
	        $data["evening_spray_continuity"] = $verlaufskontrolle->evening_spray_continuity;
	        $data["night_spray_continuity"] = $verlaufskontrolle->night_spray_continuity;
	        $data["morning_total_spraying"] = $verlaufskontrolle->morning_total_spraying;
	        $data["lunch_total_spraying"] = $verlaufskontrolle->lunch_total_spraying;
	        $data["evening_total_spraying"] = $verlaufskontrolle->evening_total_spraying;
	        $data["night_total_spraying"] = $verlaufskontrolle->night_total_spraying;
	        $data["add_date"] = $verlaufskontrolle->add_date;
			$first_visit = $this->patient->get_verlaufskontrolle($id, true); 	
			$record_date = getGMTDateToLocalDate($first_visit->add_date, 'Y-m-d');

			// $data = $verlaufskontrolle;
			$success = true;
			$message = "Daten gefunden";
		} else {
			$is_lock = false;
			$message = "Daten wurden nicht gefunden";
			$success = true;	
		}


		$last_therapie_access = $user_record['last_therapie_access'];

		if ($record_date) {
			$c_date = date('Y-m-d');
			$datediff = strtotime($c_date) - strtotime($record_date);
			
			if ($datediff < 1) {
				$current_day = 1;
			} else {
				$current_day = round($datediff / (60 * 60 * 24));
			}
		} else {
			$current_day = 1;
		}
		
		
		if ($last_therapie_access == 'Titration') {
			$arztlicher_therapieplan = $this->patient->getPatientArztlicherTherapieplan($id);

			if ($arztlicher_therapieplan) {

	            if ($arztlicher_therapieplan && $arztlicher_therapieplan->dosistyp == 'dosistyp_1') {
	                $dosistyp_1 = $this->patient->getPatientDosistyp_1($arztlicher_therapieplan->id, $current_day);

	                $morgens = $dosistyp_1->morgens_anzahl_durchgange;
	                $mittags = $dosistyp_1->mittags_anzahl_durchgange;
	                $abends = $dosistyp_1->abends_anzahl_durchgange;
	                $nachts = $dosistyp_1->nachts_anzahl_durchgange;
	            } else {
	                $dosistyp_2 = $this->patient->getPatientDosistyp_2($arztlicher_therapieplan->id, $current_day); 
	                $morgens = $dosistyp_2->morgens_anzahl_durchgange;
					$mittags = $dosistyp_2->mittags_anzahl_durchgange;
					$abends = $dosistyp_2->abends_anzahl_durchgange;
					$nachts = $dosistyp_2->nachts_anzahl_durchgange;
	            }
			} else {
				$morgens = '';
				$mittags = '';
				$abends = '';
				$nachts = '';
			}

		} else if ($last_therapie_access == 'Umstellung') {

			$patientTherapyPlanUmstellung = $this->patient->getPatientTherapyPlanUmstellung($id);

			if ($patientTherapyPlanUmstellung) {
				$typUmstellung = $this->patient->getPatientTypUmstellung($patientTherapyPlanUmstellung->id);
				$morgens = $typUmstellung->morgens_anzahl_durchgange;
				$mittags = $typUmstellung->mittags_anzahl_durchgange;
				$abends = $typUmstellung->abends_anzahl_durchgange;
				$nachts = $typUmstellung->nachts_anzahl_durchgange;
			} else {
				$morgens = '';
				$mittags = '';
				$abends = '';
				$nachts = '';
			}

		} else if ($last_therapie_access == 'Folgeverordnung') {
			$folgeverordnung = $this->patient->getPatientThherpieplanFolgeverord($id);

			if ($folgeverordnung) {
				$thherpieplan_folgeverord = $this->patient->getPatientThherpieplanFolgeverordOption($folgeverordnung->id, $current_day);
				//pr($current_day); die;
				if ($thherpieplan_folgeverord) {
					
					$morgens = $thherpieplan_folgeverord->anzahl_spruhstobe_morgens;
					$mittags = $thherpieplan_folgeverord->anzahl_spruhstobe_mittags;
					$abends = $thherpieplan_folgeverord->anzahl_spruhstobe_abends;
					$nachts = $thherpieplan_folgeverord->anzahl_spruhstobe_nachts;
				} else {
					$morgens = '';
					$mittags = '';
					$abends = '';
					$nachts = '';	
				}
			} else {
				$morgens = '';
				$mittags = '';
				$abends = '';
				$nachts = '';
			}

		} else {
			$morgens = '';
			$mittags = '';
			$abends = '';
			$nachts = '';
		}
		// pr($data); die;
		$data['morgens'] = $morgens;
		$data['mittags'] = $mittags;
		$data['abends'] = $abends;
		$data['nachts'] = $nachts;

		$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_verlaufskontrolle', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_27_post() {

		$data = array();
		$input_step27 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 26;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		
//			pr($_POST); die;
		if ($this->input->post()) {
			$id = $user_exists['id'];

			
			$is_exist_today_data = $this->patient->checkDailyRecordExist('tbl_app_weitere_einnahme', $id);

			if ($is_exist_today_data) {
				$success = true;
				$message = 'Ihr täglicher Fragebogen wurden bereits beantwortet';
			} else {

				$saveData['patient_id'] = $id;
				$saveData['add_date'] = getDefaultToGMTDate(time());
				$result = $this->patient->add_weitere_einnahme($saveData);

				if ($result) {
					$childData = array();
					$i = 0;

					foreach ($_POST['info'] as $key => $value) {	
						$childData[$i]['app_weitere_einnahme_id'] = $result;
						$childData[$i]['number_of_passages'] = $value['number_of_passages'];
						$childData[$i]['spray'] = $value['spray'];
						$childData[$i]['total_spray'] = $value['total_spray'];
						$childData[$i]['time'] = $value['time'];
						$i++;
					}

					if ($childData) {
						$this->patient->add_weitere_einnahme_content($childData);
					}

					$success = true;
					$message = 'Der heutige Fragebogen wurde bereits übertragen';
					$data = $childData;

				} else {
					$success = false;
					$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';
				}
			}

			$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_weitere_einnahme', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}
	

	function step_27_data_get() {
		$data = array();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];
		$weitere_einnahme = $this->patient->get_weitere_einnahme($id);

		if ($weitere_einnahme) {
			$childData = $this->patient->get_weitere_einnahme_content($weitere_einnahme->id);

			if ($childData) {
				$data = $childData;
			}
			$success = true;
			$message = "Daten gefunden";
		} else {	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_weitere_einnahme', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_28_medicine_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$is_selected = '';
		$_GET = $this->get();
		$current_screen_number = 27;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);

		if ($user_record) {

			$user_id = $user_record['id'];
			
		} else {
			$message = "Daten wurden nicht gefunden";
			$success = false;	
			$data = '';
		}

		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);
	}

	function step_28_post() {

		$data = array();
		$input_step28 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 27;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);

		if ($this->input->post()) {
			
			//$this->form_validation->set_rules('medicine_name', 'SCHMERZMEDIKAMENT', 'trim|required');
			$this->form_validation->set_rules('further_morning_pain_drug', 'Weitere Morgens Schmerzmittel', 'trim|required', array('required' => 'Bitte geben Sie die Dosis MORGENS ein.'));
			$this->form_validation->set_rules('further_lunch_pain_drug', 'Weitere Mittags Schmerzmittel', 'trim|required', array('required' => 'Bitte geben Sie die Dosis MITTAGS ein.'));
			$this->form_validation->set_rules('further_evening_pain_drug', 'Weitere Abends Schmerzmittel', 'trim|required', array('required' => 'Bitte geben Sie die Dosis ABENDS ein.'));
			$this->form_validation->set_rules('further_night_pain_drug', 'Weitere Nachts Schmerzmittel', 'trim|required', array('required' => 'Bitte geben Sie die Dosis NACHTS ein.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];

				
				// $is_exist_today_data = $this->patient->checkDailyRecordExist('tbl_app_weitere_schmerzmedikamente', $id);
				
				/*if ($is_exist_today_data) {
					$success = true;
					$message = 'Ihr täglicher Fragebogen wurden bereits beantwortet';
				} else {*/
					$saveData['patient_id'] = $id;
					$saveData['medicine_name'] = $this->input->post('medicine_name');
					$saveData['further_morning_pain_drug'] = $this->input->post('further_morning_pain_drug');
					$saveData['further_lunch_pain_drug'] = $this->input->post('further_lunch_pain_drug');
					$saveData['further_evening_pain_drug'] = $this->input->post('further_evening_pain_drug');
					$saveData['further_night_pain_drug'] = $this->input->post('further_night_pain_drug');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_weitere_schmerzmedikamente($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['medicine_name'] = $this->input->post('medicine_name');
						$data['further_morning_pain_drug'] = $this->input->post('further_morning_pain_drug');
						$data['further_lunch_pain_drug'] = $this->input->post('further_lunch_pain_drug');
						$data['further_evening_pain_drug'] = $this->input->post('further_evening_pain_drug');
						$data['further_night_pain_drug'] = $this->input->post('further_night_pain_drug');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				// }

			} else {
				$message = validation_errors();
				$success = false;
			}

			$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_weitere_schmerzmedikamente', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_28_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 27;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$weitere_schmerzmedikamente = $this->patient->get_weitere_schmerzmedikamente($id);

		if ($weitere_schmerzmedikamente) {
			$is_lock = true;
			$users_data['user_id'] = $weitere_schmerzmedikamente->patient_id;
			$users_data['medicine_name'] = $weitere_schmerzmedikamente->medicine_name;
			$users_data['further_morning_pain_drug'] = $weitere_schmerzmedikamente->further_morning_pain_drug;
			$users_data['further_lunch_pain_drug'] = $weitere_schmerzmedikamente->further_lunch_pain_drug;
			$users_data['further_evening_pain_drug'] = $weitere_schmerzmedikamente->further_evening_pain_drug;
			$users_data['further_night_pain_drug'] = $weitere_schmerzmedikamente->further_night_pain_drug;
			$users_data['is_initial_visit'] = false;	
		} else {
			$users_data['user_id'] = $id;
			$users_data['medicine_name'] = '';
			$users_data['further_morning_pain_drug'] = '';
			$users_data['further_lunch_pain_drug'] = '';
			$users_data['further_evening_pain_drug'] = '';
			$users_data['further_night_pain_drug'] = '';	
			$is_lock = false;
		}

		$schmerzstarke = $this->patient->get_schmerzstarke_1($id);

		if ($schmerzstarke) {
			$users_data['is_initial_visit'] = false;
		} else {
			$users_data['is_initial_visit'] = true;		
		}


		$data = $users_data;

		$begleitmedikation_data = $this->user->getbegleitmedikationDataByPatientId($id);
		//pr($begleitmedikation_data); die;
		$medicines = array();

		if ($begleitmedikation_data) {	
			//$data_records = $this->user->getbegleitmedikation_1DataByPatientId($id);
			// pr($data_records); die;
			foreach ($begleitmedikation_data as $key => $value) {
				$medicines[$key]['medicine_name'] = $value->handelsname;

				if ($weitere_schmerzmedikamente) {

					if ($value->handelsname == $weitere_schmerzmedikamente->medicine_name) {
						$medicines[$key]['is_selected'] = true;
					} else {
						$medicines[$key]['is_selected'] = false;
					}
				} else {
					$medicines[$key]['is_selected'] = false;
				}
				
				$medicines[$key]['detail']['morgens']['value'] = $value->morgens;
				$medicines[$key]['detail']['morgens']['unit'] = $value->eiheit;
				$medicines[$key]['detail']['mittags']['value'] = $value->mittags;
				$medicines[$key]['detail']['mittags']['unit'] = $value->eiheit;
				$medicines[$key]['detail']['abends']['value'] = $value->abends;
				$medicines[$key]['detail']['abends']['unit'] = $value->eiheit;
				$medicines[$key]['detail']['zur_nacht']['value'] = $value->zur_nacht;
				$medicines[$key]['detail']['zur_nacht']['unit'] = $value->eiheit;
			}
		}
		$data['medicines'] = $medicines;
		$success = true;
		$message = 'Daten gefunden';

		$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_weitere_schmerzmedikamente', $id);

		// $is_lock = $check_screen_locked['is_lock'];
		$is_lock = false;
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function step_29_post() {

		$data = array();
		$input_step29 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 28;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		
		if ($this->input->post()) {
			$token = $_SERVER['HTTP_TOKEN'];
			$this->form_validation->set_rules('pain_after_30_min_after_thc', 'sleeping pills to fall asleep', 'required|trim', array('required' => 'Bitte wählen Sie eine Option aus.'));
			$this->form_validation->set_rules('pain_bearable_after_cannaxan_thc', 'IS THE PAIN BEARABLE?', 'required|trim', array('required' => 'Bitte wählen Sie JA oder NEIN aus.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				
				$is_exist_today_data = $this->patient->checkDailyRecordExist('tbl_app_schmerzstarke_1', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Ihr täglicher Fragebogen wurden bereits beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['pain_after_30_min_after_thc'] = $this->input->post('pain_after_30_min_after_thc');
					$saveData['pain_bearable_after_cannaxan_thc'] = $this->input->post('pain_bearable_after_cannaxan_thc');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_schmerzstarke_1($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['pain_after_30_min_after_thc'] = $this->input->post('pain_after_30_min_after_thc');
						$data['pain_bearable_after_cannaxan_thc'] = $this->input->post('pain_bearable_after_cannaxan_thc');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}


			} else {
				$message = validation_errors();
				$success = false;
			}

			$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_schmerzstarke_1', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_29_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 28;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$schmerzstarke = $this->patient->get_schmerzstarke_1($id);

		if ($schmerzstarke) {
			$is_lock = true;	
			$data = $schmerzstarke;
			$success = true;
			$message = "Data Found";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_schmerzstarke_1', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);

		$this->response($response, 200);
	}

	function step_30_post() {

		$data = array();
		$input_step30 =array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 29;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_exists = $this->getUserDetailByToken($token);
		if ($this->input->post()) {
			
			$token = $_SERVER['HTTP_TOKEN'];


			$this->form_validation->set_rules('morning_breakthrough_pain', 'Breakthrough Pain in morning', 'required|trim', array('required' => 'Bitte geben Sie den Schmerz MORGENS an.'));
			$this->form_validation->set_rules('lunch_breakthrough_pain', 'Breakthrough Pain at lunch', 'required|trim', array('required' => 'Bitte geben Sie den Schmerz MITTAGS an.'));
			$this->form_validation->set_rules('evening_breakthrough_pain', 'Breakthrough Pain in evening', 'required|trim', array('required' => 'Bitte geben Sie den Schmerz ABENDS an.'));
			$this->form_validation->set_rules('night_breakthrough_pain', 'Breakthrough Pain at night', 'required|trim', array('required' => 'Bitte geben Sie den Schmerz NACHTS an.'));

			if ($this->form_validation->run()) {
				$id = $user_exists['id'];
				$is_exist_today_data = $this->patient->checkDailyRecordExist('tbl_app_schmerzstarke_2', $id);
				
				if ($is_exist_today_data) {
					$success = true;
					$message = 'Ihr täglicher Fragebogen wurden bereits beantwortet';
				} else {
					$saveData['patient_id'] = $id;
					$saveData['morning_breakthrough_pain'] = $this->input->post('morning_breakthrough_pain');
					$saveData['lunch_breakthrough_pain'] = $this->input->post('lunch_breakthrough_pain');
					$saveData['evening_breakthrough_pain'] = $this->input->post('evening_breakthrough_pain');
					$saveData['night_breakthrough_pain'] = $this->input->post('night_breakthrough_pain');
					$saveData['add_date'] = getDefaultToGMTDate(time());
					$result = $this->patient->add_schmerzstarke_2($saveData);

					if ($result) {
						$success = true;
						$message = 'Der heutige Fragebogen wurde bereits übertragen';
						$data['morning_breakthrough_pain'] = $this->input->post('morning_breakthrough_pain');
						$data['lunch_breakthrough_pain'] = $this->input->post('lunch_breakthrough_pain');
						$data['evening_breakthrough_pain'] = $this->input->post('evening_breakthrough_pain');
						$data['night_breakthrough_pain'] = $this->input->post('night_breakthrough_pain');

					} else {
						$success = false;
						$message = 'Technischer Fehler. Bitte versuchen Sie es erneut.';  
					}
				}
				
			} else {
				$message = validation_errors();
				$success = false;
			}


			$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_schmerzstarke_2', $id);

			$is_lock = $check_screen_locked['is_lock'];
			$unlock_date = $check_screen_locked['unlock_date'];

			$response = array('status' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
			$this->response($response, 200);
		}
	}

	function step_30_data_get() {

		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$current_screen_number = 29;
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		$id = $user_record['id'];

		$schmerzstarke = $this->patient->get_schmerzstarke_2($id);

		if ($schmerzstarke) {
			$is_lock = true;	
			$data = $schmerzstarke;
			$success = true;
			$message = "Data Found";
		} else {
			$is_lock = false;	
			$message = "Daten wurden nicht gefunden";
			$success = false;	
		}

		$check_screen_locked = $this->patient->checkDailyScreenLocked('tbl_app_schmerzstarke_2', $id);

		$is_lock = $check_screen_locked['is_lock'];
		$unlock_date = $check_screen_locked['unlock_date'];

		$response = array('success' => $success, 'message' => $message, 'data' => $data, 'utc_datetime' => getDefaultToGMTTime(time()), 'is_lock' => $is_lock, 'unlock_timestamp'=> $unlock_date);
		$this->response($response, 200);
	}

	function postalcodes_get() {
		$data = array();
		$success = '';
		$message = '';
		$_GET = $this->get();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		//pr($user_record); die;

		if ($user_record) {
			
			$plz_records = $this->user->getPlzRecords();
			$data['plz'] = $plz_records;
			$message = 'Data Found';
			$success = true;
		} else {
			$message = 'Daten wurden nicht gefunden';
			$success = false;
		}


		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);
	}

	function ort_post() {
		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		//pr($user_record); die;

		if ($this->input->post()) {
			//pr($_POST); die;

			$this->form_validation->set_rules('plz', 'PLZ', 'trim|required', array('required' => 'Bitte geben Sie die PLZ ein.'));

			if ($this->form_validation->run()) {
				
				$plz = $this->input->post('plz');
				
				if ($user_record) {
					
					$ort_data = $this->user->getOrtByPlz($plz);

					if ($ort_data) {
						$message = 'Data found';
						$success = true;
						$data['ort'] = $ort_data;
					} else {
						$message = 'Daten wurden nicht gefunden';
						$success = false;
					}
					//pr($ort_data); die;
				} else {
					$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch mal...';
					$success = false;
				}
			} else {
				$message = validation_errors();
				$success = false;
			}

		}

		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);

	}

	function strabe_post() {
		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		//pr($user_record); die;

		if ($this->input->post()) {

			$this->form_validation->set_rules('plz', 'PLZ', 'trim|required', array('required' => 'Bitte geben Sie die PLZ ein.'));
			$this->form_validation->set_rules('ort', 'ORT', 'trim|required', array('required' => 'Bitte geben Sie die ORT ein.'));

			if ($this->form_validation->run()) {
				
				$plz = $this->input->post('plz');
				$ort = $this->input->post('ort');
				
				if ($user_record) {
					
					$records[] = $this->user->getStrabeByPlzandOrt($plz, $ort);
					//pr($records); die;

					if ($records) {
						$message = 'Data found';
						$success = true;
						$data['strabe'] = $records;
						//$data['id'] = $records->id;
					} else {
						$message = 'Daten wurden nicht gefunden';
						$success = false;
					}
					//pr($ort_data); die;
				} else {
					$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch mal...';
					$success = false;
				}
			} else {
				$message = validation_errors();
				$success = false;
			}

		}

		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);

	}

	function apotheken_post() {
		$data = array();
		$success = '';
		$message = '';
		$_POST = $this->post();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);
		//pr($user_record); die;

		if ($this->input->post()) {

			$this->form_validation->set_rules('apotheken_id', 'Apotheken Id', 'trim|required', array('required' => 'Bitte geben Sie die Apotheken Id ein.'));
			
			if ($this->form_validation->run()) {
				
				$apotheken_id = $this->input->post('apotheken_id');
								
				if ($user_record) {
					
					$records = $this->user->getapothekenByid($apotheken_id);
					//pr($records); die;

					if ($records) {
						$message = 'Daten gefunden';
						$success = true;
						$data['record'] = $records;
						//$data['id'] = $records->id;
					} else {
						$message = 'Daten wurden nicht gefunden';
						$success = false;
					}
				} else {
					$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch mal...';
					$success = false;
				}
			} else {
				$message = validation_errors();
				$success = false;
			}

		}

		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);

	}

	function versandapotheke_get() {

		$data = array();
		$success = '';
		$message = '';
		$_GET = $this->get();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);

		if ($user_record) {

			$online_stores = $this->patient->getVersandapothekeList();

			if ($online_stores) {
				$message = 'Daten gefunden';
				$success = true;
				$data['online_stores'] = $online_stores;
			} else {
				$message = 'Daten wurden nicht gefunden';
				$success = false;
			}
			
		} else {
			$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch mal...';
			$success = false;
		}

		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);
	}

	function versandapotheke_store_details_get() {

		$data = array();
		$success = '';
		$message = '';
		$_GET = $this->get();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);

		if ($user_record) {

			$store_id = $this->input->get('store');
			$store_detail = $this->patient->getVersandapothekeListById($store_id);

			if ($store_detail) {
				$message = 'Daten gefunden';
				$success = true;
				$data['store_detail'] = $store_detail;
			} else {
				$message = 'Daten wurden nicht gefunden';
				$success = false;
			}
			
		} else {
			$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch mal...';
			$success = false;
		}
		
		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);
	}

	function suche_nach_bundesland_get() {
		$data = array();
		$success = '';
		$message = '';
		$GET = $this->get();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);

		if ($user_record) {
			$bundesland = $this->patient->getAllBundesland();

			if ($bundesland) {
				$message = 'Daten gefunden';
				$success = true;
				$data['bundesland'] = $bundesland;
			} else {
				$message = 'Daten wurden nicht gefunden';
				$success = false;
			}

		} else {
			$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch mal...';
			$success = false;
		}
		
		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);
	}

	function suche_nach_bundesland_list_get() {

		$data = array();
		$success = '';
		$message = '';
		$_GET = $this->get();
		$this->check_token_exists_in_header();
		$token = $_SERVER['HTTP_TOKEN'];
		$user_record = $this->getUserDetailByToken($token);

		if ($user_record) {

			$bundesland_name = $this->input->get('bundesland');
			//pr($bundesland_name); die;

			$offline_stores = $this->patient->getBundeslandStoresList($bundesland_name);

			if ($offline_stores) {
				$message = 'Daten gefunden';
				$success = true;
				$data['offline_stores'] = $offline_stores;
			} else {
				$message = 'Daten wurden nicht gefunden';
				$success = false;
			}
			
		} else {
			$message = 'Etwas ist schief gegangen! Bitte versuchen Sie es noch mal...';
			$success = false;
		}

		$response = array('success' => $success, 'message' => $message, 'data' => $data);
		$this->response($response, 200);
	}
}

