<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/rest/REST_Controller.php';
class Contact_us extends REST_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('api/contactus_model','contactus');
	}

	public function add_contactUs_queries_post(){
        $data = array();
		$success = 'error';
		$message = '';
		$_POST = $this->post();

		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('description', 'Description', 'trim|required');
			
			if ($this->form_validation->run()) {
				
				$insert_data['name'] = $this->input->post('name');
				$insert_data['email'] = $this->input->post('email');
				$insert_data['message'] = $this->input->post('description');
				$insert_data['add_date'] = getDefaultToGMTDate(time(),'Y-m-d H:i:s');
				$insert_id = $this->contactus->add_contact_queries($insert_data);

				if($insert_id) {
					$success = 'success';
                  	$message ='Your Query send to admin successfully.';
				  	$data['email'] = $insert_data['email']; 
			    } else { 
                  	$success ='error';
				  	$message = 'Technical error. Please try again.';
	    			$response = array('status'=>$success,'message' => $message, 'data' => $data);
			    }
            } else {
            	$message = str_replace( "\n",'',strip_tags(validation_errors()));
				$success = "error";  
	    		
            }
		}
		$response = array('status'=>$success,'message' => $message,'data' => $data);
	    $this->response($response, 200);	
	}			
	
}