<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/rest/REST_Controller.php';
class Area extends REST_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('api/area_model','area');
	}

	public function get_area_list_post() {
		$data = array();
		$success = 'error';
		$message = '';
		$_POST = $this->post();

		$area_list = $this->area->get_area();
		
		if ($area_list) {
			
			foreach ($area_list as $value) {
				$marker = $this->area->get_marker_by_area_id($value->id);
			    $value->marker = $marker; 
			}	
			
			$success ='success';
		    $message = 'Area found.';
		    $data = $area_list;
		} else {
			$success ='error';
		    $message = 'Area not found.';
		}

		$response = array('status'=>$success, 'message' => $message, 'data' => $data);
	    $this->response($response, 200);
	}
}