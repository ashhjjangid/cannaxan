<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        Parent::__construct();
        $this->load->model('mailsending_model', 'mail_send');
        $this->load->model('subscriber_model', 'subscriber');
    }


	public function index()
	{
		if ($this->session->userdata('user_id')) {
			redirect('patients');
		}
		$output['menu'] = 'Home Page';
		$this->load->view('front/includes/header', $output);
		$this->load->view('front/pages/index');
		$this->load->view('front/includes/footer');
	}

	public function subscribe()
	{

		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			
			if ($this->form_validation->run()) {

				$name = $this->input->post('name');
				$email = $this->input->post('email');
				$is_email_exist = $this->subscriber->get_record_by_email($email);

				if ($is_email_exist) {
					$message = 'You have already subscribed in our portal.';
					$success = false;
				} else {
					$ip_add = $this->input->ip_address();
					$add_date = date('Y-m-d H:i:s');
					$this->load->model('subscriber_model', 'subscriber');

					$insert_id = $this->subscriber->set_subscribers($name, $email, $ip_add, $add_date);

					if($insert_id) {
						$this->mail_send->sendAddSubscriberMail($email,$name);
						$message = 'You have successfully subscribed in our portal.';
						$success = true;
						$data['reset_form'] = true;
					} else {
						$success = false;
						$message = 'Technical error please try again!';						
					}
				}
			} else {
				$success = false;
				$message = validation_errors();
			}
			$data['message'] = $message;
			$data['success'] = $success;
			echo json_encode($data);die;
		}
	}
}
