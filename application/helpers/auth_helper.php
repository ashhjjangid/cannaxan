<?php 


// validate user token and set user session if token is correct
function validate_user_token()
{
    $ci = & get_instance();
    $ci->load->model('rest/Token_model','user_tokens');
    $headers = $ci->input->request_headers();
    
    if(!isset($headers['Authorization']) || !$headers['Authorization']) {
        $ci->output->set_status_header(403);
        header('Content-Type: application/json');
        $response = array('success'=> FALSE, 'message' => 'Token is required');
        echo json_encode($response);die;
    }

    preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches);
    $token = $matches[1]; 

    if (!empty($token)) {
        $user = $ci->user_tokens->get_user_by_token($token);

        if ($user) {
            
            if ($user->expire_date < date("Y-m-d H:i:s")) {
                $ci->output->set_status_header(401);
                $response = array('success'=> FALSE, 'message' => 'Session Expired!!!');
                header('Content-Type: application/json');
                echo json_encode($response);die;
            
            }   else {
                $ci->session->set_userdata('user_id', $user->id);
                return $user;
            }

        } else {
            $ci->output->set_status_header(401);
            $response = array('success'=> FALSE, 'message' => 'Invalid Token.');
            header('Content-Type: application/json');
            echo json_encode($response);die;
        }

    } else {
        $ci->output->set_status_header(403);
        $response = array('success'=> FALSE, 'message' => 'Token is required.');
        header('Content-Type: application/json');
        echo json_encode($response);die;
    }
}

