<?php
function show_limited_text($string, $len = 10)
{
    $string = strip_tags($string);
    if (strlen($string) > $len) {
        $string = mb_substr($string, 0, $len - 3) . "...";
    }

    return $string;
}

if (!function_exists('is_logged_in')) {
    function is_logged_in()
    {
        global $CI;
        $CI = &get_instance();
        if ($CI->session->userdata('admin_id')) {
            return true;
        } else {
            return false;
        }
        return false;
    }
}

if (!function_exists('pr')) {

    function pr($arr)
    {
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
    }
}

if (!function_exists('escape_text')) {
    function escape_text($s = '')
    {
        return str_replace('&#039;', "'", htmlspecialchars_decode(html_entity_decode($s)));
    }
}

function getExpireTime()
{
   return time()+60*60*36;
}

function getSiteOption($key = '', $value = false)
{
    if ($key == '') {
        return false;
    }
    $CI = &get_instance();
    $CI->load->model('admin/website_model', 'website');
    $value = $CI->website->getValueBySlug($key, $value);
    return $value;
}

function getStatisticsOption($key = '', $value = false)
{
    if ($key == '') {
        return false;
    }
    $CI = &get_instance();
    $CI->load->model('admin/statistics_model', 'statistics');
    $value = $CI->statistics->getValueBySlug($key, $value);
    return $value;
}

if (!function_exists('is_admin_logged_in')) {
    function is_admin_logged_in()
    {
        global $CI;
        $CI = &get_instance();
        if ($CI->session->userdata('admin_id')) {
            return true;
        } else {
            return false;
        }
        return false;
    }
}

function getMailConstants()
{
    $constants = [
        'Logo'                => '{{Logo}}',
        'Email_Address'       => '{{Email_Address}}',
        'Subject'             => '{{Subject}}',
        'Website_UR'          => '{{Website_URL}}',
        'Unsubscription_Link' => '{{Unsubscription_Link}}',
    ];
    return $constants;
}

function getImageURL($content)
{
    preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $content, $img);
    $url = $img[1];
    if ($url) {
        return $url;
    } else {
        return false;
    }
}

function get_random_key($len = 30)
{
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $len - 1);
}
function get_number_days_between_dates($start, $end)
{
    $end_time   = strtotime($end);
    $start_time = strtotime($start);
    $datediff   = $end_time - $start_time;

    return round($datediff / (60 * 60 * 24));
}
function add_log_entry($string)
{
    $filename = 'logs/' . date("Y-m-d") . '_complete_log.log';
    $myfile   = fopen($filename, "a");
    fwrite($myfile, "\n" . $string);
    fclose($myfile);
}
function get_next_password_change_date($date = null)
{
    if (!$date) {
        $date = date("Y-m-d H:i:s");
    }

    $next_date = date("Y-m-d H:i:s", strtotime("+1 Month", strtotime($date)));
    return $next_date;
}
function get_value_with_token_multiplier_factor($token_amount)
{
    return $token_amount * pow(10, 18);
}
function get_value_without_token_multiplier_factor($token_amount)
{
    return $token_amount / pow(10, 18);
}
function check_user_access($permission_id, $show_error = true)
{
    $CI = &get_instance();

    $CI->db->where('id', $CI->session->userdata('admin_id'));

    $CI->db->group_start();
    $CI->db->where("FIND_IN_SET($permission_id,tbl_users.permissions) !=", 0);
    $CI->db->or_where("FIND_IN_SET(1,tbl_users.permissions) !=", 0);
    $CI->db->group_end();

    $query  = $CI->db->get('tbl_users');
    $result = $query->row();

    if ($result) {
        return true;

    } else {

        if (!$show_error) {

            return false;
        } else {
            echo "<h2 align='center'>Sorry you are not allowed</h2>";die;
        }
    }

}
function get_hour_string_by_seconds($time)
{
    $hours = floor($time / (60 * 60));
    $time -= $hours * (60 * 60);

    $minutes = floor($time / 60);
    $time -= $minutes * 60;

    $seconds = floor($time);
    $time -= $seconds;

    return "{$hours}h:{$minutes}m:{$seconds}s";
}

function convert_post_from_input_data()
{
    if ($_SERVER['REQUEST_METHOD'] == "GET" || $_SERVER['REQUEST_METHOD'] == "POST") {
        $input     = file_get_contents('php://input');
        $post_data = json_decode($input);
    } else {
        parse_str(file_get_contents("php://input"), $post_data);
    }
    if ($post_data) {

        foreach ($post_data as $key => $value) {
            $_POST[$key] = $value;
        }
    }
}

if (!function_exists('checkRequestedDataExists')) {
    function checkRequestedDataExists($data)
    {
        if (!$data) {
            show_404();
        }
        return true;
    }
}

if (!function_exists('getEmbedVideoURLByVideoURL')) {
    function getEmbedVideoURLByVideoURL($video_url)
    {
        $checkVimeo = checkVimeoVideo($video_url);
        $check      = getDailyMotionId($video_url);
        if ($check) {
            $video_array = explode('/', $video_url);
            $video_id    = end($video_array);
            return "https://www.dailymotion.com/embed/video/" . $video_id;
        } else if ($checkVimeo) {
            $video_array = explode('/', $video_url);
            $video_id    = end($video_array);
            return "https://player.vimeo.com/video/" . $video_id;
        } else {
            $video_array = explode('=', $video_url);
            $video_id    = end($video_array);
            return "https://www.youtube.com/embed/" . $video_id;
        }
    }
}

if (!function_exists('checkVimeoVideo')) {
    function checkVimeoVideo($url)
    {
        $checkString = "/^.+vimeo.com\/?/";
        $m           = preg_match($checkString, $url);
        return $m;
    }
}

if (!function_exists('getDailyMotionId')) {
    function getDailyMotionId($url)
    {
        $checkString = "/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/";
        $m           = preg_match($checkString, $url);
        return $m;
    }
}


function checkRequestedDataExists($data)
{
    if(!$data)
    {
        show_404();
    }
    return true;
}


if (!function_exists('createSlugForTable')) {
    function createSlugForTable($title, $table)
    {
        $slug           = url_title($title);
        $slug           = strtolower($slug);
        $i              = 0;
        $params         = array();
        $params['slug'] = $slug;
        $CI             = &get_instance();
        while ($CI->db->where($params)->get($table)->num_rows()) {
            if (!preg_match('/-{1}[0-9]+$/', $slug)) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace('/[0-9]+$/', ++$i, $slug);
            }
            $params['slug'] = $slug;
        }
        return $slug;
    }

    function json_output($data, $die = true)
    {
        header('Content-Type: application/json');
        echo json_encode($data);

        if ($die) {
            die;
        }
    }

    function encrpty_password($password)
    {
        $salt         = 'CLEAR-Tokens-001';
        $encyPassword = md5(md5($password) . md5($salt));
        return $encyPassword;

    }

    function get_encrypted_password($password)
    {
      $salt = 'CLEAR-Tokens-001';
      $ency_password = md5(md5($password).md5($salt));
      return $ency_password;
    }
    
    function get_referral_code($user_id)
    {
        $CI     = &get_instance();
        $record = get_record($user_id);
        $code   = $record->code;
        if (!$record->code) {

            $code           = get_random_key(10);
            $update['code'] = $code;

            $CI->db->where('id', $user_id);
            $CI->db->set($update);
            $CI->db->update('tbl_users');
        }
        return "https://www.clearunited.com/pages/r/" . $code;
    }

    function get_record($user_id)
    {
        $CI = &get_instance();
        $CI->db->select('tbl_users.code');
        $CI->db->where('tbl_users.id', $user_id);
        $query  = $CI->db->get('tbl_users');
        $result = $query->row();
        return $result;
    }

    function get_user_image($user)
    {

        if (!empty($user->profile_image)) {
            //site_url().'assets/uploads/users/user_'.$user->user_id.'/'.$user->profile_image;
            $image = site_url() . image('uploads/users/user_' . $user->user_id . '/' . $user->profile_image, array(200, 200));
        } else {
            $image = "https://cdn.shopify.com/s/files/1/0089/0594/9231/files/dummyimg.svg?13594";
        }
        //echo $image;
        return $image;
    }
    if (!function_exists('getCategoryname')) {

        function getCategoryname($cat_id)
        {

            $CI = &get_instance();
            $CI->load->model('admin/category_model', 'cat');
            $value = $CI->cat->getCategoryname($cat_id);

            return $value;
        }
    }

    function getQuestionPoint_1($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 5;
            } else if ($option == 2) {
                return 4;
            } else if ($option == 3) {                
                return 3;
            } else if ($option == 4) {
                return 2;
            } else if ($option == 5) {
                return 1;
            }
        }
    }

    function getQuestionPoint_2($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 1;
            } else if ($option == 2) {
                return 2;
            } else if ($option == 3) {                
                return 3;
            }
        }
    }

    function getQuestionPoint_3($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 1;
            } else if ($option == 2) {
                return 2;
            } else if ($option == 3) {                
                return 3;
            }
        }
    }

    function getQuestionPoint_4($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 1;
            } else if ($option == 2) {
                return 2;
            }
        }
    }

    function getQuestionPoint_5($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 1;
            } else if ($option == 2) {
                return 2;
            }
        }
    }

    function getQuestionPoint_6($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 1;
            } else if ($option == 2) {
                return 2;
            }
        }
    }

    function getQuestionPoint_7($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 1;
            } else if ($option == 2) {
                return 2;
            }
        }
    }

    function getQuestionPoint_8($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 4;
            } else if ($option == 2) {
                return 3;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 1;
            }
        }
    }

    function getQuestionPoint_9($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 6;
            } else if ($option == 2) {
                return 5;
            } else if ($option == 3) {                
                return 4;
            } else if ($option == 4) {                
                return 3;
            } else if ($option == 5) {                
                return 2;
            } else if ($option == 6) {                
                return 1;
            }
        }
    }

    function getQuestionPoint_10($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 6;
            } else if ($option == 2) {
                return 5;
            } else if ($option == 3) {                
                return 4;
            } else if ($option == 4) {                
                return 3;
            } else if ($option == 5) {                
                return 2;
            } else if ($option == 6) {                
                return 1;
            }
        }
    }

    function getQuestionPoint_11($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 1;
            } else if ($option == 2) {
                return 2;
            } else if ($option == 3) {                
                return 3;
            } else if ($option == 4) {                
                return 4;
            } else if ($option == 5) {                
                return 5;
            } else if ($option == 6) {                
                return 6;
            }
        }
    }

    function getQuestionPoint_12($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 1;
            } else if ($option == 2) {
                return 2;
            } else if ($option == 3) {                
                return 3;
            } else if ($option == 4) {                
                return 4;
            } else if ($option == 5) {                
                return 5;
            } else if ($option == 6) {                
                return 6;
            }
        }
    }


    function getQuestionPoint_13($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 0;
            } else if ($option == 2) {
                return 1;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 3;
            } else if ($option == 5) {                
                return 4;
            } else {                
                return 0;
            }
        }
    }

    function getQuestionPoint_14($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 0;
            } else if ($option == 2) {
                return 1;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 3;
            } else if ($option == 5) {                
                return 4;
            } else {                
                return 0;
            }
        }
    }

    function getQuestionPoint_15($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 4;
            } else if ($option == 2) {
                return 3;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 1;
            } else if ($option == 5) {                
                return 0;
            } else {                
                return 0;
            }
        }
    }

    function getQuestionPoint_16($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 4;
            } else if ($option == 2) {
                return 3;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 1;
            } else if ($option == 5) {                
                return 0;
            } else {                
                return 0;
            }
        }
    }

    function getQuestionPoint_17($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 4;
            } else if ($option == 2) {
                return 3;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 1;
            } else if ($option == 5) {                
                return 0;
            } else {                
                return 0;
            }
        }
    }

    function getQuestionPoint_18($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 4;
            } else if ($option == 2) {
                return 3;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 1;
            } else if ($option == 5) {                
                return 0;
            } else {                
                return 0;
            }
        }
    }

    function getQuestionPoint_19($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 4;
            } else if ($option == 2) {
                return 3;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 1;
            } else if ($option == 5) {                
                return 0;
            } else {                
                return 0;
            }
        }
    }

    function getQuestionPoint_20($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 4;
            } else if ($option == 2) {
                return 3;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 1;
            } else if ($option == 5) {                
                return 0;
            } else {                
                return 0;
            }
        }
    }

    function getQuestionPoint_21($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 0;
            } else if ($option == 2) {
                return 1;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 3;
            } else if ($option == 5) {                
                return 4;
            } else {                
                return 0;
            }
        }
    }

    function getQuestionPoint_22($option = null)
    {
        if ($option) {

            if ($option == 1) {
                return 4;
            } else if ($option == 2) {
                return 3;
            } else if ($option == 3) {                
                return 2;
            } else if ($option == 4) {                
                return 1;
            } else if ($option == 5) {                
                return 0;
            } else {                
                return 0;
            }
        }
    }

    function sendPushToAndroid($device_token, $data)
    {
        $registrationIds = array($device_token);
        $msg['title'] = $data['notification_title'];
        $msg['body'] = $data['notification_message'];
        $msg['vibrate'] = 1;
        $msg['sound'] = 1;
        $msg['type'] = $data['notification_type'];

        if(isset($data['question_id'])) {
            $msg['question_id'] = $data['question_id'];
        }

        

        $fields = array(
            'registration_ids' => $registrationIds,
            'data' => $msg,
            'notification' => $msg
        );

        $headers = array(
            'Authorization: key= AAAAj9PTsS8:APA91bHuEPD59gNb8glD32KY130sPZhIR0yXZRIG1HbKjKZQTgnmPk1s08xVmXm_9N_SwWjfi4NY-K5oO6KtdCtKFJI24l4jaxHoiaqtikYXxmqF9_f6BQesWpqjD-hAk2nfsevgXUMz',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        pr($result); 
        curl_close( $ch );
    }
}
