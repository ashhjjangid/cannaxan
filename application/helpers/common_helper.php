<?php

function __construct()
{
    global $URI, $CFG, $IN;
    $ci = get_instance();
    $ci->load->config('config');
    setSiteConfigData();
    setMemberConfigData();
    setLocalTimeZone();
}

function setSiteConfigData()
{
    $this->config->set_item('per_page', 20);
    $this->config->set_item('per_page_front', 4);
}

function setMemberConfigData()
{
    if ($this->session->userdata('user_id')) {
        $userData = $this->getUserDataById($this->session->userdata('user_id'));
        if ($userData) {
            $this->config->set_item('first_name', $userData->first_name);
        }
    }
}

function setLocalTimeZone()
{
    /*if (!$this->session->userdata('local_time_zone')) {

        $timezone = 'Asia/Kolkata';

        $this->session->set_userdata('local_time_zone', $timezone);

    }*/
    $timezone = 'Europe/Berlin';
    date_default_timezone_set($timezone);
}

function getDefaultToGMTTime($time)
{
    $gmtTime = local_to_gmt($time);
    return $gmtTime;
}

function getDefaultToGMTDate($time, $format = 'Y-m-d H:i:s A')
{
    $gmtTime = local_to_gmt($time);
    return date($format, $gmtTime);
}

function getGMTDateToLocalDate($date, $format = 'Y-m-d H:i:s')
{
    //return $date;

    $date = new DateTime($date, new DateTimeZone('GMT'));
    $CI   = &get_instance();
    // $CI->session->userdata('local_time_zone');
    $date->setTimeZone(new DateTimeZone('Europe/Berlin'));
    return $date->format($format);
}

function getGMTDateToLocalMinutes($date, $format = 'H:i')
{
    //return $date;

    $date = new DateTime($date, new DateTimeZone('GMT'));
    $CI   = &get_instance();
    // $CI->session->userdata('local_time_zone');
    $date->setTimeZone(new DateTimeZone('Europe/Berlin'));
    return $date->format($format);
}

function showLimitedText($string, $len)
{
    $string = strip_tags($string);
    if (strlen($string) > $len) {
        $string = mb_substr($string, 0, $len - 3) . "...";
    }

    return $string;
}

function getUserDataById($user_id)
{
    $this->db->where('id', $user_id);
    $query  = $this->db->get('tbl_users');
    $result = $query->row();
    return $result;
}

if (!function_exists('checkLoginAdminStatus')) {
    function checkLoginAdminStatus()
    {
        $CI = &get_instance();
        $user = getLoginAdmin();
        if ($user) {
            return true;
        } else {
            $CI->session->sess_destroy();
            redirect('admin/login');
            return false;
        }
    }
}


function getLoginAdmin()
{
    $CI = &get_instance();
    $userId = $CI->session->userdata('admin_id');
    $CI->db->where('id', $userId);
    $CI->db->where('status', 'Active');
    $query  = $CI->db->get('tbl_users');
    $result = $query->row();
    return $result;
}

function checkAdminLogin()
{
    $CI = &get_instance();
    if ($CI->session->userdata('admin_id')) {
        return true;
    } else {

        $data['success']    = false;
        $data['message']    = 'Please login first';
        $data['error_type'] = 'auth';
        redirect('admin/login');
        echo json_encode($data);die;

    }
}

function check_login_admin_status()
{
    $user = get_login_admin();
    if ($user) {
        return true;
    } else {
        $this->session->sess_destroy();
        redirect('admin/login');
        return false;
    }

}

function get_login_admin()
{
    $CI     = &get_instance();
    $userId = $CI->session->userdata('admin_id');
    $CI->db->where('id', $userId);
    $CI->db->where('status', 'Active');
    $query  = $CI->db->get('tbl_users');
    $result = $query->row();
    return $result;
}

function getOptionValue($slug = '', $language_abbr = '')
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->where('tbl_website_setting.slug', $slug);
    if ($language_abbr) {
        $CI->db->where('tbl_website_setting.language_abbr', $language_abbr);
    }

    $query  = $CI->db->get('tbl_website_setting');
    $result = $query->row();
    if ($result) {
        return $result->value;
    }

}

function checkUserLogin()
{

    $CI = &get_instance();
    if ($CI->session->userdata('user_id')) {
        return true;
    } else {

        $data['success']    = false;
        $data['message']    = 'Please login first';
        $data['error_type'] = 'auth';

        echo json_encode($data);die;

    }
}

function checkUseAlreadyLogin()
{
    $CI     = &get_instance();
    $userId = $CI->session->userdata('admin_id');
    if ($userId) {
        redirect('demo');
    } else {
        return false;
    }
}
