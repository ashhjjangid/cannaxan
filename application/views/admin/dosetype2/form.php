
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js">
</script>
<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px" >
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/dosistypII')?>" class="tooltips" data-original-title="List of DosisTyp" data-placement="top" data-container="body">DosisTyp List
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <?php if($id != '') { ?>
        <?php echo $tag_name; ?>
        <?php } else { ?>
        Add New DosisTypII
        <?php } ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/dosistypII'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form', 'autocomplete' => 'off'));?>
        <div class="form-body">
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Tag Name
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Tag Name', 'id' => "tag_name", 'name' => "tag_name", 'class' => "form-control", 'value' => "$tag_name")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          </div>          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Morgens Anzahl Durchgänge
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Morgens Anzahl Durchgänge', 'id' => "morgens_anzahl_durchgange", 'name' => "morgens_anzahl_durchgange", 'class' => "form-control", 'value' => "$morgens_anzahl_durchgange")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div> 
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Morgens Anzahl SPS
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Morgens Anzahl SPS', 'id' => "morgens_anzahl_sps", 'name' => "morgens_anzahl_sps", 'class' => "form-control", 'value' => "$morgens_anzahl_sps")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Mittags Anzahl Durchgänge
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Mittags Anzahl Durchgänge', 'id' => "mittags_anzahl_durchgange", 'name' => "mittags_anzahl_durchgange", 'class' => "form-control", 'value' => "$mittags_anzahl_durchgange")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>        
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Mittags Anzahl SPS
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Mittags Anzahl SPS', 'id' => "mittags_anzahl_sps", 'name' => "mittags_anzahl_sps", 'class' => "form-control", 'value' => "$mittags_anzahl_sps")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Abends Anzahl Durchgänge
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Abends Anzahl Durchgänge', 'id' => "abends_anzahl_durchgange", 'name' => "abends_anzahl_durchgange", 'class' => "form-control", 'value' => "$abends_anzahl_durchgange")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Abends Anzahl SPS
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Abends Anzahl SPS', 'id' => "abends_anzahl_sps", 'name' => "abends_anzahl_sps", 'class' => "form-control", 'value' => "$abends_anzahl_sps")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Nachts Anzahl Durchgänge
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Nachts Anzahl Durchgänge', 'id' => "nachts_anzahl_durchgange", 'name' => "nachts_anzahl_durchgange", 'class' => "form-control", 'value' => "$nachts_anzahl_durchgange")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Nachts Anzahl SPS
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Nachts Anzahl SPS', 'id' => "nachts_anzahl_sps", 'name' => "nachts_anzahl_sps", 'class' => "form-control", 'value' => "$nachts_anzahl_sps")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">THC morgens [mg]
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben THC morgens [mg]', 'id' => "thc_morgens_mg", 'name' => "thc_morgens_mg", 'class' => "form-control", 'value' => "$thc_morgens_mg")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">THC mittags [mg]
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben THC mittags [mg]', 'id' => "thc_mittags_mg", 'name' => "thc_mittags_mg", 'class' => "form-control", 'value' => "$thc_mittags_mg")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">THC abends [mg]
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben THC abends [mg]', 'id' => "thc_abends_mg", 'name' => "thc_abends_mg", 'class' => "form-control", 'value' => "$thc_abends_mg")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">THC nachts [mg]
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben THC nachts [mg]', 'id' => "thc_nachts_mg", 'name' => "thc_nachts_mg", 'class' => "form-control", 'value' => "$thc_nachts_mg")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Summe THC [mg]
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Summe THC [mg]', 'id' => "summe_thc_mg", 'name' => "summe_thc_mg", 'class' => "form-control", 'value' => "$summe_thc_mg")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Display Order
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Eingeben Display Order', 'id' => "display_order", 'name' => "display_order", 'class' => "form-control", 'value' => "$display_order")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>           
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Status
              <span style="color:red">
              </span>
            </label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input type="radio" name="status" id="status_active" class="md-radiobtn" value="Active"
                  <?php echo ($status == 'Active')?'checked':''; ?>>
                  <label for="status_active">
                    <span class="inc">                    
                    </span>
                    <span class="check">
                    </span>
                    <span class="box">
                    </span>Active
                  </label>
              </div>
              <div class="md-radio has-error">
                <input type="radio" name="status" id="status_inactive" class="md-radiobtn" value="Inactive"
                  <?php echo ($status == 'Inactive')?'checked':''; ?>>
                  <label for="status_inactive">
                    <span class="inc">
                    </span>
                    <span class="check">
                    </span>
                    <span class="box">
                    </span>Inactive
                  </label>
              </div>
            </div>
            </div>          
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/dosistypII'); ?>" class="btn default">Cancel
                </a>
              </div>
            </div>
          </div>
        </div>
        </form>
    </div>
  </div>
</div>
</div>