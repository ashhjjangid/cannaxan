<?php
	if (isset($message) && $message) {
		$alert = ($success)? 'alert-success':'alert-danger';
		echo '<div class="alert ' . $alert . '">' . $message . '</div>';
	}
?>
<div class="portlet light" style="height:45px">
	<div class="row">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
				<i class="fa fa-arrow-right"></i>
			</li>

			<li>
				<a href="<?php echo site_url('admin/nebenwirkung')?>" class="tooltips" data-original-title="List of nebenwirkung" data-placement="top" data-container="body">List of nebenwirkung</a>
				<i class="fa fa-arrow-right"></i>
			</li>

			<li>
				<input type="hidden" name="" id="ids" value="<?php echo $id; ?>">

				<?php if ($id != '') { ?>
				 	<?php $fa_icon  = 'fa-pencil-square-o'; ?>
				 	<?php echo $record->nebenwirkung_name; ?>

				 <?php } else { ?>
				 	Add nebenwirkung
				 	<?php $fa_icon  = 'fa-plus'; ?>
				 <?php } ?>
			</li>	
			<li style="float:right;">
				<a class="btn red tooltips" href="<?php echo base_url('admin/nebenwirkung'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
				</a>
			</li>				
		</ul>			
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="fa fa-file-image"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>				
			</div>
			<?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
				<div class="portlet light">
					<div class="portlet-title">
						<div class="row">
							<div class="col-md-6">
								<div class="caption font-red-sunglo">
									<i class="fa fa-file-image"></i>
									<span class="caption-subject bold font-purple-plum uppercase"><i class="fa <?php echo $fa_icon; ?>"></i>About the nebenwirkung
									</span>
								</div>
							</div>					
						</div>				
					</div>
							<!-- <div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Behandungstag<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $behandungstag = isset($record->behandungstag) ? $record->behandungstag : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter Behandungstag", 'id' => "behandungstag", 'name' => "behandungstag", 'class' => "form-control", 'value' => "$behandungstag")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Visite<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $visite = isset($record->visite) ? $record->visite : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter visite", 'id' => "name", 'name' => "visite", 'class' => "form-control", 'value' => "$visite")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Arzneimittel<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $arzneimittel = isset($record->arzneimittel) ? $record->arzneimittel : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter Arzneimittel", 'id' => "arzneimittel", 'name' => "arzneimittel", 'class' => "form-control", 'value' => "$arzneimittel")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div> -->
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Nebenwirkung name<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $nebenwirkung_name = isset($record->nebenwirkung_name) ? $record->nebenwirkung_name : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter name", 'id' => "nebenwirkung_name", 'name' => "nebenwirkung_name", 'class' => "form-control", 'value' => "$nebenwirkung_name")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<!-- <div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Keine Kausal Bedingte Nebenwirkung<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $keine = isset($record->keine) ? $record->keine : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter keine", 'id' => "name", 'name' => "keine", 'class' => "form-control", 'value' => "$keine")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Mild<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $mild = isset($record->mild) ? $record->mild : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter mild", 'id' => "mild", 'name' => "mild", 'class' => "form-control", 'value' => "$mild")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Moderat<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $moderat = isset($record->moderat) ? $record->moderat : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter moderat", 'id' => "moderat", 'name' => "moderat", 'class' => "form-control", 'value' => "$moderat")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">schwer<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $schwer = isset($record->schwer) ? $record->schwer : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter schwer", 'id' => "schwer", 'name' => "schwer", 'class' => "form-control", 'value' => "$schwer")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">lebensbedrohlich<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $lebensbedrohlich = isset($record->lebensbedrohlich) ? $record->lebensbedrohlich : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter lebensbedrohlich", 'id' => "name", 'name' => "lebensbedrohlich", 'class' => "form-control", 'value' => "$lebensbedrohlich")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">kausalitat zur
                                 therapie vTHERAPIE VORHANDEN</label>
								<div class="md-radio-inline">
									<div class="md-radio">
										<input id="radio19" class="md-radiobtn" type="radio" name="kausalitat" value="Active" <?php echo ($kausalitat == 'Ja')?'checked':''; ?>>
										<label for="radio21">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Ja</label>
									</div>
									<div class="md-radio has-error">
										<input id="radio20" class="md-radiobtn" type="radio" name="kausalitat" value="Inactive" <?php echo ($status == 'Nein')?'checked':''; ?>>
										<label for="radio22">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Nein</label>
									</div>
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Status</label>
								<div class="md-radio-inline">
									<div class="md-radio">
										<input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" <?php echo ($status == 'Active')?'checked':''; ?>>
										<label for="radio19">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Active</label>
									</div>
									<div class="md-radio has-error">
										<input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" <?php echo ($status == 'Inactive')?'checked':''; ?>>
										<label for="radio20">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Inactive</label>
									</div>
								</div>
							</div> --> 
							<div class="form-actions noborder">
								<div class="row">
									<div class="col-md-offset-2 col-md-10">
										<button type="submit" class="btn green">Submit</button>
										<a href="<?php echo base_url('admin/cannabis_medikament'); ?>" class="btn default">Cancel</a>
								    </div>
								</div>
						    </div>
						</div>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
<script src="https://maps.googleapis.com/maps/api/js?v=3&key=<?php echo getSiteOption('google_map_api_key', true); ?>&sensor=false&libraries=places"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
		var input = document.getElementById('address');
        var places = new google.maps.places.Autocomplete(input);        
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
           var latitude = place.geometry.location.lat();
           var longitude = place.geometry.location.lng();
           $('#latitude').val(latitude);
           $('#longitude').val(longitude);
        });
    });

   	function updateLAT($this) {
   		var a = $($this).val();
   		$("#latitude").val('');
   		$("#longitude").val('');
   	}
 </script>

 <script type="text/javascript">

	$(function(){
	    $('#start_date').datetimepicker({
	       // startDate: new Date(),
		   // autoclose: true,
		   // format: "yyyy-mm-dd hh:ii",
	    });

	    $('#end_date').datetimepicker({
	       // startDate: new Date(),
		   // autoclose: true,
		   // format: "yyyy-mm-dd hh:ii",
	    });

	    $('#add_time').datetimepicker({

	    });
	});
 </script>

