<?php if ($records) { ?>

  <?php foreach ($records as $key => $value) { ?>
      <tr>
        <td style="text-align:center;"><input type="checkbox" class="checkboxes recordcheckbox" value="<?php echo $value->id; ?>"/>
        </td>

        <td><?php echo ($value->first_name) ? $value->first_name : '-'; ?></td>
        <td><?php echo ($value->name) ? $value->name: '-'; ?></td>
        <td><?php echo ($value->road) ? $value->road : '-'; ?></td>
        <td><?php echo ($value->email) ? $value->email : '-'; ?></td>
       <!--  <td>
          <div class="btn-group">

          <?php if ($value->status == 'Active') { ?>
            <button class="btn btn-xs btn-success status_label" type="button">Active</button>

          <?php } else { ?>
            <button class="btn btn-xs btn-danger status_label" type="button">Inactive</button>
          <?php } ?>
          <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-angle-down"></i></button>
            
          <ul class="dropdown-menu" role="menu" style="margin-top:-50px">
            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/users/change_status'); ?>" data-status="Active" onClick="changeStatusCommon(this)">Active</a></li>
            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/users/change_status'); ?>" data-status="Inactive" onClick="changeStatusCommon(this)">Inactive</a></li>
          </ul>
          </div>
        </td> -->
        <td>
          <div class="btn-group">

          <?php if ($value->is_approved == 'Yes') { ?>
            <button class="btn btn-xs btn-success status_label" type="button">Approve</button>

          <?php } else { ?>
            <button class="btn btn-xs btn-danger status_label" type="button">Unapprove</button>
          <?php } ?>
          <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-angle-down"></i></button>
            
          <ul class="dropdown-menu" role="menu" style="margin-top:-50px">
            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/users/change_approval_status'); ?>" data-status="Yes" onClick="changeStatusCommon(this)">Approve</a></li>
            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/users/change_approval_status'); ?>" data-status="No" onClick="changeStatusCommon(this)">Unapprove</a></li>
          </ul>
          </div>
        </td>

        <td><?php echo getGMTDateToLocalDate($value->add_date, 'F j,Y'); ?></td>

      <td class="text-center">
      <!-- <a href="<?php //echo base_url('admin/users/update/' . $value->id) ?>" class="btn purple tooltips" data-original-title="Update this record" data-placement="top" data-container="body"><i class="fa fa-pencil"></i></a> -->
      <a href="javascript:" class="btn btn-danger tooltips" data-original-title="Delete this record" data-placement="top" data-container="body" onclick="deleteUser(this)" data-id="<?php echo $value->id;  ?>" data-customer="<?php echo $value->id;  ?>" data-url="<?php echo base_url('admin/users/delete') ?>"><i class="fa fa-trash"></i></a>
      <?php  // } ?>
      <a href="<?php echo base_url('admin/users/view/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body"><i class="fa fa-eye"></i></a>
        
      <?php if ($value->contract_file) { ?>
        <a href="<?php echo base_url('admin/users/download_contract/' . $value->id) ?>" class="btn blue tooltips" data-original-title="View this record" data-placement="top" data-container="body"><i class="fa fa-download"></i></a>
      <?php } ?>
   
   </td> 
      </tr>               
  <?php } ?>
<?php } else { ?>
<tr>
   <td colspan="9"><div class="no-record">No record found</div></td>
</tr>
<?php }?> 
