<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js">
</script>
<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px" >
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/users')?>" class="tooltips" data-original-title="Doctors List" data-placement="top" data-container="body">Doctors List
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <?php if($id != '') { ?>
        <?php echo $first_name; ?>

        <?php } else { ?>
        Add New Doctor
        <?php } ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/users'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form', 'autocomplete' => 'off'));?>
        <div class="form-body">
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Titel 
              <span style="color:red">
              </span>
            </label>
            <div class="col-md-10">
              <select class="form-control" name="title">
                <?php pr($records->title); ?>
                <option <?php echo ($title == 'Dr.')?'selected':''; ?> value="Dr.">Dr.</option>
                <option <?php echo ($title == 'Dr.med.')?'selected':''; ?> value="Dr.med.">Dr.med.</option>
                <option <?php echo ($title == 'keine')?'selected':''; ?> value="keine">keine</option>
                <option <?php echo ($title == 'PD Dr. med.')?'selected':''; ?> value="PD Dr. med.">PD Dr. med.</option>
                <option <?php echo ($title == 'Prof. Dr. med.')?'selected':''; ?> value="Prof. Dr. med.">Prof. Dr. med.</option>
              </select>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Vorname
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Vorname', 'id' => "first_name", 'name' => "first_name", 'class' => "form-control", 'value' => "$first_name")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Nachname
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Name', 'id' => "name", 'name' => "name", 'class' => "form-control", 'value' => "$name")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Straße
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Straße", 'type' => "text", 'id' => "road", 'name' => "road", 'class' => "form-control", 'value' => "$road")) ?>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Hausnr
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Hausnr', 'id' => "hausnr", 'name' => "hausnr", 'class' => "form-control", 'value' => "$hausnr")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">PLZ
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "PLZ", 'type' => "text", 'id' => "plzl", 'name' => "plzl", 'class' => "form-control", 'oninput' => "this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');", 'value' => "$plzl")) ?>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Ort
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Ort", 'id' => "ort", 'name' => "ort", 'class' => "form-control", 'value' => "$ort")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Anrede: 
              <span style="color:red">
              </span>
            </label>
            <div class="col-md-10">
              <select class="form-control" name="lecture">
                 <?php pr($records->lecture); ?>
                <option <?php echo ($lecture == 'herr')?'selected':''; ?> value="herr">Herr</option>
                <option <?php echo ($lecture == 'Frau')?'selected':''; ?> value="Frau">Frau</option>
              </select>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">E-mail
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'E-mail', 'id' => "email", 'name' => "email", 'class' => "form-control", 'value' => "$email")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">www
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'www', 'id' => "www", 'name' => "www", 'class' => "form-control", 'value' => "$www")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Telephone Number
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "TEL. NR.", 'id' => "phonenumber", 'name' => "phonenumber", 'class' => "form-control", 'value' => "$phonenumber")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Fax
              <span style="color: red">
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "fax", 'id' => "fax", 'name' => "fax", 'class' => "form-control", 'value' => "$fax")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Name Arztpraxis
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Name Arztpraxis', 'id' => "name_doctor_practice", 'name' => "name_doctor_practice", 'class' => "form-control", 'value' => "$name_doctor_practice")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Fachrichtung 
              <span style="color:red">
              </span>
            </label>
            <div class="col-md-10">
              <select class="form-control" name="subject">
                <?php //pr($records->subject); ?>
                <option <?php echo ($subject == 'Allgemeinmedizin.')?'selected':''; ?> value="Allgemeinmedizin">Allgemeinmedizin</option>
                <option <?php echo ($subject == 'Anästhesiologie')?'selected':''; ?> value="Anästhesiologie">Anästhesiologie</option>
                <option <?php echo ($subject == 'Anatomie')?'selected':''; ?> value="Anatomie">Anatomie</option>
                <option <?php echo ($subject == 'Arbeitsmedizin')?'selected':''; ?> value="Arbeitsmedizin">Arbeitsmedizin</option>
                <option <?php echo ($subject == 'Allgemeine Chirurgie')?'selected':''; ?> value="Allgemeine Chirurgie">Allgemeine Chirurgie</option>
                <option <?php echo ($subject == 'Gefäßchirurgie')?'selected':''; ?> value="Gefäßchirurgie">Gefäßchirurgie</option>
                <option <?php echo ($subject == 'Herzchirurgie')?'selected':''; ?> value="Herzchirurgie">Herzchirurgie</option>
                <option <?php echo ($subject == 'Orthopädie und Unfallchirurgie')?'selected':''; ?> value="Orthopädie und Unfallchirurgie">Orthopädie und Unfallchirurgie</option>
                <option <?php echo ($subject == 'Thoraxchirurgie')?'selected':''; ?> value="Thoraxchirurgie">Thoraxchirurgie</option>
                <option <?php echo ($subject == 'Visceralchirurgie')?'selected':''; ?> value="Visceralchirurgie">Visceralchirurgie</option>
                <option <?php echo ($subject == 'Frauenheilkunde und Geburtshilfe ')?'selected':''; ?> value="Frauenheilkunde und Geburtshilfe ">Frauenheilkunde und Geburtshilfe </option>
                <option <?php echo ($subject == 'Hals-Nasen-Ohrenheilkunde')?'selected':''; ?> value="Hals-Nasen-Ohrenheilkunde">Hals-Nasen-Ohrenheilkunde</option>
                <option <?php echo ($subject == 'Innere und Allgemeinmedizin (Hausarzt)')?'selected':''; ?> value="Innere und Allgemeinmedizin (Hausarzt)">Innere und Allgemeinmedizin (Hausarzt)</option>
                <option <?php echo ($subject == 'Innere Medizin und Angiologie')?'selected':''; ?> value="Innere Medizin und Angiologie">Innere Medizin und Angiologie</option>
                <option <?php echo ($subject == 'Innere Medizin und Endokrinologie und Diabetologie')?'selected':''; ?> value="Innere Medizin und Endokrinologie und Diabetologie">Innere Medizin und Endokrinologie und Diabetologie</option>
                <option <?php echo ($subject == 'Innere Medizin und Gastroenterologie')?'selected':''; ?> value="Innere Medizin und Gastroenterologie">Innere Medizin und Gastroenterologie</option>
                <option <?php echo ($subject == 'Innere Medizin und Hämatologie und Onkologie ')?'selected':''; ?> value="Innere Medizin und Hämatologie und Onkologie ">Innere Medizin und Hämatologie und Onkologie </option>
                <option <?php echo ($subject == 'Innere Medizin und Kardiologie')?'selected':''; ?> value="Innere Medizin und Kardiologie">Innere Medizin und Kardiologie</option>
                <option <?php echo ($subject == 'Innere Medizin und Nephrologie')?'selected':''; ?> value="Innere Medizin und Nephrologie">Innere Medizin und Nephrologie</option>
                <option <?php echo ($subject == 'Innere Medizin und Pneumologie')?'selected':''; ?> value="Innere Medizin und Pneumologie">Innere Medizin und Pneumologie</option>
                <option <?php echo ($title == 'Innere Medizin und Rheumatologie')?'selected':''; ?> value="Innere Medizin und Rheumatologie">Innere Medizin und Rheumatologie</option>
                <option <?php echo ($title == 'Innere Medizin und Rheumatologie')?'selected':''; ?> value="Innere Medizin und Rheumatologie">Innere Medizin und Rheumatologie</option>
                <option <?php echo ($title == 'Mikrobiologie, Virologie und Infektionsepidemiologie')?'selected':''; ?> value="Mikrobiologie, Virologie und Infektionsepidemiologie">Mikrobiologie, Virologie und Infektionsepidemiologie</option>
                <option <?php echo ($title == 'Neurochirurgie')?'selected':''; ?> value="Neurochirurgie">Neurochirurgie</option>
                <option <?php echo ($title == 'Neurologie')?'selected':''; ?> value="Neurologie">Neurologie</option>
                <option <?php echo ($title == 'Nuklearmedizin')?'selected':''; ?> value="Nuklearmedizin">Nuklearmedizin</option>
                <option <?php echo ($title == 'Physikalische und Rehabilitative Medizin')?'selected':''; ?> value="Physikalische und Rehabilitative Medizin">Physikalische und Rehabilitative Medizin</option>
                <option <?php echo ($title == 'Psychiatrie und Psychotherapie')?'selected':''; ?> value="Psychiatrie und Psychotherapie">Psychiatrie und Psychotherapie</option>
                <option <?php echo ($title == 'Psychosomatische Medizin und Psychotherapie')?'selected':''; ?> value="Psychosomatische Medizin und Psychotherapie">Psychosomatische Medizin und Psychotherapie</option>
              </select>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Lanr
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Lanr', 'id' => "lanr", 'name' => "lanr", 'class' => "form-control", 'value' => "$lanr")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
         
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Videosprchstunde
              <span style="color:red">
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Link zur Videosprchstunde', 'id' => "videosprechstunde", 'name' => "videosprechstunde", 'class' => "form-control", 'value' => "$videosprechstunde")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <!-- <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Datenspeicherung</label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input id="radio21" class="md-radiobtn" type="checkbox" name="datenspeicherung" value="datenspeicherung" <?php //echo ($datenspeicherung == 'datenspeicherung')?'checked':''; ?>>
                <label for="radio21">Datenspeicherung</label>
              </div>
            </div>
          </div> -->
          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Unterschrift
            </label>
            <div class="col-md-10">
              <input type="file"  name="unterschrift" id="unterschrift" class="multifile form-control">
            </div>
          </div>  
          <?php if($task == 'edit') { ?>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">
            </label>
            <div class="col-md-10">   
              <img height="100" width="200" src="<?php echo base_url('assets/uploads/unterschrift-images/'. $records->unterschrift);?>" alt="">
            </div>
          </div>
          <?php  } ?>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Status</label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" <?php echo ($status == 'Active')?'checked':''; ?>>
                <label for="radio19">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>Active</label>
              </div>
             <div class="md-radio has-error">
                <input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" <?php echo ($status == 'Inactive')?'checked':''; ?>>
                <label for="radio20">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>Inactive</label>
              </div>
            </div>
          </div> 
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/users'); ?>" class="btn default">Cancel
                </a>
              </div>
            </div>
          </div>
        </div>
        </form>
    </div>
  </div>
</div>
</div>

