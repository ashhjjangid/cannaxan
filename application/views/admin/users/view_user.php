<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/users')?>" class="tooltips" data-original-title="List of doctor" data-placement="top" data-container="body"> List of Doctors
        </a>
        <i class="fa fa-arrow-right">
        </i>
          <?php echo $usersdata->first_name; ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/users'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Doctor Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>Name Arztpraxis
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->name_doctor_practice; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Anrede
                    </th>
                    <td>
                      <?php echo $usersdata->lecture; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Titel
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->title; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>Vorname
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->first_name; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>Nachname
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->name; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Strasse
                    </th>
                    <td>
                      <?php echo $usersdata->road; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Hausnr.
                    </th>
                    <td>
                      <?php echo $usersdata->hausnr; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      PLZ
                    </th>
                    <td>
                      <?php echo $usersdata->plzl; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Ort
                    </th>
                    <td>
                      <?php echo $usersdata->ort; ?>
                    </td>
                  </tr>
                  
                  <tr>
                    <th>E-mail
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->email; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Telephone Number
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->phonenumber ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Fax
                    </th>
                    <td>
                      <?php echo $usersdata->fax; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      www.
                    </th>
                    <td>
                      <?php echo $usersdata->www; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Videosprechstunde
                    </th>
                    <td>
                      <?php echo $usersdata->videosprechstunde; ?>
                    </td>
                  </tr>
                  <tr>
                  	<th>
                  		LANR
                  	</th>
                  	<td>
                  		<?php echo $usersdata->lanr; ?>
                  	</td>
                  </tr>                                    
                  <tr>
                    <th>
                      Fachrichtung
                    </th>
                    <td>
                      <?php echo $usersdata->subject; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Unterschrift
                    </th>
                    <td>
                      <img src="<?php echo base_url('assets/uploads/unterschrift-images/'.$usersdata->unterschrift); ?>" />
                    </td>
                  </tr>
                  <tr>
                    <th>Add Date
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo date('F d, Y',strtotime(getGMTDateToLocalDate($usersdata->add_date)));?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Status
                    </th>
                      <td style="word-break: break-all;">
                        <?php if ($usersdata->status == 'Active') { ?>
                          <button class="btn btn-xs btn-success status_label" type="button">Active</button>
                        <?php } else { ?>
                          <button class="btn btn-xs btn-danger stats_label" type="button">Inactive</button>
                        <?php } ?>
                      </td>
                  </tr>
                  
                </table>
              </section>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>