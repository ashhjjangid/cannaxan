<div class="row margin-top-10">
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
             <?php echo $users; ?>
          </h3>
          <!-- <a class="hd_title" href="javascript:void(0)">Total Doctors
          </a> -->
          <a class="hd_title" href="<?php echo base_url('admin/users'); ?>">Total Doctors
          </a>
        </div>
        <div class="icon">
          <i class="fa fa-user-md" aria-hidden="true">
          </i>
        </div>
      </div>	
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            <?php echo $patients; ?>
          </h3>
          <!-- <a class="hd_title" href="javascript:void(0)">Total Patients
          </a> -->
          <a class="hd_title" href="javascript:void(0)">Total Patients
          </a>
        </div>
        <div class="icon">
          <i class="fa fa-users" aria-hidden="true">
          </i>
        </div>
      </div>  
    </div>
  </div>
</div>