<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/Patients')?>" class="tooltips" data-original-title="List of Patients" data-placement="top" data-container="body"> List of Patients
        </a>
        <i class="fa fa-arrow-right">
        </i>
          <?php echo $laborwerte_data->first_name; ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/Patients'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Patient Laborwerte Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
                <?php if ($patient_laborwerte_data && $laborwerte_data) { ?>
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>
                      Datum
                    </th>
                    <td>
                      <?php echo date('d.m.Y', strtotime($laborwerte_data->add_date)); ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Behandungstag
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $laborwerte_data->behandungstag; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Visite
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $laborwerte_data->visite; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Arzneimittel
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $laborwerte_data->arzneimittel; ?>
                    </td>
                  </tr>
                </table>
                  
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>
                        Laborparameter
                      </th>
                      <th>
                        Wert
                      </th>
                      <th>
                        Einheit
                      </th>
                      <th>
                        Normbereich
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><?php echo $patient_laborwerte_data->laborparameter1 ?></td>
                      <td><?php echo $patient_laborwerte_data->wert1 ?></td>
                      <td><?php echo $patient_laborwerte_data->einheit1 ?></td>
                      <td><?php echo $patient_laborwerte_data->normbereich1 ?></td>                    
                    </tr>

                    <tr>
                      <td><?php echo $patient_laborwerte_data->laborparameter2 ?></td>
                      <td><?php echo $patient_laborwerte_data->wert2 ?></td>
                      <td><?php echo $patient_laborwerte_data->einheit2 ?></td>
                      <td><?php echo $patient_laborwerte_data->normbereich2 ?></td>                    
                    </tr>

                    <tr>
                      <td><?php echo $patient_laborwerte_data->laborparameter3 ?></td>
                      <td><?php echo $patient_laborwerte_data->wert3 ?></td>
                      <td><?php echo $patient_laborwerte_data->einheit3 ?></td>
                      <td><?php echo $patient_laborwerte_data->normbereich3 ?></td>                    
                    </tr>

                    <tr>
                      <td><?php echo $patient_laborwerte_data->laborparameter4 ?></td>
                      <td><?php echo $patient_laborwerte_data->wert4 ?></td>
                      <td><?php echo $patient_laborwerte_data->einheit4 ?></td>
                      <td><?php echo $patient_laborwerte_data->normbereich4 ?></td>                    
                    </tr>
                  </tbody>
                </table>
                <?php } else { ?>
                  <tr>
                    <td colspan="9"><div class="no-record">No record found</div></td>
                  </tr>
                <?php } ?>
              </section>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
