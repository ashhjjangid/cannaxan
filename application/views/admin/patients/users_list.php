<style type="text/css">
.loader-parent{position: relative;}
.loader-bx{position: absolute;left: 0;top: 0;width: 100%;height:100%;background: rgba(255,255,255,0.7)}
.loader-bx>img{max-width: 63px;filter: brightness(0);-webkit-filter: brightness(0);transform: translate(-50%,-50%);-webkit-transform: translate(-50%,-50%);position: absolute;left: 50%;top: 50%;}
</style>
<?php
if ($this->session->flashdata('message')) {
echo '<div class="alert alert-success">' . $this->session->flashdata('message') . '</div>';
}
?>
<script type="text/javascript">

  function sortByColumnName($this)
  {
    $('#insured_number').html('<i class="fa fa-sort" aria-hidden="true"></i>');
    $('#name').html('<i class="fa fa-sort" aria-hidden="true"></i>');
    $('#case_number').html('<i class="fa fa-sort" aria-hidden="true"></i>');
    $('#first_name').html('<i class="fa fa-sort" aria-hidden="true"></i>');
    $('#add_date').html('<i class="fa fa-sort" aria-hidden="true"></i>');
    var column_name = $($this).attr('data-column_name');
    var sorting_order = $($this).attr('data-sorting_order');
    if (sorting_order == "ASC") {
      $($this).attr('data-sorting_order', 'DESC');
      $("#"+column_name).html('<i class="fa fa-sort-asc" aria-hidden="true"></i>');
    } else {
      $($this).attr('data-sorting_order', 'ASC');
      $('#'+column_name).html('<i class="fa fa-sort-desc" aria-hidden="true"</i>');
    }
    var keyword = $('#keyword').val();
    var page_limit = $('#page_limit').val();
    var surl = siteurl+'admin/patients/get_ajax_list?keyword='+keyword+'&page_limit='+page_limit+'&sorting_order='+sorting_order+'&column_name='+column_name;
    getAjaxSearchData(surl);
  }
  
  function searchRecords()
  {
    var keyword = $('#keyword').val();
    var page_limit = $('#page_limit').val();
    var column_name = 'add_date';
    var sorting_order = 'ASC';
    
    var surl = siteurl+'/admin/patients/get_ajax_list?keyword='+keyword+'&page_limit='+page_limit+'&sorting_order='+sorting_order+'&column_name='+column_name;
    getAjaxSearchData(surl);
  }

  function getAjaxSearchData(surl)
  {
    //$(".lodding").css('display','block');
    
      $.getJSON(surl,function(data){
      $('body').removeClass('showloader');
        $(".lodding").css('display','none');  
      if(data.success)
      {
         $('.ajax_content').html(data.html);
         $(".paging_div").html(data.paging);
      }
    })
  }

  $(document).ready(function(){
      searchRecords();
      $(document).on('click','.loader-parent .pagination a',function(e){
        e.preventDefault();
        if($(this).attr('href'))
        {
          getAjaxSearchData($(this).attr('href'));
        }
        return false;
    })
    $('#keyword').keydown(function(event) {
      // enter has keyCode = 13, change it if you want to use another button
      if (event.keyCode == 13) {
        // event.preventDefault();
        searchRecords();
      }
    });
  });
</script>
<div class="modal fade bs-modal-sm" id="DeleteUser" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <form role="form" method="post" action="" class="ajax_form" id="DeleteUserForm">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">Delete</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" class="form-control" name="record_id" value="" id="UserID">
          <input type="hidden" class="form-control" name="cusotmer_id" value="" id="customerID">
          Are You Sure! You want to delete?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn red">Delete</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
function deleteUser($this) {
  $('#DeleteUser').modal('show');
  $('#UserID').val($($this).data('id'));
  $('#customerID').val($($this).data('customer'));
  $('#DeleteUserForm').attr('action', $($this).data('url'));
}

</script>
<script type="text/javascript">
function callBackRefresh()
{
  searchRecords();
  $('#DeleteUser').modal('hide');
}
</script>

<div class="row">
  <div class="col-md-12">
    <div class="box grey-cascade">
      <div class="portlet-body">
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-1 col-sm-12">
              <div class="form-group">
                <select name="page_limit" class="form-control input-xsmall input-inline select2" id="page_limit" onchange="searchRecords(this)">
                  <option value="5">5</option>
                  <option value="15" selected>15</option>
                  <option value="20">20</option>
                  <option value="50">50</option>
                  <option value="100">100</option>
                  <option value="300">300</option>
                </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-12 pull-right">
              <div class="">
                <input class="form-control " placeholder="Search by keyword" type="text" id="keyword" name="keyword" value="" onchange="searchRecords(this)" >
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="portlet box grey-cascade">
      <div class="portlet-title">
        <div class="caption"><i class="icon-users"></i>Lists of Patients</div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div id="alert_area"></div>
          <div class="row">
            <div class="col-md-12">
              <div class="btn-group tooltips" data-original-title="Add new Patient">
                <a href="<?php echo base_url('admin/patients/add'); ?>" class="btn green add_link">Add New <i class="fa fa-plus"></i></a>
              </div>
              <div style="margin-left:10px;" onClick="checkForNullChecked('Active',this)" data-taskurl="<?php echo base_url('admin/patients/multi_task_operation'); ?>" class="btn green tooltips" data-original-title="Active selected record">Active <i class="fa fa-check"></i></i>
              </div>
              <div style="margin-left:10px;" onClick="checkForNullChecked('Inactive',this)" data-taskurl="<?php echo base_url('admin/patients/multi_task_operation'); ?>" class="btn purple tooltips" data-original-title="Inactive selected record">Inactive<i class="fa fa-ban"></i></div>
              <div style="margin-left:10px;" onClick="checkForNullChecked('Delete',this)" data-taskurl="<?php echo base_url('admin/patients/multi_task_operation'); ?>" class="btn red tooltips" data-original-title="Delete selected record">Delete <i class="fa fa-trash"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="loader-parent">
          <table class="table table-striped table-bordered table-hover" id="sample_3">
            <thead>
              <tr>
                 <th class="table-checkbox"> <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes"/></th>   
                
                <th onclick="sortByColumnName(this);" data-sorting_order="ASC" data-column_name="insured_number"><span id="insured_number"><i class="fa fa-sort" aria-hidden="true"></i></span>Versicherten Nummer</th>
                <th onclick="sortByColumnName(this);" data-sorting_order="ASC" data-column_name="case_number"><span id="case_number"><i class="fa fa-sort" aria-hidden="true"></i></span> Fallnummer</th>
                <th onclick="sortByColumnName(this);" data-sorting_order="ASC" data-column_name="first_name"><span id="first_name"><i class="fa fa-sort" aria-hidden="true"></i></span> Name</th>
                <!-- <th onclick="sortByColumnName(this);" data-sorting_order="ASC" data-column_name="name"><span id="name"><i class="fa fa-sort" aria-hidden="true"></i></span> Doctors Name</th> -->
                <th onclick="sortByColumnName(this);" data-sorting_order="ASC" data-column_name="status"><span id="status"><i class="fa fa-sort" aria-hidden="true"></i></span> Status</th>
                <th onclick="sortByColumnName(this);" data-sorting_order="ASC" data-column_name="add_date"><span id="add_date"><i class="fa fa-sort" aria-hidden="true"></i></span> Add date</th>
                <th>Options</th>
                <!-- <th onclick="sortByColumnName(this);" data-sorting_order="ASC" data-column_name="delete_yes/no"><span id="delete_yes/no"><i class="fa fa-sort" aria-hidden="true"></i></span> Delete(Yes/No) </th> -->
              </tr>
            </thead>
            <tbody class="sortable ajax_content">
            </tbody>
          </table>
          <div class="row">
            <div class="col-md-12">
              <div class="paging_div pull-right pagination">

              </div>
            </div>
          </div>
          <div class="loader-bx lodding">
            <img src="<?php echo site_url(); ?>assets/wait.png" alt="loader">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .paging_div-not-space{text-align: right;}
  .paging_div-not-space ul{margin:0;}
</style>