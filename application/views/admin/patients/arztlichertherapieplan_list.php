<?php
if ($this->session->flashdata('message')) {
echo '<div class="alert alert-success">' . $this->session->flashdata('message') . '</div>';
}
?>
<script type="text/javascript">
  $(function(){
    var table = $('#sample_3');
    // begin first table
    table.dataTable({
      // Internationalisation. For more info refer to http://datatables.net/manual/i18n
      "language": {
        "aria": {
          "sortAscending": ": activate to sort column ascending",
          "sortDescending": ": activate to sort column descending"
        }
        ,
        "emptyTable": "No data available in table",
        "info": "Showing1 _START_ to _END_ of _TOTAL_ entries1",
        "infoEmpty": "No entries found",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "Show _MENU_ entries",
        "search": "Search:",
        "zeroRecords": "No matching records found."
      }
      ,
      "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
      "lengthMenu": [
        [5, 15, 20, 100, -1],
        [5, 15, 20, 100, "All"] // change per page values here
      ],
      // set the initial value
      "pageLength": 100,            
      "pagingType": "bootstrap_full_number",
      "language": {
        "search": "Search Anything: ",
        "lengthMenu": "  _MENU_ records",
        "paginate": {
          "previous":"Prev",
          "next": "Next",
          "last": "Last",
          "first": "First"
        }
      }
      ,
      "columnDefs": [{
        // set default column settings
        'orderable': false,
        'targets': [0]
      }
     , {
       "searchable": false,
       "targets": [0]
     }],
      "order": [
        [1, "desc"]
      ] // set first column as a default sort by asc
    });
  }
   );
</script>
<style>
  .decline_info{
    background-color: #89C4F4;
    color: #fff;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="box grey-cascade">      
      <div class="portlet-body">
      </div>
    </div>
    <div class="portlet box grey-cascade">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-star" aria-hidden="true">
          </i>
          </i>List of Arztlicher Therapieplan
      </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
        <div id="alert_area">
        </div>
        <div class="row">
          <div class="col-md-12">   
          </div>  
        </div>
      </div>
      <div class="">
        <table class="table table-striped table-bordered table-hover" id="sample_3">
          <thead>
            <tr>
              <th class="table-checkbox"> 
                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes"/>
              </th>
              <th>Rezepturarzneimittel</th>              
              <th>Wirkstoff</th>
              <th>dosistyp</th>             
              <th>Add date</th>
              <th>Options
              </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($arztlichertherapieplan as $key => $value) { ?>
            <tr>
              <td>
                <input type="checkbox" class="checkboxes recordcheckbox" value="<?php echo $value->id; ?>"/>
              </td>
              <td><?php echo $value->rezepturarzneimittel;?></td>
              <td><?php echo $value->wirkstoff;?></td>
              <td><?php echo $value->dosistyp;?></td>
              <td><?php echo getGMTDateToLocalDate($value->add_date,'F j,Y'); ?></td>
              <td class="text-center">
                <a href="<?php echo base_url('admin/patients/view_arztlichertherapieplandata/'.$value->patient_id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">
                  <i class="fa fa-eye">
                  </i>
                </a>
              </td>
            </tr>               
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
