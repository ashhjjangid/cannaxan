<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/Patients')?>" class="tooltips" data-original-title="List of Patients" data-placement="top" data-container="body"> List of Patients
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/Patients'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Patients Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
                <?php if ($patient_begleitmedikation_data) { ?>
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                     <th>Wirksoff</th>
                     <th>Handelsname</th>
                     <th>Starke</th>
                     <th>Einheit</th>
                     <th>form</th>
                     <th>morgens</th>
                     <th>Mittags</th>
                     <th>abends</th>
                     <th>zur Nacht</th>
                     <th>Einheit</th>
                     <th>Tagesdosis</th>
                     <th>Grund</th>
                     <th>Startdatum</th>
                     <th>Startdatum</th>
                   </tr>
                  </thead>
                  

                  <?php foreach ($patient_begleitmedikation_data as $key => $value) { ?>
                  <tbody>
                    <tr>
                   <td><?php echo $value->wriksoff; ?></td>
                    <td><?php echo $value->handelsname; ?> 
                    </td>
                    <td><?php echo $value->starke; ?>
                    </td>
                    <td><?php echo $value->eiheit;?>
                    </td>
                    <td><?php echo $value->form;?>
                    </td>
                    <td><?php echo ($value->morgens); ?>
                    </td>
                    <td><?php echo $value->mittags; ?>
                    </td>
                    <td><?php echo $value->abends; ?>
                    </td>
                    <td><?php echo $value->zur_nacht; ?>
                    </td>
                    <td><?php echo $value->einheit; ?>
                    </td>
                    <td><?php echo $value->tagesdosis; ?></td>
                    <td><?php echo $value->grund; ?></td>
                    <td><?php echo $value->startdatum; ?></td>
                    <td><?php echo $value->stopdatum; ?></td>
                  </tr>
                  </tbody>
                  <?php } ?>
                <?php } else { ?>
                       <tr>
                        <td colspan="9"><div class="no-record">No record found</div></td>
                      </tr>
                    <?php } ?>
                </table>
              </section>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
