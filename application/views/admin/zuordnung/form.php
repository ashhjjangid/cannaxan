 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js">
</script>
<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px" >
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/zuordnung_sps')?>" class="tooltips" data-original-title="zuordnung_sps List" data-placement="top" data-container="body">zuordnung sps List
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <?php if($id != '') { ?>
        <?php echo $wirkstoff; ?>

        <?php } else { ?>
        Add zuordnung sps
        <?php } ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/zuordnung_sps'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form', 'autocomplete' => 'off'));?>
        <div class="form-body">
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Wirkstoff
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Wirkstoff', 'id' => "wirkstoff", 'name' => "wirkstoff", 'class' => "form-control", 'value' => "$wirkstoff")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Konzentration THC mg ml
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => '?Konzentration THC mg ml', 'id' => "konzentration_thc_mg_ml", 'name' => "konzentration_thc_mg_ml", 'class' => "form-control", 'value' => "$konzentration_thc_mg_ml")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Konzentration CBD mg ml
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Konzentration CBD mg ml", 'type' => "text", 'id' => "konzentration_cbd_mg_ml", 'name' => "konzentration_cbd_mg_ml", 'class' => "form-control", 'value' => "$konzentration_cbd_mg_ml")) ?>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Sprühstöße
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Sprühstöße", 'type' => "text", 'id' => "spruhstobe", 'name' => "spruhstobe", 'class' => "form-control", 'value' => "$spruhstobe")) ?>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Volumen SPS ml
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Volumen SPS ml', 'id' => "volumen_sps_ml", 'name' => "volumen_sps_ml", 'class' => "form-control", 'value' => "$volumen_sps_ml")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Menge THC SPS mg
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Menge THC SPS mg', 'id' => "menge_thc_sps_mg", 'name' => "menge_thc_sps_mg", 'class' => "form-control", 'value' => "$menge_thc_sps_mg")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Menge CBD SPS mg
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Menge CBD SPS mg", 'id' => "menge_cbd_sps_mg", 'name' => "menge_cbd_sps_mg", 'class' => "form-control", 'value' => "$menge_cbd_sps_mg")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">SPS 20 ml flasche
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "SPS 20 ml flasche", 'id' => "sps_20_ml_flasche", 'name' => "sps_20_ml_flasche", 'class' => "form-control", 'value' => "$sps_20_ml_flasche")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Menge THC 20 ml
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Menge THC 20 ml", 'id' => "menge_thc_20_ml", 'name' => "menge_thc_20_ml", 'class' => "form-control", 'value' => "$menge_thc_20_ml")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Menge CBD 20 ml
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Menge CBD 20 ml', 'id' => "menge_cbd_20_ml", 'name' => "menge_cbd_20_ml", 'class' => "form-control", 'value' => "$menge_cbd_20_ml")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Anzahl Sprühstöße Durchgang
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Anzahl Sprühstöße Durchgang', 'id' => "anzahl_spruhstobe_durchgang", 'name' => "anzahl_spruhstobe_durchgang", 'class' => "form-control", 'value' => "$anzahl_spruhstobe_durchgang")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div> 
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/zuordnung_sps'); ?>" class="btn default">Cancel
                </a>
              </div>
            </div>
          </div>
        </div>
        </form>
    </div>
  </div>
</div>
</div>

