<!-- <?php
  if ($this->session->flashdata('message')) {
    echo '<div class="alert alert-success">' . $this->session->flashdata('message') . '</div>';
  }
?>
<script type="text/javascript">
$(function(){
  var table = $('#sample_3');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing1 _START_ to _END_ of _TOTAL_ entries1",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            
            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [{
                "orderable": false
            }, {
                "orderable": true
            },{
                "orderable": true
            }, {
                "orderable": false
            }],
            "lengthMenu": [
                [5, 15, 20, 100, -1],
                [5, 15, 20, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "Search Anything: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [1, "desc"]
            ] // set first column as a default sort by asc
        });
  });
</script>
<div class="row">
  <div class="col-md-12">
    <div class="box grey-cascade">      
      <div class="portlet-body">
        
      </div>
    </div>
    <div class="portlet box grey-cascade">
      <div class="portlet-title">
        <div class="caption"><i class="icon-docs"></i><?php echo $page_title; ?></div>
      </div>
      <div class="portlet-body">
        <div class="table-toolbar">
          <div id="alert_area"></div>
          <div class="row">
            <div class="col-md-12"> 
            
            </div>            
          </div>
        </div>
        <div class="">
          <table class="table table-striped table-bordered table-hover" id="sample_3">
            <thead>
              <tr>
                <th class="table-checkbox"> <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes"/>
                  </th>
                <th>wirkstoff</th>
                <th>spruhstobe</th>
                <th>volumen_sps_ml</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($static_pages as $key => $value) { ?>
                <tr>
                <td><input type="checkbox" class="checkboxes recordcheckbox" value="<?php echo $value->id; ?>"/></td> 
                <td><?php echo showLimitedText($value->wirkstoff,40); ?></td>
                <td><?php echo showLimitedText($value->spruhstobe,40); ?></td>
                <td><?php echo showLimitedText($value->volumen_sps_ml,40); ?></td>
                <td class="text-center">
                  <a href="<?php echo base_url('admin/zuordnung_sps/update/'.$value->id) ?>" class="btn purple tooltips" data-original-title="Update this record" data-placement="top" data-container="body"><i class="fa fa-pencil"></i></a>
                  
               
                </td>
                </tr>               
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div> -->


<div class="portlet light" style="height:45px">
  <ul class="page-breadcrumb breadcrumb">
   <!--  <div class="portlet-title">
        <div class="caption"><i class="icon-docs"></i> <?php echo $page_title; ?></div>
      </div> -->
      <i class="icon-home">
      </i>
      <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
      </a>
      <i class="fa fa-arrow-right">
      </i>
    </li>
    <li>      
      <a href="<?php echo site_url('admin/zuordnung_sps')?>" class="tooltips" data-original-title="List of zuordnung sps" data-placement="top" data-container="body">List of zuordnung sps
      </a>
      <!-- <i class="fa fa-arrow-right">
      </i> -->
    </li>
    <!-- <li><?php echo $record->wirkstoff;?>
    </li> --> 
    <li style="float:right;">
      <a class="btn red tooltips" href="<?php echo base_url('admin'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
        <i class="m-icon-swapleft m-icon-white">
        </i>
      </a>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>          
        </div>
      </div>    
      <div class="portlet-body form">   
        <div class="form-body">
          <?php foreach ($record as $key => $value) { ?>
          <div class="">
            <div class="portlet light">
              <div class="portlet-title">
                <div class="row">
                  <div class="col-md-6">
                    <div class="caption font-red-sunglo">
                      <i class="fa fa-file-image">
                      </i>
                      <a href="<?php echo base_url('admin/zuordnung_sps/update/'.$value->id) ?>" class="btn purple tooltips" data-original-title="Update this record" data-placement="top" data-container="body"><i class="fa fa-pencil"></i>Edit</a>
                    </div>
                  </div>          
                </div>        
              </div>
              <div class="portlet-body portlet-empty"> 
                <section style="margin-top: 20px; background-color: white; ">
                  <table class="table table-bordered table-condensed">
                   
                    <tr>
                      <th>Wirkstoff
                      </th>
                      <td><?php echo$value->wirkstoff ; ?></td>
                    </tr>
                    <tr>
                      <th>Konzentration THC [mg/mL]
                      </th>
                      <td>
                        <?php echo $value->konzentration_thc_mg_ml; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Konzentration CBD [mg/mL]
                      </th>
                      <td>
                        <?php echo $value->konzentration_cbd_mg_ml; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Sprühstöße
                      </th>
                      <td>
                        <?php echo $value->spruhstobe; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Volumen/SPS [mL]
                      </th>
                      <td>
                        <?php echo  $value->volumen_sps_ml; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Menge THC/SPS [mg]
                      </th>
                      <td>
                        <?php echo $value->menge_thc_sps_mg; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Menge CBD/SPS [mg]
                      </th>
                      <td>
                        <?php echo $value->menge_cbd_sps_mg; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>SPS/20 mL Flasche
                      </th>
                      <td>
                        <?php echo  $value->sps_20_ml_flasche; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Menge THC/20ml
                      </th>
                      <td>
                        <?php echo $value->menge_thc_20_ml; ?>
                      </td>
                    </tr>
                    <tr>
                      <th> Anzahl Sprühstöße/Durchgang
                      </th>
                      <td>
                        <?php echo $value->anzahl_spruhstobe_durchgang; ?>
                      </td>
                    </tr>
                  </table>
                </section>                                       
              </div>
            </div>
          </div>           
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>