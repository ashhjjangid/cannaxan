<?php if ($records) { ?>

  <?php foreach ($records as $key => $value) { ?>
      <tr>
        <td style="text-align:center;"><input type="checkbox" class="checkboxes recordcheckbox" value="<?php echo $value->id; ?>"/>
        </td>
        <td>
          <?php if(!empty($value->image)) { ?>
          <img height="60" width="60" src="<?php echo base_url('assets/uploads/users/profile/'.$value->image);?>" alt="">
          <?php }  else { ?>
          <img height="60" width="60" src="<?php echo base_url('assets/uploads/noimageavailable.png');?>" alt="">
          <?php } ?>
        </td>

        <td><?php echo ($value->name) ? $value->name : '-'; ?></td>
        <td><?php echo ($value->email) ? $value->email : '-'; ?></td>
        <td><?php echo ($value->description) ? $value->description: '-'; ?></td>
        <!-- <td><?php echo ($value->dob) ? date('F j, Y',strtotime($value->dob)) : '-'; ?></td> -->
        <td><?php echo ($value->phone_number) ? $value->phone_number : '-'; ?></td>
        <td>
          <div class="btn-group">

          <?php if ($value->status == 'Active') { ?>
            <button class="btn btn-xs btn-success status_label" type="button">Activate</button>

          <?php } else { ?>
            <button class="btn btn-xs btn-danger status_label" type="button">Deactivate</button>
          <?php } ?>
          <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-angle-down"></i></button>
            
          <ul class="dropdown-menu" role="menu" style="margin-top:-50px">
            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/individuals/changeStatus'); ?>" data-status="Active" onClick="changeStatusCommon(this)">Activate</a></li>
            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/individuals/changeStatus'); ?>" data-status="Inactive" onClick="changeStatusCommon(this)">Deactivate</a></li>
          </ul>
          </div>
        </td>

        <td><?php echo getGMTDateToLocalDate($value->add_date, 'F j,Y'); ?></td>

        <td class="text-center">
          <a href="<?php echo base_url('admin/individuals/update/'.$value->id) ?>" class="btn purple tooltips" title="Update this record" data-original-title="Update this record" data-placement="top" data-container="body"><i class="fa fa-pencil"></i></a>
          
          <a href="<?php echo base_url('admin/individuals/view/'.$value->id) ?>" class="btn yellow tooltips" title="View this record" data-original-title="View this record" data-placement="top" data-container="body"><i class="fa fa-eye"></i></a>

          <!-- <a href="<?php echo base_url('admin/trips/'.$value->id) ?>" class="btn green tooltips" title="Trip List" data-original-title="Trip List" data-placement="top" data-container="body"><i class="fa fa-calendar"></i></a> -->

          <!-- <a data-toggle="modal" data-id="<?php echo $value->id; ?>" title="Delete this record" data-url="<?php echo base_url('admin/individuals/delete'); ?>" class="btn btn-danger tooltips" onClick="deleteRecord(this);" data-original-title="Delete this record" data-placement="top" data-container="body"><i class="fa fa-remove"></i> -->
          </a>
        </td> 
      </tr>               
  <?php } ?>
<?php } else { ?>
<tr>
   <td colspan="10"><div class="no-record">No record found</div></td>
</tr>
<?php }?>