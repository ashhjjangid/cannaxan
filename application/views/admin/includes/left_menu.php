<div class="page-sidebar-wrapper">
	<div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
		<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<li class="start <?php echo ($left_menu == 'Dashboard')?'active':''; ?>">
				<a href="<?php echo site_url('admin'); ?>">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
				</a>
			</li>
			
			<li class="<?php echo ($left_menu == "doctors_management")?'active open':'';  ?>">
				<a href="javascript:;">
					<i class="fa fa-user-md"></i>
					<span class="title">Doctors Management</span>
					<span class="arrow" <?php echo ($left_menu == 'doctors_management')?'open':''; ?>></span>
				</a>
				<ul class="sub-menu">
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'doctors_list')?'active':''; ?>">
						<a href="<?php echo base_url('admin/users'); ?>">
							<i class="fa fa-list" aria-hidden="true"></i>
							<span class="title">List</span>
						</a>
					</li>
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'unapproved_doctors_list')?'active':''; ?>">
						<a href="<?php echo base_url('admin/users/unapproved_users'); ?>">
							<i class="fa fa-list" aria-hidden="true"></i>
							<span class="title">Unapproved</span>
						</a>
					</li>
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'add_doctors_listt')?'active':''; ?>">
						<a href="<?php echo base_url('admin/users/add'); ?>">
							<i class="fa fa-plus" aria-hidden="true"></i>
							<span class="title">Add</span>
						</a>
					</li>
				</ul>
			</li>
			<!-- <li class="<?php // echo ($left_menu == 'patient_management')?'active open':''; ?>">
				<a href="javascript:;">
				<i class="fa fa-users" aria-hidden="true"></i>
				<span class="title">Patients Management</span>
				<span class="arrow <?php // echo ($left_menu == 'patient_management')?'open':''; ?>"></span>
				</a>
				<ul class="sub-menu">						
					<li class="<?php // echo (isset($left_submenu) && $left_submenu == 'patient_list')?'active':''; ?>">
						<a href="<?php // echo base_url('admin/patients'); ?>">
						<i class="fa fa-list" aria-hidden="true"></i>
						<span class="title">List</span>	
						</a>
					</li>
					<li class="<?php // echo (isset($left_submenu) && $left_submenu == 'add_patient_list')?'active':''; ?>">
						<a href="<?php // echo base_url('admin/patients/add'); ?>">
						<i class="fa fa-plus" aria-hidden="true"></i>
						<span class="title">Add</span>	
						</a>
					</li>
				</ul>
			</li> -->
				
			<li class="<?php echo ($left_menu == "Zuordnung_sps_management")?'active open':'';  ?>">
				<a href="<?php echo base_url('admin/zuordnung_sps'); ?>">
					<i class="fa fa-file"></i>
					<span class="title">Zuordnung SPS</span>

				</a>
			</li>

			<li class="<?php echo ($left_menu == "icd_code_management")?'active open':'';  ?>">
				<a href="<?php echo base_url('admin/icd_codes'); ?>">
					<i class="fa fa-list"></i>
					<span class="title">ICD Code</span>

				</a>
			</li>
			<li class="<?php echo ($left_menu == 'dosistyp_management')?'active open':''; ?>">
				<a href="javascript:;">
				<i class="fa fa-list" aria-hidden="true"></i>
				<span class="title">Dosistyp Management</span>
				<span class="arrow <?php echo ($left_menu == 'dosistyp_management')?'open':''; ?>"></span>
				</a>
				<ul class="sub-menu">						
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'dosistyp_list')?'active':''; ?>">
						<a href="<?php echo base_url('admin/dosistyp'); ?>">
						<i class="fa fa-list" aria-hidden="true"></i>
						<span class="title">Dosistyp I</span>	
						</a>
					</li>
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'dosistyp')?'active':''; ?>">
						<a href="<?php echo base_url('admin/DosistypII'); ?>">
						<i class="fa fa-list" aria-hidden="true"></i>
						<span class="title">Dosistyp II </span>	
						</a>
					</li>
				</ul>
			</li>
			<li class="<?php echo ($left_menu == "cannabismedikament_management")?'active open':'';  ?>">
				<a href="<?php echo base_url('admin/cannabis_medikament'); ?>">
					<i class="fa fa-list"></i>
					<span class="title">Cannabis Medikament</span>
				</a>
			</li>
             
             <li class="<?php echo ($left_menu == "nebenwirkung_management")?'active open':'';  ?>">
				<a href="<?php echo base_url('admin/nebenwirkung'); ?>">
					<i class="fa fa-list"></i>
					<span class="title">Nebenwirkung</span>

				</a>
			</li>

			<!-- <li class="<?php echo ($left_menu == 'Static_Pages')?'active open':''; ?>">
				<a href="javascript:;">
				<i class="fa fa-book" aria-hidden="true"></i>
				<span class="title">Content Management</span>
				<span class="arrow <?php echo ($left_menu == 'Static_Pages')?'open':''; ?>"></span>
				</a>
				
				<ul class="sub-menu">						
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'static_page_list')?'active':''; ?>">
						<a href="<?php echo site_url('admin/static_page'); ?>">
						<i class="fa fa-list" aria-hidden="true"></i>
						<span class="title">Static Pages (Web)</span>	
						</a>
					</li>
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'static_page_mobile_list')?'active':''; ?>">
						<a href="<?php echo site_url('admin/static_page_mobile'); ?>">
						<i class="fa fa-list" aria-hidden="true"></i>
						<span class="title">Static Pages (Mobile)</span>	
						</a>
					</li>
				</ul>
			</li> -->
			<li class="<?php echo ($left_menu == 'Mail_templates')?'active open':''; ?>">
				<a href="javascript:;">
				<i class="fa fa-envelope-o"></i>
				<span class="title">E-mail Templates</span>
				<span class="arrow <?php echo ($left_menu == 'Mail_templates')?'open':''; ?>"></span>
				</a>
				<ul class="sub-menu">
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'Mail_templates_list')?'active':''; ?>">
						<a href="<?php echo base_url('admin/mail_templates'); ?>">		
						<i class="fa fa-list"></i>
						<span class="title">List</span>	
						</a>
					</li>
				</ul>
			</li>
			<!-- <li class="<?php echo ($left_menu == "team_management")?'active open':'';  ?>">
				<a href="<?php echo base_url('admin/team_management'); ?>">
					<i class="fa fa-users"></i>
					<span class="title">Team Management</span>
				</a>
			</li> -->
			<!-- <li class="<?php echo ($left_menu == 'FAQ_management')?'active open':''; ?>">
				<a href="javascript:;">
				<i class="fa fa-question" aria-hidden="true"></i>
				<span class="title">FAQ Management</span>
				<span class="arrow <?php echo ($left_menu == 'FAQ_management')?'open':''; ?>"></span>
				</a>
				
				<ul class="sub-menu">						
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'faq_category')?'active':''; ?>">
						<a href="<?php echo site_url('admin/faq_category'); ?>">
						<i class="fa fa-list" aria-hidden="true"></i>
						<span class="title">Categories</span>	
						</a>
					</li>
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'FAQ_list')?'active':''; ?>">
						<a href="<?php echo site_url('admin/faq'); ?>">
						<i class="fa fa-list" aria-hidden="true"></i>
						<span class="title">Questions</span>	
						</a>
					</li>
				</ul>
			</li> -->
			
			<!-- <li class="<?php echo ($left_menu == 'Templates')?'active open':''; ?>">
				<a href="<?php echo site_url('admin/newsletter_templates');?>">
				<i class="fa fa-newspaper-o" aria-hidden="true"></i>
				<span class="title">Newsletter</span>
				</a>
			</li> -->			
			
     		<li class="<?php echo ($left_menu == 'Site_options')?'active open':''; ?>">
				<a href="javascript:void(0);">
					<i class="fa fa-wrench"></i>
					<span class="title">Site Options</span>
					<span class="arrow <?php echo ($left_menu == 'Site_options')?'open':''; ?>"></span>
				</a>
				<ul class="sub-menu">
					<li class="<?php echo (isset($left_submenu) && $left_submenu == 'website')?'active':''; ?>">
						<a href="<?php echo base_url('admin/website'); ?>">
							<i class="icon-settings"></i>
							<span class="title">Website Setting</span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
<!-- END SIDEBAR icon-money