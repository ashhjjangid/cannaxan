<!DOCTYPE html>
      <section class="banner-section anmeldung">
         <div class="container-fluid">
            <div class="banner-img">
               <img src="<?php echo $this->config->item('front_assets'); ?>images/banner-img.png">
            </div>
         </div>
      </section>

      <section class="same-section min-height align-items-center-custom">
         <div class="container-fluid">
               <?php echo form_open(current_url(), array('class' => 'ajax_form'));?>
            <div class="row">
                  <div class="col-md-7">
                     <div class="row">
                        <div class="col-md-8 align-self-center">
                           <label class="w-100 text-right mb-0">Versichertennummer:</label>
                        </div>
                        <div class="col-md-4">
                           <input type="text" name="versichertennummer" class="form-control" placeholder="Versichertennummer">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 align-self-center">
                     <div class="custom-btn w-100 text-right">
                        <button type="submit" class="btn btn-primary"><span>Laden bzw. Registrieren</span></a>
                     </div>
                  </div>
            </div>
               </form>
         </div>
      </section>