<section class="same-section">
         <div class="container-fluid">
            <div class="free-space-on-left">
               <div class="row">
                  <div class="col-md-6">
                     <div class="same-heading ">
                        <h2 class="text-center">Willkommen!</h2>
                        <p>Sehr geehrter Herr/Frau Dr. Name</p>
                        <p> Vielen Dank für die Registrierung bei der CannaXan Arzt-App. Nach Überprüfung Ihrer Angaben, schalten wir Sie für die Nutzung der CannaXan Arzt-App frei und senden Ihnen Ihr Passwort zu, das Sie bitte bei der ersten Anmeldung ändern.</p>
                        <p> Mit freundlichen Gr<span class="text-lowercase">üß</span>en</p>
                        <p class="pb-3"> Ihr CannaXan Team</p>
                     </div>
                  </div>
                  <div class="col-md-5 align-self-end">
                     <div class="custom-btn w-100 text-right">
                        <a href="index.html" class="btn btn-info"><span>Weiter</span></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>