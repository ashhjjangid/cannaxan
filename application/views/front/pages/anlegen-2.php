<section class="same-section min-height-high">
   <div class="container-fluid">
      <div class="same-heading mb-4">
         <h2>Patientenangaben 2/3</h2>
      </div>
      <div class="min-height">
         <div class="row align-items-center">
            <div class="col-md-6">
               <?php echo form_open(current_url().'?in='.$insured_number, array('class' => 'form-horizontal ajax_form'));?>
               <div class="form-group">
                  <div class="row align-items-center">
                     <div class="col-md-9">
                        <label class="mb-0">Bestehendes Cannabis Medikament</label>
                     </div>
                     <div class="col-md-3">
                        <div class="dropdown select-box red-select <?php echo (isset($is_exist_patient->bestehendes) && $is_exist_patient->bestehendes)?'not-red':''; ?>" id="select_bestehendes_dropdown_box">
                           <button id="bestehendes_btn" class="dropdown-select dropdown-select-bold" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="red-bold"><?php echo (isset($bestehendes) && !empty($is_exist_patient->name_cannabis) && $is_exist_patient->name_cannabis)?$bestehendes:'Nein'; ?></i>
                           
                           <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu" id="select-bestehendes" aria-labelledby="bestehendes_btn">
                           
                           <li value="Yes" data-toggle="tooltip" data-placement="bottom">Ja</li>
                           <li value="No" data-toggle="tooltip" data-placement="bottom">Nein</li>
                           </ul>
                        </div>
                        <input type="hidden" name="bestehendes" id="bestehendes" class="bestehendes-field" value="<?php echo ($is_exist_patient && $is_exist_patient->bestehendes)?$is_exist_patient->bestehendes:''; ?>">
                        <!-- <div class="select-box">
                           <select class="form-control" name="bestehendes" id="bestehendes" onchange="getBestehendes()">
                              <option value="Yes" <?php echo ($is_exist_patient && $is_exist_patient->bestehendes == 'Yes') ? 'selected' : ''; ?>>Ja</option>
                              <option value="No" <?php echo ($is_exist_patient && $is_exist_patient->bestehendes == 'No') ? 'selected' : ''; ?>>Nein</option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div> -->
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6 true-type-section">
               <div class="form-group">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <label class="mb-0">Name Cannabis Medikament</label>
                     </div>
                     <div class="col-md-6">
                        <div class="dropdown select-box red-select" id="cann-box">
                        <button id="name_cannabis" class="dropdown-select text-center" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $name_cannabis_text; ?>
                           
                           <span><i class="fas fa-chevron-down"></i></span>
                        </button>
                        <ul class="dropdown-menu text-center" id="name_cannabis" aria-labelledby="name_cannabis">
                           <li value=""></li>
                           <?php if ($cannabis_medikament) { ?>

                              <?php foreach ($cannabis_medikament as $key => $value) { 

                                    $selected = '';

                                    if ($is_exist_patient && $value->id == $is_exist_patient->name_cannabis) {
                                       $selected = 'selected';
                                    }

                                 ?>
                                 <li value="<?php echo $value->id; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $value->name; ?>" <?php echo $selected; ?>><?php echo $value->name; ?> </li>
                              <?php } ?>
                           <?php } ?>
                           
                        </ul>
                        </div>
                        <input type="hidden" name="name_cannabis" id="name_cannabis" class="name_cannabis-field" value="<?php echo ($is_exist_patient && $is_exist_patient->name_cannabis)?$is_exist_patient->name_cannabis:''; ?>">
                        <!-- <div class="select-box">
                           <select class="form-control cannabis_medikament" name="name_cannabis" onchange="changeMedikament()">

                              <?php if ($cannabis_medikament) { ?>

                                 <?php foreach ($cannabis_medikament as $key => $value) { 

                                       $selected = '';

                                       if ($is_exist_patient && $value->id == $is_exist_patient->name_cannabis) {
                                          $selected = 'selected';
                                       }

                                    ?>
                                    <option value="<?php echo $value->id; ?>" <?php echo $selected; ?>><?php echo $value->name; ?> </option>
                                 <?php } ?>
                              <?php } ?>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row align-items-center true-type-section">
            <div class="col-md-6">
               <div class="form-group row align-items-center">
                  <div class="col-md-6">
                     <div class="row align-items-center">
                        <div class="col-md-6">
                           <label class="mb-0">Dosiseinheit:</label>
                        </div>
                        <div class="col-md-6">
                           <div class="select-box">
                              <input type="text" id="dosiseinheit_first" name="dosiseinheit_first" class="form-control" placeholder="" value="<?php echo ($is_exist_patient)?$is_exist_patient->dosiseinheit_first:''; ?>" readonly>
                              <!-- <select class="form-control" id="dosiseinheit_first" name="dosiseinheit_first">
                                 <option value="g" <?php if ($cannabis_medikament == "Blüten THC hoch (z.B. Bedrocan, Pedanios 22/1 etc.)") {
                                    
                                 echo ($is_exist_patient && $is_exist_patient->dosiseinheit_first == "g")?'selected':''; } ?>>g</option>
                                 <option value="ml" <?php if ($cannabis_medikament == "Tilray 25 Extrakt") {                             
                                 echo ($is_exist_patient && $is_exist_patient->dosiseinheit_first == "ml")?'selected':''; } ?>>ml</option>
                                 <option value="Kapseln" <?php if ($cannabis_medikament == '') {

                                 } echo ($is_exist_patient && $is_exist_patient->dosiseinheit_first == "Kapseln")?'selected':''; ?>>Kapseln</option>
                                 <option value="Sprühstöße" <?php if ($cannabis_medikament == "Sativex") {
                                    
                                 echo ($is_exist_patient && $is_exist_patient->dosiseinheit_first == "Sprühstöße")?'selected':''; } ?>>Sprühstöße</option>
                                 <option value="Tropfen" <?php if ($cannabis_medikament == "Dronabinol (0,83mg Dronabinol/Tropfen)") {
                                    
                                 echo ($is_exist_patient && $is_exist_patient->dosiseinheit_first == "Tropfen")?'selected':''; } ?>>Tropfen</option>
                              </select>
                              <span><i class="fas fa-chevron-down"></i></span> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="row align-items-center">
                        <div class="col-md-6">
                           <label class="mb-0">Tagesdosis:</label>
                        </div>
                        <div class="col-md-6">
                           <input type="text" class="form-control red-input text-center" id="tagesdosis_first" name="tagesdosis_first" placeholder="Tagesdosis" onkeyup="calculatevalues()" value="<?php echo str_replace('.', ',', ($is_exist_patient)?$is_exist_patient->tagesdosis_first:''); ?>">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <div class="row align-items-center">
                     <div class="col-md-12">
                        <div class="row align-items-center">
                           <div class="col-md-6">
                              <label>Tagesdosis: <span>(mg THC)</span></label>
                           </div>
                           <div class="col-md-6">
                              <input type="text" id="tagesdosis_mg_first" name="tagesdosis_mg_first" class="form-control text-center" placeholder="Tagesdosis (mg THC)" value="<?php echo str_replace('.', ',', round(($is_exist_patient)?$is_exist_patient->tagesdosis_mg_first:'',1)); ?>"  readonly>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-7">
               <label class="mt-3 w-100">Grund für Umstellung: (Mehrfachnennung möglich )</label>
               <div class="inline-radio line">
                  <ul>
                     <li>
                           
                        <?php $grund_fur_umstellung_values = ($is_exist_patient)?$is_exist_patient->grund_fur_umstellung:'';
                           $grund_fur_umstellung_values = explode(',', $grund_fur_umstellung_values);
                           //pr($grund_fur_umstellung_values);
                        ?>

                              

                        <div class="checkbox-custom">
                           <input type="checkbox" id="chea1" name="grund_fur_umstellung[]" value="Nebenwirkungen" 
                           <?php foreach ($grund_fur_umstellung_values as $key => $value) { ?>
                           <?php if ($value == "Nebenwirkungen") { 
                              
                           echo ($value == "Nebenwirkungen")?'checked':''; } }?>>
                           <label for="chea1">
                              <span class="cheak"></span>Nebenwirkungen
                           </label>
                        </div>
                     </li>
                     <li>
                        <div class="checkbox-custom">

                           <input type="checkbox" id="radio-2" name="grund_fur_umstellung[]" value="Ungenügende Wirkung" <?php foreach ($grund_fur_umstellung_values as $key => $value) { if ($value == "Ungenügende Wirkung") {
                              
                           echo ($value == "Ungenügende Wirkung")?'checked':''; } } ?>>
                           <label for="radio-2">
                              <span class="cheak"></span>Ungenügende Wirkung
                           </label>

                        </div>
                     </li>
                     <li>
                        <div class="checkbox-custom">
                           <input type="checkbox" id="radio-3" name="grund_fur_umstellung[]" value="Fehlende Kostenübernahme" <?php foreach ($grund_fur_umstellung_values as $key => $value) { if ($value == "Fehlende Kostenübernahme") { echo ($value == "Fehlende Kostenübernahme")?'checked':''; } } ?>>
                           <label for="radio-3">
                              <span class="cheak"></span>Fehlende Kostenübernahme
                           </label>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="col-md-5">
               <div class="text-area mt-5">
                  <p>Anderer Grund</p>
                  <textarea class="form-control" name="anderer_grund" placeholder=""><?php echo ($is_exist_patient)?$is_exist_patient->anderer_grund:''; ?></textarea>
               </div>
            </div>
         </div>
      
         <!-- <div class="row form-group align-items-center true-type-section">
            <div class="col-md-6">
               <label class="mb-0">Name eines Nicht zuordenbares Cannabis Medikament:</label>
            </div>
            <div class="col-md-6">
               <input type="text" name="name_eines_nicht" class="form-control red-input" placeholder="Nicht zuordenbares Cannabis Medikament" value="<?php echo ($is_exist_patient)?$is_exist_patient->name_eines_nicht:''; ?>">
            </div>
         </div> -->
         <!-- <div class="row form-group true-type-section">
            <div class="col-md-3">
               <div class="row align-items-center">
                  <div class="col-md-6">
                     <label class="mb-0"> Dosiseinheit:</label>
                  </div>
                  <div class="col-md-6">
                     <div class="dropdown select-box red-select" id="dosiseinheit_second">
                        <button id="dosiseinheit_second_btn" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $is_exist_patient->dosiseinheit_second; ?>
                        
                        <span><i class="fas fa-chevron-down"></i></span>
                        </button>
                        <ul class="dropdown-menu" id="select-dosiseinheit_second" aria-labelledby="dosiseinheit_second_btn">

                        <li value=""></li>  
                        <li value="g" data-toggle="tooltip" data-placement="bottom">g</li>
                        <li value="ml" data-toggle="tooltip" data-placement="bottom">mL</li>
                        <li value="Kapseln" data-toggle="tooltip" data-placement="bottom">Kapseln</li>
                        <li value="Sprühstöße" data-toggle="tooltip" data-placement="bottom">Sprühstöße</li>
                        <li value="Tropfen" data-toggle="tooltip" data-placement="bottom">Tropfen</li>
                        </ul>
                     </div>
                     <input type="hidden" name="dosiseinheit_second" id="dosiseinheit_second" class="dosiseinheit_second-field" value="<?php echo ($is_exist_patient && $is_exist_patient->dosiseinheit_second)?$is_exist_patient->dosiseinheit_second:''; ?>">
                     <div class="select-box">
                        <select class="form-control" name="dosiseinheit_second"> 
                           <option value="g" <?php echo ($is_exist_patient && $is_exist_patient->dosiseinheit_second == "g")?'selected':''; ?>>g</option>
                           <option value="ml" <?php echo ($is_exist_patient && $is_exist_patient->dosiseinheit_second == "mL")?'selected':''; ?>>mL</option>
                           <option value="Kapseln" <?php echo ($is_exist_patient && $is_exist_patient->dosiseinheit_second == "Kapseln")?'selected':''; ?>>Kapseln</option>
                           <option value="Sprühstöße" <?php echo ($is_exist_patient && $is_exist_patient->dosiseinheit_second == "Sprühstöße")?'selected':''; ?>>Sprühstöße</option>
                           <option value="Tropfen" <?php echo ($is_exist_patient && $is_exist_patient->dosiseinheit_second == "Tropfen")?'selected':''; ?>>Tropfen</option>
                        </select>
                        <span><i class="fas fa-chevron-down"></i></span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="row align-items-center">
                  <div class="col-md-6">
                     <label class="mb-0"> Tagesdosis:</label>
                  </div>
                  <div class="col-md-6">
                     <input type="text" name="tagesdosis_second" class="form-control red-input text-center" placeholder="Tagesdosis" value="<?php echo str_replace('.', ',', ($is_exist_patient)?$is_exist_patient->tagesdosis_second:''); ?>">
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="row align-items-center">
                  <div class="col-md-6">
                     <label class="mb-0"> Tagesdosis: <span>(mg THC)</span></label>
                  </div>
                  <div class="col-md-6">
                     <input type="text" name="tagesdosis_mg_thc_second" class="form-control red-input text-center" placeholder="Tagesdosis (mg THC)" value="<?php echo str_replace('.', ',', ($is_exist_patient)?$is_exist_patient->tagesdosis_mg_thc_second:''); ?>">
                  </div>
               </div>
            </div>
         </div> -->
         <div class="row false-type-section">
            
         </div>
      </div>
      <div class="w-100 text-right mt-5">
         <button class="btn btn-primary" type="submit"><span>Speichern & weiter</span></button>
      </div>
   </div>
</section>

<script type="text/javascript">

   $(document).ready(function() {
      var data_exists = $('.bestehendes-field').val();
      if (data_exists) {

         $('#select_bestehendes_dropdown_box').addClass('not-red');
      }  
      getBestehendes();   
   })
   function getBestehendes() 
   {
      var t_type = $('.bestehendes-field').val();
      // alert(t_type)
      if (t_type == 'Yes') {
         $('.true-type-section').show();
         $('.false-type-section').hide();
      } else {
         $('.true-type-section').hide();
         $('.false-type-section').show();
      }
   }

   function changeMedikament() {
      var cannabis_medikament = $('.cannabis_medikament').val();
      if (cannabis_medikament == "1") {
         $('#dosiseinheit_first').val("Sprühstöße");
      } else if (cannabis_medikament == "2") {
         $('#dosiseinheit_first').val("Tropfen");
      } else if (cannabis_medikament == "3") {
         $('#dosiseinheit_first').val("g");
      } else if (cannabis_medikament == "4") {
         $('#dosiseinheit_first').val("mL");
      } else if (cannabis_medikament == "5") {
         $('#dosiseinheit_first').val("mL");
      } else if (cannabis_medikament == "6") {
         $('#dosiseinheit_first').val("mL");
      } else if (cannabis_medikament == "7") {
         $('#dosiseinheit_first').val("Tropfen");
      } else if (cannabis_medikament == "8") {
         $('#dosiseinheit_first').val("Tropfen");
      } else if (cannabis_medikament == "9") {
         $('#dosiseinheit_first').val("mL");
      }
   }

   function calculatevalues() {
      var cannabis_medikament = $('.name_cannabis-field').val();
      if (cannabis_medikament == "1") {
         var tagesdosis_first = $('#tagesdosis_first').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 2.7*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis_mg_first').val(tagesdosis_first_round_off);
      } else if (cannabis_medikament == "2") {
         var tagesdosis_first = $('#tagesdosis_first').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 0.83*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis_mg_first').val(tagesdosis_first_round_off);
         
      } else if (cannabis_medikament == "3") {
         var tagesdosis_first = $('#tagesdosis_first').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 0.2*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis_mg_first').val(tagesdosis_first_round_off);
      } else if (cannabis_medikament == "4") {
         var tagesdosis_first = $('#tagesdosis_first').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 25*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis_mg_first').val(tagesdosis_first_round_off);
      } else if (cannabis_medikament == "5") {
         var tagesdosis_first = $('#tagesdosis_first').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 10*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis_mg_first').val(tagesdosis_first_round_off);
      } else if (cannabis_medikament == "6") {
         var tagesdosis_first = $('#tagesdosis_first').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 5*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis_mg_first').val(tagesdosis_first_round_off);
      } else if (cannabis_medikament == "7") {
         var tagesdosis_first = $('#tagesdosis_first').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 0.6*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis_mg_first').val(tagesdosis_first_round_off);
      } else if (cannabis_medikament == "8") {
         var tagesdosis_first = $('#tagesdosis_first').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 0.6*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis_mg_first').val(tagesdosis_first_round_off);
      } else if (cannabis_medikament == "9") {
         var tagesdosis_first = $('#tagesdosis_first').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 50*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis_mg_first').val(tagesdosis_first_round_off);
      }
   }

   $('#name_cannabis li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).val();
      //alert(getValue);
     $('#name_cannabis').text(getText);
     $('.name_cannabis-field').val(getValue);
     $('#cann-box').addClass('not-red');
      $('#tagesdosis_first').val('');
      $('#tagesdosis_mg_first').val('');

      if (getValue == '') {
         $('#dosiseinheit_first').val('');
      }

     if (getValue == "1") {
         $('#dosiseinheit_first').val("Sprühstöße");
      } else if (getValue == "2") {
         $('#dosiseinheit_first').val("Tropfen");
      } else if (getValue == "3") {
         $('#dosiseinheit_first').val("g");
      } else if (getValue == "4") {
         $('#dosiseinheit_first').val("mL");
      } else if (getValue == "5") {
         $('#dosiseinheit_first').val("mL");
      } else if (getValue == "6") {
         $('#dosiseinheit_first').val("mL");
      } else if (getValue == "7") {
         $('#dosiseinheit_first').val("Tropfen");
      } else if (getValue == "8") {
         $('#dosiseinheit_first').val("Tropfen");
      } else if (getValue == "9") {
         $('#dosiseinheit_first').val("mL");
      }
   });

   $('#select_bestehendes_dropdown_box li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).attr('value');
      
      //alert(newValue);
     $('#bestehendes_btn').text(getText);
     $('.bestehendes-field').val(getValue);
     $('#select_bestehendes_dropdown_box').addClass('not-red');
     getBestehendes();
   });

   $('#dosiseinheit_second li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).val();
      //alert(getText);
      //alert(getValue);
     $('#dosiseinheit_second_btn').text(getText);
     $('.dosiseinheit_second-field').val(getText);
     $('#dosiseinheit_second').addClass('not-red');
   });
</script>