<section class="same-section min-height-high">
 <div class="container-fluid">
    <div class="same-heading mb-4">
       <h2>Patientenangaben 3/3</h2>
    </div>
    <div class="min-height">
      <div class="row">
         <div class="col-md-7">
            <label>Relevante Begleiterkrankung</label>
         </div>
         <div class="col-md-5">
            <label>Bestehend seit</label>
         </div>
      </div>
      <?php echo form_open(current_url().'?in='.$insured_number, array('class' => 'ajax_form'));?>
      <div class="row justify-content-center">
         <div class="col-md-9">
            <div class="row mt-3">
               <div class="col-8"></div>
               <div class="col-4">
                  <div class="row custom-row text-center">
                     <div class="col"><label>> 3</label></div>
                     <div class="col"><label>> 6</label></div>
                     <div class="col"><label>> 12</label></div>
                     <div class="col"><label>Monaten</label></div>
                  </div>
               </div>
            </div>
            <div class="row-list">
               <div class="append-row-section">
               <?php if(isset($begleiterkrankungen) && !empty($begleiterkrankungen)) { ?>

                  <?php foreach ($begleiterkrankungen as $key => $value) { ?>
                     <div class="row align-items-center options-rows">
                        <div class="col-md-8">
                           <div class="no-input">
                              <strong><?php echo $key + 1; ?></strong>
                              <input type="text" name="relevante[<?php echo $key + 1; ?>][description]" class="form-control red-input" placeholder="" value="<?php echo $value->description ?>">
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="row list-cheak">
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                    <input type="radio" name="relevante[<?php echo $key + 1; ?>][option]" value="3" id="me-<?php echo $key; ?>" <?php echo $value->bestehend_seit == '3'? 'checked':''; ?>>
                                    <label for="me-<?php echo $key; ?>"><span class="cheak"></span></label>
                                 </div>
                              </div>
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                       <input type="radio" name="relevante[<?php echo $key + 1; ?>][option]" value="6" id="me-1-<?php echo $key; ?>" <?php echo $value->bestehend_seit == '6'? 'checked':''; ?>>
                                       <label for="me-1-<?php echo $key; ?>"><span class="cheak"></span></label>
                                    </div>
                              </div>
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                       <input type="radio" name="relevante[<?php echo $key + 1; ?>][option]" value="12" id="me-3-<?php echo $key; ?>" <?php echo $value->bestehend_seit == '12'? 'checked':''; ?>>
                                       <label for="me-3-<?php echo $key; ?>"><span class="cheak"></span></label>
                                    </div>
                              </div>
                              <div class="col cheak-width"></div>
                           </div>
                        </div>
                     </div>
                  <?php } ?>
               <?php } else { ?>
                  <div class="row align-items-center options-rows">
                     <div class="col-md-8">
                        <div class="no-input">
                           <strong>1</strong>
                           <input type="text" name="relevante[1][description]" class="form-control" placeholder="">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="row list-cheak">
                           <div class="col ">
                              <div class="checkbox-custom small">
                                 <input type="radio" name="relevante[1][option]" value="3" id="me">
                                 <label for="me"><span class="cheak"></span></label>
                              </div>
                           </div>
                           <div class="col ">
                              <div class="checkbox-custom small">
                                    <input type="radio" name="relevante[1][option]" value="6" id="me-1">
                                    <label for="me-1"><span class="cheak"></span></label>
                                 </div>
                           </div>
                           <div class="col ">
                              <div class="checkbox-custom small">
                                    <input type="radio" name="relevante[1][option]" value="12" id="me-3">
                                    <label for="me-3"><span class="cheak"></span></label>
                                 </div>
                           </div>
                           <div class="col cheak-width"></div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="row align-items-center options-rows">
                     <div class="col-md-8">
                        <div class="no-input">
                           <strong>2</strong>
                           <input type="text" name="relevante[2][description]" class="form-control" placeholder="">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="row list-cheak">
                           <div class="col ">
                              <div class="checkbox-custom small">
                                 <input type="radio" id="me1" name="relevante[2][option]" value="3">
                                 <label for="me1"><span class="cheak"></span></label>
                              </div>
                           </div>
                           <div class="col ">
                              <div class="checkbox-custom small">
                                    <input type="radio" name="relevante[2][option]" value="6" id="me-12">
                                    <label for="me-12"><span class="cheak"></span></label>
                                 </div>
                           </div>
                           <div class="col ">
                              <div class="checkbox-custom small">
                                    <input type="radio" name="relevante[2][option]" value="12" id="me3">
                                    <label for="me3"><span class="cheak"></span></label>
                                 </div>
                           </div>
                           <div class="col cheak-width"></div>
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center options-rows">
                     <div class="col-md-8">
                        <div class="no-input">
                           <strong>3</strong>
                           <input type="text" name="relevante[3][description]" class="form-control" placeholder="">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="row list-cheak">
                           <div class="col ">
                              <div class="checkbox-custom small">
                                 <input type="radio" name="relevante[3][option]" value="3" id="me10">
                                 <label for="me10"><span class="cheak"></span></label>
                              </div>
                           </div>
                           <div class="col ">
                              <div class="checkbox-custom small">
                                    <input type="radio" name="relevante[3][option]" value="6" id="me-120">
                                    <label for="me-120"><span class="cheak"></span></label>
                                 </div>
                           </div>
                           <div class="col ">
                              <div class="checkbox-custom small">
                                    <input type="radio" name="relevante[3][option]" value="12" id="me33">
                                    <label for="me33"><span class="cheak"></span></label>
                                 </div>
                           </div>
                           <div class="col cheak-width"></div>
                        </div>
                     </div>
                  </div> -->
                  <?php } ?>
               </div>
               <div class="row align-items-center">
                  <div class="col-md-8">
                     <div class="no-input">
                        <button class="plus-btn text-left" onclick="addRow()" type="button">
                           <strong><i class="fas fa-plus"></i></strong>
                        </button>
                     </div>
                  </div>
                  <div class="col-md-4">
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
    <div class="w-100 text-right btn-ro-space">
       <button type="submit" class="btn btn-primary"><span>Speichern & Weiter</span></button>
    </div>
  </form>
  </div>
</section>
<script type="text/javascript">
  function addRow(){
    var total_row = $('.options-rows').length;

    $.ajax({
      url: "<?php echo base_url('patients/add_begleiterkrankungen_row'); ?>",
      type: 'post',
      dataType: 'json',
      data: {digit:total_row},
      success: function(data) {
        $('.append-row-section').append(data.html);
      }
    })
  }
</script>