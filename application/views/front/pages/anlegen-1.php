<section class="same-section">
   <div class="container-fluid">
      <div class="same-heading mb-4">
         <h2>Patientenangaben 1/3</h2>
      </div>
      <div class="row">
         <div class="col-md-8">
            <div class="row align-items-center">
               <div class="col-md-8">
                  <?php echo form_open(current_url().'?in='.$insured_number, array('class' => 'form-horizontal ajax_form'));?>
                  <div class="form-group">
                     <div class="row align-items-center">
                        <div class="col-md-6">
                           <label class="m-0">Versichertennummer:</label>
                        </div>
                        <div class="col-md-6">
                           <input type="text" name="versichertennummer" class="form-control red-input" placeholder="Versichertennummer" value="<?php echo $insured_number; ?>">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <div class="row align-items-center">
                        <div class="col-md-6">
                           <label class="m-0">Fallnummer:</label>
                        </div>
                        <div class="col-md-6">
                           <input type="text" name="fallnummer" class="form-control" placeholder="Fallnummer" value="<?php echo ($fallnummer)?$fallnummer:''; ?>" readonly>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group">
               <div class="row align-items-center">
                  <div class="col-md-6">
                     <label class="m-0">Diagnose Code <!-- (ICD 10-GM) --></label>
                  </div>
                  <div class="col-md-6">
                     <input type="text" name="diagnose_code" id="diagnose_code" class="form-control" placeholder="Diagnose Code" readonly="" value="<?php echo ($is_exist_patient)?$is_exist_patient->diagnostic_code:''; ?>">
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <div class="form-group">
               <div class="row align-items-center">
                  <div class="col-md-4">
                     <label class="m-0">Name</label>
                  </div>
                  <div class="col-md-8">
                     <input type="text" name="name" class="form-control blue-input" placeholder="Name" value="<?php echo ($is_exist_patient)?$is_exist_patient->last_name:''; ?>">
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group">
               <div class="row align-items-center">
                  <div class="col-md-6">
                     <label class="m-0">Geburtsdatum</label>
                  </div>
                  <div class="col-md-6">
                    <!--  <input type="text" name="geburtsdatum" class="form-control geburtsdatum red-input" placeholder="TT.MM.JJJJ" readonly="" value="<?php echo $geburtsdatum; ?>"> -->
                    <div class="inline-dropdown">
                       <div class="dropdown select-box red-select date " id="date_of_birth_date">
                           <button id="date_of_birth_date_btn" class="dropdown-select <?php echo ($is_exist_patient && $date_of_birth_date)?'':'dropdown-blue'; ?>  " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($date_of_birth_date)?$date_of_birth_date:''; ?>
                             
                             <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu" id="select-date-of-birth-date" aria-labelledby="date_of_birth_date_btn">
                             
                           <?php for ($i=1; $i <= 31 ; $i++) { ?>
                              <li value="<?php echo $i; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $i; ?></li>
                              
                           <?php } ?>
                           </ul>
                            <input type="hidden" name="date_of_birth_date" id="date_of_birth_date_input" class="date_of_birth_date_input-field" value="<?php echo ($date_of_birth_date)?$date_of_birth_date:''; ?>">
                        </div>

                        <div class="dropdown select-box red-select month " id="date_of_birth_month">
                           <button id="date_of_birth_month_btn" class="dropdown-select <?php echo ($is_exist_patient && $date_of_birth_date)?'':'dropdown-blue'; ?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($date_of_birth_month)?$date_of_birth_month:''; ?>
                             
                             <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu" id="select-date-of-birth-month" aria-labelledby="date_of_birth_month_btn">
                             
                           <?php //for ($i=0; $i <= 12 ; $i++) { ?>
                              <li value="1" data-toggle="tooltip" data-placement="bottom">Jan</li>
                              <li value="2" data-toggle="tooltip" data-placement="bottom">Feb</li>
                              <li value="3" data-toggle="tooltip" data-placement="bottom">Mar</li>
                              <li value="4" data-toggle="tooltip" data-placement="bottom">Apr</li>
                              <li value="5" data-toggle="tooltip" data-placement="bottom">May</li>
                              <li value="6" data-toggle="tooltip" data-placement="bottom">Jun</li>
                              <li value="7" data-toggle="tooltip" data-placement="bottom">Jul</li>
                              <li value="8" data-toggle="tooltip" data-placement="bottom">Aug</li>
                              <li value="9" data-toggle="tooltip" data-placement="bottom">Sep</li>
                              <li value="10" data-toggle="tooltip" data-placement="bottom">Oct</li>
                              <li value="11" data-toggle="tooltip" data-placement="bottom">Nov</li>
                              <li value="12" data-toggle="tooltip" data-placement="bottom">Dec</li>
                              
                           <?php //} ?>
                           </ul>
                            <input type="hidden" name="date_of_birth_month" id="date_of_birth_month_input" class="date_of_birth_month_input-field" value="<?php echo ($dob_month)?$dob_month:''; ?>">
                        </div>

                        <div class="dropdown select-box red-select year " id="date_of_birth_year">
                           <button id="date_of_birth_year_btn" class="dropdown-select <?php echo ($is_exist_patient && $date_of_birth_date)?'':'dropdown-blue'; ?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($date_of_birth_year)?$date_of_birth_year:''; ?>
                             
                             <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu" id="select-date-of-birth-year" aria-labelledby="date_of_birth_year_btn">
                             
                           <?php
                              $current_year = date('Y');
                              $from_year = date('Y') - 100;
                              
                            for ($i=$current_year; $i >= $from_year ; $i--) { ?>

                              <li value="<?php echo $i; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $i; ?></li>
                              
                           <?php } ?>
                           </ul>
                            <input type="hidden" name="date_of_birth_year" id="date_of_birth_year_input" class="date_of_birth_year_input-field" value="<?php echo ($date_of_birth_year)?$date_of_birth_year:''; ?>">
                        </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <div class="form-group">
               <div class="row align-items-center">
                  <div class="col-md-4">
                     <label class="m-0">VorName</label>
                  </div>
                  <div class="col-md-8">
                     <input type="text" name="vorname" class="form-control blue-input" placeholder="Vorname" value="<?php echo ($is_exist_patient)?$is_exist_patient->first_name:''; ?>">
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group">
               <div class="row align-items-center">
                  <div class="col-md-6">
                     <label class="m-0">Gewicht (kg)</label>
                  </div>
                  <div class="col-md-6">
                     <input type="text" name="gewicht" class="form-control" placeholder="Gewicht (kg)" readonly="" value="<?php echo ($is_exist_patient)?$is_exist_patient->weight:''; ?>">
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <div class="form-group">
               <div class="row align-items-center">
                  <div class="col-md-4">
                     <label class="m-0">Telefonnummer*</label>
                  </div>
                  <div class="col-md-8">
                     <input type="text" name="telefonnummer" class="form-control blue-input red-input" placeholder="+49 ..." value="<?php echo ($is_exist_patient)?$is_exist_patient->telephone_number:''; ?>">
                      <p class="input-messge">*erforderlich für Apotheker wegen gesetzl. Beratungspflicht</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group">
               <div class="row align-items-center">
                  <div class="col-md-6">
                     <label class="m-0">Größe (cm)</label>
                  </div>
                  <div class="col-md-6">
                     <input type="text" name="grobe" class="form-control" placeholder="Größe (cm)" readonly="" value="<?php echo ($is_exist_patient)?$is_exist_patient->size:''; ?>">
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4 offset-md-8">
            <div class="form-group upword">
               <div class="row align-items-center">
                  <div class="col-md-6">
                     <label class="m-0">Geschlecht</label>
                  </div>
                  <div class="col-md-6">
                     <input type="text" name="geschlecht" class="form-control" placeholder="" readonly="Geschlecht" value="<?php echo ($is_exist_patient)?$is_exist_patient->gender:''; ?>">
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="input-space">
         <div class="row align-items-center">
            <div class="col custom-label-bx">
               <label>Hauptindikation:</label>
            </div>
            <div class="col">
               <div class="dropdown select-box" id="hauptindikation-box">
                 <button id="hauptindikation" class="dropdown-select dropdown-select-bold <?php echo ($is_exist_patient && $is_exist_patient->hauptindikation)?'':'red-input'; ?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $hauptindikation_text; ?>
                   
                   <span><i class="fas fa-chevron-down"></i></span>
                 </button>
                 <ul class="dropdown-menu" id="hauptindikation" aria-labelledby="hauptindikation">

                   <?php if ($icd_code_zuordnung) { ?>
                                                
                        <?php foreach ($icd_code_zuordnung as $key => $value) { 
                              $selected = '';

                              if ($is_exist_patient && ($is_exist_patient->hauptindikation == $value->id)) {
                                 $selected = 'selected';
                              }
                           ?>
                           <li value="<?php echo $value->id; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $value->indikation; ?>" data-icd="<?php echo $value->icd_10; ?>" <?php echo $selected; ?>><?php echo $value->indikation; ?></li>
                        <?php } ?>
                     <?php } ?>
                 </ul>
               </div>
               <input type="hidden" name="hauptindikation" id="hauptindikation_indikation" class="hauptindikation-field" value="<?php echo ($is_exist_patient && $is_exist_patient->hauptindikation)?$is_exist_patient->hauptindikation:''; ?>">
               <!-- <div class="select-box">
                  <select class="form-control red-select" name="hauptindikation" id="hauptindikation" onchange="getHauptindikation(this)">
                     
                     <?php if ($icd_code_zuordnung) { ?>
                        <option value="">Select Hauptindikation</option>
                        
                        <?php foreach ($icd_code_zuordnung as $key => $value) { 
                              $selected = '';

                              if ($is_exist_patient && ($is_exist_patient->hauptindikation == $value->id)) {
                                 $selected = 'selected';
                              }
                           ?>
                           <option value="<?php echo $value->id; ?>" data-icd="<?php echo $value->icd_10; ?>" <?php echo $selected; ?>><?php echo $value->indikation; ?></option>
                        <?php } ?>
                     <?php } ?>
                  </select>
                  <span><i class="fas fa-chevron-down"></i></span>
               </div> -->
               <!-- <input type="text" name="hauptindikation" class="form-control" placeholder="Rückenschmerzen, nicht näher bezeichnet : Nicht näher bezeichnete Lokalisation"> -->
            </div>
         </div>
      </div>
      <!-- <div class="text-area-box">
         <div class="row align-items-center">
            <div class="col-md-2"></div>
            <div class="col-md-8">
               <div class="text-area">
                  <p>Nicht zuordenbare Hauptindikation:</p>
                  <textarea class="form-control" name="nicht" placeholder=""><?php //echo ($is_exist_patient)?$is_exist_patient->main_indication_not_assigned:''; ?></textarea>
               </div>
            </div>
         </div>
      </div> -->
      <div class="row">
         <div class="col custom-label-bx"></div>
         <div class="col">
            <div class="page-cheak-box ml-0">
               <div class="checkbox-custom">
                  <input type="checkbox" id="me" name="is_chronic_indication" <?php echo ($is_exist_patient && $is_exist_patient->is_chronic_indication == "Yes")?'checked':'';  ?>>
                  <label for="me"><span class="cheak"></span>Chronische Indikation seit mindestens 3 Monaten und mit den vorhandenen Therapieoptionen keine ausreichende Linderung</label>
               </div>
            </div>
         </div>
      </div>
      <div class="row align-items-center">
         <div class="col-md-4 ">
            <div class="radio-left-text">
               <p>MODERATE BIS SCHWERE SCHMERZEN: <br> <strong> ≥ 4 AUF DER NRS SKALA</strong> (0 – 10)</p>
            </div>
         </div>
         <div class="col-md-8 pl-0">
            <div class="inline-radio inline-radio-2  ">
               <?php 
                  $checked_first = ''; 
                  $checked_second = '';

                  if ($is_exist_patient) {
                     
                     if ($is_exist_patient->pain_scale == 'Not Applicable') {
                        $checked_first = 'checked';
                     } else {
                        $checked_second = 'checked';
                     }

                  } else {
                     $checked_first = 'checked';

                  }

               ?>
               <ul>
                  <li>
                     <div class="radio-box">
                        <input type="radio" name="pain_scale" value="Not Applicable" id="radio"  <?php echo $checked_first; ?>>
                        <label for="radio">
                           <span class="radio-cheak"></span>
                           Nicht zutreffend
                        </label>
                     </div>
                  </li>
                  <li>
                     <div class="radio-box">
                        <input type="radio" name="pain_scale" value="Applicable" id="radio-2" <?php echo $checked_second; ?>>
                        <label for="radio-2">
                           <span class="radio-cheak"></span>
                           zutreffend
                        </label>
                     </div>
                  </li>
               </ul>

            </div>
            <!-- <div class="radio-select-box">
               <div class="row align-items-center">
                  <div class="col-md-3">
                        <div class="dropdown select-box red-select foroget-password-cover nein-cover" id="select_is_patient_signed">
                        <button id="is_patient_signed_btn" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ja
                          
                          <span><i class="fas fa-chevron-down"></i></span>
                        </button>
                        <ul class="dropdown-menu" id="select-is_patient_signed" aria-labelledby="is_patient_signed_btn">
                          
                         <li value="1" data-toggle="tooltip" data-placement="bottom">Ja</li>
                          <li value="0" data-toggle="tooltip" data-placement="bottom">Nein</li>
                        </ul>
                         <input type="hidden" name="is_patient_signed" id="is_patient_signed" class="is_patient_signed-field" value="Yes">
                        <div id="foroget_password_box" style="display: none;">
                           
                           <p>Die Speicherung der Patientenangaben ist erst möglich, wenn der Patient die Dateneinverständniserklärung unterschrieben hat.</p>
                        </div>
                      </div>
                     
                  </div>
                  <div class="col-md-9">
                     <label class="m-0">Patient hat Dateneinverständniserklärung unterzeichnet:</label>
                  </div>
               </div>
            </div> -->
         </div>

      </div>
      <div class="col-md-12">
         <label class="mb-0 mt-4">MAINZER STADIENMODELL DER SCHMERZCHRONIFIZIERUNG:</label>
         <div class="inline-radio inline-radio-three">
            <ul>
               <li>
                  <div class="radio-box">
                     <input type="checkbox" class="grad" onclick="singlecheck(this)" name="grad" value="grad 1" id="grad-1" <?php echo (isset($is_exist_patient->grad) && $is_exist_patient->grad == 'grad 1')?'checked':''; ?>>
                     <label for="grad-1">
                        <span class="radio-cheak"><span></span></span>
                        Grad 1
                     </label>
                  </div>
               </li>
               <li>
                  <div class="radio-box">
                     <input type="checkbox" class="grad" onclick="singlecheck(this)" name="grad" value="grad 2" id="grad-2" <?php echo (isset($is_exist_patient->grad) && $is_exist_patient->grad == 'grad 2')?'checked':''; ?>>
                     <label for="grad-2">
                        <span class="radio-cheak"><span></span></span>
                        Grad 2
                     </label>
                  </div>
               </li>
               <li>
                  <div class="radio-box">
                     <input type="checkbox" class="grad" onclick="singlecheck(this)" name="grad" value="grad 3" id="grad-3" <?php echo (isset($is_exist_patient->grad) && $is_exist_patient->grad == 'grad 3')?'checked':''; ?>>
                     <label for="grad-3">
                        <span class="radio-cheak"><span></span></span>
                        Grad 3
                     </label>
                  </div>
               </li>
            </ul>
         </div>
      </div>
      <div class="row align-items-center">
         <div class="col-md-4 ">
            <div class="radio-left-text">
                
               <p>TEILNAHME PATIENT AN RTA </br> (RETROSPEKTIVE THERAPIEFALLAUSWERTUNG)</p>
            </div>
         </div>
         <div class="col-md-8 pl-0">
            <div class="inline-radio inline-radio-2  ">

               <ul>
                  <li>
                     <div class="radio-box">
                        <input type="radio" name="nrs_scale" value="Yes" id="radio4" <?php echo (isset($is_exist_patient->grad) && $is_exist_patient->nrs_scale == 'Yes')?'checked':''; ?>>
                        <label for="radio4">
                           <span class="radio-cheak"><span></span></span>
                           ja
                        </label>
                     </div>
                  </li>
                  <li>
                     <div class="radio-box">
                        <input type="radio" name="nrs_scale" value="No" id="radio-5" <?php echo (isset($is_exist_patient->grad) && $is_exist_patient->nrs_scale == 'No')?'checked':''; ?>>
                        <label for="radio-5">
                           <span class="radio-cheak"><span></span></span>
                           nein
                        </label>
                     </div>
                  </li>
               </ul>

            </div>
         </div>

      </div>

      <div class="w-100 text-right">
         <button class="btn btn-primary" type="submit"><span>Speichern & weiter</span></button>
      </div>
      </div>
   </div>
</section> 

<script type="text/javascript">
   
   $(document).ready(function() {
      var data_exists = $('#hauptindikation_indikation').val();
      if (data_exists) {

         $('#hauptindikation-box').addClass('not-red');
      }      
   }) 


   $(function(){
      $('.geburtsdatum').datepicker({
         format: 'dd.mm.yyyy'
      })
   })

   function singlecheck($this) {
      var checked_row = $($this).is(':checked');
      
      if (checked_row  == true) {
        $(':checkbox').closest('div.radio-box').find(".grad").prop('checked', false);
        $($this).prop('checked', true);
      }
   }

   function getHauptindikation($this)
   {
      var hauptindikation = $('#hauptindikation li').attr('data-icd');
      alert(hauptindikation);
      $('#diagnose_code').val(hauptindikation);
   }

   function requiredPatientSigned(data) {

      if (!data.success) {
         $('.foroget_password_box').css('display', 'block');
      }
   }

   /*function checkPatientSigned($this) {
      if ($($this).val() == '1') {
         $('#foroget_password_box').css('display', 'none');
      } else {
         $('#foroget_password_box').css('display', 'block');
      }
   }*/

   $('#hauptindikation li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).val();
     var hauptindikation = $(this).attr('data-icd');
      //alert(hauptindikation);
      $('#diagnose_code').val(hauptindikation);
     $('#hauptindikation').text(getText);
     $('.hauptindikation-field').val(getValue);
     $('#hauptindikation-box').find('.red-input').removeClass('red-input');
   }); 

   $('#select_is_patient_signed li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).val();
      //alert(getValue);
      if (getValue == "1") {
         newText = "Ja";
         newValue = "Yes";
      } else {
         newText = "Nein";
         newValue = "No";
      }

         //checkPatientSigned(this);
      //alert(newValue);
     $('#is_patient_signed_btn').text(newText);
     $('.is_patient_signed-field').val(newValue);
     $('#select_is_patient_signed').addClass('not-red');
   });

   $('#select-date-of-birth-date li').click(function() {
     var getText = $(this).text(); 
     $('#date_of_birth_date_btn').removeClass('dropdown-blue');
      
     var getValue = $(this).attr('value');
      //alert(getValue);
      //alert(newValue);
     $('#date_of_birth_date_btn').text(getText);
     $('.date_of_birth_date_input-field').val(getText);
     //$('#select_is_patient_signed').addClass('not-red');
   });

   $('#select-date-of-birth-month li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).attr('value');
     //alert(getValue); 
     $('#date_of_birth_month_btn').removeClass('dropdown-blue');
     
     $('#date_of_birth_month_btn').text(getText);
     $('.date_of_birth_month_input-field').val(getValue);
     //$('#select_is_patient_signed').addClass('not-red');
   });

   $('#select-date-of-birth-year li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).attr('value');
      $('#date_of_birth_year_btn').removeClass('dropdown-blue');
     $('#date_of_birth_year_btn').text(getText);

     $('.date_of_birth_year_input-field').val(getText);
     //$('#select_is_patient_signed').addClass('not-red');
   });
</script>