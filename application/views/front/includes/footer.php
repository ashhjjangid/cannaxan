      <footer>
         <div class="container-fluid">
            <div class="footer-box">
               <div class="container">
                  <div class="row">
                     <div class="col-md-3 align-self-end">
                        <div class="footer-nav">
                           <ul>
                              <li><a href="https://www.cannaxan.de/gmbh/impressum" target="_blank">Impressum</a></li>
                              <!-- <li><a href="#">AGB</a></li> -->
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="footer-nav text-center"> 
                           <ul>
                              <li>Tel.:+49 8024 46869 70</li>
                              <li>Fax:+49 8024 46869 99</li>
                              <li>CannaXan GmbH | Birkerfeld 12 | 83627 Warngau</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-3 align-self-end">
                        <div class="footer-nav text-right">
                           <ul>
                              <li><a href="mailto:info@cannaxan.de">info@cannaxan.de</a></li>
                              <li><a href="https://www.cannaxan.de/gmbh/datenschutz" target="_blank">Datenschutz</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <script type="text/javascript" src="<?php echo $this->config->item('front_assets'); ?>js/common.js"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/front/'); ?>js/jquery.form.js"></script>
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
      <script type="text/javascript" src="<?php echo $this->config->item('front_assets'); ?>js/pagescript.js"></script>
      <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
       
      <!-- <script type="text/javascript" src="<?php echo $this->config->item('front_assets'); ?>js/bootstrap.min.js"></script> -->
      <script type="text/javascript">
         $(function () {
           $('[data-toggle="tooltip"]').tooltip();
           $(".event-list-box-cover").find("li.active").prev().addClass("after-none");
           $( ".event-list-box-cover>ul>li" )
            .on( "mouseenter", function() {
               $(this).prev().addClass("after-none-hover");
            })
            .on( "mouseleave", function() {
               $(this).prev().removeClass("after-none-hover");
            });
         });
      </script>
      
   </body>
</html>