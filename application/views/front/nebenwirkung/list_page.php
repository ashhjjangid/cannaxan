<section class="same-section begleiterkrankungen-section">
   <form method="post" class="ajax_form" action="<?php echo base_url('nebenwirkung/add_nebenwirkung_data').'?in='. $insurance_number; ?>" id="nebenwirkung_data_form">
      <div class="container-fluid">
         <div class="same-heading ">
            <h2 class="text-left">Erfassung Nebenwirkungen</h2>
         </div>
         <div class="row">
            <div class="col-md-4">
               <div class="form-group">
                  <div class="row align-items-center">
                     <div class="col-sm-6">
                        <label>Datum</label>
                     </div>
                     <div class="col-sm-6">
                        <!-- <input type="text" name="datum" id="datum" class="form-control text-center" placeholder="" value="<?php echo $datum; ?>" readonly> -->
                        <div class="dropdown select-box red-select" id="select_added_date">
                           <button id="datum_btn" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($select_date)?date('d.m.Y', strtotime($select_date)):''; ?>
                           <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu" id="datum-visite" aria-labelledby="datum_btn">
                              <?php if ($visits) { ?>
                              <?php 
                                 foreach ($visits as $key => $value) { 
                                    $datum = date('Y-m-d', strtotime($value->datum));
                                    // echo $updateDate.' '.$select_date;
                                    $selected = '';
                                 
                                    if ($select_date == $datum) {
                                       $selected = 'selected';
                                    }
                                 
                                 ?>
                              <li data-visit-number="<?php echo $value->visite; ?>" data-visit-behandungstag="<?php echo $value->visite; ?>" value="<?php echo date('Y-m-d', strtotime($value->datum)); ?>" <?php echo $selected; ?>><?php echo date('d.m.Y', strtotime($value->datum)); ?></li>
                              <?php } ?>
                              <?php } else { ?>
                              <li value="" data-visit-number="">Visit not available</li>
                              <?php } ?>
                           </ul>
                        </div>
                        <input type="hidden" name="datum" id="datum" class="datum-field" data-visit-number-field="<?php echo $visite; ?>" data-visit-behandungstag-field="<?php echo $visite; ?>" value="<?php echo $datum; ?>">
                        <input type="hidden" name="is_exist" value="<?php echo $is_current_record; ?>">
                        <!-- <select class="form-control text-center select_added_date" name="datum" onchange="selectDate()">
                           <?php if ($visits) { ?>
                           
                                 <?php 
                              foreach ($visits as $key => $value) { 
                              $updateDate = date('Y-m-d', strtotime($value->update_date));
                              // echo $updateDate.' '.$select_date;
                              $selected = '';
                              
                              if ($medication && ($datum == $updateDate)) {
                                 $selected = 'selected';
                              }
                              
                              ?>
                                 <option data-visit-number="<?php echo $value->visit_number; ?>" value="<?php echo date('Y-m-d', strtotime($value->update_date)); ?>" <?php echo $selected; ?>><?php echo date('d.m.Y', strtotime($value->update_date)); ?></option>
                              <?php } ?>
                           <?php } else { ?>
                              <option value="" data-visit-number="">Visit not available</option>
                           <?php } ?>
                           </select> -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- <div class="col-md-4">
               <div class="form-group">
                  <div class="row align-items-center">
                     <div class="col-sm-6">
                        <label>BEHANDLUNGSTAG</label>
                     </div>
                     <div class="col-sm-6">
                        <input type="text" name="behandungstag" class="form-control text-center behandungstag-val" placeholder="" value="<?php echo $behandungstag; ?>" readonly>
                     </div>
                  </div>
               </div>
            </div> -->
            <input type="hidden" name="behandungstag" class="form-control text-center behandungstag-val" placeholder="" value="<?php echo $behandungstag; ?>" readonly>
            <div class="col-md-4">
               <div class="form-group">
                  <div class="row align-items-center">
                     <div class="col-sm-6">
                        <label>Visite</label>
                     </div>
                     <div class="col-sm-6">
                        <!-- <input type="text" name="visite" class="form-control text-center" placeholder="" value="<?php echo $visite; ?>"> -->
                        <input type="text" name="visite" value="<?php echo $visite; ?>" class="form-control text-center visit-number" placeholder="" readonly>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="min-height">
            <div class="row">
               
               <div class="col-md-4">
                  <div class="form-group">
                     <div class="row align-items-center">
                        <div class="col-sm-6">
                           <label>Arzneimittel</label>
                        </div>
                        <div class="col-sm-6">
                           <input type="text" name="arzneimittel" class="form-control text-center" placeholder="" value="<?php echo $arzneimittel; ?>" readonly>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-8">
                        <div class="form-group">
                           <div class="row align-items-center">
                              <div class="col-sm-9">
                                 <label class="mb-0">kausal bedingte Nebenwirkung</label>
                              </div>
                              <div class="col-sm-3">
                                 <!-- <input type="text" name="keine_kausal_bedingte" class="form-control text-center" placeholder="" value="<?php echo $keine_kausal_bedingte; ?>"> -->
                                 <div class="dropdown select-box" id="select_keine_kausal_bedingte_box">
                                    <button id="keine_btn" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($keine_kausal_bedingte)?$keine_kausal_bedingte:''; ?>
                                    <span><i class="fas fa-chevron-down"></i></span>
                                    </button>
                                    <ul class="dropdown-menu" id="keine" aria-labelledby="keine_btn">
                                       <li value="ja">ja</li>
                                       <li value="nein">nein</li>
                                    </ul>
                                 </div>
                                 <input type="hidden" name="keine_kausal_bedingte" id="keine-field" class="keine-field" value="<?php echo ($keine_kausal_bedingte)?$keine_kausal_bedingte:''; ?>">
                                 <!-- <select name="keine_kausal_bedingte" class="form-control text-center" >
                                    <option value="ja" <?php echo ($keine_kausal_bedingte == 'ja')?'selected':''; ?>>Ja</option>
                                    <option value="nein" <?php echo ($keine_kausal_bedingte == 'nein')?'selected':''; ?>>Nein</option>
                                    </select> -->
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4"></div>
                  </div>
               </div>
            </div>
            <div class="tabel-overlap tabel-overlap-box NEBENWIRKUNG-table NEBENWIRKUNG-table-view <?php echo ($keine_kausal_bedingte && ($keine_kausal_bedingte == 'ja'))?'':'d-none'; ?>">
               <table cellspacing="0"  cellpadding="0" width="100%" class="table table-striped table-bordered site-table">
                  <thead>
                     <tr>
                        <th>NEBENWIRKUNG</th>
                        <th colspan="4" class="p-0 intable">
                           <table cellpadding="0" cellspacing="0" width="100%" align="center">
                              <tr>
                                 <th colspan="4">
                                    <center> SCHWEREGRAD</center>
                                 </th>
                              </tr>
                              <tr>
                                 <th style="background:#2CA61E; ">mild</th>
                                 <th style="background:#FCAF16; ">moderat</th>
                                 <th style="background:#F76816; ">schwer</th>
                                 <th style="background:#D91313; ">lebensbedrohlich</th>
                              </tr>
                           </table>
                        </th>
                        <th>KAUSALITÄT ZUR<br> THERAPIE VORHANDEN</th>
                     </tr>
                  </thead>
                  <?php 
                     foreach ($records as $key => $value) { 
                     
                        $mild_schweregrad = '';
                        $moderat_schweregrad = '';
                        $schwer_schweregrad = '';
                        $lebensbedrohlich_schweregrad = '';
                        $kausalitat_zur_therapie_vorhanden = '';
                        
                        if (isset($table_data_exists)) { 
                           foreach ($table_data_exists as $k => $val) { 
                     
                           if ($value->id == $val->nebenwirkung_name) {
                                 $mild_schweregrad = $val->mild_schweregrad;
                                 $moderat_schweregrad = $val->moderat_schweregrad;
                                 $schwer_schweregrad = $val->schwer_schweregrad;
                                 $lebensbedrohlich_schweregrad = $val->lebensbedrohlich_schweregrad;
                                 $kausalitat_zur_therapie_vorhanden = $val->kausalitat_zur_therapie_vorhanden;
                                 break;
                           }
                        }
                     }
                     ?>
                  <tr>
                     <td><input type="hidden" name="data[<?php echo $key; ?>][nebenwirkung_name]" value="<?php echo $value->id; ?>"><?php echo $value->nebenwirkung_name; ?></td>
                     <td style="text-align: center;">
                        <label class="switch" for="checkbox_<?php echo $key; ?>_mild_schweregrad">
                           <input type="checkbox" id="checkbox_<?php echo $key; ?>_mild_schweregrad" name="data[<?php echo $key; ?>][mild_schweregrad]" class="mild_schweregrad" onchange="singlecheck(this)" <?php echo ($mild_schweregrad == 'Yes')?'checked':''; ?>/>
                           <div class="slider round"></div>
                        </label>
                        <!-- <div class="dropdown select-box select_mild_schweregrad_box">
                           <button class="dropdown-select mild_schweregrad_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($mild_schweregrad == 'Yes')?'X':'-'; ?>
                              
                              <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu mild_schweregrad_box" aria-labelledby="mild_schweregrad_btn">
                              <li value="Yes" onclick="select_mild_schweregrad_box(this)">X</li>
                              <li value="No" onclick="select_mild_schweregrad_box(this)">-</li>
                           </ul>
                           </div> -->
                        <!-- <input type="hidden" name="data[<?php echo $key; ?>][mild_schweregrad]" class="mild_schweregrad-field" value="<?php echo ($mild_schweregrad == 'Yes')?'Yes':'No'; ?>"> -->
                        <!-- <select name="data[<?php echo $key; ?>][mild_schweregrad]">
                           <option value="No" <?php echo ($mild_schweregrad == "No")?'selected':''; ?>>-</option>
                           <option value="Yes" <?php echo ($mild_schweregrad == "Yes")?'selected':''; ?>>X</option>
                           </select> -->
                     </td>
                     <td style="text-align: center;">
                        <label class="switch" for="checkbox_<?php echo $key; ?>_moderat_schweregrad">
                           <input type="checkbox" id="checkbox_<?php echo $key; ?>_moderat_schweregrad" name="data[<?php echo $key; ?>][moderat_schweregrad]" class="moderat_schweregrad" onchange="singlecheck(this)" <?php echo ($moderat_schweregrad == 'Yes')?'checked':''; ?>/>
                           <div class="slider round"></div>
                        </label>
                        <!-- <div class="dropdown select-box select_moderat_schweregrad_box">
                           <button class="dropdown-select moderat_schweregrad_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($moderat_schweregrad == 'Yes')?'X':'-'; ?>
                              
                              <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu moderat_schweregrad_box" aria-labelledby="moderat_schweregrad_btn">
                              <li value="Yes" onclick="select_moderat_schweregrad_box(this)">X</li>
                              <li value="No" onclick="select_moderat_schweregrad_box(this)">-</li>
                           </ul>
                           </div>
                           <input type="hidden" name="data[<?php echo $key; ?>][moderat_schweregrad]" class="moderat_schweregrad-field" value="<?php echo ($moderat_schweregrad == 'Yes')?'Yes':'No'; ?>"> -->
                        <!-- <select name="data[<?php echo $key; ?>][moderat_schweregrad]">
                           <option value="No" <?php echo ($moderat_schweregrad == 'No')?'selected':''; ?>>-</option>
                           <option value="Yes" <?php echo ($moderat_schweregrad == 'Yes')?'selected':''; ?>>X</option>
                           </select> -->
                     </td>
                     <td style="text-align: center;">
                        <label class="switch" for="checkbox_<?php echo $key; ?>_schwer_schweregrad">
                           <input type="checkbox" id="checkbox_<?php echo $key; ?>_schwer_schweregrad" name="data[<?php echo $key; ?>][schwer_schweregrad]" class="schwer_schweregrad" onchange="singlecheck(this)" <?php echo ($schwer_schweregrad == 'Yes')?'checked':''; ?>/>
                           <div class="slider round"></div>
                        </label>
                        <!-- <div class="dropdown select-box select_schwer_schweregrad_box">
                           <button class="dropdown-select schwer_schweregrad_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($schwer_schweregrad == 'Yes')?'X':'-'; ?>
                              
                              <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu schwer_schweregrad_box" aria-labelledby="schwer_schweregrad_btn">
                              <li value="Yes" onclick="select_schwer_schweregrad_box(this)">X</li>
                              <li value="No" onclick="select_schwer_schweregrad_box(this)">-</li>
                           </ul>
                           </div>
                           <input type="hidden" name="data[<?php echo $key; ?>][schwer_schweregrad]" class="schwer_schweregrad-field" value="<?php echo ($schwer_schweregrad == 'Yes')?'Yes':'No'; ?>"> -->
                        <!-- <select name="data[<?php echo $key; ?>][schwer_schweregrad]">
                           <option value="No" <?php echo ($schwer_schweregrad == 'No')?'selected':'' ?>>-</option>
                           <option value="Yes" <?php echo ($schwer_schweregrad == 'Yes')?'selected':'' ?>>X</option>
                           </select> -->
                     </td>
                     <td style="text-align: center;">
                        <label class="switch" for="checkbox_<?php echo $key; ?>_lebensbedrohlich_schweregrad">
                           <input type="checkbox" id="checkbox_<?php echo $key; ?>_lebensbedrohlich_schweregrad" name="data[<?php echo $key; ?>][lebensbedrohlich_schweregrad]" class="lebensbedrohlich_schweregrad" onchange="singlecheck(this)" <?php echo ($lebensbedrohlich_schweregrad == 'Yes')?'checked':''; ?>/>
                           <div class="slider round"></div>
                        </label>
                        <!-- <div class="dropdown select-box select_lebensbedrohlich_schweregrad_box">
                           <button class="dropdown-select lebensbedrohlich_schweregrad_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($lebensbedrohlich_schweregrad == 'Yes')?'X':'-'; ?>
                              
                              <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu lebensbedrohlich_schweregrad_box" aria-labelledby="lebensbedrohlich_schweregrad_btn">
                              <li value="Yes" onclick="select_lebensbedrohlich_schweregrad_box(this)">X</li>
                              <li value="No" onclick="select_lebensbedrohlich_schweregrad_box(this)">-</li>
                           </ul>
                           </div>
                           <input type="hidden" name="data[<?php echo $key; ?>][lebensbedrohlich_schweregrad]" class="lebensbedrohlich_schweregrad-field" value="<?php echo ($lebensbedrohlich_schweregrad == 'Yes')?'Yes':'No'; ?>"> -->
                        <!-- <select name="data[<?php echo $key; ?>][lebensbedrohlich_schweregrad]">
                           <option value="No" <?php echo ($lebensbedrohlich_schweregrad == 'No')?'selected':''; ?>>-</option>
                           <option value="Yes" <?php echo ($lebensbedrohlich_schweregrad == 'Yes')?'selected':''; ?>>X</option>
                           </select> -->
                     </td>
                     <td style="text-align: center;">
                        <label class="switch" for="checkbox_<?php echo $key; ?>_kausalitat_zur_therapie_vorhanden">
                           <input type="checkbox" class="kausalitat_zur_therapie_vorhanden" onchange="Kausalitatcheck(this)" id="checkbox_<?php echo $key; ?>_kausalitat_zur_therapie_vorhanden" name="data[<?php echo $key; ?>][kausalitat_zur_therapie_vorhanden]" <?php echo ($kausalitat_zur_therapie_vorhanden == 'ja')?'checked':''; ?>/>
                           <div class="slider round"></div>
                        </label>
                        <!-- <div class="dropdown select-box select_kausalitat_zur_therapie_vorhanden_box">
                           <button class="dropdown-select kausalitat_zur_therapie_vorhanden_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($kausalitat_zur_therapie_vorhanden == 'ja')?'ja':'nein'; ?>
                              
                              <span><i class="fas fa-chevron-down"></i></span>
                           </button>
                           <ul class="dropdown-menu kausalitat_zur_therapie_vorhanden_box" aria-labelledby="kausalitat_zur_therapie_vorhanden_btn">
                              <li value="ja" onclick="select_kausalitat_zur_therapie_vorhanden_box(this)">ja</li>
                              <li value="nein" onclick="select_kausalitat_zur_therapie_vorhanden_box(this)">nein</li>
                           </ul>
                           </div>
                           <input type="hidden" name="data[<?php echo $key; ?>][kausalitat_zur_therapie_vorhanden]" class="kausalitat_zur_therapie_vorhanden-field" value="<?php echo ($kausalitat_zur_therapie_vorhanden == 'ja')?'ja':'nein'; ?>"> -->
                        <!-- <select name="data[<?php echo $key; ?>][kausalitat_zur_therapie_vorhanden]">
                           <option value="ja" <?php echo ($kausalitat_zur_therapie_vorhanden == 'ja')?'selected':''; ?>>Ja</option>
                           <option value="nein" <?php echo ($kausalitat_zur_therapie_vorhanden == 'nein')?'selected':''; ?>>Nein</option>
                           </select> -->
                     </td>
                  </tr>
                  <?php } ?>
                  <!-- <tr>
                     <td>Erbrechen</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Appetitsteigerung</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Gewichtszunahme</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Konstipation</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Diarrho</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Mundtrockenheit</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Tachykardie</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Palpitaionen</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Hypotonie</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Hypotonie</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Schwindel</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Gleichgewichtsstorungen</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Verschwonmmenes Sehen</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Aufmerksamkeitsstorungen </td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Gedachtsnisstorungen </td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Dysarthrie</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Desorientierung</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr>
                     <tr>
                     <td>Mudigkeit</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;"><i class="fas fa-times"></i></td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">-</td>
                     <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                     </td>
                     </tr> -->
               </table>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="btn-mar-top text-right">
                  <button type="submit" class="btn btn-primary"><span>Speichern &amp; weiter</span></button>
               </div>
            </div>
         </div>
      </div>
   </form>
</section>
<script type="text/javascript">
   /*$(function(){
     $('#datum').datepicker({
         format: 'dd.mm.yyyy'
     })
   })*/
   $(function(){
     var s_date = '<?php echo $select_date; ?>';
   
     if (s_date) {
       selectDate();
     }
   //   initDatepicker();
   // selectDate();
   });
   /*function selectDate()
   {
   var data_visit_number = $('.select_added_date option:selected').attr('data-visit-number');
   $('.visit-number').val(data_visit_number);
   }*/
   
   $('#select_keine_kausal_bedingte_box li').click(function() {
   var getText = $(this).text();
   var getValue = $(this).attr('value');
   if (getValue == 'ja') {
   $('.NEBENWIRKUNG-table-view').removeClass('d-none');
   } else {
   $('.NEBENWIRKUNG-table-view').addClass('d-none');
   }
   $('#keine_btn').text(getText);
   $('#keine-field').val(getValue);
   });
   
   function select_mild_schweregrad_box($this) {
   
   var getText = $($this).text(); 
   var getValue = $($this).attr('value');
   
   $($this).closest('tr').find('.mild_schweregrad_btn').text(getText);     
   $($this).closest('tr').find('.mild_schweregrad-field').val(getValue); 
   }
   
   function select_moderat_schweregrad_box($this) {
   
   var getText = $($this).text(); 
   var getValue = $($this).attr('value');
   
   $($this).closest('tr').find('.moderat_schweregrad_btn').text(getText);     
   $($this).closest('tr').find('.moderat_schweregrad-field').val(getValue); 
   }
   
   function select_schwer_schweregrad_box($this) {
   
   var getText = $($this).text();
   var getValue = $($this).attr('value');
   
   $($this).closest('tr').find('.schwer_schweregrad_btn').text(getText);     
   $($this).closest('tr').find('.schwer_schweregrad-field').val(getValue); 
   }
   
   function select_lebensbedrohlich_schweregrad_box($this) {
   
   var getText = $($this).text(); 
   var getValue = $($this).attr('value');
   
   $($this).closest('tr').find('.lebensbedrohlich_schweregrad_btn').text(getText);     
   $($this).closest('tr').find('.lebensbedrohlich_schweregrad-field').val(getValue); 
   }
   
   function select_kausalitat_zur_therapie_vorhanden_box($this) {
   
   var getText = $($this).text(); 
   var getValue = $($this).attr('value');
   
   $($this).closest('tr').find('.kausalitat_zur_therapie_vorhanden_btn').text(getText);     
   $($this).closest('tr').find('.kausalitat_zur_therapie_vorhanden-field').val(getValue); 
   }
   
   function selectDate()
   {
   var data_visit_number = $('.datum-field').attr('data-visit-number-field');
   var data_behandungstag = $('.datum-field').attr('data-visit-behandungstag-field');
   $('.visit-number').val(data_visit_number);
   $('.behandungstag-val').val(data_behandungstag);
   }
   
   $('#select_added_date li').click(function() {
   
   var getText = $(this).text(); 
   var getVisitNumber = $(this).attr('data-visit-number');
   var getVisitBehandungstag = $(this).attr('data-visit-behandungstag');
   var getValue = $(this).val();
   //alert(getText); 
   $('#datum_btn').text(getText);
   $('.datum-field').attr('data-visit-number-field', getVisitNumber);
   $('.datum-field').attr('data-visit-behandungstag-field', getVisitBehandungstag);
   $('.datum-field').val(getText);
   $('#select_added_date').addClass('not-red');
   selectDate();
   });

   function singlecheck($this) {
      var checked_row = $($this).is(':checked');
      
      if (checked_row  == true) {
        $($this).closest('tr').find(".mild_schweregrad").prop('checked', false);
        $($this).closest('tr').find(".moderat_schweregrad").prop('checked', false);
        $($this).closest('tr').find(".schwer_schweregrad").prop('checked', false);
        $($this).closest('tr').find(".lebensbedrohlich_schweregrad").prop('checked', false);
        $($this).prop('checked', true);
      } else {
        $($this).closest('tr').find(".kausalitat_zur_therapie_vorhanden").prop('checked', false);

      }
   }

   function Kausalitatcheck($this) {
      var checked_row = $($this).is(':checked');
      var mild_schweregrad = $($this).closest('tr').find(".mild_schweregrad").is(':checked');
      var moderat_schweregrad = $($this).closest('tr').find(".moderat_schweregrad").is(':checked');
      var schwer_schweregrad = $($this).closest('tr').find(".schwer_schweregrad").is(':checked');
      var lebensbedrohlich_schweregrad = $($this).closest('tr').find(".lebensbedrohlich_schweregrad").is(':checked');

      if (checked_row == true) {
         if (mild_schweregrad == true || moderat_schweregrad == true || schwer_schweregrad == true || lebensbedrohlich_schweregrad == true) {
            $($this).prop('checked', true);
         } else {
            $($this).prop('checked', false);

         }
      }
   }

</script>