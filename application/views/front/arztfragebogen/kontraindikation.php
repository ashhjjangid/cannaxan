<div class="same-section">  
    <div class="container-fluid">   
      <form method="post" action="<?php echo current_url().'?in='.$insured_number.'&qid='.$qid; ?>" class="form-horizontal ajax_form">
        <div class="same-heading mb-4">
            <h2>Kostenübernahmeantrag 5/6</h2>
        </div>
        <div class="min-height">
          <div class="row_erkrankung">
            <h5 class="mb-3 simple-txt"><strong>Warum kommen Alternativtherapien bei der vorliegenden Diagnose und der Therapiehistorie nicht zum Einsatz?</strong></h5>

              <div class="checkbox-custom normal-radio">
                <input type="checkbox" name="who_stufe_2_medikamente" value="ja" <?php echo ($who_stufe_2_medikamente)?'checked':''; ?>  id="allergien_gee">
                <label for="allergien_gee"><span class="cheak"></span>WHO Stufe 2 Medikamente führen aus ärztlicher Sicht zu starken Nebenwirkungen beim Patienten</label>
              </div>

              <div class="checkbox-custom normal-radio">
                <input type="checkbox" name="who_stufe_2_medikamente_keiner" value="ja" <?php echo ($who_stufe_2_medikamente_keiner)?'checked':''; ?> id="allergien_who">
                <label for="allergien_who"><span class="cheak"></span>WHO Stufe 2 Medikamente führen aus ärztlicher Sicht zu keiner Verbesserung der Symptomatik beim Patienten</label>
              </div>
              <div class="checkbox-custom normal-radio">
                <input type="checkbox" name="who_stufe_3_medikamente" value="ja" <?php echo ($who_stufe_3_medikamente)?'checked':''; ?> id="allergien_3">
                <label for="allergien_3"><span class="cheak"></span>WHO Stufe 3 Medikamente führen aus ärztlicher Sicht zu starken Nebenwirkungen beim Patienten</label>
              </div>
              <div class="checkbox-custom normal-radio">
                <input type="checkbox" name="who_stufe_3_medikamente_keiner" value="ja" <?php echo ($who_stufe_3_medikamente_keiner)?'checked':''; ?>  id="allergien_4">
                <label for="allergien_4"><span class="cheak"></span>WHO Stufe 3 Medikamente führen aus ärztlicher Sicht zu keiner Verbesserung der Symptomatik beim Patienten</label>
              </div>
              <div class="checkbox-custom normal-radio">
                <input type="checkbox" name="weitere_begrundung" value="ja" <?php echo ($weitere_begrundung)?'checked':''; ?> id="allergien_5">
                <label for="allergien_5"><span class="cheak"></span>weitere Begründung:</label>
              </div>
              <div class="mt-2 mb-3">
                <textarea class="form-control" name="weitere_begrundung_text"><?php echo $weitere_begrundung_text; ?></textarea>
              </div>

              <h5 class="mb-3 simple-txt"><strong>Ist eine Kontraindikation bei der Verwendung von CannaXan während der Therapie zu erwarten?</strong></h5>
              <div class="radio-box normal-radio">
                <input type="radio" class="select-kontraindikation" name="kontraindikation" onclick="selectkontraindikation_options()" <?php echo ($kontraindikation == 'Yes')?'checked':''; ?> value="Yes" id="select-kontraindikation">
                <label for="select-kontraindikation">
                    <span class="radio-cheak"> <span></span> </span>
                    ja
                </label>
                <div class="normal-radio-inner" id="kontraindikation_options">
                  <div class="radio-box normal-radio">
                    <input type="checkbox" class="select-relative-kontraindikation" name="relative_kontraindikation" <?php echo ($relative_kontraindikation)?'checked':''; ?> value="Relative Kontraindikation" onclick="selectkontraindikation_options()" id="select-relative-kontraindikation">
                    <label for="select-relative-kontraindikation">
                        <span class="radio-cheak"> <span></span> </span>
                        Relative Kontraindikation
                    </label>
                    <div class="normal-radio-inner" id="relative-kontraindikation-options">
                      <div class="checkbox-custom normal-radio">
                        <input type="checkbox" name="allergien_gegen" <?php echo ($allergien_gegen)?'checked':''; ?> value="1" id="allergien_gegen">
                        <label for="allergien_gegen"><span class="cheak"></span>Allergien gegen Cannabinoide oder einen der genannten sonstigen Bestandteile von CannaXan</label>
                      </div>
                      <div class="checkbox-custom normal-radio">
                        <input type="checkbox" name="nierenerkrankung" <?php echo ($nierenerkrankung)?'checked':''; ?> value="2" id="nierenerkrankung">
                        <label for="nierenerkrankung"><span class="cheak"></span>schwere Leber- und/oder Nierenerkrankung</label>
                      </div>
                    </div>
                  </div>
                  <div class="radio-box normal-radio">
                    <input type="checkbox" class="select-absolute-kontraindikation" name="absolute_kontraindikation" <?php echo ($absolute_kontraindikation)?'checked':''; ?> value="Absolute Kontraindikation" onclick="selectkontraindikation_options()" id="select-absolute-kontraindikation">
                    <label for="select-absolute-kontraindikation">
                        <span class="radio-cheak"> <span></span> </span>
                        Absolute Kontraindikation
                    </label>
                    <div class="normal-radio-inner" id="absolute-kontraindikation-options">
                      <div class="checkbox-custom normal-radio">
                        <input type="checkbox" name="schwere_personlichkeitsstorung" <?php echo ($schwere_personlichkeitsstorung)?'checked':''; ?> value="1" id="schwere_personlichkeitsstorung">
                        <label for="schwere_personlichkeitsstorung"><span class="cheak"></span>schwere Persönlichkeitsstörung (z.B. Schizophrenie, Psychose)</label>
                      </div>
                      <div class="checkbox-custom normal-radio">
                        <input type="checkbox" name="schwere_kardiovaskulare" <?php echo ($schwere_kardiovaskulare)?'checked':''; ?> value="2" id="schwere_kardiovaskulare">
                        <label for="schwere_kardiovaskulare"><span class="cheak"></span>schwere kardiovaskuläre Erkrankung </label>
                      </div>
                      <div class="checkbox-custom normal-radio">
                        <input type="checkbox" name="kinder" <?php echo ($kinder)?'checked':''; ?> value="3" id="kinder">
                        <label for="kinder"><span class="cheak"></span>Kinder und Jugendliche unter 18 Jahren</label>
                      </div>
                      <div class="checkbox-custom normal-radio">
                        <input type="checkbox" name="schwangerschaft" <?php echo ($schwangerschaft)?'checked':''; ?> value="4" id="schwangerschaft">
                        <label for="schwangerschaft"><span class="cheak"></span>Schwangerschaft und Stillzeit </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="radio-box normal-radio">
                <input type="radio" class="select-kontraindikation" name="kontraindikation" onclick="selectkontraindikation_options()" <?php echo ($kontraindikation == 'No')?'checked':''; ?> value="No" id="nein">
                <label for="nein">
                    <span class="radio-cheak"> <span></span> </span>
                    nein
                </label>
              </div>
          </div>
        </div>
        <div class="btn-group-custom text-right">
          <a class="btn btn-primary" href="<?php echo base_url('kostenubernahmeantrag/step-4'.'?in='.$insured_number.'&qid='.$qid); ?>"><span><i class="fas fa-long-arrow-alt-left"></i> Zurück</span></a>
          <button type="submit" class="btn btn-primary"><span>Weiter <i class="fas fa-long-arrow-alt-right"></i></span></button>
        </div>
      </form>
    </div>
</div>
<script>
  $(function() {
    selectkontraindikation_options();
  })
  function selectkontraindikation_options() {
    if ($('input[name="kontraindikation"]:checked').val() == 'Yes') {
      $('#kontraindikation_options').css('display','block');
    } else {
      $('#kontraindikation_options').css('display','none');
    }

    if ($('input[name="relative_kontraindikation"]:checked').val() == 'Relative Kontraindikation') {
      $('#relative-kontraindikation-options').css('display','block');
    } else {
      $('#relative-kontraindikation-options').css('display','none');
    }

    if ($('input[name="absolute_kontraindikation"]:checked').val() == 'Absolute Kontraindikation') {
      $('#absolute-kontraindikation-options').css('display','block');
    } else {
      $('#absolute-kontraindikation-options').css('display','none');
    }
  }
</script>