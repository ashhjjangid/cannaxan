<section class="same-section">
   <div class="container-fluid">
    <form method="post" action="<?php echo current_url().'?in='.$insured_number.'&qid='.$qid; ?>" class="form-horizontal ajax_form">
      <input type="hidden" name="erfolgt_hidden" value="0">
      <div class="same-heading mb-4">
         <h2>Kostenübernahmeantrag 1/6</h2>
         <!-- <?php //echo form_open(current_url().'?in='.$insured_number.'&qid='.$qid, array('class' => 'form-horizontal ajax_form'));?> -->
      </div>
      <h5 class="mb-3 simple-txt"><strong>Angaben zur automatischen Erstellung des Kostenübernahmeantrag von CannaXan</strong></h5>
      <h5 class="mb-4 simple-txt"><strong>Erfolgt die Verordnung im Rahmen der genehmigten Versorgung nach § 37b SGB V (spezialisierte ambulante Palliativversorgung)?</strong></h5>
      <?php
        $checked_first = '';
        $checked_second = '';
        if ($question_1_data) {
          
          if ($question_1_data->answer == "Yes") {
             $checked_first = 'checked';
          } else {
             $checked_second = 'checked';
          }
        }
        ?>
      <div class="radio-box normal-radio">
         <input type="radio" name="37b_sgb_v" <?php echo $checked_first; ?> value="Yes" id="radio">
         <label for="radio">
            <span class="radio-cheak"><span></span> </span>
            ja
         </label>
      </div>

      <div class="radio-box normal-radio">
         <input type="radio" name="37b_sgb_v" <?php echo $checked_second; ?> value="No" id="radio-2">
         <label for="radio-2">
            <span class="radio-cheak"><span></span> </span>
            nein
         </label>
      </div>

      <h5 class="mb-4 mt-3 simple-txt"><strong>Wie lautet das Behandlungsziel?</strong></h5>
      <?php //foreach ($question_2_data as $key => $value) { ?>
        <div class="checkbox-custom normal-radio">
          <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '1')?'checked':''; } } ?> value="1" id="behandlungsziel_1">
          <label for="behandlungsziel_1"><span class="cheak"></span>Kontrolle des Appetits</label>
        </div>
        <div class="checkbox-custom normal-radio">
          <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '2')?'checked':''; } } ?> value="2" id="behandlungsziel_2">
          <label for="behandlungsziel_2"><span class="cheak"></span>Kontrolle des Gewichtsverlusts</label>
        </div>
        <div class="checkbox-custom normal-radio">
          <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '3')?'checked':''; } } ?> value="3" id="behandlungsziel_3">
          <label for="behandlungsziel_3"><span class="cheak"></span>Verbesserung der Schmerzsituation</label>
        </div>
        <div class="checkbox-custom normal-radio">
          <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '4')?'checked':''; } } ?> value="4" id="behandlungsziel_4">
          <label for="behandlungsziel_4"><span class="cheak"></span>Verbesserung der Lebensqualität</label>
        </div>
        <div class="checkbox-custom normal-radio">
          <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '5')?'checked':''; } } ?> value="5" id="behandlungsziel_5">
          <label for="behandlungsziel_5"><span class="cheak"></span>Erhalt der Arbeitsfähigkeit</label>
        </div>
        <div class="normal-radio" id="Zunehmende-txtarea">
          <!-- <label for="Zunehmende-txtarea"><strong class="same-font-size">Kurze Beschreibung der Grunderkrankung</strong></label> -->
           <textarea class="form-control" name="behandlungsziel_text_field"><?php if ($question_2_data) { foreach ($question_2_data as $key => $value) { echo $value->behandlungsziel_text_field; } } ?></textarea>
        </div>
      <?php //} ?>
      <div class="btn-group-custom text-right">
        <button type="submit" class="btn btn-primary float-none"><span>Weiter <i class="fas fa-long-arrow-alt-right"></i></span></button>
      </div>
       
        <!-- <div class="col-md-10">
           
           <label>Wie lautet das Behandlungsziel?</label>
           <div class="col-md-8">
              <div class="no-input">
                <?php //foreach ($question_2_data as $key => $value) { ?>
                    
                 <div class="col ">
                    <div class="checkbox-custom small">
                        <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '1')?'checked':''; } } ?> value="1" id="behandlungsziel_1">
                        <label for="behandlungsziel_1"><span class="cheak"></span>Kontrolle des Appetits</label>
                     </div>
                 </div>
                 <div class="col ">
                    <div class="checkbox-custom small">
                        <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '2')?'checked':''; } } ?> value="2" id="behandlungsziel_2">
                        <label for="behandlungsziel_2"><span class="cheak"></span>Kontrolle des Gewichtsverlusts</label>
                     </div>
                 </div>
                 <div class="col ">
                    <div class="checkbox-custom small">
                        <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '3')?'checked':''; } } ?> value="3" id="behandlungsziel_3">
                        <label for="behandlungsziel_3"><span class="cheak"></span>Verbesserung der Schmerzsituation</label>
                     </div>
                 </div>
                 <div class="col ">
                    <div class="checkbox-custom small">
                        <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '4')?'checked':''; } } ?> value="4" id="behandlungsziel_4">
                        <label for="behandlungsziel_4"><span class="cheak"></span>Verbesserung der Lebensqualität</label>
                     </div>
                 </div>
                 <div class="col ">
                    <div class="checkbox-custom small">
                        <input type="checkbox" name="behandlungsziel[]" <?php if($question_2_data) { foreach ($question_2_data as $key => $value) { echo ($value->answer == '5')?'checked':''; } } ?> value="5" id="behandlungsziel_5">
                        <label for="behandlungsziel_5"><span class="cheak"></span>Erhalt der Arbeitsfähigkeit</label>
                     </div>
                 </div>
                <?php //} ?>
              </div>
           </div>
        </div>
      </div> -->

   </div>
</section> 

<!-- <script type="text/javascript"> 

   $('#select_patient_question_1 li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).val();
      //alert(getValue);
      if (getValue == "1") {
         newText = "Ja";
         newValue = "Yes";
      } else {
         newText = "Nein";
         newValue = "No";
      }

         checkPatientSigned(this);
      //alert(newValue);
     $('#patient_question_1_btn').text(newText);
     $('.patient_question_1-field').val(newValue);
     $('#select_patient_question_1').addClass('not-red');
   });
</script> -->