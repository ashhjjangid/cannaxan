<section class="same-section">
	<div class="container-fluid">
    <form method="post" action="<?php echo current_url().'?in='.$insured_number.'&qid='.$qid; ?>" class="form-horizontal ajax_form">
      <input type="hidden" name="klini" value="">
      <div class="same-heading mb-4">
      <h2>Kostenübernahmeantrag 6/6</h2>
    </div>
      <div class="min-height">
        <h5 class="mb-4 simple-txt"><strong>Sind die Therapieoptionen, die für die vorliegende Indikation zur Reduktion der Symptomatik aus ärztlicher Sicht durchzuführen sind, ausgeschöpft?</strong></h5>
        <div class="inline-radio mb-3">
            <div class="radio-box normal-radio">
               <input type="radio" name="therapieoptionen_haben" value="ja" <?php echo ($therapieoptionen_haben == 'ja')?'checked':''; ?> id="bitte">
               <label for="bitte">
                  <span class="radio-cheak"> <span></span></span>
                  ja
               </label>
            </div>
            <div class="radio-box normal-radio">
               <input type="radio" name="therapieoptionen_haben" value="nein" <?php echo ($therapieoptionen_haben == 'nein')?'checked':''; ?> id="bitte2">
               <label for="bitte2">
                  <span class="radio-cheak"> <span></span></span>nein 
               </label>
            </div>
        </div>

    		<h5 class="mb-4 simple-txt"><strong>Erfolgt die Therapie im Rahmen einer klinischen Prüfung?</strong></h5>
    		<div class="radio-box normal-radio">
           <input type="radio" name="klinischen_prufung" <?php echo ($klinischen_prufung == 'Yes')?'checked':''; ?> value="Yes" id="radio">
           <label for="radio">
              <span class="radio-cheak"> <span></span> </span>
              ja
           </label>
        </div>
        <div class="radio-box normal-radio">
           <input type="radio" name="klinischen_prufung" <?php echo ($klinischen_prufung == 'No')?'checked':''; ?> value="No" id="radio-2">
           <label for="radio-2">
              <span class="radio-cheak"> <span></span> </span>
              nein
           </label>
        </div>
      </div>
      <div class="btn-group-custom text-right">
          <a class="btn btn-primary" href="<?php echo base_url('kostenubernahmeantrag/step-5'.'?in='.$insured_number.'&qid='.$qid); ?>"><span><i class="fas fa-long-arrow-alt-left"></i> Zurück</span></a>
          <button type="submit" class="btn btn-primary"><span>Drucken/Speichern </span></button>
        </div>
    </form>
	</div>
</section>