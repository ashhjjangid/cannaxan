<section class="same-section same-blog all-tabel-event">
   <div class="container-fluid">
      <div class="same-heading table-heading">
         <h2 class="text-left">Aktuelle Begleitmedikation</h2>
         
      </div>
      <div class="tabel-event">
        <form method="post" class="ajax_form" action="<?php echo base_url('process_control/addNewBegleithmedikationData'). '?in='. $insurance_number; ?>">
        <div class="min-height-high">
          <h2>Drücken Sie auf Plus, um eine weitere Zeile hinzuzufügen!</h2>
          <br>
          <!-- <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                      <div class="row">
                          <div class="col-sm-5">
                            <label>Datum</label>
                          </div>
                          <div class="col-sm-7">
                            <input type="text" id="date" name="add_date" value="<?php echo $add_date; ?>" class="form-control text-center" placeholder=""> 
                            <select class="form-control text-center select_added_date" name="visite_date" onchange="selectDate()">

                              <?php if ($visits) { ?>

                                    <?php 
                                    foreach ($visits as $key => $value) { 
                                      $updateDate = date('Y-m-d', strtotime($value->update_date));
                                      // echo $updateDate.' '.$select_date;
                                      $selected = '';

                                      if ($medication && ($medication->visite_date == $updateDate)) {
                                        $selected = 'selected';
                                      }

                                    ?>
                                    <option data-visit-number="<?php echo $value->visit_number; ?>" value="<?php echo date('Y-m-d', strtotime($value->update_date)); ?>" <?php echo $selected; ?>><?php echo date('d.m.Y', strtotime($value->update_date)); ?></option>
                                  <?php } ?>
                              <?php } else { ?>
                                  <option value="" data-visit-number="">Visit not available</option>
                              <?php } ?>
                            </select>
                          </div>
                      </div>
                    </div>
                </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="row">
                          <div class="col-sm-5">
                            <label>Visite</label>
                          </div>
                          <div class="col-sm-7">
                            <input type="text" name="visite" value="<?php echo $visite; ?>" class="form-control text-center visit-number" placeholder="" readonly>
                          </div>
                      </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
              </div> -->
          <div class="same-heading table-heading">           
            <p>Tragen sie bitte alle Schmerz-relevanten begleitenden Medikamente ein, die zu Beginn und während der CannaXan Behandlung eingenommen werden.</p>
            <p> Wurde die Dosierung einer Begleitmedikation geändert, dann tragen Sie bitte die neue Dosierung in eine neue Zeile ein und Fügen Sie dem vorherigen Eintrag ein entsprechendes Stoppdatum hinzu.</p>
            <p> Beim Stopdatum geben Sie bitte die letzte Einnahme des jeweiligen Begleitmedikaments ein.</p>
          </div>
          <table class="table site-table table-striped">
            <thead>
              <tr>
                <th>Nr.</th>
                <th>Wirkstoff</th>
                <th>Handelsname</th>
                <th>Stärke</th>
                <th>Einheit</th>
                <th>Form</th>
                <th>Morgens</th>
                <th>Mittags</th>
                <th>Abends</th>
                <th>Nachts</th>
                <th>Einheit</th>
                <th>Tagesmenge</th>
                <th>Grund</th>
                <th>Startdatum</th>
                <th>Stopdatum</th>
              </tr>
            </thead>
            <tbody class="append-new-row">
              <?php if (isset($patients_medication)) { foreach ($patients_medication as $key => $value) { ?>
                
              <tr class="table-row-length filled-row">
                <td><?php echo $key + 1; ?></td>
                <td><input type="text" name="data[<?php echo $key; ?>][wriksoff]" class="form-control" value="<?php echo $value->wriksoff; ?>"/></td>
                <td><input type="text" name="data[<?php echo $key; ?>][handelsname]" class="form-control" value="<?php echo $value->handelsname; ?>"/></td>
                <td><input type="text" name="data[<?php echo $key; ?>][starke]" class="form-control" value="<?php echo $value->starke; ?>"/></td>
                <td>
                    <!-- <div class="select-box">
                      <select class="form-control" name="data[<?php echo $key; ?>][eiheit]">
                        <?php /* if (isset($value->eiheit)) {  ?>
                        <option value="<?php echo $value->eiheit; ?>" <?php if ($id == $value->begleitmedikation_id) { ?> selected <?php } ?> >
                          <?php echo $value->eiheit; ?>
                        </option>
                      <?php } */ ?>
                          <option value="mg" <?php echo ($value->eiheit == 'mg')?'selected':''; ?>>mg</option>
                          <option value="ml" <?php echo ($value->eiheit == 'ml')?'selected':''; ?>>ml</option>
                      </select>
                      <span><i class="fas fa-chevron-down"></i></span>
                    </div> -->
                    <div class="dropdown select-box select_eiheit_dropdown_box">
                      <button class="dropdown-select eiheit_values_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($value->eiheit)?$value->eiheit:''; ?>                     
                        <span><i class="fas fa-chevron-down"></i></span>
                      </button>
                      <ul class="dropdown-menu select-form_1" aria-labelledby="eiheit_values_btn">
                        <li value="Sprühstoß" data-toggle="tooltip" data-placement="bottom" onclick="select_eiheit_dropdown_box(this)">Sprühstoß</li>
                        <li value="mg" data-toggle="tooltip" data-placement="bottom" onclick="select_eiheit_dropdown_box(this)">mg</li>
                        <li value="mL" data-toggle="tooltip" data-placement="bottom" onclick="select_eiheit_dropdown_box(this)">mL</li>
                        <li value="µg" data-toggle="tooltip" data-placement="bottom" onclick="select_eiheit_dropdown_box(this)">µg</li>
                        <li value="IE" data-toggle="tooltip" data-placement="bottom" onclick="select_eiheit_dropdown_box(this)">IE</li>
                        <li value="Tropfen" data-toggle="tooltip" data-placement="bottom" onclick="select_eiheit_dropdown_box(this)">Tropfen</li>
                        <li value="Hub" data-toggle="tooltip" data-placement="bottom" onclick="select_eiheit_dropdown_box(this)">Hub</li>
                        <li value="Tablette" data-toggle="tooltip" data-placement="bottom" onclick="select_eiheit_dropdown_box(this)">Tablette</li>
                      </ul>
                    </div>
                    <input type="hidden" name="data[<?php echo $key; ?>][eiheit]" class="eiheit_values-field" value="<?php echo ($value->eiheit)?$value->eiheit:''; ?>">
                </td>
                <td>
                    <!-- <div class="select-box">
                      <select class="form-control" id="form_values_<?php echo $key ?>" onchange="changeformvalues(this)" name="data[<?php echo $key; ?>][form]">
                        <?php /* if (isset($value->form)) {  ?>
                          <option value="<?php echo $value->form; ?>" <?php if ($id == $value->begleitmedikation_id) { ?> selected <?php } ?> >
                            <?php echo $value->form; ?>
                          </option>
                          <?php } */ ?>
                          <option value=""></option>
                          <option value="Tablette" <?php echo ($value->form == 'Tablette')?'selected':''; ?>>Tablette</option>
                          <option value="Lösung" <?php echo ($value->form == 'Lösung')?'selected':''; ?>>Lösung</option>
                          <option value="Tropfen" <?php echo ($value->form == 'Tropfen')?'selected':''; ?>>Tropfen</option>
                          <option value="Pflaster" <?php echo ($value->form == 'Pflaster')?'selected':''; ?>>Pflaster</option>
                          <option value="Zäpfchen" <?php echo ($value->form == 'Zäpfchen')?'selected':''; ?>>Zäpfchen</option>
                      </select>
                      <span><i class="fas fa-chevron-down"></i></span>
                    </div> -->
                    <div class="dropdown select-box select_form_dropdown_box">
                      <button class="dropdown-select form_values_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($value->form)?$value->form:''; ?>                     
                        <span><i class="fas fa-chevron-down"></i></span>
                      </button>
                      <ul class="dropdown-menu select-form_1" aria-labelledby="form_values_btn">
                        <li value="Tablette" data-toggle="tooltip" data-placement="bottom" onclick="select_form_dropdown_box(this)">Tablette</li>
                        <li value="Lösung" data-toggle="tooltip" data-placement="bottom" onclick="select_form_dropdown_box(this)">Lösung</li>
                        <li value="Tropfen" data-toggle="tooltip" data-placement="bottom" onclick="select_form_dropdown_box(this)">Tropfen</li>
                        <li value="Pflaster" data-toggle="tooltip" data-placement="bottom" onclick="select_form_dropdown_box(this)">Pflaster</li>
                        <li value="Zäpfchen" data-toggle="tooltip" data-placement="bottom" onclick="select_form_dropdown_box(this)">Zäpfchen</li>
                        <li value="Aerosol" data-toggle="tooltip" data-placement="bottom" onclick="select_form_dropdown_box(this)">Aerosol</li>
                      </ul>
                    </div>
                    <input type="hidden" name="data[<?php echo $key; ?>][form]" onchange="changeformvalues(this)" class="form_values-field" value="<?php echo ($value->form)?$value->form:''; ?>">
                </td>
                <?php 
                  $morgens_num = ($value->morgens)?$value->morgens:0;
                  $mittags_num = ($value->mittags)?$value->mittags:0;
                  $abends_num = ($value->abends)?$value->abends:0;
                  $zur_nacht_num = ($value->zur_nacht)?$value->zur_nacht:0;

                  $total_tg = $morgens_num + $mittags_num + $abends_num + $zur_nacht_num;
                ?>

                <td><input type="text" name="data[<?php echo $key; ?>][morgens]" class="form-control morgens_field" value="<?php echo str_replace('.', ',', round($value->morgens,1)); ?>" onkeyup="total_tagodosis(this)" oninput="this.value = this.value.replace(/[^0-9,]/g, '').replace(/(\.,*)\./g, '$1');"/></td>
                <td><input type="text" name="data[<?php echo $key; ?>][mittags]" class="form-control mittags_field" value="<?php echo str_replace('.', ',', round($value->mittags,1)); ?>" onkeyup="total_tagodosis(this)" oninput="this.value = this.value.replace(/[^0-9,]/g, '').replace(/(\.,*)\./g, '$1');"/></td>
                <td><input type="text" name="data[<?php echo $key; ?>][abends]" class="form-control abends_field" value="<?php echo str_replace('.', ',', round($value->abends,1)); ?>" onkeyup="total_tagodosis(this)" oninput="this.value = this.value.replace(/[^0-9,]/g, '').replace(/(\.,*)\./g, '$1');"/></td>
                <td><input type="text" name="data[<?php echo $key; ?>][zur_nacht]" class="form-control zur_nacht_field" value="<?php echo str_replace('.', ',', round($value->zur_nacht,1)); ?>" onkeyup="total_tagodosis(this)" oninput="this.value = this.value.replace(/[^0-9,]/g, '').replace(/(\.,*)\./g, '$1');"/></td>
                <td><input type="text" id="einheit" name="data[<?php echo $key; ?>][einheit]" class="form-control einheit" value="<?php echo $value->einheit; ?>"/ ></td>
                <td><input type="text" name="data[<?php echo $key; ?>][tagesdosis]" class="form-control total_tagodosis" readonly value="<?php echo str_replace('.', ',', round($total_tg,1)); ?>"/></td>
                <td>
                  <!-- <div class="select-box">
                    <select class="form-control" name="data[<?php echo $key; ?>][grund]">
                      <?php /* if (isset($value->grund)) {  ?>
                          <option value="<?php echo $value->grund; ?>" <?php if ($id == $value->begleitmedikation_id) { ?> selected <?php } ?> >
                            <?php echo $value->grund; ?>
                          </option>
                          <?php } */ ?>
                        <option value="Schmerz" <?php echo ($value->grund == 'Schmerz')?'selected':''; ?>>Schmerz</option>
                        <option value="Angststörung" <?php echo ($value->grund == 'Angststörung')?'selected':''; ?>>Angststörung</option>
                        <option value="Schlafstörung" <?php echo ($value->grund == 'Schlafstörung')?'selected':''; ?>>Schlafstörung</option>
                        <option value="Depression" <?php echo ($value->grund == 'Depression')?'selected':''; ?>>Depression</option>
                        <option value="Hypertonie" <?php echo ($value->grund == 'Hypertonie')?'selected':''; ?>>Hypertonie</option>
                    </select>
                    <span><i class="fas fa-chevron-down"></i></span>
                  </div> -->
                  <div class="dropdown select-box select_grund_dropdown_box">
                      <button class="dropdown-select grund_values_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($value->grund)?$value->grund:''; ?>                     
                        <span><i class="fas fa-chevron-down"></i></span>
                      </button>
                      <ul class="dropdown-menu select-form_1" aria-labelledby="grund_values_btn">
                        <li value="Schmerz" data-toggle="tooltip" data-placement="bottom" onclick="select_grund_dropdown_box(this)">Schmerz</li>
                        <li value="Angststörung" data-toggle="tooltip" data-placement="bottom" onclick="select_grund_dropdown_box(this)">Angststörung</li>
                        <li value="Schlafstörung" data-toggle="tooltip" data-placement="bottom" onclick="select_grund_dropdown_box(this)">Schlafstörung</li>
                        <li value="Depression" data-toggle="tooltip" data-placement="bottom" onclick="select_grund_dropdown_box(this)">Depression</li>
                        <li value="Hypertonie" data-toggle="tooltip" data-placement="bottom" onclick="select_grund_dropdown_box(this)">Hypertonie</li>
                      </ul>
                    </div>
                    <input type="hidden" name="data[<?php echo $key; ?>][grund]" class="grund_values-field" value="<?php echo ($value->grund)?$value->grund:''; ?>">
                </td>
                <td><input type="text" id="startdate" name="data[<?php echo $key; ?>][startdatum]" class="form-control startdate" value="<?php echo date('d.m.Y', strtotime($value->startdatum)); ?>"/></td>
                <td>

                    <input type="text" id="stopdate" name="data[<?php echo $key; ?>][stopdatum]" class="form-control stopdate" value="<?php echo ($value->stopdatum)?date('d.m.Y', strtotime($value->stopdatum)):''; ?>"/>

                  </td>
              </tr>
              <?php } } ?>
              <tr class="table-row-length">
                <td><button class="plus-btn cursor-pointer" type="button" onclick="addRow()">+</button></td>
                <td colspan="14" style="border-right: 0!important; border-bottom: 0!important;"></td>
                <!-- <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="select-box">
                      <select class="form-control">
                          <option></option>
                          <option></option>
                      </select>
                      <span><i class="fas fa-chevron-down"></i></span>
                    </div>
                </td>
                <td>
                    <div class="select-box">
                      <select class="form-control">
                          <option></option>
                          <option></option>
                      </select>
                      <span><i class="fas fa-chevron-down"></i></span>
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="select-box">
                      <select class="form-control">
                          <option></option>
                          <option></option>
                      </select>
                      <span><i class="fas fa-chevron-down"></i></span>
                    </div>
                </td>
                <td></td>
                <td></td> -->
              </tr>
            </tbody>
          </table>          
         </div>
         <div class="form-group btn-mar-top text-right">
            <button type="submit" class="btn btn-primary"><span>Speichern &amp; weiter</span></button>
          </div>
         </form>
        </div>
      </div>
   </div>
   <!-- <input type="text" class="form-control startdate1"/> -->
</section>
      <script type="text/javascript">
  function addRow(){
    var total_row = $('.table-row-length').length;
      //alert(total_row);
    $.ajax({
      url: "<?php echo base_url('process_control/add_new_begleithmedikation_row'); ?>",
      type: 'post',
      dataType: 'json',
      data: {digit:total_row},
      success: function(data) {

        if (total_row == 1) {
          $('.append-new-row').prepend(data.html);
        } else {
          $('.append-new-row .filled-row:last').after(data.html);
        }
        initDatepicker();
      }
    })
  }

  function total_tagodosis($this) {
    var morgens = $($this).closest('tr').find('.morgens_field').val().replace(',','.');
    var mittags = $($this).closest('tr').find('.mittags_field').val().replace(',','.');
    var abends = $($this).closest('tr').find('.abends_field').val().replace(',','.');
    var zur_nacht = $($this).closest('tr').find('.zur_nacht_field').val().replace(',','.');

    if (morgens.trim()) {
      var morgens_value =  morgens;


    } else {
      var morgens_value =  0;
    }

    if (mittags.trim()) {
      var mittags_value =  mittags;
    } else {
      var mittags_value =  0;
    }
    
    if (abends.trim()) {
      var abends_value =  abends;
    } else {
      var abends_value =  0;
    }
    
    if (zur_nacht.trim()) {
      var zur_nacht_value =  zur_nacht;
    } else {
      var zur_nacht_value =  0;
    }

    var total_tagodosis = parseFloat(morgens_value) + parseFloat(mittags_value) + parseFloat(abends_value) + parseFloat(zur_nacht_value);
    //alert(total_tagodosis);
    var new_total_tagodosis = total_tagodosis.toFixed(1).replace('.',',');
    //alert(new_total_tagodosis);

    $($this).closest('tr').find('.total_tagodosis').val(new_total_tagodosis);
  }
</script>
<!-- <style type="text/css">
  .datepicker > .datepicker-days{display: block !important;}
</style> -->
<script type="text/javascript">

  $(function(){
      initDatepicker();
      selectDate();
  });
  function selectDate()
  {
    var data_visit_number = $('.select_added_date option:selected').attr('data-visit-number');
    $('.visit-number').val(data_visit_number);
  }
  function initDatepicker(){
    var date = new Date();
   // var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    date.setDate(date.getDate());
    $('.startdate').datepicker({
        startDate: date,
        todayHighlight : true,
       // autoclose: true,
        format: "dd.mm.yyyy",
      });

      $('.stopdate').datepicker({
        startDate: date,
        todayHighlight : true,
       // autoclose: true,
        format: "dd.mm.yyyy",
      });
  }

  function changeformvalues($this) {
 

    var form_values = $($this).closest('tr').find('.form_values-field').val();
      // alert(form_values);

      if (form_values == "Tablette") {
          $($this).closest('tr').find('.einheit').val("Stück");
      } else if (form_values == "Tropfen") {
        $($this).closest('tr').find('.einheit').val("Tropfen");
      } else if (form_values == "Lösung") {
        $($this).closest('tr').find('.einheit').val("mL");
      } else if (form_values == "Pflaster") {
        $($this).closest('tr').find('.einheit').val("Stück");
      } else if (form_values == "Zäpfchen") {
        $($this).closest('tr').find('.einheit').val("Stück");
      } else if (form_values == "Aerosol") {
        $($this).closest('tr').find('.einheit').val("Hübe");
      } else {
        $($this).closest('tr').find('.einheit').val("");
      } 
    /*$(this).each(function() {
    var form_values = $(this).find('form_values_select');
    var selected_value = $('form_values_select').val();
      alert(selected_value);
      if (form_values == "Tablette") {
          $('#einheit_<?php echo $key ?>').val("Stück");
      }
      if (form_values == "Tropfen") {
        $('#einheit_<?php echo $key ?>').val("Tropfen");
      }
      if (form_values == "Lösung") {
        $('#einheit_<?php echo $key ?>').val("mL");
      }
      if (form_values == "Pflaster") {
        $('#einheit_<?php echo $key ?>').val("Stück");
      }
      if (form_values == "Zäpfchen") {
        $('#einheit_<?php echo $key ?>').val("Stück");
      }
    })*/
  }

  /*$('.select_form_dropdown_box li').click(function() {
      var getText = $(this).text(); 
      alert(getText);
      var getValue = $(this).attr('value');
      // alert(getValue);
      //alert(getText); 
      $(this).closest('tr').find('.form_values_btn').text(getText);     
      $(this).closest('tr').find('.form_values-field').val(getText);     
      //$('#form_values_btn').text(getText);
      //$('#form_values-field').val(getValue);
      changeformvalues(this);
   });*/

   function select_form_dropdown_box($this)
   {
      var getText = $($this).text(); 
      var getValue = $($this).attr('value');
      // alert(getValue);
      //alert(getText); 
      $($this).closest('tr').find('.form_values_btn').text(getText);     
      $($this).closest('tr').find('.form_values-field').val(getText);     
      //$('#form_values_btn').text(getText);
      //$('#form_values-field').val(getValue);
      changeformvalues($this);
   }

   function select_grund_dropdown_box($this) 
   {
      var getText = $($this).text(); 
      var getValue = $($this).attr('value');
       
      $($this).closest('tr').find('.grund_values_btn').text(getText);     
      $($this).closest('tr').find('.grund_values-field').val(getText); 
   }


  /*$('.select_grund_dropdown_box li').click(function() {
      var getText = $(this).text(); 
      var getValue = $(this).attr('value');
       
      $(this).closest('tr').find('.grund_values_btn').text(getText);     
      $(this).closest('tr').find('.grund_values-field').val(getText);     
   });*/
   function select_eiheit_dropdown_box($this) 
   {
      var getText = $($this).text(); 
      var getValue = $($this).attr('value');
       
      $($this).closest('tr').find('.eiheit_values_btn').text(getText);     
      $($this).closest('tr').find('.eiheit_values-field').val(getText); 
   }
 </script>