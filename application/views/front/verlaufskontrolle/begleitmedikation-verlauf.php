<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<section class="same-section same-blog schmerz-blog">
   <div class="container-fluid">
   <div class="same-heading">
      <h2 class="text-left">Verlauf Begleitmedikation</h2>
   </div>
   <div class="row">
       <?php if (isset($patient_medicines_graph) && !empty($patient_medicines_graph)) { ?>
       <?php foreach ($patient_medicines_graph as $key => $value) { ?>
        <div class="col-lg-6">
           <div class="chart-schmerz chart-schmerz-new-text chart-bxx text-center">
              <div id="chart-<?php echo $key; ?>"></div>
           </div>
           <script type="text/javascript">
              Highcharts.chart('chart-<?php echo $key; ?>', {
              chart: {
                zoomType: 'xy'
              },
              title: {
                text: 'Handelsname: <?php echo $value['medicine_name']; ?>'
              },
              subtitle: {
                text: ''
              },
              xAxis: [{
                categories: <?php echo $value['medicine_series']; ?>,
                crosshair: true
              }],
              yAxis: [{ // Primary yAxis
                labels: {
                  format: '{value}',
                  style: {
                    color: "#BABDCB"
                  }
                },
                title: {
                  text: 'NRS Schmerz',
                  style: {
                    color: "#BABDCB"
                  }
                }
              }, { // Secondary yAxis
                min: 0,
                
                title: {
                  text: '<?php echo $value['eiheit']; ?>',
                  style: {
                    color: "#9E2B5F"
                  }
                },
                labels: {
                  format: '{value}',
                  style: {
                    color: "#9E2B5F"
                  }
                },
                opposite: true
              }],
              tooltip: {
                shared: true
              },
              legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor:
                  Highcharts.defaultOptions.legend.backgroundColor || // theme
                  'rgba(255,255,255,0.25)'
              },
              series: [{
              
                name: 'NRS Schmerz',
                type: 'spline',
                color: '#BABDCB',
                data: <?php echo $value['medicine_column']; ?>,
                tooltip: {
                  valueSuffix: ''
                }
              
              }, {
                name: '<?php echo $value['eiheit']; ?>',
                type: 'spline',
                color: "#9E2B5F",
                yAxis: 1,
                data: <?php echo $value['medicine_line']; ?>,
              
                tooltip: {
                  valueSuffix: ''
                }
              }],
              exporting: {
              buttons: {
              contextButton: {
                 menuItems: [
                     'printChart',
                     'separator',
                     'downloadPNG',
                     'downloadJPEG',
                     'downloadPDF',
                     'downloadSVG'
                 ]
              }
              }
              }
              });
           </script>
        </div>
       <?php } ?>
       <?php } ?>
       <?php if (isset($begleitmedikation_count) && !empty($begleitmedikation_count) && $begleitmedikation_count == 1) { ?>
       		
       			
		       <div class="col-lg-6" style="margin-top: -55px">
		         <div class="same-heading">
		            <h2 class="text-left">Schmerzverlauf</h2>
		         </div>
		         <div class="chart-schmerz chart-schmerz-new-text chart-bxx text-center">
		            <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
		            <div id="chart-container"></div>
		         </div>
		      </div>
       		
  		<?php } ?>
   </div>
   <?php if (isset($begleitmedikation_count) && !empty($begleitmedikation_count) && $begleitmedikation_count > 1) { ?>

     <div class="row">
       <div class="col-lg-6">
           <div class="same-heading">
              <h2 class="text-left">Schmerzverlauf</h2>
           </div>
           <div class="chart-schmerz chart-schmerz-new-text chart-bxx text-center">
              <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
              <div id="chart-container"></div>
           </div>
        </div>
     </div>
  <?php ?>
<?php } ?>
</section>
<script type="text/javascript">
  Highcharts.chart('chart-container', {
  chart: {
    zoomType: 'xy'
  },
  title: {
    text: 'Schmerzverlauf'
  },
  subtitle: {
    text: ''
  },
  xAxis: [{
    categories: <?php echo $series; ?>,
    crosshair: true
  }],
  yAxis: [{ // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: "#BABDCB"
      }
    },
    title: {
      text: 'NRS Schmerz',
      style: {
        color: "#BABDCB"
      }
    }
  }, { // Secondary yAxis
    min: 0,
    
    title: {
      text: 'Dosis [mg THC]',
      style: {
        color: "#9E2B5F"
      }
    },
    labels: {
      format: '{value}',
      style: {
        color: "#9E2B5F"
      }
    },
    opposite: true
  }],
  tooltip: {
    shared: true
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 120,
    verticalAlign: 'top',
    y: 100,
    floating: true,
    backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || // theme
      'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'NRS Schmerz',
    type: 'spline',
    color: '#BABDCB',
    data: <?php echo $column; ?>,
    tooltip: {
      valueSuffix: ''
    }

  }, {
    name: 'Dosis [mg THC]',
    type: 'spline',
    yAxis: 1,
    color: '#9E2B5F',
    data: <?php echo $line; ?>,
    tooltip: {
      valueSuffix: ''
    }
  }],
    exporting: {
      buttons: {
          contextButton: {
              menuItems: [
                  'printChart',
                  'separator',
                  'downloadPNG',
                  'downloadJPEG',
                  'downloadPDF',
                  'downloadSVG'
              ]
          }
      }
  }
});
</script>