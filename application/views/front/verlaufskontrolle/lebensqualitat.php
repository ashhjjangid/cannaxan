<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<section class="same-section same-blog schmerz-blog">
 <div class="container-fluid">
    <div class="same-heading table-heading">
       <h2 class="text-left">Verlauf Lebensqualität</h2>
    </div>
    <div class="text-center">
      <div class="chart-schmerz  chart-bx text-center">
         <!-- <img src="<?php echo base_url('Prescription_medicines')?>images/schmerz.png" alt=""> -->
         <div class="emoji">
         <div id="chart-container"></div>
          </div>
      </div>
    </div>
 </div>
</section>
<script type="text/javascript">
  Highcharts.chart('chart-container', {
  chart: {
    zoomType: 'xy'
  },
  title: {
    text: 'Verlauf Lebensqualität'
  },
  subtitle: {
    text: ''
  },
  xAxis: [{
    categories: <?php echo $series; ?>,
    crosshair: true
  }],
  yAxis: [{ // Primary yAxis
    min: 12,
    max: 48,
    labels: {
      format: '{value}',
      style: {
        color: "#BABDCB"
      }
    },
    title: {
      text: 'SF-12 Score',
      style: {
        color: "#BABDCB"
      }
    }
  }, { // Secondary yAxis
  	min: 0,
    
    title: {
      min: 2,
      text: 'Dosis [mg THC]',
      style: {
        color: "#9E2B5F"
      }
    },
    labels: {
      format: '{value}',
      style: {
        color: "#9E2B5F"
      }
    },
    opposite: true
  }],
  tooltip: {
    shared: true
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 120,
    verticalAlign: 'top',
    y: 100,
    floating: true,
    backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || // theme
      'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'SF-12 Score',
    type: 'spline',   
    color: '#BABDCB',
    data: <?php echo $column; ?>,
    tooltip: {
      valueSuffix: ''
    }

  }, {
    name: 'Dosis [mg THC]',
    type: 'spline',
    yAxis: 1,
    color: "#9E2B5F",
    data: <?php echo $line; ?>,
    tooltip: {
      valueSuffix: ''
    }
  }],
  exporting: {
	    buttons: {
	        contextButton: {
	            menuItems: [
	                'printChart',
	                'separator',
	                'downloadPNG',
	                'downloadJPEG',
	                'downloadPDF',
	                'downloadSVG'
	            ]
	        }
	    }
	}
});
</script>