<section class="same-section">
         <div class="container-fluid">
            <div class="same-heading mb-4">
               <h2>Patientenangaben 3/3</h2>
            </div>
            <div class="row">
               <div class="col-md-7">
                  <label>Relevante Begleiterkrankung</label>
               </div>
               <div class="col-md-5">
                  <label>Bestehend seit</label>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-9">
                  <div class="row mt-3">
                     <div class="col-8"></div>
                     <div class="col-4">
                        <div class="row">
                           <div class="col"><label>> 3</label></div>
                           <div class="col"><label>> 6</label></div>
                           <div class="col"><label>> 12</label></div>
                           <div class="col"><label>Monaten</label></div>
                        </div>
                     </div>
                  </div>
                  <div class="row-list">
                     <div class="row align-items-center">
                        <div class="col-md-8">
                           <div class="no-input">
                              <strong>1</strong>
                              <input type="text" name="" class="form-control" placeholder="Rückenschmerzen, nicht näher bezeichnet">
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="row list-cheak">
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                     <input type="checkbox" id="me">
                                     <label for="me"><span class="cheak"></span></label>
                                  </div>
                              </div>
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                        <input type="checkbox" id="me-1">
                                        <label for="me-1"><span class="cheak"></span></label>
                                     </div>
                              </div>
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                        <input type="checkbox" id="me-3">
                                        <label for="me-3"><span class="cheak"></span></label>
                                     </div>
                              </div>
                              <div class="col cheak-width"></div>
                           </div>
                        </div>
                     </div>
                     <div class="row align-items-center">
                        <div class="col-md-8">
                           <div class="no-input">
                              <strong>2</strong>
                              <input type="text" name="" class="form-control" placeholder="Multiple Sklerose neuropathische Schmerzen">
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="row list-cheak">
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                     <input type="checkbox" id="me1">
                                     <label for="me1"><span class="cheak"></span></label>
                                  </div>
                              </div>
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                        <input type="checkbox" id="me-12">
                                        <label for="me-12"><span class="cheak"></span></label>
                                     </div>
                              </div>
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                        <input type="checkbox" id="me3">
                                        <label for="me3"><span class="cheak"></span></label>
                                     </div>
                              </div>
                              <div class="col cheak-width"></div>
                           </div>
                        </div>
                     </div>
                     <div class="row align-items-center">
                        <div class="col-md-8">
                           <div class="no-input">
                              <strong>3</strong>
                              <input type="text" name="" class="form-control" placeholder="Chronische Schmerzstörung mit somatischen und psychischen Faktoren">
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="row list-cheak">
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                     <input type="checkbox" id="me10">
                                     <label for="me10"><span class="cheak"></span></label>
                                  </div>
                              </div>
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                        <input type="checkbox" id="me-120">
                                        <label for="me-120"><span class="cheak"></span></label>
                                     </div>
                              </div>
                              <div class="col ">
                                 <div class="checkbox-custom small">
                                        <input type="checkbox" id="me33">
                                        <label for="me33"><span class="cheak"></span></label>
                                     </div>
                              </div>
                              <div class="col cheak-width"></div>
                           </div>
                        </div>
                     </div>
                     <div class="row align-items-center">
                        <div class="col-md-8">
                           <div class="no-input">
                              <button class="plus-btn">
                                 <strong><i class="fas fa-plus"></i></strong>
                              </button>
                           </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="w-100 text-right btn-ro-space">
               <a href="#" class="btn btn-primary"><span>Anmelden</span></a>
            </div>
         </div>
      </section>