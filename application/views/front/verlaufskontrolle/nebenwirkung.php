<section class="same-section begleiterkrankungen-section">
         <div class="container-fluid">
            <div class="same-heading ">
              <h2 class="text-left">Nebenwirkung</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Datum</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="29.02.2020">
                        </div>
                     </div>
                  </div>
               </div>
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Behandungstag</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="21">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4"></div>
            </div>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Visite</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="3">
                        </div>
                     </div>
                  </div>
               </div>
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Arzneimittel</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="CannaXan-THC">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4"></div>
               </div>
               <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-8">
                          <label>Keine kausal bedingte Nebenwirkung</label>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="" class="form-control text-center" placeholder="nein">
                          </div>
                       </div>
                    </div>
                    </div>
                    <div class="col-md-4"></div>
                  </div>
                </div>
               </div>
            </div>
            <div class="tabel-overlap">
              <table cellspacing="0"  cellpadding="0" width="100%" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                        <th>Nebenwirkung</th>
                        <th colspan="4" class="p-0">
                         <table cellpadding="0" cellspacing="0" width="100%" align="center">
                            <tr>
                               <th colspan="4"> <center> schweregrad</center></th>
                            </tr>
                            <tr>
                               <th style="background:#2CA61E; ">mild</th>
                               <th style="background:#FCAF16; ">moderat</th>
                               <th style="background:#F76816; ">schwer</th>
                               <th style="background:#D91313; ">lebensbedrohlich</th>
                            </tr>
                         </table></th>
                        <th>KAUSALITÄT ZUR<br> THERAPIE VORHANDEN</th>
                     </tr>
                  </thead>
                 <tr>
                    <td>Ubelkeit</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Erbrechen</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Appetitsteigerung</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Gewichtszunahme</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Konstipation</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Diarrho</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Mundtrockenheit</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Tachykardie</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Palpitaionen</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Hypotonie</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Hypotonie</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Schwindel</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Gleichgewichtsstorungen</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Verschwonmmenes Sehen</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Aufmerksamkeitsstorungen </td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                  <tr>
                    <td>Gedachtsnisstorungen </td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Dysarthrie</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Desorientierung</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 <tr>
                    <td>Mudigkeit</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;"><i class="fas fa-times"></i></td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">-</td>
                    <td style="text-align: center;">
                     <select>
                        <option>ja</option>
                        <option>ja</option>
                     </select>
                    </td>
                 </tr>
                 

              </table>
           </div> 
                <div class="col-md-12">
                  <div class="btn-mar-top text-right">
                    <a href="<?php echo base_url('Process_control/nebenwirkungen_auswertung')?>" class="btn btn-primary"><span>Speichern &amp; weiter</span></a>
                  </div>
                </div>
         </div>
      </section>