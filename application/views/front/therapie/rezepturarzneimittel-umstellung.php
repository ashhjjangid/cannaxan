<section class="same-section rezepturarzneimittel-section pb-0">
         <div class="container-fluid">
            <div class="page-heading">
              <div class="row align-items-center">
                <div class="col-md-7">
                  <div class="custom-heading">
                    <h2>Hinweis:<br> 
                      <span>Diesen Therapieplan 2x ausdrucken<br> (1x für Patient, 1x für Abgabe durch Patient bei Apotheke)</span>
                    </h2>
                  </div>
                </div>
                <div class="col-md-5 text-right">
                  <div class="min-width-btn">
                    <button class="btn btn-primary" onclick="makepdf()"><span>Therapieplan herunterladen</span></button>
                    <a class="btn btn-primary" href="<?php echo base_url('process_control/begleitmedikation?in='.$versichertennr) ?>"><span>Weiter mit Begleitmedikamente</span></a>
                  </div>
                </div>
              </div>
            </div>
            <section class="blue-line-section pb-0" style="border-bottom: none;">
              <div class="same-heading ">
                <div class="row align-items-center">
                  <div class="col-md-7">
                      <h2 class="text-left">ÄRZTLICHER THERAPIEPLAN</h2>
                  </div>
                  <div class="col-md-5 text-right">
                   <div class="heading-logo">
                    <img src="<?php echo $this->config->item('front_assets'); ?>images/logo.png" alt="logo">
                   </div>
                  </div>
                </div>
              </div>
              <div class="row ">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-7">
                        <label class="blod-text">Lebenslange Arztnummer
                          <span><?php echo $title.' '. $first_name.' '. $last_name.'<br/>'. $strabe. ' '.$hausnr.'<br/>'. $plzl.' '.$ort; ?><!-- Dr. med. Onkel Doktor um die Ecke Musterstraße 1a 81828 Musterstadt --></span>
                        </label>
                      </div>
                      <div class="col-sm-5">
                        <input type="text" name="lanr" class="form-control text-center" placeholder="29.02.2020" readonly="" value="<?php echo $lanr; ?>">
                      </div>
                    </div>
                  </div>
                  </div>
                 <div class="col-md-4">
                 </div>
                 <div class="col-md-4"></div>
                </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                      <div class="col-sm-7">
                         <label>Rezepturarzneimittel</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="rezepturarzneimittel" class="form-control text-center" readonly="" placeholder="CannaXan-THC" value="<?php echo $rezepturarzneimittel; ?>">
                      </div>
                   </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>Versichertennr.</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="versichertennr" class="form-control text-center" placeholder="A123456789" readonly="" value="<?php echo $versichertennr; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                      <div class="col-sm-7">
                         <label>Wirkstoff:</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="wirkstoff" class="form-control text-center" readonly="" placeholder="CannaXan-701-1.1" value="<?php echo $wirkstoff; ?>">
                      </div>
                   </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>Name</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="name" class="form-control text-center" placeholder="Muster" readonly="" value="<?php echo $name; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                      <div class="col-sm-7">
                         <label>Therapiedauer (Tage):</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="tag_name" class="form-control text-center" readonly="" placeholder="30" value="<?php echo $anzahl_therapietage; ?>">
                      </div>
                   </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>VorName</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="vorname" class="form-control text-center" placeholder="Beispiel" readonly="" value="<?php echo $vorname; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                      <div class="col-sm-7">
                         <label>Therapietyp:</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="dosistyp" class="form-control text-center" readonly="" placeholder="Umstellung" value="<?php echo 'Umstellung'; ?>">
                      </div>
                   </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>Geb. Datum</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="gedatum" class="form-control text-center" placeholder="TT.MM.JJJJ" readonly="" value="<?php echo $gedatum; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                      <div class="col-sm-7">
                         <label>Ausstellungsdatum:</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" id="datum" name="datum" class="form-control text-center" readonly="" placeholder="18.11.2019" value="<?php echo $datum; ?>">
                      </div>
                   </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>Telefonr.</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="telefone" class="form-control text-center" placeholder="0165 87654321" readonly="" value="<?php echo $telefone; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                      <div class="col-sm-7">
                         <label>Dosistyp</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="dosistyp" class="form-control text-center" readonly="" placeholder="Dosistyp 1" value="<?php echo $dosistyp; ?>">
                      </div>
                   </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-3 tabel-1small">
                      <div class="text-rezept gray-bx">
                        <div class="heading-right">
                          <span>Angaben für BtM Rezept:</span>
                           </div>
                          <div class="row">
                              <div class="col-md-12">
                                 <div class="text-rezept-event">
                                    <ul>
                                       <li><p>Summe THC <i class="font-style-none">[mg]</i> <input type="hidden" name="total_summe" class="total_summe_input" value="0"></p><span class="total_summe_thc">0</span></li>
                                        <li><p>Summe CannaXan 701-1.1 <i class="font-style-none">[mL]</i></p> <input type="hidden" name="total_cannaxan_thc" class="total_cannaxan_thc_input" value="0"><span class="Summe-CannaXan-THC">0</span></li>
                                        <li><p>Verordnete Anzahl 20 <i class="font-style-none">mL</i> Flaschen CannaXan 701-1.1 </p><input type="hidden" name="verordnete_anzahl" class="verordnete_anzahl_input" value="0"><span class="Anzahl-20mL-Flaschen">0</span></li>
                                        <li><p>Rechnerische Restmenge <br> CannaXan 701-1.1 <br> nach 30 Tagen <i class="font-style-none">[mL]</i></p> <input type="hidden" name="rechnerische_restmenge" class="rechnerische_restmenge_input" value="0"><span class="CannaXan-THC-nach-30-Tagen">0</span></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                      </div>
                     <div class="col-md-9 tabel-2small">
                      <div class="table-box-form">
                        <!-- <span><i class="text-red">Eingabewerte in rot.</i> <strong>ALLE SCHWARZEN UND <span>BLAUEN</span> WERTE WERDEN BERECHNET</strong></span> -->
                        <div class="curve-table-bx curve-table2-bx">
                          <div class="curve-table-header">
                            <img src="<?php echo $this->config->item('front_assets'); ?>images/table-header-1.png" alt="logo"/>
                          </div>
                          <table cellspacing="0" cellspacing="0" class="table table-striped table-bordered">
                            <?php if ($table) { $i = 1; ?>

                            <?php foreach ($table as $key => $value) { 
                              $total_summe_1 = ($typUmstellung[0]->morgens_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg) + ($typUmstellung[0]->mittags_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg) + ($typUmstellung[0]->abends_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg) + ($typUmstellung[0]->nachts_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg);

                              ?>    
                              <tr class="row_<?php echo $key; ?> adjust-colum">
                                <td class="no-cell"><?php echo $i; $i++; ?></td>
                                <td class="no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][morgens_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($typUmstellung[0]->morgens_anzahl_durchgange)); ?>" data-key="<?php echo $key; ?>" readonly=""></td>

                                <td class="no-cell morgens_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][morgens_anzahl_sps]" value="<?php echo $typUmstellung[0]->morgens_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($typUmstellung[0]->morgens_anzahl_sps)); ?></span readonly=""></td>

                                <td class="selected no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][mittags_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($typUmstellung[0]->mittags_anzahl_durchgange)); ?>" data-key="<?php echo $key; ?>" readonly=""></td>

                                <td class="selected no-cell mittags_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][mittags_anzahl_sps]" value="<?php echo $typUmstellung[0]->mittags_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($typUmstellung[0]->mittags_anzahl_sps)); ?></span readonly=""></td>

                                <td class="no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][abends_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($typUmstellung[0]->abends_anzahl_durchgange)); ?>" data-key="<?php echo $key; ?>" readonly=""></td>

                                <td class="no-cell abends_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][abends_anzahl_sps]" value="<?php echo $typUmstellung[0]->abends_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($typUmstellung[0]->abends_anzahl_sps)); ?></span readonly=""></td>

                                <td class="selected no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][nachts_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($typUmstellung[0]->nachts_anzahl_durchgange)); ?>" data-key="<?php echo $key; ?>" readonly=""></td>

                                <td class="selected no-cell nachts_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][nachts_anzahl_sps]" value="<?php echo $typUmstellung[0]->nachts_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($typUmstellung[0]->nachts_anzahl_sps)); ?></span readonly=""></td>

                                <td class="big-cell thc_morgens_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_morgens_mg]" value="<?php echo $typUmstellung[0]->morgens_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', substr($typUmstellung[0]->morgens_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg, 0, 3)); ?></span readonly=""></td>

                                <td class="big-cell thc_mittags_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_mittags_mg]" value="<?php echo $typUmstellung[0]->mittags_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', substr($typUmstellung[0]->mittags_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg, 0, 3)); ?></span readonly=""></td>

                                <td class="big-cell thc_abends_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_abends_mg]" value="<?php echo $typUmstellung[0]->abends_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', substr($typUmstellung[0]->abends_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg, 0, 3)); ?></span readonly=""></td>

                                <td class="big-cell thc_nachts_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_nachts_mg]" value="<?php echo $typUmstellung[0]->nachts_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', substr($typUmstellung[0]->nachts_anzahl_sps * $zuordnung_sps->menge_thc_sps_mg, 0, 3)); ?></span readonly=""></td>

                                <td class="big-cell summe_thc_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][summe_thc_mg]" value="<?php echo $total_summe_1; ?>"><span class="text-field"><?php echo str_replace('.', ',', ($total_summe_1>=10)?substr($total_summe_1, 0, 4):substr($total_summe_1, 0, 3)); ?></span readonly=""></td>

                              </tr>
                            <?php } ?>
                          <?php } ?>
                          </table>
                          <div class="table-right-part">
                            <p>Anz. = Anzahl</p>
                            <p>SPS = Sprühstöße </p>
                          </div>
                          <div class="rotate-border"></div>
                        </div>
                      </div>
                     </div>
                   </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <section class="same-section titration-event pt-0">
        <div class="container-fluid">
          
          <section class=" blue-line-section " style="border-top: none;">
            <div class="container-fluid">
              <div class="event-blog">
                <ul>
                  <li>
                    <span>Einnahmehinweis:</span>
                    <p>1. Morgens und/oder abends <b>jeweils EINEN</b> Sprühstoß in die linke Wangeninnenseite, unter die Zunge, sowie in die rechte Wangeninnenseite geben. Dieser Vorgang wird als <b>Durchgang</b> bezeichnet.</p>
                    <div class="einnah-images">
                      <img src="<?php echo $this->config->item('front_assets'); ?>images/einnahmehinweis.png" alt="">
                      <div class="row">
                        <div class="col-md-4">
                          <p>Linke Wangeninnenseite</p>
                        </div>
                        <div class="col-md-4">
                          <p>Unter die Zunge</p>
                        </div>
                        <div class="col-md-4">
                          <p>Rechte Wangeninnenseite</p>
                        </div>

                      </div>
                    </div>
                  </li>
                  <li>
                    <p>2. 90 Sekunden warten, dann schlucken!</p>
                  </li>
                  <li>
                    <p>3. Obigen Durchgang gemäß Verordnung wiederholen (Beispiel: bei jeweils 6 Sprühstößen morgens und abends: obigen Durchgang 2x wiederholen und dazwischen 90 Sekunden warten, dann schlucken).</p>
                  </li>
                  <li>
                    <span>Hinweise für Patienten:</span>
                    <p><!-- Die Dosissteigerung ist solange durchzuführen bis die Nebenwirkungen (Schwindel, Berauschtheit etc.) zu stark werden. Danach sollte auf die Dosis zurückgegangen werden, bei der die Nebenwirkungen noch erträglich waren. Sollten bei der niedrigen Dosis noch Nebenwirkungen bestehen, werden diese innerhalb weniger Tage bei Beibehaltung der Dosis verschwinden. -->
                      Bitte steigern Sie die Dosis gemäß dem Therapieplan bis sich eine ausreichende Wirkung eingestellt hat. Sollten Nebenwirkungen  wie z.B. Schwindel oder Berauschtheit eintreten, kontaktieren Sie bitte Ihren behandelnden Arzt und bleiben Sie bei der bestehenden Dosis. 
                    </p>
                  </li>
                  <li>
                    <span>Hinweise für Rezepteinreichung in Apotheke:</span>
                    <p>
                       Bitte geben Sie eine Kopie dieses Therapieplans zusammen mit Ihrem Rezept in der Apotheke ab, damit der Apotheker eine gesetzlich verpflichtete Plausibilitätsprüfung durchführen kann.
                    </p>
                  </li>
                </u1>
                <div class="bottom-text">
                  <p>Stempel/Unterschrift des/der verordneten Arztes/Ärztin</p>
                <?php if ($unterschrift) { ?>
                  <img height="75" width="200" src="<?php echo base_url('assets/uploads/unterschrift-images/'. $unterschrift);?>" align="right" alt="">
                <?php } ?>
                </div>
              </div>
            </div>
          </section>
        </div>
      </section>
<script>
  function makepdf() {
    var getSummeValue = $('.total_summe_input').val();
    var getTotalCannaxanTHC = $('.total_cannaxan_thc_input').val();
    var getVerordneteAnzahl = $('.verordnete_anzahl_input').val();
    var getRechnerische = $('.rechnerische_restmenge_input').val();
    var url = "<?php echo base_url('therapie/generate_umstellung_pdf?in='.$versichertennr); ?>&summethc="+getSummeValue+'&totalcannthc='+getTotalCannaxanTHC+'&verordneteanzahl='+getVerordneteAnzahl+'&rechnerische='+getRechnerische;
    /*alert(getSummeValue);
    alert(getTotalCannaxanTHC);
    alert(getVerordneteAnzahl);
    alert(getRechnerische);
    alert(url);*/
    window.location.href = url;
  }

  $(function(){
    total_summe_thc();
  });

  function total_summe_thc()
  {
    var total_sum = 0;
    var total_length = parseInt($('.summe_thc_mg').length) - 1;
   
    $('.summe_thc_mg').each(function(key, value){
      // alert($(this).find('input').val());
      total_sum = parseFloat(total_sum) + parseFloat($(this).find('input').val());
      var new_total_sum = total_sum.toFixed(1).replace(/\./g, ',');

      if (key == total_length) {
        $('.total_summe_thc').text(new_total_sum);
        $('.total_summe_input').val(total_sum.toFixed(1));
      }
    })
    
    var konzentration_thc_mg_ml = "<?php echo $zuordnung_sps->konzentration_thc_mg_ml; ?>";

    var summe_CannaXan_THC = (total_sum / konzentration_thc_mg_ml).toFixed(1);
    var new_summe_CannaXan_THC = summe_CannaXan_THC.replace(/\./g, ',');
    var anzahl_20mL_Flaschen = (summe_CannaXan_THC / 20).toFixed(1);
    var anzahl_20mL_Flaschen = Math.ceil(anzahl_20mL_Flaschen);
    //alert(anzahl_20mL_Flaschen);
    var new_anzahl_20mL_Flaschen = anzahl_20mL_Flaschen;
    var cannaXan_THC_nach_30_Tagen = ((anzahl_20mL_Flaschen * 20) - summe_CannaXan_THC).toFixed(1);
    var new_cannaXan_THC_nach_30_Tagen = cannaXan_THC_nach_30_Tagen.replace(/\./g, ',');
    $('.Summe-CannaXan-THC').text(new_summe_CannaXan_THC);
    $('.Anzahl-20mL-Flaschen').text(new_anzahl_20mL_Flaschen);
    $('.CannaXan-THC-nach-30-Tagen').text(new_cannaXan_THC_nach_30_Tagen);

    $('.total_cannaxan_thc_input').val(summe_CannaXan_THC);
    $('.verordnete_anzahl_input').val(anzahl_20mL_Flaschen);
    $('.rechnerische_restmenge_input').val(cannaXan_THC_nach_30_Tagen);
  }

  function currentdate() {
    $('#datum').datepicker({
         format: 'yyyy-mm-dd'
      })
  }
</script>