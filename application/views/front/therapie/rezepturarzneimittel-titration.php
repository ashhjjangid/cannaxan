<section class="same-section rezepturarzneimittel-section pb-0">
   <div class="container-fluid">
      <div class="page-heading">
        <div class="row align-items-center">
          <div class="col-md-7">
            <div class="custom-heading">
              <h2>Hinweis:<br> 
                <span>Diesen Therapieplan 2x ausdrucken<br> (1x für Patient, 1x für Abgabe durch Patient bei Apotheke)</span>
              </h2>
            </div>
          </div>
          <div class="col-md-5 text-right">
            <div class="min-width-btn">
              <button class="btn btn-primary" onclick="makepdf()"><span>Therapieplan herunterladen</span></button>
              <a class="btn btn-primary" href="<?php echo base_url('process_control/begleitmedikation?in='.$versichertennr) ?>"><span>weiter mit Begleitmedikation</span></a>
            </div>
          </div>
        </div>
      </div>
      <section class="blue-line-section b-0 pb-0" style="border-bottom: none;">
        <div class="same-heading ">
          <div class="row align-items-center">
            <div class="col-md-7">
                <h2 class="text-left">ÄRZTLICHER THERAPIEPLAN</h2>
            </div>
            <div class="col-md-5 text-right">
             <div class="heading-logo">
              <img src="<?php echo $this->config->item('front_assets'); ?>images/logo.png" alt="logo">
             </div>
            </div>
          </div>
        </div>
        <div class="row ">
          <div class="col-md-6">
            <div class="form-group">
              <div class="row">
                <div class="col-sm-7">
                  <label class="blod-text">Lebenslange Arztnummer
                    <span><?php echo $title.' '. $first_name.' '. $last_name.'<br/>'. $strabe. ' '.$hausnr.'<br/>'. $plzl.' '.$ort; ?><!-- Dr. med. Onkel Doktor um die Ecke Musterstraße 1a 81828 Musterstadt --></span>
                  </label>
                </div>
                <div class="col-sm-5">
                  <input type="text" name="lanr" class="form-control text-center" placeholder="29.02.2020" readonly="" value="<?php echo $lanr; ?>">
                </div>
              </div>
            </div>
            </div>
           <div class="col-md-4">
           </div>
           <div class="col-md-4"></div>
          </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-7">
                     <label>Rezepturarzneimittel</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="rezepturarzneimittel" class="form-control text-center" readonly="" placeholder="CannaXan-THC" value="<?php echo $rezepturarzneimittel; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-5 offset-md-2 align-self-center">
                     <label>Versichertennr.</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="versichertennr" class="form-control text-center" readonly="" placeholder="A123456789" value="<?php echo $versichertennr; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-7">
                     <label>Wirkstoff:</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="wirkstoff" class="form-control text-center" readonly="" placeholder="CannaXan-701-1.1" value="<?php echo $wirkstoff; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-5 offset-md-2 align-self-center">
                     <label>Name</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="name" class="form-control text-center" readonly="" placeholder="Muster" value="<?php echo $name; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-7">
                     <label>Therapiedauer (Tage):</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="tag_name" class="form-control text-center" readonly="" placeholder="30" value="30">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-5 offset-md-2 align-self-center">
                     <label>VorName</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="vorname" class="form-control text-center" readonly="" placeholder="Beispiel" value="<?php echo $vorname; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-7">
                     <label>Therapietyp:</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="dosistyp" class="form-control text-center" readonly="" placeholder="Titration" value="<?php echo 'Titration'; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-5 offset-md-2 align-self-center">
                     <label>Geb. Datum</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="gedatum" class="form-control text-center" readonly="" placeholder="TT.MM.JJJJ" value="<?php echo $gedatum; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-7">
                     <label>Ausstellungsdatum:</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" id="datum" name="datum" class="form-control text-center" readonly="" placeholder="18.11.2019" value="<?php echo $datum; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-5 offset-md-2 align-self-center">
                     <label>Telefonr.</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="telefone" class="form-control text-center" readonly="" placeholder="0165 87654321" value="<?php echo $telefone; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-7">
                     <label>Dosistyp</label>
                  </div>
                  <div class="col-sm-5">
                     <input type="text" name="dosistyp" class="form-control text-center" readonly="" placeholder="Dosistyp 1" value="<?php echo $dosistyp; ?>">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-3 tabel-1small">
                <div class="text-rezept padding-top">
                  <div class="heading-right">
                    <span>Angaben für BtM Rezept:</span>
                     </div>
                    <div class="row">
                        <div class="col-md-12">
                           <div class="text-rezept-event low-span-width">
                              <ul>
                                 <li>Summe THC <i class="font-style-none"> [mg]</i> <span><?php echo str_replace('.', ',', round($sum_thc_mg,1)); ?></span></li>
                                 <li><p> Summe CannaXan 701-1.1 <i class="font-style-none">[mL]</i></p> <span><?php echo str_replace('.', ',', round($sum_cannaxan_thc,1)); ?></span></li>
                                 <li><p>Verordnete Anzahl 20 <i class="font-style-none">mL</i> Flaschen CannaXan 701-1.1</p> <span><?php echo str_replace('.', ',', (int)$verordnete_anzahl); ?></span></li>
                                 <li><p>Rechnerische Restmenge CannaXan 701-1.1 nach 30 Tagen <i class="font-style-none">[mL]</i></p><span><?php echo str_replace('.', ',', round($rechnerische_restmenge,1)); ?></span></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                </div>
               <div class="col-md-9 tabel-2small">
                <div class="table-box-form">
                  <!-- <span><i class="text-red">Eingabewerte in rot.</i> <strong>ALLE SCHWARZEN UND <span>BLAUEN</span> WERTE WERDEN BERECHNET</strong></span> -->
                  <div class="curve-table-bx curve-table2-bx">
                    <div class="curve-table-header">
                      <img src="<?php echo $this->config->item('front_assets'); ?>images/table-header-1.png" alt="logo"/>
                    </div>
                    <table cellspacing="0" cellspacing="0" class="table table-striped table-bordered">
                      <?php if ($dosistyp_data) { $i = 1; ?>

                      <?php foreach ($dosistyp_data as $key => $value) { ?>    
                        <tr class="row_<?php echo $key; ?> adjust-colum">
                          <td class="no-cell"><?php echo $i; $i++; ?></td>
                          <td class="no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][morgens_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->morgens_anzahl_durchgange)); ?>" data-key="<?php echo $key; ?>" readonly=""></td>

                          <td class="no-cell morgens_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][morgens_anzahl_sps]" value="<?php echo $value->morgens_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->morgens_anzahl_sps)); ?></span readonly=""></td>

                          <td class="selected no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][mittags_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->mittags_anzahl_durchgange)); ?>" data-key="<?php echo $key; ?>" readonly=""></td>

                          <td class="selected no-cell mittags_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][mittags_anzahl_sps]" value="<?php echo $value->mittags_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->mittags_anzahl_sps)); ?></span readonly=""></td>

                          <td class="no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][abends_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->abends_anzahl_durchgange)); ?>" data-key="<?php echo $key; ?>" readonly=""></td>

                          <td class="no-cell abends_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][abends_anzahl_sps]" value="<?php echo $value->abends_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->abends_anzahl_sps)); ?></span readonly=""></td>

                          <td class="selected no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][nachts_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->nachts_anzahl_durchgange)); ?>" data-key="<?php echo $key; ?>" readonly=""></td>

                          <td class="selected no-cell nachts_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][nachts_anzahl_sps]" value="<?php echo $value->nachts_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->nachts_anzahl_sps)); ?></span readonly=""></td>

                          <td class="big-cell thc_morgens_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_morgens_mg]" value="<?php echo $value->thc_morgens_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', substr($value->thc_morgens_mg, 0, 3)); ?></span readonly=""></td>

                          <td class="big-cell thc_mittags_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_mittags_mg]" value="<?php echo $value->thc_mittags_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', substr($value->thc_mittags_mg, 0, 3)); ?></span readonly=""></td>

                          <td class="big-cell thc_abends_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_abends_mg]" value="<?php echo $value->thc_abends_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', substr($value->thc_abends_mg, 0, 3)); ?></span readonly=""></td>

                          <td class="big-cell thc_nachts_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_nachts_mg]" value="<?php echo $value->thc_nachts_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', substr($value->thc_nachts_mg, 0, 3)); ?></span readonly=""></td>

                          <td class="big-cell summe_thc_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][summe_thc_mg]" value="<?php echo $value->summe_thc_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', ($value->summe_thc_mg>=10)?substr($value->summe_thc_mg, 0, 4):substr($value->summe_thc_mg, 0, 3)); ?></span readonly=""></td>

                        </tr>
                      <?php } ?>
                    <?php } ?>
                    </table>
                    <div class="table-right-part">
                      <p>Anz. = Anzahl</p>
                      <p>SPS = Sprühstöße </p>
                    </div>
                    <div class="rotate-border"></div>
                  </div>
                </div>
               </div>
             </div>
            </div>
        </div>
      </section>
    </div>
  </div>
</section>
<section class="same-section titration-event pt-0">
  <div class="container-fluid">
    <section class="blue-line-section pt-0" style=" border-top: none;">
      <div class="event-blog">
        <ul>
          <li>
            <span>Einnahmehinweis:</span>
            <p>1. Morgens und/oder abends <b>jeweils EINEN</b> Sprühstoß in die linke Wangeninnenseite, unter die Zunge, sowie in die rechte Wangeninnenseite geben. Dieser Vorgang wird als <b>Durchgang</b> bezeichnet.</p>
            <div class="einnah-images">
              <img src="<?php echo $this->config->item('front_assets'); ?>images/einnahmehinweis.png" alt="">
              <div class="row">
                <div class="col-md-4">
                  <p>Linke Wangeninnenseite</p>
                </div>
                <div class="col-md-4">
                  <p>Unter die Zunge</p>
                </div>
                <div class="col-md-4">
                  <p>Rechte Wangeninnenseite</p>
                </div>

              </div>
            </div>
          </li>
          <li>
            <p>2. 90 Sekunden warten, dann schlucken!</p>
          </li>
          <li>
            <p>3. Obigen Durchgang gemäß Verordnung wiederholen (Beispiel: bei jeweils 6 Sprühstößen morgens und abends: obigen Durchgang 2x wiederholen und dazwischen 90 Sekunden warten, dann schlucken).</p>
          </li>
          <li>
            <span>Hinweise für Patienten:</span>
            <p><!-- Die Dosissteigerung ist solange durchzuführen bis die Nebenwirkungen (Schwindel, Berauschtheit etc.) zu stark werden. Danach sollte auf die Dosis zurückgegangen werden, bei der die Nebenwirkungen noch erträglich waren. Sollten bei der niedrigen Dosis noch Nebenwirkungen bestehen, werden diese innerhalb weniger Tage bei Beibehaltung der Dosis verschwinden. -->
              Bitte steigern Sie die Dosis gemäß dem Therapieplan bis sich eine ausreichende Wirkung eingestellt hat. Sollten Nebenwirkungen  wie z.B. Schwindel oder Berauschtheit eintreten, kontaktieren Sie bitte Ihren behandelnden Arzt und bleiben Sie bei der bestehenden Dosis. 
            </p>
          </li>
          <li>
            <span>Hinweise für Rezepteinreichung in Apotheke:</span>
            <p>
               Bitte geben Sie eine Kopie dieses Therapieplans zusammen mit Ihrem Rezept in der Apotheke ab, damit der Apotheker eine gesetzlich verpflichtete Plausibilitätsprüfung durchführen kann.
            </p>
          </li>
        </u1>
        <div class="bottom-text">
          <p>Stempel/Unterschrift des/der verordneten Arztes/Ärztin</p>
        <?php if ($unterschrift) { ?>
          <img height="75" width="200" src="<?php echo base_url('assets/uploads/unterschrift-images/'. $unterschrift);?>" align="right" alt="">
        <?php } ?>
        </div>
      </div>
      
    </section>
  </div>
</section>
<script>
  function makepdf() {
    window.location.href = "<?php echo base_url('therapie/generate_pdf?in='.$versichertennr); ?>"
  }

  function currentdate() {
    $('#datum').datepicker({
         format: 'yyyy-mm-dd'
      })
  }
</script>