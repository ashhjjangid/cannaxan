<div class="tabel-1box tabel-3box">
  <table cellspacing="0" class="table  table-bordered">
   <tr>
      <th>Äquivalente THC Extrakt Dosis [mg/Tag]</th>
      <th class="border-right-none">Anzahl Tropfen. Vertanical THC/CBD Extrakt. 1 Tropfen des Vollextraktes entspricht: 0,6 mg THC & 0,6 mg CBD</th>
      <th colspan="8" class="action-td">
         <div class="tabel-1box-header">
          <div class="row">
            <div class="col morgens sub-box color">
              <span class="border-dark"></span>
              <div class="sub-heading">Morgens</div>
              <div class="row">
                <div class="col"><div class="text-box anz"><p>Anzahl Durchgänge</p></div></div>
                <div class="col"><div class="text-box sps"><p>Anzahl SPS</p></div></div>
              </div>
            </div>
            <div class="col morgens sub-box color">
              <span class="border-dark"></span>
              <div class="sub-heading">Mittags</div>
              <div class="row">
                <div class="col"><div class="text-box anz"><p class="colorp">Anzahl Durchgänge</p></div></div>
                <div class="col"><div class="text-box sps"><p class="colorp">Anzahl SPS</p></div></div>
              </div>
            </div>
            <div class="col morgens sub-box color">
              <span class="border-dark"></span>
              <div class="sub-heading">Abends</div>
              <div class="row">
                <div class="col"><div class="text-box anz"><p>Anzahl Durchgänge</p></div></div>
                <div class="col"><div class="text-box sps"><p>Anzahl SPS</p></div></div>
              </div>
            </div>
            <div class="col morgens sub-box color">
              <span class="border-dark"></span>
              <div class="sub-heading">Nachts</div>
              <div class="row">
                <div class="col"><div class="text-box anz"><p class="colorp">Anzahl Durchgänge</p></div></div>
                <div class="col"><div class="text-box sps"><p class="colorp">Anzahl SPS</p></div></div>
              </div>
            </div>
          </div>
        </div>
      </th>
      <th class="border-left-none">START tgl. CannaXan-THC Dosis [mg THC]</th>
      <th>Transferfaktor</th>
      <th>Eintitration</th>
   </tr>
      <?php if (isset($typUmstellung) && !empty($typUmstellung)) { ?>

    <?php $i = 1; foreach ($typUmstellung as $key => $value) { ?>
      <tr class="row_<?php echo $i; ?>">
        <td class="first-row"><input type="hidden" value="<?php echo $value->tag_name; ?>" name="typumstelung[<?php echo $i; ?>][tag_name]"><?php // echo str_replace('.', ',', round($value->tag_name)); ?><?php $tag_value =str_replace('.', ',', round($value->tag_name, 1)); if (strpos($tag_value,',') === false) { echo $tag_value.',0'; } else { echo $tag_value; } ?></td>
        <td><input type="hidden" value="<?php echo $value->tag_description; ?>" name="typumstelung[<?php echo $i; ?>][tag_description]"><?php echo str_replace('.', ',', round($value->tag_description)); ?></td>
        <td class="nocell"><p><input type="text" name="typumstelung[<?php echo $i; ?>][morgens_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->morgens_anzahl_durchgange)); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMorgens(this)" data-key="<?php echo $i; ?>"></p></td>
        <td class="nocell morgens_anzahl_sps"><input type="hidden" name="typumstelung[<?php echo $i; ?>][morgens_anzahl_sps]" value="<?php echo $value->morgens_anzahl_sps; ?>"><p class="text-field"><?php echo str_replace('.', ',', round($value->morgens_anzahl_sps)); ?></p></td>
        <td class="selected nocell"><p><input type="text" name="typumstelung[<?php echo $i; ?>][mittags_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->mittags_anzahl_durchgange)); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMittags(this)" data-key="<?php echo $i; ?>"></p></td>
        <td class="selected nocell mittags_anzahl_sps"><input type="hidden" name="typumstelung[<?php echo $i; ?>][mittags_anzahl_sps]" value="<?php echo $value->mittags_anzahl_sps; ?>"><p class="text-field"><?php echo str_replace('.', ',', round($value->mittags_anzahl_sps)); ?></p></td>
        <td class="nocell"><p><input type="text" name="typumstelung[<?php echo $i; ?>][abends_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->abends_anzahl_durchgange)); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpAbends(this)" data-key="<?php echo $i; ?>"></p></td>
        <td class="nocell abends_anzahl_sps"><input type="hidden" name="typumstelung[<?php echo $i; ?>][abends_anzahl_sps]" value="<?php echo $value->abends_anzahl_sps; ?>"><p class="text-field"><?php echo str_replace('.', ',', round($value->abends_anzahl_sps)); ?></p></td>
        <td class="nocell selected"><p><input type="text" name="typumstelung[<?php echo $i; ?>][nachts_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->nachts_anzahl_durchgange)); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpNachts(this)" data-key="<?php echo $i; ?>"></p></td>
        <td class="nocell selected nachts_anzahl_sps"><input type="hidden" name="typumstelung[<?php echo $i; ?>][nachts_anzahl_sps]" value="<?php echo $value->nachts_anzahl_sps; ?>"><p class="text-field"><?php echo str_replace('.', ',', round($value->nachts_anzahl_sps)); ?></p></td>
        <td class="transferfaktor"><input type="hidden" name="typumstelung[<?php echo $i; ?>][transferfaktor]" value="<?php echo $value->start_tgl_cannaxan_thc_dosis_mg_thc; ?>"> <span class="text-field"><?php // echo str_replace('.', ',', round($value->start_tgl_cannaxan_thc_dosis_mg_thc,1)); ?><?php $number_value =str_replace('.', ',', round($value->start_tgl_cannaxan_thc_dosis_mg_thc, 1)); if (strpos($number_value,',') === false) { echo $number_value.',0'; } else { echo $number_value; } ?></span></td>
        <td class="eintitration"><input type="hidden" name="typumstelung[<?php echo $i; ?>][eintitration]" value="<?php echo $value->transferfaktor; ?>"> <span class="text-field"><?php echo str_replace('.', ',', round($value->transferfaktor,1)); ?></span></td>
        <td>nein, sofortige Umstellung auf CannaXan-THC möglich. Reduktion der Dosis innerhalb von 5 Wochen Therapie möglich.</td>
      </tr>
    <?php $i++; } ?>
   <?php } else { ?>
   <tr class="row_1">
      <td><input type="hidden" value="12.5" name="typumstelung[1][tag_name]">12.5</td>
      <td><input type="hidden" value="0.5" name="typumstelung[1][tag_description]">0.5</td>
      <td class="nocell"><p><input type="text" name="typumstelung[1][morgens_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMorgens(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell morgens_anzahl_sps"><input type="hidden" name="typumstelung[1][morgens_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="selected nocell"><p><input type="text" name="typumstelung[1][mittags_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMittags(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="selected nocell mittags_anzahl_sps"><input type="hidden" name="typumstelung[1][mittags_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="nocell"><p><input type="text" name="typumstelung[1][abends_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpAbends(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell abends_anzahl_sps"><input type="hidden" name="typumstelung[1][abends_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="nocell selected"><p><input type="text" name="typumstelung[1][nachts_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpNachts(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell selected nachts_anzahl_sps"><input type="hidden" name="typumstelung[1][nachts_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="transferfaktor"><input type="hidden" name="typumstelung[1][transferfaktor]" value="0"> <span class="text-field">0</span></td>
      <td class="eintitration"><input type="hidden" name="typumstelung[1][eintitration]" value="0"> <span class="text-field">0</span></td>
      <td>nein, sofortige Umstellung auf CannaXan-THC möglich. Reduktion der Dosis innerhalb von 5 Wochen Therapie möglich.</td>
    </tr>
    <tr class="row_2">
      <td><input type="hidden" value="25" name="typumstelung[2][tag_name]">25</td>
      <td><input type="hidden" value="1" name="typumstelung[2][tag_description]">1</td>
      <td class="nocell"><p><input type="text" name="typumstelung[2][morgens_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMorgens(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell morgens_anzahl_sps"><input type="hidden" name="typumstelung[2][morgens_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="selected nocell"><p><input type="text" name="typumstelung[2][mittags_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMittags(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="selected nocell mittags_anzahl_sps"><input type="hidden" name="typumstelung[2][mittags_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="nocell"><p><input type="text" name="typumstelung[2][abends_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpAbends(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell abends_anzahl_sps"><input type="hidden" name="typumstelung[2][abends_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="nocell selected"><p><input type="text" name="typumstelung[2][nachts_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpNachts(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell selected nachts_anzahl_sps"><input type="hidden" name="typumstelung[2][nachts_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="transferfaktor"><input type="hidden" name="typumstelung[2][transferfaktor]" value="0"> <span class="text-field">0</span></td>
      <td class="eintitration"><input type="hidden" name="typumstelung[2][eintitration]" value="0"> <span class="text-field">0</span></td>
      <td>nein, sofortige Umstellung auf CannaXan-THC möglich. Reduktion der Dosis innerhalb von 5 Wochen Therapie möglich.</td>
    </tr>
    <tr class="row_3">
      <td><input type="hidden" value="37.5" name="typumstelung[3][tag_name]">37.5</td>
      <td><input type="hidden" value="1.5" name="typumstelung[3][tag_description]">1.5</td>
      <td class="nocell"><p><input type="text" name="typumstelung[3][morgens_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMorgens(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell morgens_anzahl_sps"><input type="hidden" name="typumstelung[3][morgens_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="selected nocell"><p><input type="text" name="typumstelung[3][mittags_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMittags(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="selected nocell mittags_anzahl_sps"><input type="hidden" name="typumstelung[3][mittags_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="nocell"><p><input type="text" name="typumstelung[3][abends_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpAbends(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell abends_anzahl_sps"><input type="hidden" name="typumstelung[3][abends_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="nocell selected"><p><input type="text" name="typumstelung[3][nachts_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpNachts(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell selected nachts_anzahl_sps"><input type="hidden" name="typumstelung[3][nachts_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="transferfaktor"><input type="hidden" name="typumstelung[3][transferfaktor]" value="0"> <span class="text-field">0</span></td>
      <td class="eintitration"><input type="hidden" name="typumstelung[3][eintitration]" value="0"> <span class="text-field">0</span></td>
      <td>nein, sofortige Umstellung auf CannaXan-THC möglich. Reduktion der Dosis innerhalb von 5 Wochen Therapie möglich.</td>
    </tr>
    <tr class="row_4">
      <td><input type="hidden" value="50" name="typumstelung[4][tag_name]">50</td>
      <td><input type="hidden" value="2" name="typumstelung[4][tag_description]">2</td>
      <td class="nocell"><p><input type="text" name="typumstelung[4][morgens_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMorgens(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell morgens_anzahl_sps"><input type="hidden" name="typumstelung[4][morgens_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="selected nocell"><p><input type="text" name="typumstelung[4][mittags_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMittags(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="selected nocell mittags_anzahl_sps"><input type="hidden" name="typumstelung[4][mittags_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="nocell"><p><input type="text" name="typumstelung[4][abends_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpAbends(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell abends_anzahl_sps"><input type="hidden" name="typumstelung[4][abends_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="nocell selected"><p><input type="text" name="typumstelung[4][nachts_anzahl_durchgange]" value="0" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpNachts(this)" data-key="<?php echo '1'; ?>"></p></td>
      <td class="nocell selected nachts_anzahl_sps"><input type="hidden" name="typumstelung[4][nachts_anzahl_sps]" value="0"><p class="text-field">0</p></td>
      <td class="transferfaktor"><input type="hidden" name="typumstelung[4][transferfaktor]" value="0"> <span class="text-field">0</span></td>
      <td class="eintitration"><input type="hidden" name="typumstelung[4][eintitration]" value="0"> <span class="text-field">0</span></td>
      <td>nein, sofortige Umstellung auf CannaXan-THC möglich. Reduktion der Dosis innerhalb von 5 Wochen Therapie möglich.</td>
    </tr>
    <?php } ?>
  </table>
</div>