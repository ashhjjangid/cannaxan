<section class="same-section same-blog">
   <div class="container-fluid">
      <div class="same-heading ">
         <h2 class="text-left">Therapieplan: Folgeverordnung</h2>
      </div>
      <form method="post" action="<?php echo base_url('therapie/add_folgeverordnung?in='.$insured_number); ?>" class="ajax_form">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Hauptindikation:</label>
                     </div>
                     <div class="col-sm-9">
                        <?php if ($icd_code_zuordnung) { ?>
                        <?php foreach ($icd_code_zuordnung as $key => $value) { 
                           $selected = '';
                           
                           if ($is_exist_patient && ($is_exist_patient->hauptindikation == $value->id)) {
                              $selected = 'selected'; ?>
                        <input type="text" name="hauptindikation" class="form-control" placeholder="Hauptindikation" value="<?php echo $value->indikation; ?>" readonly>
                        <?php } }
                           ?>
                        <?php  } ?>
                        <!-- <div class="select-box">
                           <select class="form-control" name="hauptindikation" id="hauptindikation">
                           
                           <?php if ($icd_code_zuordnung) { ?>
                           
                              <?php foreach ($icd_code_zuordnung as $key => $value) { 
                              $selected = '';
                              
                              if ($folgeverordnung && ($folgeverordnung->hauptindikation == $value->id)) {
                                 $selected = 'selected';
                              }
                              ?>
                                 <option value="<?php echo $value->id; ?>" data-icd="<?php echo $value->icd_10; ?>" <?php echo $selected; ?>><?php echo $value->indikation; ?></option>
                              <?php } ?>
                           <?php } ?>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                           </div> -->
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-12">
               <div class="row">
                  <div class="col-md-6">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-6">
                                    <label>Rezepturarzneimittel:</label>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="select-box">
                                       <input type="text" name="rezepturarzneimittel" class="form-control" placeholder="" value="CannaXan-THC" readonly="">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-6">
                                    <label>Wirkstoff:</label>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="select-box">
                                       <input type="text" name="wirkstoff" class="form-control" value="CannaXan-701-1.1" readonly="">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-6">
                                    <label>THC Konzentration <span>[mg/mL]</span></label>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="select-box">
                                       <input type="text" name="thc_konzentration_mg_ml" class="form-control text-center" placeholder="" value="<?php echo str_replace('.', ',', round(($folgeverordnung)?$folgeverordnung->thc_konzentration_mg_ml:$zuordnung_sps->konzentration_thc_mg_ml,1)); ?>" readonly="">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="row">
                        <!-- <div class="col-md-12">
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-7">
                                    <label>Tagesdosis THC [<span>mg</span>]<br> <span>(Optimum von Wirkung/<br> Nebenwirkung aus Titrationsphase, oder Anpassung)</span></label>
                                 </div>
                                 <div class="col-sm-5">
                                    <input type="text" name="tagesdosis_thc_mg" placeholder="Tagesdosis THC [mg]" value="<?php echo str_replace('.', ',', round(($folgeverordnung)?$folgeverordnung->tagesdosis_thc_mg:'',1)); ?>" onkeyup="getCalculation()" class="form-control text-center red-input tagesdosis_thc_mg">
                                 </div>
                              </div>
                           </div>
                           </div> -->
                        <div class="col-md-12">
                           <div class="form-group dark-gray">
                              <div class="row">
                                 <div class="col-sm-7">
                                    <label>Gleichbleibende Tagesdosis CBD festlegen<br> <span>(Optimum von Wirkung/ Nebenwirkung aus Titrationsphase, oder Anpassung)</span></label>
                                 </div>
                                 <div class="col-sm-5">
                                    <div class="checkbox-custom">
                                       <input type="checkbox" id="me" name="gleichbleibende_tagesdosis" <?php echo ($folgeverordnung && $folgeverordnung->gleichbleibende_tagesdosis == "Yes")?'checked':''; ?>>
                                       <label for="me"><span class="cheak"></span></label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-8">
                              <label>Therapiedauer für<br> gleichbleibende Tagesdosis [Tage]</label>
                           </div>
                           <div class="col-sm-4">
                              <div class="select-box">
                                 <input type="text" name="therapiedauer" class="form-control text-center red-input therapiedauer" placeholder="0" value="<?php echo ($folgeverordnung)?$folgeverordnung->therapiedauer:''; ?>" onkeyup="getCalculation()">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-7">
                              <label>Tagesdosis CBD <span>[mg]</span></label>
                           </div>
                           <div class="col-sm-5">
                              <div class="select-box">
                                 <input type="text" name="tagesdosis_cbd" class="form-control text-center" placeholder="9,2" value="<?php echo ($folgeverordnung)?$folgeverordnung->tagesdosis_cbd:''; ?>">
                              </div>
                           </div>
                        </div>
                     </div>
                     </div> -->
               </div>
            </div>
         </div>
         <p class="comman-heading"><i class="text-red">Eingabewerte in rot.</i> <strong>ALLE SCHWARZEN UND <span>BLAUEN</span> WERTE WERDEN BERECHNET</strong></p>
         <div class="text-rezept mt-3">
            <div class="row">
               <div class="col-md-3">
                  <div class="text-rezept-event gray-bx mb-3">
                     <div class="heading-right">
                        <span>Angaben für BtM Rezept:</span>
                     </div>
                     <ul>
                        <li>CannaXan 701-1.1; <span>2,2 <i class="font-style-none">mg/mL</i> THC</span></li>
                        <li>insgesamt:<span><span class="verordnete_anzahl">0</span> x 20 <i class="font-style-none">mL</i> <span class="insgesamt-value"></span> <i class="font-style-none">mg</i> THC</span></li>
                     </ul>
                     <span class="dosierung">
                        <!-- Dosierung siehe Therapieplan -->Dosierung gemäß schriftlicher Anweisung
                     </span>
                  </div>
                  <div class="text-rezept-event low-span-width gray-bx">
                     <div class="heading-right">
                        <span>Zusammenfassung:</span>
                     </div>
                     <ul>
                        <li>
                           <p>Summe THC <i class="font-style-none">[mg]</i> <input type="hidden" name="total_summe" class="total_summe_input" value=""></p>
                           <span class="total_summe">0,0</span>
                        </li>
                        <li>
                           <p>Summe CannaXan 701-1.1 <i class="font-style-none">[mL]</i> <input type="hidden" name="total_cannaxan_thc" class="total_cannaxan_thc_input" value=""></p>
                           <span class="total_cannaxan_thc">0,0</span>
                        </li>
                        <li>
                           <p>Verordnete Anzahl 20<i class="font-style-none">mL</i> Flaschen CannaXan 701-1.1 <input type="hidden" name="verordnete_anzahl" class="verordnete_anzahl_input" value="0"></p>
                           <span class="verordnete_anzahl">0</span>
                        </li>
                        <li>
                           <p>Rechnerische Restmenge <br> CannaXan 701-1.1 <br> nach 30 Tagen <i class="font-style-none">[mL]</i> <input type="hidden" name="rechnerische_restmenge" class="rechnerische_restmenge_input" value="0"></p>
                           <span class="rechnerische_restmenge">0,0</span>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="col-md-9 table-box-form umstelung-content-section">
                  <div class=" append-table-section">
                     <div class="tabel-1box tabel-3box">
                        <table cellspacing="0" class="table  table-bordered mb-0">
                           <tr>
                              <th colspan="8" class="action-td">
                                 <div class="tabel-1box-header">
                                    <div class="row">
                                       <div class="col morgens sub-box color">
                                          <span class="border-dark"></span>
                                          <div class="sub-heading">Morgens</div>
                                          <div class="row">
                                             <div class="col">
                                                <div class="text-box anz">
                                                   <p>Anzahl Durchgänge</p>
                                                </div>
                                             </div>
                                             <div class="col">
                                                <div class="text-box sps">
                                                   <p>Anzahl SPS</p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col morgens sub-box color">
                                          <span class="border-dark"></span>
                                          <div class="sub-heading">Mittags</div>
                                          <div class="row">
                                             <div class="col">
                                                <div class="text-box anz">
                                                   <p class="colorp">Anzahl Durchgänge</p>
                                                </div>
                                             </div>
                                             <div class="col">
                                                <div class="text-box sps">
                                                   <p class="colorp">Anzahl SPS</p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col morgens sub-box color">
                                          <span class="border-dark"></span>
                                          <div class="sub-heading">Abends</div>
                                          <div class="row">
                                             <div class="col">
                                                <div class="text-box anz">
                                                   <p>Anzahl Durchgänge</p>
                                                </div>
                                             </div>
                                             <div class="col">
                                                <div class="text-box sps">
                                                   <p>Anzahl SPS</p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col morgens sub-box color">
                                          <span class="border-dark"></span>
                                          <div class="sub-heading">Nachts</div>
                                          <div class="row">
                                             <div class="col">
                                                <div class="text-box anz">
                                                   <p class="colorp">Anzahl Durchgänge</p>
                                                </div>
                                             </div>
                                             <div class="col">
                                                <div class="text-box sps">
                                                   <p class="colorp">Anzahl SPS</p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </th>
                              <th class="border-left-none">START tgl. CannaXan-THC Dosis [mg THC]</th>
                           </tr>
                           <tr class="row_1">
                              <td class="nocell">
                                 <p><input type="text" name="morgens_anzahl_durchgange" value="<?php echo round(($folgeverordnung)?$folgeverordnung->morgens_anzahl_durchgange:'',1); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMorgens(this)" data-key="<?php echo '1'; ?>"></p>
                              </td>
                              <td class="nocell morgens_anzahl_sps">
                                 <input type="hidden" name="morgens_anzahl_sps" value="<?php echo round(($folgeverordnung)?$folgeverordnung->morgens_anzahl_sps:'',1); ?>">
                                 <p class="text-field"><?php echo str_replace('.', ',', round(($folgeverordnung)?$folgeverordnung->morgens_anzahl_sps:'',1)); ?></p>
                              </td>
                              <td class="selected nocell">
                                 <p><input type="text" name="mittags_anzahl_durchgange" value="<?php echo round(($folgeverordnung)?$folgeverordnung->mittags_anzahl_durchgange:'',1); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMittags(this)" data-key="<?php echo '1'; ?>"></p>
                              </td>
                              <td class="selected nocell mittags_anzahl_sps">
                                 <input type="hidden" name="mittags_anzahl_sps" value="<?php echo round(($folgeverordnung)?$folgeverordnung->mittags_anzahl_sps:'',1); ?>">
                                 <p class="text-field"><?php echo str_replace('.', ',', round(($folgeverordnung)?$folgeverordnung->mittags_anzahl_sps:'',1)); ?></p>
                              </td>
                              <td class="nocell">
                                 <p><input type="text" name="abends_anzahl_durchgange" value="<?php echo round(($folgeverordnung)?$folgeverordnung->abends_anzahl_durchgange:'',1); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpAbends(this)" data-key="<?php echo '1'; ?>"></p>
                              </td>
                              <td class="nocell abends_anzahl_sps">
                                 <input type="hidden" name="abends_anzahl_sps" value="<?php echo round(($folgeverordnung)?$folgeverordnung->abends_anzahl_sps:'',1); ?>">
                                 <p class="text-field"><?php echo str_replace('.', ',', round(($folgeverordnung)?$folgeverordnung->abends_anzahl_sps:'',1)); ?></p>
                              </td>
                              <td class="nocell selected">
                                 <p><input type="text" name="nachts_anzahl_durchgange" value="<?php echo round(($folgeverordnung)?$folgeverordnung->nachts_anzahl_durchgange:'',1); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpNachts(this)" data-key="<?php echo '1'; ?>"></p>
                              </td>
                              <td class="nocell selected nachts_anzahl_sps">
                                 <input type="hidden" name="nachts_anzahl_sps" value="<?php echo round(($folgeverordnung)?$folgeverordnung->nachts_anzahl_sps:'',1); ?>">
                                 <p class="text-field"><?php echo str_replace('.', ',', round(($folgeverordnung)?$folgeverordnung->nachts_anzahl_sps:'',1)); ?></p>
                              </td>
                              <td class="transferfaktor"><input type="hidden" name="transferfaktor" class="transferfaktor_value" value="<?php echo round(($folgeverordnung)?$folgeverordnung->transferfaktor:'',1); ?>"> <span class="text-field"><?php echo str_replace('.', ',', round(($folgeverordnung)?$folgeverordnung->transferfaktor:'',1)); ?></span></td>
                           </tr>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
            <!-- <div class="col-md-6">
               <div class="heading-right">
                  <span>Text für BtM Rezept:</span>
               </div>
               <div class="row">
                  <div class="col-md-1"></div>
                  <div class="col-md-11">
                     <div class="text-rezept-event">
                        <ul>
                           <li>CannaXan 701-1.1; 2,2 mg/mL THC</li>
                           <li>insgesamt:<span>7 x 20 mL 308,0 mg THC</span></li>
                        </ul>
                        <span class="dosierung">Dosierung siehe Therapieplan</span>
                     </div>
                  </div>
               </div>
               </div> -->
         </div>
        
         <div class="row mt-3">
            <div class="col-md-12 text-right">
               <button class="btn btn-primary" type="submit"><span>Speichern & weiter</span></button>
            </div>
         </div>
      </form>
   </div>
</section>
<script type="text/javascript">
   getCalculation();
   function getCalculation() {
     var transferfaktor_value = $('.transferfaktor_value').val();
     var therapiedauer = $('.therapiedauer').val();
     
      if (transferfaktor_value.trim() && therapiedauer.trim()) {
   
         var total_summe = parseFloat(transferfaktor_value) * parseInt(therapiedauer);
      
      } else {
         var total_summe = 0;
      }
   
   
     total_summe_thc(total_summe);
   
     // if (transferfaktor_value.trim() && therapiedauer.trim()) {
     //   $.ajax({
     //     url: '<?php echo base_url('therapie/getFolgeverordnungCalculation?in='.$insured_number); ?>',
     //     type: 'post',
     //     data: {transferfaktor_value:transferfaktor_value, therapiedauer:therapiedauer},
     //     dataType: 'json',
     //     success: function (data) {
     //       total_summe_thc(data.total_summe);
     //     }
     //   })
     // } else {
     //   return false;
     // }
   }
   
   function total_summe_thc(total_sum)
   {
     $('.total_summe_input').val(total_sum.toFixed(1));
     var konzentration_thc_mg_ml = "<?php echo $zuordnung_sps->konzentration_thc_mg_ml; ?>";
     var menge_thc_20_ml = "<?php echo $zuordnung_sps->menge_thc_20_ml; ?>";
     var menge_thc_20_ml = parseInt(menge_thc_20_ml);
   
     var summe_CannaXan_THC = (total_sum / konzentration_thc_mg_ml).toFixed(1);
     var anzahl_20mL_Flaschen = (summe_CannaXan_THC / 20).toFixed(1);
     var anzahl_20mL_Flaschen = Math.ceil(anzahl_20mL_Flaschen);
     var insgesamt_value = (anzahl_20mL_Flaschen * menge_thc_20_ml).toFixed(1);
     var cannaXan_THC_nach_30_Tagen = ((anzahl_20mL_Flaschen * 20) - summe_CannaXan_THC).toFixed(1);
   
     $('.total_cannaxan_thc_input').val(summe_CannaXan_THC);
     $('.verordnete_anzahl_input').val(anzahl_20mL_Flaschen);
     $('.rechnerische_restmenge_input').val(cannaXan_THC_nach_30_Tagen);
   
     var new_total_summe = total_sum.toFixed(1);
     var new_summe_CannaXan_THC = summe_CannaXan_THC.replace('.', ',');
     var new_anzahl_20mL_Flaschen = anzahl_20mL_Flaschen;
     var new_insgesamt_value = insgesamt_value.replace('.', ',');
     var new_cannaXan_THC_nach_30_Tagen = cannaXan_THC_nach_30_Tagen.replace('.', ',');
     $('.total_summe').text(new_total_summe.replace('.', ','));
     $('.total_cannaxan_thc').text(new_summe_CannaXan_THC);
     $('.verordnete_anzahl').text(new_anzahl_20mL_Flaschen);
     $('.insgesamt-value').text(new_insgesamt_value);
     $('.rechnerische_restmenge').text(new_cannaXan_THC_nach_30_Tagen);
   }
   
    /*$(function(){
     getDosistyp();
   })
   function getDosistyp()
   {
     $.ajax({
       url: '<?php echo base_url('therapie/getFolgeverordnung?in='.$insured_number); ?>',
       type: 'get',
       dataType: 'json',
       success: function (data) {
         $('.append-table-section').html(data.html);
         total_summe_thc();
       }
     })
   }
   
   function keyUpMorgens($this) 
   {
     var field_value = $($this).val();
     var field_key = $($this).attr('data-key');
     var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
     
     if (field_value.trim()) {
       var thc_morgens_mg = (parseFloat(field_value) * parseFloat(menge_thc_sps_mg)).toFixed(1);
       $($this).closest('.row_'+field_key).find('.thc_morgens_mg > input').val(thc_morgens_mg);
       $($this).closest('.row_'+field_key).find('.thc_morgens_mg > .text-field').text(thc_morgens_mg);
       row_sum(field_key);
     } else {
       // var thc_morgens_mg = (parseFloat(field_value) * parseFloat(menge_thc_sps_mg)).toFixed(1);
       $($this).closest('.row_'+field_key).find('.thc_morgens_mg > input').val(0);
       $($this).closest('.row_'+field_key).find('.thc_morgens_mg > .text-field').text(0);
       row_sum(field_key);
     }
   }
   
   function keyUpMittags($this) 
   {
     var field_value = $($this).val();
     var field_key = $($this).attr('data-key');
     var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
     
     if (field_value.trim()) {
       var thc_mittags_mg = (parseFloat(field_value) * parseFloat(menge_thc_sps_mg)).toFixed(1);
       $($this).closest('.row_'+field_key).find('.thc_mittags_mg > input').val(thc_mittags_mg);
       $($this).closest('.row_'+field_key).find('.thc_mittags_mg > .text-field').text(thc_mittags_mg);
       row_sum(field_key);
     } else {
       // var thc_mittags_mg = (parseFloat(field_value) * parseFloat(menge_thc_sps_mg)).toFixed(1);
       $($this).closest('.row_'+field_key).find('.thc_mittags_mg > input').val(0);
       $($this).closest('.row_'+field_key).find('.thc_mittags_mg > .text-field').text(0);
       row_sum(field_key);
     }
   }
   
   function keyUpAbends($this) 
   {
     var field_value = $($this).val();
     var field_key = $($this).attr('data-key');
     var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
     
     if (field_value.trim()) {
       var thc_abends_mg = (parseFloat(field_value) * parseFloat(menge_thc_sps_mg)).toFixed(1);
       $($this).closest('.row_'+field_key).find('.thc_abends_mg > input').val(thc_abends_mg);
       $($this).closest('.row_'+field_key).find('.thc_abends_mg > .text-field').text(thc_abends_mg);
       row_sum(field_key);
     } else {
       // var thc_abends_mg = (parseFloat(field_value) * parseFloat(menge_thc_sps_mg)).toFixed(1);
       $($this).closest('.row_'+field_key).find('.thc_abends_mg > input').val(0);
       $($this).closest('.row_'+field_key).find('.thc_abends_mg > .text-field').text(0);
       row_sum(field_key);
     }
   }
   
   function keyUpNachts($this) 
   {
     var field_value = $($this).val();
     var field_key = $($this).attr('data-key');
     var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
     
     if (field_value.trim()) {
       var thc_nachts_mg = (parseFloat(field_value) * parseFloat(menge_thc_sps_mg)).toFixed(1);
       $($this).closest('.row_'+field_key).find('.thc_nachts_mg > input').val(thc_nachts_mg);
       $($this).closest('.row_'+field_key).find('.thc_nachts_mg > .text-field').text(thc_nachts_mg);
       row_sum(field_key);
     } else {
       // var thc_nachts_mg = (parseFloat(field_value) * parseFloat(menge_thc_sps_mg)).toFixed(1);
       $($this).closest('.row_'+field_key).find('.thc_nachts_mg > input').val(0);
       $($this).closest('.row_'+field_key).find('.thc_nachts_mg > .text-field').text(0);
       row_sum(field_key);
     }
   }
   
   function row_sum(field_key) 
   {
     var thc_morgens_mg = parseFloat($('.row_'+field_key).find('.thc_morgens_mg > input').val());
     var thc_mittags_mg = parseFloat($('.row_'+field_key).find('.thc_mittags_mg > input').val());
     var thc_abends_mg = parseFloat($('.row_'+field_key).find('.thc_abends_mg > input').val());
     var thc_nachts_mg = parseFloat($('.row_'+field_key).find('.thc_nachts_mg > input').val());
     var total = (thc_morgens_mg + thc_mittags_mg + thc_abends_mg + thc_nachts_mg).toFixed(1);
     $('.row_'+field_key).find('.summe_thc_mg > input').val(total);
     $('.row_'+field_key).find('.summe_thc_mg > .text-field').text(total);
     total_summe_thc();
   }
   
    function total_summe_thc()
    {
       var total_sum = 0;
       var total_length = parseInt($('.summe_thc_mg').length) - 1;
   
       $('.summe_thc_mg').each(function(key, value){
          total_sum = parseFloat(total_sum) + parseFloat($(this).find('input').val());
   
          if (key == total_length) {
            $('.total_summe_input').val(total_sum.toFixed(1));
            $('.total_summe').text(total_sum.toFixed(1));
          }
       });
     
       var konzentration_thc_mg_ml = "<?php echo $zuordnung_sps->konzentration_thc_mg_ml; ?>";
       var Summe_CannaXan_THC = (total_sum / konzentration_thc_mg_ml).toFixed(1);
       var Anzahl_20mL_Flaschen = (Summe_CannaXan_THC / 20).toFixed(1);
       var CannaXan_THC_nach_30_Tagen = ((Anzahl_20mL_Flaschen * 20) - Summe_CannaXan_THC).toFixed(1);
       $('.total_cannaxan_thc_input').val(Summe_CannaXan_THC);
       $('.total_cannaxan_thc').text(Summe_CannaXan_THC)
       $('.verordnete_anzahl_input').val(Anzahl_20mL_Flaschen);
       $('.verordnete_anzahl').text(Anzahl_20mL_Flaschen)
       $('.rechnerische_restmenge_input').val(CannaXan_THC_nach_30_Tagen);
       $('.rechnerische_restmenge').text(CannaXan_THC_nach_30_Tagen)
   }*/
   
   function keyUpMorgens($this) 
    {
     var field_value = $($this).val();
     /*if(field_value.match(/\./g)){      
   
     } 
     else {
       var field_value = field_value.replace(/\./g, ',');
     }*/
     field_value = field_value.replace(',', '.');
     var field_key = $($this).attr('data-key');
     var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
     var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
     
     if (field_value.trim()) {
       var morgens_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
       var morgens_anzahl_sps_text = morgens_anzahl_sps.replace(/\./g, ',');
       $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > .text-field').text(morgens_anzahl_sps_text);
       $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > input').val(morgens_anzahl_sps);
       row_sum(field_key);
     } else {
        $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > .text-field').text(0);
       $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > input').val(0);
       row_sum(field_key);
     }
    }
   
    function keyUpMittags($this) 
    {
     var field_value = $($this).val();
     /*if(field_value.match(/\./g)){      
   
     } 
     else {
       var field_value = field_value.replace(/\./g, ',');
     }*/
     field_value = field_value.replace(',', '.');
     var field_key = $($this).attr('data-key');
     var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
     var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
     
     if (field_value.trim()) {
       var mittags_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
       var mittags_anzahl_sps_text = mittags_anzahl_sps.replace(/\./g, ',');
       $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > .text-field').text(mittags_anzahl_sps_text);
       $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > input').val(mittags_anzahl_sps);
       row_sum(field_key);
     }  else {
        $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > .text-field').text(0);
       $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > input').val(0);
       row_sum(field_key);
     }
    }
   
    function keyUpAbends($this) 
    {
     var field_value = $($this).val();
     /*if(field_value.match(/\./g)){      
   
     } 
     else {
       var field_value = field_value.replace(/\./g, ',');
     }*/
     field_value = field_value.replace(',', '.');
     var field_key = $($this).attr('data-key');
     var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
     var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
     
     if (field_value.trim()) {
       var abends_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
       var abends_anzahl_sps_text = abends_anzahl_sps.replace(/\./g, ',');
       $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > .text-field').text(abends_anzahl_sps_text);
       $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > input').val(abends_anzahl_sps);
      row_sum(field_key);
     }  else {
        $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > .text-field').text(0);
       $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > input').val(0);
       row_sum(field_key);
     }
    }
   
    function keyUpNachts($this) 
    {
     var field_value = $($this).val();
     /*if(field_value.match(/\./g)){      
   
     } 
     else {
       var field_value = field_value.replace(/\./g, ',');
     }*/
     field_value = field_value.replace(',', '.');
     var field_key = $($this).attr('data-key');
     var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
     var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
     
     if (field_value.trim()) {
       var nachts_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
       var nachts_anzahl_sps_text = nachts_anzahl_sps.replace(/\./g, ',');
       $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > .text-field').text(nachts_anzahl_sps_text);
       $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > input').val(nachts_anzahl_sps);
       row_sum(field_key);
     } else {
        $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > .text-field').text(0);
       $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > input').val(0);
       row_sum(field_key);
     }
   }
   
   
    function row_sum(field_key) 
    {
       var morgens_anzahl_sps = parseFloat($('.row_'+field_key).find('.morgens_anzahl_sps > input').val());
       var mittags_anzahl_sps = parseFloat($('.row_'+field_key).find('.mittags_anzahl_sps > input').val());
       var abends_anzahl_sps = parseFloat($('.row_'+field_key).find('.abends_anzahl_sps > input').val());
       var nachts_anzahl_sps = parseFloat($('.row_'+field_key).find('.nachts_anzahl_sps > input').val());
       var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
       var total = ((parseFloat(morgens_anzahl_sps) * parseFloat(menge_thc_sps_mg)) + (parseFloat(mittags_anzahl_sps) * parseFloat(menge_thc_sps_mg)) + (parseFloat(abends_anzahl_sps) * parseFloat(menge_thc_sps_mg)) +(parseFloat(nachts_anzahl_sps) * parseFloat(menge_thc_sps_mg))).toFixed(1);
       var total_text = total.replace(/\./g, ',');
       $('.row_'+field_key).find('.transferfaktor > input').val(total);
       $('.row_'+field_key).find('.transferfaktor > .text-field').text(total_text);
       var first_row = parseFloat($('.row_'+field_key).find('.first-row >input').val());
       getCalculation();
      
    }
</script>