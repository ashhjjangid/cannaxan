
      <section class="banner-section anmeldung">
         <div class="container-fluid">
            <div class="banner-img">
               <img src="<?php echo $this->config->item('front_assets'); ?>images/banner-img-2.png">
            </div>
         </div>
      </section>

      <section class="same-section">
         <div class="container-fluid">
            <div class="row align-items-end">
                  <div class="col-md-12">
                     <div class="input-fill-box">
               			<form method="post" class="ajax_form" action="<?php echo current_url(); ?>" id="register_form">
                        <div class="logo-box text-center">
                           <img src="<?php echo $this->config->item('front_assets'); ?>images/logo.png" alt="logo">
                        </div>
                        <div class="row align-items-center">
                           <div class="col-4">
                              <label>Benutzername:</label>
                           </div>
                           <div class="col-8">
                              <input type="`text" name="email" placeholder="benutzername@arzt.de" class="form-control">
                           </div>
                        </div>
                        <div class="row align-items-center">
                           <div class="col-4">
                              <label>password:</label>
                           </div>
                           <div class="col-8">
                              <input type="password" name="password" placeholder="******" class="form-control bold-form-control">
                           </div>
                        </div>
                        <div class="custom-btn login-btn">
                           <button type="submit" class="btn btn-primary"><span>Anmelden</span></button>
                        </div>
                     </form>
                        <div class="foroget-password-cover">
                           <a href="javascript:void(0)" id="foroget_password" onclick="forgotUserPassword()" class="foroget-password">Passwort vergessen?</a>
                           
                            <div id="foroget_password_box" class="foroget_password_box" style="display: none;">
                              <a href="javascript:void(0)" class="popup-closer"><i class="fas fa-times"></i></a>
                                 <h4>PASSWORT VERGESSEN</h4>
                                 <p>Wenn Sie Ihr Passwort vergessen haben, dann können Sie es hier zurücksetzen: Bitte tragen Sie hier Ihre hinterlegte E-Mailadresse ein und wir schicken Ihnen ein neues Passwort zu.</p>
                                 <div class="foroget-password-input">
                                    <form method="post" class="ajax_form forgot_password_form" id="forgot_password_form" action="<?php echo base_url('doctors/forgetpassword')?>">
                                       <div class="reset-password-email">
                                          <div class="form-group">
                                             <label>Benutzername: </label>
                                             <input type="text" id="email" name="email" class="form-control">
                                          </div>
                                       </div>
                                       <div class="form-group mb-0">
                                          <button type="submit" class="btn btn-primary"><span>Speichern &amp; weiter</span></button>
                                       </div>
                                    </form>
                                    <form method="post" class="ajax_form reset_password_form" id="reset_password_form">
                                    
                                       <div class="reset-password-fields">
                                          <div class="form-group">
                                             <label>Neues Passwort</label>
                                             <input type="password" id="new_password" name="new_password" class="form-control">
                                          </div>
                                          <div class="form-group">
                                             <label>Passwort Bestätigung</label>
                                             <input type="password" id="confirm_password" name="confirm_password" class="form-control">
                                          </div>
                                       </div>
                                       <div class="form-group mb-0">
                                          <button type="submit" class="btn btn-primary"><span>Speichern &amp; weiter</span></button>
                                       </div>
                                    </form>
                                 </div>
                              </div>  
                          </div>
                           <!-- <div class="foroget-password-cover">
                              <div id="reset_password_box" class="foroget_password_box" style="display: none;">
                                 <h4>Passwort</h4>
                                 <p>Bitte geben Sie hier Ihr neues Passwort ein. Zur Gewärung einer besseren Datensicherheit muss es aus minimum acht Zeichen bestehen. Das Passwort muss min. ein Sonderzeichen sowie eine Zahl enthalten.</p>
                                 <div class="foroget-password-input">
                                    <form method="post" class="ajax_form reset_password_form" id="reset_password_form">
                                    
                                       <div class="reset-password-fields">
                                          <div class="form-group">
                                             <label>Neues Passwort</label>
                                             <input type="password" id="new_password" name="new_password" class="form-control">
                                          </div>
                                          <div class="form-group">
                                             <label>Passwort Bestätigung</label>
                                             <input type="password" id="confirm_password" name="confirm_password" class="form-control">
                                          </div>
                                       </div>
                                       <div class="form-group mb-0">
                                          <button type="submit" class="btn btn-info"><span>Speichern &amp; weiter</span></button>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                        </div> -->
                     </div>
                  </div>
            </div>
         </div>

      </section>


<script type="text/javascript">

   $(document).ready(function() {
      $('.popup-closer').on('click', function(){
         $(this).parent('.foroget_password_box').hide()
      });


      // $('.foroget_password_box').css('display','block');
      /*$('.foroget-password').on('click', function() {
         $('.foroget_password_box').css('display','none');
         $('.reset-password-fields').css('display','none');
      });*/

     /* if ($('.foroget_password_box').css('display','none')) {

         $('#speichern').click(function() {
            var reset_form = $('#reset_password_form');
            var email = $('#email').val();
            var surl = '<?php echo base_url('doctors/forgetpassword'); ?>';
            
            if (email != '') {
               $.ajax({
                  url: surl,
                  type: "POST",
                  dataType: "json",
                  data: {"email" :email},
                  success: function(data) {
                     
                  }
               });
            }

            var new_password = $('#new_password').val();
            var confirm_password = $('#confirm_password').val();
            $.ajax({
               url: '<?php echo base_url('doctors/resetpassword'); ?>',
               type: "POST",
               dataType: "json",
               data: {"new_password" :new_password, "confirm_password" :confirm_password},
               success: function(data) {
                  return true;
               }
            });
         });
      }*/
      resetUserPassword();
   });
   
   function resetUserPassword() {
      
      var key = "<?php echo $key; ?>";

      if (key != '') {
        // alert('here');
        $('.foroget_password_box').css("display", "block");
      	$('.reset_password_form').css("display", "block");
      	$('.forgot_password_form').css("display", "none");
      }
   }

   function forgotUserPassword() {
      
      var key = "<?php echo $key; ?>";
      if (key != '') {
         
	        $('.foroget_password_box').css("display", "block");
	      	$('.reset_password_form').css("display", "block");
	      	$('.forgot_password_form').css("display", "none");
       } else {

	      $('.foroget_password').css("display", "block");
	      $('.reset_password_form').css("display", "none");
	      $('.forgot_password_form').css("display", "block");
        // alert('here');
       }
   }
</script>
   