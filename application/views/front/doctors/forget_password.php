
      <section class="banner-section anmeldung">
         <div class="container-fluid">
            <div class="banner-img">
               <img src="<?php echo $this->config->item('front_assets'); ?>images/banner-img-2.png">
            </div>
         </div>
      </section>

      <section class="same-section">
         <div class="container-fluid">
            <div class="row align-items-end">
                  <div class="col-md-12">
                  	<div class="same-heading small-heading-p text-center mb-5">
                        <h2 >Passwort</h2>
                        <p>
                        	Bitte geben Sie hier Ihr neues Passwort ein. Zur Gewärung einer besseren Datensicherheit muss es aus minimum acht Zeichen bestehen. Das Passwort muss min. ein 
                           Sonderzeichen sowie eine Zahl enthalten.
                        </p>
                     </div>
                     <div class="input-fill-box">
                        <form method="post" class="ajax_form" action="<?php echo current_url(); ?>" id="register_form">
                        <div class="logo-box text-center">
                           <img src="<?php echo $this->config->item('front_assets'); ?>images/logo.png" alt="logo">
                        </div>
                        <div class="row align-items-center">
                           <div class="col-4">
                              <label>Neues Passwort:
                                 <!-- <div class="dropmenu-box">
                                    <div class="drop-heading">
                                       <h2>Passwort</h2>
                                    </div>
                                    <p>Bitte geben Sie hier Ihr neues Passwort ein.
                                       Zur Gewärung einer besseren Datensicherheit muss
                                       es aus minimum acht Zeichen bestehen.
                                       Das Passwort muss min. ein Sonderzeichen sowie
                                       eine Zahl enthalten.</p>
                                    
                                 </div> -->
                              </label>
                           </div>
                           <div class="col-8">
                              <input type="password" name="set_new_password" placeholder="******" class="form-control bold-form-control" autocomplete="off">
                           </div>
                        </div>
                        <div class="row align-items-center">
                           <div class="col-4">
                              <label>Passwort Bestätigung:</label>
                           </div>
                           <div class="col-8">
                              <input type="password" name="set_confirm_password" placeholder="******" class="form-control bold-form-control" autocomplete="off">
                           </div>
                        </div>
                        <div class="custom-btn login-btn">
                           <?php if ($this->session->userdata('user_id')) { ?>
                              
                              <button type="submit" class="btn btn-primary"><span>Speichern & Anmelden</span></button>
                           <?php } else { ?>
                              <button type="submit" class="btn btn-primary"><span>Passwort festlegen</span></button>
                              
                           <?php } ?>
                        </div>
                     </form>
<!--                         <div class="foroget-password-cover">
                           <a href="javascript:void(0)" id="foroget_password" onclick="forgotUserPassword()" class="foroget-password">Passwort vergessen?</a>
                           
                            <div id="foroget_password_box" class="foroget_password_box" style="display: none;">
                                 <h4>Passwort</h4>
                                 <p>Bitte geben Sie hier Ihr neues Passwort ein. Zur Gewärung einer besseren Datensicherheit muss es aus minimum acht Zeichen bestehen. Das Passwort muss min. ein Sonderzeichen sowie eine Zahl enthalten.</p>
                                 <div class="foroget-password-input">
                                    <form method="post" class="ajax_form forgot_password_form" id="forgot_password_form" action="<?php echo base_url('doctors/forgetpassword')?>">
                                       <div class="reset-password-email">
                                          <div class="form-group">
                                             <label>Email</label>
                                             <input type="text" id="email" name="email" class="form-control">
                                          </div>
                                       </div>
                                       <div class="form-group mb-0">
                                          <button type="submit" class="btn btn-info"><span>Speichern &amp; weiter</span></button>
                                       </div>
                                    </form>
                                    <form method="post" class="ajax_form reset_password_form" id="reset_password_form">
                                    
                                       <div class="reset-password-fields">
                                          <div class="form-group">
                                             <label>Neues Passwort</label>
                                             <input type="password" id="new_password" name="new_password" class="form-control">
                                          </div>
                                          <div class="form-group">
                                             <label>Passwort Bestätigung</label>
                                             <input type="password" id="confirm_password" name="confirm_password" class="form-control">
                                          </div>
                                       </div>
                                       <div class="form-group mb-0">
                                          <button type="submit" class="btn btn-info"><span>Speichern &amp; weiter</span></button>
                                       </div>
                                    </form>
                                 </div>
                              </div>  
                          </div> -->
                     </div>
                  </div>
            </div>
         </div>

      </section>


<!-- <script type="text/javascript">

   $(document).ready(function() {

      setUserPassword();
   });
   
   function setUserPassword() {
      
      var key = "<?php echo $key; ?>";

      if (key != '') {
        // alert('here');
        $('.foroget_password_box').css("display", "block");
         $('.reset_password_form').css("display", "block");
         $('.forgot_password_form').css("display", "none");
      }
   }

   function forgotUserPassword() {
      
      var key = "<?php echo $key; ?>";
      if (key != '') {
         
           $('.foroget_password_box').css("display", "block");
            $('.reset_password_form').css("display", "block");
            $('.forgot_password_form').css("display", "none");
       } else {

         $('.foroget_password').css("display", "block");
         $('.reset_password_form').css("display", "none");
         $('.forgot_password_form').css("display", "block");
       }
   }
</script> -->
<script>
   function showpasswortpopup() {
    $('.dropmenu-box').css('display', 'block');
    
    $(".dropmenu-box").mouseleave(function(){
      $(".dropmenu-box").css('display', 'none');
    });
  }
</script>
   
   <style type="text/css">
      .login-btn{ left: 108.2%; }
      @media screen and (max-width: 1700px)
      {
         .login-btn{left: 104.2%!important;}
      }
         
   </style>