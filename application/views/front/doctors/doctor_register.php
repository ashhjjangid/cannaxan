      <section class="same-section">
         <div class="container-fluid">
            <form method="post" class="ajax_form" action="<?php echo current_url(); ?>" id="register_form" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">Titel</label>
                           </div>
                           <div class="col-sm-5">
                            <input type="text" name="title" class="form-control red-input" id="title" placeholder="titel" value="<?php echo ($title)?$title:''; ?>" readonly>
                              <!-- <div class="dropdown select-box">
                                <button id="dLabel" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ($title)?$title:''; ?>
                                  
                                  <span><i class="fas fa-chevron-down"></i></span>
                                </button>
                                <ul class="dropdown-menu" id="title" aria-labelledby="dLabel">
                                  <li value="Dr.">Dr.</li>
                                  <li value="Dr. med">Dr. med</li>
                                  <li value="keine">keine</li>
                                  <li value="PD Dr. med.">PD Dr. med.</li>
                                  <li value="Prof. Dr. med.">Prof. Dr. med.</li>
                                </ul>
                              </div>
                              <input type="hidden" name="title" class="title-field" value="<?php echo ($title)?$title:''; ?>"> -->
                              <!-- <div class="select-box">
                                 <select name="title" class="form-control">
                                    <option value="Dr.">Dr.</option>
                                    <option value="Dr. med">Dr. med</option>
                                    <option value="keine">keine</option>
                                    <option value="PD Dr. med.">PD Dr. med.</option>
                                    <option value="Prof. Dr. med.">Prof. Dr. med.</option>
                                 </select>
                                 <span><i class="fas fa-chevron-down"></i></span>
                              </div> -->
                           </div>
                        </div>
                     </div>
                  </div>
              </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">Anrede</label>
                           </div>
                           <div class="col-sm-5">
                            <input type="text" name="salutation" class="form-control red-input" id="salutation" placeholder="Andre" value="<?php echo ($anedre)?$anedre:''; ?>" readonly>
                              <!-- <div class="dropdown select-box" id="salutation-box">
                                <button id="salutation" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  
                                  <span><i class="fas fa-chevron-down"></i></span>
                                </button>
                                <ul class="dropdown-menu" id="salutation" aria-labelledby="salutation">
                                  <li value="Herr">Herr</li>
                                  <li value="Frau">Frau</li>
                                </ul>
                              </div>
                              <input type="hidden" name="salutation" class="salutation-field" value=""> -->
                              <!-- <div class="select-box">
                                 <select name="salutation" class="form-control">
                                    <option value="Herr">Herr</option>
                                    <option value="Frau">Frau</option>
                                 </select>
                                 <span><i class="fas fa-chevron-down"></i></span>
                              </div> -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 offset-md-2 align-self-center">
                              <label class="mb-0">Name Arztpraxis</label>
                           </div>
                           <div class="col-sm-6">
                              <input type="text" name="name_doctor_practice" class="form-control red-input" placeholder="Arztpraxis">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">Nachname</label>
                           </div>
                           <div class="col-sm-5">
                              <input type="text" name="name" class="form-control red-input" placeholder="Nachname" id="last_name" value="<?php echo ($last_name)?$last_name:''; ?>" readonly>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 offset-md-2 align-self-center">
                              <label class="mb-0">Fachrichtung:</label>
                           </div>
                           <div class="col-sm-6">
                              <div class="dropdown select-box" id="specialization-box">
                                <button id="specialization" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  
                                  <span><i class="fas fa-chevron-down"></i></span>
                                </button>
                                <ul class="dropdown-menu" id="specialization" aria-labelledby="specialization">
                                  <li value="Facharzt für Allgemeinmedizin" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Allgemeinmedizin">Facharzt für Allgemeinmedizin</li>
                                  <li value="Facharzt für Anästhesiologie" data-toggle="tooltip" data-placement="bottom">Facharzt für Anästhesiologie</li>
                                  <li value="Facharzt für Anatomie" data-toggle="tooltip" data-placement="bottom">Facharzt für Anatomie</li>
                                  <li value="Facharzt für Arbeitsmedizin" data-toggle="tooltip" data-placement="bottom">Facharzt für Arbeitsmedizin</li>
                                  <li value="Facharzt für Allgemeine Chirurgie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Allgemeine Chirurgie">Facharzt für Allgemeine Chirurgie</li>
                                  <li value="Facharzt für Gefäßchirurgie" data-toggle="tooltip" data-placement="bottom">Facharzt für Gefäßchirurgie</li>
                                  <li value="Facharzt für Herzchirurgie" data-toggle="tooltip" data-placement="bottom">Facharzt für Herzchirurgie</li>
                                  <li value="Facharzt für Facharzt für Orthopädie und Unfallchirurgie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Facharzt für Orthopädie und Unfallchirurgie">Facharzt für Facharzt für Orthopädie und Unfallchirurgie</li>
                                  <li value="Facharzt für Thoraxchirurgie" data-toggle="tooltip" data-placement="bottom">Facharzt für Thoraxchirurgie</li>
                                  <li value="Facharzt für Visceralchirurgie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Visceralchirurgie">Facharzt für Visceralchirurgie</li>
                                  <li value="Facharzt für Frauenheilkunde und Geburtshilfe" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Frauenheilkunde und Geburtshilfe">Facharzt für Frauenheilkunde und Geburtshilfe</li>
                                  <li value="Facharzt für Hals-Nasen-Ohrenheilkunde" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Hals-Nasen-Ohrenheilkunde">Facharzt für Hals-Nasen-Ohrenheilkunde</li>
                                  <li value="Facharzt für Innere und Allgemeinmedizin (Hausarzt)" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Innere und Allgemeinmedizin (Hausarzt)">Facharzt für Innere und Allgemeinmedizin (Hausarzt)</li>
                                  <li value="Facharzt für Innere Medizin und Angiologie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Innere Medizin und Angiologie">Facharzt für Innere Medizin und Angiologie</li>
                                  <li value="Facharzt für Innere Medizin und Endokrinologie und Diabetologie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Innere Medizin und Endokrinologie und Diabetologie">Facharzt für Innere Medizin und Endokrinologie und Diabetologie</li>
                                  <li value="Facharzt für Innere Medizin und Gastroenterologie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Innere Medizin und Gastroenterologie">Facharzt für Innere Medizin und Gastroenterologie</li>
                                  <li value="Facharzt für Innere Medizin und Hämatologie und Onkologie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Innere Medizin und Hämatologie und Onkologie">Facharzt für Innere Medizin und Hämatologie und Onkologie</li>
                                  <li value="Facharzt für Innere Medizin und Kardiologie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Innere Medizin und Kardiologie">Facharzt für Innere Medizin und Kardiologie</li>
                                  <li value="Facharzt für Innere Medizin und Nephrologie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Innere Medizin und Nephrologie">Facharzt für Innere Medizin und Nephrologie</li>
                                  <li value="Facharzt für Innere Medizin und Pneumologie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Innere Medizin und Pneumologie">Facharzt für Innere Medizin und Pneumologie</li>
                                  <li value="Facharzt für Innere Medizin und Rheumatologie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Innere Medizin und Rheumatologie">Facharzt für Innere Medizin und Rheumatologie</li>
                                 <li value="Facharzt für Mikrobiologie, Virologie und Infektionsepidemiologie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Mikrobiologie, Virologie und Infektionsepidemiologie">Facharzt für Mikrobiologie, Virologie und Infektionsepidemiologie</li>
                                 <li value="Facharzt für Neurochirurgie" data-toggle="tooltip" data-placement="bottom">Facharzt für Neurochirurgie</li>
                                 <li value="Facharzt für Neurologie" data-toggle="tooltip" data-placement="bottom">Facharzt für Neurologie</li>
                                 <li value="Facharzt für Nuklearmedizin" data-toggle="tooltip" data-placement="bottom">Facharzt für Nuklearmedizin</li>
                                 <li value="Facharzt für Physikalische und Rehabilitative Medizin" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Physikalische und Rehabilitative Medizin">Facharzt für Physikalische und Rehabilitative Medizin</li>
                                 <li value="Facharzt für Psychiatrie und Psychotherapie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Psychiatrie und Psychotherapie">Facharzt für Psychiatrie und Psychotherapie</li>
                                 <li value="Facharzt für Psychosomatische Medizin und Psychotherapie" data-toggle="tooltip" data-placement="bottom" title="Facharzt für Psychosomatische Medizin und Psychotherapie">Facharzt für Psychosomatische Medizin und Psychotherapie</li>

                                </ul>
                              </div>
                              <input type="hidden" name="specialization" class="specialization-field" value="">
                              
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">VorName</label>
                           </div>
                           <div class="col-sm-5">
                              <input type="text" name="first_name" class="form-control red-input" id="first_name" placeholder="Vorname" value="<?php echo ($first_name)?$first_name:''; ?>" readonly>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 offset-md-2 align-self-center">
                              <label class="mb-0">LANR:</label>
                           </div>
                           <div class="col-sm-6">
                              <input type="text" name="lanr" class="form-control red-input" placeholder="123456789">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="other-form-box">
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                           <div class="row align-items-end">
                              <div class="col-sm-4 align-self-center">
                                 <label class="mb-0">Stra<span>&szlig;</span>e</label>
                              </div>
                              <div class="col-sm-5">
                                 <input type="text" name="road" class="form-control red-input" id="strabe" placeholder="Musterstraße" value="<?php echo ($strabe)?$strabe:''; ?>" readonly>
                              </div>
                              <div class="col-md-3">
                                 <label class="mb-2">Hausnr.</label>
                                 <input type="text" name="house_no" class="form-control red-input" id="house_no" placeholder="Hausnr" value="<?php echo ($house_no)?$house_no:''; ?>" readonly>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group mt-4">
                           <div class="row">
                              <div class="col-sm-4 offset-md-2 align-self-center">
                                 <label class="mb-0">E-Mail</label>
                              </div>
                              <div class="col-sm-6">
                                 <input type="text" name="email" class="form-control red-input" placeholder="E-Mail" value="<?php echo ($email)?$email:''; ?>" readonly>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="col-md-6">
                        <div class="form-group">
                           <div class="row">
                              <div class="col-sm-4 align-self-center">
                                 <label class="mb-0">Videosprechstunde</label>
                                 <p>Bitte fügen Sie hier den zu Ihrer Videosprechstunde oder onleinTerminkalender ein.</p>
                              </div>
                              <div class="col-sm-5">
                                 <input type="text" name="videosprechstunde" class="form-control" placeholder=" Link zur Videosprchstunde">
                              </div>
                           </div>
                        </div>
                     </div> -->
                     <div class="col-md-6">
                        <div class="form-group">
                           <div class="row">
                              <div class="col-sm-4 align-self-center">
                                 <label class="mb-0">PLZ</label>
                              </div>
                              <div class="col-sm-5">
                                 <input type="text" name="plzl" class="form-control red-input" id="plzl" placeholder="PLZ" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo ($plzl)?$plzl:''; ?>" readonly>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <div class="row">
                              <div class="col-sm-4 offset-md-2 align-self-center">
                                 <label class="mb-0">www.</label>
                              </div>
                              <div class="col-sm-6">
                                 <input type="text" name="website" class="form-control gray-input" placeholder="www.">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <div class="row">
                              <div class="col-sm-4 align-self-center">
                                 <label class="mb-0">Ort</label>
                              </div>
                              <div class="col-sm-5">
                                 <input type="text" name="ort" class="form-control red-input" id="ort" placeholder="Ort" value="<?php echo ($ort)?$ort:''; ?>" readonly>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                              <div class="col-sm-4 align-self-center">
                                 <label class="mb-0">Tel. nr.</label>
                              </div>
                              <div class="col-sm-5">
                                 <input type="text" name="phonenumber" class="form-control red-input" placeholder="+49 ....">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                              <div class="col-sm-4 align-self-center">
                                 <label class="mb-0">Fax</label>
                              </div>
                              <div class="col-sm-5">
                                 <input type="text" name="fax" class="form-control gray-input" placeholder="+49 ....">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                              <div class="col-sm-4 align-self-center">
                                 <label class="mb-0 small-on-laptop">Videosprechstunde</label>
                                 <div class="extra-button-line">
                                    <p>Bitte fügen Sie hier den Link zu Ihrer Videosprechstunde oder Online-Terminkalender ein.</p>
                                 </div>
                              </div>
                              <div class="col-sm-5">
                                 <input type="text" name="videosprechstunde" class="form-control gray-input" placeholder="Link zur Videosprechstunde">
                              </div>
                           </div>
                        </div>
                        
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <div class="row">
                              <div class="col-md-6 offset-md-6">
                                 <div class="row vereinbarung">
                                  <div class="cheak_open-modal">
                                     <!-- <input type="checkbox" class="data_storage" name="data_storage" id="me"> -->
                                     <label for="me" onmouseenter="vereinbarungPopupShow()"><span class="cheak"></span>Vereinbarung Gemäß Art. 26 DSGVO</label>
                                  </div>

                                  </div>
                                  <input type="hidden" name="vertarg" id="vertarg" value="<?php echo ($vertarg)?$vertarg:''; ?>">
                                  <input type="hidden" name="verstanden_vertarg" id="verstanden_vertarg" value="<?php echo ($verstanden_vertarg)?$verstanden_vertarg:''; ?>">
                                  <input type="hidden" name="akzeptieren_vertarg" id="akzeptieren_vertarg" value="<?php echo ($akzeptieren_vertarg)?$akzeptieren_vertarg:''; ?>">
                                     <div id="foroget_password_box" class="register-page-popup long-content-popup close-button-popup new-big-popup" style="display: none;">
                                       <a href="javascript:void(0)" class="popup-closer"><i class="fas fa-times"></i></a>
                                       <div class="scroller">
                                          <h4>VEREINBARUNG GEMÄß ART. 26 DSGVO</h4>
                                          <div class="popup-header">
                                            <p>zwischen</p>
                                            <strong>CannaXan GmbH</strong>
                                            <p>Birkerfeld 12</p>
                                            <p>83627 Warngau</p>
                                            <p>─ nachfolgend bezeichnet als <strong>Verantwortlicher</strong> zu 1 ─</p>
                                            <p>und</p>
                                            <p><strong><?php echo (($title && $title == 'keine')?'':$title).' '.$first_name.' '.$last_name; ?></strong></p>
                                            <p><?php echo $strabe.' '.$house_no; ?></p>
                                            <p><?php echo $plzl.' '.$ort; ?></p>
                                            <p>─ nachfolgend bezeichnet als Verantwortlicher zu 2 ─</p>
                                            <p class="mb-3">─ beide gemeinsam als die <strong>Parteien</strong> bezeichnet ─</p>
                                            <p><strong>Präambel</strong></p>
                                          </div>

                                          <div class="popup-body-txt">
                                            <p>Die Parteien nutzen gemeinsam die CannaXan-App. Im Rahmen ihrer Zusammenarbeit verarbeiten die Parteien personenbezogene Daten und Gesundheitsdaten. Für einen Teil dieser Daten haben die Parteien die Zwecke und Mittel der Verarbeitung (im Folgenden: die <strong>Datenverarbeitung </strong>) gemeinsam festgelegt. Die Parteien sind im Hinblick auf diese Datenverarbeitung mithin gemeinsam Verantwortliche i.S.d. Art. 26 DSGVO i.V.m. Art. 4 Nr. 7 DSGVO. </p>
                                            <p>Mit dieser Vereinbarung möchten die Parteien ihre Rechte und Pflichten im Hinblick auf die gemeinsame Verarbeitung der personenbezogenen Daten näher konkretisieren.
                                            </p>
                                            <ol class="popup-ol-txt">
                                               <li>
                                                  <strong><strong>1.</strong> Gegenstand dieser Vereinbarung</strong>
                                                  <ol>
                                                     <li>
                                                        <p><span>1.1</span> <strong>Beschreibung der Datenverarbeitung.</strong> Gegenstand, Zweck und Umfang der Datenverarbeitung sind in <strong> Anlage 1</strong> näher beschrieben.
                                                           Insbesondere enthält <strong> Anlage 1</strong> eine Beschreibung der Art der verarbeiteten Daten und der Kategorien der betroffenen Personen.
                                                        </p>
                                                     </li>
                                                  </ol>
                                               </li>
                                               <li>
                                                  <strong><strong>2.</strong> Phasen der Datenverarbeitung</strong>
                                                  <ol>
                                                     <li>
                                                        <p><span>2.1</span> <strong>Einteilung in Prozessabschnitte.</strong> Die Parteien haben in <strong>Anlage 2 </strong> die Prozessabschnitte definiert, für die eine gemeinsame Verantwortlichkeit besteht, und festgelegt, welche Partei für die Durchführung des jeweiligen Prozessabschnittes zuständig sein soll.</p>
                                                     </li>
                                                     <li>
                                                        <p><span>2.2</span> <strong>Eigenständige Verantwortlichkeit. </strong> Für Prozessabschnitte, die nicht in <strong> Anlage 2 </strong> benannt sind, bleibt jede Partei eigenständiger
                                                           Verantwortlicher i.S.d. Art. 4 Nr. 7 DSGVO.
                                                        </p>
                                                     </li>
                                                     <li>
                                                        <p><span>2.3</span> <strong>Verfahrensverzeichnis.</strong> Beide Parteien werden die Datenverarbeitung in ihr jeweiliges Verfahrensverzeichnis gem. Art. 30 Abs. 1 S. 1 DSGVO aufnehmen und dort als Verfahren in gemeinsamer Verantwortung vermerken.</p>
                                                     </li>
                                                  </ol>
                                               </li>
                                               <li>
                                                  <strong><strong>3.</strong> Informationspflichten nach Art. 13, 14 DSGVO</strong>
                                                  <ol>
                                                     <li>
                                                        <p><span>3.1</span> Zuständigkeit. Für die Erfüllung der Informationspflichten nach Art. 13, 14 DSGVO ist die in <strong> Anlage 3</strong> genannte Partei zuständig.</p>
                                                     </li>
                                                     <li>
                                                        <p><span>3.2</span> Inhaltliche Vorgaben. Betroffenen Personen sind die erforderlichen Informationen in präziser, transparenter, verständlicher und
                                                           leicht zugänglicher Form in einer klaren und einfachen Sprache zu übermitteln.
                                                        </p>
                                                     </li>
                                                     <li>
                                                        <p><span>3.3</span> Zurverfügungstellung dieser Vereinbarung. Die nach Ziffer 3.1 für die Erfüllung der Informationspflichten zuständige Partei verpflichtet sich, den wesentlichen Inhalt dieser Vereinbarung den betroffenen Personen gemäß Art. 26 Abs. 2 Satz 2 DSGVO zur Verfügung zu stellen. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>3.4</span> Abstimmung. Soweit möglich stimmen die Parteien den Inhalt und die Formulierung etwaiger Informationen an betroffene Personen nach dieser Ziffer 3 miteinander ab.</p>
                                                     </li>
                                                  </ol>
                                               <li>
                                                  <strong><strong>4.</strong> Betroffenenanfragen nach Art. 15 ff. DSGVO</strong>
                                                  <ol>
                                                     <li>
                                                        <p><span>4.1</span> <strong> Zuständigkeit.</strong> Für die Bearbeitung von Anfragen betroffener Personen nach den Art. 15 ff. DSGVO ist die in <strong>Anlage 3</strong> genannte Partei zuständig. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>4.2</span> <strong>Keine Beschränkung.</strong> Die Parteien sind sich bewusst, dass betroffene Personen die ihnen nach den Art. 15 ff. DSGVO zustehenden Rechte bei und gegenüber jeder Partei geltend machen können. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>4.3</span> <strong> Weiterleitung von Ersuchen.</strong> Wendet sich eine betroffene Person in Wahrnehmung ihrer Rechte nach den Art. 15 ff. DSGVO an eine Partei, die gemäß Ziffer 4.1 nicht für die Erfüllung des Ersuchens zuständig ist, so leitet die unzuständige Partei das Ersuchen unverzüglich an die zuständige Partei weiter. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>4.4</span> <strong> Gegenseitige Unterstützung.</strong> Die Parteien stellen sich bei Bedarf die zur Bearbeitung und Beantwortung von Betroffenenanfragen erforderlichen Informationen gegenseitig zu Verfügung. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>4.5</span> <strong>Löschung von Betroffenendaten.</strong> Die Löschung von Daten des Betroffenen durch eine Partei darf nur nach schriftlicher Information der anderen Partei erfolgen. Diese kann der Löschung widersprechen, sofern sie ein berechtigtes Interesse am Fortbestand der Daten hat; ein solches kann sich insbesondere aus einer gesetzlichen Aufbewahrungspflicht ergeben. </p>
                                                     </li>
                                                  </ol>
                                               </li>
                                               </li>
                                               <li>
                                                  <strong><strong>5.</strong> Meldepflichten gemäß Art. 33, 34 DSGVO</strong>
                                                  <ol>
                                                     <li><p><span>5.1</span> <strong>Zuständigkeit.</strong> Für Meldungen von Datenschutzverstößen nach Art. 33, 34 DSGVO ist die in <strong> Anlage 3 </strong> genannte Partei zuständig.</p></li>
                                                     <li>
                                                        <p><span>5.2</span> <strong>Informationspflicht.</strong> Stellt eine Partei in Bezug auf die unter dieser Vereinbarung verarbeiteten personenbezogenen Daten eine Datenschutzverletzung i.S.d. Art. 4 Nr. 12 DSGVO fest, informiert sie die andere Partei unverzüglich hierüber und stellt der anderen Partei die zur Prüfung der Datenschutzverletzung erforderlichen Informationen in angemessener Weise zur Verfügung. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>5.3</span> <strong>Inhaltliche Abstimmung. </strong> Soweit möglich und zumutbar stimmen die Parteien den Inhalt etwaigen Meldungen nach Art. 33, 34 DSGVO miteinander ab. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>5.4</span> <strong>Sonstige Fehler. </strong> Ziffer 5.2 gilt entsprechend, sofern eine Partei Unregelmäßigkeiten oder Fehler bei der Datenverarbeitung und/oder eine Verletzung der Bestimmungen dieser Vereinbarung oder anwendbaren Datenschutzrechts (insbesondere der DSGVO) feststellt. </p>
                                                     </li>
                                                  </ol>
                                               </li>
                                               <li>
                                                  <strong><strong>6.</strong> Zusammenarbeit mit Aufsichtsbehörden</strong>
                                                  <ol>
                                                     <li>
                                                        <p><span>6.1</span> <strong> Anfragen. </strong> Wendet sich eine Datenschutzaufsichtsbehörde an eine der Parteien im Zusammenhang mit der Datenverarbeitung unter dieser Vereinbarung, so informiert diese Partei die andere Partei unverzüglich hierüber.</p> 
                                                      </li>
                                                     <li>
                                                        <p><span>6.2</span> <strong> Abstimmung.</strong> Soweit möglich stimmen sich die Parteien vor Beantwortung von Anfragen von Datenschutzaufsichtsbehörden, die Zusammenhang mit der Datenverarbeitung unter dieser Vereinbarung stehen, miteinander ab. Dies gilt auch für die Einlegung von Rechtsbehelfen und Rechtsmitteln gegen etwaige aufsichtsbehördliche Maßnahmen, die im Zusammenhang mit der Datenverarbeitung unter dieser Vereinbarung stehen. </p>
                                                     </li>
                                                  </ol>
                                               </li>
                                               <li>
                                                  <strong><strong>7.</strong> Haftung; Freistellung</strong>
                                                  <ol>
                                                     <li>
                                                        <p><span>7.1</span> <strong>Haftung gegenüber Betroffenen. </strong> Die Parteien haften gegenüber betroffenen Personen nach den gesetzlichen Vorschriften </p>
                                                     </li>
                                                     <li>
                                                        <p><span>7.2</span> <strong>Anteiliger Ausgleich im Innenverhältnis.</strong> Soweit eine Partei ihre Pflichten aus dieser Vereinbarung verletzt, stellt sie die andere Partei von aus dem Pflichtverstoß entstandenen Schäden in jenem Umfang frei, wie dies ihrem in dieser Vereinbarung vereinbarten Verantwortungsbeitrag für den Verstoß entspricht. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>7.3</span> <strong>Umfang der Freistellung. </strong> Die Freistellungspflicht gemäß Ziffer 7.2 umfasst etwaige Ansprüche von und Verbindlichkeiten gegenüber Dritten (einschließlich etwaiger gerichtlicher und außergerichtlicher Rechtsverteidigungskosten). </p>
                                                     </li>
                                                     <li>
                                                        <p><span>7.4</span> <strong>Gegenseitige Unterstützung. </strong> Die Parteien unterstützen sich gegenseitig in angemessener Weise im Zusammenhang mit der Verteidigung, dem Vergleich und der Erfüllung etwaiger Ansprüche und Verbindlichkeiten nach Ziffer 7.3. </p>
                                                     </li>
                                                  </ol>
                                               </li>
                                               <li>
                                                  <strong><strong>8.</strong> Laufzeit und Kündigung</strong>
                                                     <ol>
                                                        <li>
                                                           <p><span>8.1</span> <strong>Inkrafttreten und Laufzeit. </strong> Diese Vereinbarung tritt am Tag ihrer Unterzeichnung in Kraft und läuft <strong><i>[auf unbestimmte Zeit].</i></strong> </p>
                                                        </li>
                                                        <li>
                                                           <p><span>8.2</span> <strong>Kündigung aus wichtigem Grund. </strong> Jede Partei hat das Recht, diese Vereinbarung aus wichtigem Grund zu kündigen. Ein wichtiger Grund liegt insbesondere vor, wenn eine Partei einen Verstoß gegen eine wesentliche Pflicht aus dieser Vereinbarung begeht und im Falle eines heilbaren Verstoßes diesen nicht innerhalb von  <strong> 30 Tagen </strong> nach Benachrichtigung durch die andere Partei heilt .</p>
                                                        </li>
                                                     </ol>
                                               </li>
                                               <li>
                                                  <strong><strong>9.</strong>  Schlussbestimmungen</strong>
                                                  <ol>
                                                     <li>
                                                        <p><span>9.1</span> <strong> Vorrang dieser Vereinbarung.</strong> Im Falle eines Widerspruchs zwischen dieser Vereinbarung und sonstigen Vereinbarungen der Parteien, geht diese Vereinbarung vor, soweit der Widerspruch die Rechte und Pflichten der Parteien als gemeinsam Verantwortliche betrifft. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>9.2</span> <strong>Salvatorische Klausel.</strong> Sollte eine Bestimmung dieser Vereinbarung unwirksam oder nicht durchsetzbar sein oder werden, so bleiben die übrigen Bestimmungen dieser Vereinbarung hiervon unberührt. Anstelle der unwirksamen oder nicht durchsetzbaren Bestimmung werden die Parteien eine solche vereinbaren, die die Anforderungen des Art. 26 DSGVO hinreichend berücksichtigt und dem Zweck der unwirksamen oder nicht durchsetzbaren Bestimmung am nächsten kommt. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>9.3</span> <strong> Änderungen. </strong> Änderungen und Ergänzungen dieser Vereinbarung bedürfen der Schriftform. Dies gilt auch für den Verzicht auf dieses Formerfordernis. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>9.4</span> <strong>Anwendbares Recht. </strong> Diese Vereinbarung unterliegt deutschem Recht (einschließlich der DSGVO) unter Ausschluss des internationalen Privatrechts. </p>
                                                     </li>
                                                     <li>
                                                        <p><span>9.5</span> <strong>Gerichtsstand. </strong> Ausschließlicher Gerichtsstand für alle Streitigkeiten zwischen den Parteien aus und im Zusammenhang mit dieser Vereinbarung und ihrer Durchführung ist München. </p>
                                                     </li>
                                                  </ol>
                                               </li>
                                            </ol>
                                            <div class="row">
                                              <div class="col-md-6">
                                                <p><strong>Warngau, <?php echo date('d.m.Y'); ?></strong></p>
                                                <p>Zustimmung durch den Geschäftsführer <br> Dr. Werner Brand</p>
                                                <p>CannaXan GmbH</p>
                                              </div>
                                              <div class="col-md-6">
                                                <p><strong><?php echo $ort; ?>, <?php echo date('d.m.Y'); ?></strong></p>
                                                <p>Hiermit stimme ich dem Vertrag zu</p>
                                                <p><?php echo (($title && $title == 'keine')?'':$title).' '.$first_name.' '.$last_name; ?></p>
                                              </div>
                                            </div>
                                            <div class="text-center mt-4">
                                              <strong style="text-decoration: underline; font-size: 18px; letter-spacing: 1px; color: #fff;">ANLAGE 1</strong>
                                              <p>Beschreibung der Datenverarbeitung in gemeinsamer Verantwortlichkeit</p>
                                            </div>
                                            <ol style="list-style-type: upper-roman;" class="upper-roman popup-ol-txt">
                                             <li>
                                                <strong>Gegenstand der Datenverarbeitung</strong>
                                                <ol>
                                                   <li>
                                                      <p>Gegenstand der Datenverarbeitung unter dieser Vereinbarung ist.</p>
                                                      <ol>
                                                         <li>
                                                            <p>Die gemeinsame Verarbeitung von personenbezogenen Daten und Gesundheitsdaten in 
                                                            einer App durch die CannaXan GmbH. </p>
                                                         </li>
                                                      </ol>
                                                   </li>
                                                </ol>
                                             </li>
                                             <li>
                                                <strong>Zweck der Datenverarbeitung</strong>
                                                <ol style="list-style-type: none">
                                                   <li>
                                                      <p>Die Datenverarbeitung erfolgt zu folgenden Zwecken:</p>  
                                                      <ol>
                                                         <li>
                                                            <p>Zur Verwendung der CannaXan-App, damit insbesondere aus den Daten ein 
                                                            Titrationsplan erstellt werden kann, zur Erstellung eines BTM-Rezeptes und zur Dokumentation des Therapieverlaufs, die insbesondere der übersichtlichen
                                                            Archivierung der vom Patienten erfassten Daten als Grundlage für die Therapieentscheidungen des 
                                                            Arztes dienen können.</p>
                                                         </li>
                                                      </ol>
                                                   </li>
                                                </ol>
                                             </li>
                                             <li>
                                                <strong>Art der personenbezogenen Daten</strong>
                                                <ol>
                                                   <li>
                                                      <p>Folgende Datenarten sind regelmäßig Gegenstand der Datenverarbeitung:</p>
                                                      <ol>
                                                         <li>
                                                            <p>Personenbezogenen Daten verarbeiten:</p>
                                                            <ol style="list-style-type: disc;">
                                                               <li style="list-style-type: disc;">
                                                                  <p>Vorname, Name, Geschlecht, Größe, Gewicht</p>
                                                               </li>
                                                            </ol>
                                                         </li>
                                                         <li>
                                                            <p>Gesundheitsdaten:</p>
                                                            <ol>
                                                               <li style="list-style-type: disc;">
                                                                  <p>Indikation, Schmerzwert, Schlafqualitätsfragebogen, 
                                                                  Lebensqualitätsfragebogen, Arzneimitteldosis, Begleitmedikamente, Nebenwirkungen, Laborparameter, 
                                                                  Arztfragebogen zur Erstellung des Kostenübernameantrags bei der Krankenkasse</p>
                                                               </li>
                                                            </ol>
                                                         </li>
                                                      </ol>
                                                   </li>
                                                </ol>
                                             </li>
                                             <li>
                                                <strong>Kategorien der betroffenen Personen</strong>
                                                <ol>
                                                   <li>
                                                      <p>Folgende Personenkreise sind regelmäßig von der Datenverarbeitung betroffen:</p>
                                                   </li>
                                                   <li>
                                                      <p>den Verantwortlichen, und den dazugehörigen verbundenen Unternehmen, dem behandelnden Arzt und Apotheken</p>
                                                   </li>
                                                </ol>
                                             </li>
                                            </ol>
                                            <div class="text-center mt-4">
                                              <strong style="text-decoration: underline; font-size: 18px; letter-spacing: 1px; color: #fff;">ANLAGE 2</strong>
                                              <p>Übersicht Prozessabschnitte</p>
                                            </div>
                                            <table cellpadding="12" cellspacing="0" width="100%" border="0" style=" border-color:#10069F;border-collapse: collapse;" class="table_striped">
                                             <tr>
                                                <td><strong style="font-size: 18px; letter-spacing: 1px; color: #10069F;">Beschreibung des Prozessabschnitts</strong></td>
                                                <td><strong style="font-size: 18px; letter-spacing: 1px; color: #10069F;">In gemeinsamer Verantwortlichkeit?</strong></td>
                                                <td colspan="2" style=" padding: 0;">
                                                   <table width="100%" class="bg-transparent" cellpadding="12" cellspacing="0" width="100%" >
                                                      <tr>
                                                         <td colspan="2" align="center" style="     border-top: none; border-left: none; border-right: none; border-bottom-color:#10069F; border-bottom-width: 1px; border-bottom-style: solid;"><strong style="font-size: 18px; letter-spacing: 1px; color: #10069F;">Zuständig?</strong></td>
                                                      </tr>
                                                      <tr>
                                                         <td style="border-left: none; border-bottom: none; "><strong style=" font-size: 18px; letter-spacing: 1px; color: #10069F;">Verantwortlicher zu 1</strong></td>
                                                         <td style="    border-left: none; border-bottom: none; border-right: none;"><strong style="font-size: 18px; letter-spacing: 1px; color: #10069F; ">Verantwortlicher zu 2</strong></td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td width="25%"><strong style="color: #5e6690; font-size: 15px;">Erhebung der Daten</strong></td>
                                                <td width="25%" align="center"><strong style="color: #000; font-size: 15px;">nein </strong></td>
                                                <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;"></strong></td>
                                                <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                             </tr>
                                             <tr>
                                                <td width="25%"><strong style="color: #5e6690; font-size: 15px;">Speicherung der Daten </strong></td>
                                                <td width="25%" align="center"><strong style="color: #000; font-size: 15px;">nein </strong></td>
                                                <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;"></strong></td>
                                             </tr>
                                             <tr>
                                                <td width="25%"><strong style="color: #5e6690; font-size: 15px;">Änderung der Daten</strong></td>
                                                <td width="25%" align="center"><strong style="color: #000; font-size: 15px;">nein </strong></td>
                                                <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;"></strong></td>
                                                <td  width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                             </tr>
                                             <tr>
                                                <td width="25%"><strong style="color: #5e6690; font-size: 15px;">Löschung der Daten </strong></td>
                                                <td width="25%" align="center"><strong style="color: #000; font-size: 15px;">nein </strong></td>
                                                <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;"></strong></td>
                                             </tr>
                                          </table>

                                            <div class="text-center mt-4">
                                              <strong style="text-decoration: underline; font-size: 18px; letter-spacing: 1px; color: #fff;">ANLAGE 3</strong>
                                              <p>Allokation der DSGVO-Pflichten</p>
                                            </div>

                                                  <table cellpadding="12" cellspacing="0" width="100%" border="0" style=" border-color:#10069F;border-collapse: collapse;" class="table_striped">
                                                   <tr>
                                                      <td width="25%"><strong style="font-size: 18px; letter-spacing: 1px; color: #000">Pflichten aus der DSGVO</strong></td>
                                                      <td width="25%"><strong style="font-size: 18px; letter-spacing: 1px; color: #000">Einschlägige Regelung</strong></td>
                                                      <td width="25%"><strong style="font-size: 18px; letter-spacing: 1px; color: #000">Verantwortlicher zu 1</strong></td>
                                                      <td width="25%"><strong style="font-size: 18px; letter-spacing: 1px; color: #000">Verantwortlicher zu 2</strong></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%"><strong style="font-size: 18px; letter-spacing: 1px; color: #000">I. Informationspflichten</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Erfüllung der Informationspflicht bei Erhebung personenbezogener Daten</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 13</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Erfüllung der Informationspflicht, wenn Daten nicht bei Betroffenen erhoben wurden </td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 14</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%"><strong style="font-size: 18px; letter-spacing: 1px; color: #000">II. Betroffenenanfragen</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Bearbeitung von Auskunftsverlangen</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 15</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Bearbeitung von Berichtigungsanfragen</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 16</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Bearbeitung von Löschbegehren</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 17</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Bearbeitung von Begehren auf Beschränkung der Verarbeitung</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 18</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Erfüllung von Mitteilungspflichten im Zusammenhang mit der Berichtigung, Löschung oder Einschränkung der Ver arbeitung</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 19</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Abwicklung von Herausgabeverlangen</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 20</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Bearbeitung von Widersprüchen</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 21</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%"><strong style="font-size: 18px; letter-spacing: 1px; color: #000">III. Meldepflichten</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Meldung von Datenpannen an die Aufsichtsbehörde</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 33</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Meldung von Datenpannen an die betroffene Person</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 34</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%"><strong style="font-size: 18px; letter-spacing: 1px; color: #000">IV. Sonstige Pflichten</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Festlegung, Dokumentation, Überprüfung und Aktualisierung der techn.-org. Maßnahmen</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 24 i.V.m.Art 32</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Einschaltung von Auftragsverarbeitern bzw. Unterauftragsverarbeitern und deren Überprüfung</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 28</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;">Ggf. Datenschutzfolgenabschätzung und Konsultation einer Aufsichtsbehörde</td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;" align="center">Art. 35, 36</td>
                                                      <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
                                                      <td width="25%" style="font-size: 18px; letter-spacing: 1px; color: #000;"></td>
                                                   </tr>
                                                </table>
                                          </div>
                                       </div>
                                    </div>
                                  <button type="button" onclick="createpdf()" class="btn btn-primary mb-3 w-100"><span>Download Pdf</span></button>
                                  <button type="submit" class="btn btn-primary w-100"><span>REGISTRIERUNG</span></button>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                              <div class="col-sm-4 offset-md-2 align-self-center">
                                 <label class="mb-0">Unterschrift*</label>
                                 <div class="extra-button-line">
                                    <p>*Hier können Sie Ihre Unterschrift, als JPEG-Datei, für das Rezept hochladen.</p>
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="choose-file-bx">
                                  <input type="file" id="unterschrift" name="signature" class="form-control" placeholder="">
                                  <label for="unterschrift"><div>Datei hochladen</div> <span class="fileName"></span> </label>
                                </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </section>

      <script type="text/javascript">
         function dataStoreCheck() {

            if ($('#foroget_password_box').css('display', 'none')) {
               $('#foroget_password_box').css('display', 'block');
            } else {
               $('#foroget_password_box').css('display', 'none');
            }
         }

         function vereinbarungPopupShow() {
          $('.register-page-popup').show();

          $('register-page-popup').mouseleave(function() {
            $('.register-page-popup').hide();

          })
         }

         $(document).ready(function() {

          /*close popup with button*/
            $('.popup-closer').on('click', function(){
               $(this).parent('.register-page-popup').hide()
            }); 

            /*$('.vereinbarung').on('click', function(){
              $('#foroget_password_box').css('display', 'block');
            });*/

            $('#title li').on('click', function() {
              var getValue = $(this).text();
              //alert(getValue);
              $('#dLabel').text(getValue);
              $('.title-field').val(getValue);
            });

            $('#salutation li').click(function() {
              var getValue = $(this).text();
              $('#salutation').text(getValue);
              $('.salutation-field').val(getValue);
            });

            $('#specialization li').click(function() {
              var getValue = $(this).text();
              $('#specialization').text(getValue);
              $('.specialization-field').val(getValue);
            });
            $(document).on('change', "#unterschrift",function(){
              if ($(this).val()) {

                  var filename = $(this).val().split("\\");
               
                  filename = filename[filename.length-1];

                  $('.fileName').text(filename);
              }
            });
         });

         function createpdf() {
          var first_name = $('#first_name').val();
          var last_name = $('#last_name').val();
          var strabe = $('#strabe').val();
          var house_no = $('#house_no').val();
          var plzl = $('#plzl').val();
          var title = $('#title').val();
          var ort = $('#ort').val();
          var vertarg = $('#vertarg').val();
          var verstanden_vertarg = $('#verstanden_vertarg').val();
          var akzeptieren_vertarg = $('#akzeptieren_vertarg').val();

          $.ajax({
            url: '<?php echo base_url('doctors/download_contract') ?>',
            type:'post',
            dataType: 'json',
            data:{first_name:first_name, last_name:last_name, strabe:strabe, house_no:house_no, plzl:plzl, title:title, ort:ort, vertarg:vertarg, verstanden_vertarg:verstanden_vertarg, akzeptieren_vertarg:akzeptieren_vertarg},
            success: function (data) {
              forceDownload(data.file_url, data.file_name);
            }
          })
         }

         function forceDownload(href, file_name) {
            var anchor = document.createElement('a');
            anchor.href = href;
            anchor.target = '_blank';
            anchor.download = file_name;
            anchor.click();
          }
      </script>
<style type="text/css">
  .upper-roman li ol{ margin-left: 25px; }
   .upper-roman>li{ list-style-type: upper-roman;     color: #fff;}
   ol.upper-roman.popup-ol-txt {
    padding-left: 15px;
}

 .table_striped{ border:2px solid #fff; border-collapse: collapse; }
    .table_striped th{ border:2px solid #fff; border-collapse: collapse; color: #fff!important  }
    .table_striped td{ border:2px solid #fff;border-collapse: collapse; color: #fff!important }
    .table_striped td table{ border:none!important; }
    .table_striped td strong{color: #fff!important}
    .table_striped td span{ color: #fff!important }
</style>