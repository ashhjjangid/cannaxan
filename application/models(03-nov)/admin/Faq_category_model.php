<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**Team_management_model
 * 
 */
class Faq_category_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_faq_categories';
	}

	public function add_new_category($data)
	{
		$data['add_date'] = getDefaultToGMTDate(time());
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_all_categories()
	{
		$this->db->select($this->table.'.*');
		$query = $this->db->get($this->table);	
		return $query->result();
	}	

	public function get_member_for_update($id)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('id =', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function update_faq_category($id, $data)
	{
		$data['update_date'] = getDefaultToGMTDate(time());
		$this->db->select($this->table.'.*');
		$this->db->where('id =', $id);
		$this->db->update($this->table, $data);

		if ($this->db->affected_rows()) {
			return true;
		}else {
			return false;
		}
	}

	public function view_faq_category($id)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('id =', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function change_status_by_id($id, $status)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('id =', $id);
		$this->db->set('status', $status);
		$this->db->update($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function delete_category($id)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('id =', $id);
		$this->db->delete($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
}