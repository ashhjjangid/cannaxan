<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Static_page_mobile_model class.
 * @extends CI_Model
 */

class Static_page_mobile_model extends CI_Model {

   /**
    * __construct function.
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_static_page_mobile';
    }

    /**
    * get_all_static_pages function.
    * @access public
    * @return static page records
    */

    function get_all_static_pages() {
        $this->db->select($this->table.'.*');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    /**
    * get_static_page_by_id function.
    * @access public
    * @param mixed $id
    * @return row of static page
    */

    function get_static_page_by_id($id) {
        $this->db->where('id',$id);
        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    /**
    * set_static_page_by_id function.
    * @access public
    * @param mixed $id
    * @param mixed $data as array
    * @return true or false
    */

    function set_static_page_by_id($id,$data) {
        $data['edited_time'] = getDefaultToGMTDate(time());
        
        $this->db->where('id',$id);
        $result = $this->db->update($this->table,$data);
        return $this->db->affected_rows();
    }

    /**
    * add_static_page function.
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    function add_static_page($data) {
        $data['is_deleteable'] = 'No';
        $data['add_date'] = getDefaultToGMTDate(time());

        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

    /**
    * change_status_by_id function.
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

    function change_status_by_id($id,$status) {
        //print_r($status);
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table);
        
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

}
