<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Subcategory_model class.
 * 
 * @extends CI_Controller
 */

class Subbrand_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_subbrand';
        $this->table_brand = 'tbl_brand';
            }



   /**
    * getCrew function.
    * 
    * @access public
    * @return records of all Crews
    */

    public function getsubcate($searchData) {

        $this->db->select($this->table.'.*');
        

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.category_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.subcategory_name', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }

        if (isset($searchData) && $searchData['limit']) {
            $this->db->limit($searchData['limit'], $searchData['search_index']);
        }

        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }
  public function get_brand() {
        $this->db->select($this->table.'.*');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }
  
   /**
    * setsubbrandById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of crew type of user
    */

    public function getsubbrandById($id) {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id',$id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

 
    
   /**
    * getsubcategory function.
    * 
    * @access public
    * @param mixed $category_id
    * @return result of subcategory
    */

    public function getsubbrand()
    {  

        $this->db->select($this->table.'.*');
        $this->db->select($this->table_brand.'.brand_name');
        $this->db->join($this->table_brand, $this->table_brand.'.id = '.$this->table.'.brand_id');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }
    
  
   /**
    * deletesubbrandById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function deletesubbrandById($id) 
    {
       $this->db->where('id',$id); 
       $this->db->delete($this->table); 
    }


   /**
    * addsubbrand function.
    * 
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    public function addsubbrand($data) {

        $data['add_date'] = getDefaultToGMTDate(time());
        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

   

   /**
    * setsubbrandById function.
    * 
    * @access public
    * @param mixed $data as array
    * @param mixed $id
    * @return true or false
    */

    public function setsubbrandById($id,$data) {
       $data['update_date'] = getDefaultToGMTDate(time());       
        $this->db->where('id',$id);
        $this->db->update($this->table,$data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
         function get_data() {
        $this->db->select($this->table.'.*');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }
    
    function data() {
        $this->db->select($this->table.'.*');
        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }
   /**
    * changeStatusById function.
    * 
    * @access public
    * @param mixed $status
    * @param mixed $id
    * @return true or false
    */

     public function changeStatusById($id, $status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
   
}