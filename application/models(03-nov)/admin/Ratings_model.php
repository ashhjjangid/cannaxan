<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Faq_model class.
 * 
 * @extends CI_Controller
 */

class Ratings_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();        
        $this->reviews = 'tbl_reviews';
        $this->user = 'tbl_user';
        $this->scooter = 'tbl_scooter';

    }

    /**
    * adding new faq function.
    * 
    * @access public
    * @return void
   */

    public function addrating($data)
    {
        $data['add_date'] = getDefaultToGMTDate(time());
        $this->db->insert($this->reviews, $data);
        return $this->db->insert_id();
    }

    /**
    * get_data function.
    * @access public
    * @return all rows in the reviews
   */

    public function get_data() {
        $this->db->select($this->reviews.'.*');
        $query = $this->db->get($this->reviews);
        $result = $query->result();
        return $result;
    }

    /**
    *  get_rating_for_update function.
    *  @access public
    *  @return void
   */

    public function get_rating_for_update($id)
    {
        $this->db->select($this->reviews.'.*');
        $this->db->where('rating_id', $id);
        $query = $this->db->get($this->reviews);
        return $query->row();
    }

    /**
    * set_updated_rating function.
    * @access public
    * @return void
   */

    public function set_updated_rating($id, $data)
    {
        $data['update_date'] = getDefaultToGMTDate(time());
        $this->db->select($this->reviews.'.*');
        $this->db->where('rating_id', $id);
        $this->db->update($this->reviews, $data);
        if ($this->db->affected_rows()) 
        {
            return true;
        } else {
            return false;
        }
    }
    /**
    * change_status_by_id function.
    * @access public
    * @return void
   */

    public function change_status_by_id($id,$status)
    {
        $this->db->select($this->reviews.'.*');
        $this->db->where($this->reviews.'.rating_id', $id);
        $this->db->set($this->reviews.'.status', $status);
        $this->db->update($this->reviews);
        if ($this->db->affected_rows()) 
        {
            return true;
        }
        else 
        {
            return false; 
        }
    }

    /**
    * delete_rating function.
    * @access public
    * @return void
   */

    public function delete_rating($id)
    {
        $this->db->select($this->reviews);
        $this->db->where('rating_id', $id);
        $this->db->delete($this->reviews);
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    public function get_list_of_reviews()
    {
        $this->db->select($this->reviews.'.*');
        $this->db->select($this->scooter.'.id, maker_name, name');
        $this->db->select($this->user.'.id, first_name, last_name');        
        $this->db->join($this->scooter, $this->scooter.'.id='. $this->reviews.'.scooter_id');
        $this->db->join($this->user, $this->user.'.id='. $this->reviews.'.user_id');
        $query = $this->db->get($this->reviews);
        //pr($query); die;
        return $query->result();
    }

    public function view_single_review($id)
    {
        $this->db->select($this->reviews.'.*');
        $this->db->select($this->scooter.'.id, maker_name, name');
        $this->db->select($this->user.'.id, first_name, last_name');      
        $this->db->where($this->reviews.'.rating_id', $id);
        $this->db->join($this->user, $this->user.'.id='. $this->reviews.'.user_id');
        $this->db->join($this->scooter, $this->scooter.'.id='. $this->reviews.'.scooter_id');
        $query = $this->db->get($this->reviews);

        return $query->row();
    }

}
   