<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Faq_model class.
 * 
 * @extends CI_Controller
 */

class Faq_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_faq';
        $this->tbl = 'tbl_faq_categories';

    }

    /**
    * adding new faq function.
    * 
    * @access public
    * @return void
   */

    public function add_new_faq($data)
    {
        $data['add_date'] = getDefaultToGMTDate(time());
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
    * get_data function.
    * @access public
    * @return all rows in the table
   */

    public function get_data() {
        $this->db->select($this->table.'.*');
        $this->db->select($this->tbl.'.category_name');
        $this->db->join($this->tbl, $this->tbl.'.id = '.$this->table.'.category_id');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    /**
    *  get_category function.
    *  @access public
    *  @return all rows in the table
    */

    public function get_category() {
        $this->db->select($this->tbl.'.*');
        $query = $this->db->get($this->tbl);
        $result = $query->result();
        return $result;
    }

    /**
    *  get_faq_for_update function.
    *  @access public
    *  @return void
   */

    public function get_faq_for_update($id)
    {
        $this->db->select($this->table.'.*');
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    /**
    * set_updated_faq function.
    * @access public
    * @return void
   */

    public function set_updated_faq($id, $data)
    {
        $data['update_date'] = getDefaultToGMTDate(time());
        $this->db->select($this->table.'.*');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows()) 
        {
            return true;
        } else {
            return false;
        }
    }

    /**
    * delete_faq function.
    * @access public
    * @return void
   */

    public function delete_faq($id)
    {
        $this->db->select($this->table);
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    /**
    *view_faq function.
    * @access public
    * @return void
   */

    public function view_faq($id)
    {
       $this->db->select($this->table.'.*');
       $this->db->select($this->tbl.'.category_name');
       $this->db->where($this->table.'.id=', $id);
       $this->db->join($this->tbl, $this->tbl.'.id = '.$this->table.'.category_id');
       $query = $this->db->get($this->table);
       $result = $query->row();
       return $result;
    }

    /**
    * change_status_by_id function.
    * @access public
    * @return void
   */

    public function change_status_by_id($id, $status)
    {
        $this->db->select($this->table.'.*');
        $this->db->where('id', $id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) 
        {
            return true;
        }else
        {
            return false;
        }
    }

}
   