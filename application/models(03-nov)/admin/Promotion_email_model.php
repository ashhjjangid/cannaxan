<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Send_email_model extends CI_Model {
	
	/**
	* __construct function.
	* 
	* @access public
	* @return void
	*/
	function __construct() {
		parent::__construct();
		$this->table = 'tbl_promotion_email';
	}

   /**
    * get_all_data function.
    * 
    * @access public
    * @param mixed $id
    * @return records of all news letter
    */

	function get_all_data() {
		$id = $this->session->userdata('admin_id');
		$query = $this->db->get($this->table);
		$result = $query->result();
		return $result;
	}

   /**
    * insert_email function.
    * 
    * @access public
    * @param mixed $inesrt_arr as aaray
    * @return insert id
    */

	function insert_email($inesrt_arr) {
		$this->db->insert($this->table, $inesrt_arr);
		return $this->db->insert_id();
	}

   /**
    * get_email_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @return row of newsletter templete
    */

	function get_email_by_id($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

   /**
    * update_email function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $data
    * @return true or false
    */

	function update_email($id, $data) {
		$this->db->where('id',$id);
		$this->db->update($this->table,$data);
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

   /**
    * delete_email function.
    * 
    * @access public
    * @param mixed $id
    * @return true
    */

	function delete_email($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return true;
	}

   /**
    * change_status_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

	function change_status_by_id($id, $status) {
		$this->db->where('id',$id);
		$this->db->set('status',$status);
		$this->db->update($this->table);

		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}	
	}

   /**
    * get_email_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @return row of news letter
    */	

	function get_email_by_id($id) {
		$this->db->select('id,subject,message,status');
		$this->db->where('id',$id);
		$query = $this->db->get($this->table);
		return $query->row();
	}	
}