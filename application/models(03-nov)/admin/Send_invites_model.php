<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Send_invites_model extends CI_Model {
	
	/**
	* __construct function.
	* 
	* @access public
	* @return void
	*/
	function __construct() {
		parent::__construct();
		$this->table = 'tbl_send_invites';
		$this->tbl ='tbl_user';
	}

   /**
    * get_all_data function.
    * 
    * @access public
    * @param mixed $id
    * @return records of all data
    */

	function get_all_data() {
		$this->db->select($this->table.'.*');
        $this->db->select($this->tbl.'.first_name');
        $this->db->join($this->tbl, $this->tbl.'.id = '.$this->table.'.user_id');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
	}

 	
}