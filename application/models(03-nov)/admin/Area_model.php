<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Area_model class.
 * 
 * @extends CI_Controller
 */

class Area_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->area_escooter = 'tbl_area_escooter';
        $this->area_marker ='tbl_area_marker';
    }


    public function get_area_data() {
        $this->db->select($this->area_escooter.'.*');
        $query = $this->db->get($this->area_escooter);
        $result = $query->result();
        return $result;
    }
   
    public function change_status_by_id($id, $status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->area_escooter);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
       public function get_all_area_data() {
        $this->db->select($this->area_marker.'.*');
        //$this->db->where($this->area_escooter.'.id', $id);
        $query = $this->db->get($this->area_marker);
        $result = $query->result();
        return $result;
    }

    public function add_polygon_data($data)
    {
    	$data['add_date'] = getDefaultToGMTDate(time());
    	$this->db->select($this->area_escooter.'.*');
    	$query = $this->db->insert($this->area_escooter, $data);
    	return $this->db->insert_id();

    }
     public function add_marker_data($data)
    {
        $data['add_date'] = getDefaultToGMTDate(time());
        $this->db->select($this->area_marker.'.*');
        $query = $this->db->insert($this->area_marker, $data);
        return $this->db->insert_id();

    }
    public function change_status_by_id_marker($id, $status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->area_marker);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    
}
