<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**Hunters_model
 */
class Zuordnung_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_zuordnung_sps';
	}

	public function no_of_hunters()
	{
		$query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function count_zuordnung($searchData)
	{
		$this->db->select($this->table.'.*');
		//$this->db->where('user_type', 'Hunters');

		if (isset($searchData) && $searchData['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->table.'.first_name', $searchData['keyword']);
			$this->db->or_like($this->table.'.last_name', $searchData['keyword']);
			$this->db->group_end();
		}

		if (isset($searchData) && $searchData['status']) {
			$this->db->where($this->table.'.status', $searchData['status']);
		}

		if (isset($searchData) && $searchData['sorting_order']) {
			$this->db->order_by($this->table.'.'.$searchData['column_name'], $searchData['sorting_order']);
		}

		$query = $this->db->get($this->table);
		return $query->num_rows();

	}

	public function getAllzuordnung($searchData)
	{
		$this->db->select($this->table.'.*');
		//$this->db->where('user_type', 'Hunters');
		if (isset($searchData) && $searchData['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->table.'.first_name', $searchData['keyword']);
			$this->db->or_like($this->table.'.last_name', $searchData['keyword']);
			$this->db->or_like($this->table.'.email', $searchData['keyword']);
			$this->db->group_end();
		}

		if (isset($searchData) && $searchData['status']) {
			$this->db->where($this->table.'.status', $searchData['status']);
		}

		if (isset($searchData) && $searchData['sorting_order']) {
			$this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);
		}

		if (isset($searchData) && $searchData['limit']) {
			$this->db->limit($searchData['limit'], $searchData['search_index']);
		}

		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function add_new_hunter($data)
	 {   
		$data['add_date'] = getDefaultToGMTDate(time());
		$data['user_type'] = 'Hunters';
		$this->db->select($this->table.'*');
		$query = $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	 }

	public function getzuordnung()
	{
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function getzuordnungById($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function change_status_by_id($id,$status)
	{
		$this->db->where('id', $id);
		$this->db->set('status', $status);
		$this->db->update($this->table);

		if (!$this->db->affected_rows()) {
			return false;
		} else {
			return true;
		}
	}
	

	public function setzuordnungById($id, $data)
	{  
		
		$this->db->select($this->table.'.*');
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);

		if (!$this->db->affected_rows()) {
			return false;
		} else
		{
			return true;
		}
	}

   /* public function delete_user_by_id($id)
    {
       $this->db->where('id',$id); 
       $this->db->delete($this->table); 
    }*/

	public function remove_file($id)
	{
		$files = $this->get_hunter_by_id($id);
		if ($files->profile) 
		{
			$path = 'assets/uploads/hunters_images/';
			unlink($path . $files->profile);	
		}
	}
}

?>