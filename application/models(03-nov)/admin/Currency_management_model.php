<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Currency_management_model class.
 * @extends CI_Controller
 */

class Currency_management_model extends CI_Model {

   /**
    * __construct function.
    * @access public
    * @return void
    */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_currencies';
    }

 
   /**
    * getcurrency function.
    * @access public
    * @return records of all Crews
    */

    public function getcurrency($searchData) {

        $this->db->select($this->table.'.*');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.currency_name', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }

        if (isset($searchData) && $searchData['limit']) {
            $this->db->limit($searchData['limit'], $searchData['search_index']);
        }

        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

   /**
    * getcurrencyById function.
    * @access public
    * @param mixed $id
    */

    public function getcurrencyById($id) {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id',$id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

   

   /**
    * addcurrency function.
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    public function addcurrency($data) {

        $data['add_date'] = getDefaultToGMTDate(time());
        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

   /**
    * setcurrencyById function.
    * @access public
    * @param mixed $data as array
    * @param mixed $id
    * @return true or false
    */

    public function setcurrencyById($id,$data) {
        $data['update_date'] = getDefaultToGMTDate(time());
        $this->db->where('id',$id);
        $this->db->update($this->table,$data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * changeStatusById function.
    * @access public
    * @param mixed $status
    * @param mixed $id
    * @return true or false
    */

    public function changeStatusById($id, $status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
   /**
    * get_currency function.
    * @access public
    */

    public function get_currency() {
        $this->db->select($this->table.'.*');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }
   
   /**
    * deletecurrencyById function.
    * @access public
    */

    public function deletecurrencyById($id){
       $this->db->where('id',$id); 
       $this->db->delete($this->table); 
    }
}