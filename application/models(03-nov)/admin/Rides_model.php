<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Rides_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->tbl = 'tbl_rides';
        $this->user = 'tbl_user';
        $this->scooter = 'tbl_scooter';
    }

    /**
    * get_all_rides_pages function.
    * 
    * @access public
    * @return static page records
    */

      public function get_all_rides_pages()
    {
        $this->db->select($this->tbl.'.*');
        $this->db->where('is_status','Completed');
        $this->db->select($this->scooter.'.id, maker_name, name');
        $this->db->select($this->user.'.id, first_name, last_name');        
        $this->db->join($this->scooter, $this->scooter.'.id='. $this->tbl.'.scooter_id');
        $this->db->join($this->user, $this->user.'.id='. $this->tbl.'.user_id');
        $query = $this->db->get($this->tbl);
        //pr($query); die;
        return $query->result();
    }

    /**
    * get_static_page_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @return row of static page
    */

    function get_static_page_by_id($id) {
        $this->db->where('id',$id);
        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    /**
    * set_static_page_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $data as array
    * @return true or false
    */

    function set_static_page_by_id($id,$data) {
        $data['edited_time'] = getDefaultToGMTDate(time());
        
        $this->db->where('id',$id);
        $result = $this->db->update($this->table,$data);
        return $this->db->affected_rows();
    }

    /**
    * add_static_page function.
    * 
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    function add_static_page($data) {
        $data['is_deleteable'] = 'No';
        $data['add_date'] = getDefaultToGMTDate(time());

        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

    /**
    * delete_static_page_by_id function.
    * 
    * @access public
    * @param mixed $static_page_id
    * @return null
    */

    function delete_static_page_by_id($static_page_id) {
        $this->db->where('id',$static_page_id);
        $this->db->delete($this->table);
    }

      public function get_ride_by_id($id)
    {
        $this->db->select($this->tbl.'.*');
        $this->db->select($this->scooter.'.id, maker_name, name');
        $this->db->select($this->user.'.id, first_name, last_name');      
        $this->db->where($this->tbl.'.ride_id', $id);
        $this->db->join($this->user, $this->user.'.id='. $this->tbl.'.user_id');
        $this->db->join($this->scooter, $this->scooter.'.id='. $this->tbl.'.scooter_id');
        $query = $this->db->get($this->tbl);

        return $query->row();
    }


    /**
    * change_status_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

    function change_status_by_id($id,$status) {
        $this->db->where($this->tbl.'.ride_id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->tbl);
        
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

}
