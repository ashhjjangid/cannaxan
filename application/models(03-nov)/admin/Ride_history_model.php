<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Faq_model class.
 * 
 * @extends CI_Controller
 */

class Ride_history_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();        
        $this->tbl = 'tbl_rides';
        $this->user = 'tbl_user';
        $this->scooter = 'tbl_scooter';

    }

    /**
    * adding new faq function.
    * 
    * @access public
    * @return void
   */

    public function addrating($data)
    {
        $data['add_date'] = getDefaultToGMTDate(time());
        $this->db->insert($this->tbl, $data);
        return $this->db->insert_id();
    }

    /**
    * get_data function.
    * @access public
    * @return all rows in the tbl
   */

    public function get_data() {
        $this->db->select($this->tbl.'.*');
        $query = $this->db->get($this->tbl);
        $result = $query->result();
        return $result;
    }

    /**
    *  get_rating_for_update function.
    *  @access public
    *  @return void
   */

    public function get_rating_for_update($id)
    {
        $this->db->select($this->tbl.'.*');
        $this->db->where('rating_id', $id);
        $query = $this->db->get($this->tbl);
        return $query->row();
    }

    /**
    * set_updated_rating function.
    * @access public
    * @return void
   */

    public function set_updated_rating($id, $data)
    {
        $data['update_date'] = getDefaultToGMTDate(time());
        $this->db->select($this->tbl.'.*');
        $this->db->where('rating_id', $id);
        $this->db->update($this->tbl, $data);
        if ($this->db->affected_rows()) 
        {
            return true;
        } else {
            return false;
        }
    }
    /**
    * change_status_by_id function.
    * @access public
    * @return void
   */

    public function change_status_by_id($id,$status)
    {
        $this->db->select($this->tbl.'.*');
        $this->db->where($this->tbl.'.ride_id', $id);
        $this->db->set($this->tbl.'.status', $status);
        $this->db->update($this->tbl);
        if ($this->db->affected_rows()) 
        {
            return true;
        }
        else 
        {
            return false; 
        }
    }

    /**
    * delete_rating function.
    * @access public
    * @return void
   */

    public function delete_rating($id)
    {
        $this->db->select($this->tbl);
        $this->db->where('ride_id', $id);
        $this->db->delete($this->tbl);
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    public function get_list_of_rides()
    {
        $this->db->select($this->tbl.'.*');
        $this->db->where('is_status','Completed');
        $this->db->select($this->scooter.'.id, maker_name, name');
        $this->db->select($this->user.'.id, first_name, last_name');        
        $this->db->join($this->scooter, $this->scooter.'.id='. $this->tbl.'.scooter_id');
        $this->db->join($this->user, $this->user.'.id='. $this->tbl.'.user_id');
        $query = $this->db->get($this->tbl);
        //pr($query); die;
        return $query->result();
    }

    public function view_single_review($id)
    {
        $this->db->select($this->tbl.'.*');
        $this->db->select($this->scooter.'.id, maker_name, name');
        $this->db->select($this->user.'.id, first_name, last_name');      
        $this->db->where($this->tbl.'.ride_id', $id);
        $this->db->join($this->user, $this->user.'.id='. $this->tbl.'.user_id');
        $this->db->join($this->scooter, $this->scooter.'.id='. $this->tbl.'.scooter_id');
        $query = $this->db->get($this->tbl);

        return $query->row();
    }

}
   