<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Service_model class.
 * 
 * @extends CI_Controller
 */

class Users_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_users';
        //$this->table_skipper_crew = 'tbl_skipper_crew';
    }

   /**
    * countCrew function.
    * 
    * @access public
    * @param mixed $seacrhData as array
    * @return count of crew type of users
    */
    public function countUsers($searchData)
    {
        $this->db->select($this->table.'.*');
        //$this->db->where($this->table.'.user_type', 'Crew');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.first_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.last_name', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }
        
        $query  = $this->db->get($this->table);
        $result = $query->num_rows();
        return $result;
    }

   /**
    * getCrew function.
    * 
    * @access public
    * @return records of all Crews
    */

    public function get_all_users($searchData) {

        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.user_type', 'User');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.first_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.last_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.email', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }

        if (isset($searchData) && $searchData['limit']) {
            $this->db->limit($searchData['limit'], $searchData['search_index']);
        }

        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

   /**
    * getCrewsById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of crew type of user
    */

    public function get_user_by_id($id) {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id',$id);
        $this->db->where($this->table.'.user_type','User');
        $query = $this->db->get($this->table);
        return $query->row();
    }

   /**
    * delete_user_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function delete_user_by_id($id) {
       $this->db->where('id',$id); 
       $this->db->delete($this->table); 
    }

   /**
    * add_user function.
    * 
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    public function add_user($data) {

        $data['add_date'] = getDefaultToGMTDate(time());
        /*$data['user_type'] = 'User';*/
        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

   /**
    * set_user_by_id function.
    * 
    * @access public
    * @param mixed $data as array
    * @param mixed $id
    * @return true or false
    */

    public function set_user_by_id($id,$data) {
        $data['update_date'] = getDefaultToGMTDate(time());

        /*if (isset($data['image']) && $data['image']) {
            $this->removeFileById($id);
        }*/
        
        $this->db->where('id',$id);
        $this->db->update($this->table,$data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * changeStatusById function.
    * 
    * @access public
    * @param mixed $status
    * @param mixed $id
    * @return true or false
    */

    public function change_status_by_id($id, $status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }


   /**
    * doesEmailExist function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $id
    * @return true or false
    */

    public function doesEmailExist($email = '', $id = '') {
        $this->db->select($this->table.'.id');
        $this->db->where($this->table.'.id!=',$id);
        $this->db->where($this->table.'.email',$email);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

   /**
    * doesUsernameExist function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $id
    * @return true or false
    */

    public function doesUsernameExist($username = '', $id = '') {
        $this->db->select($this->table.'.id');
        $this->db->where($this->table.'.id!=',$id);
        $this->db->where($this->table.'.username',$username);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    /**
    * removeFileById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    /*public function removeFileById($id)
    {
        $users = $this->gethelpdeskById($id);

        if ($users->image) {
            $path = './assets/uploads/help_desk/';
            unlink($path . $users->image);
        }
    }*/    
    
}
