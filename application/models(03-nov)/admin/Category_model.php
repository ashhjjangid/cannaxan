<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Crew_model class.
 * 
 * @extends CI_Controller
 */

class Category_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_categories';
        //$this->table_skipper_crew = 'tbl_skipper_crew';
    }



   /**
    * getCrew function.
    * 
    * @access public
    * @return records of all Crews
    */

    public function getproduct($searchData) {

        $this->db->select($this->table.'.*');
        //$this->db->where($this->table.'.user_type', 'Crew');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.first_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.last_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.email', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }

        if (isset($searchData) && $searchData['limit']) {
            $this->db->limit($searchData['limit'], $searchData['search_index']);
        }

        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

   /**
    * getActiveCrew function.
    * 
    * @access public
    * @return result of all Active Crew
    */

    public function get_category() {
        $this->db->select($this->table.'.*');
        //$this->db->where($this->table.'.user_type', 'Crew');
        //$this->db->where($this->table.'.status', 'Active');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

   /**
    * getCrewsById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of crew type of user
    */

    public function getcategoryById($id) {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id',$id);
        //$this->db->where($this->table.'.user_type','category');
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function getproductsById($id) {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id',$id);
        //$this->db->where($this->table.'.user_type','Crew');
        $query = $this->db->get($this->table);
        return $query->row();
    }
  
    
   /**
    * getSkippersCrew function.
    * 
    * @access public
    * @param mixed $skipper_id
    * @return result of skipper crew
    */

    public function getSkippersCrew($skipper_id)
    {   
        $this->db->select($this->table.'.*');
        $this->db->select($this->table_skipper_crew.'.*');
        $this->db->where($this->table_skipper_crew.'.skipper_id',$skipper_id);
        $this->db->join($this->table, $this->table.'.id = '.$this->table_skipper_crew.'.crew_id');
        $query = $this->db->get($this->table_skipper_crew);
        $result = $query->result();
        return $result;
    }

   /**
    * isSkipperCrewAlreadyExists function.
    * 
    * @access public
    * @param mixed $skipper_id
    * @param mixed $Crew_id
    * @return number of rows
    */
   
    public function isSkipperCrewAlreadyExists($Skipper_id,$Crew_id) {
        $this->db->select($this->table_skipper_crew.'.id');
        $this->db->where($this->table_skipper_crew.'.skipper_id',$Skipper_id);
        $this->db->where($this->table_skipper_crew.'.crew_id',$Crew_id);
        $query = $this->db->get($this->table_skipper_crew);
        return $query->num_rows();
    }

  

   /**
    * deleteSkipperCrewById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function deletecategoryById($id) 
    {
       $this->db->where('id',$id); 
       $this->db->delete($this->table); 
    }

   /**
    * addCrew function.
    * 
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    public function addcategory($data) {

        $data['add_date'] = getDefaultToGMTDate(time());
       // $data['user_type'] = 'Crew';
        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

   

   /**
    * setCrewById function.
    * 
    * @access public
    * @param mixed $data as array
    * @param mixed $id
    * @return true or false
    */

    public function setcategoryById($id,$data) {
       $data['update_date'] = getDefaultToGMTDate(time());       
        $this->db->where('id',$id);
        $this->db->update($this->table,$data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
function get_data() {
        $this->db->select($this->table.'.*');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }
   /**
    * changeStatusById function.
    * 
    * @access public
    * @param mixed $status
    * @param mixed $id
    * @return true or false
    */

    public function changeStatusById($id, $status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   

   
  
    
}