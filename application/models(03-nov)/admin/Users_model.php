<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');


class Users_model extends CI_Model {


    function __construct() {
        parent::__construct();
        $this->table = 'tbl_members';

    }


    public function add_doctors($data)
    {
        $data['add_date'] = getDefaultToGMTDate(time());
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }


    public function count_users($searchData)
    {
        if ( isset($searchData['keyword']) && $searchData['keyword'] ) {
            $this->db->group_start();
            $this->db->or_like($this->table. '.first_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.name',$searchData['keyword']);
            $this->db->or_like($this->table.'.email',$searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.'.$searchData['column_name'], $searchData['sorting_order']);
        }
        $this->db->select($this->table.'.*');
        $this->db->where('is_approved', 'Yes');
        $this->db->where('is_register_completed', 'Yes');
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

  

    public function get_all_users($searchData)
    {
        $this->db->select($this->table. '.*');

        if (isset($searchData['keyword']) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.first_name',$searchData['keyword']);
            $this->db->or_like($this->table.'.name',$searchData['keyword']);
            $this->db->or_like($this->table.'.email', $searchData['keyword']);
            $this->db->group_end();
        }

        if(isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.'.$searchData['column_name'], $searchData['sorting_order']);
        }

        if (isset($searchData['limit']) && $searchData['limit'] ) {
            $this->db->limit($searchData['limit'],$searchData['search_index']);
        }
        $this->db->where('is_approved', 'Yes');
        $this->db->where('is_register_completed', 'Yes');

        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function count_unapproved_users($searchData)
    {
        if ( isset($searchData['keyword']) && $searchData['keyword'] ) {
            $this->db->group_start();
            $this->db->or_like($this->table. '.first_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.name',$searchData['keyword']);
            $this->db->or_like($this->table.'.email',$searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.'.$searchData['column_name'], $searchData['sorting_order']);
        }
        $this->db->select($this->table.'.*');
        $this->db->where('is_approved', 'No');
        $this->db->where('is_register_completed', 'Yes');
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

  

    public function get_all_unapproved_users($searchData)
    {
        $this->db->select($this->table. '.*');

        if (isset($searchData['keyword']) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.first_name',$searchData['keyword']);
            $this->db->or_like($this->table.'.name',$searchData['keyword']);
            $this->db->or_like($this->table.'.email', $searchData['keyword']);
            $this->db->group_end();
        }

        if(isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.'.$searchData['column_name'], $searchData['sorting_order']);
        }

        if (isset($searchData['limit']) && $searchData['limit'] ) {
            $this->db->limit($searchData['limit'],$searchData['search_index']);
        }
        $this->db->where('is_approved', 'No');
        $this->db->where('is_register_completed', 'Yes');
        $query = $this->db->get($this->table);
        return $query->result();
    }


    public function get_doctors_for_update($id)
    {
        $this->db->select($this->table.'.*');
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function get_doctors()
    {
        $this->db->select($this->table.'.*');
        $query = $this->db->get($this->table);
        return $query->result();
    }


    public function set_updated_doctors_data($id, $data)
    {
        $data['update_date'] = getDefaultToGMTDate(time());
        $this->db->select($this->table.'.*');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows()) 
        {
            return true;
        } else {
            return false;
        }
    }

    public function delete_user($id)
    {
        $this->db->select($this->table);
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function view_user($id)
    {
        $this->db->select($this->table.'.*');
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function change_status_by_id($id, $status)
    {
        $this->db->select($this->table.'.*');
        $this->db->where('id', $id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) 
        {
            return true;
        }else
        {
            return false;
        }
    }

    public function change_approval_status_by_id($id, $approval)
    {
        $this->db->select($this->table.'.*');
        $this->db->where('id', $id);
        $this->db->set('is_approved', $approval);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) 
        {
            return true;
        }else
        {
            return false;
        }
    }

    public function Countnumberofdoctors()
     {
      $this->db->select($this->table.'.*');
      $this->db->where('is_register_completed', 'Yes');
      $query = $this->db->get($this->table);
      return $query->num_rows();  
    }

}
   