<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**Riders_model
 * 
 */
class Riders_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_users';
	}

	public function no_of_riders()
	{
		$query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function countRiders($searchData)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('user_type=', 'Riders');
		if (isset($searchData) && $searchData['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->table.'.first_name', $searchData['keyword']);
			$this->db->or_like($this->table.'.last_name', $searchData['keyword']);
			$this->db->group_end();
		}

		if (isset($searchData) && $searchData['status']) {
			$this->db->where($this->table.'.status', $searchData['status']);
		}

		if (isset($searchData) && $searchData['sorting_order']) {
			$this->db->order_by($this->table.'.'.$searchData['column_name'], $searchData['sorting_order']);
		}

		$query = $this->db->get($this->table);
		return $query->num_rows();

	}

	public function getAllRiders($searchData)
	{
		//$this->db->select($this->table.'.*');
		$this->db->where('user_type=', 'Riders');
		if (isset($searchData) && $searchData['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->table.'.first_name', $searchData['keyword']);
			$this->db->or_like($this->table.'.last_name', $searchData['keyword']);
			$this->db->or_like($this->table.'.email', $searchData['keyword']);
			$this->db->group_end();
		}

		if (isset($searchData) && $searchData['status']) {
			$this->db->where($this->table.'.status', $searchData['status']);
		}

		if (isset($searchData) && $searchData['sorting_order']) {
			$this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);
		}

		if (isset($searchData) && $searchData['limit']) {
			$this->db->limit($searchData['limit'], $searchData['search_index']);
		}

		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get_all_riders()
	{
		$this->db->where('user_type=', 'Riders');
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function add_new_rider($data)
	{
		$data['add_date'] = getDefaultToGMTDate(time());
		$data['user_type'] = 'Riders';
		//pr($data); die;
		$this->db->select($this->table.'*');
		$query = $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_rider_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->where('user_type=', 'Riders');
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function change_status_by_id($id,$status)
	{
		$this->db->where('id', $id);
		$this->db->set('status', $status);
		$this->db->update($this->table);

		if (!$this->db->affected_rows()) {
			return false;
		} else {
			return true;
		}
	}

	public function update_rider_details($id, $data)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('user_type=', 'Riders');
		if (isset($data['profile']) && $data['profile']) 
		{
			$this->remove_file($id);	
		}
		$data['update_date'] = getDefaultToGMTDate(time());
		$this->db->select($this->table.'.*');
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);

		if (!$this->db->affected_rows()) {
			return false;
		} else
		{
			return true;
		}
	}

	public function doesEmailExist($email, $id = '') {
		$this->db->select($this->table.'.id');
		$this->db->where('id!=', $id);
		$this->db->where('email', $email);
		$query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function remove_file($id)
	{
		$files = $this->get_rider_by_id($id);
		if ($files->profile) 
		{
			$path = 'assets/uploads/rider_images/';
			unlink($path . $files->profile);	
		}
	}
}

?>