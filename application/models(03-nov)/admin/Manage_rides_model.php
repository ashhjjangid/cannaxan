<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Manage_rides_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->rides = 'tbl_rides';
        $this->scooter = 'tbl_scooter';
        $this->user = 'tbl_user';

    }

    /**
    * get_static_page_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @return row of static page
    */

    function get_static_page_by_id($id) {
        $this->db->where('ride_id',$id);
        $query = $this->db->get($this->rides);
        $result = $query->row();
        return $result;
    }

   

    /**
    * change_status_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

    function change_status_by_id($id,$status) {
        $this->db->where('ride_id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->rides);
        
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    public function get_rides()
    {
        $this->db->select($this->rides.'.*');
        $this->db->select($this->scooter.'.id, maker_name, name');
        $this->db->select($this->user.'.id, first_name, last_name');
        $this->db->where($this->rides.'.is_status', 'In progress');
        $this->db->join($this->scooter, $this->scooter.'.id='.$this->rides.'.scooter_id');
        $this->db->join($this->user, $this->user.'.id=' . $this->rides.'.user_id');
        $query = $this->db->get($this->rides);
        return $query->result();
    }

    public function view_ride_details($id)
    {
        $this->db->select($this->rides.'.*');
        $this->db->select($this->scooter.'.id, maker_name, name');
        $this->db->select($this->user.'.id, first_name, last_name');
        $this->db->where($this->rides.'.ride_id', $id);
        $this->db->join($this->scooter, $this->scooter.'.id='.$this->rides.'.scooter_id');
        $this->db->join($this->user, $this->user.'.id=' . $this->rides.'.user_id');
        $query = $this->db->get($this->rides);
        return $query->row();
    }

}
