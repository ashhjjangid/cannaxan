<?php
if (!defined('BASEPATH'))
  { exit('No direct script access allowed');
}

/**
 * User_model class.
 * 
 * @extends CI_Controller
 */

class User_model extends CI_Model
{
   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    public function __construct()
    {
        parent::__construct();
        $this->table = 'tbl_users';
        $this->table_key = 'tbl_reset_password_key';
    }

    /**
    * setWalletBalance function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $amount
    * @return true or false
    */

    function setWalletBalance($amount,$id) {
        $this->db->where('id',$id);
        $this->db->set('wallet_balance',$amount);
        $result = $this->db->update($this->table);
        return $this->db->affected_rows();        
    }
    
    /**
    * changeUserStatusById function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

    public function changeUserStatusById($id, $status)
    {
        $this->db->where('id', $id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * deleteUserById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function deleteUserById($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    /**
    * checkUserExistByOldPassword function.
    * 
    * @access public
    * @param mixed $password
    * @param mixed $userId
    * @return row of user
    */

    public function checkUserExistByOldPassword($password, $userId)
    {
        $this->db->where('id', $userId);
        $this->db->where('password', $password);
        $query  = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    /**
    * setUserDetails function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $data as array
    * @return true or false
    */

    public function setUserDetails($id, $data)
    {
        $data['update_date'] = getDefaultToGMTDate(time());
        $data['last_ip'] = $_SERVER['REMOTE_ADDR'];
        $data['is_email_verified'] = 'Yes';
        
        if (isset($data['image']) && $data['image']) {
            $this->removeFileById($id);
        }

        $this->db->where('id', $id);
        $result = $this->db->update($this->table, $data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * getUserDetails function.
    * 
    * @access public
    * @param mixed $id
    * @return row of user details
    */

    public function getUserDetails($id)
    {
        $this->db->where('id', $id);
        $query  = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    /**
    * getAllusers function.
    * 
    * @access public
    * @return result of all users
    */

    public function getAllusers()
    {
        $this->db->select($this->table.'.*');
        $query  = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

   /**
    * check_valid_user function.
    * 
    * @access public
    * @param mixed $user as array
    * @return result if user is admin
    */

   public function check_valid_user($user)
   {
      $this->db->select('id');
      $this->db->where('status', 'Active');
      $this->db->where('user_type', 'Admin');
      $this->db->from($this->table);
      $this->db->where($user);
      $query = $this->db->get();
      return $query->row();
   }


   /**
    * getUserByEmailId function.
    * 
    * @access public
    * @param mixed $emailId 
    * @return row of user
    */

   public function getUserByEmailId($emailId)
   {
      $this->db->select('*');
      $this->db->where('email', $emailId);
      $query  = $this->db->get($this->table);
      $result = $query->row();
      return $result;
   }

   /**
    * setPasswordKey function.
    * 
    * @access public
    * @param mixed $id 
    * @param mixed $data as array 
    * @return true or false
    */

    public function setPasswordKey($id, $data)
   {  
      $data['last_ip'] = $_SERVER['REMOTE_ADDR'];
      $data['add_date'] = getDefaultToGMTDate(time());

      $this->db->where('user_id', $id);
      $query  = $this->db->get($this->table_key);
      $result = $query->num_rows();
      
      if($result) {
         $this->db->where('user_id', $id);
         $this->db->update($this->table_key, $data);

         if ($this->db->affected_rows()) {
             return true;
         } else {
             return false;
         }
      } else {
          $this->db->insert($this->table_key,$data);
          return $this->db->insert_id();
      }
   }

   /**
    * is_key_exists function.
    * 
    * @access public
    * @param mixed $key
    * @return row of user
    */

    public function is_key_exists($key)
    {
        $time = getDefaultToGMTDate(time());
        $this->db->select($this->table_key.'.user_id as id');
        $this->db->where('expire_time >',$time);
        $this->db->where('auth_key',$key);
        $this->db->where('key_type','Reset');
        $query = $this->db->get($this->table_key);
        $result = $query->row();
        return $result;   
    }

   /**
    * setExpireUserKey function.
    * 
    * @access public
    * @param mixed $userId
    * @return row of userkey record
    */

   public function setExpireUserKey($key)
   {
      $this->db->where('auth_key',$key);
      $this->db->set('used','Yes');
      $this->db->update($this->table_key);

      if ($this->db->affected_rows()) {
          return true;
      } else {
          return false;
      }
   }


   /**
    * getPasswordKey function.
    * 
    * @access public
    * @param mixed $key
    * @return row of user
    */

   public function getPasswordKey($key)
   { 
      $time = getDefaultToGMTDate(time());
      $this->db->select($this->table_key.'.*');
      $this->db->where($this->table_key.'.auth_key', $key);
      $this->db->where($this->table_key.'.used', 'No');
      $this->db->where($this->table_key.'.expire_time >',$time);
      $query  = $this->db->get($this->table_key);
      $result = $query->row();
      return $result;
   }

   /**
    * checkExpireTime function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $key
    * @return row of user
    */

   public function getPasswordExpireTime($id, $time, $key)
   {
      $this->db->where('user_id', $id);
      $this->db->where('expire_time >',$time);
      $this->db->where('auth_key', $key);
      $query  = $this->db->get($this->table_key);
      $result = $query->row();
      return $result;
   }

   /**
    * setUserPassword function.
    * 
    * @access public
    * @param mixed $data as array
    * @param mixed $id
    * @return row of user
    */

   public function setUserPassword($id, $password)
   {
      $update_date = getDefaultToGMTDate(time());
      $this->db->where('id', $id);
      $this->db->set('password', $password);
      $this->db->set('update_date', $update_date);
      $this->db->update($this->table);
      
      if ($this->db->affected_rows()) {
         return true;
      } else {
         return false;
      }


   }

   /**
    * get_user_by_email function.
    * 
    * @access public
    * @param mixed $email
    * @return row of user
    */

   public function get_user_by_email($email)
   {
      $this->db->select('*');
      $this->db->where('email', $email);
      $query  = $this->db->get($this->table);
      $result = $query->row();
      return $result;
   }

    /**
    * doesEmailExist function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $id
    * @return true or false
    */

    public function doesEmailExist($email = '', $id = '') {
        $this->db->select($this->table.'.id');
        $this->db->where($this->table.'.id!=',$id);
        $this->db->where($this->table.'.email',$email);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

   /**
    * removeFileById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function removeFileById($id)
    {
        $users = $this->getUserDetails($id);

        if ($users->image) {

            if($users->user_type=='Skipper') {

                $path = './assets/uploads/skipper/profile/';

            } if($users->user_type=='Crew') {

               $path = './assets/uploads/crew/profile/';
            } 
            unlink($path . $users->image);
        }
    }
    
}    

