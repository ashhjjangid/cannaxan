<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Send_email_model extends CI_Model {
	
	/**
	* __construct function.
	* 
	* @access public
	* @return void
	*/
	function __construct() {
		parent::__construct();
		$this->table = 'tbl_promotion_email';
		$this->tbl ='tbl_user';
	}

   /**
    * get_all_data function.
    * 
    * @access public
    * @param mixed $id
    * @return records of all data
    */

	function get_all_data() {
		$this->db->select($this->table.'.*');
        $this->db->select($this->tbl.'.email');
        $this->db->join($this->tbl, $this->tbl.'.id = '.$this->table.'.user_id');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
	}

   /**
    * insert_email function.
    * 
    * @access public
    * @param mixed $inesrt_arr as aaray
    * @return insert id
    */

	function insert_email($insert_arr) {
		$data['add_date'] = getDefaultToGMTDate(time());
		$this->db->insert($this->table, $insert_arr,$data);
		return $this->db->insert_id();
	}

   /**
    * get_email_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @return row of send email
    */

	function get_email_by_id($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function get_user() {
        $this->db->select($this->tbl.'.*');
        $query = $this->db->get($this->tbl);
        $result = $query->result();
        return $result;
    }
    public function get_email($id)
    {
        $this->db->select($this->table.'.*');
        $this->db->where('id',$id);
        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getUserDetails($id)
    {
        $this->db->where('id', $id);
        $query  = $this->db->get($this->tbl);
        $result = $query->row();
        return $result;
    }

    public function get_user_view($id)
    {
       $this->db->select($this->table.'.*');
       $this->db->select($this->tbl.'.email');
       //$this->db->select($this->tbl.'.user_type');
       $this->db->where($this->table.'.id=', $id);
       $this->db->join($this->tbl, $this->tbl.'.id = '.$this->table.'.user_id');
       $query = $this->db->get($this->table);
       $result = $query->row();
       return $result;
    }

   /**
    * delete_email function.
    * 
    * @access public
    * @param mixed $id
    * @return true
    */

	function delete_email($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return true;
	}

   /**
    * change_status_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

	function change_status_by_id($id, $status) {
		$this->db->where('id',$id);
		$this->db->set('status',$status);
		$this->db->update($this->table);

		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}	
	}

   /**
    * get_newsletter_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @return row of news letter
    */	

	function get_newsletter_by_id($id) {
		$this->db->select('id,subject,message,status');
		$this->db->where('id',$id);
		$query = $this->db->get($this->table);
		return $query->row();
	}	
}