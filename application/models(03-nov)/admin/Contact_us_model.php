<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Contact_us_model class.
 * 
 * @extends CI_Controller
 */

class Contact_us_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_contact_us';
    }

   /**
    * getAllContactQueries function.
    * 
    * @access public
    * @return list of all contact queries
    */

    function getAllContactQueries() {
        $this->db->select($this->table.'.*');
        $this->db->order_by('add_date','desc');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

   /**
    * countSearch function.
    * 
    * @access public
    * @param mixed $searchData as null
    * @return count of all contact queries
    */

    function countSearch($searchData = NULL) {
        $this->db->select($this->table.'.*');
        $query = $this->db->get($this->table);
        $result = $query->num_rows();
        return $result;
    }

   /**
    * setContactQuery function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $data as array
    * @return count of all contact queries
    */

    function setContactQuery($id,$data) {
        $data['is_replied'] = 'Yes';
        $data['is_viewed'] = 'Yes';
                
        $this->db->where('id',$id);
        $this->db->update($this->table,$data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * getSignleRecordById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of contact query
    */

    function getSignleRecordById($id) {
        $this->db->select('*');
        $this->db->where('id',$id);
        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

   /**
    * deleteContactById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    function deleteContactById($id) {
        $this->db->where('id',$id);
        $this->db->delete($this->table);
    }

   /**
    * changeStatusById function.
    * 
    * @access public
    * @param mixed $id
    * @return true or false
    */

    function changeStatusById($id) {
        $this->db->where('id',$id);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }        
    }

   /**
    * replyContactQuery function.
    * 
    * @access public
    * @param mixed $data as array
    * @param mixed $id
    * @return inserted id
    */

    function replyContactQuery($data,$id){
        $this->db->where('id',$id);
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();

    }
}
