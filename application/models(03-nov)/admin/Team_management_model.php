<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**Team_management_model
 * 
 */
class Team_management_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_teams';
	}

	public function add_member($data)
	{
		$data['add_date'] = getDefaultToGMTDate(time());
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_all_team_members()
	{
		$this->db->select($this->table.'.*');
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get_member_for_update($id)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('id =', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function update_member_data($id, $data)
	{
		$this->db->select($this->table.'.*');

		if (isset($data['image']) && $data['image']) {
			$this->remove_image($id);	
		}

		$data['update_date'] = getDefaultToGMTDate(time());
		$this->db->where('id =', $id);
		$this->db->update($this->table, $data);

		if ($this->db->affected_rows()) {
			return true;
		}else {
			return false;
		}
	}

	public function view_member($id)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('id =', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function change_status_by_id($id, $status)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('id =', $id);
		$this->db->set('status', $status);
		$this->db->update($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function delete_member($id)
	{
		$this->db->select($this->table.'.*');
		$this->db->where('id =', $id);
		$this->db->delete($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function remove_image($id)
	{
		$member = $this->get_member_for_update($id);

		if ($member->image) 
		{
			$path = './assets/uploads/images/';
			unlink($path.$member->image);
		}
	}
}