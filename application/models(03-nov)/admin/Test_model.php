<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Crew_model class.
 * 
 * @extends CI_Controller
 */

class Test_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_users';
        $this->table_skipper_crew = 'tbl_skipper_crew';
    }

   /**
    * countUsers function.
    * 
    * @access public
    * @param mixed $seacrhData as array
    * @return count of type of users
    */
    public function countUsers($searchData)
    {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.user_type', 'User');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.first_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.last_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.email', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }
        
        $query  = $this->db->get($this->table);
        $result = $query->num_rows();
        return $result;
    }

   /**
    * getUsers function.
    * 
    * @access public
    * @return records of all Users
    */

    public function getUsers($searchData) {

        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.user_type', 'User');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.first_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.last_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.email', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }

        if (isset($searchData) && $searchData['limit']) {
            $this->db->limit($searchData['limit'], $searchData['search_index']);
        }

        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

   /**
    * getActiveCrew function.
    * 
    * @access public
    * @return result of all Active Crew
    */

    public function getActiveCrew() {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.user_type', 'Crew');
        $this->db->where($this->table.'.status', 'Active');
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

   /**
    * getUserById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of type of user
    */

    public function getUserById($id) {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id',$id);
        $this->db->where($this->table.'.user_type','User');
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function getCrewsById($id) {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id',$id);
        $this->db->where($this->table.'.user_type','Crew');
        $query = $this->db->get($this->table);
        return $query->row();
    }
   /**
    * getSkipperCrewById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of skipper crew
    */

    public function getSkipperCrewById($id) {
        $this->db->select($this->table.'.*');
        $this->db->select($this->table_skipper_crew.'.*');
        $this->db->where($this->table_skipper_crew.'.id',$id);
        $this->db->where($this->table.'.user_type','Crew');
        $this->db->join($this->table, $this->table.'.id = '.$this->table_skipper_crew.'.crew_id');
        $query = $this->db->get($this->table_skipper_crew);
        return $query->row();

    }
    
   /**
    * getSkippersCrew function.
    * 
    * @access public
    * @param mixed $skipper_id
    * @return result of skipper crew
    */

    public function getSkippersCrew($skipper_id)
    {   
        $this->db->select($this->table.'.*');
        $this->db->select($this->table_skipper_crew.'.*');
        $this->db->where($this->table_skipper_crew.'.skipper_id',$skipper_id);
        $this->db->join($this->table, $this->table.'.id = '.$this->table_skipper_crew.'.crew_id');
        $query = $this->db->get($this->table_skipper_crew);
        $result = $query->result();
        return $result;
    }

   /**
    * isSkipperCrewAlreadyExists function.
    * 
    * @access public
    * @param mixed $skipper_id
    * @param mixed $Crew_id
    * @return number of rows
    */
   
    public function isSkipperCrewAlreadyExists($Skipper_id,$Crew_id) {
        $this->db->select($this->table_skipper_crew.'.id');
        $this->db->where($this->table_skipper_crew.'.skipper_id',$Skipper_id);
        $this->db->where($this->table_skipper_crew.'.crew_id',$Crew_id);
        $query = $this->db->get($this->table_skipper_crew);
        return $query->num_rows();
    }

   /**
    * deleteCrewById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function deleteCrewById($id) {
       $this->db->where('id',$id); 
       $this->db->delete($this->table); 
    }

   /**
    * deleteSkipperCrewById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function deleteSkipperCrewById($id) {
       $this->db->where('id',$id); 
       $this->db->delete($this->table_skipper_crew); 
    }

   /**
    * addCrew function.
    * 
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    public function addCrew($data) {

        $data['add_date'] = getDefaultToGMTDate(time());
        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

   /**
    * addSkipperCrew function.
    * 
    * @access public
    * @param mixed $data as array
    * @return null
    */

    public function addSkipperCrew($data) {
        $data['add_date'] = getDefaultToGMTDate(time());
        $this->db->insert($this->table_skipper_crew,$data);
        return $this->db->insert_id();        
    }

   /**
    * setCrewById function.
    * 
    * @access public
    * @param mixed $data as array
    * @param mixed $id
    * @return true or false
    */

    public function setUserById($id,$data) {
        $data['update_date'] = getDefaultToGMTDate(time());

        if (isset($data['image']) && $data['image']) {
            $this->removeFileById($id);
        }
        
        $this->db->where('id',$id);
        $this->db->update($this->table,$data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * changeStatusById function.
    * 
    * @access public
    * @param mixed $status
    * @param mixed $id
    * @return true or false
    */

    public function changeStatusById($id, $status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * changeStatusBySkipperCrewId function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

    public function changeStatusBySkipperCrewId($id,$status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table_skipper_crew);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }


   /**
    * doesEmailExist function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $id
    * @return true or false
    */

    public function doesEmailExist($email = '', $id = '') {
        $this->db->select($this->table.'.id');
        $this->db->where($this->table.'.id!=',$id);
        $this->db->where($this->table.'.email',$email);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

   /**
    * doesUsernameExist function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $id
    * @return true or false
    */

    public function doesUsernameExist($username = '', $id = '') {
        $this->db->select($this->table.'.id');
        $this->db->where($this->table.'.id!=',$id);
        $this->db->where($this->table.'.username',$username);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    /**
    * removeFileById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function removeFileById($id)
    {
        $users = $this->getCrewsById($id);

        if ($users->image) {
            $path = './assets/uploads/crew/profile/';
            unlink($path . $users->image);
        }
    }    
    
}
