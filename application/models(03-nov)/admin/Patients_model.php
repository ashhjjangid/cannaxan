<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**Hunters_model
 */
class Patients_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_patients';
		$this->doctors = 'tbl_members';
		$this->questions = 'tbl_patient_questions';
		$this->tbl_doctors_id = 'tbl_doctors_id';
		$this->cannabis_medikament = 'tbl_cannabis_medikament';
		$this->patients_weitere_cannaxan_dosis = 'tbl_patients_weitere_cannaxan_dosis';
		$this->patient_therapy_plan_umstellung = 'tbl_patient_therapy_plan_umstellung';
		$this->patient_typ_umstellung = 'tbl_patient_typ_umstellung';
		$this->tbl_verlaufskontrolle_laborwerte = 'tbl_verlaufskontrolle_laborwerte';
		$this->tbl_verlaufskontrolletbl_laborwerte_laborparameter = 'tbl_verlaufskontrolletbl_laborwerte_laborparameter';
		$this->tbl_patient_thherpieplan_folgeverord = 'tbl_patient_thherpieplan_folgeverord';
		$this->tbl_patient_thherpieplan_folgeverord_option = 'tbl_patient_thherpieplan_folgeverord_option';
		$this->tbl_nebenwirkung_keine_kausal_bedingte = 'tbl_nebenwirkung_keine_kausal_bedingte';
		$this->tbl_cannabis_medikament = 'tbl_cannabis_medikament';
		$this->tbl_nebenwirkung ='tbl_nebenwirkung';
		$this->tbl_nebenwirkung_data = 'tbl_nebenwirkung_data';
		$this->arztlicher_therapieplan = 'tbl_arztlicher_therapieplan';
		$this->patient_therapy_plan_umstellung = 'tbl_patient_therapy_plan_umstellung';
		$this->patient_thherpieplan_folgeverord = 'tbl_patient_thherpieplan_folgeverord'; 
		$this->icd_code_zuordnung = 'tbl_icd_code_zuordnung';
	}

	public function count_patients($searchData)
	{
		$this->db->select($this->table.'.*');
		if (isset($searchData) && $searchData['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->table.'.first_name', $searchData['keyword']);
			$this->db->or_like($this->table.'.insured_number', $searchData['keyword']);
			$this->db->or_like($this->table.'.case_number', $searchData['keyword']);
			$this->db->group_end();

		}

		if (isset($searchData) && $searchData['status']) {
			$this->db->where($this->table.'.status', $searchData['status']);
		}

		if (isset($searchData) && $searchData['sorting_order']) {
			$this->db->order_by($this->table.'.'.$searchData['column_name'], $searchData['sorting_order']);
		}
		$query = $this->db->get($this->table);
		return $query->num_rows();

	}

	public function getAllpatients($searchData)
	{
		$this->db->select($this->table.'.*');
		if (isset($searchData) && $searchData['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->table.'.first_name', $searchData['keyword']);
			$this->db->or_like($this->table.'.insured_number', $searchData['keyword']);
			$this->db->or_like($this->table.'.case_number', $searchData['keyword']);
			$this->db->group_end();
		}

		if (isset($searchData) && $searchData['status']) {
			$this->db->where($this->table.'.status', $searchData['status']);
		}

		if (isset($searchData) && $searchData['sorting_order']) {
			$this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);
		}

		if (isset($searchData) && $searchData['limit']) {
			$this->db->limit($searchData['limit'], $searchData['search_index']);
		}

		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function add_patients($data)
	 {   
	 	//pr($data); die;
		$data['add_date'] = getDefaultToGMTDate(time());
		$this->db->select($this->table.'*');
		$query = $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	 }

	public function get_patients_by_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	function getPatientAllInformation($id) {
		$this->db->select($this->table.'.*');
		//$this->db->select($this->questions.'.*');
		$this->db->where($this->table.'.id', $id);
		//$this->db->join($this->questions, $this->questions.'.patient_id='. $this->table.'.id');
		$query = $this->db->get($this->table);
		return $query->row();
	}

	function getPatientAppQuestionsData($id) {
		$this->db->select($this->questions.'.*');
		$this->db->where($this->questions.'.patient_id', $id);
		$this->db->where($this->questions.'.is_process_complete', 'Yes');
		$this->db->order_by($this->questions.'.id', 'DESC');
		$this->db->group_by($this->questions.'.patient_id');
		$query = $this->db->get($this->questions);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function get_patientweitere_cannaxan_dosis($id) {
		$this->db->select($this->patients_weitere_cannaxan_dosis.'.*');
		$this->db->where($this->patients_weitere_cannaxan_dosis.'.patient_id', $id);
		$this->db->group_by($this->patients_weitere_cannaxan_dosis.'.patient_id');
		$this->db->order_by($this->patients_weitere_cannaxan_dosis.'.id', 'DESC');
		$query = $this->db->get($this->patients_weitere_cannaxan_dosis);
		//pr($this->db->last_query()); die;
		return $query->result();
	}

	public function change_status_by_id($id,$status)
	{
		$this->db->where('id', $id);
		$this->db->set('status', $status);
		$this->db->update($this->table);

		if (!$this->db->affected_rows()) {
			return false;
		} else {
			return true;
		}
	}
	

	public function update_patients_details($id, $data)
	{  
		$data['update_date'] = getDefaultToGMTDate(time());
		$this->db->select($this->table.'.*');
		$this->db->select($this->table.'.*');
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);

		if (!$this->db->affected_rows()) {
			return false;
		} else
		{
			return true;
		}
	}

    public function delete_patients($id)
    {
       $this->db->where('id',$id); 
       $this->db->delete($this->table); 
    }

    public function Countnumberofpatients()
     {
      $this->db->select($this->table.'.*');
      $query = $this->db->get($this->table);
      return $query->num_rows();  
    }

    function getPatientumstellungData($id) {
    	$this->db->select($this->table.'.*');
    	$this->db->select($this->patient_therapy_plan_umstellung.'.id as therapy_plan_id, patient_id, cannabis_medikament_id, dosiseinheit, tagesdosis_bisheriges, tagesdosis, rezepturarzneimittel, wirkstoff, anzahl_therapietage');
    	$this->db->where($this->table.'.id' , $id);
    	$this->db->join($this->patient_therapy_plan_umstellung, $this->patient_therapy_plan_umstellung.'.patient_id ='. $this->table. '.id');
    	$query = $this->db->get($this->table);
    	//pr($this->db->last_query()); die;
    	return $query->row();
    }

    public function getPaitentumstellungDataByPatientId($id)
	{
        $this->db->select($this->patient_typ_umstellung.'.*');
        $this->db->where($this->patient_typ_umstellung.'.patient_therapy_plan_umstellung_id',$id); 
        $query = $this->db->get($this->patient_typ_umstellung);
        return $query->result();
	}

	public function getcannabis_medikament()
	{
        $this->db->select($this->tbl_cannabis_medikament.'.name');
        //$this->db->where($this->tbl_patient_dosistyp_1.'.arztlicher_therapieplan_id',$id); 
        $query = $this->db->get($this->tbl_cannabis_medikament);
        return $query->result();
	}

	function getPatientlaborwerteData($id) {
    	$this->db->select($this->table.'.*');
    	$this->db->select($this->tbl_verlaufskontrolle_laborwerte.'.id as patients_laborwerte_id,patient_id,visite,behandungstag, arzneimittel');
    	$this->db->where($this->table.'.id' , $id);
    	$this->db->join($this->tbl_verlaufskontrolle_laborwerte, $this->tbl_verlaufskontrolle_laborwerte.'.patient_id ='. $this->table. '.id');
    	$query = $this->db->get($this->table);
    	//pr($this->db->last_query()); die;
    	return $query->row();
    }

    public function getPaitentlaborwerteDataByPatientId($id)
	{
        $this->db->select($this->tbl_verlaufskontrolletbl_laborwerte_laborparameter.'.*');
        $this->db->where($this->tbl_verlaufskontrolletbl_laborwerte_laborparameter.'.laborwerte_id',$id); 
        $query = $this->db->get($this->tbl_verlaufskontrolletbl_laborwerte_laborparameter);
        return $query->row();
	}

	 function getPatientnebenwirkungData($id) {

    	$this->db->select('id as patient_nebenwirkung_id ,patient_id,datum,behandungstag,visite,arzneimittel,keine_kausal_bedingte');
    	$this->db->where('patient_id', $id);
    	$this->db->order_by('patient_nebenwirkung_id', 'DESC');
    	//$this->db->group_by('patient_id');
    	$query = $this->db->get($this->tbl_nebenwirkung_keine_kausal_bedingte);
    	return $query->row();
    }
    public function getnebenwirkung()
	{
        $this->db->select($this->tbl_nebenwirkung.'.id, nebenwirkung_name');
        //$this->db->where($this->tbl_patient_dosistyp_1.'.arztlicher_therapieplan_id',$id); 
        $query = $this->db->get($this->tbl_nebenwirkung);
        return $query->result();
	}
	public function getnebenwirkung_data($id)
	{
        $this->db->select($this->tbl_nebenwirkung_data.'.*');
        $this->db->where('behandlunggstag_id',$id); 
        $query = $this->db->get($this->tbl_nebenwirkung_data);
        return $query->result();
	}

    function getPatientfolgeverordData($id) {
    	$this->db->select($this->table.'.*');
    	$this->db->select($this->tbl_patient_thherpieplan_folgeverord.'.id as patient_folgeverord_id ,patient_id,rezepturarzneimittel,	wirkstoff,thc_konzentration_mg_ml,tagesdosis_thc_mg,gleichbleibende_tagesdosis,therapiedauer,	tagesdosis_cbd,total_summe,total_cannaxan_thc,verordnete_anzahl,rechnerische_restmenge');
    	$this->db->where($this->table.'.id' , $id);
    	$this->db->join($this->tbl_patient_thherpieplan_folgeverord, $this->tbl_patient_thherpieplan_folgeverord.'.patient_id ='. $this->table. '.id');
    	$query = $this->db->get($this->table);
    	//pr($this->db->last_query()); die;
    	return $query->row();
    }

    public function getPaitentfolgeverordDataByPatientId($id)
	{
        $this->db->select($this->tbl_patient_thherpieplan_folgeverord_option.'.*');
        $this->db->where($this->tbl_patient_thherpieplan_folgeverord_option.'.patient_thherpieplan_folgeverord_id',$id); 
        $query = $this->db->get($this->tbl_patient_thherpieplan_folgeverord_option);
        return $query->result();
	}

	public function getarztlicherTherapieplandata()
	{   
		$this->db->select($this->arztlicher_therapieplan.'.*');
		//$this->db->where($this->arztlicher_therapieplan.'.id','$id');
		$this->db->order_by('add_date', 'DESC');
		$query = $this->db->get($this->arztlicher_therapieplan);
		return $query->result();
	}

	public function getumstellungData()
	{   
		$this->db->select($this->patient_therapy_plan_umstellung.'.*');
		//$this->db->where($this->arztlicher_therapieplan.'.id','$id');
		$this->db->order_by('add_date', 'DESC');
		$query = $this->db->get($this->patient_therapy_plan_umstellung);
		return $query->result();
	}

	public function getfolgeverordData()
	{   
		$this->db->select($this->patient_thherpieplan_folgeverord.'.*');
		//$this->db->where($this->arztlicher_therapieplan.'.id','$id');
		$this->db->order_by('add_date', 'DESC');
		$query = $this->db->get($this->patient_thherpieplan_folgeverord);
		return $query->result();
	}

	public function add_doctorsid($data)
	{   
		$data['add_date'] = getDefaultToGMTDate(time());
		$this->db->insert($this->tbl_doctors_id,$data);
        //return $this->db->insert_id();
	}

	function countPatientsByDoctorId($doctor_id) {

		$this->db->where('doctor_id', $doctor_id);
		$query = $this->db->get($this->tbl_doctors_id);
		return $query->num_rows();
	}

	function checkOtherDoctorPatientById($patient_id) {
		
		$this->db->where('patient_id', $patient_id);
		$query = $this->db->get($this->tbl_doctors_id);
		return $query->row();	
	}

	public function icd_code_zuordnung() 
	{
		$query = $this->db->get($this->icd_code_zuordnung);
		return $query->result();
	}

	public function getCannabisMedikament() 
	{
		$query = $this->db->get($this->cannabis_medikament);
		return $query->result();
	}
}

?>


