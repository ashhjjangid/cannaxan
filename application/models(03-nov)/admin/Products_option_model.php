<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Crew_model class.
 * 
 * @extends CI_Controller
 */

class Products_option_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_products';
        $this->table_products_opt = 'tbl_products_options';
        $this->table_product_varient_option =  'tbl_products_varients_options';
    }

   /**
    * count_product_option function.
    * 
    * @access public
    * @param mixed $seacrhData as array
    * @return count of crew type of users
    */
    public function count_product_option($searchData)
    {
        $this->db->select($this->table_products_opt.'.*');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table_products_opt.'.option_name', $searchData['keyword']);
            $this->db->or_like($this->table_products_opt.'.default_value', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table_products_opt.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table_products_opt.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }
        
        $query  = $this->db->get($this->table_products_opt);
        $result = $query->num_rows();
        return $result;
    }

   /**
    * get_product_option function.
    * 
    * @access public
    * @return records of all Crews
    */

    public function get_product_option($searchData) {

        $this->db->select($this->table_products_opt.'.*');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table_products_opt.'.option_name', $searchData['keyword']);
            $this->db->or_like($this->table_products_opt.'.default_value', $searchData['keyword']);
            //$this->db->or_like($this->table.'.email', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table_products_opt.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table_products_opt.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }

        if (isset($searchData) && $searchData['limit']) {
            $this->db->limit($searchData['limit'], $searchData['search_index']);
        }

        $query = $this->db->get($this->table_products_opt);
        $result = $query->result();
        return $result;
    }

   /**
    * getActiveCrew function.
    * 
    * @access public
    * @return result of all Active Crew
    */

    public function get_products_for_options() {
        $this->db->select($this->table_products_opt.'.*');
        //$this->db->where($this->table.'.user_type', 'Crew');
        //$this->db->where($this->table.'.status', 'Active');
        $query = $this->db->get($this->table_products_opt);
        $result = $query->result();
        return $result;
    }

   /**
    * getCrewsById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of crew type of user
    */

    public function get_options_by_id($id) {
        $this->db->where($this->table_products_opt.'.id',$id);
        $this->db->select($this->table_products_opt.'.*');
        //$this->db->where($this->table.'.user_type','Crew');
        $query = $this->db->get($this->table_products_opt);
        return $query->row();
    }

    public function getCrewsById($id) {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id',$id);
        $this->db->where($this->table.'.user_type','Crew');
        $query = $this->db->get($this->table);
        return $query->row();
    }
   /**
    * getSkipperCrewById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of skipper crew
    */

    public function get_product_option_by_id($id) {
        $this->db->select($this->table_products_opt.'.*');
        $this->db->select($this->table_product_varient_option.'.*');
        //$this->db->where($this->table_product_varient_option.'.id',$id);
        //$this->db->where($this->table.'.user_type','Crew');
        $this->db->join($this->table_products_opt, $this->table_products_opt.'.id = '.$this->table_product_varient_option.'.varient_id');

        $query = $this->db->get($this->table_products_opt);
        return $query->row();

    }
    
   /**
    * get_product_option_by_id function.
    * 
    * @access public
    * @param mixed $skipper_id
    * @return result of skipper crew
    */

    public function getSkipperCrewById()
    {   
        $this->db->select($this->table.'.*');
        $this->db->select($this->table_products_opt.'.*');
        //$this->db->where($this->table_products_opt.'.product_id',$skipper_id);
        $this->db->join($this->table, $this->table.'.id = '.$this->table_products_opt.'.product_id');
        $query = $this->db->get($this->table_products_opt);
        $result = $query->result();
        return $result;
    }

   /**
    * is_product_option_already_exists function.
    * 
    * @access public
    * @param mixed $skipper_id
    * @param mixed $Crew_id
    * @return number of rows
    */
   
    public function is_product_option_already_exists($product_id,$option_id) {
        $this->db->select($this->table_products_opt.'.id');
        $this->db->where($this->table_products_opt.'.product_id',$product_id);
        $this->db->where($this->table_products_opt.'.id',$option_id);
        $query = $this->db->get($this->table_products_opt);
        return $query->num_rows();
    }

   /**
    * deleteCrewById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function delete_product_varient_option_by_id($id) {
       $this->db->where('id',$id); 
       $this->db->delete($this->table_product_varient_option); 
    }

   /**
    * delete_product_option_by_id function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function delete_product_option_by_id($id) {
       $this->db->where('id',$id); 
       $this->db->delete($this->table_products_opt); 
    }

   /**
    * addCrew function.
    * 
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    public function add_new_option($data) {

        /*$data['add_date'] = getDefaultToGMTDate(time());
        $data['user_type'] = 'Crew';*/
        $this->db->insert($this->table_products_opt,$data);
        return $this->db->insert_id();
    }

   /**
    * add_product_option function.
    * 
    * @access public
    * @param mixed $data as array
    * @return null
    */

    public function add_product_option($data) {
        //$data['add_date'] = getDefaultToGMTDate(time());
        $this->db->insert($this->table_products_opt,$data);
        return $this->db->insert_id();        
    }

   /**
    * setCrewById function.
    * 
    * @access public
    * @param mixed $data as array
    * @param mixed $id
    * @return true or false
    */

    public function setCrewById($id,$data) {
        $data['update_date'] = getDefaultToGMTDate(time());

        if (isset($data['image']) && $data['image']) {
            $this->removeFileById($id);
        }
        
        $this->db->where('id',$id);
        $this->db->update($this->table,$data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * changeStatusById function.
    * 
    * @access public
    * @param mixed $status
    * @param mixed $id
    * @return true or false
    */

    public function changeStatusById($id, $status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * changeStatusBySkipperCrewId function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

    public function changeStatusBySkipperCrewId($id,$status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table_products_opt);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }


   /**
    * doesEmailExist function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $id
    * @return true or false
    */

    public function doesEmailExist($email = '', $id = '') {
        $this->db->select($this->table.'.id');
        $this->db->where($this->table.'.id!=',$id);
        $this->db->where($this->table.'.email',$email);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

   /**
    * doesUsernameExist function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $id
    * @return true or false
    */

    public function doesUsernameExist($username = '', $id = '') {
        $this->db->select($this->table.'.id');
        $this->db->where($this->table.'.id!=',$id);
        $this->db->where($this->table.'.username',$username);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    /**
    * removeFileById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function removeFileById($id)
    {
        $users = $this->getCrewsById($id);

        if ($users->image) {
            $path = './assets/uploads/crew/profile/';
            unlink($path . $users->image);
        }
    }    
    
}
