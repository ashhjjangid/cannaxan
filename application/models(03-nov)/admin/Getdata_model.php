<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**Hunters_model
 */
class Getdata extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_patients';
		$this->icd_code_zuordnung = 'tbl_icd_code_zuordnung';
		$this->cannabis_medikament = 'tbl_cannabis_medikament';
		$this->patient_begleiterkrankungen = 'tbl_patient_begleiterkrankungen';
		$this->zuordnung_sps = 'tbl_zuordnung_sps';
		$this->dosistyp_1 = 'tbl_dosistyp_1';
		$this->dosistyp_2 = 'tbl_dosistyp_2';
		$this->patient_typ_umstellung = 'tbl_patient_typ_umstellung';
		$this->patient_therapy_plan_umstellung = 'tbl_patient_therapy_plan_umstellung';
		$this->thherpieplan_folgeverord = 'tbl_thherpieplan_folgeverord';
		$this->patient_thherpieplan_folgeverord = 'tbl_patient_thherpieplan_folgeverord';
		$this->patient_thherpieplan_folgeverord_option = 'tbl_patient_thherpieplan_folgeverord_option';
		
		$this->arztlicher_therapieplan = 'tbl_arztlicher_therapieplan';
		$this->patient_dosistyp_1 = 'tbl_patient_dosistyp_1';
		$this->patient_dosistyp_2 = 'tbl_patient_dosistyp_2';
	}

	public function getdataumstellungByid()
	{
		$this->db->select($this->table.'.*');
        /*$this->db->select($this->tbl_image.'.*');
        $this->db->where($this->table.'.id',$id); 
        $this->db->join($this->tbl_image,$this->tbl_image.'.deals_id='.$this->table.'.id');
        $this->db->where($this->table.'.is_delete','No');*/
        $query = $this->db->get($this->patient_therapy_plan_umstellung);
        return $query->result_array();
	}
	
}