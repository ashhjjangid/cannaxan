<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Products_model class.
 * 
 * @extends CI_Controller
 */

class Products_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_products';
    }
    
   /**
    * countSkipper function.
    * 
    * @access public
    * @param mixed $seacrhData as array
    * @return count of all user records
    */

    public function countproducts($searchData)
    {
        $this->db->select($this->table.'.*');
        //$this->db->where($this->table.'.user_type', 'Skipper');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.product_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.email', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }
        
        $query  = $this->db->get($this->table);
        $result = $query->num_rows();
        return $result;
    }


   /**
    * getSkipper function.
    * 
    * @access public
    * @return records of all skippers
    */

    public function getproducts($searchData=false) {
        $this->db->select($this->table.'.*');

        if (isset($searchData) && $searchData['keyword']) {
            $this->db->group_start();
            $this->db->or_like($this->table.'.product_name', $searchData['keyword']);
            $this->db->or_like($this->table.'.email', $searchData['keyword']);
            $this->db->group_end();
        }

        if (isset($searchData) && $searchData['status']) {
            $this->db->where($this->table.'.status', $searchData['status']);
        }

        if (isset($searchData) && $searchData['sorting_order']) {
            $this->db->order_by($this->table.'.' . $searchData['column_name'], $searchData['sorting_order']);

        }

        if (isset($searchData) && $searchData['limit']) {
            $this->db->limit($searchData['limit'], $searchData['search_index']);
        }

        $query = $this->db->get($this->table);
        $result = $query->result();
        //pr($this->db->last_query());
        return $result;
    }

   /**
    * getSkipperById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of user
    */

    public function get_products_by_id($id) {
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id',$id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

   /**
    * deleteSkipperById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function deleteSkipperById($id) {
       $this->db->delete($this->table, array('id' => $id)); 
    }

   /**
    * addSkipper function.
    * 
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    public function addproducts($data) {
        $data['add_date'] = getDefaultToGMTDate(time());

        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

   /**
    * setSkipperById function.
    * 
    * @access public
    * @param mixed $data as array
    * @param mixed $id
    * @return true or false
    */

    public function set_product_by_id($id,$data) {
        //$data['update_date'] = getDefaultToGMTDate(time());

        $this->db->where('id',$id);
        $this->db->update($this->table,$data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * changeStatusById function.
    * 
    * @access public
    * @param mixed $status
    * @param mixed $id
    * @return true or false
    */

    public function changeStatusById($id, $status) {
        $this->db->where('id',$id);
        $this->db->set('status', $status);
        $this->db->update($this->table);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * doesEmailExist function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $id
    * @return true or false
    */

    public function doesEmailExist($email = '', $id = '') {
        $this->db->select($this->table.'.id');
        $this->db->where($this->table.'.id!=',$id);
        $this->db->where($this->table.'.email',$email);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

   /**
    * doesUsernameExist function.
    * 
    * @access public
    * @param mixed $email
    * @param mixed $id
    * @return true or false
    */

    public function doesUsernameExist($username, $id) {
        $this->db->select($this->table.'.id');
        $this->db->where($this->table.'.id!=',$id);
        $this->db->where($this->table.'.username',$username);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }


    /**
    * removeFileById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    public function removeFileById($id)
    {
        $users = $this->getSkipperById($id);

        if ($users->image) {
            $path = './assets/uploads/skipper/profile/';
            unlink($path . $users->image);
        }
    }     
    
}