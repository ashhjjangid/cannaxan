<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Scooter_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->scooter = 'tbl_scooter';
		$this->scooter_image = 'tbl_scooter_image';
		$this->hunter = 'tbl_user';
		$this->scooter_charging = 'tbl_charging_scooter';
		$this->rides = 'tbl_rides';
	}

	public function count_scooter($searchdata)
	{
		$this->db->select($this->scooter.'.*');

		if (isset($searchdata) && $searchdata['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->scooter.'.name', $searchdata['keyword']);
			$this->db->or_like($this->scooter.'.maker_name', $searchdata['keyword']);
			$this->db->or_like($this->scooter.'.year_of_made', $searchdata['keyword']);
			$this->db->group_end();
		}

		if (isset($searchdata) && $searchdata['status']) {
			$this->db->where($this->scooter.'.status', $searchdata['status']);
		}

		if (isset($searchdata) && $searchdata['sorting_order']) {
			$this->db->order_by($this->scooter.'.'. $searchdata['column_name'], $searchdata['sorting_order']);
		}

		$query = $this->db->get($this->scooter);
		return $query->num_rows();
	}

	public function get_scooters($searchdata)
	{
		$this->db->select($this->scooter.'.*');

		if (isset($searchdata) && $searchdata['keyword']) {
			$this->db->group_start();
			$this->db->or_like($this->scooter.'.name', $searchdata['keyword']);
			$this->db->or_like($this->scooter.'.maker_name', $searchdata['keyword']);
			$this->db->or_like($this->scooter.'.year_of_made', $searchdata['keyword']);
			$this->db->group_end();
		}

		if (isset($searchdata) && $searchdata['status']) {
			$this->db->where($this->scooter.'.status', $searchdata['status']);
		}

		if (isset($searchdata) && $searchdata['sorting_order']) {
			$this->db->order_by($this->scooter.'.'. $searchdata['column_name'], $searchdata['sorting_order']);
		}

		if (isset($searchdata) && $searchdata['limit']) {
            $this->db->limit($searchdata['limit'], $searchdata['search_index']);
        }

		$query = $this->db->get($this->scooter);
		return $query->result();
	}

	public function get_all_scooters()
	{
		$this->db->select($this->scooter.'.*');
		$query = $this->db->get($this->scooter);
		return $query->result();
	}

	public function add_new_scooter($data)
	{
		$data['add_date'] = getDefaultToGMTDate(time());
		//$this->db->select($this->scooter.'.*');
		$this->db->insert($this->scooter, $data);
		return $this->db->insert_id();
	}

	public function add_scooter_image($data)
	{
		$data['add_date'] = getDefaultToGMTDate(time());
		//$this->db->select($this->scooter_image.'.*');
		$this->db->insert($this->scooter_image, $data);
		return $this->db->insert_id();
	}

	public function get_scooter_by_id($id)
	{
		
		$this->db->select($this->scooter.'.*');
		$this->db->where('id=', $id);
		$query = $this->db->get($this->scooter);
		return $query->row();
	}

	public function get_image_by_id($id)
	{
		$this->db->where($this->scooter_image.'.scooter_id=', $id);
		$query = $this->db->get($this->scooter_image);
		return $query->row();
	}

	public function update_scooter_details($id, $data)
	{
		$data['update_date'] = getDefaultToGMTDate(time());
		//$this->db->select($this->scooter.'.*');
		$this->db->where('id=', $id);
		$this->db->update($this->scooter, $data);
		if ($this->db->affected_rows()) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	public function update_scooter_image($id, $data)
	{		
		$this->db->select($this->scooter_image.'.*');

		if (isset($data['image_name']) && $data['image_name']) 
		{
			$this->remove_old_file($id);
		}
		$this->db->where('scooter_id=', $id);
		$this->db->update($this->scooter_image, $data);
		if ($this->db->affected_rows()) 
		{
			return true;
		} else
		{
			return false;
		}
	}

	public function remove_old_file($id)
	{
		$this->db->select($this->scooter_image.'.*');
		$imagedata = $this->get_image_by_id($id);
		$images = explode(',', $imagedata->image_name);
		//pr($images); die;
		if ($images) 
		{	
			foreach ($images as $value) 
			{					
				$path = './assets/uploads/scooter_images/';
				unlink($path.$value);
			}
		}
	}

	public function view_scooter_details($id)
	{
		$this->db->select($this->scooter.'.*');
		$this->db->select($this->scooter_image.'.scooter_id, image_name');
		$this->db->where($this->scooter.'.id=', $id);
		$this->db->join($this->scooter_image, $this->scooter_image.'.scooter_id='. $this->scooter.'.id');
		$query = $this->db->get($this->scooter);
		return $query->row();
	}

	public function delete_scooter($id)
	{		
		$image_data = $this->get_image_by_id($id);
		//pr($image_data); die;
		if(isset($image_data->scooter_id) && $image_data->scooter_id)
		{
			//pr($image_data->id);
			$this->remove_old_file($id);
			$this->delete_scooter_image($id);
		}
		$this->delete_scooter_from_charging($id);
		$this->db->where($this->scooter.'.id=', $id);
		$this->db->delete($this->scooter);
		if ($this->db->affected_rows()) 
		{
			return true;
		} else {
			return false;
		}

	}

	public function delete_scooter_image($id)
	{
		$this->db->select($this->scooter_image.'.*');
		$this->db->where('scooter_id', $id);
		$this->db->delete($this->scooter_image);
		if ($this->db->affected_rows()) 
		{
			return true;
		} else {
			return false;
		}
	}

	public function change_status_by_id($id, $status)
	{
		$this->db->select($this->scooter.'.*');
		$this->db->where('id=', $id);
		$this->db->set('status', $status);
		$query = $this->db->update($this->scooter);
		if ($this->db->affected_rows()) 
		{
			return true;
		} else {
			return false;
		}
	}

	public function list_scooters_for_charging()
	{
		$this->db->select($this->scooter_charging.'.*');
		$this->db->select($this->scooter.'.id, maker_name, name');
		$this->db->select($this->hunter.'.id, first_name, last_name');
		$this->db->where($this->scooter_charging.'.status=','Charging');
		$this->db->join($this->scooter, $this->scooter.'.id='. $this->scooter_charging.'.scooter_id');
		$this->db->join($this->hunter, $this->hunter.'.id='. $this->scooter_charging.'.hunter_id');
		$query = $this->db->get($this->scooter_charging);
		//pr($query); die;
		return $query->result();
	}

	public function view_scooters_for_charging($charging_id)
	{
		$this->db->select($this->scooter_charging.'.*');
		$this->db->select($this->scooter.'.id, maker_name, name');
		$this->db->select($this->hunter.'.id, first_name, last_name');		
		$this->db->where($this->scooter_charging.'.charging_id', $charging_id);
		$this->db->join($this->hunter, $this->hunter.'.id='. $this->scooter_charging.'.hunter_id');
		$this->db->join($this->scooter, $this->scooter.'.id='. $this->scooter_charging.'.scooter_id');
		$query = $this->db->get($this->scooter_charging);

		return $query->row();
	}

	public function list_scooters_full_charged()
	{
		$this->db->select($this->scooter_charging.'.*');
		$this->db->select($this->scooter.'.id, maker_name, name');
		$this->db->select($this->hunter.'.id, first_name, last_name');
		$this->db->where($this->scooter_charging.'.status=','Fully Charged');
		$this->db->join($this->scooter, $this->scooter.'.id='. $this->scooter_charging.'.scooter_id');
		$this->db->join($this->hunter, $this->hunter.'.id='. $this->scooter_charging.'.hunter_id');
		$query = $this->db->get($this->scooter_charging);
		//pr($query); die;
		return $query->result();
	}

	public function view_scooters_charged($charging_id)
	{
		$this->db->select($this->scooter_charging.'.*');
		$this->db->select($this->scooter.'.id, maker_name, name');
		$this->db->select($this->hunter.'.id, first_name, last_name');		
		$this->db->where($this->scooter_charging.'.charging_id', $charging_id);
		$this->db->join($this->hunter, $this->hunter.'.id='. $this->scooter_charging.'.hunter_id');
		$this->db->join($this->scooter, $this->scooter.'.id='. $this->scooter_charging.'.scooter_id');
		$query = $this->db->get($this->scooter_charging);

		return $query->row();
	}

	public function list_reserved_scooters()
	{
		$this->db->select($this->rides.'.*');
		$this->db->select($this->scooter.'.id, maker_name, name');
		$this->db->select($this->hunter.'.id, first_name, last_name');
		$this->db->where('is_reserved', 'Yes');
		$this->db->join($this->scooter, $this->scooter.'.id='.$this->rides.'.scooter_id');
		$this->db->join($this->hunter, $this->hunter.'.id=' . $this->rides.'.user_id');
		$query = $this->db->get($this->rides);
		return $query->result();
	}

	public function view_scooters_reserved($ride_id)
	{
		$this->db->select($this->rides.'.*');
		$this->db->select($this->scooter.'.id, maker_name, name');
		$this->db->select($this->hunter.'.id, first_name, last_name');
		$this->db->where('is_reserved=', 'Yes');
		$this->db->where($this->rides.'.ride_id=', $ride_id);
		$this->db->join($this->scooter, $this->scooter.'.id='.$this->rides.'.scooter_id');
		$this->db->join($this->hunter, $this->hunter.'.id=' . $this->rides.'.user_id');
		$query = $this->db->get($this->rides);
		
		return $query->row();
	}

	public function delete_scooter_from_charging($id)
	{
		//pr($id);die('here');
		$this->db->select($this->scooter_charging.'.*');
		$this->db->where($this->scooter_charging.'.charging_id=', $id);
		$this->db->delete($this->scooter_charging);
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
}