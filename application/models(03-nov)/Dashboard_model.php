<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_members';
		$this->patients = 'tbl_patients';
	}

	public function getuserById($user_id)
	{
		$this->db->where('id', $user_id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	function getPatientByInsuranceNumber($insured_number) {

		$this->db->where('insured_number', $insured_number);
		$query = $this->db->get($this->patients);
		return $query->row();
	}
}

?>