<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**Hunters_model
 */
class Patients_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_patients';
		$this->icd_code_zuordnung = 'tbl_icd_code_zuordnung';
		$this->cannabis_medikament = 'tbl_cannabis_medikament';
		$this->patient_begleiterkrankungen = 'tbl_patient_begleiterkrankungen';
		$this->zuordnung_sps = 'tbl_zuordnung_sps';
		$this->dosistyp_1 = 'tbl_dosistyp_1';
		$this->dosistyp_2 = 'tbl_dosistyp_2';
		$this->patient_typ_umstellung = 'tbl_patient_typ_umstellung';
		$this->patient_therapy_plan_umstellung = 'tbl_patient_therapy_plan_umstellung';
		$this->thherpieplan_folgeverord = 'tbl_thherpieplan_folgeverord';
		$this->patient_thherpieplan_folgeverord = 'tbl_patient_thherpieplan_folgeverord';
		$this->patient_thherpieplan_folgeverord_option = 'tbl_patient_thherpieplan_folgeverord_option';
		
		$this->arztlicher_therapieplan = 'tbl_arztlicher_therapieplan';
		$this->patient_dosistyp_1 = 'tbl_patient_dosistyp_1';
		$this->patient_dosistyp_2 = 'tbl_patient_dosistyp_2';
		$this->tbl_doctors_id = 'tbl_doctors_id'; 
	}

	function getPatientByInsuranceNumber($insured_number) {
		$this->db->where('insured_number', $insured_number);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function icd_code_zuordnung() 
	{
		$query = $this->db->get($this->icd_code_zuordnung);
		return $query->result();
	}
	public function add_patient($data)
	{
		$data['add_date'] = getDefaultToGMTDate(time());
		// $this->db->select($this->table.'*');
		$query = $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update_patient($id, $data)
	{
		$data['update_date'] = getDefaultToGMTDate(time());
		$this->db->where('id', $id);
		$query = $this->db->update($this->table, $data);
		
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	public function get_patients_by_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function getPatientByInsuredNumber($insured_number)
	{
		$this->db->where('insured_number', $insured_number);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function getCannabisMedikament() 
	{
		$query = $this->db->get($this->cannabis_medikament);
		return $query->result();
	}

	public function add_begleiterkrankungen($data, $patient_id)
	{
		$this->delete_begleiterkrankungen($patient_id);
		$this->db->insert_batch($this->patient_begleiterkrankungen, $data);
	}

	public function delete_begleiterkrankungen($patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->delete($this->patient_begleiterkrankungen);
	}

	public function getPatientBegleiterkrankungen($patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$query = $this->db->get($this->patient_begleiterkrankungen);
		return $query->result();
	}

	public function getZuordnungSps() 
	{
		$query = $this->db->get($this->zuordnung_sps);
		return $query->row();
	}

	public function getDosistyp_1()
	{
		$query = $this->db->get($this->dosistyp_1);
		return $query->result();	
	}

	public function arztlicher_therapieplan_table($arztlicher_therapieplan)
	{
		$this->db->where('display_order <=', $arztlicher_therapieplan);
		$query = $this->db->get('tbl_arztlicher_therapieplan_table');
		return $query->result();	
	}

	public function getDosistyp_2()
	{
		$query = $this->db->get($this->dosistyp_2);
		return $query->result();	
	}

	public function getThherpieplanFolgeverord()
	{
		$query = $this->db->get($this->thherpieplan_folgeverord);
		return $query->result();	
	}

	public function getPatientTherapyPlanUmstellung($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->patient_therapy_plan_umstellung);
		return $query->row();
	}

	public function	getPatientTypUmstellung($patient_therapy_plan_umstellung_id)
	{
		$this->db->where('patient_therapy_plan_umstellung_id', $patient_therapy_plan_umstellung_id	);
		//$this->db->limit('4');
		$this->db->order_by('id', 'ASC');
		//pr($this->db->last_query()); die;
		$query = $this->db->get($this->patient_typ_umstellung);
		return $query->result();
	}


	public function updatePatientTherapyPlanUmstellung($id, $data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update($this->patient_therapy_plan_umstellung, $data);
		
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function addPatientTherapyPlanUmstellung($data)
	{
		$this->db->insert($this->patient_therapy_plan_umstellung, $data);
		return $this->db->insert_id();
	}

	public function	deletePatientTypUmstellung($patient_therapy_plan_umstellung_id)
	{
		$this->db->where('patient_therapy_plan_umstellung_id', $patient_therapy_plan_umstellung_id);
		$this->db->delete($this->patient_typ_umstellung);
	}

	public function addPatientTypUmstellung($data)
	{
		$this->db->insert($this->patient_typ_umstellung, $data);
		return $this->db->insert_id();
	}

	public function getPatientThherpieplanFolgeverord($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->patient_thherpieplan_folgeverord);
		return $query->row();
	}


	public function addPatientThherpieplanFolgeverord($data)
	{
		$this->db->insert($this->patient_thherpieplan_folgeverord, $data);
		return $this->db->insert_id();
	}

	public function updatePatientThherpieplanFolgeverord($id, $data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update($this->patient_thherpieplan_folgeverord, $data);
		
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function getPatientThherpieplanFolgeverordOption($patient_thherpieplan_folgeverord_id)
	{
		$this->db->where('patient_thherpieplan_folgeverord_id', $patient_thherpieplan_folgeverord_id);
		$query = $this->db->get($this->patient_thherpieplan_folgeverord_option);
		return $query->result();
	}

	public function addPatientThherpieplanFolgeverordOption($data)
	{
		$this->db->insert($this->patient_thherpieplan_folgeverord_option, $data);
		return $this->db->insert_id();
	}

	public function	deletePatientThherpieplanFolgeverordOption($patient_thherpieplan_folgeverord_id)
	{
		$this->db->where('patient_thherpieplan_folgeverord_id', $patient_thherpieplan_folgeverord_id);
		$this->db->delete($this->patient_thherpieplan_folgeverord_option);
	}

	public function addPatientArztlicherTherapieplan($data)
	{
		$this->db->insert($this->arztlicher_therapieplan, $data);
		return $this->db->insert_id();
	}


	public function getPatientArztlicherTherapieplan($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->arztlicher_therapieplan);
		return $query->row();
	}

	public function getPatientDosistyp_1($arztlicher_therapieplan_id)
	{
		$this->db->where('arztlicher_therapieplan_id', $arztlicher_therapieplan_id);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get($this->patient_dosistyp_1);
		return $query->result();
	}

	public function getPatientDosistyp_2($arztlicher_therapieplan_id)
	{
		$this->db->where('arztlicher_therapieplan_id', $arztlicher_therapieplan_id);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get($this->patient_dosistyp_2);
		return $query->result();
	}


	public function addPatientDosistyp_1($data)
	{
		$this->db->insert($this->patient_dosistyp_1, $data);
		return $this->db->insert_id();
	}

	public function addPatientDosistyp_2($data)
	{
		$this->db->insert($this->patient_dosistyp_2, $data);
		return $this->db->insert_id();
	}

	public function getPatientandDoctorTherapieplan($patient_id, $doctor_id)
	{
	
		$this->db->where('patient_id', $patient_id);
		$this->db->where('doctor_id', $doctor_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->arztlicher_therapieplan);
		return $query->row();
	}
	public function add_doctorsid($data)
	{   
		$data['add_date'] = getDefaultToGMTDate(time());
		$this->db->insert($this->tbl_doctors_id,$data);
        //return $this->db->insert_id();
	}

	function checkDoctorPatientByIds($doctor_id, $patient_id) {

		$this->db->where('doctor_id', $doctor_id);
		$this->db->where('patient_id', $patient_id);
		$query = $this->db->get($this->tbl_doctors_id);
		return $query->row();
	}

	function checkOtherDoctorPatientById($patient_id) {
		
		$this->db->where('patient_id', $patient_id);
		$query = $this->db->get($this->tbl_doctors_id);
		return $query->row();	
	}

	function countPatientsByDoctorId($doctor_id) {

		$this->db->where('doctor_id', $doctor_id);
		$query = $this->db->get($this->tbl_doctors_id);
		return $query->num_rows();
	}

	function getMainTypUmstellung($umstellung_medikament_id, $tagodosis){
		$this->db->where('umstellung_medikament_id', $umstellung_medikament_id);
		$this->db->where('min_value <=', $tagodosis);
		$this->db->where('max_value >=', $tagodosis);
		$query = $this->db->get('tbl_type_umstellung_medikament_1');
		return $query->result();
	}
}