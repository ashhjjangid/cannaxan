<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Area_model extends CI_Model {
	
	function __construct() {
		parent::__construct();       
	}

	public function get_area() {
		$this->db->select('*');
		$query = $this->db->get('tbl_area_escooter');
		return $query->result();
	}

	public function get_marker_by_area_id($area_id) {
		$this->db->where('area_id', $area_id);
		$query = $this->db->get('tbl_area_marker');
		return $query->result();
	}


}