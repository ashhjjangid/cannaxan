<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctors_model extends CI_Model {
	function __construct(){
		parent::__construct();  
		$this->doctors = 'tbl_members';
	}

	function loginCredentials($email, $password) {

		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$this->db->where('is_approved', 'Yes');
		$this->db->where('is_register_completed', 'Yes');
		$query = $this->db->get($this->doctors);
		return $query->row();
	}

	function registerNewDoctor($data) {
		$query = $this->db->insert($this->doctors, $data);
		return $this->db->insert_id();
	}

	function getUserByEmail($email) {

		$this->db->where('email', $email);
		$this->db->where('is_approved', 'Yes');
		$query = $this->db->get($this->doctors);
		return $query->row();
	}

	function getUserById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->doctors);
		return $query->row();
	}

	function getUserBypasswordkey($setpasswordkey) {
		$this->db->where('set_password_key', $setpasswordkey);
		$query = $this->db->get($this->doctors);
		return $query->row();
	}

	function setNewPassword($id, $password) {
		
	    $this->db->where('id', $id);	
	    $this->db->set('password', $password);
	    $this->db->update($this->doctors);
	      
	    if ($this->db->affected_rows()) {
	        return true;
	    } else {
	        return false;
	    }
	}

	function setNewDoctorPassword($id, $data) {
		
	    $this->db->where('id', $id);	
	    $this->db->update($this->doctors, $data);
	      
	    if ($this->db->affected_rows()) {
	        return true;
	    } else {
	        return false;
	    }
	}

	function updateDoctor($id, $data) {
		
	    $this->db->where('id', $id);	
	    $this->db->update($this->doctors, $data);
	      
	    if ($this->db->affected_rows()) {
	        return true;
	    } else {
	        return false;
	    }
	}

	function getDoctorRecordById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->doctors);
		return $query->row();
	}
	function getDoctorByVerificationKey($verification_key) {
		$this->db->where('verification_key', $verification_key);
		$query = $this->db->get($this->doctors);
		return $query->row();
	}
}