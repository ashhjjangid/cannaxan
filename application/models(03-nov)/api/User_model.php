<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	function __construct(){
		parent::__construct();  
		$this->patients = 'tbl_patients';  
		$this->questions = 'tbl_patient_questions'; 
		$this->patients_weitere_cannaxan_dosis = 'tbl_patients_weitere_cannaxan_dosis'; 
		$this->apothekensuche_and_zum_cannaxan_bezug = 'tbl_apothekensuche_and_zum_cannaxan_bezug'; 
		$this->apotheken_zum_bezug_des_rezepturarzneimittels = 'tbl_apothekensuche_and_zum_cannaxan_bezug';
		$this->begleitmedikation = 'tbl_begleitmedikation';
		$this->begleitmedikation_1 = 'tbl_begleitmedikation_1';
	}

	function getLastVisitNumber($patient_id, $question_id){
		$this->db->select('visit_number');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('id !=', $question_id);
		$this->db->where('is_process_complete', 'Yes');
		$this->db->order_by('visit_number', 'DESC');
		$query = $this->db->get($this->questions);
		return $query->row();
	}

	function getLoginUserDetails($insurance_number, $date_of_birth) {

		$this->db->where('insured_number', $insurance_number);
		$this->db->where('date_of_birth', $date_of_birth);
		$query = $this->db->get($this->patients);
		return $query->row();
	}

	function update_user($id, $data) {

		$this->db->where('id', $id);
		$this->db->update($this->patients, $data);
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function getRecordByToken($token) {
		
		$this->db->where('token', $token);
		$query = $this->db->get($this->patients);
		return $query->row();
	}

	function getUserByToken($token) {
		
		$this->db->where('token', $token);
		$query = $this->db->get($this->patients);
		return $query->row_array();
	}

	function check_patient_questions_exists($id) {

		$this->db->where('patient_id', $id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->questions);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function check_patient_cannaxan_dosis_questions_exists($question_id) {

		$this->db->where('patient_question_id', $question_id);
		$query = $this->db->get($this->patients_weitere_cannaxan_dosis);
		return $query->result_array();
	}

	function addPatientQuestionAnswered($data) {

		$this->db->insert($this->questions, $data);
		return $this->db->insert_id();
	}

	function updatePatientQuestionAnswered($id, $data) {

		$this->db->where('patient_id', $id);
		$this->db->update($this->questions, $data);
		//pr($this->db->last_query()); die;
		return true;
	}

	function userQuestionsexists($id, $data) {

		$this->db->where('patient_id', $id);
		$this->db->where($data);
		$query = $this->db->get($this->questions);
		pr($this->db->last_query()); die;
		return $query->row_array();
	}
	
	function addNewCannaxanDosisforPatient($data) {

		//pr($data); die;

		// $this->db->insert_batch($this->patients_weitere_cannaxan_dosis, $data);
		$this->db->insert($this->patients_weitere_cannaxan_dosis, $data);
		return $this->db->insert_id();
	}

	function updatePatientCannaxanDosis($id, $data) {

		$this->db->where('patient_id');
		$this->db->update($this->patients_weitere_cannaxan_dosis, $data);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function deletePatientCannaxan_dosis($question_id) {

		$this->db->where('patient_question_id', $question_id);
		$this->db->delete($this->patients_weitere_cannaxan_dosis);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function getQuestionDetailById($question_id, $patient_id)
	{
		$this->db->where('id', $question_id);
		$this->db->where('patient_id', $patient_id);
		$query = $this->db->get($this->questions);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function getSecondLastQuestionDetail($question_id, $patient_id)
	{
		$this->db->where('is_process_complete', 'Yes');
		$this->db->where('id !=', $question_id);
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->questions);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function updateQuestionById($question_id, $data)
	{
		$this->db->where('id', $question_id);
		$this->db->update($this->questions, $data);
		
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function getLastQuestionDetailByPatientId($id) {

		$this->db->where('is_process_complete', 'Yes');
		$this->db->where('patient_id', $id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->questions);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function getPlzRecords() {
		$this->db->select($this->apotheken_zum_bezug_des_rezepturarzneimittels.'.plz');
		$this->db->group_by('plz');
		$query = $this->db->get($this->apotheken_zum_bezug_des_rezepturarzneimittels);
		return $query->result();
	}

	function getOrtByPlz($plz) {
		$this->db->select($this->apotheken_zum_bezug_des_rezepturarzneimittels.'.ort');
		$this->db->where('plz', $plz);
		$query = $this->db->get($this->apotheken_zum_bezug_des_rezepturarzneimittels);
		return $query->result();
	}

	function getStrabeByPlzandOrt($plz, $ort) {
		$this->db->select($this->apotheken_zum_bezug_des_rezepturarzneimittels.'.id, strabe');
		$this->db->where('plz', $plz);
		$this->db->where('ort', $ort);
		$query = $this->db->get($this->apotheken_zum_bezug_des_rezepturarzneimittels);
		//pr($this->db->last_query());
		return $query->row();
	}

	function getapothekenByid($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->apotheken_zum_bezug_des_rezepturarzneimittels);
		return $query->row();
	}

	/*function getbegleitmedikationDataByPatientId($user_id) {
		$this->db->select($this->begleitmedikation.'.*');
		$this->db->select($this->begleitmedikation_1.'.wriksoff');
		$this->db->where($this->begleitmedikation.'.patient_id', $user_id);
		$this->db->group_by($this->begleitmedikation.'.add_date');
		$this->db->join($this->begleitmedikation_1, $this->begleitmedikation_1.'.begleitmedikation_id =.'. $this->begleitmedikation.'.id');
		$query = $this->db->get($this->begleitmedikation);
		//pr($this->db->last_query()); die;
		return $query->result();
	}*/

	function getbegleitmedikationDataByPatientId($id) {
		$date = date('Y-m-d');
		$this->db->where('startdatum <=', $date);
		$this->db->group_start();
			$this->db->where('stopdatum >=', $date);
			$this->db->or_where('stopdatum', '');
			$this->db->or_where('stopdatum', null);
		$this->db->group_end();
		$this->db->where('patient_id', $id);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get($this->begleitmedikation_1);
		// $query = $this->db->get($this->begleitmedikation);
		return $query->result();
	}

	function getbegleitmedikation_1DataByPatientId($id) {
		$this->db->where('begleitmedikation_id', $id);
		$query = $this->db->get($this->begleitmedikation_1);
		return $query->result();
	}
}