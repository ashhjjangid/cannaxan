<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus_model extends CI_Model {
	function __construct(){
		parent::__construct();       
	}

	public function add_contact_queries($data=array()) 
	{
		$this->db->insert('tbl_contact_us',$data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}

	}

}