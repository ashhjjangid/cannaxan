<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->tbl_app_schmerzstarke = 'tbl_app_schmerzstarke';
		$this->tbl_app_keine_schmerzmittel = 'tbl_app_keine_schmerzmittel';
		$this->tbl_app_lebensqualitat_1 = 'tbl_app_lebensqualitat_1';
		$this->tbl_app_lebensqualitat_2 = 'tbl_app_lebensqualitat_2';
		$this->tbl_app_lebensqualitat_3 = 'tbl_app_lebensqualitat_3';
		$this->tbl_app_lebensqualitat_4 = 'tbl_app_lebensqualitat_4';
		$this->tbl_app_lebensqualitat_5 = 'tbl_app_lebensqualitat_5';
		$this->tbl_app_lebensqualitat_6 = 'tbl_app_lebensqualitat_6';
		$this->tbl_app_lebensqualitat_7 = 'tbl_app_lebensqualitat_7';
		$this->tbl_app_lebensqualitat_8 = 'tbl_app_lebensqualitat_8';
		$this->tbl_app_lebensqualitat_9 = 'tbl_app_lebensqualitat_9';
		$this->tbl_app_lebensqualitat_10 = 'tbl_app_lebensqualitat_10';
		$this->tbl_app_schlafqualitat_1 = 'tbl_app_schlafqualitat_1';
		$this->tbl_app_schlafqualitat_2 = 'tbl_app_schlafqualitat_2';
		$this->tbl_app_schlafqualitat_3 = 'tbl_app_schlafqualitat_3';
		$this->tbl_app_schlafqualitat_4 = 'tbl_app_schlafqualitat_4';
		$this->tbl_app_schlafqualitat_5 = 'tbl_app_schlafqualitat_5';
		$this->tbl_app_schlafqualitat_6 = 'tbl_app_schlafqualitat_6';
		$this->tbl_app_schlafqualitat_7 = 'tbl_app_schlafqualitat_7';
		$this->tbl_app_schlafqualitat_8 = 'tbl_app_schlafqualitat_8';
		$this->tbl_app_schlafqualitat_9 = 'tbl_app_schlafqualitat_9';
		$this->tbl_app_schlafqualitat_10 = 'tbl_app_schlafqualitat_10';
		$this->tbl_app_schlafqualitat_11 = 'tbl_app_schlafqualitat_11';
		$this->tbl_app_verlaufskontrolle = 'tbl_app_verlaufskontrolle';
		$this->tbl_app_weitere_einnahme = 'tbl_app_weitere_einnahme';
		$this->tbl_app_weitere_einnahme_content = 'tbl_app_weitere_einnahme_content';
		$this->tbl_app_weitere_schmerzmedikamente = 'tbl_app_weitere_schmerzmedikamente';
		$this->tbl_app_notification_reminder = 'tbl_app_notification_reminder';
		$this->tbl_app_schmerzstarke_1 = 'tbl_app_schmerzstarke_1';
		$this->tbl_app_schmerzstarke_2 = 'tbl_app_schmerzstarke_2';
		$this->arztlicher_therapieplan = 'tbl_arztlicher_therapieplan';
		$this->patient_dosistyp_1 = 'tbl_patient_dosistyp_1';
		$this->patient_dosistyp_2 = 'tbl_patient_dosistyp_2';
		$this->patient_typ_umstellung = 'tbl_patient_typ_umstellung';
		$this->patient_therapy_plan_umstellung = 'tbl_patient_therapy_plan_umstellung';
		$this->patient_thherpieplan_folgeverord = 'tbl_patient_thherpieplan_folgeverord';
		$this->patient_thherpieplan_folgeverord_option = 'tbl_patient_thherpieplan_folgeverord_option';
	}

	public function getPatientArztlicherTherapieplan($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->arztlicher_therapieplan);
		return $query->row();
	}

	public function getPatientDosistyp_1($arztlicher_therapieplan_id, $day)
	{
		$this->db->where('arztlicher_therapieplan_id', $arztlicher_therapieplan_id);
		$index = ($day - 1) * 1;
		$this->db->limit(1, $index);
		$this->db->order_by('id', 'asc');
		$query = $this->db->get($this->patient_dosistyp_1);
		return $query->row();
	}

	public function getPatientDosistyp_2($arztlicher_therapieplan_id, $day)
	{
		$this->db->where('arztlicher_therapieplan_id', $arztlicher_therapieplan_id);
		$this->db->order_by('id', 'asc');
		$index = ($day - 1) * 1;
		$this->db->limit(1, $index);
		$query = $this->db->get($this->patient_dosistyp_2);
		return $query->row();
	}

	public function getPatientTherapyPlanUmstellung($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->patient_therapy_plan_umstellung);
		return $query->row();
	}

	public function	getPatientTypUmstellung($patient_therapy_plan_umstellung_id)
	{
		$this->db->where('patient_therapy_plan_umstellung_id', $patient_therapy_plan_umstellung_id);
		$this->db->order_by('id', 'ASC');
		/*$index = ($day - 1) * 1;
		$this->db->limit(1, $index);*/
		$query = $this->db->get($this->patient_typ_umstellung);
		return $query->row();
	}

	public function getPatientThherpieplanFolgeverord($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->patient_thherpieplan_folgeverord);
		return $query->row();
	}

	public function getPatientThherpieplanFolgeverordOption($patient_thherpieplan_folgeverord_id, $day)
	{
		$this->db->where('patient_thherpieplan_folgeverord_id', $patient_thherpieplan_folgeverord_id);
		$this->db->order_by('id', 'ASC');
		$index = ($day - 1) * 1;
		$this->db->limit(1, $index);
		$query = $this->db->get($this->patient_thherpieplan_folgeverord_option);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function checkDailyRecordExist($table_name, $patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$result = $this->db->get($table_name);
		$record = $result->row();

		if ($record) {

			$start_date_local = getGMTDateToLocalDate($record->add_date, 'Y-m-d');
			$start_date = strtotime($start_date_local.' 00:00:01');
			$end_date = strtotime($start_date_local.' 23:59:59');
			$current_date = time();

			if ($current_date >= $start_date && $current_date <= $end_date) {
				$is_locked = true; 
			} else {
				$is_locked = false;
			}

		} else {
			$is_locked = false;
		}
		// $is_locked = false;
		return $is_locked;
	}

	function checkDailyScreenLocked($table_name, $patient_id) 
	{
		$start_date = date('Y-m-d').' 00:00:01';
		$end_date = date('Y-m-d').' 23:59:59';
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$result = $this->db->get($table_name);
		$record = $result->row();

		if ($record) {

			$start_date_local = getGMTDateToLocalDate($record->add_date, 'Y-m-d');
			$start_date = strtotime($start_date_local.' 00:00:01');
			$end_date = strtotime($start_date_local.' 23:59:59');
			$current_date = time();

			if ($current_date >= $start_date && $current_date <= $end_date) {
				$is_locked = true; 
				$unlock_date = strtotime('+1 day', $start_date); 
			} else {
				$is_locked = false;
				$unlock_date = '0';
			}

		} else {
			$is_locked = false;
			$unlock_date = '0';
		}
		// $is_locked = false;
		//$unlock_date = '0';
		$data['is_lock'] = $is_locked;
		$data['unlock_date'] = $unlock_date;
		return $data;
	}

	function checkMonthlyRecordExist($table_name, $patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get($table_name);
		$record = $result->row();

		if ($record) {

			$start_date_local = strtotime(getGMTDateToLocalDate($record->add_date, 'Y-m-d'));
			$end_date_local = strtotime('+1 month', $start_date_local);
			$current_date = time();

			if ($current_date >= $start_date_local && $current_date <= $end_date_local) {
				$is_locked = true; 
			} else {
				$is_locked = false;
			}

		} else {
			$is_locked = false;
		}
		// $is_locked = false;
		return $is_locked;
	}

	function checkMonthlyScreenLocked($table_name, $patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get($table_name);
		$record = $result->row();

		if ($record) {

			$start_date_local = strtotime(getGMTDateToLocalDate($record->add_date, 'Y-m-d'));
			$end_date_local = strtotime('+1 month', $start_date_local);
			$current_date = time();

			if ($current_date >= $start_date_local && $current_date <= $end_date_local) {
				$is_locked = true;
				$unlock_date = strtotime('+1 month', $start_date_local); 
				$unlock_date = strtotime('+1 day', $unlock_date); 
			} else {
				$is_locked = false;
				$unlock_date = '0';
			}

		} else {
			$is_locked = false;
			$unlock_date = '0';
		}
		// $is_locked = false;
		//$unlock_date = '0';
		$data['is_lock'] = $is_locked;
		$data['unlock_date'] = $unlock_date;
		return $data;
	}

	function checkWeeklyRecordExist($table_name, $patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get($table_name);
		$record = $result->row();

		if ($record) {

			$start_date_local = strtotime(getGMTDateToLocalDate($record->add_date, 'Y-m-d'));
			$end_date_local = strtotime('+7 days', $start_date_local);
			$current_date = time();

			if ($current_date >= $start_date_local && $current_date <= $end_date_local) {
				$is_locked = true; 
			} else {
				$is_locked = false;
			}

		} else {
			$is_locked = false;
		}
		// $is_locked = false;
		return $is_locked;
	}

	function checkWeeklyScreenLocked($table_name, $patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$result = $this->db->get($table_name);
		$record = $result->row();

		if ($record) {

			$start_date_local = strtotime(getGMTDateToLocalDate($record->add_date, 'Y-m-d'));
			$end_date_local = strtotime('+7 days', $start_date_local);
			$current_date = time();

			if ($current_date >= $start_date_local && $current_date <= $end_date_local) {
				$is_locked = true;
				$unlock_date = strtotime('+7 days', $start_date_local); 
				$unlock_date = strtotime('+1 day', $unlock_date); 
			} else {
				$is_locked = false;
				$unlock_date = '0';
			}

		} else {
			$is_locked = false;
			$unlock_date = '0';
		}
		// $is_locked = false;
		//$unlock_date = '0';
		$data['is_lock'] = $is_locked;
		$data['unlock_date'] = $unlock_date;
		return $data;
	}

	function add_schmerzstarke($data)
	{
		$this->db->insert($this->tbl_app_schmerzstarke, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_schmerzstarke($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schmerzstarke);
		return $result->row();
	}

	function add_keine_schmerzmittel($data)
	{
		$this->db->insert($this->tbl_app_keine_schmerzmittel, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_keine_schmerzmittel($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_keine_schmerzmittel);
		return $result->row();
	}

	function add_app_lebensqualitat_1($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_1, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_1($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_1);
		return $result->row();
	}

	function add_app_lebensqualitat_2($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_2, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_2($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_2);
		return $result->row();
	}

	function add_app_lebensqualitat_3($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_3, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_3($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_3);
		return $result->row();
	}

	function add_app_lebensqualitat_4($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_4, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_4($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_4);
		return $result->row();
	}

	function add_app_lebensqualitat_5($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_5, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_5($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_5);
		return $result->row();
	}

	function add_app_lebensqualitat_6($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_6, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_6($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_6);
		return $result->row();
	}

	function add_app_lebensqualitat_7($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_7, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_7($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_7);
		return $result->row();
	}

	function add_app_lebensqualitat_8($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_8, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_8($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_8);
		return $result->row();
	}

	function add_app_lebensqualitat_9($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_9, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_9($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_9);
		return $result->row();
	}

	function add_app_lebensqualitat_10($data)
	{
		$this->db->insert($this->tbl_app_lebensqualitat_10, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_lebensqualitat_10($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_lebensqualitat_10);
		return $result->row();
	}

	function add_app_schlafqualitat_1($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_1, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_1($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_1);
		return $result->row();
	}

	function add_app_schlafqualitat_2($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_2, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_2($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_2);
		return $result->row();
	}

	function add_app_schlafqualitat_3($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_3, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_3($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_3);
		return $result->row();
	}

	function add_app_schlafqualitat_4($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_4, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_4($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_4);
		return $result->row();
	}

	function add_app_schlafqualitat_5($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_5, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_5($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_5);
		return $result->row();
	}

	function add_app_schlafqualitat_6($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_6, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_6($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_6);
		return $result->row();
	}

	function add_app_schlafqualitat_7($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_7, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_7($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_7);
		return $result->row();
	}

	function add_app_schlafqualitat_8($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_8, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_8($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_8);
		return $result->row();
	}

	function add_app_schlafqualitat_9($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_9, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_9($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_9);
		return $result->row();
	}

	function add_app_schlafqualitat_10($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_10, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_10($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_10);
		return $result->row();
	}	

	function add_app_schlafqualitat_11($data)
	{
		$this->db->insert($this->tbl_app_schlafqualitat_11, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_app_schlafqualitat_11($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schlafqualitat_11);
		return $result->row();
	}	

	function add_verlaufskontrolle($data)
	{
		$this->db->insert($this->tbl_app_verlaufskontrolle, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_verlaufskontrolle($patient_id, $flag = false)
	{
		$this->db->where('patient_id', $patient_id);

		if ($flag) {
			$this->db->order_by('id', 'asc');
		} else {
			$this->db->order_by('id', 'desc');
		}
		$result = $this->db->get($this->tbl_app_verlaufskontrolle);
		return $result->row();
	}

	function add_weitere_einnahme($data)
	{
		$this->db->insert($this->tbl_app_weitere_einnahme, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}


	function add_weitere_einnahme_content($data)
	{
		$this->db->insert_batch($this->tbl_app_weitere_einnahme_content, $data);
	}	

	function get_weitere_einnahme($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_weitere_einnahme);
		return $result->row();
	}

	function get_weitere_einnahme_content($app_weitere_einnahme_id)
	{
		$this->db->where('app_weitere_einnahme_id', $app_weitere_einnahme_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_weitere_einnahme_content);
		return $result->result();
	}


	function add_weitere_schmerzmedikamente($data)
	{
		$this->db->insert($this->tbl_app_weitere_schmerzmedikamente, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_weitere_schmerzmedikamente($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_weitere_schmerzmedikamente);
		return $result->row();
	}

	function update_reminder($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->tbl_app_notification_reminder, $data);
	}

	function add_reminder($data)
	{
		$this->db->insert($this->tbl_app_notification_reminder, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_patient_reminder($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_notification_reminder);
		return $result->row();
	}

	function add_schmerzstarke_1($data)
	{
		$this->db->insert($this->tbl_app_schmerzstarke_1, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_schmerzstarke_1($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schmerzstarke_1);
		return $result->row();
	} 

	function add_schmerzstarke_2($data)
	{
		$this->db->insert($this->tbl_app_schmerzstarke_2, $data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function get_schmerzstarke_2($patient_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->tbl_app_schmerzstarke_2);
		return $result->row();
	} 

	function delete_send_notification($patient_id, $date, $type){
		$this->db->where('patient_id', $patient_id);
		$this->db->where('notification_send_date', $date);
		$this->db->where('notification_type', $type);
		$this->db->delete('tbl_notifications');
	}

	function getVersandapothekeList() {
		$this->db->where('btm_versand', 'Ja');
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('tbl_apothekensuche_and_zum_cannaxan_bezug');
		return $query->result();
	}

	function getVersandapothekeListById($store_id) {
		$this->db->where('id', $store_id);		
		$query = $this->db->get('tbl_apothekensuche_and_zum_cannaxan_bezug');
		return $query->row();
	}

	function getAllBundesland() {
		$this->db->select('bundesland');
		$this->db->where('btm_versand', 'nein');
		$this->db->group_by('bundesland');
		$this->db->order_by('bundesland','ASC');
		$query = $this->db->get('tbl_apothekensuche_and_zum_cannaxan_bezug');
		return $query->result();
	}

	function getBundeslandStoresList($bundesland) {
		//pr($bundesland); die;
		$this->db->where('bundesland', $bundesland);
		$this->db->where('btm_versand', 'nein');
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('tbl_apothekensuche_and_zum_cannaxan_bezug');
		//pr($this->db->last_query()); die;
		return $query->result();
	}
}