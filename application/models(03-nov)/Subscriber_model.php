<?php

class Subscriber_model extends CI_Model
{
	public function set_subscribers($name, $email, $ip_add, $add_date)
	{
		$this->db->set('name', $name);
		$this->db->set('email', $email);
		$this->db->set('ip_address',$ip_add);
		$this->db->set('add_date', $add_date);

		$query = $this->db->insert('tbl_subscribers');
        
        if($this->db->insert_id()) {
        	return $this->db->insert_id();
        } else {
        	return false;
        }
	}

	public function get_record_by_email($email)
	{
		$this->db->where('email', $email);
		$query = $this->db->get('tbl_subscribers');
		return $query->row();
	}
}
?>