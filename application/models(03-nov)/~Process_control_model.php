<?php

class Process_control_model extends CI_Model
{
	function __construct() {

		parent::__construct();
		$this->begleithmedikation_parent = 'tbl_begleitmedikation';
		$this->begleithmedikation = 'tbl_begleitmedikation_1';
		$this->laborwerte ='tbl_verlaufskontrolle_laborwerte';
		$this->laborparameter = 'tbl_verlaufskontrolletbl_laborwerte_laborparameter';
		$this->nebenwirkung_keine_kausal_bedingte = 'tbl_nebenwirkung_keine_kausal_bedingte';
		$this->nebenwirkung_table_data = 'tbl_nebenwirkung_data';
		$this->nebenwirkung = 'tbl_nebenwirkung';
	}

	function addbegleithmedikationdata($data) {
		//pr($data); die;
		$this->db->insert_batch($this->begleithmedikation, $data);
		//pr($this->db->last_query()); die;
		return $this->db->insert_id();
	}

	function addbegleithmedikationparentData($data) {
		$this->db->insert($this->begleithmedikation_parent, $data);
		return $this->db->insert_id();
	}

	function getPatientDataRecords($patient_id, $doctor_id) {
		
		// $this->db->where('doctor_id', $doctor_id);
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		/*$this->db->group_start();
        $this->db->or_like('add_date');
        $this->db->group_end();*/
		$query = $this->db->get($this->begleithmedikation_parent);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function getPatientsMedicationsDetails($patient_id, $flag = false) {
		
		if ($flag) {
			$date = date('Y-m-d');
			$this->db->where('startdatum <=', $date);
			$this->db->group_start();
				$this->db->where('stopdatum >=', $date);
				$this->db->or_where('stopdatum', '');
				$this->db->or_where('stopdatum', null);
			$this->db->group_end();
		}
		$this->db->order_by('id', 'asc');
		// $this->db->where('begleitmedikation_id', $begleitmedikation_id);
		$this->db->where('patient_id', $patient_id);
		$query = $this->db->get($this->begleithmedikation);
		return $query->result();
	}

	function getPatientsMedications($patient_id) {
		$this->db->order_by('id', 'DESC');
		$this->db->where('patient_id', $patient_id);
		$query = $this->db->get($this->begleithmedikation_parent);
		return $query->result();
	}

	function deletepreviousbegleithmedikationdatabypatientid($id) {
		$this->db->where('patient_id', $id);
		$this->db->delete($this->begleithmedikation);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function getLaborwerteByVisitNumber($visite, $patient_id) {
		$this->db->where('visite', $visite);
		$this->db->where('patient_id', $patient_id);
		$query = $this->db->get($this->laborwerte);
		return $query->row();
	}

	function getPatientLaborwertes($patient_id) {
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('visite', 'asc');
		$query = $this->db->get($this->laborwerte);
		return $query->result();
	}

	function getLaborwerteLaborParameter($laborwerte_id) {
		$this->db->where('laborwerte_id', $laborwerte_id);
		$query = $this->db->get($this->laborparameter);
		return $query->row();
	}

	function addlaborwerte($input) {
		$this->db->insert($this->laborwerte,$input);
		return $this->db->insert_id();
	}

	function updatelaborwerte($input, $id) {
		$this->db->where('id', $id);
		$this->db->update($this->laborwerte,$input);
		return $this->db->insert_id();
	}

	function addlaborparameter($data) {
		$this->db->insert($this->laborparameter,$data);
		return $this->db->insert_id();
	}

	function updatelaborparameter($input, $laborwerte_id) {
		$this->db->where('laborwerte_id', $laborwerte_id);
		$this->db->update($this->laborparameter,$input);
		return true;
	}


	function getDataRecords() {
		//$this->db->where('patient_id', $id);
		$this->db->select($this->laborwerte.'.*');
		$this->db->select($this->laborparameter.'.*');
        //$this->db->where($this->laborwerte.'.id',$id); 
        $this->db->join($this->laborparameter,$this->laborparameter.'.laborwerte_id='.$this->laborwerte.'.id');
		$query = $this->db->get($this->laborwerte);
		return $query->result();
	}

	public function getprocessDataById($id)
	{
	    $this->db->where('patient_id', $id);
		$this->db->select($this->laborwerte.'.*');
		$this->db->select($this->laborparameter.'.*');
        //$this->db->where($this->laborwerte.'.id',$id); 
        $this->db->order_by($this->laborwerte.'.id', 'desc');
        $this->db->join($this->laborparameter,$this->laborparameter.'.laborwerte_id='.$this->laborwerte.'.id');
		$query = $this->db->get($this->laborwerte);
		return $query->row();
	}

	public function getProcessDataByQuestionId($id, $qid)
	{
	    $this->db->where('patient_id', $id);
	    $this->db->where('patient_question_id', $qid);
		$this->db->select($this->laborwerte.'.*');
		$this->db->select($this->laborparameter.'.*');
        //$this->db->where($this->laborwerte.'.id',$id); 
        $this->db->join($this->laborparameter,$this->laborparameter.'.laborwerte_id='.$this->laborwerte.'.id');
		$query = $this->db->get($this->laborwerte);
		return $query->row();
	}
	
	public function getVerlaufskontrolleLaborwerteQuestionId($id, $qid)
	{
	    $this->db->where('patient_id', $id);
	    $this->db->where('patient_question_id', $qid);
		$query = $this->db->get($this->laborwerte);
		return $query->row();
	}

	/*function getproccesscontrolDataRecords($id) {
		//$this->db->where('patient_id', $id);
		$this->db->select($this->laborwerte.'.*');
		$this->db->select($this->laborparameter.'.*');
        $this->db->where($this->laborwerte.'laborwerte_id',$id); 
        $this->db->join($this->laborparameter,$this->laborparameter.'.id='.$this->laborwerte.'.id');
		$query = $this->db->get($this->laborwerte);
		return $query->result();
	}*/

	function getPatientVisits($patient_id)
	{
		$this->db->select('tbl_nebenwirkung_keine_kausal_bedingte.*');
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('add_date', 'ASC');
		$query = $this->db->get('tbl_nebenwirkung_keine_kausal_bedingte');
		// pr($query->result()); die;
		return $query->result();
	}

	function getPatientQuestions($patient_id)
	{
		$this->db->select('tbl_patient_questions.update_date, id, visit_number');
		$this->db->where('is_process_complete', 'Yes');
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('update_date', 'desc');
		$query = $this->db->get('tbl_patient_questions');
		return $query->result();
	}

	function getPatientVisits_1($patient_id)
	{
		$this->db->select('tbl_patient_questions.*');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('tbl_patient_questions');
		return $query->row();
	}

	function getLatestVerlaufskontrolleLaborwerte($patient_id, $visit_number)
	{
		// echo $patient_id.' '.$visit_number;
		$this->db->where('patient_id', $patient_id);
		$this->db->where($this->laborwerte.'.visite', $visit_number);
		$this->db->select($this->laborwerte.'.id, patient_id, doctor_id, behandungstag, visite, arzneimittel');
		$this->db->select($this->laborparameter.'.laborparameter1,laborparameter2,laborparameter3,laborparameter4,wert1,wert2,wert3,wert4,einheit1,einheit2,einheit3,	einheit4,normbereich1,normbereich2,normbereich3,normbereich4'); 
        $this->db->join($this->laborparameter,$this->laborparameter.'.laborwerte_id='.$this->laborwerte.'.id');
		$query = $this->db->get($this->laborwerte);
		// pr($this->db->last_query()); 
		return $query->row();
	}

	function getLatestBegleitmedikation($patient_id, $visit_number){
		$this->db->where('patient_id', $patient_id);
		$this->db->where('visite', $visit_number);
		$this->db->order_by('add_date', 'DESC');
		$query = $this->db->get($this->begleithmedikation_parent);
		return $query->row();
	}

	function getLatestBegleitmedikationDetail($patient_id, $date){
		$this->db->where('startdatum <=', $date);
		$this->db->group_start();
			$this->db->where('stopdatum >=', $date);
			$this->db->or_where('stopdatum', '');
			$this->db->or_where('stopdatum', null);
		$this->db->group_end();
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->begleithmedikation);
		return $query->result();
	}

	function getLatestBebenwirkung($patient_id, $visit_number){
		$this->db->select($this->nebenwirkung_keine_kausal_bedingte.'.*');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('visite', $visit_number);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->nebenwirkung_keine_kausal_bedingte);
		return $query->row();
	}

	function getSideEffectsDataByBehandlunggstagId($id) {
		// $this->db->select($this->nebenwirkung_table_data.'.*');

		$this->db->select($this->nebenwirkung.'.nebenwirkung_name');
		$this->db->where($this->nebenwirkung_table_data.'.behandlunggstag_id', $id);
		$this->db->order_by($this->nebenwirkung_table_data.'.id', 'DESC');
		$this->db->join($this->nebenwirkung, $this->nebenwirkung.'.id='. $this->nebenwirkung_table_data.'.nebenwirkung_name');
		$this->db->group_start();
			$this->db->where($this->nebenwirkung_table_data.'.schwer_schweregrad', 'Yes');
			$this->db->or_where($this->nebenwirkung_table_data.'.lebensbedrohlich_schweregrad', 'Yes');
		$this->db->group_end();
		$query = $this->db->get($this->nebenwirkung_table_data);
		return $query->result(); 
	}

	function get_first_date_severe_pain_last_week($patient_id) {
		$this->db->select('add_date');
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('tbl_app_schmerzstarke');
		return $query->row();
	}

	function get_last_date_severe_pain_last_week($patient_id) {
		$this->db->select('add_date');
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('tbl_app_schmerzstarke');
		return $query->row();
	}

	function total_severe_pain_last_week($patient_id ,$start_date, $end_date){
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->select('SUM(severe_pain_last_week) as total_severe_pain_last_week');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schmerzstarke');
		return $query->row();

	}

	function get_app_verlaufskontrolle($patient_id ,$start_date, $end_date){
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_verlaufskontrolle');
		return $query->result();

	}

	function get_app_weitere_einnahme($patient_id ,$start_date, $end_date){
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_weitere_einnahme');
		return $query->result();

	}

	function get_weitere_einnahme_content($app_weitere_einnahme_id)
	{
		$this->db->where('app_weitere_einnahme_id', $app_weitere_einnahme_id);
		$this->db->order_by('id', 'desc');
		$result = $this->db->get('tbl_app_weitere_einnahme_content');
		return $result->result();
	}

	function total_no_pain_reliever_severe_pain($patient_id ,$start_date, $end_date){
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->select('SUM(no_pain_reliever_severe_pain) as total_no_pain_reliever_severe_pain');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_keine_schmerzmittel');
		return $query->row();
	}

	function get_first_date_app_lebensqualitat_1($patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('tbl_app_lebensqualitat_1');
		return $query->row();
	} 

	function get_last_date_app_lebensqualitat_1($patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('tbl_app_lebensqualitat_1');
		return $query->row();
	} 

	function get_first_date_app_schlafqualitat_1($patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('tbl_app_schlafqualitat_1');
		return $query->row();
	} 

	function get_last_date_app_schlafqualitat_1($patient_id) 
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('tbl_app_schlafqualitat_1');
		return $query->row();
	} 

	function get_app_lebensqualitat_1($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_1');
		return $query->result();
	} 
	function get_app_lebensqualitat_2($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_2');
		return $query->result();
	} 
	function get_app_lebensqualitat_3($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_3');
		return $query->result();
	} 
	function get_app_lebensqualitat_4($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_4');
		return $query->result();
	} 
	function get_app_lebensqualitat_5($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_5');
		return $query->result();
	} 
	function get_app_lebensqualitat_6($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_6');
		return $query->result();
	} 
	function get_app_lebensqualitat_7($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_7');
		return $query->result();
	} 
	function get_app_lebensqualitat_8($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_8');
		return $query->result();
	} 
	function get_app_lebensqualitat_9($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_9');
		return $query->result();
	} 
	function get_app_lebensqualitat_10($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_lebensqualitat_10');
		return $query->result();
	}

	function get_app_schlafqualitat_1($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_2');
		return $query->result();
	} 
	function get_app_schlafqualitat_2($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_3');
		return $query->result();
	} 
	function get_app_schlafqualitat_3($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_4');
		return $query->result();
	} 
	function get_app_schlafqualitat_4($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_5');
		return $query->result();
	} 
	function get_app_schlafqualitat_5($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_6');
		return $query->result();
	} 
	function get_app_schlafqualitat_6($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_7');
		return $query->result();
	} 
	function get_app_schlafqualitat_7($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_8');
		return $query->result();
	} 
	function get_app_schlafqualitat_8($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_9');
		return $query->result();
	} 
	function get_app_schlafqualitat_9($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_10');
		return $query->result();
	} 
	function get_app_schlafqualitat_10($patient_id ,$start_date, $end_date) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$query = $this->db->get('tbl_app_schlafqualitat_11');
		return $query->result();
	} 

	function get_weitere_schmerzmedikamente($patient_id ,$start_date, $end_date, $medicine_name) 
	{
		$start_date_gmt = getDefaultToGMTDate(strtotime($start_date. ' 00:00:01'), 'Y-m-d H:i:s');
		$end_date_gmt = getDefaultToGMTDate(strtotime($end_date. ' 23:59:59'), 'Y-m-d H:i:s');
		$this->db->select('SUM(further_morning_pain_drug) as total_further_morning_pain_drug');
		$this->db->select('SUM(further_lunch_pain_drug) as total_further_lunch_pain_drug');
		$this->db->select('SUM(further_evening_pain_drug) as total_further_evening_pain_drug');
		$this->db->select('SUM(further_night_pain_drug) as total_further_night_pain_drug');
		$this->db->where('patient_id', $patient_id);
		$this->db->where('add_date >=', $start_date_gmt);
		$this->db->where('add_date <=', $end_date_gmt);
		$this->db->where('medicine_name', $medicine_name);
		$this->db->group_by('medicine_name');
		$query = $this->db->get('tbl_app_weitere_schmerzmedikamente');
		return $query->row();
	}

	function get_first_date_weitere_schmerzmedikamente($patient_id) {
		$this->db->select('add_date');
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('tbl_app_weitere_schmerzmedikamente');
		return $query->row();
	}

	function get_last_date_weitere_schmerzmedikamente($patient_id) {
		$this->db->select('add_date');
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('tbl_app_weitere_schmerzmedikamente');
		return $query->row();
	}

	function getHauptindikationById($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_icd_code_zuordnung');
		return $query->row();
	} 
}