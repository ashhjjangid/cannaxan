<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	function __construct(){
		parent::__construct();       
	}

	public function add_otp($data=array()) 
	{
		$this->db->insert('tbl_send_otp',$data);

		if ($this->db->insert_id()) {
			return $this->db->insert_id();
		} else {
			return false;
		}

	}
	
	public function verify_otp($id,$otp)
	{
		$this->db->select('*');
		$this->db->from('tbl_send_otp');
		$this->db->where('id',$id);
		$this->db->where('otp',$otp);
		$query = $this->db->get();
		return $query->row();
	}
	public function get_user_by_phone_number($phone_number)
	{
		$this->db->select('email, id');
		$this->db->from('tbl_user');
		$this->db->where('phonenumber',$phone_number);
		$query=$this->db->get();
		return $query->row();
	}
	public function add_user($data=array())
	{
        $this->db->insert('tbl_user',$data);
		return $this->db->insert_id();
	}

	public function update_user($data, $id)
	{
		$this->db->where('id', $id);
        $this->db->update('tbl_user',$data);
		return true;
	}

	public function get_otp_by_id($id){
		$this->db->select('*');
		$this->db->from('tbl_send_otp');
		$this->db->where('id',$id);
		$query = $this->db->get();
        return $query->row();
	}

	public function update_otp($data, $id)
	{
		$this->db->where('id', $id);
        $this->db->update('tbl_send_otp',$data);
		return true;
	}

	public function getRecordByToken($token)
	{
		$this->db->select('*');
		$this->db->where('token',$token);
		$query = $this->db->get('tbl_user');
		return $query->row();
	}

	public function update_profile($data,$id)
	{
		$this->db->where('id', $id);
        $this->db->update('tbl_user',$data);
        return $this->db->affected_rows();
	}

	public function update_user_by_id($data,$id)
	{
        $this->db->where('id', $id);
        $this->db->update('tbl_user',$data);
        return true;
	}
}