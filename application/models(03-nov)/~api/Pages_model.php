<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends CI_Model {
	
	function __construct() {
		parent::__construct();       
	}

	public function getTermCondition()
	{
		$this->db->select('*');
		$this->db->where('slug','terms-and-condition');
		$query = $this->db->get('tbl_static_page');
        return $query->row();
	}
}