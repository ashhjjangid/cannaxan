<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_model extends CI_Model {
	
	function __construct() {
		parent::__construct();       
	}

	public function get_faq_categories() {
		$this->db->select('*');
		$query = $this->db->get('tbl_faq_categories');
		return $query->result();
	}

	public function get_faq_by_category_id($category_id) {
		$this->db->where('category_id', $category_id);
		$query = $this->db->get('tbl_faq');
		return $query->result();
	}


}