<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['subscribe'] = 'home/subscribe';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Routing for Admin Panel

$route['admin'] = 'admin/admin/index';
$route['admin/login'] = 'admin/registration/login';
$route['admin/logout'] = 'admin/registration/logout';
$route['admin/forgot-password'] = 'admin/registration/forgotPassword';
$route['admin/change-password'] = 'admin/admin/changePasswordForAdmin';
$route['admin/update-profile'] = 'admin/admin/updateProfile';
$route['admin/reset-password/(:any)'] = 'admin/registration/resetPassword/$1';
$route['admin/static-page'] = 'admin/static_page/index';

$route['admin/service/'] = 'admin/service/index/$1';
$route['admin/service/add'] = 'admin/service/add';
$route['admin/service/add/(:any)'] = 'admin/service/add/$1';
$route['admin/service/multiTaskOperation'] = 'admin/service/multiTaskOperation';
$route['admin/service/delete'] = 'admin/service/delete';
$route['admin/service/changeStatus'] = 'admin/service/changeStatus';
$route['admin/service/(:any)'] = 'admin/service/index';

$route['admin/offers/multiTaskOperation'] = 'admin/offers/multiTaskOperation';
$route['admin/offers/delete'] = 'admin/offers/delete';
$route['admin/offers/changeStatus'] = 'admin/offers/changeStatus';
$route['admin/offers/update/(:any)'] = 'admin/offers/update/$1';
$route['admin/offers/add/(:any)'] = 'admin/offers/add/$1';
$route['admin/offers/(:any)'] = 'admin/offers/index/$1';

$route['admin/trips/multiTaskOperation'] = 'admin/trips/multiTaskOperation';
$route['admin/trips/delete'] = 'admin/trips/delete';
$route['admin/trips/changeStatus'] = 'admin/trips/changeStatus';
$route['admin/trips/update/(:any)'] = 'admin/trips/update/$1';
$route['admin/trips/add'] = 'admin/trips/add';
$route['admin/trips/add/(:any)'] = 'admin/trips/add/$1';
$route['admin/trips/add_location'] = 'admin/trips/add_location';
$route['admin/trips/add_dates'] = 'admin/trips/add_dates';
$route['admin/trips/(:any)'] = 'admin/trips/index/$1';


$route['admin/branch/multiTaskOperation'] = 'admin/branch/multiTaskOperation';
$route['admin/branch/delete'] = 'admin/branch/delete';
$route['admin/branch/changeStatus'] = 'admin/branch/changeStatus';
$route['admin/branch/getServices'] = 'admin/branch/getServices';
$route['admin/branch/update/(:any)'] = 'admin/branch/update/$1';
$route['admin/branch/add/(:any)'] = 'admin/branch/add/$1';
$route['admin/branch/(:any)'] = 'admin/branch/index/$1';


$route['admin/mail_schedule/multiTaskOperation'] = 'admin/mail_schedule/multiTaskOperation';
$route['admin/mail_schedule/delete'] = 'admin/mail_schedule/delete';
$route['admin/mail_schedule/changeStatus'] = 'admin/mail_schedule/changeStatus';
$route['admin/mail_schedule/update/(:any)'] = 'admin/mail_schedule/update/$1';
$route['admin/mail_schedule/add/(:any)'] = 'admin/mail_schedule/add/$1';
$route['admin/mail_schedule/(:any)'] = 'admin/mail_schedule/index/$1';
//Routing for front Panel
$route['setpassword/(:any)'] = 'setpassword/index/$1';
$route['kostenubernahmeantrag/step-1'] = 'arztfragebogen/index';
$route['kostenubernahmeantrag/step-2'] = 'arztfragebogen/arztfragebogen_step_2';
$route['kostenubernahmeantrag/step-3'] = 'arztfragebogen/arztfragebogen_step_3';
$route['kostenubernahmeantrag/step-4'] = 'arztfragebogen/arztfragebogen_step_4';
$route['kostenubernahmeantrag/step-5'] = 'arztfragebogen/arztfragebogen_step_5';
$route['kostenubernahmeantrag/step-6'] = 'arztfragebogen/arztfragebogen_step_6';

$route['doctors/register/step-1'] = 'doctors/registration_step_one';
$route['doctors/register.step-2'] = 'doctors/registration_step_two';
$route['doctors/register/success'] = 'doctors/register_success';