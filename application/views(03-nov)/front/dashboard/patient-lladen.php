 <section class="banner-section anmeldung">
         <div class="container-fluid">
            <div class="banner-img">
               <img src="<?php echo $this->config->item('front_assets'); ?>images/banner-img.png">
            </div>
         </div>
      </section>

      <section class="same-section">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-7">
                  <div class="row">
                     <div class="col-md-9 align-self-center">
                        <label class="w-100 text-right mb-0">Versichertennummer:</label>
                     </div>
                     <div class="col-md-3">
                        <input type="text" name="" class="form-control" placeholder="A123456789">
                     </div>
                  </div>
               </div>
               <div class="col-md-5 align-self-center">
                  <div class="custom-btn w-100 text-right">
                     <a class="btn btn-info"><span>laden</span></a>
                  </div>
               </div>
            </div>
         </div>
      </section>