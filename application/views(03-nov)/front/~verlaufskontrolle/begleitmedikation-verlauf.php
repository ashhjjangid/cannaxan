<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<section class="same-section same-blog schmerz-blog">
   <div class="container-fluid">
      <div class="same-heading">
         <h2 class="text-left">Verlauf Begleitmedikation</h2>
      </div>
      <?php if (isset($patient_medicines_graph) && !empty($patient_medicines_graph)) { ?>
        <div class="row">
           <?php foreach ($patient_medicines_graph as $key => $value) { ?>
            <div class="col-lg-6">
              <div class="chart-schmerz chart-bx text-center">
                <div id="chart-<?php echo $key; ?>"></div>
              </div>
              <script type="text/javascript">
                   Highcharts.chart('chart-<?php echo $key; ?>', {
                   chart: {
                     zoomType: 'xy'
                   },
                   title: {
                     text: 'Handelsname: <?php echo $value['medicine_name']; ?>'
                   },
                   subtitle: {
                     text: ''
                   },
                   xAxis: [{
                     categories: <?php echo $value['medicine_series']; ?>,
                     crosshair: true
                   }],
                   yAxis: [{ // Primary yAxis
                     labels: {
                       format: '{value}',
                       style: {
                         color: "#BABDCB"
                       }
                     },
                     title: {
                       text: 'NRS Schmerz',
                       style: {
                         color: "#BABDCB"
                       }
                     }
                   }, { // Secondary yAxis
                     title: {
                       text: '<?php echo $value['medicine_name']; ?>',
                       style: {
                         color: "#9E2B5F"
                       }
                     },
                     labels: {
                       format: '{value}',
                       style: {
                         color: "#9E2B5F"
                       }
                     },
                     opposite: true
                   }],
                   tooltip: {
                     shared: true
                   },
                   legend: {
                     layout: 'vertical',
                     align: 'left',
                     x: 120,
                     verticalAlign: 'top',
                     y: 100,
                     floating: true,
                     backgroundColor:
                       Highcharts.defaultOptions.legend.backgroundColor || // theme
                       'rgba(255,255,255,0.25)'
                   },
                   series: [{

                     name: 'NRS Schmerz',
                     type: 'column',
                     color: '#BABDCB',
                     data: <?php echo $value['medicine_column']; ?>,
                     tooltip: {
                       valueSuffix: ''
                     }

                   }, {
                     name: '<?php echo $value['medicine_name']; ?>',
                     type: 'spline',
                     color: "#9E2B5F",
                     yAxis: 1,
                     data: <?php echo $value['medicine_line']; ?>,

                     tooltip: {
                       valueSuffix: ''
                     }
                   }],
                   exporting: {
					    buttons: {
					        contextButton: {
					            menuItems: [
					                'printChart',
					                'separator',
					                'downloadPNG',
					                'downloadJPEG',
					                'downloadPDF',
					                'downloadSVG'
					            ]
					        }
					    }
					}
                 });
               </script>
            </div>
           <?php } ?>
        </div>
      <?php } ?>
      <div class="same-heading mt-5">
         <h2 class="text-left">Schmerzverlauf</h2>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="chart-schmerz chart-bx text-center">
             <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
             <div id="chart-container"></div>
          </div>
        </div>
      </div>
   </div>
</section>
<script type="text/javascript">
  Highcharts.chart('chart-container', {
  chart: {
    zoomType: 'xy'
  },
  title: {
    text: 'Schmerzverlauf'
  },
  subtitle: {
    text: ''
  },
  xAxis: [{
    categories: <?php echo $series; ?>,
    crosshair: true
  }],
  yAxis: [{ // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: "#9E2B5F"
      }
    },
    title: {
      text: 'NRS Schmerz',
      style: {
        color: "#9E2B5F"
      }
    }
  }, { // Secondary yAxis
    title: {
      text: '',
      style: {
        color: "#BABDCB"
      }
    },
    labels: {
      format: '{value}',
      style: {
        color: "#BABDCB"
      }
    },
    opposite: true
  }],
  tooltip: {
    shared: true
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 120,
    verticalAlign: 'top',
    y: 100,
    floating: true,
    backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || // theme
      'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'NRS Schmerz',
    type: 'column',
    yAxis: 1,
    color: "#BABDCB",
    data: <?php echo $column; ?>,
    tooltip: {
      valueSuffix: ''
    }

  }, {
    name: 'Dosis [mg THC]',
    type: 'spline',
    color: "#9E2B5F",
    data: <?php echo $line; ?>,
    tooltip: {
      valueSuffix: ''
    }
  }],
  exporting: {
	    buttons: {
	        contextButton: {
	            menuItems: [
	                'printChart',
	                'separator',
	                'downloadPNG',
	                'downloadJPEG',
	                'downloadPDF',
	                'downloadSVG'
	            ]
	        }
	    }
	}
});
</script>

