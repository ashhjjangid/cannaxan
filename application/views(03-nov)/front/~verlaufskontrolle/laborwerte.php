 <section class="same-section begleiterkrankungen-section">
         <div class="container-fluid">
            <form method="post" class="ajax_form" action="<?php echo base_url('process_control/laborwerte?in='.$insurance_number); ?>">
            <div class="same-heading ">
              <h2 class="text-left">Laborwerte</h2>
            </div>      
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                     <div class="row align-items-center">
                        <div class="col-sm-6">
                           <label>Datum</label>
                        </div>
                        <div class="col-sm-6">
                           <!-- <input type="text" id="date" name="add_date" value="<?php echo $add_date; ?>" class="form-control text-center select_added_date" placeholder=""> -->
                           <div class="dropdown select-box red-select" id="select_added_date">
                             <button id="datum_btn" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $add_date; ?>
                               
                               <span><i class="fas fa-chevron-down"></i></span>
                             </button>
                             <ul class="dropdown-menu" id="datum-visite" aria-labelledby="datum_btn">

                               <?php if ($visits) { ?>

                                  <?php 
                                  foreach ($visits as $key => $value) { 
                                    $datum = date('Y-m-d', strtotime($value->datum));
                                    // echo $updateDate.' '.$select_date;
                                    $selected = '';

                                    if ($select_date == $datum) {
                                      $selected = 'selected';
                                    }

                                  ?>
                                       <li data-visit-number="<?php echo $value->visite; ?>" data-visit-behandungstag="<?php echo $value->behandungstag; ?>" value="<?php echo date('Y-m-d', strtotime($value->datum)); ?>" <?php echo $selected; ?>><?php echo date('d.m.Y', strtotime($value->datum)); ?></li>
                                    <?php } ?>
                                 <?php } else { ?>
                                        <li value="" data-visit-number="">Visit not available</li>
                                 <?php } ?>
                             </ul>
                           </div>
                           <input type="hidden" name="datum" id="datum" class="datum-field" data-visit-number-field="<?php echo $visite; ?>" data-visit-behandungstag-field="<?php echo $behandungstag; ?>" value="<?php echo $select_date; ?>">

                            <!-- <select class="form-control text-center select_added_date" name="datum" onchange="selectDate()">

                             <?php if ($visits) { ?>

                                  <?php 
                                  foreach ($visits as $key => $value) { 
                                    $datum = date('Y-m-d', strtotime($value->datum));
                                    // echo $updateDate.' '.$select_date;
                                    $selected = '';

                                    if ($select_date == $datum) {
                                      $selected = 'selected';
                                    }

                                  ?>
                                  <option data-visit-number="<?php echo $value->visite; ?>" data-visit-behandungstag="<?php echo $value->behandungstag; ?>" value="<?php echo date('Y-m-d', strtotime($value->datum)); ?>" <?php echo $selected; ?>><?php echo date('d.m.Y', strtotime($value->datum)); ?></option>
                                <?php } ?>
                             <?php } else { ?>
                                <option value="" data-visit-number="">Visit not available</option>
                             <?php } ?>
                           </select>  -->
                        </div>
                     </div>
                  </div>
               </div>
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row align-items-center">
                        <div class="col-sm-6">
                           <label>Behandungstag</label>
                        </div>
                        <div class="col-sm-6">
                           <input type="text" name="behandungstag" value="" class="form-control text-center behandungstag-val" placeholder="" readonly>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                     <div class="row align-items-center">
                        <div class="col-sm-6">
                           <label>Visite</label>
                        </div>
                        <div class="col-sm-6">
                           <input type="text" name="visite" value="<?php echo $visite; ?>" class="form-control text-center visit-number" placeholder="" readonly>
                        </div>
                     </div>
                  </div>
               </div>
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row align-items-center">
                        <div class="col-sm-6">
                           <label>Arzneimittel</label>
                        </div>
                        <div class="col-sm-6">
                           <input type="text" name="arzneimittel" value="CannaXan-THC" class="form-control text-center" placeholder="CannaXan-THC" readonly>
                        </div>
                     </div>
                  </div>                  
               </div>
            </div>

            <div class="begleiterkrankungen-blog">
              <div class="form-group">
                <label >Laborparameter</label>
              </div>
               <div class="row">
                  <div class="col-md-6 align-self-end">
                     <div class="col-md-12">
                        <div class="form-group">
                           <div class="row align-items-center">
                              <div class="col-sm-2">
                                 <label class="text-right">1.</label>
                              </div>
                              <div class="col-sm-10">
                                 <input type="text" name="Laborwerte1" value="AST, GOT (Aspertat-Aminotransferase)" readonly="" class="form-control" placeholder="AST, GOT (Aspertat-Aminotransferase)">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <div class="row align-items-center">
                              <div class="col-sm-2">
                                 <label class="text-right">2.</label>
                              </div>
                              <div class="col-sm-10">
                                 <input type="text" name="Laborwerte2" value="ALT, GPT (Alanin-Aminotransferase)" class="form-control" readonly="" placeholder="ALT, GPT (Alanin-Aminotransferase)">
                              </div>
                           </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="row align-items-center">
                            <div class="col-sm-2">
                               <label class="text-right">3.</label>
                            </div>
                            <div class="col-sm-10">
                               <input type="text" name="Laborwerte3" value="AP (Alkalische Phosphatase)" readonly="" class="form-control" placeholder="AP (Alkalische Phosphatase)">
                            </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                          <div class="row align-items-center">
                            <div class="col-sm-2">
                               <label class="text-right">4.</label>
                            </div>
                            <div class="col-sm-10">
                               <input type="text" name="Laborwerte4" value="Bilirubin, gesamt" readonly="" class="form-control" placeholder="Bilirubin, gesamt">
                            </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Wert</label>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="wert1" value="<?php echo $wert1; ?>" class="form-control text-center red-input" placeholder="0">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="wert2" value="<?php echo $wert2; ?>" class="form-control text-center red-input" placeholder="0">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="wert3" value="<?php echo $wert3; ?>" class="form-control text-center red-input" placeholder="0">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="wert4" value="<?php echo $wert4; ?>"  class="form-control text-center red-input" placeholder="0">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Einheit</label>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <!-- <div class="select-box" >
                                <select class="form-control" name="einheit1">
                                   
                                   <option value="mg" <?php echo ($einheit1 && $einheit1 == 'mg')?'selected':''; ?>>mg</option>
                                   <option value="dl" <?php echo ($einheit1 && $einheit1 == 'dL')?'selected':''; ?>>dL</option>
                                   <option value="µmol" <?php echo ($einheit1 && $einheit1 == 'µmol')?'selected':''; ?>>µmol</option>
                                   <option value="L" <?php echo ($einheit1 && $einheit1 == 'L')?'selected':''; ?>>L</option>
                                </select>
                                <span><i class="fas fa-chevron-down"></i></span>
                              </div> -->
                              <div class="row einheit-first-row">
                                
                                <div class="dropdown select-box" id="select_einheit_dropdown_box">
                                   <button class="dropdown-select einheit_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($einheit1) && !empty($einheit1) && $einheit1)?$einheit1:''; ?>                       
                                     <span><i class="fas fa-chevron-down"></i></span>
                                   </button>
                                   <ul class="dropdown-menu" aria-labelledby="einheit_btn">
                                    <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectEinheit(this)"></li>
                                     <li value="mg" data-toggle="tooltip" data-placement="bottom" title="mg" onclick="selectEinheit(this)">mg</li>
                                     <li value="dl" data-toggle="tooltip" data-placement="bottom" title="dl" onclick="selectEinheit(this)">dl</li>
                                     <li value="µmol" data-toggle="tooltip" data-placement="bottom" title="µmol" onclick="selectEinheit(this)">µmol</li>
                                     <li value="L" data-toggle="tooltip" data-placement="bottom" title="L" onclick="selectEinheit(this)">L</li>
                                   </ul>
                                 </div>
                                 <input type="hidden" name="einheit1" class="einheit-field" value="<?php echo ($einheit1)?$einheit1:''; ?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <!-- <div class="select-box">
                                <select class="form-control" name="einheit2">
                                  
                                  <option value="mg" <?php echo ($einheit2 && $einheit2 == 'mg')?'selected':''; ?>>mg</option>
                                  <option value="dl" <?php echo ($einheit2 && $einheit2 == 'dL')?'selected':''; ?>>dL</option>
                                  <option value="µmol" <?php echo ($einheit2 && $einheit2 == 'µmol')?'selected':''; ?>>µmol</option>
                                  <option value="L" <?php echo ($einheit2 && $einheit2 == 'L')?'selected':''; ?>>L</option>
                                </select>
                                <span><i class="fas fa-chevron-down"></i></span>
                              </div> -->
                              <div class="row einheit-first-row">
                                
                                <div class="dropdown select-box" id="select_einheit_dropdown_box">
                                   <button class="dropdown-select einheit_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($einheit2) && !empty($einheit2) && $einheit2)?$einheit2:''; ?>                       
                                     <span><i class="fas fa-chevron-down"></i></span>
                                   </button>
                                   <ul class="dropdown-menu" aria-labelledby="einheit_btn">
                                    <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectEinheit(this)"></li>
                                     <li value="mg" data-toggle="tooltip" data-placement="bottom" title="mg" onclick="selectEinheit(this)">mg</li>
                                     <li value="dl" data-toggle="tooltip" data-placement="bottom" title="dl" onclick="selectEinheit(this)">dl</li>
                                     <li value="µmol" data-toggle="tooltip" data-placement="bottom" title="µmol" onclick="selectEinheit(this)">µmol</li>
                                     <li value="L" data-toggle="tooltip" data-placement="bottom" title="L" onclick="selectEinheit(this)">L</li>
                                   </ul>
                                 </div>
                                 <input type="hidden" name="einheit2" class="einheit-field" value="<?php echo ($einheit2)?$einheit2:''; ?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <!-- <div class="select-box">
                                <select class="form-control" name="einheit3">
                                  <option value="mg" <?php echo ($einheit3 && $einheit3 == 'mg')?'selected':''; ?>>mg</option>
                                  <option value="dl" <?php echo ($einheit3 && $einheit3 == 'dL')?'selected':''; ?>>dL</option>
                                  <option value="µmol" <?php echo ($einheit3 && $einheit3 == 'µmol')?'selected':''; ?>>µmol</option>
                                  <option value="L" <?php echo ($einheit3 && $einheit3 == 'L')?'selected':''; ?>>L</option>
                                </select>
                                <span><i class="fas fa-chevron-down"></i></span>
                              </div> -->
                              <div class="row einheit-first-row">
                                
                                <div class="dropdown select-box" id="select_einheit_dropdown_box">
                                   <button class="dropdown-select einheit_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($einheit3) && !empty($einheit3) && $einheit3)?$einheit3:''; ?>                       
                                     <span><i class="fas fa-chevron-down"></i></span>
                                   </button>
                                   <ul class="dropdown-menu" aria-labelledby="einheit_btn">
                                    <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectEinheit(this)"></li>
                                     <li value="mg" data-toggle="tooltip" data-placement="bottom" title="mg" onclick="selectEinheit(this)">mg</li>
                                     <li value="dl" data-toggle="tooltip" data-placement="bottom" title="dl" onclick="selectEinheit(this)">dl</li>
                                     <li value="µmol" data-toggle="tooltip" data-placement="bottom" title="µmol" onclick="selectEinheit(this)">µmol</li>
                                     <li value="L" data-toggle="tooltip" data-placement="bottom" title="L" onclick="selectEinheit(this)">L</li>
                                   </ul>
                                 </div>
                                 <input type="hidden" name="einheit3" class="einheit-field" value="<?php echo ($einheit3)?$einheit3:''; ?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                             <!--  <div class="select-box">
                                <select class="form-control" name="einheit4">
                                  
                                  <option value="mg" <?php echo ($einheit4 && $einheit4 == 'mg')?'selected':''; ?>>mg</option>
                                  <option value="dl" <?php echo ($einheit4 && $einheit4 == 'dL')?'selected':''; ?>>dL</option>
                                  <option value="µmol" <?php echo ($einheit4 && $einheit4 == 'µmol')?'selected':''; ?>>µmol</option>
                                  <option value="L" <?php echo ($einheit4 && $einheit4 == 'L')?'selected':''; ?>>L</option>
                                </select>
                                <span><i class="fas fa-chevron-down"></i></span>
                              </div> -->
                              <div class="row einheit-first-row">
                                
                                <div class="dropdown select-box" id="select_einheit_dropdown_box">
                                   <button class="dropdown-select einheit_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($einheit4) && !empty($einheit4) && $einheit4)?$einheit4:''; ?>                       
                                     <span><i class="fas fa-chevron-down"></i></span>
                                   </button>
                                   <ul class="dropdown-menu" aria-labelledby="einheit_btn">
                                    <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectEinheit(this)"></li>
                                     <li value="mg" data-toggle="tooltip" data-placement="bottom" title="mg" onclick="selectEinheit(this)">mg</li>
                                     <li value="dl" data-toggle="tooltip" data-placement="bottom" title="dl" onclick="selectEinheit(this)">dl</li>
                                     <li value="µmol" data-toggle="tooltip" data-placement="bottom" title="µmol" onclick="selectEinheit(this)">µmol</li>
                                     <li value="L" data-toggle="tooltip" data-placement="bottom" title="L" onclick="selectEinheit(this)">L</li>
                                   </ul>
                                 </div>
                                 <input type="hidden" name="einheit4" class="einheit-field" value="<?php echo ($einheit4)?$einheit4:''; ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Normbereich</label>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="normbereich1"onkeypress="return isNumberKey(event)" value="<?php echo $normbereich1; ?>" class="form-control text-center red-input" placeholder="000-000">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="normbereich2" onkeypress="return isNumberKey(event)" value="<?php echo $normbereich2; ?>" class="form-control text-center red-input" placeholder="000-000">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" onkeypress="return isNumberKey(event)" value="<?php echo $normbereich3; ?>" name="normbereich3" class="form-control text-center red-input" placeholder="000-000">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="normbereich4" onkeypress="return isNumberKey(event)" value="<?php echo $normbereich4; ?>" class="form-control text-center red-input" placeholder="000-000">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-12 text-right">
                     <div class="form-group btn-mar-top">
                        <button type="submit" class="btn btn-primary"><span>Speichern &amp; weiter</span></button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

  <script type="text/javascript">

  $(function(){
    selectDate();

      $('#date').datepicker({
         // startDate: new Date(),
       // autoclose: true,
        format: "dd.mm.yyyy",
      });

      $('.stopdate').datepicker({
         // startDate: new Date(),
       // autoclose: true,
        format: "dd.mm.yyyy",
      });

      $('#add_time').datepicker({

      });
  });

    function isNumberKey(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

     return true;
  }

  function selectDate()
  {
    var data_visit_number = $('.datum-field').attr('data-visit-number-field');
    var data_behandungstag = $('.datum-field').attr('data-visit-behandungstag-field');
    $('.visit-number').val(data_visit_number);
    $('.behandungstag-val').val(data_behandungstag);
  }

  $('#select_added_date li').click(function() {

     var getText = $(this).text(); 
     var getVisitNumber = $(this).attr('data-visit-number');
     var getVisitBehandungstag = $(this).attr('data-visit-behandungstag');
     var getValue = $(this).val();
     //alert(getText); 
     $('#datum_btn').text(getText);
     $('.datum-field').attr('data-visit-number-field', getVisitNumber);
     $('.datum-field').attr('data-visit-behandungstag-field', getVisitBehandungstag);
     $('.datum-field').val(getText);
     $('#select_added_date').addClass('not-red');
     selectDate();
   });

  $(function(){
     /* $('.select_added_date').datepicker({
         format: 'yyyy-mm-dd'
      })*/
   })

  function selectEinheit($this)
  {
    var selected_value = $($this).attr('value');
    var selected_text = $($this).text();
    //alert(selected_text);
    $($this).closest('.einheit-first-row').find('.einheit-field').val(selected_value);
    $($this).closest('.einheit-first-row').find('.einheit_btn').text(selected_text);
  }
 </script>