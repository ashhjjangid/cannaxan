 <form method="post" class="ajax_form" action="<?php echo current_url(); ?>" id="register_form" enctype="multipart/form-data">
<section class="same-section">
   <div class="container-fluid">
      <div class="same-heading" style="text-align: center;">
         <h2>VEREINBARUNG GEMÄß ART. 26 DSGVO</h2>
         <p>ZWISCHEN <br> <strong>CANNAXAN GMBH</strong> <br> BIRKERFELD 12 <br> 83627 WARNGAU <br> ─ NACHFOLGEND BEZEICHNET ALS <strong>VERANTWORTLICHER ZU 1</strong> ─ <br> UND</p>
      </div>
      <div class="row mt-5">
         <div class="col-md-6">
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-4 align-self-center">
                        <label class="mb-0">VorName</label>
                     </div>
                     <div class="col-sm-5">
                        <input type="text" name="first_name" class="form-control" placeholder="Vorname">
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-4 align-self-center">
                        <label class="mb-0">Name</label>
                     </div>
                     <div class="col-sm-5">
                        <input type="text" name="last_name" class="form-control" placeholder="Name">
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                   <div class="row align-items-end">
                      <div class="col-sm-4 align-self-center">
                         <label class="mb-0">Stra<span>ß</span>e</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="strabe" class="form-control" placeholder="Musterstraße">
                      </div>
                      <div class="col-md-3">
                         <label class="mb-2">Hausnr.</label>
                         <input type="text" name="house_no" class="form-control" placeholder="Hausnr">
                      </div>
                   </div>
                </div>
             </div>
             <div class="col-md-6">
              <div class="form-group">
                   <div class="row">
                      <div class="col-sm-4 align-self-center">
                         <label class="mb-0">Titel</label>
                      </div>
                      <div class="col-sm-5">
                         <!-- <input type="text" name="title" class="form-control" placeholder="Titel"> -->
                         <div class="dropdown select-box">
                          <button id="dLabel" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <!-- Select -->
                            <span><i class="fas fa-chevron-down"></i></span>
                          </button>
                          <ul class="dropdown-menu" id="title" aria-labelledby="dLabel">
                            <li value="Dr.">Dr.</li>
                            <li value="Dr. med">Dr. med</li>
                            <li value="keine">keine</li>
                            <li value="PD Dr. med.">PD Dr. med.</li>
                            <li value="Prof. Dr. med.">Prof. Dr. med.</li>
                          </ul>
                        </div>
                        <input type="hidden" name="title" class="title-field" value="">
                      </div>
                   </div>
                </div>
             </div>
             <div class="col-md-6">
                <div class="form-group">
                   <div class="row">
                      <div class="col-sm-4 align-self-center">
                         <label class="mb-0">PLZ</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="plzl" class="form-control" placeholder="PLZ">
                      </div>
                   </div>
                </div>
             </div>
             <div class="col-md-6">
                <div class="form-group">
                   <div class="row">
                      <div class="col-sm-4 align-self-center">
                         <label class="mb-0">ORT</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="ort" class="form-control" placeholder="München">
                      </div>
                   </div>
                </div>
             </div>
      </div>
      <p style="color: #5e6586;margin-top: 20px;margin-bottom: 20px;text-align: center;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">─ NACHFOLGEND BEZEICHNET ALS <strong>VERANTWORTLICHER ZU 2</strong> ─ <br> ─ BEIDE GEMEINSAM ALS DIE <strong>PARTEIEN</strong> BEZEICHNET ─</p>
      <p style="color: #000;margin-top: 20px;margin-bottom: 20px;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Präambel</p>
      <p style="color: #000;margin-top: 20px;margin-bottom: 20px;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Die Parteien nutzen gemeinsam die CannaXan-App. Im Rahmen ihrer Zusammenarbeit verarbeiten die Parteien personenbezogene Daten und Gesundheitsdaten. Für einen Teil dieser Daten haben die Parteien die Zwecke und Mittel der Verarbeitung (im Folgenden: die <strong style="color: #000">Datenverarbeitung </strong>) gemeinsam festgelegt. Die Parteien sind im Hinblick auf diese Datenverarbeitung mithin gemeinsam Verantwortliche i.S.d. Art. 26 DSGVO i.V.m. Art. 4 Nr. 7 DSGVO. </p>
      <p style="color: #000;margin-top: 20px;margin-bottom: 20px;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Mit dieser Vereinbarung möchten die Parteien ihre Rechte und Pflichten im Hinblick auf die gemeinsame Verarbeitung der personenbezogenen
         Daten näher konkretisieren
      </p>
      <ol style="list-style-type: none;">
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';"><strong>1.</strong> Gegenstand dieser Vereinbarung</strong>
            <ol>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';"><strong style="color: #000">Beschreibung der Datenverarbeitung.</strong> Gegenstand, Zweck und Umfang der Datenverarbeitung sind in <strong style="color: #000"> Anlage 1</strong> näher beschrieben.
                     Insbesondere enthält <strong style="color: #000"> Anlage 1</strong> eine Beschreibung der Art der verarbeiteten Daten und der Kategorien der betroffenen Personen.
                  </p>
               </li>
            </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Phasen der Datenverarbeitung</strong>
            <ol>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';"><strong style="color: #000">Einteilung in Prozessabschnitte.</strong> Die Parteien haben in <strong style="color: #000">Anlage 2 </strong> die Prozessabschnitte definiert, für die eine gemeinsame Verantwortlichkeit besteht, und festgelegt, welche Partei für die Durchführung des jeweiligen Prozessabschnittes zuständig sein soll.</p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';"><strong style="color: #000">Eigenständige Verantwortlichkeit. </strong> Für Prozessabschnitte, die nicht in <strong style="color: #000"> Anlage 2 </strong> benannt sind, bleibt jede Partei eigenständiger
                     Verantwortlicher i.S.d. Art. 4 Nr. 7 DSGVO.
                     .
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';"><strong style="color: #000">Verfahrensverzeichnis.</strong> Beide Parteien werden die Datenverarbeitung in ihr jeweiliges Verfahrensverzeichnis gem. Art. 30 Abs. 1 S. 1 DSGVO aufnehmen und dort als Verfahren in gemeinsamer Verantwortung vermerken</p>
               </li>
            </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Informationspflichten nach Art. 13, 14 DSGVO</strong>
            <ol>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Zuständigkeit. Für die Erfüllung der Informationspflichten nach Art. 13, 14 DSGVO ist die in <strong> Anlage 3</strong> genannte Partei zuständig</p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Inhaltliche Vorgaben. Betroffenen Personen sind die erforderlichen Informationen in präziser, transparenter, verständlicher und
                     leicht zugänglicher Form in einer klaren und einfachen Sprache zu übermitteln.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     Zurverfügungstellung dieser Vereinbarung. Die nach Ziffer 3.1 für die Erfüllung der Informationspflichten zuständige Partei verpflichtet
                     sich, den wesentlichen Inhalt dieser Vereinbarung den betroffenen Personen gemäß Art. 26 Abs. 2 Satz 2 DSGVO zur Verfügung zu
                     stellen
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Abstimmung. Soweit möglich stimmen die Parteien den Inhalt und die Formulierung etwaiger Informationen an betroffene Personen nach dieser Ziffer 3 miteinander ab</p>
               </li>
            </ol>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Betroffenenanfragen nach Art. 15 ff. DSGVO</strong>
            <ol>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong style="color: #000"> Zuständigkeit.</strong> Für die Bearbeitung von Anfragen betroffener Personen nach den Art. 15 ff. DSGVO ist die in <strong>Anlage 3</strong> genannte
                     Partei zuständig.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong style="color: #000">Keine Beschränkung.</strong> Die Parteien sind sich bewusst, dass betroffene Personen die ihnen nach den Art. 15 ff. DSGVO zustehenden
                     Rechte bei und gegenüber jeder Partei geltend machen können
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong style="color: #000"> Weiterleitung von Ersuchen.</strong> Wendet sich eine betroffene Person in Wahrnehmung ihrer Rechte nach den Art. 15 ff. DSGVO an eine
                     Partei, die gemäß Ziffer 4.1 nicht für die Erfüllung des Ersuchens zuständig ist, so leitet die unzuständige Partei das Ersuchen
                     unverzüglich an die zuständige Partei weiter.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong style="color: #000"> Gegenseitige Unterstützung.</strong> Die Parteien stellen sich bei Bedarf die zur Bearbeitung und Beantwortung von Betroffenenanfragen
                     erforderlichen Informationen gegenseitig zu Verfügung
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Löschung von Betroffenendaten.</strong> Die Löschung von Daten des Betroffenen durch eine Partei darf nur nach schriftlicher Information der
                     anderen Partei erfolgen. Diese kann der Löschung widersprechen, sofern sie ein berechtigtes Interesse am Fortbestand der Daten hat;
                     ein solches kann sich insbesondere aus einer gesetzlichen Aufbewahrungspflicht ergeben.
                  </p>
               </li>
            </ol>
         </li>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Meldepflichten gemäß Art. 33, 34 DSGVO</strong>
            <ol>
               <li><p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';"><strong>Zuständigkeit.</strong> Für Meldungen von Datenschutzverstößen nach Art. 33, 34 DSGVO ist die in Anlage 3 genannte Partei zuständig.</p></li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                    <strong>Informationspflicht.</strong> Stellt eine Partei in Bezug auf die unter dieser Vereinbarung verarbeiteten personenbezogenen Daten eine 
                     Datenschutzverletzung i.S.d. Art. 4 Nr. 12 DSGVO fest, informiert sie die andere Partei unverzüglich hierüber und stellt der anderen Partei die zur Prüfung der 
                     Datenschutzverletzung erforderlichen Informationen in angemessener Weise zur Verfügung
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Inhaltliche Abstimmung. </strong> Soweit möglich und zumutbar stimmen die Parteien den Inhalt etwaigen Meldungen nach Art. 33, 34 DSGVO miteinander ab.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Sonstige Fehler. </strong> Ziffer 5.2 gilt entsprechend, sofern eine Partei Unregelmäßigkeiten oder Fehler bei der Datenverarbeitung und/oder eine Verletzung der Bestimmungen dieser Vereinbarung oder anwendbaren Datenschutzrechts (insbesondere der DSGVO) feststellt.
                  </p>
               </li>
            </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Zusammenarbeit mit Aufsichtsbehörden</strong>
            <ol>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';"><strong> Anfragen. </strong> Wendet sich eine Datenschutzaufsichtsbehörde an eine der 
                  Parteien im Zusammenhang mit der Datenverarbeitung unter dieser Vereinbarung, so informiert diese Partei die andere Partei unverzüglich hierüber.</p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                    <strong> Abstimmung.</strong> Soweit möglich stimmen sich die Parteien vor Beantwortung von Anfragen von Datenschutzaufsichtsbehörden, die Zusammenhang mit der 
                    Datenverarbeitung unter dieser Vereinbarung stehen, miteinander ab. Dies gilt auch für die Einlegung von Rechtsbehelfen und Rechtsmitteln gegen etwaige 
                    aufsichtsbehördliche Maßnahmen, die im Zusammenhang mit der Datenverarbeitung unter dieser Vereinbarung stehen.
                  </p>
               </li>
            </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Haftung; Freistellung</strong>
            <ol>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Haftung gegenüber Betroffenen. </strong> Die Parteien haften gegenüber betroffenen Personen nach den gesetzlichen Vorschriften
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Anteiliger Ausgleich im Innenverhältnis.</strong> Soweit eine Partei ihre Pflichten aus dieser Vereinbarung verletzt, stellt sie die andere Partei von aus dem Pflichtverstoß 
                     entstandenen Schäden in jenem Umfang frei, wie dies ihrem in dieser Vereinbarung vereinbarten Verantwortungsbeitrag für den Verstoß entspricht.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                    <strong>Umfang der Freistellung. </strong> Die Freistellungspflicht gemäß Ziffer 7.2 umfasst etwaige Ansprüche von und Verbindlichkeiten gegenüber Dritten (einschließlich 
                    etwaiger gerichtlicher und außergerichtlicher Rechtsverteidigungskosten)
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Gegenseitige Unterstützung. </strong> Die Parteien unterstützen sich gegenseitig in angemessener Weise im Zusammenhang mit der Verteidigung, dem Vergleich und der Erfüllung etwaiger Ansprüche und Verbindlichkeiten nach Ziffer 7.3.
                  </p>
               </li>
            </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Haftung; Freistellung</strong>
            <ol>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong> Haftung gegenüber Betroffenen.</strong> Die Parteien haften gegenüber betroffenen Personen nach den gesetzlichen Vorschriften.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Anteiliger Ausgleich im Innenverhältnis.</strong> Soweit eine Partei ihre Pflichten aus dieser Vereinbarung verletzt, stellt sie die andere Partei
                     von aus dem Pflichtverstoß entstandenen Schäden in jenem Umfang frei, wie dies ihrem in dieser Vereinbarung vereinbarten 
                     Verantwortungsbeitrag für den Verstoß entspricht.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                    <strong>Umfang der Freistellung.</strong> Die Freistellungspflicht gemäß Ziffer 7.2 umfasst etwaige Ansprüche von und Verbindlichkeiten gegenüber 
                     Dritten (einschließlich etwaiger gerichtlicher und außergerichtlicher Rechtsverteidigungskosten)
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Gegenseitige Unterstützung.</strong> Die Parteien unterstützen sich gegenseitig in angemessener Weise im Zusammenhang mit der Verteidigung, dem Vergleich und der Erfüllung etwaiger Ansprüche und Verbindlichkeiten nach Ziffer 7.3.
                  </p>
               </li>
            </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Laufzeit und Kündigung</strong>
               <ol>
                  <li>
                     <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                        <strong>Inkrafttreten und Laufzeit. </strong> Diese Vereinbarung tritt am Tag ihrer Unterzeichnung in Kraft und läuft <strong><i>[auf unbestimmte Zeit].</i></strong>
                     </p>
                  </li>
                  <li>
                     <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                        <strong>Kündigung aus wichtigem Grund. </strong> Jede Partei hat das Recht, diese Vereinbarung aus wichtigem Grund zu kündigen. Ein wichtiger Grund
                        liegt insbesondere vor, wenn eine Partei einen Verstoß gegen eine wesentliche Pflicht aus dieser Vereinbarung begeht und im Falle
                        eines heilbaren Verstoßes diesen nicht innerhalb von 30 Tagen nach Benachrichtigung durch die andere Partei heilt
                     </p>
                  </li>
               </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Schlussbestimmungen</strong>
            <ol>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong> Vorrang dieser Vereinbarung.</strong> Im Falle eines Widerspruchs zwischen dieser Vereinbarung und sonstigen Vereinbarungen der Parteien, geht diese Vereinbarung vor, soweit der Widerspruch die Rechte und Pflichten der Parteien als gemeinsam Verantwortliche betrifft
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Salvatorische Klausel.</strong> Sollte eine Bestimmung dieser Vereinbarung unwirksam oder nicht durchsetzbar sein oder werden, so bleiben
                     die übrigen Bestimmungen dieser Vereinbarung hiervon unberührt. Anstelle der unwirksamen oder nicht durchsetzbaren Bestimmung
                     werden die Parteien eine solche vereinbaren, die die Anforderungen des Art. 26 DSGVO hinreichend berücksichtigt und dem Zweck
                     der unwirksamen oder nicht durchsetzbaren Bestimmung am nächsten kommt.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong> Änderungen. </strong> Änderungen und Ergänzungen dieser Vereinbarung bedürfen der Schriftform. Dies gilt auch für den Verzicht auf dieses Formerfordernis.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                    <strong>Anwendbares Recht. </strong> Diese Vereinbarung unterliegt deutschem Recht (einschließlich der DSGVO) unter Ausschluss des internationalen Privatrechts.
                  </p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
                     <strong>Gerichtsstand. </strong> Ausschließlicher Gerichtsstand für alle Streitigkeiten zwischen den Parteien aus und im Zusammenhang mit dieser Vereinbarung und ihrer Durchführung ist München.
                  </p>
               </li>
            </ol>
         </li>
      </ol>
      <table cellspacing="0"  cellpadding="5"  width="100%" style="margin-bottom: 70px;">
         <tr>
            <td valign="center"><strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Warngau, Datum</strong></td>
            <td valign="center"><strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Ort , Datum</strong></td>
         </tr>
         <tr>
            <td valign="center"><strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Zustimmung durch den Geschäftsführer</strong></td>
            <td valign="center"><strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">
            <!-- <div class="checkbox-custom" style="display: inline-block; vertical-align: middle; margin-right: 10px!important;">
               <input type="checkbox" id="vertarg" name="vertarg">
               <span class="cheak"></span>
            </div>
            <label for="vertarg">Hiermit stimme ich dem Vertrag zu</label></strong></td> -->
            <div class="checkbox-custom" style="display: inline-block; vertical-align: middle; margin-right: 10px!important;">
               <input type="checkbox" class="vertarg" name="vertarg" id="vertarg" value="Yes">
               <label for="vertarg"><span class="cheak"></span>Hiermit stimme ich dem Vertrag zu</label>

            </div>
         </tr>
         <tr>
            <td colspan="2">
               <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Dr. Werner Brand</strong>
            </td>
         </tr>
         <tr>
            <td valign="center"><strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">CannaXan GmbH</strong></td>
            <td valign="center"><strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Name Arzt</strong></td>
         </tr>
      </table>

      <strong style="text-decoration: underline; font-size: 18px; letter-spacing: 1px; color: #1221c5;">ANLAGE 1</strong>
      <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light'; margin-bottom: 20px; margin-top: 10px;">B E SCHR E I BUNG DE R DAT ENVE RAR B E I TUNG IN GE M E INSA M E R VE RANTWO RT L ICHK E I T</p>
      <ol style="list-style-type: upper-roman;" class="upper-roman">
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Gegenstand der Datenverarbeitun</strong>
            <ol style="list-style-type: none;">
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Gegenstand der Datenverarbeitung unter dieser Vereinbarung ist</p>
                  <ol style="list-style-type: none;">
                     <li>
                        <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Die gemeinsame Verarbeitung von personenbezogenen Daten und Gesundheitsdaten in 
                        einer App durch die CannaXan GmbH. </p>
                     </li>
                  </ol>
               </li>
            </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Zweck der Datenverarbeitung</strong>
            <ol style="list-style-type: none">
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Die Datenverarbeitung erfolgt zu folgenden Zwecken:</p>  
                  <ol style="list-style-type: none;">
                     <li>
                        <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Zur Verwendung der CannaXan-App, damit insbesondere aus den Daten ein 
                        Titrationsplan erstellt werden kann, zur Erstellung eines BTM-Rezeptes und zur Dokumentation des Therapieverlaufs, die insbesondere der übersichtlichen
                        Archivierung</p>
                     </li>
                     <li>
                        <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">der vom Patienten erfassten Daten als Grundlage für die Therapieentscheidungen des 
                        Arztes dienen können.</p>
                     </li>
                  </ol>
               </li>
            </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Art der personenbezogenen Daten</strong>
            <ol style="list-style-type: none;">
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Folgende Datenarten sind regelmäßig Gegenstand der Datenverarbeitung:</p>
                  <ol style="list-style-type: none;">
                     <li>
                        <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Personenbezogenen Daten verarbeiten:</p>
                        <ol style="list-style-type: disc;">
                           <li style="list-style-type: disc;">
                              <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Vorname, Name, Geschlecht, Größe, Gewicht</p>
                           </li>
                        </ol>
                     </li>
                     <li>
                        <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Gesundheitsdaten:</p>
                        <ol>
                           <li style="list-style-type: disc;">
                              <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Indikation, Schmerzwert, Schlafqualitätsfragebogen, 
                              Lebensqualitätsfragebogen, Arzneimitteldosis, Begleitmedikamente, Nebenwirkungen, Laborparameter, 
                              Arztfragebogen zur Erstellung des Kostenübernameantrags bei der Krankenkasse</p>
                           </li>
                        </ol>
                     </li>
                  </ol>
               </li>
            </ol>
         </li>
         <li>
            <strong style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Kategorien der betroffenen Personen</strong>
            <ol>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">Folgende Personenkreise sind regelmäßig von der Datenverarbeitung betroffen:</p>
               </li>
               <li>
                  <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light';">den Verantwortlichen, und den dazugehörigen verbundenen Unternehmen, dem behandelnden Arzt und Apotheken</p>
               </li>
            </ol>
         </li>
      </ol>
      <strong style="text-decoration: underline; font-size: 18px; letter-spacing: 1px; color: #1221c5; margin-top: 20px;">ANLAGE 2</strong>
      <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light'; margin-bottom: 20px; margin-top: 10px;">ÜB E R S ICHT P ROZ E SSAB SCHNI T T E</p>


      <table cellpadding="12" cellspacing="0" width="100%" border="1" style=" border-color:#1221c5;border-collapse: collapse;" class="table_striped">
         <tr>
            <td><strong style="font-size: 16px; letter-spacing: 1px; color: #1221c5;">Beschreibung des Prozessabschnitts</strong></td>
            <td><strong style="font-size: 16px; letter-spacing: 1px; color: #1221c5;">In gemeinsamer Verantwortlichkeit?</strong></td>
            <td colspan="2" style=" padding: 0;">
               <table width="100%" class="bg-transparent" cellpadding="12" cellspacing="0" width="100%" >
                  <tr>
                     <td colspan="2" align="center" style="border-bottom-color:#1221c5; border-bottom-width: 1px; border-bottom-style: solid;"><strong style="font-size: 16px; letter-spacing: 1px; color: #1221c5;">Zuständig?</strong></td>
                  </tr>
                  <tr>
                     <td><strong style="font-size: 16px; letter-spacing: 1px; color: #1221c5;">Verantwortlicher zu 1</strong></td>
                     <td style="border-left-color:#1221c5; border-left-width: 1px; border-left-style: solid;"><strong style="font-size: 16px; letter-spacing: 1px; color: #1221c5; ">Verantwortlicher zu 2</strong></td>
                  </tr>
               </table>
            </th>
         </tr>
         <tr>
            <td width="25%"><strong style="color: #5e6690; font-size: 15px;">Erhebung der Daten</strong></td>
            <td width="25%" align="center"><strong style="color: #000; font-size: 15px;">nein </strong></td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;"></strong></td>
            <td align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
         </tr>
         <tr>
            <td width="25%"><strong style="color: #5e6690; font-size: 15px;">Speicherung der Daten </strong></td>
            <td width="25%" align="center"><strong style="color: #000; font-size: 15px;">nein </strong></td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td align="center" ><strong style="color: #000; font-size: 15px;"></strong></td>
         </tr>
         <tr>
            <td width="25%"><strong style="color: #5e6690; font-size: 15px;">Änderung der Daten</strong></td>
            <td width="25%" align="center"><strong style="color: #000; font-size: 15px;">nein </strong></td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;"></strong></td>
            <td align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
         </tr>
         <tr>
            <td width="25%"><strong style="color: #5e6690; font-size: 15px;">Löschung der Daten </strong></td>
            <td width="25%" align="center"><strong style="color: #000; font-size: 15px;">nein </strong></td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td align="center" ><strong style="color: #000; font-size: 15px;"></strong></td>
         </tr>
      </table>

      <strong style="text-decoration: underline; display: block; font-size: 18px; letter-spacing: 1px; color: #1221c5; margin-top: 20px;">ANLAGE 2</strong>
      <p style="color: #000;line-height: 1.4;font-size: 20px;font-family: 'Aller-Light'; margin-bottom: 20px; margin-top: 10px;">ÜB E R S ICHT P ROZ E SSAB SCHNI T T E</p>

      <table cellpadding="12" cellspacing="0" width="100%" border="1" style=" border-color:#1221c5;border-collapse: collapse;" class="table_striped">
         <tr>
            <td width="25%"><strong style="font-size: 16px; letter-spacing: 1px; color: #000">Pflichten aus der DSGVO</strong></td>
            <td width="25%"><strong style="font-size: 16px; letter-spacing: 1px; color: #000">Einschlägige Regelung</strong></td>
            <td width="25%"><strong style="font-size: 16px; letter-spacing: 1px; color: #000">Verantwortlicher zu 1</strong></td>
            <td width="25%"><strong style="font-size: 16px; letter-spacing: 1px; color: #000">Verantwortlicher zu 2</strong></td>
         </tr>
         <tr>
            <td width="25%"><strong style="font-size: 16px; letter-spacing: 1px; color: #000">I. Informationspflichten</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Erfüllung der Informationspflicht bei Erhebung personenbezogener Daten</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 13</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Erfüllung der Informationspflicht, wenn Daten nicht bei Betroffenen erhoben wurden </td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 14</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%"><strong style="font-size: 16px; letter-spacing: 1px; color: #000">II. Betroffenenanfragen</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Bearbeitung von Auskunftsverlangen</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 15</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Bearbeitung von Berichtigungsanfragen</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 16</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Bearbeitung von Löschbegehren</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 17</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Bearbeitung von Begehren auf Beschränkung der Verarbeitung</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 18</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Erfüllung von Mitteilungspflichten im Zusammenhang mit der Berichtigung, Löschung oder Einschränkung der Ver arbeitung</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 19</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Abwicklung von Herausgabeverlangen</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 20</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Bearbeitung von Widersprüchen</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 21</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%"><strong style="font-size: 16px; letter-spacing: 1px; color: #000">III. Meldepflichten</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Meldung von Datenpannen an die Aufsichtsbehörde</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 33</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Meldung von Datenpannen an die betroffene Person</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 34</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%"><strong style="font-size: 16px; letter-spacing: 1px; color: #000">IV. Sonstige Pflichten</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Festlegung, Dokumentation, Überprüfung und Aktualisierung der techn.-org. Maßnahmen</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 24 i.V.m. Art 32</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Einschaltung von Auftragsverarbeitern bzw. Unterauftragsverarbeitern und deren Überprüfung</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 28</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
         <tr>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Ggf. Datenschutzfolgenabschätzung und Konsultation einer Aufsichtsbehörde</td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;">Art. 35, 36</td>
            <td width="25%" align="center" ><strong style="color: #000; font-size: 15px;">X</strong></td>
            <td width="25%" style="font-size: 16px; letter-spacing: 1px; color: #000;"></td>
         </tr>
      </table>
      <table width="100%" cellpadding="8" cellspacing="0" style=" margin-top: 50px;">
         <tr>
            <td width="100">
               <div class="checkbox-custom" >
                  <input type="checkbox" class="vertarg" name="verstanden_vertarg" id="verstanden_vertarg" value="Yes">
                  <label for="verstanden_vertarg"><span class="cheak"></span><strong style="font-family: 'Aller-Bold'; font-size: 20px;">Ich habe den Vertrag gelesen und verstanden</strong></label>

            </div>
            </td>
            <td>
               <!-- <strong style="font-family: 'Aller-Bold'; font-size: 20px;">Ich habe den Vertrag gelesen und verstanden</strong> -->
            </td>
         </tr>
         <tr>
            <td width="100">
               <div class="checkbox-custom" >
                  <input type="checkbox" class="vertarg" name="akzeptieren_vertarg" id="akzeptieren_vertarg" value="Yes">
                  <label for="akzeptieren_vertarg"><span class="cheak"></span><strong style="font-family: 'Aller-Bold'; font-size: 20px;">Ich stimme dem Vertrag zu und akzeptiere ihn</strong></label>
            </div>
            </td>
            <td>
               <!-- <strong style="font-family: 'Aller-Bold'; font-size: 20px;">Ich stimme dem Vertrag zu und akzeptiere ihn</strong> -->
            </td>
         </tr>
         <tr>
            <td colspan="2" align="right">
               <button class="btn btn-info"><span>Nächster</span></button>
            </td>
         </tr>
         <tr>
            <td colspan="2">
               <p style="color: #5e6586;margin-top: 20px;margin-bottom: 20px;text-align: center;line-height: 1.4;font-size: 20px;font-family: 'Aller-Bold';">Auf der folgenden Seite steht Ihnen dieser Vertrag als PDF-Datei zu Downloade bereit</p>
            </td>
         </tr>
      </table>
   </div>
</section>
</form>
<style type="text/css">
   .same-section{padding: 30px 0;}
   .same-heading{display: block;}
   .same-heading h2{color: #10069F; text-transform: uppercase; font-family: 'Aller-Bold'; font-size: 32px; padding-bottom: 18px; letter-spacing: 3.2px; }
   .same-heading p{color: #5e6586;}
   ol li{list-style-type: none;}
   ol>li{ margin-bottom: 20px; }
   ol li ol{ margin-top: 15px; }
   .upper-roman li ol{ margin-left: 25px; }
   .upper-roman>li{ list-style-type: upper-roman; }
   .table_striped td{ font-family: 'Aller-Light'; }
   .table_striped *{ font-family: 'Aller-Light'; }
   .table_striped tbody>tr:nth-of-type(odd) {background-color:#fff;}
   .table_striped tbody>tr{ background: #e8e9ed }
   .bg-transparent tr{ background:transparent!important; }
.checkbox-custom{ margin: 0!important; }
.checkbox-custom span.cheak{content:''; display: block; border: 2px solid #4dbfb6;  height: 50px; width: 50px; position: relative; top: 0px; left: 0; border:8px solid #e8e9ed; border-radius: 6px;  background: #fff;    box-shadow: -1px -1px 4px rgb(0 0 0 / 32%), inset -1px -1px 3px rgb(0 0 0 / 32%);}
.checkbox-custom span.cheak:after{ content: "";  position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%); -webkit-transform: translate(-50%,-50%); width: 20px; height: 20px; background: #170da1; display: none; }
.checkbox-custom span.cheak:after{ display: block; }
.checkbox-custom span.cheak{ box-shadow: inset 0 0 4px 0px rgba(0,0,0,0.5); -webkit-box-shadow: inset 0 0 4px 0px rgba(0,0,0,0.5); }

.btn.btn-info{ background: #E8E9ED;
    box-shadow: -3px -3px 4px #5E658699;
    min-width: 270px;
    padding: 8px 10px;
    border: none;
    font-family: 'Aller-Bold';
    border-radius: 0; }
    .btn span {
    display: block;
    width: 100%;
    box-shadow: 1px 1px 2px #5E658699;
    font-size: 20px;
    text-transform: uppercase;
    padding: 6px;
    letter-spacing: 2px;background: #10069F;
    color: #ffffff;
}
</style>

<script type="text/javascript">
  $('#title li').on('click', function() {
    var getValue = $(this).text();
    //alert(getValue);
    $('#dLabel').text(getValue);
    $('.title-field').val(getValue);
  });
</script>