<section class="banner-section anmeldung">
   <div class="container-fluid">
      <div class="banner-img">
         <img src="<?php echo $this->config->item('front_assets'); ?>images/banner-img-3.png">
      </div>
   </div>
</section>
<section class="same-section">
         <div class="container-fluid">
               <div class="row">
                  <div class="col-md-7 offset-lg-1" style="padding-left: 80px;">
                     <div class="same-heading small-heading-p">
                        <h2>BESTÄTIGUNG IHRER REGISTRIERUNG!</h2>
                        <?php if ($herr == 'Herr') { ?>                           
                           <p>Sehr geehrter <?php echo ucfirst($herr). ' '. $title.' '. $dr_name; ?>,</p>
                        <?php } else { ?>
                           <p>Sehr geehrte <?php echo ucfirst($herr). ' '. $title.' '. $dr_name; ?>,</p>
                        <?php } ?>
                        <p>FALLS SIE DER VEREINBARUNG PERSÖNLICH ZUSTIMMEN, BESTÄTIGEN SIE DIES BITTE INDEM SIE DEN UNTENSTEHENDEN BUTTON KLICKEN. DAMIT WERDEN SIE IN UNSERER DATENBANK ALS CANNAXAN ARZT-APP NUTZER REGISTRIERT. SIE KÖNNEN DIE VEREINBARUNG GEMÄß §13 ABS. 3 TMG JEDERZEIT WIDERRUFEN. DEN WIDERRUF SENDEN SIE BITTE MIT ANGABE IHRER VOLLSTÄNDIGEN KONTAKTDATEN AN <a href="mailto:info@cannaxan.de" class="color-red">INFO@CANNAXAN.DE.</a></p>
                        <p> Mit freundlichen GrÜ<span class="text-lowercase">ß</span>en</p>
                        <p class="pb-3"> Ihr CannaXan Team</p>
                     </div>
                  </div>
                  <div class="col-md-4 align-self-end">
                     <div class="custom-btn w-100 text-right">
                        <form action="<?php echo base_url('doctors/doctor_verification?key='.$key); ?>" method="get" class="ajax_form">
                           <button type="submit" class="btn btn-primary"><span>BESTÄTIGUNG DER <br> REGISTRIERUNG</span></button>
                        </form>
                     </div>
                  </div>
               </div>
         </div>
      </section>