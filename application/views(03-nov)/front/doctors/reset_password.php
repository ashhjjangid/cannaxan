<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>CannaXan</title>
      <link rel="icon" href="<?php echo $this->config->item('front_assets'); ?>/images/favicon.png" type="image">
      <link rel="icon" href="<?php echo $this->config->item('front_assets'); ?>images/favicon.ico" type="image">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>css/bootstrap.min.css"/>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>css/style.css"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>css/media.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   </head>
   <body>
      <header>
         <div class="container-fluid">
            <div class="header-box">
               <div class="row align-items-center">
                   <div class="col">
                     <div class="nav-box">
                        <ul>
                           <li><a href="<?php echo base_url('doctors/register'); ?>">Registrierung</a></li>
                           <li><a href="<?php echo base_url('doctors/login'); ?>">Anmeldung</a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col logo-width">
                     <div class="logo-box text-right">
                        <a href="index.html">
                           <img src="<?php echo $this->config->item('front_assets'); ?>images/logo.png" alt="logo"/>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>

      <section class="banner-section anmeldung">
         <div class="container-fluid">
            <div class="banner-img">
               <img src="<?php echo $this->config->item('front_assets'); ?>images/banner-img-2.png">
            </div>
         </div>
      </section>

      <section class="same-section">
         <div class="container-fluid">
               <form method="post">
            <div class="row align-items-end">
                  <div class="col-md-8">
                     <div class="input-fill-box">
                        <div class="logo-box text-center">
                           <img src="<?php echo $this->config->item('front_assets'); ?>images/logo.png" alt="logo">
                        </div>
                        <div class="row align-items-center">
                           <div class="col-4">
                              <label>Benutzername:</label>
                           </div>
                           <div class="col-8">
                              <input type="`text" name="email" placeholder="arzt@onkeldoktor.de" class="form-control">
                           </div>
                        </div>
                        <div class="row align-items-center">
                           <div class="col-4">
                              <label>password:</label>
                           </div>
                           <div class="col-8">
                              <input type="password" name="password" placeholder="123" class="form-control">
                           </div>
                        </div>
                        <div class="foroget-password-cover">
                           <a href="<?php echo base_url('doctors/forgetpassword') ?>" class="foroget-password" id="foroget_password">Passwort vergessen?</a>
                              
                           <div id="foroget_password_box" style="display: none;">
                              <h4>Passwort</h4>
                              <p>Bitte geben Sie hier Ihr neues Passwort ein. Zur Gewärung einer besseren Datensicherheit muss es aus minimum acht Zeichen bestehen. Das Passwort muss min. ein Sonderzeichen sowie eine Zahl enthalten.</p>
                              <div class="foroget-password-input">
                                 <div class="form-group">
                                    <label>Neues Passwort</label>
                                    <input type="password" name="new_password" class="form-control">
                                 </div>
                                 <div class="form-group">
                                    <label>Passwort Bestätigung</label>
                                    <input type="password" name="confirm_password" class="form-control">
                                 </div>
                                 <div class="form-group mb-0">
                                    <button type="submit" class="btn btn-info"><span>Speichern &amp; weiter</span></button>
                                 </div>
                              </div>
                           </div>

                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="custom-btn">
                        <button type="submit" class="btn btn-info"><span>Anmelden</span></button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </section>

      <footer>
         <div class="container-fluid">
            <div class="footer-box">
               <div class="container">
                  <div class="row">
                     <div class="col-md-4">
                        <div class="footer-nav">
                           <ul>
                              <li><a href="#">AGB</a></li>
                              <li><a href="#">Impressum</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="footer-nav text-center"> 
                           <ul>
                              <li>Tel.:<a href="#">+49 8024 46869 70</a></li>
                              <li>Fax:<a href="#">+49 8024 46869 99</a></li>
                              <li><a href="#">CannaXan GmbH | Birkerfeld 12 | 83627 Warngau</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="footer-nav text-right">
                           <ul>
                              <li><a href="#">info@cannaxan.de</a></li>
                              <li><a href="#">Datenschutz</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <script type="text/javascript" src="<?php echo $this->config->item('front_assets'); ?>js/common.js"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/front/'); ?>js/jquery.form.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script type="text/javascript" src="<?php echo $this->config->item('front_assets'); ?>js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo $this->config->item('front_assets'); ?>js/pagescript.js"></script>
      <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
   </body>
   <script type="text/javascript">
      

   </script>
</html>