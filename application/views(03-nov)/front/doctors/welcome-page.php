      <section class="banner-section anmeldung">
         <div class="container-fluid">
            <div class="banner-img">
               <img src="<?php echo $this->config->item('front_assets'); ?>images/banner-img-3.png">
            </div>
         </div>
      </section>
<section class="same-section">
         <div class="container-fluid">
               <div class="row">
                  <div class="col-md-1">
                  </div>
                  <div class="col-md-7">
                     <div class="same-heading small-heading-p">
                        <h2 class="text-center">Willkommen!</h2>
                        <?php if ($herr == 'Herr') { ?>                           
                           <p>Sehr geehrter <?php echo ucfirst($herr).$title.' '. $dr_name; ?>,</p>
                        <?php } else { ?>
                           <p>Sehr geehrte <?php echo ucfirst($herr).$title.' '. $dr_name; ?>,</p>
                        <?php } ?>
                        <p> Vielen Dank für die Registrierung bei der CannaXan Arzt-App. Nach Überprüfung Ihrer Angaben, schalten wir Sie für die Nutzung der CannaXan Arzt-App frei und senden Ihnen Ihr Passwort zu, dass Sie bitte bei der ersten Anmeldung ändern.</p>
                        <p> Mit freundlichen GrÜ<span class="text-lowercase">ß</span>en</p>
                        <p class="pb-3"> Ihr CannaXan Team</p>
                     </div>
                  </div>
                  <div class="col-md-4 align-self-end">
                     <div class="custom-btn w-100 text-right">
                        <a href="<?php echo base_url('doctors/login')?>" class="btn btn-primary"><span>Weiter</span></a>
                     </div>
                  </div>
               </div>
         </div>
      </section>