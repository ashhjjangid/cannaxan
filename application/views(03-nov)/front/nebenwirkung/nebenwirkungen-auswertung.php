
      <section class="same-section begleiterkrankungen-section">
         <div class="container-fluid">
            <div class="same-heading ">
              <h2 class="text-left">Nebenwirkung Auswertung</h2>
            </div>
            <div class="row">
              <!-- <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-6">
                           <label>Datum</label>
                        </div>
                        <div class="col-sm-6">
                           <input type="text" name="" class="form-control text-center" value="<?php echo $datum; ?>">
                        </div>
                     </div>
                  </div>
               </div> -->
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-6">
                           <label>BEHANDLUNGSTAG</label>
                        </div>
                        <div class="col-sm-6">
                           <!-- <input type="text" name="" class="form-control text-center" placeholder="21"> -->
                           <input type="text" name="behandungstag" class="form-control text-center" placeholder="" value="<?php echo $behandungstag; ?>">
                        </div>
                     </div>
                  </div>
               </div>
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-6">
                           <label>Arzneimittel</label>
                        </div>
                        <div class="col-sm-6">
                           <input type="text" name="arzneimittel" class="form-control text-center" placeholder="" value="<?php echo $arzneimittel; ?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4"></div>
               </div>
               <div class="col-md-4"></div>
            </div>
            <div class="row">
              <!-- <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-6">
                           <label>Visite</label>
                        </div>
                        <div class="col-sm-6">
                           <input type="text" name="visite" value="<?php echo $visite; ?>" class="form-control text-center visit-number" placeholder="" readonly>
                        </div>
                     </div>
                  </div>
               </div> -->
               <!--  <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-6">
                           <label>Arzneimittel</label>
                        </div>
                        <div class="col-sm-6">
                           <input type="text" name="arzneimittel" class="form-control text-center" placeholder="" value="<?php echo $arzneimittel; ?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4"></div>
               </div> -->
               <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-9">
                          <label>kausal bedingte Nebenwirkung</label>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="keine_kausal_bedingte" class="form-control text-center" placeholder="nein" value="<?php echo $keine_kausal_bedingte; ?>">
                          </div>
                       </div>
                    </div>
                    </div>
                    <div class="col-md-3"></div>
                  </div>
                </div>
               </div>
            </div>

            <div class="container">
              <div class="tabel-overlap Wochen-table">
                <table cellspacing="0"  cellpadding="0" width="100%" class="table table-striped table-bordered site-table">
                    <thead>
                      <tr>
                        <th>NEBENWIRKUNG</th>
                        <?php $total_visit =  ($patient_visits)?count($patient_visits):'0';?>
                        <th colspan="<?php echo $total_visit; ?>" class="p-0">
                         <table cellpadding="0" cellspacing="0" width="100%" align="center">
                            <tr>
                               <th colspan="<?php echo $total_visit; ?>"> <center>VISTIENNUMMER</center></th>
                            </tr>
                            <tr>
                              <?php foreach ($patient_visits as $key => $value) { ?>
                                <th style=""><?php echo $value->visite; ?></th>
                              <?php } ?>
                               <!-- <th style="">2</th>
                               <th style="">3</th>
                               <th style="">4</th>
                               <th style="">5</th>
                               <th style="">6</th>
                               <th style="">7</th> -->
                            </tr>
                         </table></th>
                        <th style="text-align: center;">KAUSALITÄT ZUR<br> THERAPIE VORHANDEN</th>
                      </tr>
                    </thead>
                   <?php foreach ($records as $key => $value) { ?>

                   <tr>
                      <td><?php echo $value->nebenwirkung_name; ?></td>

                      <?php foreach ($patient_visits as $k => $v) { ?>
                        <td style="text-align: center;">
                          <?php 
                            
                            if ($visit_array && isset($visit_array[$v->visite])){ 
                              
                              if (isset($visit_array[$v->visite][$value->id])){
                                echo $visit_array[$v->visite][$value->id];
                              }  
                            } 
                          ?>
                        </td>
                      <?php } ?>

                      <!-- <td style="text-align: center;">
                        <?php 
                          
                          if ($visit_array && isset($visit_array[1])){ 
                            
                            if (isset($visit_array[1][$value->id])){
                              echo $visit_array[1][$value->id];
                            }  
                          } 
                        ?>
                      </td>
                      <td style="text-align: center;">
                        <?php 
                          
                          if ($visit_array && isset($visit_array[2])){ 
                            
                            if (isset($visit_array[2][$value->id])){
                              echo $visit_array[2][$value->id];
                            }  
                          } 
                        ?>
                      </td>
                      <td style="text-align: center;">
                        <?php 
                          
                          if ($visit_array && isset($visit_array[3])){ 
                            
                            if (isset($visit_array[3][$value->id])){
                              echo $visit_array[3][$value->id];
                            }  
                          } 
                        ?>
                      </td>
                      <td style="text-align: center;">
                        <?php 
                          
                          if ($visit_array && isset($visit_array[4])){ 
                            
                            if (isset($visit_array[4][$value->id])){
                              echo $visit_array[4][$value->id];
                            }  
                          } 
                        ?>
                      </td>
                      <td style="text-align: center;">
                        <?php 
                          
                          if ($visit_array && isset($visit_array[5])){ 
                            
                            if (isset($visit_array[5][$value->id])){
                              echo $visit_array[5][$value->id];
                            }  
                          } 
                        ?>
                      </td>
                      <td style="text-align: center;">
                        <?php 
                          
                          if ($visit_array && isset($visit_array[6])){ 
                            
                            if (isset($visit_array[6][$value->id])){
                              echo $visit_array[6][$value->id];
                            }  
                          } 
                        ?>
                      </td>
                      <td style="text-align: center;">
                        <?php 
                          
                          if ($visit_array && isset($visit_array[7])){ 
                            
                            if (isset($visit_array[7][$value->id])){
                              echo $visit_array[7][$value->id];
                            }  
                          } 
                        ?>
                      </td> -->

                      <td style="text-align: center;">

                        <?php foreach ($table_data_exists as $ke => $val) {  ?>
                          
                          <?php if ($value->id == $val->nebenwirkung_name) { ?>
                             <?php echo ($val->kausalitat_zur_therapie_vorhanden)?$val->kausalitat_zur_therapie_vorhanden:'nein'; ?>
                        <!-- <div class="dropdown select-box select_kausalitat_zur_therapie_vorhanden_box">
                                <button class="dropdown-select kausalitat_zur_therapie_vorhanden_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <?php echo ($val->kausalitat_zur_therapie_vorhanden)?$val->kausalitat_zur_therapie_vorhanden:'nein'; ?>
                                  <span><i class="fas fa-chevron-down"></i></span>
                                </button>
                                <ul class="dropdown-menu kausalitat_zur_therapie_vorhanden_box" aria-labelledby="kausalitat_zur_therapie_vorhanden_btn">
                                  <li value="ja" onclick="select_kausalitat_zur_therapie_vorhanden_box(this)">ja</li>
                                  <li value="nein" onclick="select_kausalitat_zur_therapie_vorhanden_box(this)">nein</li>
                                </ul>
                            </div>
                            <input type="hidden" name="data[<?php echo $key; ?>][kausalitat_zur_therapie_vorhanden]" class="kausalitat_zur_therapie_vorhanden-field" value="<?php echo ($val->kausalitat_zur_therapie_vorhanden)?$val->kausalitat_zur_therapie_vorhanden:'nein'; ?>"> -->
                       <!-- <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select> -->
                          
                        <?php break; } } ?>
                      </td>
                   </tr>
                   <?php } ?>
                   <!-- <tr>
                      <td>Erbrechen</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center; ">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                   <tr>
                      <td>Ubelkeit</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center; "><span style="background:#F76816; display:block; height:20px;"></span></td>
                      <td style="text-align: center;"><span style="background:#FCAF16; display:block; height:20px;"></span></td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                   <tr>
                      <td>Erbrechen</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center; ">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                    <tr>
                      <td>Erbrechen</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center; ">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                    <tr>
                      <td>Erbrechen</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center; ">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr> -->
                   
                </table>
             </div> 
           <div class="tabel-view-box text-right">
             <ul>
               <li><span style="background: #2CA61E;"></span>mild</li>
               <li><span style="background:#FCAF16; "></span>moderat</li>
               <li><span style="background: #F76816;"></span>schwer</li>
               <li><span style="background:#D91313 "></span>lebensbedrohlich</li>
             </ul>
           </div>
            </div>
         </div>
      </section>
<script>
    function select_kausalitat_zur_therapie_vorhanden_box($this) {

  var getText = $($this).text(); 
  var getValue = $($this).attr('value');

  $($this).closest('tr').find('.kausalitat_zur_therapie_vorhanden_btn').text(getText);     
  $($this).closest('tr').find('.kausalitat_zur_therapie_vorhanden-field').val(getValue); 
  }
  </script>
