<div class="same-section">  
    <div class="container-fluid">   
        <div class="same-heading mb-4">
            <h2>Arztfragebogen 2/6</h2>
        </div>
        <form method="post" action="<?php echo current_url().'?in='.$insured_number.'&qid='.$qid; ?>" class="form-horizontal ajax_form">
          <div class="min-height">
            <div class="row_erkrankung">
              <h5 class="mb-3 simple-txt"><strong class="same-font-size">Ist die Erkrankung schwerwiegend?</strong></h5>
              <div class="radio-box normal-radio">
                 <input type="radio" class="select-schwerwiegend" name="erkrankung_schwerwiegend" onclick="selectoptions()" <?php echo ($erkrankung_schwerwiegend == 'Yes')?'checked':''; ?> value="Yes" id="Ja">
                 <label for="Ja">
                    <span class="radio-cheak"></span>
                    ja
                 </label>
              </div>

              <div class="radio-box normal-radio erkrankung-popup">
                 <input type="radio" class="select-schwerwiegend" name="erkrankung_schwerwiegend" onclick="selectoptions()" <?php echo ($erkrankung_schwerwiegend == 'No')?'checked':''; ?> value="No" id="nein">
                 <label for="nein">
                    <span class="radio-cheak"></span>
                    nein
                 </label>
                 <div class="dropmenu-box">
                    <div class="drop-heading">
                      <h3>ACHTUNG: </h3>
                      <p>Patient ist nicht für eine Cannabis Therapie geeignet</p>
                    </div> 
                  </div>
              </div>
            </div>
            <div class="erkrankung-schwerwiegend-multiple-option">
                <!-- <h5 class="mb-4 mt-3 simple-txt">Falls ja:</h5> -->
                <div class="checkbox-custom normal-radio">
                   <input type="checkbox" name="endgradig_chronifizierte_number" <?php echo ($endgradig_chronifizierte_number)?'checked':''; ?> value="1" id="falls">
                   <label for="falls">
                      <span class="cheak"></span>
                      Endgradig chronifizierte, schwerste Schmerzen mit einem Schmerzwert von <input type="text" name="nrs_skala" maxlength="2" oninput="this.value = this.value.replace(/[^0-9]/g, '');" value="<?php echo $endgradig_chronifizierte_number; ?>" class="form-control red-input"> auf <br> einer NRS Skala von 0 bis 10.
                   </label>
                </div>
                <div class="checkbox-custom normal-radio">
                   <input type="checkbox" name="aufhebung_der_schlafarchitektur" <?php echo ($aufhebung_der_schlafarchitektur)?'checked':''; ?> value="2" id="aufhebung">
                   <label for="aufhebung">
                      <span class="cheak"></span>
                      Aufhebung der Schlafarchitektur
                   </label>
                </div>
                <div class="checkbox-custom normal-radio">
                   <input type="checkbox" name="depressive_entwicklung" <?php echo ($depressive_entwicklung)?'checked':''; ?> value="3" id="Depressive">
                   <label for="Depressive">
                      <span class="cheak"></span>
                      Depressive Entwicklung
                   </label>
                </div>
                <div class="checkbox-custom normal-radio">
                   <input type="checkbox" name="latente_suizidalitat" <?php echo ($latente_suizidalitat)?'checked':''; ?> value="4" id="latente">
                   <label for="latente">
                      <span class="cheak"></span>
                      latente Suizidalität
                   </label>
                </div>
                <div class="checkbox-custom normal-radio">
                   <input type="checkbox" onclick="selectedBeruflichenTatigkeit()" name="beruflichen_tatigkeit" <?php echo ($beruflichen_tatigkeit)?'checked':''; ?> value="5" id="beruflichen">
                   <label for="beruflichen">
                      <span class="cheak"></span>
                      Einschränkung der beruflichen Tätigkeit:
                   </label>
                   <div class="inline-radio beruflichen-radiobtn">
                       <ul>
                         <li>
                            <div class="radio-box normal-radio">
                               <input type="radio" name="beruflichen" <?php echo ($beruflichen_tatigkeit == 'keine')?'checked':''; ?> value="keine" id="keine">
                               <label for="keine">
                                  <span class="radio-cheak"></span>
                                  keine
                               </label>
                            </div>
                         </li>
                         <li>
                            <div class="radio-box normal-radio">
                               <input type="radio" name="beruflichen" <?php echo ($beruflichen_tatigkeit == 'mittel')?'checked':''; ?> value="mittel" id="mittel">
                               <label for="mittel">
                                  <span class="radio-cheak"></span>
                                  mittel
                               </label>
                            </div>
                         </li>
                         <li>
                            <div class="radio-box normal-radio">
                               <input type="radio" name="beruflichen" <?php echo ($beruflichen_tatigkeit == 'schwer')?'checked':''; ?> value="schwer" id="schwer">
                               <label for="schwer">
                                  <span class="radio-cheak"></span>
                                  schwer
                               </label>
                            </div>
                         </li>
                         <li>
                            <div class="radio-box normal-radio">
                               <input type="radio" name="beruflichen" <?php echo ($beruflichen_tatigkeit == 'Aufgabe der beruflichen Tätigkeit')?'checked':''; ?> value="Aufgabe der beruflichen Tätigkeit" id="Aufgabe">
                               <label for="Aufgabe">
                                  <span class="radio-cheak"></span>
                                  Aufgabe der beruflichen Tätigkeit
                               </label>
                            </div>
                         </li>
                        </ul>
                   </div>
                </div>
                <div class="checkbox-custom normal-radio">
                   <input type="checkbox" name="bettlagerigkeit_aufgrund_schmerzen" <?php echo ($bettlagerigkeit_aufgrund_schmerzen)?'checked':''; ?> value="6" id="Zunehmende">
                   <label for="Zunehmende">
                      <span class="cheak"></span>
                      Zunehmende Bettlägerigkeit aufgrund Schmerzen Kurze Beschreibung der Grunderkrankung
                   </label>
                </div>
                <div class="normal-radio" id="Zunehmende-txtarea">
                  <label for="Zunehmende-txtarea"><strong class="same-font-size">Kurze Beschreibung der Grunderkrankung</strong></label>
                   <textarea class="form-control" name="zunehmende_bettlaerigkeit"><?php echo $bettlagerigkeit_aufgrund_schmerzen; ?></textarea>
                </div>

                <div class="checkbox-custom normal-radio mt-4">
                   <input type="checkbox" name="die_gesamtsymptomatik" <?php echo ($die_gesamtsymptomatik)?'checked':''; ?> value="7" id="Gesamtsymptomatik">
                   <label for="Gesamtsymptomatik">
                      <span class="cheak"></span>
                      Die Gesamtsymptomatik erfüllt die Bedingungen einer palliativen Begleitung im Endstadium
                   </label>
                </div>

                <div class="checkbox-custom normal-radio">
                   <input type="checkbox" name="die_lebenserwartung" <?php echo ($die_lebenserwartung)?'checked':''; ?> value="8" id="Lebenserwartung">
                   <label for="Lebenserwartung">
                      <span class="cheak"></span>
                      Die Lebenserwartung der/Patientin, des Patienten wird auf ca. <input type="text" name="lebenserwartung_patienten" value="<?php echo $die_lebenserwartung; ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" class="form-control red-input"> Monate geschätzt.
                   </label>
                </div>
            </div>
          </div>
          <div class="btn-group-custom text-right">
            <a class="btn btn-primary back" href="<?php echo base_url('arztfragebogen/step-1?in='.$insured_number.'&qid='. $qid); ?>"><span><i class="fas fa-long-arrow-alt-left"></i> Zurück</span></a>
            <button type="submit" class="btn btn-primary next-btn"><span>Weiter <i class="fas fa-long-arrow-alt-right"></i></span></button>
          </div>
        </form>
    </div>
</div>

<script>
    $(function(){
        selectoptions();
        selectedBeruflichenTatigkeit();
        
    })
    function selectoptions() {
        var selectedOption = $('input[name="erkrankung_schwerwiegend"]:checked').val();
        //alert(selectedOption);
        $('.next-btn').hide();
        if (selectedOption == 'Yes') {
            $('.erkrankung-schwerwiegend-multiple-option').css('display', 'block');
            $('.dropmenu-box').css('display', 'none');
            $('.next-btn').show();
        } else {
            $('.erkrankung-schwerwiegend-multiple-option').css('display', 'none');
            $('.next-btn').hide();
            if (selectedOption == 'No') {

            	$('.dropmenu-box').css('display', 'block');
            }
        }
    }

    function selectedBeruflichenTatigkeit() {
        var selectedOption = $('input[name="beruflichen_tatigkeit"]:checked').val();
        //alert(selectedOption);
        if (selectedOption == '5') {
            $('.beruflichen-radiobtn').css('display', 'block');
        } else {
            $('.beruflichen-radiobtn').css('display', 'none');
        }
    }

    function selectedBeruflichenTatigkeit() {
        var selectedOption = $('input[name="beruflichen_tatigkeit"]:checked').val();
        //alert(selectedOption);
        if (selectedOption == '5') {
            $('.beruflichen-radiobtn').css('display', 'block');
        } else {
            $('.beruflichen-radiobtn').css('display', 'none');
        }
    }

    /*function selectZunehmende() {
        var selectedOption = $('input[name="bettlagerigkeit_aufgrund_schmerzen"]:checked').val();
        //alert(selectedOption);
        if (selectedOption == '6') {
            $('#Zunehmende-txtarea').css('display', 'block');
        } else {
            $('#Zunehmende-txtarea').css('display', 'none');
        }
    }*/
</script>