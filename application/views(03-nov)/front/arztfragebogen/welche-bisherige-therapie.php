<div class="same-section min-height-high">
  <div class="container-fluid"> 
    <form method="post" class="form-horizontal ajax_form" action="<?php echo current_url().'?in='.$insured_number.'&qid='.$qid; ?>">
    <div class="same-heading mb-4">
      <h2>Arztfragebogen 4/6</h2>
    </div>
    <h5 class="mb-4 simple-txt"><strong>Welche weiteren Therapien werden zurzeit durchgeführt?</strong></h5>
    <div class="inline-radio">
      <ul>
        <li>          
          <div class="checkbox-custom normal-radio">
             <input type="checkbox" name="physiotherapie_1" <?php echo ($physiotherapie_1 == 'Physiotherapie')?'checked':''; ?> value="Physiotherapie" id="Physiotherapie_1">
             <label for="Physiotherapie_1">
                <span class="cheak"></span>
                Physiotherapie
             </label>
          </div>
        </li>
        <li>          
          <div class="checkbox-custom normal-radio">
             <input type="checkbox" name="interventionelle_verfahren" <?php echo ($interventionelle_verfahren == 'Interventionelle Verfahren')?'checked':''; ?> value="Interventionelle Verfahren" id="interventionelle_verfahren">
             <label for="interventionelle_verfahren">
                <span class="cheak"></span>
                Interventionelle Verfahren
             </label>
          </div>
        </li>
        <li>          
          <div class="checkbox-custom normal-radio">
             <input type="checkbox" name="psychotherapie_2" <?php echo ($psychotherapie_2 == 'Psychotherapie')?'checked':''; ?> value="Psychotherapie" id="Psychotherapie_2">
             <label for="Psychotherapie_2">
                <span class="cheak"></span>
                Psychotherapie
             </label>
          </div>
        </li>
        <li>          
          <div class="checkbox-custom normal-radio">
             <input type="checkbox" name="tens" <?php echo ($tens == 'TENS')?'checked':''; ?> value="TENS" id="tens">
             <label for="tens">
                <span class="cheak"></span>
                TENS
             </label>
          </div>
        </li>
        <li>          
          <div class="checkbox-custom normal-radio">
             <input type="checkbox" name="akupunktur" <?php echo ($akupunktur == 'Akupunktur')?'checked':''; ?> value="Akupunktur" id="akupunktur">
             <label for="akupunktur">
                <span class="cheak"></span>
                Akupunktur
             </label>
          </div>
        </li>
        <li>          
          <div class="checkbox-custom normal-radio">
             <input type="checkbox" name="keine" <?php echo ($keine == 'Keine')?'checked':''; ?> value="Keine" id="keine">
             <label for="keine">
                <span class="cheak"></span>
                Keine
             </label>
          </div>
        </li>
      </ul>
    </div>
    <h5 class="mb-4 mt-4 simple-txt"><strong>Welche bisherige Therapie ist bei der Erkrankung unter 3. mit welchem Erfolg durchgeführt worden?</strong></h5>
    <div class="checkbox-custom normal-radio">
       <input type="checkbox" name="who_stufe1" <?php echo (isset($who_1_patient_answer) && !empty($who_1_patient_answer) && $who_1_patient_answer)?'checked':''; ?> value="Yes" id="who_stufe1" onclick="showWhoStufeMain(this)" data-class-name="who-stufe-1-main">
       <label for="who_stufe1">
          <span class="cheak"></span>
          WHO Stufe 1
       </label>
       <div class="normal-radio-inner who-stufe-1-main" style="display: <?php echo (isset($who_1_patient_answer) && !empty($who_1_patient_answer) && $who_1_patient_answer)?'block':'none'; ?>;">
        <div class="radio-box normal-radio">
          <input type="checkbox" class="select-nicht-opioidanalgetika" <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->option_name == 'Nicht-Opioidanalgetika')?'checked':''; ?> onclick="showWhoStufeChild(this)" data-class-name="nicht-opioidanalgetika-child" name="nicht_opioidanalgetika" value="Nicht-Opioidanalgetika" id="Nicht-Opioidanalgetika">
          <label for="Nicht-Opioidanalgetika">
            <span class="radio-cheak"></span>
            Nicht-Opioidanalgetika
          </label>
          <div class="medikament-main-row normal-radio-inner nicht-opioidanalgetika-child" style="display: <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->option_name == 'Nicht-Opioidanalgetika')?'block':'none'; ?>;">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_1)?$nichtPatientAnswers->medikament_dropdown_1:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Acetylsalicylsäure" data-toggle="tooltip" data-placement="bottom" title="Acetylsalicylsäure" onclick="selectMedikamentFirst(this)">Acetylsalicylsäure</li>
                      <li value="Celecoxib" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Celecoxib</li>
                      <li value="Diclofenac" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Diclofenac</li>
                      <li value="Indometacin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Indometacin</li>
                      <li value="Ibuprofen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Ibuprofen</li>
                      <li value="Ketoprofen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Ketoprofen</li>
                      <li value="Metamizol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Metamizol</li>
                      <li value="Naproxen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Naproxen</li>
                      <li value="Paracetamol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Paracetamol</li>
                      <li value="Parecoxib" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Parecoxib</li>
                      <li value="Phenazon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Phenazon</li>
                      <li value="Piroxicam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Piroxicam</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="nicht_opioidanalgetika_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_1)?$nichtPatientAnswers->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_1 && $nichtPatientAnswers->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="nicht_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_1_text_field)?$nichtPatientAnswers->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_2)?$nichtPatientAnswers->medikament_dropdown_2:''; ?>                     
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectMedikamentSecond(this)"></li>
                      <li value="Acetylsalicylsäure" data-toggle="tooltip" data-placement="bottom" title="Acetylsalicylsäure" onclick="selectMedikamentSecond(this)">Acetylsalicylsäure</li>
                      <li value="Celecoxib" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Celecoxib</li>
                      <li value="Diclofenac" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Diclofenac</li>
                      <li value="Indometacin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Indometacin</li>
                      <li value="Ibuprofen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Ibuprofen</li>
                      <li value="Ketoprofen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Ketoprofen</li>
                      <li value="Metamizol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Metamizol</li>
                      <li value="Naproxen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Naproxen</li>
                      <li value="Paracetamol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Paracetamol</li>
                      <li value="Parecoxib" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Parecoxib</li>
                      <li value="Phenazon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Phenazon</li>
                      <li value="Piroxicam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Piroxicam</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                     </ul>
                   </div>
                   <input type="hidden" name="nicht_opioidanalgetika_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_2)?$nichtPatientAnswers->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_2 && $nichtPatientAnswers->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>">
                <div class="form-group">
                  <input type="text" name="nicht_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_2_text_field)?$nichtPatientAnswers->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_3)?$nichtPatientAnswers->medikament_dropdown_3:''; ?>                     
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectMedikamentThird(this)"></li>
                      <li value="Acetylsalicylsäure" data-toggle="tooltip" data-placement="bottom" title="Acetylsalicylsäure" onclick="selectMedikamentThird(this)">Acetylsalicylsäure</li>
                      <li value="Celecoxib" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Celecoxib</li>
                      <li value="Diclofenac" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Diclofenac</li>
                      <li value="Indometacin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Indometacin</li>
                      <li value="Ibuprofen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Ibuprofen</li>
                      <li value="Ketoprofen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Ketoprofen</li>
                      <li value="Metamizol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Metamizol</li>
                      <li value="Naproxen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Naproxen</li>
                      <li value="Paracetamol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Paracetamol</li>
                      <li value="Parecoxib" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Parecoxib</li>
                      <li value="Phenazon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Phenazon</li>
                      <li value="Piroxicam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Piroxicam</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                     </ul>
                   </div>
                   <input type="hidden" name="nicht_opioidanalgetika_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_3)?$nichtPatientAnswers->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_3 && $nichtPatientAnswers->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="nicht_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->medikament_dropdown_3_text_field)?$nichtPatientAnswers->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="nicht_therapieeflog" <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_2">
              <label for="keine_ausreichende_2">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="nicht_therapieeflog" <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_2">
              <label for="erfolglos_2">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->gastrointestinal_dropdown)?$nichtPatientAnswers->gastrointestinal_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="nicht_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->gastrointestinal_dropdown)?$nichtPatientAnswers->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->gastrointestinal_dropdown && $nichtPatientAnswers->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="nicht_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->gastrointestinal_dropdown_text_field)?$nichtPatientAnswers->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->kardial_dropdown)?$nichtPatientAnswers->kardial_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="nicht_kardial_input" class="kardial-field" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->kardial_dropdown)?$nichtPatientAnswers->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->kardial_dropdown && $nichtPatientAnswers->kardial_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="nicht_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->kardial_dropdown_text_field)?$nichtPatientAnswers->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->nephrologisch_dropdown)?$nichtPatientAnswers->nephrologisch_dropdown:''; ?>                
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="nicht_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->nephrologisch_dropdown)?$nichtPatientAnswers->nephrologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->nephrologisch_dropdown && $nichtPatientAnswers->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="nicht_nephrologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->nephrologisch_dropdown_text_field)?$nichtPatientAnswers->nephrologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->neurologisch_dropdown)?$nichtPatientAnswers->neurologisch_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="nicht_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->neurologisch_dropdown)?$nichtPatientAnswers->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->neurologisch_dropdown && $nichtPatientAnswers->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="nicht_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($nichtPatientAnswers) && !empty($nichtPatientAnswers) && $nichtPatientAnswers->neurologisch_dropdown_text_field)?$nichtPatientAnswers->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="radio-box normal-radio">
          <input type="checkbox" class="select-schwerwiegend" onclick="showWhoStufeChild(this)" data-class-name="steroide-child" name="steroide" <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->option_name == 'Steroide')?'checked':''; ?> value="Steroide" id="Steroide">
          <label for="Steroide">
            <span class="radio-cheak"></span>
            Steroide
          </label>
          <div class="medikament-main-row normal-radio-inner steroide-child" style="display: <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->option_name == 'Steroide')?'block':'none'; ?>;">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_1)?$steroidePatientAnswers->medikament_dropdown_1:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Betamethason" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Betamethason</li>
                      <li value="Cloprednol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Cloprednol</li>
                      <li value="Deflazacort" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Deflazacort</li>
                      <li value="Dexamethason" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Dexamethason</li>
                      <li value="Fluocortolon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Fluocortolon</li>
                      <li value="Hydrocortison (Kortisol)"  data-toggle="tooltip" data-placement="bottom" title="Hydrocortison (Kortisol)" onclick="selectMedikamentFirst(this)">Hydrocortison (Kortisol)</li>
                      <li value="Methylprednisolon" data-toggle="tooltip" data-placement="bottom" title="Methylprednisolon" onclick="selectMedikamentFirst(this)">Methylprednisolon</li>
                      <li value="Prednisolon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Prednisolon</li>
                      <li value="Prednison" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Prednison</li>
                      <li value="Triamcinolon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Triamcinolon</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="steroide_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_1)?$steroidePatientAnswers->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_1 && $steroidePatientAnswers->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="steroide_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_1_text_field)?$steroidePatientAnswers->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_2)?$steroidePatientAnswers->medikament_dropdown_2:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectMedikamentSecond(this)"></li>
                      <li value="Betamethason" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Betamethason</li>
                      <li value="Cloprednol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Cloprednol</li>
                      <li value="Deflazacort" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Deflazacort</li>
                      <li value="Dexamethason" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Dexamethason</li>
                      <li value="Fluocortolon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Fluocortolon</li>
                      <li value="Hydrocortison (Kortisol)" data-toggle="tooltip" data-placement="bottom" title="Hydrocortison (Kortisol)" onclick="selectMedikamentSecond(this)">Hydrocortison (Kortisol)</li>
                      <li value="Methylprednisolon" data-toggle="tooltip" data-placement="bottom" title="Methylprednisolon" onclick="selectMedikamentSecond(this)">Methylprednisolon</li>
                      <li value="Prednisolon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Prednisolon</li>
                      <li value="Prednison" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Prednison</li>
                      <li value="Triamcinolon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Triamcinolon</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="steroide_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_2)?$steroidePatientAnswers->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_2 && $steroidePatientAnswers->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="steroide_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_2_text_field)?$steroidePatientAnswers->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_3)?$steroidePatientAnswers->medikament_dropdown_3:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectMedikamentThird(this)"></li>
                      <li value="Betamethason" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Betamethason</li>
                      <li value="Cloprednol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Cloprednol</li>
                      <li value="Deflazacort" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Deflazacort</li>
                      <li value="Dexamethason" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Dexamethason</li>
                      <li value="Fluocortolon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Fluocortolon</li>
                      <li value="Hydrocortison (Kortisol)" data-toggle="tooltip" data-placement="bottom" title="Hydrocortison (Kortisol)" onclick="selectMedikamentThird(this)">Hydrocortison (Kortisol)</li>
                      <li value="Methylprednisolon" data-toggle="tooltip" data-placement="bottom" title="Methylprednisolon" onclick="selectMedikamentThird(this)">Methylprednisolon</li>
                      <li value="Prednisolon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Prednisolon</li>
                      <li value="Prednison" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Prednison</li>
                      <li value="Triamcinolon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Triamcinolon</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="steroide_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_3)?$steroidePatientAnswers->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_3 && $steroidePatientAnswers->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="steroide_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->medikament_dropdown_3_text_field)?$steroidePatientAnswers->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="steroide_therapieeflog" <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_3">
              <label for="keine_ausreichende_3">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="steroide_therapieeflog" <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_3">
              <label for="erfolglos_3">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->gastrointestinal_dropdown)?$steroidePatientAnswers->gastrointestinal_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                       <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="steroide_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->gastrointestinal_dropdown)?$steroidePatientAnswers->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->gastrointestinal_dropdown && $steroidePatientAnswers->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="steroide_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->gastrointestinal_dropdown_text_field)?$steroidePatientAnswers->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->kardial_dropdown)?$steroidePatientAnswers->kardial_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="steroide_kardial_input" class="kardial-field" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->kardial_dropdown)?$steroidePatientAnswers->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->kardial_dropdown && $steroidePatientAnswers->kardial_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="steroide_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->kardial_dropdown_text_field)?$steroidePatientAnswers->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->nephrologisch_dropdown)?$steroidePatientAnswers->nephrologisch_dropdown:''; ?>                
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="steroide_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->nephrologisch_dropdown)?$steroidePatientAnswers->nephrologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->nephrologisch_dropdown && $steroidePatientAnswers->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="steroide_nephrologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->nephrologisch_dropdown_text_field)?$steroidePatientAnswers->nephrologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->neurologisch_dropdown)?$steroidePatientAnswers->neurologisch_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="steroide_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->neurologisch_dropdown)?$steroidePatientAnswers->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->neurologisch_dropdown && $steroidePatientAnswers->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="steroide_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($steroidePatientAnswers) && !empty($steroidePatientAnswers) && $steroidePatientAnswers->neurologisch_dropdown_text_field)?$steroidePatientAnswers->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="radio-box normal-radio">
          <input type="checkbox" class="select-neuroleptika" onclick="showWhoStufeChild(this)" data-class-name="neuroleptika-child" name="neuroleptika" <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->option_name == 'Neuroleptika')?'checked':''; ?> value="Neuroleptika" id="neuroleptika">
          <label for="neuroleptika">
            <span class="radio-cheak"></span>
            Neuroleptika
          </label>
          <div class="medikament-main-row normal-radio-inner neuroleptika-child" style="display: <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->option_name == 'Neuroleptika')?'block':'none'; ?>;">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_1)?$neuroleptikaPatientAnswers->medikament_dropdown_1:''; ?>                   
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Chlorprothixen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Chlorprothixen</li>
                      <li value="Haloperidol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Haloperidol</li>
                      <li value="Olanzapin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Olanzapin</li>
                      <li value="Promethazin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Promethazin</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="neuroleptika_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_1)?$neuroleptikaPatientAnswers->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_1 && $neuroleptikaPatientAnswers->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="neuroleptika_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_1_text_field)?$neuroleptikaPatientAnswers->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_2)?$neuroleptikaPatientAnswers->medikament_dropdown_2:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)"></li>
                       <li value="Chlorprothixen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Chlorprothixen</li>
                       <li value="Haloperidol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Haloperidol</li>
                       <li value="Olanzapin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Olanzapin</li>
                      <li value="Promethazin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Promethazin</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="neuroleptika_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_2)?$neuroleptikaPatientAnswers->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_2 && $neuroleptikaPatientAnswers->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="neuroleptika_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_2_text_field)?$neuroleptikaPatientAnswers->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_3)?$neuroleptikaPatientAnswers->medikament_dropdown_3:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)"></li>
                       <li value="Chlorprothixen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Chlorprothixen</li>
                       <li value="Haloperidol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Haloperidol</li>
                       <li value="Olanzapin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Olanzapin</li>
                      <li value="Promethazin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Promethazin</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="neuroleptika_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_3)?$neuroleptikaPatientAnswers->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_3 && $neuroleptikaPatientAnswers->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="neuroleptika_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->medikament_dropdown_3_text_field)?$neuroleptikaPatientAnswers->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="neuroleptika_therapieeflog" <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_4">
              <label for="keine_ausreichende_4">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="neuroleptika_therapieeflog" <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_4">
              <label for="erfolglos_4">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->gastrointestinal_dropdown)?$neuroleptikaPatientAnswers->gastrointestinal_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                       <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="neuroleptika_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->gastrointestinal_dropdown)?$neuroleptikaPatientAnswers->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->gastrointestinal_dropdown && $neuroleptikaPatientAnswers->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?> ;">
                <div class="form-group">
                  <input type="text" name="neuroleptika_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->gastrointestinal_dropdown_text_field)?$neuroleptikaPatientAnswers->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->kardial_dropdown)?$neuroleptikaPatientAnswers->kardial_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="neuroleptika_kardial_input" class="kardial-field" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->kardial_dropdown)?$neuroleptikaPatientAnswers->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->kardial_dropdown && $neuroleptikaPatientAnswers->kardial_dropdown == 'Freifeld')?'block':'none'; ?> ;">
                <div class="form-group">
                  <input type="text" name="neuroleptika_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->kardial_dropdown_text_field)?$neuroleptikaPatientAnswers->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->nephrologisch_dropdown)?$neuroleptikaPatientAnswers->nephrologisch_dropdown:''; ?>               
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="neuroleptika_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->nephrologisch_dropdown)?$neuroleptikaPatientAnswers->nephrologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->nephrologisch_dropdown && $neuroleptikaPatientAnswers->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?> ;">
                <div class="form-group">
                  <input type="text" name="neuroleptika_nephrologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->nephrologisch_dropdown_text_field)?$neuroleptikaPatientAnswers->nephrologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->neurologisch_dropdown)?$neuroleptikaPatientAnswers->neurologisch_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="neuroleptika_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->neurologisch_dropdown)?$neuroleptikaPatientAnswers->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->neurologisch_dropdown && $neuroleptikaPatientAnswers->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="neuroleptika_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($neuroleptikaPatientAnswers) && !empty($neuroleptikaPatientAnswers) && $neuroleptikaPatientAnswers->neurologisch_dropdown_text_field)?$neuroleptikaPatientAnswers->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="radio-box normal-radio">
          <input type="checkbox" class="select-antidepressiva" onclick="showWhoStufeChild(this)" data-class-name="antidepressiva-child" name="antidepressiva" <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->option_name == 'Antidepressiva')?'checked':''; ?> value="Antidepressiva" id="antidepressiva">
          <label for="antidepressiva">
            <span class="radio-cheak"></span>
            Antidepressiva
          </label>
          <div class="medikament-main-row normal-radio-inner antidepressiva-child" style="display: <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->option_name == 'Antidepressiva')?'block':'none'; ?>;">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_1)?$antidepressivaPatientAnswers->medikament_dropdown_1:''; ?>                     
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Amitriptylin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Amitriptylin</li>
                      <li value="Clomipramin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Clomipramin</li>
                      <li value="Desipramin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Desipramin</li>
                      <li value="Doxepin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Doxepin</li>
                      <li value="Duloxetin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Duloxetin</li>
                      <li value="Fluoxetin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Fluoxetin</li>
                      <li value="Imipramin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Imipramin</li>
                      <li value="Milnacipran" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Milnacipran</li>
                      <li value="Mirtazapin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Mirtazapin</li>
                      <li value="Nortriptylin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Nortriptylin</li>
                      <li value="Venlafaxin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Venlafaxin</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antidepressiva_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_1)?$antidepressivaPatientAnswers->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_1 && $antidepressivaPatientAnswers->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antidepressiva_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_1_text_field)?$antidepressivaPatientAnswers->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_2)?$antidepressivaPatientAnswers->medikament_dropdown_2:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)"></li>
                      <li value="Amitriptylin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Amitriptylin</li>
                      <li value="Clomipramin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Clomipramin</li>
                      <li value="Desipramin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Desipramin</li>
                      <li value="Doxepin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Doxepin</li>
                      <li value="Duloxetin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Duloxetin</li>
                      <li value="Fluoxetin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Fluoxetin</li>
                      <li value="Imipramin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Imipramin</li>
                      <li value="Milnacipran" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Milnacipran</li>
                      <li value="Mirtazapin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Mirtazapin</li>
                      <li value="Nortriptylin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Nortriptylin</li>
                      <li value="Venlafaxin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Venlafaxin</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antidepressiva_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_2)?$antidepressivaPatientAnswers->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_2 && $antidepressivaPatientAnswers->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antidepressiva_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_2_text_field)?$antidepressivaPatientAnswers->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_3)?$antidepressivaPatientAnswers->medikament_dropdown_3:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)"></li>
                      <li value="Amitriptylin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Amitriptylin</li>
                      <li value="Clomipramin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Clomipramin</li>
                      <li value="Desipramin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Desipramin</li>
                      <li value="Doxepin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Doxepin</li>
                      <li value="Duloxetin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Duloxetin</li>
                      <li value="Fluoxetin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Fluoxetin</li>
                      <li value="Imipramin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Imipramin</li>
                      <li value="Milnacipran" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Milnacipran</li>
                      <li value="Mirtazapin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Mirtazapin</li>
                      <li value="Nortriptylin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Nortriptylin</li>
                      <li value="Venlafaxin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Venlafaxin</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antidepressiva_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_3)?$antidepressivaPatientAnswers->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_3 && $antidepressivaPatientAnswers->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antidepressiva_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->medikament_dropdown_3_text_field)?$antidepressivaPatientAnswers->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="antidepressiva_therapieeflog" <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_5">
              <label for="keine_ausreichende_5">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="antidepressiva_therapieeflog" <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_5">
              <label for="erfolglos_5">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->gastrointestinal_dropdown)?$antidepressivaPatientAnswers->gastrointestinal_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                       <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antidepressiva_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->gastrointestinal_dropdown)?$antidepressivaPatientAnswers->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->gastrointestinal_dropdown && $antidepressivaPatientAnswers->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antidepressiva_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->gastrointestinal_dropdown_text_field)?$antidepressivaPatientAnswers->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->kardial_dropdown)?$antidepressivaPatientAnswers->kardial_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antidepressiva_kardial_input" class="kardial-field" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->kardial_dropdown)?$antidepressivaPatientAnswers->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->kardial_dropdown && $antidepressivaPatientAnswers->kardial_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antidepressiva_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->kardial_dropdown_text_field)?$antidepressivaPatientAnswers->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->nephrologisch_dropdown)?$antidepressivaPatientAnswers->nephrologisch_dropdown:''; ?>                
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antidepressiva_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->nephrologisch_dropdown)?$antidepressivaPatientAnswers->nephrologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->nephrologisch_dropdown && $antidepressivaPatientAnswers->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antidepressiva_nephrologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->nephrologisch_dropdown_text_field)?$antidepressivaPatientAnswers->nephrologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->neurologisch_dropdown)?$antidepressivaPatientAnswers->neurologisch_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antidepressiva_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->neurologisch_dropdown)?$antidepressivaPatientAnswers->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->neurologisch_dropdown && $antidepressivaPatientAnswers->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antidepressiva_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antidepressivaPatientAnswers) && !empty($antidepressivaPatientAnswers) && $antidepressivaPatientAnswers->neurologisch_dropdown_text_field)?$antidepressivaPatientAnswers->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="radio-box normal-radio">
          <input type="checkbox" class="select-aedativa" onclick="showWhoStufeChild(this)" data-class-name="sedativa-child" name="sedativa" <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->option_name == 'Sedativa')?'checked':''; ?> value="Sedativa" id="sedativa">
          <label for="sedativa">
            <span class="radio-cheak"></span>
            Sedativa
          </label>
          <div class="medikament-main-row normal-radio-inner sedativa-child" style="display: <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->option_name == 'Sedativa')?'block':'none'; ?>;">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_1)?$sedativaPatientAnswers->medikament_dropdown_1:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Clorazepat" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Clorazepat</li>
                      <li value="Diazepam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Diazepam</li>
                      <li value="Lorazepam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Lorazepam</li>
                      <li value="Midazolam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Midazolam</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="sedativa_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_1)?$sedativaPatientAnswers->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_1 && $sedativaPatientAnswers->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="sedativa_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_1_text_field)?$sedativaPatientAnswers->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_2)?$sedativaPatientAnswers->medikament_dropdown_2:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)"></li>
                       <li value="Clorazepat" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Clorazepat</li>
                       <li value="Diazepam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Diazepam</li>
                       <li value="Lorazepam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Lorazepam</li>
                      <li value="Midazolam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Midazolam</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="sedativa_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_2)?$sedativaPatientAnswers->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_2 && $sedativaPatientAnswers->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="sedativa_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_2_text_field)?$sedativaPatientAnswers->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_3)?$sedativaPatientAnswers->medikament_dropdown_3:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)"></li>
                       <li value="Clorazepat" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Clorazepat</li>
                       <li value="Diazepam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Diazepam</li>
                       <li value="Lorazepam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Lorazepam</li>
                      <li value="Midazolam" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Midazolam</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="sedativa_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_3)?$sedativaPatientAnswers->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_3 && $sedativaPatientAnswers->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="sedativa_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->medikament_dropdown_3_text_field)?$sedativaPatientAnswers->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="sedativa_therapieeflog" <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_6">
              <label for="keine_ausreichende_6">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="sedativa_therapieeflog" <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_6">
              <label for="erfolglos_6">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->gastrointestinal_dropdown)?$sedativaPatientAnswers->gastrointestinal_dropdown:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                       <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="sedativa_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->gastrointestinal_dropdown)?$sedativaPatientAnswers->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->gastrointestinal_dropdown && $sedativaPatientAnswers->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="sedativa_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->gastrointestinal_dropdown_text_field)?$sedativaPatientAnswers->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->kardial_dropdown)?$sedativaPatientAnswers->kardial_dropdown:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="sedativa_kardial_input" class="kardial-field" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->kardial_dropdown)?$sedativaPatientAnswers->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->kardial_dropdown && $sedativaPatientAnswers->kardial_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="sedativa_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->kardial_dropdown_text_field)?$sedativaPatientAnswers->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->nephrologisch_dropdown)?$sedativaPatientAnswers->nephrologisch_dropdown:''; ?>              
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="sedativa_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->nephrologisch_dropdown)?$sedativaPatientAnswers->nephrologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->nephrologisch_dropdown && $sedativaPatientAnswers->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="sedativa_nephrologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->nephrologisch_dropdown_text_field)?$sedativaPatientAnswers->nephrologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->neurologisch_dropdown)?$sedativaPatientAnswers->neurologisch_dropdown:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="sedativa_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->neurologisch_dropdown)?$sedativaPatientAnswers->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->neurologisch_dropdown && $sedativaPatientAnswers->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="sedativa_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($sedativaPatientAnswers) && !empty($sedativaPatientAnswers) && $sedativaPatientAnswers->neurologisch_dropdown_text_field)?$sedativaPatientAnswers->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="radio-box normal-radio">
          <input type="checkbox" class="select-antikonvulsiva" onclick="showWhoStufeChild(this)" data-class-name="antikonvulsiva-child" name="antikonvulsiva" <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->option_name == 'Antikonvulsiva')?'checked':''; ?> value="Antikonvulsiva" id="antikonvulsiva">
          <label for="antikonvulsiva">
            <span class="radio-cheak"></span>
            Antikonvulsiva
          </label>
          <div class="medikament-main-row normal-radio-inner antikonvulsiva-child" style="display: <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->option_name == 'Antikonvulsiva')?'block':'none'; ?>;">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_1)?$antikonvulsivaPatientAnswers->medikament_dropdown_1:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Carbamazepin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Carbamazepin</li>
                      <li value="Gabapentin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Gabapentin</li>
                      <li value="Lamotrigin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Lamotrigin</li>
                      <li value="Oxcarbazepin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Oxcarbazepin</li>
                      <li value="Phenytoin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Phenytoin</li>
                      <li value="Pregabalin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Pregabalin</li>
                      <li value="Topiramat" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Topiramat</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antikonvulsiva_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_1)?$antikonvulsivaPatientAnswers->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_1 && $antikonvulsivaPatientAnswers->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antikonvulsiva_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_1_text_field)?$antikonvulsivaPatientAnswers->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_2)?$antikonvulsivaPatientAnswers->medikament_dropdown_2:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)"></li>
                       <li value="Carbamazepin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Carbamazepin</li>
                       <li value="Gabapentin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Gabapentin</li>
                       <li value="Lamotrigin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Lamotrigin</li>
                       <li value="Oxcarbazepin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Oxcarbazepin</li>
                       <li value="Phenytoin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Phenytoin</li>
                      <li value="Pregabalin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Pregabalin</li>
                       <li value="Topiramat" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Topiramat</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antikonvulsiva_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_2)?$antikonvulsivaPatientAnswers->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_2 && $antikonvulsivaPatientAnswers->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antikonvulsiva_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_2_text_field)?$antikonvulsivaPatientAnswers->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_3)?$antikonvulsivaPatientAnswers->medikament_dropdown_3:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)"></li>
                       <li value="Carbamazepin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Carbamazepin</li>
                       <li value="Gabapentin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Gabapentin</li>
                       <li value="Lamotrigin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Lamotrigin</li>
                       <li value="Oxcarbazepin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Oxcarbazepin</li>
                       <li value="Phenytoin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Phenytoin</li>
                      <li value="Pregabalin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Pregabalin</li>
                       <li value="Topiramat" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Topiramat</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antikonvulsiva_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_3)?$antikonvulsivaPatientAnswers->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_3 && $antikonvulsivaPatientAnswers->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antikonvulsiva_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->medikament_dropdown_3_text_field)?$antikonvulsivaPatientAnswers->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="antikonvulsiva_therapieeflog" <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_7">
              <label for="keine_ausreichende_7">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="antikonvulsiva_therapieeflog" <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_7">
              <label for="erfolglos_7">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->gastrointestinal_dropdown)?$antikonvulsivaPatientAnswers->gastrointestinal_dropdown:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                       <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antikonvulsiva_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->gastrointestinal_dropdown)?$antikonvulsivaPatientAnswers->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->gastrointestinal_dropdown && $antikonvulsivaPatientAnswers->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antikonvulsiva_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->gastrointestinal_dropdown_text_field)?$antikonvulsivaPatientAnswers->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->kardial_dropdown)?$antikonvulsivaPatientAnswers->kardial_dropdown:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antikonvulsiva_kardial_input" class="kardial-field" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->kardial_dropdown)?$antikonvulsivaPatientAnswers->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->kardial_dropdown && $antikonvulsivaPatientAnswers->kardial_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antikonvulsiva_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->kardial_dropdown_text_field)?$antikonvulsivaPatientAnswers->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->nephrologisch_dropdown)?$antikonvulsivaPatientAnswers->nephrologisch_dropdown:''; ?>                 
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antikonvulsiva_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->nephrologisch_dropdown)?$antikonvulsivaPatientAnswers->nephrologisch_dropdown:''; ?> ">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->nephrologisch_dropdown && $antikonvulsivaPatientAnswers->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antikonvulsiva_nephrologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->nephrologisch_dropdown_text_field)?$antikonvulsivaPatientAnswers->nephrologisch_dropdown_text_field:''; ?> ">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->neurologisch_dropdown)?$antikonvulsivaPatientAnswers->neurologisch_dropdown:''; ?>                        
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antikonvulsiva_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->neurologisch_dropdown)?$antikonvulsivaPatientAnswers->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->neurologisch_dropdown && $antikonvulsivaPatientAnswers->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antikonvulsiva_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antikonvulsivaPatientAnswers) && !empty($antikonvulsivaPatientAnswers) && $antikonvulsivaPatientAnswers->neurologisch_dropdown_text_field)?$antikonvulsivaPatientAnswers->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="radio-box normal-radio">
          <input type="checkbox" class="select-antiemetika" onclick="showWhoStufeChild(this)" data-class-name="antiemetika-child" name="antiemetika" <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->option_name == 'Antiemetika')?'checked':''; ?> value="Antiemetika" id="antiemetika">
          <label for="antiemetika">
            <span class="radio-cheak"></span>
            Antiemetika
          </label>
          <div class="medikament-main-row normal-radio-inner antiemetika-child" style="display: <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->option_name == 'Antiemetika')?'block':'none'; ?>;">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_1)?$antiemetikaPatientAnswers->medikament_dropdown_1:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Aprepitant" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Aprepitant</li>
                      <li value="Dimenhydrinat" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Dimenhydrinat</li>
                      <li value="Fosaprepitant" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Fosaprepitant</li>
                      <li value="Granisetron" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Granisetron</li>
                      <li value="Metoclopramid" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Metoclopramid</li>
                      <li value="Netupitant" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Netupitant</li>
                      <li value="Ondansetron" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Ondansetron</li>
                      <li value="Palonosetron" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Palonosetron</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antiemetika_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_1)?$antiemetikaPatientAnswers->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_1 && $antiemetikaPatientAnswers->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antiemetika_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_1_text_field)?$antiemetikaPatientAnswers->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_1)?$antiemetikaPatientAnswers->medikament_dropdown_2:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)"></li>
                      <li value="Aprepitant" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Aprepitant</li>
                      <li value="Dimenhydrinat" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Dimenhydrinat</li>
                      <li value="Fosaprepitant" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Fosaprepitant</li>
                      <li value="Granisetron" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Granisetron</li>
                      <li value="Metoclopramid" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Metoclopramid</li>
                      <li value="Netupitant" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Netupitant</li>
                      <li value="Ondansetron" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Ondansetron</li>
                      <li value="Palonosetron" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Palonosetron</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antiemetika_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_2)?$antiemetikaPatientAnswers->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_2 && $antiemetikaPatientAnswers->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antiemetika_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_2_text_field)?$antiemetikaPatientAnswers->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_3)?$antiemetikaPatientAnswers->medikament_dropdown_3:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)"></li>
                      <li value="Aprepitant" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Aprepitant</li>
                      <li value="Dimenhydrinat" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Dimenhydrinat</li>
                      <li value="Fosaprepitant" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Fosaprepitant</li>
                      <li value="Granisetron" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Granisetron</li>
                      <li value="Metoclopramid" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Metoclopramid</li>
                      <li value="Netupitant" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Netupitant</li>
                      <li value="Ondansetron" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Ondansetron</li>
                      <li value="Palonosetron" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Palonosetron</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antiemetika_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_3)?$antiemetikaPatientAnswers->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_3 && $antiemetikaPatientAnswers->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antiemetika_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_3_text_field)?$antiemetikaPatientAnswers->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="antiemetika_therapieeflog" <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_8">
              <label for="keine_ausreichende_8">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="antiemetika_therapieeflog" <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_8">
              <label for="erfolglos_8">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_1)?$antiemetikaPatientAnswers->gastrointestinal_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                       <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antiemetika_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->gastrointestinal_dropdown)?$antiemetikaPatientAnswers->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->gastrointestinal_dropdown && $antiemetikaPatientAnswers->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antiemetika_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->gastrointestinal_dropdown_text_field)?$antiemetikaPatientAnswers->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->kardial_dropdown)?$antiemetikaPatientAnswers->kardial_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antiemetika_kardial_input" class="kardial-field" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->kardial_dropdown)?$antiemetikaPatientAnswers->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->kardial_dropdown && $antiemetikaPatientAnswers->kardial_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antiemetika_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->kardial_dropdown_text_field)?$antiemetikaPatientAnswers->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_1)?$antiemetikaPatientAnswers->nephrologisch_dropdown:''; ?>                
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antiemetika_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->nephrologisch_dropdown)?$antiemetikaPatientAnswers->nephrologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->nephrologisch_dropdown && $antiemetikaPatientAnswers->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antiemetika_nephrologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->nephrologisch_dropdown_text_field)?$antiemetikaPatientAnswers->nephrologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->medikament_dropdown_1)?$antiemetikaPatientAnswers->neurologisch_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="antiemetika_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->neurologisch_dropdown)?$antiemetikaPatientAnswers->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->neurologisch_dropdown && $antiemetikaPatientAnswers->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="antiemetika_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($antiemetikaPatientAnswers) && !empty($antiemetikaPatientAnswers) && $antiemetikaPatientAnswers->neurologisch_dropdown_text_field)?$antiemetikaPatientAnswers->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
    <div class="checkbox-custom normal-radio">
       <input type="checkbox" name="who_stufe2" <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer)?'checked':''; ?> value="Yes" id="who_stufe2" onclick="showWhoStufeMain(this)" data-class-name="who-stufe-2-main">
       <label for="who_stufe2">
          <span class="cheak"></span>
          WHO Stufe 2
       </label>
       <div class="normal-radio-inner who-stufe-2-main" style="display: <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer)?'block':'none'; ?>;">
        <div class="radio-box normal-radio">
          <input type="checkbox" onclick="showWhoStufeChild(this)" data-class-name="schwach-wirksame-opioide-child" name="schwach_wirksame_opioide" <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->option_name == 'Schwach wirksame Opioide')?'checked':''; ?> value="Schwach wirksame Opioide" class="schwach-wirksame-opioide">
          <label for="schwach-wirksame-opioide" class="pl-3">
            <span class="radio-cheak d-none"></span>
            Schwach wirksame Opioide
          </label>
          <div class="medikament-main-row normal-radio-inner schwach-wirksame-opioide-child" style="display: <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->option_name == 'Schwach wirksame Opioide')?'block':'none'; ?>;">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_1)?$who_2_patient_answer->medikament_dropdown_1:''; ?>                    
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Codein" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Codein</li>
                      <li value="Dihydrocodein" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Dihydrocodein</li>
                      <li value="Tilidin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Tilidin</li>
                      <li value="Tilidin & Naloxon" data-toggle="tooltip" data-placement="bottom" title="Tilidin & Naloxon" onclick="selectMedikamentFirst(this)">Tilidin & Naloxon</li>
                      <li value="Tilidin & Naloxon retardiert" data-toggle="tooltip" data-placement="bottom" title="Tilidin & Naloxon retardiert" onclick="selectMedikamentFirst(this)">Tilidin & Naloxon retardiert</li>
                      <li value="Tramadol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Tramadol</li>
                      <li value="Tramadol retardiert" data-toggle="tooltip" data-placement="bottom" title="Tramadol retardiert" onclick="selectMedikamentFirst(this)">Tramadol retardiert</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="schwach_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_1)?$who_2_patient_answer->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_1 && $who_2_patient_answer->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="schwach_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_1_text_field)?$who_2_patient_answer->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_2)?$who_2_patient_answer->medikament_dropdown_2:''; ?>                    
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)"></li>
                     <li value="Codein" onclick="selectMedikamentSecond(this)">Codein</li>
                      <li value="Dihydrocodein" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Dihydrocodein</li>
                      <li value="Tilidin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Tilidin</li>
                      <li value="Tilidin & Naloxon" data-toggle="tooltip" data-placement="bottom" title="Tilidin & Naloxon" onclick="selectMedikamentSecond(this)">Tilidin & Naloxon</li>
                      <li value="Tilidin & Naloxon retardiert" data-toggle="tooltip" data-placement="bottom" title="Tilidin & Naloxon retardiert" onclick="selectMedikamentSecond(this)">Tilidin & Naloxon retardiert</li>
                      <li value="Tramadol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Tramadol</li>
                      <li value="Tramadol retardiert"  data-toggle="tooltip" data-placement="bottom" title="Tramadol retardiert" onclick="selectMedikamentSecond(this)">Tramadol retardiert</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="schwach_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_2)?$who_2_patient_answer->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_2 && $who_2_patient_answer->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="schwach_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_1_text_field)?$who_2_patient_answer->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer)?$who_2_patient_answer->medikament_dropdown_3:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)"></li>
                      <li value="Codein" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Codein</li>
                      <li value="Dihydrocodein" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Dihydrocodein</li>
                      <li value="Tilidin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Tilidin</li>
                      <li value="Tilidin & Naloxon" data-toggle="tooltip" data-placement="bottom" title="Tilidin & Naloxon" onclick="selectMedikamentThird(this)">Tilidin & Naloxon</li>
                      <li value="Tilidin & Naloxon retardiert" data-toggle="tooltip" data-placement="bottom" title="Tilidin & Naloxon retardiert" onclick="selectMedikamentThird(this)">Tilidin & Naloxon retardiert</li>
                      <li value="Tramadol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Tramadol</li>
                      <li value="Tramadol retardiert" data-toggle="tooltip" data-placement="bottom" title="Tramadol retardiert" onclick="selectMedikamentThird(this)">Tramadol retardiert</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="schwach_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_3)?$who_2_patient_answer->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_3 && $who_2_patient_answer->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="schwach_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->medikament_dropdown_3_text_field)?$who_2_patient_answer->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="schwach_therapieeflog" <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_9">
              <label for="keine_ausreichende_9">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="schwach_therapieeflog" <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_9">
              <label for="erfolglos_9">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->gastrointestinal_dropdown)?$who_2_patient_answer->gastrointestinal_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                       <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="schwach_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->gastrointestinal_dropdown)?$who_2_patient_answer->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->gastrointestinal_dropdown && $who_2_patient_answer->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="schwach_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->gastrointestinal_dropdown_text_field)?$who_2_patient_answer->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->kardial_dropdown)?$who_2_patient_answer->kardial_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="schwach_kardial_input" class="kardial-field" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->kardial_dropdown)?$who_2_patient_answer->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->kardial_dropdown && $who_2_patient_answer->kardial_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="schwach_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->kardial_dropdown_text_field)?$who_2_patient_answer->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->nephrologisch_dropdown)?$who_2_patient_answer->nephrologisch_dropdown:''; ?>               
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="schwach_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->nephrologisch_dropdown)?$who_2_patient_answer->nephrologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->nephrologisch_dropdown && $who_2_patient_answer->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="schwach_nephrologisch_text" class="form-control red-input" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->nephrologisch_dropdown_text_field)?$who_2_patient_answer->nephrologisch_dropdown_text_field:''; ?>" placeholder="">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->neurologisch_dropdown)?$who_2_patient_answer->neurologisch_dropdown:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="schwach_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->neurologisch_dropdown)?$who_2_patient_answer->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->neurologisch_dropdown && $who_2_patient_answer->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="schwach_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($who_2_patient_answer) && !empty($who_2_patient_answer) && $who_2_patient_answer->neurologisch_dropdown_text_field)?$who_2_patient_answer->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
    <div class="checkbox-custom normal-radio">
       <input type="checkbox" name="who_stufe3" <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer)?'checked':''; ?> value="Yes" id="who_stufe3" onclick="showWhoStufeMain(this)" data-class-name="who-stufe-3-main">
       <label for="who_stufe3">
          <span class="cheak"></span>
          WHO Stufe 3
       </label>
       <div class="normal-radio-inner who-stufe-3-main" style="display: <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer)?'block':'none'; ?>;">
        <div class="radio-box normal-radio">
          <input type="checkbox" onclick="showWhoStufeChild(this)" data-class-name="stark-wirksame-opioide-child" name="stark_wirksame_opioide" <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->option_name == 'Stark wirksame Opioide')?'checked':''; ?> value="Stark wirksame Opioide" class="stark-wirksame-opioide">
          <label for="stark-wirksame-opioide" class="pl-3">
            <span class="radio-cheak d-none"></span>
            Stark wirksame Opioide
          </label>
          <div class="medikament-main-row normal-radio-inner stark-wirksame-opioide-child" style="display: <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->option_name == 'Stark wirksame Opioide')?'block':'none'; ?>;">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_1)?$who_3_patient_answer->medikament_dropdown_1:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Buprenorphin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Buprenorphin</li>
                      <li value="Buprenorphin-TTS" data-toggle="tooltip" data-placement="bottom" title="Buprenorphin-TTS" onclick="selectMedikamentFirst(this)">Buprenorphin-TTS</li>
                      <li value="Fentanyl" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Fentanyl</li>
                      <li value="Fentanyl buccal" data-toggle="tooltip" data-placement="bottom" title="Fentanyl buccal" onclick="selectMedikamentFirst(this)">Fentanyl buccal</li>
                      <li value="Fentanyl nasal" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Fentanyl nasal</li>
                      <li value="Fentanyl peroral" data-toggle="tooltip" data-placement="bottom" title="Fentanyl peroral" onclick="selectMedikamentFirst(this)">Fentanyl peroral</li>
                      <li value="Fentanyl-TTS" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Fentanyl-TTS</li>
                      <li value="Hydromorphon" data-toggle="tooltip" data-placement="bottom" title="Hydromorphon" onclick="selectMedikamentFirst(this)">Hydromorphon</li>
                      <li value="Levomethadon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Levomethadon</li>
                      <li value="Methadon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Methadon</li>
                      <li value="Morphin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Morphin</li>
                      <li value="Oxycodon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Oxycodon</li>
                      <li value="Oxycodon & Naloxon" data-toggle="tooltip" data-placement="bottom" title="Oxycodon & Naloxon" onclick="selectMedikamentFirst(this)">Oxycodon & Naloxon</li>
                      <li value="Tapentadol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Tapentadol</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="stark_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_1)?$who_3_patient_answer->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_1 && $who_3_patient_answer->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="stark_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_1_text_field)?$who_3_patient_answer->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_2)?$who_3_patient_answer->medikament_dropdown_2:''; ?>                     
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)"></li>
                      <li value="Buprenorphin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Buprenorphin</li>
                      <li value="Buprenorphin-TTS" data-toggle="tooltip" data-placement="bottom" title="Buprenorphin-TTS" onclick="selectMedikamentSecond(this)">Buprenorphin-TTS</li>
                      <li value="Fentanyl" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Fentanyl</li>
                      <li value="Fentanyl buccal" data-toggle="tooltip" data-placement="bottom" title="Fentanyl buccal" onclick="selectMedikamentSecond(this)">Fentanyl buccal</li>
                      <li value="Fentanyl nasal" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Fentanyl nasal</li>
                      <li value="Fentanyl peroral" data-toggle="tooltip" data-placement="bottom" title="Fentanyl peroral" onclick="selectMedikamentSecond(this)">Fentanyl peroral</li>
                      <li value="Fentanyl-TTS" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Fentanyl-TTS</li>
                      <li value="Hydromorphon" data-toggle="tooltip" data-placement="bottom" title="Hydromorphon" onclick="selectMedikamentSecond(this)">Hydromorphon</li>
                      <li value="Levomethadon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Levomethadon</li>
                      <li value="Methadon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Methadon</li>
                      <li value="Morphin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Morphin</li>
                      <li value="Oxycodon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Oxycodon</li>
                      <li value="Oxycodon & Naloxon" data-toggle="tooltip" data-placement="bottom" title="Oxycodon & Naloxon" onclick="selectMedikamentSecond(this)">Oxycodon & Naloxon</li>
                      <li value="Tapentadol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Tapentadol</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="stark_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_2)?$who_3_patient_answer->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_2 && $who_3_patient_answer->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="stark_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_2_text_field)?$who_3_patient_answer->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_3)?$who_3_patient_answer->medikament_dropdown_3:''; ?>                     
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)"></li>
                      <li value="Buprenorphin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Buprenorphin</li>
                      <li value="Buprenorphin-TTS" data-toggle="tooltip" data-placement="bottom" title="Buprenorphin-TTS" onclick="selectMedikamentThird(this)">Buprenorphin-TTS</li>
                      <li value="Fentanyl" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Fentanyl</li>
                      <li value="Fentanyl buccal" data-toggle="tooltip" data-placement="bottom" title="Fentanyl buccal" onclick="selectMedikamentThird(this)">Fentanyl buccal</li>
                      <li value="Fentanyl nasal" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Fentanyl nasal</li>
                      <li value="Fentanyl peroral" data-toggle="tooltip" data-placement="bottom" title="Fentanyl peroral" onclick="selectMedikamentThird(this)">Fentanyl peroral</li>
                      <li value="Fentanyl-TTS" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Fentanyl-TTS</li>
                      <li value="Hydromorphon" data-toggle="tooltip" data-placement="bottom" title="Hydromorphon" onclick="selectMedikamentThird(this)">Hydromorphon</li>
                      <li value="Levomethadon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Levomethadon</li>
                      <li value="Methadon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Methadon</li>
                      <li value="Morphin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Morphin</li>
                      <li value="Oxycodon" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Oxycodon</li>
                      <li value="Oxycodon & Naloxon" data-toggle="tooltip" data-placement="bottom" title="Oxycodon & Naloxon" onclick="selectMedikamentThird(this)">Oxycodon & Naloxon</li>
                      <li value="Tapentadol" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Tapentadol</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="stark_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_3)?$who_3_patient_answer->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_3 && $who_3_patient_answer->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="stark_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->medikament_dropdown_3_text_field)?$who_3_patient_answer->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="stark_therapieeflog" <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_10">
              <label for="keine_ausreichende_10">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="stark_therapieeflog" <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_10">
              <label for="erfolglos_10">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->gastrointestinal_dropdown)?$who_3_patient_answer->gastrointestinal_dropdown:''; ?>                    
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                       <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="stark_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->gastrointestinal_dropdown)?$who_3_patient_answer->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->gastrointestinal_dropdown && $who_3_patient_answer->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="stark_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->gastrointestinal_dropdown_text_field)?$who_3_patient_answer->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->kardial_dropdown)?$who_3_patient_answer->kardial_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="stark_kardial_input" class="kardial-field" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->kardial_dropdown)?$who_3_patient_answer->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->kardial_dropdown && $who_3_patient_answer->kardial_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="stark_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->kardial_dropdown_text_field)?$who_3_patient_answer->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->nephrologisch_dropdown)?$who_3_patient_answer->nephrologisch_dropdown:''; ?>               
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="stark_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->nephrologisch_dropdown)?$who_3_patient_answer->nephrologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->nephrologisch_dropdown && $who_3_patient_answer->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="stark_nephrologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->nephrologisch_dropdown_text_field)?$who_3_patient_answer->nephrologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->neurologisch_dropdown)?$who_3_patient_answer->neurologisch_dropdown:''; ?>                    
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="stark_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->neurologisch_dropdown)?$who_3_patient_answer->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->neurologisch_dropdown && $who_3_patient_answer->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="stark_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($who_3_patient_answer) && !empty($who_3_patient_answer) && $who_3_patient_answer->neurologisch_dropdown_text_field)?$who_3_patient_answer->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
    <div class="checkbox-custom normal-radio">
       <input type="checkbox" name="spasmolytika" <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer)?'checked':''; ?> value="Yes" id="spasmolytika" onclick="showWhoStufeMain(this)" data-class-name="spasmolytika-main">
       <label for="spasmolytika">
          <span class="cheak"></span>
          Spasmolytika
       </label>
       <div class="normal-radio-inner spasmolytika-main" style="display: <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer)?'block':'none'; ?>;">
          <div class="medikament-main-row normal-radio-inner">
            <div class="row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Medikament:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_1">
                     <button class="dropdown-select nicht_btn_1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_1)?$spasmolytika_patient_answer->medikament_dropdown_1:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_1">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)"></li>
                      <li value="Atracurium" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Atracurium</li>
                      <li value="Baclofen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Baclofen</li>
                      <li value="Botulinumtoxin" data-toggle="tooltip" data-placement="bottom" title="Botulinumtoxin" onclick="selectMedikamentFirst(this)">Botulinumtoxin</li>
                      <li value="Butylscopolamin" data-toggle="tooltip" data-placement="bottom" title="Butylscopolamin" onclick="selectMedikamentFirst(this)">Butylscopolamin</li>
                      <li value="Dantrolen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Dantrolen</li>
                      <li value="Glycopyrronium" data-toggle="tooltip" data-placement="bottom" title="Glycopyrronium" onclick="selectMedikamentFirst(this)">Glycopyrronium</li>
                      <li value="Magnesium" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Magnesium</li>
                      <li value="Mebeverin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Mebeverin</li>
                      <li value="Methocarbamol" data-toggle="tooltip" data-placement="bottom" title="Methocarbamol" onclick="selectMedikamentFirst(this)">Methocarbamol</li>
                      <li value="Scopolamin-TTS" data-toggle="tooltip" data-placement="bottom" title="Scopolamin-TTS" onclick="selectMedikamentFirst(this)">Scopolamin-TTS</li>
                      <li value="Tolperison" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Tolperison</li>
                      <li value="Tizanidin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Tizanidin</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentFirst(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="spasmolytika_input_1" class="opioidanalgetika-field-1" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_1)?$spasmolytika_patient_answer->medikament_dropdown_1:''; ?>">
                 </div>
              </div>
              <div class="col-lg-4 medikament_free_text_first" style="display: <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_1 && $spasmolytika_patient_answer->medikament_dropdown_1 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="spasmolytika_medikament_free_text_first" class="form-control red-input" placeholder="" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_1_text_field)?$spasmolytika_patient_answer->medikament_dropdown_1_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_2">
                     <button class="dropdown-select nicht_btn_2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_2)?$spasmolytika_patient_answer->medikament_dropdown_2:''; ?>                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="nicht_btn_2">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)"></li>
                      <li value="Atracurium" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Atracurium</li>
                      <li value="Baclofen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Baclofen</li>
                      <li value="Botulinumtoxin" data-toggle="tooltip" data-placement="bottom" title="Botulinumtoxin" onclick="selectMedikamentSecond(this)">Botulinumtoxin</li>
                      <li value="Butylscopolamin" data-toggle="tooltip" data-placement="bottom" title="Butylscopolamin" onclick="selectMedikamentSecond(this)">Butylscopolamin</li>
                      <li value="Dantrolen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Dantrolen</li>
                      <li value="Glycopyrronium" data-toggle="tooltip" data-placement="bottom" title="Glycopyrronium" onclick="selectMedikamentSecond(this)">Glycopyrronium</li>
                      <li value="Magnesium" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Magnesium</li>
                      <li value="Mebeverin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Mebeverin</li>
                      <li value="Methocarbamol" data-toggle="tooltip" data-placement="bottom" title="Methocarbamol" onclick="selectMedikamentSecond(this)">Methocarbamol</li>
                      <li value="Scopolamin-TTS" data-toggle="tooltip" data-placement="bottom" title="Scopolamin-TTS" onclick="selectMedikamentSecond(this)">Scopolamin-TTS</li>
                      <li value="Tolperison" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Tolperison</li>
                      <li value="Tizanidin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Tizanidin</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentSecond(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="spasmolytika_input_2" class="opioidanalgetika-field-2" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_2)?$spasmolytika_patient_answer->medikament_dropdown_2:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_second" style="display: <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_2 && $spasmolytika_patient_answer->medikament_dropdown_2 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="spasmolytika_medikament_free_text_second" class="form-control red-input" placeholder="" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_2_text_field)?$spasmolytika_patient_answer->medikament_dropdown_2_text_field:''; ?>">
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nicht_dropdown_box_3">
                     <button class="dropdown-select nicht_btn_3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_3)?$spasmolytika_patient_answer->medikament_dropdown_3:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nicht_btn_3">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)"></li>
                      <li value="Atracurium" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Atracurium</li>
                      <li value="Baclofen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Baclofen</li>
                      <li value="Botulinumtoxin" data-toggle="tooltip" data-placement="bottom" title="Botulinumtoxin" onclick="selectMedikamentThird(this)">Botulinumtoxin</li>
                      <li value="Butylscopolamin" data-toggle="tooltip" data-placement="bottom" title="Butylscopolamin" onclick="selectMedikamentThird(this)">Butylscopolamin</li>
                      <li value="Dantrolen" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Dantrolen</li>
                      <li value="Glycopyrronium" data-toggle="tooltip" data-placement="bottom" title="Glycopyrronium" onclick="selectMedikamentThird(this)">Glycopyrronium</li>
                      <li value="Magnesium" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Magnesium</li>
                      <li value="Mebeverin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Mebeverin</li>
                      <li value="Methocarbamol" data-toggle="tooltip" data-placement="bottom" title="Methocarbamol" onclick="selectMedikamentThird(this)">Methocarbamol</li>
                      <li value="Scopolamin-TTS" data-toggle="tooltip" data-placement="bottom" title="Scopolamin-TTS" onclick="selectMedikamentThird(this)">Scopolamin-TTS</li>
                      <li value="Tolperison" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Tolperison</li>
                      <li value="Tizanidin" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Tizanidin</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectMedikamentThird(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="spasmolytika_input_3" class="opioidanalgetika-field-3" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_3)?$spasmolytika_patient_answer->medikament_dropdown_3:''; ?>">
                </div>
              </div>
              <div class="col-lg-4 medikament_free_text_third" style="display: <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_3 && $spasmolytika_patient_answer->medikament_dropdown_3 == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="spasmolytika_medikament_free_text_third" class="form-control red-input" placeholder="" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->medikament_dropdown_3_text_field)?$spasmolytika_patient_answer->medikament_dropdown_3_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Therapieerfolg:</label></div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="spasmolytika_therapieeflog" <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'checked':''; ?> value="keine ausreichende Schmerzreduktion" id="keine_ausreichende_1">
              <label for="keine_ausreichende_1">
                <span class="radio-cheak"></span>
                keine ausreichende Schmerzreduktion
              </label>
            </div>
            <div class="radio-box normal-radio">
              <input type="radio" class="select-schwerwiegend" name="spasmolytika_therapieeflog" <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->therapieerfolg == 'erfolglos')?'checked':''; ?> value="erfolglos" id="erfolglos_1">
              <label for="erfolglos_1">
                <span class="radio-cheak"></span>
                erfolglos
              </label>
            </div>
            <div class="mt-3 mb-3"><label class="blod-text">Nebenwirkungen:</label></div>
            <div class="row gastrointestinal-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Gastrointestinal:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_gastrointestinal_dropdown_box">
                     <button class="dropdown-select gastrointestinal_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->gastrointestinal_dropdown)?$spasmolytika_patient_answer->gastrointestinal_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="gastrointestinal_btn">
                       <li value="0" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)"></li>
                       <li value="Appetitverlust" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Appetitverlust</li>
                       <li value="Magenblutung" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Magenblutung</li>
                       <li value="Obstipation" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Obstipation</li>
                      <li value="Übelkeit" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Übelkeit</li>
                       <li value="Ulcus" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Ulcus</li>
                       <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectGastrointestinal(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="spasmolytika_gastrointestinal_input" class="gastrointestinal-field" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->gastrointestinal_dropdown)?$spasmolytika_patient_answer->gastrointestinal_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 gastrointestinal-text-field" style="display: <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->gastrointestinal_dropdown && $spasmolytika_patient_answer->gastrointestinal_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="spasmolytika_gastrointestinal_text" class="form-control red-input" placeholder="" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->gastrointestinal_dropdown_text_field)?$spasmolytika_patient_answer->gastrointestinal_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row kardial-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Kardial:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_kardial_dropdown_box">
                     <button class="dropdown-select kardial_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->kardial_dropdown)?$spasmolytika_patient_answer->kardial_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="kardial_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectKardial(this)"></li>
                      <li value="Herzrythmusstörung" data-toggle="tooltip" data-placement="bottom" title="Herzrythmusstörung" onclick="selectKardial(this)">Herzrythmusstörung</li>
                      <li value="Ödembildung" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Ödembildung</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectKardial(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="spasmolytika_kardial_input" class="kardial-field" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->kardial_dropdown)?$spasmolytika_patient_answer->kardial_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 kardial-text-field" style="display: <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->kardial_dropdown && $spasmolytika_patient_answer->kardial_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="spasmolytika_kardial_text" class="form-control red-input" placeholder="" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->kardial_dropdown_text_field)?$spasmolytika_patient_answer->kardial_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row nephrologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Nephrologisch:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_nephrologisch_dropdown_box">
                     <button class="dropdown-select nephrologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->nephrologisch_dropdown)?$spasmolytika_patient_answer->nephrologisch_dropdown:''; ?>               
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="nephrologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNephrologisch(this)"></li>
                      <li value="Leberinsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Leberinsuffizienz" onclick="selectNephrologisch(this)">Leberinsuffizienz</li>
                      <li value="Niereninsuffizienz" data-toggle="tooltip" data-placement="bottom" title="Niereninsuffizienz" onclick="selectNephrologisch(this)">Niereninsuffizienz</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNephrologisch(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="spasmolytika_nephrologisch_input" class="nephrologisch-field" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->nephrologisch_dropdown)?$spasmolytika_patient_answer->nephrologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 nephrologisch-text-field" style="display: <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->nephrologisch_dropdown && $spasmolytika_patient_answer->nephrologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="spasmolytika_nephrologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->nephrologisch_dropdown_text_field)?$spasmolytika_patient_answer->nephrologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
            <div class="row neurologisch-main-row">
              <div class="col-lg-2 align-self-center">
                <label class="blod-text">Neurologisch/<br/>psychotrop:</label>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <div class="dropdown select-box" id="select_neurologisch_dropdown_box">
                     <button class="dropdown-select neurologisch_btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->neurologisch_dropdown)?$spasmolytika_patient_answer->neurologisch_dropdown:''; ?>                      
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="neurologisch_btn">
                      <li value="0" data-toggle="tooltip" data-placement="bottom" title="" onclick="selectNeurologischPsychotrop(this)"></li>
                      <li value="Paradoxe Reaktion" data-toggle="tooltip" data-placement="bottom" title="Paradoxe Reaktion" onclick="selectNeurologischPsychotrop(this)">Paradoxe Reaktion</li>
                      <!-- <li value="Reaktion" data-toggle="tooltip" data-placement="bottom" title="Reaktion" onclick="selectNeurologischPsychotrop(this)">Reaktion</li> -->
                      <li value="Schwindel" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Schwindel</li>
                      <li value="Freifeld" data-toggle="tooltip" data-placement="bottom" onclick="selectNeurologischPsychotrop(this)">Freifeld</li>
                     </ul>
                   </div>
                   <input type="hidden" name="spasmolytika_neurologisch_input" class="neurologisch-field" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->neurologisch_dropdown)?$spasmolytika_patient_answer->neurologisch_dropdown:''; ?>">
                </div>
              </div>
              <div class="col-lg-5 neurologisch-text-field" style="display: <?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->neurologisch_dropdown && $spasmolytika_patient_answer->neurologisch_dropdown == 'Freifeld')?'block':'none'; ?>;">
                <div class="form-group">
                  <input type="text" name="spasmolytika_neurologisch_text" class="form-control red-input" placeholder="" value="<?php echo (isset($spasmolytika_patient_answer) && !empty($spasmolytika_patient_answer) && $spasmolytika_patient_answer->neurologisch_dropdown_text_field)?$spasmolytika_patient_answer->neurologisch_dropdown_text_field:''; ?>">
                </div>
              </div>
            </div>
          </div>
       </div>
    </div>
    <div class="btn-group-custom text-right">
      <a href="<?php echo base_url('arztfragebogen/step-3?in='.$insured_number.'&qid='.$qid); ?>" class="btn btn-primary"><span><i class="fas fa-long-arrow-alt-left"></i> Zurück</span></a>
      <button type="submit" class="btn btn-primary"><span>Weiter <i class="fas fa-long-arrow-alt-right"></i></span></button>
    </div>
  </form>
  </div>
</div>

<script type="text/javascript">

  function showWhoStufeMain($this)
  {
    var class_name = $($this).attr('data-class-name');
    
    if ($($this).is(':checked')) {    
      $('.'+class_name).show();

      if (class_name == 'who-stufe-2-main') {
        $('.schwach-wirksame-opioide').prop('checked', true);
        $('.schwach-wirksame-opioide-child').show();
      } else if (class_name == 'who-stufe-3-main') {
        $('.stark-wirksame-opioide').prop('checked', true);
        $('.stark-wirksame-opioide-child').show();
      }
    } else {
      $('.'+class_name).hide();

      if (class_name == 'who-stufe-2-main') {
        $('.schwach-wirksame-opioide').prop('checked', false);
        $('.schwach-wirksame-opioide-child').hide();
      } else if (class_name == 'who-stufe-3-main') {
        $('.stark-wirksame-opioide').prop('checked', false);
        $('.stark-wirksame-opioide-child').hide();
      }
    }
  }

  function showWhoStufeChild($this)
  {
    var class_name = $($this).attr('data-class-name');
    
    if ($($this).is(':checked')) {    
      $('.'+class_name).show();
    } else {
      $('.'+class_name).hide();
    }
  }

  function selectMedikamentFirst($this)
  {
    var selected_value = $($this).attr('value');
    var selected_text = $($this).text();
    //alert(selected_text);
    $($this).closest('.medikament-main-row').find('.opioidanalgetika-field-1').val(selected_value);

    if (selected_value == 'Freifeld') {
      $($this).closest('.medikament-main-row').find('.medikament_free_text_first').show();
    } else {
      $($this).closest('.medikament-main-row').find('.medikament_free_text_first').hide();
    }
    
    $($this).closest('.medikament-main-row').find('.nicht_btn_1').text(selected_text);
  }


  function selectMedikamentSecond($this)
  {
    var selected_value = $($this).attr('value');
    var selected_text = $($this).text();

    $($this).closest('.medikament-main-row').find('.opioidanalgetika-field-2').val(selected_value);

    if (selected_value == 'Freifeld') {
      $($this).closest('.medikament-main-row').find('.medikament_free_text_second').show();
    } else {
      $($this).closest('.medikament-main-row').find('.medikament_free_text_second').hide();
    }
    
    $($this).closest('.medikament-main-row').find('.nicht_btn_2').text(selected_text);
  }

  function selectMedikamentThird($this)
  {
    var selected_value = $($this).attr('value');
    var selected_text = $($this).text();

    $($this).closest('.medikament-main-row').find('.opioidanalgetika-field-3').val(selected_value);

    if (selected_value == 'Freifeld') {
      $($this).closest('.medikament-main-row').find('.medikament_free_text_third').show();
    } else {
      $($this).closest('.medikament-main-row').find('.medikament_free_text_third').hide();
    }
    
    $($this).closest('.medikament-main-row').find('.nicht_btn_3').text(selected_text);
  }

  function selectGastrointestinal($this)
  {
    var selected_value = $($this).attr('value');
    var selected_text = $($this).text();

    $($this).closest('.gastrointestinal-main-row').find('.gastrointestinal-field').val(selected_value);

    if (selected_value == 'Freifeld') {
      $($this).closest('.gastrointestinal-main-row').find('.gastrointestinal-text-field').show();
    } else {
      $($this).closest('.gastrointestinal-main-row').find('.gastrointestinal-text-field').hide();
    }
    
    $($this).closest('.gastrointestinal-main-row').find('.gastrointestinal_btn').text(selected_text);
  }

  function selectKardial($this)
  {
    var selected_value = $($this).attr('value');
    var selected_text = $($this).text();

    $($this).closest('.kardial-main-row').find('.kardial-field').val(selected_value);

    if (selected_value == 'Freifeld') {
      $($this).closest('.kardial-main-row').find('.kardial-text-field').show();
    } else {
      $($this).closest('.kardial-main-row').find('.kardial-text-field').hide();
    }
    
    $($this).closest('.kardial-main-row').find('.kardial_btn').text(selected_text);
  }

  function selectNephrologisch($this)
  {
    var selected_value = $($this).attr('value');
    var selected_text = $($this).text();

    $($this).closest('.nephrologisch-main-row').find('.nephrologisch-field').val(selected_value);

    if (selected_value == 'Freifeld') {
      $($this).closest('.nephrologisch-main-row').find('.nephrologisch-text-field').show();
    } else {
      $($this).closest('.nephrologisch-main-row').find('.nephrologisch-text-field').hide();
    }
    
    $($this).closest('.nephrologisch-main-row').find('.nephrologisch_btn').text(selected_text);
  }

  function selectNeurologischPsychotrop($this)
  {
    var selected_value = $($this).attr('value');
    var selected_text = $($this).text();

    $($this).closest('.neurologisch-main-row').find('.neurologisch-field').val(selected_value);

    if (selected_value == 'Freifeld') {
      $($this).closest('.neurologisch-main-row').find('.neurologisch-text-field').show();
    } else {
      $($this).closest('.neurologisch-main-row').find('.neurologisch-text-field').hide();
    }
    
    $($this).closest('.neurologisch-main-row').find('.neurologisch_btn').text(selected_text);
  }
</script>