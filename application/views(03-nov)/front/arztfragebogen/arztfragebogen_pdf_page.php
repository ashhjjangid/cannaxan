<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>


<div class="container-fluid">
<style type="text/css">
		mark{background: transparent;color: blue;}
		p{margin-top: 0; margin-bottom: 10px;}
		ul,
		ol{ display: block; clear: both; margin-bottom: 15px; margin-top: 15px; padding-left: 20px; }
		ul li{padding-left: 28px;}
		ul li,
		ol li{ display: block; clear: both; position: relative; padding-left: 22px; }
		ul li + li,
		ol li + li{ margin-top: 6px; }

		ul>li:before{ content: "";width: 10px;height: 10px;background:#303030;position: absolute;left: 0;top: 5px;background-size: 100%; border-radius: 100%;}
		ul>li li:before{ content: ""; width: 6px; height: 6px; border: solid 1px #303030; background:transparent; border-radius: 8px; position: absolute; left: 0; top: 8px; }

		ol{ list-style-type: lower-alpha; margin-block-start: 1em; margin-block-end: 1em; margin-inline-start: 0px; margin-inline-end: 0px; padding-inline-start: 18px;}
		ol li{display: list-item; text-align: -webkit-match-parent;list-style: lower-alpha; padding-left: 10px;}
		ol>li li{ list-style: none; padding-left: 25px; }
		ol>li li:before{ content: ""; width: 6px; height: 6px; border: solid 1px #303030; background:transparent; border-radius: 8px; position: absolute; left: 0; top: 8px; }
		.cheakbox{  position: relative; border: 2px solid #4dbfb6; height: 30px; width: 30px; display: block; border: 4px solid #e8e9ed; border-radius: 6px; background: #fff; box-shadow: -1px -1px 4px rgb(0 0 0 / 32%), inset -1px -1px 3px rgb(0 0 0 / 32%) }
		.cheakbox.cheak{  }
		.cheakbox.cheak:after {content: ""; position: absolute; top: 48%; left: 49%; transform: translate(-50%,-50%); -webkit-transform: translate(-50%,
			-50%); width: 15px; height: 15px; background: #170da1; }.table.site-table{border: 2px  solid #192351;}

		.table.site-table{border: 1px  solid #192351 ; text-align: center;}
table.table.site-table tr{background: #E8E9ED;}
table.table.site-table tr th{background: transparent; border-color: #192351;border-width: 2px;}
table.table.site-table thead tr{background: #5e6586;}
table.table.site-table thead tr tr{background: transparent!important;}
table.table.site-table tr th table tr:first-child th{border-top: none;border-left: none;border-right: none;}
table.table.site-table tr th table tr:last-child th{border-bottom: none;}
table.table.site-table tr th table tr:last-child th:first-child{border-left: none;}
table.table.site-table tr th table tr:last-child th:last-child{border-right: none;}
table.table.site-table tr td{border-color: #192351!important;border-width: 1px; border-style: solid; padding: 5px 1px; font-size: 12px;}
table.table.site-table tr td input{color: #000!important;}

.table-striped tbody tr:nth-of-type(odd) {
    background: #fff!important;
}
.tabel-event table.table tr:nth-child(even) {
    background: #e8e9ed;
}

	</style>
<table width="100%" cellpadding="0"  cellspacing="0" border="0" bgcolor="#fff" style="font-family: 'arial'; line-height: 1.5;" >
		<tr>
			<td>
				<table width="1000" align="center" cellpadding="0" cellspacing="0">
					<!-- <tr>
						<td>
							<table cellpadding="15" cellspacing="0" width="100%">
								<tr>
									<td style="border-bottom-style: solid; border-bottom-width: 2px; border-bottom-color: #848484; color: #848484; font-size: 20px;">Arztfragebogen zur Kostenübernahme von CannaXan 701-1.1...</td>
								</tr>
							</table>
						</td>
					</tr> -->
					<tr>
						<td>
							<table cellpadding="15" cellspacing="0" width="100%">
								<tr>
									<td>
										<table cellpadding="5" cellspacing="0" width="100%">
											<tr>
												<td style="color: #050000;">
													<?php echo $arztpraxis; ?> <br>
													<?php echo $strabe.' '. $house_no; ?> <br>
													<?php echo $plzl. ' '. $ort; ?>
												</td>
											</tr>
											<tr>
												<td></td>
											</tr>
											<tr style="color: #050000;">
												<td><strong style="font-size: 18px;">Ärztliche Bescheinigung zur Verwendung von Cannabinoiden nach § 31 Abs. 6 SGB V</strong></td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<span>Name des Patienten: <strong><?php echo $is_exists_patient->first_name; ?> <?php echo $is_exists_patient->last_name; ?></strong></span><br>
													<span>geboren am : <strong><?php echo date('d.m.Y', strtotime($is_exists_patient->date_of_birth)); ?></strong></span><br>
													<span>Versichertennummer: <strong><?php echo $is_exists_patient->insured_number; ?></strong></span>
												</td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<mark>1. Erfolgt die Verordnung im Rahmen der genehmigten Versorgungnach § 37b SGB V (spezialisierte ambulante Palliativversorgung)?</mark><br>
													<ul>
														<li><?php echo (isset($question_1_part_first->answer) && !empty($question_1_part_first->answer) && $question_1_part_first->answer == 'Yes')?'ja':'nein'; ?></li>
													</ul>
												</td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<mark>2. Welches Produkt soll dem Patienten in welcher Dosierung und Darreichungsform verordnet werden? Welche Inhaltsstoffe hat dieses Produkt?<br></mark>
													<table width="100%" cellpadding="5" cellspacing="0">
														<tr>
															<td>
																<span>Wirkstoff: <strong><?php echo $wirkstoff; ?></strong></span>
															</td>
														</tr>
														<tr>
															<td>
																<span>Handelsname: <strong><?php echo $rezepturarzneimittel; ?></strong></span>
															</td>
														</tr>
														<tr>
															<td>
																<span>Darreichungsform: <strong>oromukosales Spray, flüssig</strong></span>
															</td>
														</tr>
														<tr>
															<td>
																<span>Verordnungsmenge THC [mg] in 30 Tagen: <strong>: <?php echo $summe_cannaxan_thc; ?></strong></span>
															</td>
														</tr>
														<tr>
															<td>
																<span>Tagesdosis: Auftitration gemäß ärztlichem Therapieplan. Ermittlung der optimalen Tagesdosis erst nach Ende der Titrationsphase ermittelbar</span>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<mark>3. Welche Erkrankung soll behandelt werden?</mark>
													<br>
													
															
															<?php echo (isset($hauptindikation->indikation) && !empty($hauptindikation->indikation) && $hauptindikation->indikation)?'<ul><li>'.$hauptindikation->indikation.',':''; ?> <?php echo (isset($hauptindikation->icd_10) && !empty($hauptindikation->icd_10) && $hauptindikation->icd_10)?$hauptindikation->icd_10.'</li></ul>':''; ?>
														
												</td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<mark>4. Wie lautet das Behandlungsziel?</mark>
													<?php if ($question_1_part_second) { ?>
														<ul>

															<?php foreach ($question_1_part_second as $key => $value) { ?>
																<li>
																	<?php 
																		if ($value->answer == '1') { 
																			echo 'Kontrolle des Appetits';
																		} else if ($value->answer == '2') { 
																			echo 'Kontrolle des Gewichtsverlusts';
																		} else if ($value->answer == '3') {
																			echo 'Verbesserung der Schmerzsituation';
																		} else if ($value->answer == '4') { 
																			echo 'Verbesserung der Lebensqualität';
																		} else if ($value->answer == '5') { 
																			echo 'Erhalt der Arbeitsfähigkeit';
																		} else if ($value->behandlungsziel_text_field) {
																			echo $value->behandlungsziel_text_field;
																		}
																	?>
																</li>
																
															<?php } ?>
														</ul>
													<?php } ?>
													<!-- Falls ja:
													<ul>
														<li>Endgradig chronifizierte, schwerste Schmerzen mit einem Schmerzwert von auf einer NRS Skala von 0 bis 10.</li>
														<li>Aufhebung der Schlafarchitektur</li>
														<li>Depressive Entwicklung</li>
														<li>latente Suizidalität</li>
														<li>Einschränkung der beruflichen Tätigkeit
															<ul>
																<li>keine</li>
																<li>mittel</li>
															</ul>
														</li>
														<li>Die Gesamtsymptomatik erfüllt die Bedingungen einer palliativen Begleitung im Endstadium</li>
													</ul> -->
												</td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<mark>5. Ist die Erkrankung schwerwiegend?</mark>
													<br>
													<ul><li><?php echo (isset($question_2) && !empty($question_2) && $question_2->answer == 'Yes')?'Ja':'nein'; ?></li></ul>
													<?php if ($question_2 && $question_2->answer == 'Yes') { ?>
														<ul>
													<mark>Falls ja, welcher Verlauf/Symptomatik/Beeinträchtigungen oder anderes begründet den Schweregrad?</mark>
															<?php if ($question_2->endgradig_chronifizierte_number) { ?>
																<li>Endgradig chronifizierte, schwerste Schmerzen mit einem Schmerzwert von <?php echo $question_2->endgradig_chronifizierte_number; ?> auf einer NRS Skala von 0 bis 10.</li>
															<?php } ?>

															<?php if ($question_2->aufhebung_der_schlafarchitektur) { ?>
																<li>Aufhebung der Schlafarchitektur</li>
															<?php } ?>

															<?php if ($question_2->depressive_entwicklung) { ?>

																<li>Depressive Entwicklung</li>
															<?php } ?>

															<?php if ($question_2->latente_suizidalitat) { ?>
																<li>latente Suizidalität</li>
															<?php } ?>

															<?php if ($question_2->beruflichen_tatigkeit) { ?>

																<li>Einschränkung der beruflichen Tätigkeit:
																	<ul>
																		<?php if ($question_2->beruflichen_tatigkeit == 'keine') { ?>
																			<li>keine</li>
																		<?php } else if ($question_2->beruflichen_tatigkeit == 'mittel') { ?>
																			<li>mittel</li>
																		<?php } else if ($question_2->beruflichen_tatigkeit == 'schwer') { ?>
																			<li>schwer</li>
																		<?php } else if ($question_2->beruflichen_tatigkeit == 'Aufgabe der beruflichen Tätigkeit') { ?>
																			<li>Aufgabe der beruflichen Tätigkeit</li>
																		<?php } ?>

																	</ul>
																</li>	
															<?php } ?>
															
															<?php if ($question_2->bettlagerigkeit_aufgrund_schmerzen) { ?>
																<li>Zunehmende Bettlägerigkeit aufgrund Schmerzen 
																</li>
															<?php } ?>
															<?php if ($question_2->bettlagerigkeit_aufgrund_schmerzen) { ?>
																<li>Kurze Beschreibung der Grunderkrankung
																	<ul>
																		<li><?php echo $question_2->bettlagerigkeit_aufgrund_schmerzen; ?></li>
																	</ul>
																</li>
															<?php } ?>

															<?php if ($question_2->die_gesamtsymptomatik) { ?>
																<li>Die Gesamtsymptomatik erfüllt die Bedingungen einer palliativen Begleitung im Endstadium.
																</li>
															<?php } ?>

															<?php if ($question_2->die_lebenserwartung) { ?>
																<li>Die Lebenserwartung der/Patientin, des Patienten wird auf ca. <?php echo $question_2->die_lebenserwartung; ?> Monate geschätzt.</li>
															<?php } ?>
														</ul>
													<?php } ?>
														
													<br>
													
													<br>

													<?php if ($question_3 && $question_3->answer == 'Yes') { ?>
														Es ist eine medizinische Versorgung (ärztliche Behandlung, Arzneimitteltherapie) erforderlich, ohne die nach ärztlicher Einschätzung

														<ul>
															<?php if ($question_3->eine_lebensbedrohliche_verschlimmerung) { ?>
																<br/>eine lebensbedrohliche Verschlimmerung
															<?php } ?>

															<?php if ($question_3->eine_verminderung_der_lebenserwartung) { ?>
																<br/>eine Verminderung der Lebenserwartung
															<?php } ?>

															<?php if ($question_3->dauerhafte_beeintrachtigung) { ?>
																<br/>eine dauerhafte Beeinträchtigung der gesundheitsbezogenen Lebensqualität
															<?php } ?>
														</ul>
													<p>durch die zugrundeliegende schwerwiegende Erkrankung zu erwarten ist. Vergleiche Definition der schwerwiegenden chronischen Erkrankungen durch den Gemeinsamen Bundesausschuss (<a style="color: inherit;" href="https://www.g‐ba.de/institution/sys/faq/12/" target="_blank">https://www.g‐ba.de/institution/sys/faq/12/</a>). Die Schwere der Erkrankung wird nach SGB V begründet durch unkontrollierbare Schmerzen unter Ausschöpfung der Standardtherapien und schwere neuropsychiatrische Einschränkungen.</p>
													<?php } ?>
												</td>
											</tr>
											<!-- <tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													Falls ja, welcher Verlauf/Symptomatik/Beeinträchtigungen oder anderes begründet den Schweregrad?
													<br>
													<?php if ($question_2 && $question_2->answer == 'Yes') { ?>
														<ul>
															<?php if ($question_2->endgradig_chronifizierte_number) { ?>
																<li>Endgradig chronifizierte, schwerste Schmerzen mit einem Schmerzwert von auf einer NRS Skala von <?php echo $question_2->endgradig_chronifizierte_number; ?> 0 bis 10.</li>
															<?php } ?>

															<?php if ($question_2->aufhebung_der_schlafarchitektur) { ?>
																<li>Aufhebung der Schlafarchitektur</li>
															<?php } ?>

															<?php if ($question_2->depressive_entwicklung) { ?>

																<li>Depressive Entwicklung</li>
															<?php } ?>

															<?php if ($question_2->latente_suizidalitat) { ?>
																<li>latente Suizidalität</li>
															<?php } ?>

															<?php if ($question_2->beruflichen_tatigkeit) { ?>

																<li>Einschränkung der beruflichen Tätigkeit:
																	<ul>
																		<?php if ($question_2->beruflichen_tatigkeit == 'keine') { ?>
																			<li>keine</li>
																		<?php } else if ($question_2->beruflichen_tatigkeit == 'mittel') { ?>
																			<li>mittel</li>
																		<?php } else if ($question_2->beruflichen_tatigkeit == 'schwer') { ?>
																			<li>schwer</li>
																		<?php } else if ($question_2->beruflichen_tatigkeit == 'Aufgabe der beruflichen Tätigkeit') { ?>
																			<li>Aufgabe der beruflichen Tätigkeit</li>
																		<?php } ?>

																	</ul>
																</li>	
															<?php } ?>
															
															<?php if ($question_2->bettlagerigkeit_aufgrund_schmerzen) { ?>
																<li>Zunehmende Bettlägerigkeit aufgrund Schmerzen Kurze Beschreibung der Grunderkrankung
																	<ul>
																		<li><?php echo $question_2->bettlagerigkeit_aufgrund_schmerzen; ?></li>
																	</ul>
																</li>
															<?php } ?>

															<?php if ($question_2->die_gesamtsymptomatik) { ?>
																<li>Die Gesamtsymptomatik erfüllt die Bedingungen einer palliativen Begleitung im Endstadium.
																</li>
															<?php } ?>

															<?php if ($question_2->die_lebenserwartung) { ?>
																<li>Die Lebenserwartung der/Patientin, des Patienten wird auf ca. <?php echo $question_2->die_lebenserwartung; ?> Monate geschätzt.</li>
															<?php } ?>
														</ul>
													<?php } else { ?>
														<ul>
															<li>nein</li>
														</ul>
													<?php } ?>
													
													<br>

													<?php if ($question_3 && $question_3->answer == 'Yes') { ?>
														<ul>
															<?php if ($question_3->eine_lebensbedrohliche_verschlimmerung) { ?>
																<li>eine lebensbedrohliche Verschlimmerung</li>
															<?php } ?>

															<?php if ($question_3->eine_verminderung_der_lebenserwartung) { ?>
																<li>eine Verminderung der Lebenserwartung</li>
															<?php } ?>

															<?php if ($question_3->dauerhafte_beeintrachtigung) { ?>
																<li>eine dauerhafte Beeinträchtigung der gesundheitsbezogenen Lebensqualität</li>
															<?php } ?>
														</ul>
													<?php } ?>
												</td>
											</tr> -->
											<tr>
												<td ></td>
											</tr>
											<?php if ($is_exists_patient->grad) { ?>
												<tr>
													<td style="color: #050000;">
													<mark>6. Falls eine Schmerzchronifizierung besteht, welchen Grad besitzt diese nach dem Mainzer Stadienmodell</mark>
													<br>
														<ul>

															<li>
																<?php 
																	if ($is_exists_patient->grad == 'grad 1') { 
																		echo 'Grad 1';
																	} else if ($is_exists_patient->grad == 'grad 2') { 
																		echo 'Grad 2';
																	} else {
																		echo 'Grad 3';
																	} 
																?>
															</li>
																
															
														</ul>
													</td>
												</tr>
											<?php } ?>
											<?php if ($begleiterkrankungen){ ?>
												<tr>
													<td style="color: #050000;">
														<mark>
															<?php if (!$is_exists_patient->grad) { ?>
															6. 
														<?php } else { ?>
															7.
														<?php } ?>
														Bestehen weitere Erkrankungen?</mark>
														<br>
														<table width="100%" cellspacing="0" cellpadding="5">
															<tr>
																<!-- <td width="75%">RELEVANTE BEGLEITERKRANKUNG</td> -->
																<!-- <td width="25">
																	BESTEHEND SEIT
																</td> -->
															</tr>
														</table>
														<table width="100%" cellspacing="0" cellpadding="5" style="margin-top: 30px;">
															<tr>
																<td width="30px"></td>
																<td></td>
																<td>> 3</td>
																<td width="50px">> 6</td>
																<td width="50px">> 12</td>
																<td width="120px"> MONATEN</td>
															</tr>
															<br/>
															<?php if ($begleiterkrankungen){ $i = 1; ?>
																<?php foreach ($begleiterkrankungen as $key => $value) { ?>
																	<tr>
																		<td width="50px">
																			<strong style="font-size: 24px; color: #192351;"><?php echo $i; ?></strong>
																		</td>
																		<td><?php echo $value->description; ?></td>
																		<td width="50px"><span class="cheakbox <?php echo ($value->bestehend_seit == '3')?'cheak':''; ?>"></span></td>
																		<td width="50px"><span class="cheakbox <?php echo ($value->bestehend_seit == '6')?'cheak':''; ?>"></span></td>
																		<td width="50px"><span class="cheakbox <?php echo ($value->bestehend_seit == '12')?'cheak':''; ?>"></span></td>
																		<td width="120px"></td>
																	</tr>
																<?php $i++; } ?>
															<?php } ?>
															<!-- <tr>
																<td width="50px">
																	<strong style="font-size: 24px; color: #192351;">2</strong>
																</td>
																<td>welcher Verlauf/Symptomatik/Beeinträchtigungen oder anderes begründet</td>
																<td width="50px"><span class="cheakbox"></span></td>
																<td width="50px"><span class="cheakbox"></span></td>
																<td width="50px"><span class="cheakbox cheak"></span></td>
																<td width="120px"></td>
															</tr> -->
														</table>
													</td>
												</tr>
											<?php } ?>
											<tr>
												<td ></td>
											</tr>
											<?php if ($begleitmedikation) { ?>
												<tr>
													<td style="color: #050000;">
														<mark>
															<?php if (!$is_exists_patient->grad) { ?>
																<?php if (!$begleiterkrankungen) { ?>
																	6.
																<?php } else { ?>
																	7. 
																<?php } ?>
														<?php } else { ?>
															<?php if (!$begleiterkrankungen) { ?>
																	7.
																<?php } else { ?>
																	8. 
																<?php } ?>
															
														<?php } ?>
														Welche Medikation wird aktuell verwendet? (Bitte Angaben von Wirkstoff und Dosis)</mark>
														<!-- <br><strong style="color: red">[Verlaufskontrolle „Begleitmedikation“]</strong> -->
														<br/>
														<br/>
														<table class="table site-table table-striped" width="100%" cellpadding="0" cellspacing="0" style=" border: 1px  solid #192351; border-collapse: collapse;">
											             <tr style="background: #5e6586!important;">
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Nr.</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Wirksoff</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Handelsname</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Stärke</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Einheit</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Form</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Morgens</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Mittags</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Abends</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Nachts</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Einheit</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Tagesdosis</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Grund</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Startdatum</th>
											               <th style="color: #fff; font-size: 12px; border:1px solid #192351; padding: 7px 2px">Stopdatum</th>
											            </tr>

											            <?php if ($begleitmedikation) { $i = 1; ?>

											            	<?php foreach ($begleitmedikation as $key => $value) { ?>
											            		<tr>
													            	<td><?php echo $i; $i++; ?></td>
													            	<td><?php echo $value->wriksoff; ?></td>
													            	<td><?php echo $value->handelsname; ?></td>
													            	<td><?php echo $value->starke; ?></td>
													            	<td><?php echo $value->eiheit; ?></td>
													            	<td><?php echo $value->form; ?></td>
													            	<td><?php echo $value->morgens; ?></td>
													            	<td><?php echo $value->mittags; ?></td>
													            	<td><?php echo $value->abends; ?></td>
													            	<td><?php echo $value->zur_nacht; ?></td>
													            	<td><?php echo $value->einheit; ?></td>
													            	<td><?php echo $value->tagesdosis; ?></td>
													            	<td><?php echo $value->grund; ?></td>
													            	<td><?php echo ($value->startdatum)?date('d.m.Y', strtotime($value->startdatum)):''; ?></td>
													            	<td><?php echo ($value->stopdatum)?date('d.m.Y', strtotime($value->stopdatum)):''; ?></td>
													            </tr>
											            	<?php } ?>
											            <?php } ?>
											         </table>
													</td>
												</tr>
											<?php } ?>
											<tr>
												<td style="color: #050000;">
													<mark>
													<?php if (!$is_exists_patient->grad) { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																6.
															<?php } else { ?>
																7.
															<?php } ?>
														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																7.
															<?php } else { ?>
																8.
															<?php } ?>																
														<?php } ?>
														
													<?php } else { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																7.
															<?php } else { ?>
																8.
															<?php } ?>
															
														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																8.
															<?php } else { ?>
																9.
															<?php } ?>
															 
														<?php } ?>
														
													<?php } ?>
													Welche weiteren Therapien werden zurzeit durchgeführt?</mark>
													<br><!-- <strong style="color: red">[Arztfragebogen 4/15]</strong> -->
													<ul>
														<?php if (isset($question_4->physiotherapie_1) && $question_4->physiotherapie_1) { ?>
															
															<li><?php echo $question_4->physiotherapie_1; ?></li>
														<?php } ?> 
														<?php if (isset($question_4->interventionelle_verfahren) && $question_4->interventionelle_verfahren) { ?>
															
															<li><?php echo $question_4->interventionelle_verfahren; ?></li>
														<?php } ?> 
														<?php if (isset($question_4->psychotherapie_2) && $question_4->psychotherapie_2) { ?>
															
															<li><?php echo $question_4->psychotherapie_2; ?></li>
														<?php } ?>
														
														<?php if (isset($question_4->tens) && $question_4->tens) { ?>
															
															<li><?php echo $question_4->tens; ?></li>
														<?php } ?>
														<?php if (isset($question_4->akupunktur) && $question_4->akupunktur) { ?>
															
															<li><?php echo $question_4->akupunktur; ?></li>
														<?php } ?>
														<?php if (isset($question_4->keine) && $question_4->keine) { ?>
															
															<li><?php echo $question_4->keine; ?></li>
														<?php } ?>
													</ul>
													<br>
													<p><mark>
													<?php if (!$is_exists_patient->grad) { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																7.
															<?php } else { ?>
																8.
															<?php } ?>
														
														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																8.
															<?php } else { ?>
																9.
															<?php } ?>
															
														<?php } ?>
														
													<?php } else { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																8.
															<?php } else { ?>
																9.
															<?php } ?>

														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																9.
															<?php } else { ?>
																10.
															<?php } ?>
															
														<?php } ?>
														
													<?php } ?>
													Welche bisherige Therapie ist bei der Erkrankung unter 3. mit welchem Erfolg durchgeführt worden?</mark></p>
													<ul>
														<?php if ($question_5) { ?>
															
															<li>
																<?php if ($question_5_WHO_1_nicht || $question_5_WHO_1_steroide || $question_5_WHO_1_neuroleptika || $question_5_WHO_1_antidepressiva || $question_5_WHO_1_sedativa || $question_5_WHO_1_antikonvulsiva || $question_5_WHO_1_antiemetika) { ?>

																		WHO Stufe 1
																	
																<?php } ?>
																<ul>
																	<?php if (isset($question_5_WHO_1_nicht->option_name) && $question_5_WHO_1_nicht->option_name) { ?>
															
																		<li><?php echo $question_5_WHO_1_nicht->option_name; ?>

																			<ul><li><strong>Medikament:</strong>
																				<?php if (isset($question_5_WHO_1_nicht->medikament_dropdown_1) && $question_5_WHO_1_nicht->medikament_dropdown_1) { ?>
																				<ul><?php echo $question_5_WHO_1_nicht->medikament_dropdown_1; ?>
																				<?php if($question_5_WHO_1_nicht->medikament_dropdown_1 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_nicht->medikament_dropdown_1_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_nicht->medikament_dropdown_2) && $question_5_WHO_1_nicht->medikament_dropdown_2) { ?>
																					<ul><?php echo $question_5_WHO_1_nicht->medikament_dropdown_2; ?>
																						<?php if($question_5_WHO_1_nicht->medikament_dropdown_2 == 'Freifeld') { ?>
																							<ul><?php echo $question_5_WHO_1_nicht->medikament_dropdown_2_text_field; ?></ul>
																						<?php } ?>
																					</ul>
																						
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_nicht->medikament_dropdown_3) && $question_5_WHO_1_nicht->medikament_dropdown_3) { ?>
																					<ul><?php echo $question_5_WHO_1_nicht->medikament_dropdown_3; ?>
																				<?php if($question_5_WHO_1_nicht->medikament_dropdown_3 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_nicht->medikament_dropdown_3_text_field; ?></ul>
																					<?php } ?>
																					</ul>
																						
																				<?php } ?>
																			</li></ul>
																			
																			<?php if (isset($question_5_WHO_1_nicht) && $question_5_WHO_1_nicht->therapieerfolg) { ?>
																				<ul><li>Therapieerfolg:
																				<?php echo ($question_5_WHO_1_nicht->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																			</li></ul>
																			<?php } ?>
																			
																			<?php if ($question_5_WHO_1_nicht->gastrointestinal_dropdown || $question_5_WHO_1_nicht->kardial_dropdown || $question_5_WHO_1_nicht->nephrologisch_dropdown || $question_5_WHO_1_nicht->neurologisch_dropdown) { ?>
																				
																			<ul><li>
																					
																				Nebenwirkungen:
																	
																			<?php if (isset($question_5_WHO_1_nicht->gastrointestinal_dropdown) && $question_5_WHO_1_nicht->gastrointestinal_dropdown) { ?>
																			<ul>Gastrointestinal:
																				<?php echo $question_5_WHO_1_nicht->gastrointestinal_dropdown ?>
																				
																				<?php if ($question_5_WHO_1_nicht->gastrointestinal_dropdown == 'Freifeld') { ?>
																					<ul style="padding-left: 174px"><?php echo $question_5_WHO_1_nicht->gastrointestinal_dropdown_text_field; ?></ul>
																				<?php } ?>
																			</ul>
																			<?php } ?>
																				
																			<?php if (isset($question_5_WHO_1_nicht->kardial_dropdown) && $question_5_WHO_1_nicht->kardial_dropdown) { ?>
																				<ul>Kardial:
																					<?php echo $question_5_WHO_1_nicht->kardial_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_nicht->kardial_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 115px">
																							<br/><?php echo $question_5_WHO_1_nicht->kardial_dropdown_text_field; ?>
																						</ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_nicht->nephrologisch_dropdown) && $question_5_WHO_1_nicht->nephrologisch_dropdown) { ?>
																				<ul>Nephrologisch:
																					<?php echo $question_5_WHO_1_nicht->nephrologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_nicht->nephrologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 165px"><?php echo $question_5_WHO_1_nicht->nephrologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_nicht->neurologisch_dropdown) && $question_5_WHO_1_nicht->neurologisch_dropdown) { ?>
																				<ul>Neurologisch/psychotrop:
																					<?php echo $question_5_WHO_1_nicht->neurologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_nicht->neurologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 241px"><?php echo $question_5_WHO_1_nicht->neurologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			</li>
																			</ul>
																		<?php } ?>
																		</li>
																	
																	<?php } ?>

																	<?php if (isset($question_5_WHO_1_steroide->option_name) && $question_5_WHO_1_steroide->option_name) { ?>
															
																		<li><?php echo $question_5_WHO_1_steroide->option_name; ?>

																			<ul><li><strong>Medikament:</strong>
																				<?php if (isset($question_5_WHO_1_steroide->medikament_dropdown_1) && $question_5_WHO_1_steroide->medikament_dropdown_1) { ?>
																				<ul><?php echo $question_5_WHO_1_steroide->medikament_dropdown_1; ?>
																				<?php if($question_5_WHO_1_steroide->medikament_dropdown_1 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_steroide->medikament_dropdown_1_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_steroide->medikament_dropdown_2) && $question_5_WHO_1_steroide->medikament_dropdown_2) { ?>
																					<ul><?php echo $question_5_WHO_1_steroide->medikament_dropdown_2; ?>
																						<?php if($question_5_WHO_1_steroide->medikament_dropdown_2 == 'Freifeld') { ?>
																							<ul><?php echo $question_5_WHO_1_steroide->medikament_dropdown_2_text_field; ?></ul>
																						<?php } ?>
																					</ul>
																						
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_steroide->medikament_dropdown_3) && $question_5_WHO_1_steroide->medikament_dropdown_3) { ?>
																					<ul><?php echo $question_5_WHO_1_steroide->medikament_dropdown_3; ?>
																				<?php if($question_5_WHO_1_steroide->medikament_dropdown_3 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_steroide->medikament_dropdown_3_text_field; ?></ul>
																					<?php } ?>
																					</ul>
																						
																				<?php } ?>
																			</li></ul>
																			<?php if (isset($question_5_WHO_1_steroide) && $question_5_WHO_1_steroide->therapieerfolg) { ?>
																			<ul><li>Therapieerfolg:
																				<?php echo ($question_5_WHO_1_steroide->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																			</li></ul>
																			<?php } ?>
																			
																			<?php if ($question_5_WHO_1_steroide->gastrointestinal_dropdown || $question_5_WHO_1_steroide->kardial_dropdown || $question_5_WHO_1_steroide->nephrologisch_dropdown || $question_5_WHO_1_steroide->neurologisch_dropdown) { ?>
																				
																			<ul><li>
																					
																				Nebenwirkungen:
																	
																			<?php if (isset($question_5_WHO_1_steroide->gastrointestinal_dropdown) && $question_5_WHO_1_steroide->gastrointestinal_dropdown) { ?>
																			<ul>Gastrointestinal:
																				<?php echo $question_5_WHO_1_steroide->gastrointestinal_dropdown ?>
																				
																				<?php if ($question_5_WHO_1_steroide->gastrointestinal_dropdown == 'Freifeld') { ?>
																					<ul style="padding-left: 174px">
																						<?php echo $question_5_WHO_1_steroide->gastrointestinal_dropdown_text_field; ?>
																					</ul>
																				<?php } ?>
																			</ul>
																			<?php } ?>
																				
																			<?php if (isset($question_5_WHO_1_steroide->kardial_dropdown) && $question_5_WHO_1_steroide->kardial_dropdown) { ?>
																				<ul>Kardial:
																					<?php echo $question_5_WHO_1_steroide->kardial_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_steroide->kardial_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 115px">
																							<li><?php echo $question_5_WHO_1_steroide->kardial_dropdown_text_field; ?></li>
																						</ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_steroide->nephrologisch_dropdown) && $question_5_WHO_1_steroide->nephrologisch_dropdown) { ?>
																				<ul>Nephrologisch:
																					<?php echo $question_5_WHO_1_steroide->nephrologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_steroide->nephrologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 165px"><?php echo $question_5_WHO_1_steroide->nephrologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_steroide->neurologisch_dropdown) && $question_5_WHO_1_steroide->neurologisch_dropdown) { ?>
																				<ul>Neurologisch/psychotrop:
																					<?php echo $question_5_WHO_1_steroide->neurologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_steroide->neurologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 241px"><?php echo $question_5_WHO_1_steroide->neurologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			</li>
																			</ul>
																		<?php } ?>
																		</li>
																	<?php } ?>
																	<?php if (isset($question_5_WHO_1_neuroleptika->option_name) && $question_5_WHO_1_neuroleptika->option_name) { ?>
															
																		<li><?php echo $question_5_WHO_1_neuroleptika->option_name; ?>

																			<ul><li><strong>Medikament:</strong>
																				<?php if (isset($question_5_WHO_1_neuroleptika->medikament_dropdown_1) && $question_5_WHO_1_neuroleptika->medikament_dropdown_1) { ?>
																				<ul><?php echo $question_5_WHO_1_neuroleptika->medikament_dropdown_1; ?>
																				<?php if($question_5_WHO_1_neuroleptika->medikament_dropdown_1 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_neuroleptika->medikament_dropdown_1_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_neuroleptika->medikament_dropdown_2) && $question_5_WHO_1_neuroleptika->medikament_dropdown_2) { ?>
																					<ul><?php echo $question_5_WHO_1_neuroleptika->medikament_dropdown_2; ?>
																						<?php if($question_5_WHO_1_neuroleptika->medikament_dropdown_2 == 'Freifeld') { ?>
																							<ul><?php echo $question_5_WHO_1_neuroleptika->medikament_dropdown_2_text_field; ?></ul>
																						<?php } ?>
																					</ul>
																						
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_neuroleptika->medikament_dropdown_3) && $question_5_WHO_1_neuroleptika->medikament_dropdown_3) { ?>
																					<ul><?php echo $question_5_WHO_1_neuroleptika->medikament_dropdown_3; ?>
																				<?php if($question_5_WHO_1_neuroleptika->medikament_dropdown_3 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_neuroleptika->medikament_dropdown_3_text_field; ?></ul>
																					<?php } ?>
																					</ul>
																						
																				<?php } ?>
																			</li></ul>
																			<?php if (isset($question_5_WHO_1_neuroleptika) && $question_5_WHO_1_neuroleptika->therapieerfolg) { ?>
																			<ul><li>Therapieerfolg:
																				<?php echo ($question_5_WHO_1_neuroleptika->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																			</li></ul>
																			<?php } ?>
																			
																			<?php if ($question_5_WHO_1_neuroleptika->gastrointestinal_dropdown || $question_5_WHO_1_neuroleptika->kardial_dropdown || $question_5_WHO_1_neuroleptika->nephrologisch_dropdown || $question_5_WHO_1_neuroleptika->neurologisch_dropdown) { ?>
																				
																			<ul><li>
																					
																				Nebenwirkungen:
																	
																			<?php if (isset($question_5_WHO_1_neuroleptika->gastrointestinal_dropdown) && $question_5_WHO_1_neuroleptika->gastrointestinal_dropdown) { ?>
																			<ul>Gastrointestinal:
																				<?php echo $question_5_WHO_1_neuroleptika->gastrointestinal_dropdown ?>
																				
																				<?php if ($question_5_WHO_1_neuroleptika->gastrointestinal_dropdown == 'Freifeld') { ?>
																					<ul style="padding-left: 174px"><?php echo $question_5_WHO_1_neuroleptika->gastrointestinal_dropdown_text_field; ?></ul>
																				<?php } ?>
																			</ul>
																			<?php } ?>
																				
																			<?php if (isset($question_5_WHO_1_neuroleptika->kardial_dropdown) && $question_5_WHO_1_neuroleptika->kardial_dropdown) { ?>
																				<ul>Kardial:
																					<?php echo $question_5_WHO_1_neuroleptika->kardial_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_neuroleptika->kardial_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 115px">
																							<li><?php echo $question_5_WHO_1_neuroleptika->kardial_dropdown_text_field; ?></li>
																						</ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_neuroleptika->nephrologisch_dropdown) && $question_5_WHO_1_neuroleptika->nephrologisch_dropdown) { ?>
																				<ul>Nephrologisch:
																					<?php echo $question_5_WHO_1_neuroleptika->nephrologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_neuroleptika->nephrologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 165px"><?php echo $question_5_WHO_1_neuroleptika->nephrologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_neuroleptika->neurologisch_dropdown) && $question_5_WHO_1_neuroleptika->neurologisch_dropdown) { ?>
																				<ul>Neurologisch/psychotrop:
																					<?php echo $question_5_WHO_1_neuroleptika->neurologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_neuroleptika->neurologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 241px"><?php echo $question_5_WHO_1_neuroleptika->neurologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			</li>
																			</ul>
																		<?php } ?>
																		</li>
																	<?php } ?>
																	<?php if (isset($question_5_WHO_1_antidepressiva->option_name) && $question_5_WHO_1_antidepressiva->option_name) { ?>
															
																		<li><?php echo $question_5_WHO_1_antidepressiva->option_name; ?>

																			<ul><li><strong>Medikament:</strong>
																				<?php if (isset($question_5_WHO_1_antidepressiva->medikament_dropdown_1) && $question_5_WHO_1_antidepressiva->medikament_dropdown_1) { ?>
																				<ul><?php echo $question_5_WHO_1_antidepressiva->medikament_dropdown_1; ?>
																				<?php if($question_5_WHO_1_antidepressiva->medikament_dropdown_1 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_antidepressiva->medikament_dropdown_1_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_antidepressiva->medikament_dropdown_2) && $question_5_WHO_1_antidepressiva->medikament_dropdown_2) { ?>
																					<ul><?php echo $question_5_WHO_1_antidepressiva->medikament_dropdown_2; ?>
																						<?php if($question_5_WHO_1_antidepressiva->medikament_dropdown_2 == 'Freifeld') { ?>
																							<ul><?php echo $question_5_WHO_1_antidepressiva->medikament_dropdown_2_text_field; ?></ul>
																						<?php } ?>
																					</ul>
																						
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_antidepressiva->medikament_dropdown_3) && $question_5_WHO_1_antidepressiva->medikament_dropdown_3) { ?>
																					<ul><?php echo $question_5_WHO_1_antidepressiva->medikament_dropdown_3; ?>
																				<?php if($question_5_WHO_1_antidepressiva->medikament_dropdown_3 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_antidepressiva->medikament_dropdown_3_text_field; ?></ul>
																					<?php } ?>
																					</ul>
																						
																				<?php } ?>
																			</li></ul>
																			<?php if (isset($question_5_WHO_1_antidepressiva) && $question_5_WHO_1_antidepressiva->therapieerfolg) { ?>
																			<ul><li>Therapieerfolg:
																				<?php echo ($question_5_WHO_1_antidepressiva->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																			</li></ul>
																			<?php } ?>
																			
																			<?php if ($question_5_WHO_1_antidepressiva->gastrointestinal_dropdown || $question_5_WHO_1_antidepressiva->kardial_dropdown || $question_5_WHO_1_antidepressiva->nephrologisch_dropdown || $question_5_WHO_1_antidepressiva->neurologisch_dropdown) { ?>
																				
																			<ul><li>
																					
																				Nebenwirkungen:
																	
																			<?php if (isset($question_5_WHO_1_antidepressiva->gastrointestinal_dropdown) && $question_5_WHO_1_antidepressiva->gastrointestinal_dropdown) { ?>
																			<ul>Gastrointestinal:
																				<?php echo $question_5_WHO_1_antidepressiva->gastrointestinal_dropdown ?>
																				
																				<?php if ($question_5_WHO_1_antidepressiva->gastrointestinal_dropdown == 'Freifeld') { ?>
																					<ul style="padding-left: 174px"><?php echo $question_5_WHO_1_antidepressiva->gastrointestinal_dropdown_text_field; ?></ul>
																				<?php } ?>
																			</ul>
																			<?php } ?>
																				
																			<?php if (isset($question_5_WHO_1_antidepressiva->kardial_dropdown) && $question_5_WHO_1_antidepressiva->kardial_dropdown) { ?>
																				<ul>Kardial:
																					<?php echo $question_5_WHO_1_antidepressiva->kardial_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_antidepressiva->kardial_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 115px">
																							<li><?php echo $question_5_WHO_1_antidepressiva->kardial_dropdown_text_field; ?></li>
																						</ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_antidepressiva->nephrologisch_dropdown) && $question_5_WHO_1_antidepressiva->nephrologisch_dropdown) { ?>
																				<ul>Nephrologisch:
																					<?php echo $question_5_WHO_1_antidepressiva->nephrologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_antidepressiva->nephrologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 165px"><?php echo $question_5_WHO_1_antidepressiva->nephrologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_antidepressiva->neurologisch_dropdown) && $question_5_WHO_1_antidepressiva->neurologisch_dropdown) { ?>
																				<ul>Neurologisch/psychotrop:
																					<?php echo $question_5_WHO_1_antidepressiva->neurologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_antidepressiva->neurologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 241px"><?php echo $question_5_WHO_1_antidepressiva->neurologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			</li>
																			</ul>
																		<?php } ?>
																		</li>
																	<?php } ?>
																	<?php if (isset($question_5_WHO_1_sedativa->option_name) && $question_5_WHO_1_sedativa->option_name) { ?>
															
																		<li><?php echo $question_5_WHO_1_sedativa->option_name; ?>

																			<ul><li><strong>Medikament:</strong>
																				<?php if (isset($question_5_WHO_1_sedativa->medikament_dropdown_1) && $question_5_WHO_1_sedativa->medikament_dropdown_1) { ?>
																				<ul><?php echo $question_5_WHO_1_sedativa->medikament_dropdown_1; ?>
																				<?php if($question_5_WHO_1_sedativa->medikament_dropdown_1 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_sedativa->medikament_dropdown_1_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_sedativa->medikament_dropdown_2) && $question_5_WHO_1_sedativa->medikament_dropdown_2) { ?>
																					<ul><?php echo $question_5_WHO_1_sedativa->medikament_dropdown_2; ?>
																						<?php if($question_5_WHO_1_sedativa->medikament_dropdown_2 == 'Freifeld') { ?>
																							<ul><?php echo $question_5_WHO_1_sedativa->medikament_dropdown_2_text_field; ?></ul>
																						<?php } ?>
																					</ul>
																						
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_sedativa->medikament_dropdown_3) && $question_5_WHO_1_sedativa->medikament_dropdown_3) { ?>
																					<ul><?php echo $question_5_WHO_1_sedativa->medikament_dropdown_3; ?>
																				<?php if($question_5_WHO_1_sedativa->medikament_dropdown_3 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_sedativa->medikament_dropdown_3_text_field; ?></ul>
																					<?php } ?>
																					</ul>
																						
																				<?php } ?>
																			</li></ul>
																			<?php if (isset($question_5_WHO_1_sedativa) && $question_5_WHO_1_sedativa->therapieerfolg) { ?>
																				<ul><li>Therapieerfolg:
																				<?php echo ($question_5_WHO_1_sedativa->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																				</li></ul>
																			<?php } ?>
																			
																			<?php if ($question_5_WHO_1_sedativa->gastrointestinal_dropdown || $question_5_WHO_1_sedativa->kardial_dropdown || $question_5_WHO_1_sedativa->nephrologisch_dropdown || $question_5_WHO_1_sedativa->neurologisch_dropdown) { ?>
																				
																			<ul><li>
																					
																				Nebenwirkungen:
																	
																			<?php if (isset($question_5_WHO_1_sedativa->gastrointestinal_dropdown) && $question_5_WHO_1_sedativa->gastrointestinal_dropdown) { ?>
																			<ul>Gastrointestinal:
																				<?php echo $question_5_WHO_1_sedativa->gastrointestinal_dropdown ?>
																				
																				<?php if ($question_5_WHO_1_sedativa->gastrointestinal_dropdown == 'Freifeld') { ?>
																					<ul style="padding-left: 174px"><?php echo $question_5_WHO_1_sedativa->gastrointestinal_dropdown_text_field; ?></ul>
																				<?php } ?>
																			</ul>
																			<?php } ?>
																				
																			<?php if (isset($question_5_WHO_1_sedativa->kardial_dropdown) && $question_5_WHO_1_sedativa->kardial_dropdown) { ?>
																				<ul>Kardial:
																					<?php echo $question_5_WHO_1_sedativa->kardial_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_sedativa->kardial_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 115px">
																							<li><?php echo $question_5_WHO_1_sedativa->kardial_dropdown_text_field; ?></li>
																						</ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_sedativa->nephrologisch_dropdown) && $question_5_WHO_1_sedativa->nephrologisch_dropdown) { ?>
																				<ul>Nephrologisch:
																					<?php echo $question_5_WHO_1_sedativa->nephrologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_sedativa->nephrologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 165px"><?php echo $question_5_WHO_1_sedativa->nephrologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_sedativa->neurologisch_dropdown) && $question_5_WHO_1_sedativa->neurologisch_dropdown) { ?>
																				<ul>Neurologisch/psychotrop:
																					<?php echo $question_5_WHO_1_sedativa->neurologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_sedativa->neurologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 241px"><?php echo $question_5_WHO_1_sedativa->neurologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			</li>
																			</ul>
																		<?php } ?>
																		</li>
																	<?php } ?>

																	<?php if (isset($question_5_WHO_1_antikonvulsiva->option_name) && $question_5_WHO_1_antikonvulsiva->option_name) { ?>
															
																		<li><?php echo $question_5_WHO_1_antikonvulsiva->option_name; ?>

																			<ul><li><strong>Medikament:</strong>
																				<?php if (isset($question_5_WHO_1_antikonvulsiva->medikament_dropdown_1) && $question_5_WHO_1_antikonvulsiva->medikament_dropdown_1) { ?>
																				<ul><?php echo $question_5_WHO_1_antikonvulsiva->medikament_dropdown_1; ?>
																				<?php if($question_5_WHO_1_antikonvulsiva->medikament_dropdown_1 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_antikonvulsiva->medikament_dropdown_1_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_antikonvulsiva->medikament_dropdown_2) && $question_5_WHO_1_antikonvulsiva->medikament_dropdown_2) { ?>
																					<ul><?php echo $question_5_WHO_1_antikonvulsiva->medikament_dropdown_2; ?>
																						<?php if($question_5_WHO_1_antikonvulsiva->medikament_dropdown_2 == 'Freifeld') { ?>
																							<ul><?php echo $question_5_WHO_1_antikonvulsiva->medikament_dropdown_2_text_field; ?></ul>
																						<?php } ?>
																					</ul>
																						
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_antikonvulsiva->medikament_dropdown_3) && $question_5_WHO_1_antikonvulsiva->medikament_dropdown_3) { ?>
																					<ul><?php echo $question_5_WHO_1_antikonvulsiva->medikament_dropdown_3; ?>
																				<?php if($question_5_WHO_1_antikonvulsiva->medikament_dropdown_3 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_antikonvulsiva->medikament_dropdown_3_text_field; ?></ul>
																					<?php } ?>
																					</ul>
																						
																				<?php } ?>
																			</li></ul>
																			<?php if (isset($question_5_WHO_1_antikonvulsiva) && $question_5_WHO_1_antikonvulsiva->therapieerfolg) { ?>
																			<ul><li>Therapieerfolg:
																				<?php echo ($question_5_WHO_1_antikonvulsiva->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																			</li></ul>
																			<?php } ?>
																			
																			<?php if ($question_5_WHO_1_antikonvulsiva->gastrointestinal_dropdown || $question_5_WHO_1_antikonvulsiva->kardial_dropdown || $question_5_WHO_1_antikonvulsiva->nephrologisch_dropdown || $question_5_WHO_1_antikonvulsiva->neurologisch_dropdown) { ?>
																				
																			<ul><li>
																					
																				Nebenwirkungen:
																	
																			<?php if (isset($question_5_WHO_1_antikonvulsiva->gastrointestinal_dropdown) && $question_5_WHO_1_antikonvulsiva->gastrointestinal_dropdown) { ?>
																			<ul>Gastrointestinal:
																				<?php echo $question_5_WHO_1_antikonvulsiva->gastrointestinal_dropdown ?>
																				
																				<?php if ($question_5_WHO_1_antikonvulsiva->gastrointestinal_dropdown == 'Freifeld') { ?>
																					<ul style="padding-left: 174px"><?php echo $question_5_WHO_1_antikonvulsiva->gastrointestinal_dropdown_text_field; ?></ul>
																				<?php } ?>
																			</ul>
																			<?php } ?>
																				
																			<?php if (isset($question_5_WHO_1_antikonvulsiva->kardial_dropdown) && $question_5_WHO_1_antikonvulsiva->kardial_dropdown) { ?>
																				<ul>Kardial:
																					<?php echo $question_5_WHO_1_antikonvulsiva->kardial_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_antikonvulsiva->kardial_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 115px">
																							<li><?php echo $question_5_WHO_1_antikonvulsiva->kardial_dropdown_text_field; ?></li>
																						</ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_antikonvulsiva->nephrologisch_dropdown) && $question_5_WHO_1_antikonvulsiva->nephrologisch_dropdown) { ?>
																				<ul>Nephrologisch:
																					<?php echo $question_5_WHO_1_antikonvulsiva->nephrologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_antikonvulsiva->nephrologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 165px"><li><?php echo $question_5_WHO_1_antikonvulsiva->nephrologisch_dropdown_text_field; ?></li></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_antikonvulsiva->neurologisch_dropdown) && $question_5_WHO_1_antikonvulsiva->neurologisch_dropdown) { ?>
																				<ul>Neurologisch/psychotrop:
																					<?php echo $question_5_WHO_1_antikonvulsiva->neurologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_antikonvulsiva->neurologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 241px"><?php echo $question_5_WHO_1_antikonvulsiva->neurologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			</li>
																			</ul>
																		<?php } ?>
																		</li>
																	<?php } ?>

																	<?php if (isset($question_5_WHO_1_antiemetika->option_name) && $question_5_WHO_1_antiemetika->option_name) { ?>
															
																		<li><?php echo $question_5_WHO_1_antiemetika->option_name; ?>

																			<ul><li><strong>Medikament:</strong>
																				<?php if (isset($question_5_WHO_1_antiemetika->medikament_dropdown_1) && $question_5_WHO_1_antiemetika->medikament_dropdown_1) { ?>
																				<ul><?php echo $question_5_WHO_1_antiemetika->medikament_dropdown_1; ?>
																				<?php if($question_5_WHO_1_antiemetika->medikament_dropdown_1 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_antiemetika->medikament_dropdown_1_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_antiemetika->medikament_dropdown_2) && $question_5_WHO_1_antiemetika->medikament_dropdown_2) { ?>
																					<ul><?php echo $question_5_WHO_1_antiemetika->medikament_dropdown_2; ?>
																						<?php if($question_5_WHO_1_antiemetika->medikament_dropdown_2 == 'Freifeld') { ?>
																							<ul><?php echo $question_5_WHO_1_antiemetika->medikament_dropdown_2_text_field; ?></ul>
																						<?php } ?>
																					</ul>
																						
																				<?php } ?>
																				<?php if (isset($question_5_WHO_1_antiemetika->medikament_dropdown_3) && $question_5_WHO_1_antiemetika->medikament_dropdown_3) { ?>
																					<ul><?php echo $question_5_WHO_1_antiemetika->medikament_dropdown_3; ?>
																				<?php if($question_5_WHO_1_antiemetika->medikament_dropdown_3 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_1_antiemetika->medikament_dropdown_3_text_field; ?></ul>
																					<?php } ?>
																					</ul>
																						
																				<?php } ?>
																			</li></ul>
																			<?php if (isset($question_5_WHO_1_antiemetika) && $question_5_WHO_1_antiemetika->therapieerfolg) { ?>
																			<ul><li>Therapieerfolg:
																				<?php echo ($question_5_WHO_1_antiemetika->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																			</li></ul>
																			<?php } ?>
																			
																			<?php if ($question_5_WHO_1_antiemetika->gastrointestinal_dropdown || $question_5_WHO_1_antiemetika->kardial_dropdown || $question_5_WHO_1_antiemetika->nephrologisch_dropdown || $question_5_WHO_1_antiemetika->neurologisch_dropdown) { ?>
																				
																			<ul><li>
																					
																				Nebenwirkungen:
																	
																			<?php if (isset($question_5_WHO_1_antiemetika->gastrointestinal_dropdown) && $question_5_WHO_1_antiemetika->gastrointestinal_dropdown) { ?>
																			<ul>Gastrointestinal:
																				<?php echo $question_5_WHO_1_antiemetika->gastrointestinal_dropdown ?>
																				
																				<?php if ($question_5_WHO_1_antiemetika->gastrointestinal_dropdown == 'Freifeld') { ?>
																					<ul style="padding-left: 174px"><?php echo $question_5_WHO_1_antiemetika->gastrointestinal_dropdown_text_field; ?></ul>
																				<?php } ?>
																			</ul>
																			<?php } ?>
																				
																			<?php if (isset($question_5_WHO_1_antiemetika->kardial_dropdown) && $question_5_WHO_1_antiemetika->kardial_dropdown) { ?>
																				<ul>Kardial:
																					<?php echo $question_5_WHO_1_antiemetika->kardial_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_antiemetika->kardial_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 115px">
																							<li><?php echo $question_5_WHO_1_antiemetika->kardial_dropdown_text_field; ?></li>
																						</ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_antiemetika->nephrologisch_dropdown) && $question_5_WHO_1_antiemetika->nephrologisch_dropdown) { ?>
																				<ul>Nephrologisch:
																					<?php echo $question_5_WHO_1_antiemetika->nephrologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_antiemetika->nephrologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 165px"><?php echo $question_5_WHO_1_antiemetika->nephrologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_1_antiemetika->neurologisch_dropdown) && $question_5_WHO_1_antiemetika->neurologisch_dropdown) { ?>
																				<ul>Neurologisch/psychotrop:
																					<?php echo $question_5_WHO_1_antiemetika->neurologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_1_antiemetika->neurologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 241px"><?php echo $question_5_WHO_1_antiemetika->neurologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			</li>
																			</ul>
																		<?php } ?>
																		</li>
																	<?php } ?>
																</ul>
															</li>
														<?php } ?>
														
													
														<?php if (isset($question_5_WHO_2) && $question_5_WHO_2) { ?>
															
															<li>WHO Stufe 2
																<ul>
																	<?php if (isset($question_5_WHO_2->option_name) && $question_5_WHO_2->option_name) { ?>
															
																		<li><?php echo $question_5_WHO_2->option_name; ?>

																			<ul><li><strong>Medikament:</strong>
																				<?php if (isset($question_5_WHO_2->medikament_dropdown_1) && $question_5_WHO_2->medikament_dropdown_1) { ?>
																				<ul><?php echo $question_5_WHO_2->medikament_dropdown_1; ?>
																				<?php if($question_5_WHO_2->medikament_dropdown_1 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_2->medikament_dropdown_1_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																				<?php } ?>
																				<?php if (isset($question_5_WHO_2->medikament_dropdown_2) && $question_5_WHO_2->medikament_dropdown_2) { ?>
																					<ul><?php echo $question_5_WHO_2->medikament_dropdown_2; ?>
																						<?php if($question_5_WHO_2->medikament_dropdown_2 == 'Freifeld') { ?>
																							<ul><?php echo $question_5_WHO_2->medikament_dropdown_2_text_field; ?></ul>
																						<?php } ?>
																					</ul>
																						
																				<?php } ?>
																				<?php if (isset($question_5_WHO_2->medikament_dropdown_3) && $question_5_WHO_2->medikament_dropdown_3) { ?>
																					<ul><?php echo $question_5_WHO_2->medikament_dropdown_3; ?>
																				<?php if($question_5_WHO_2->medikament_dropdown_3 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_2->medikament_dropdown_3_text_field; ?></ul>
																					<?php } ?>
																					</ul>
																						
																				<?php } ?>
																			</li></ul>
																			<?php if (isset($question_5_WHO_2) && $question_5_WHO_2->therapieerfolg) { ?>
																			<ul><li>Therapieerfolg:
																				<?php echo ($question_5_WHO_2->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																			</li></ul>
																			<?php } ?>
																			<?php if ($question_5_WHO_2->gastrointestinal_dropdown || $question_5_WHO_2->kardial_dropdown || $question_5_WHO_2->nephrologisch_dropdown || $question_5_WHO_2->neurologisch_dropdown) { ?>
																				
																			<ul><li>
																					
																				Nebenwirkungen:
																	
																			<?php if (isset($question_5_WHO_2->gastrointestinal_dropdown) && $question_5_WHO_2->gastrointestinal_dropdown) { ?>
																			<ul>Gastrointestinal:
																				<?php echo $question_5_WHO_2->gastrointestinal_dropdown ?>
																				
																				<?php if ($question_5_WHO_2->gastrointestinal_dropdown == 'Freifeld') { ?>
																					<ul style="padding-left: 174px"><?php echo $question_5_WHO_2->gastrointestinal_dropdown_text_field; ?></ul>
																				<?php } ?>
																			</ul>
																			<?php } ?>
																				
																			<?php if (isset($question_5_WHO_2->kardial_dropdown) && $question_5_WHO_2->kardial_dropdown) { ?>
																				<ul>Kardial:
																					<?php echo $question_5_WHO_2->kardial_dropdown ?>
																					
																					<?php if ($question_5_WHO_2->kardial_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 115px">
																							<li><?php echo $question_5_WHO_2->kardial_dropdown_text_field; ?></li>
																						</ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_2->nephrologisch_dropdown) && $question_5_WHO_2->nephrologisch_dropdown) { ?>
																				<ul>Nephrologisch:
																					<?php echo $question_5_WHO_2->nephrologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_2->nephrologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 165px"><?php echo $question_5_WHO_2->nephrologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_2->neurologisch_dropdown) && $question_5_WHO_2->neurologisch_dropdown) { ?>
																				<ul>Neurologisch/psychotrop:
																					<?php echo $question_5_WHO_2->neurologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_2->neurologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 241px"><?php echo $question_5_WHO_2->neurologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			</li>
																			</ul>
																		<?php } ?>
																		</li>
																	</ul>
															</li>
															<?php } ?>
																
														<?php } ?>
														
														<?php if (isset($question_5_WHO_3) && $question_5_WHO_3) { ?>
															
															<li>WHO Stufe 3
																<ul>
																	<?php if (isset($question_5_WHO_3->option_name) && $question_5_WHO_3->option_name) { ?>
															
																		<li><?php echo $question_5_WHO_3->option_name; ?>

																			<ul><li><strong>Medikament:</strong>
																				<?php if (isset($question_5_WHO_3->medikament_dropdown_1) && $question_5_WHO_3->medikament_dropdown_1) { ?>
																				<ul><?php echo $question_5_WHO_3->medikament_dropdown_1; ?>
																				<?php if($question_5_WHO_3->medikament_dropdown_1 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_3->medikament_dropdown_1_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																				<?php } ?>
																				<?php if (isset($question_5_WHO_3->medikament_dropdown_2) && $question_5_WHO_3->medikament_dropdown_2) { ?>
																					<ul><?php echo $question_5_WHO_3->medikament_dropdown_2; ?>
																						<?php if($question_5_WHO_3->medikament_dropdown_2 == 'Freifeld') { ?>
																							<ul><?php echo $question_5_WHO_3->medikament_dropdown_2_text_field; ?></ul>
																						<?php } ?>
																					</ul>
																						
																				<?php } ?>
																				<?php if (isset($question_5_WHO_3->medikament_dropdown_3) && $question_5_WHO_3->medikament_dropdown_3) { ?>
																					<ul><?php echo $question_5_WHO_3->medikament_dropdown_3; ?>
																				<?php if($question_5_WHO_3->medikament_dropdown_3 == 'Freifeld') { ?>
																						<ul><?php echo $question_5_WHO_3->medikament_dropdown_3_text_field; ?></ul>
																					<?php } ?>
																					</ul>
																						
																				<?php } ?>
																			</li></ul>
																			<?php if (isset($question_5_WHO_3) && $question_5_WHO_3->therapieerfolg) { ?>
																			<ul><li>Therapieerfolg:
																				<?php echo ($question_5_WHO_3->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																			</li></ul>
																			<?php } ?>
																			
																			<?php if ($question_5_WHO_3->gastrointestinal_dropdown || $question_5_WHO_3->kardial_dropdown || $question_5_WHO_3->nephrologisch_dropdown || $question_5_WHO_3->neurologisch_dropdown) { ?>
																				
																			<ul><li>
																					
																				Nebenwirkungen:
																	
																			<?php if (isset($question_5_WHO_3->gastrointestinal_dropdown) && $question_5_WHO_3->gastrointestinal_dropdown) { ?>
																			<ul>Gastrointestinal:
																				<?php echo $question_5_WHO_3->gastrointestinal_dropdown ?>
																				
																				<?php if ($question_5_WHO_3->gastrointestinal_dropdown == 'Freifeld') { ?>
																					<ul style="padding-left: 174px"><?php echo $question_5_WHO_3->gastrointestinal_dropdown_text_field; ?></ul>
																				<?php } ?>
																			</ul>
																			<?php } ?>
																				
																			<?php if (isset($question_5_WHO_3->kardial_dropdown) && $question_5_WHO_3->kardial_dropdown) { ?>
																				<ul>Kardial:
																					<?php echo $question_5_WHO_3->kardial_dropdown ?>
																					
																					<?php if ($question_5_WHO_3->kardial_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 115px">
																							<li><?php echo $question_5_WHO_3->kardial_dropdown_text_field; ?></li>
																						</ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_3->nephrologisch_dropdown) && $question_5_WHO_3->nephrologisch_dropdown) { ?>
																				<ul>Nephrologisch:
																					<?php echo $question_5_WHO_3->nephrologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_3->nephrologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 165px"><?php echo $question_5_WHO_3->nephrologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			<?php if (isset($question_5_WHO_3->neurologisch_dropdown) && $question_5_WHO_3->neurologisch_dropdown) { ?>
																				<ul>Neurologisch/psychotrop:
																					<?php echo $question_5_WHO_3->neurologisch_dropdown ?>
																					
																					<?php if ($question_5_WHO_3->neurologisch_dropdown == 'Freifeld') { ?>
																						<ul style="padding-left: 241px"><?php echo $question_5_WHO_3->neurologisch_dropdown_text_field; ?></ul>
																					<?php } ?>
																				</ul>
																			<?php } ?>
																			</li>
																			</ul>
																		<?php } ?>
																		</li>
															
															</li>
														<?php } ?>
																	
																</ul>
															</li>
														<?php } ?>
														
														<?php if (isset($question_5_Spasmolytika) && $question_5_Spasmolytika) { ?>
														
															<li>Spasmolytika

																<ul><li><strong>Medikament:</strong>
																	<?php if (isset($question_5_Spasmolytika->medikament_dropdown_1) && $question_5_Spasmolytika->medikament_dropdown_1) { ?>
																	<ul><?php echo $question_5_Spasmolytika->medikament_dropdown_1; ?>
																	<?php if($question_5_Spasmolytika->medikament_dropdown_1 == 'Freifeld') { ?>
																			<ul><?php echo $question_5_Spasmolytika->medikament_dropdown_1_text_field; ?></ul>
																		<?php } ?>
																	</ul>
																	<?php } ?>
																	<?php if (isset($question_5_Spasmolytika->medikament_dropdown_2) && $question_5_Spasmolytika->medikament_dropdown_2) { ?>
																		<ul><?php echo $question_5_Spasmolytika->medikament_dropdown_2; ?>
																			<?php if($question_5_Spasmolytika->medikament_dropdown_2 == 'Freifeld') { ?>
																				<ul><?php echo $question_5_Spasmolytika->medikament_dropdown_2_text_field; ?></ul>
																			<?php } ?>
																		</ul>
																			
																	<?php } ?>
																	<?php if (isset($question_5_Spasmolytika->medikament_dropdown_3) && $question_5_Spasmolytika->medikament_dropdown_3) { ?>
																		<ul><?php echo $question_5_Spasmolytika->medikament_dropdown_3; ?>
																	<?php if($question_5_Spasmolytika->medikament_dropdown_3 == 'Freifeld') { ?>
																			<ul><?php echo $question_5_Spasmolytika->medikament_dropdown_3_text_field; ?></ul>
																		<?php } ?>
																		</ul>
																			
																	<?php } ?>
																</li></ul>
																<?php if (isset($question_5_Spasmolytika) && $question_5_Spasmolytika->therapieerfolg) { ?>
																<ul><li>Therapieerfolg:
																	<?php echo ($question_5_Spasmolytika->therapieerfolg == 'keine ausreichende Schmerzreduktion')?'keine ausreichende Schmerzreduktion':'erfolglos'; ?>
																</li></ul>
																<?php } ?>
																
																<?php if ($question_5_Spasmolytika->gastrointestinal_dropdown || $question_5_Spasmolytika->kardial_dropdown || $question_5_Spasmolytika->nephrologisch_dropdown || $question_5_Spasmolytika->neurologisch_dropdown) { ?>
																	
																<ul><li>
																					
																		Nebenwirkungen:
															
																<?php if (isset($question_5_Spasmolytika->gastrointestinal_dropdown) && $question_5_Spasmolytika->gastrointestinal_dropdown) { ?>
																<ul>Gastrointestinal:
																	<?php echo $question_5_Spasmolytika->gastrointestinal_dropdown ?>
																	
																	<?php if ($question_5_Spasmolytika->gastrointestinal_dropdown == 'Freifeld') { ?>
																		<ul style="padding-left: 174px"><?php echo $question_5_Spasmolytika->gastrointestinal_dropdown_text_field; ?></ul>
																	<?php } ?>
																</ul>
																<?php } ?>
																	
																<?php if (isset($question_5_Spasmolytika->kardial_dropdown) && $question_5_Spasmolytika->kardial_dropdown) { ?>
																	<ul>Kardial:
																		<?php echo $question_5_Spasmolytika->kardial_dropdown ?>
																		
																		<?php if ($question_5_Spasmolytika->kardial_dropdown == 'Freifeld') { ?>
																			<ul style="padding-left: 115px">
																				<li><?php echo $question_5_Spasmolytika->kardial_dropdown_text_field; ?></li>
																			</ul>
																		<?php } ?>
																	</ul>
																<?php } ?>
																<?php if (isset($question_5_Spasmolytika->nephrologisch_dropdown) && $question_5_Spasmolytika->nephrologisch_dropdown) { ?>
																	<ul>Nephrologisch:
																		<?php echo $question_5_Spasmolytika->nephrologisch_dropdown ?>
																		
																		<?php if ($question_5_Spasmolytika->nephrologisch_dropdown == 'Freifeld') { ?>
																			<ul style="padding-left: 165px"><?php echo $question_5_Spasmolytika->nephrologisch_dropdown_text_field; ?></ul>
																		<?php } ?>
																	</ul>
																<?php } ?>
																<?php if (isset($question_5_Spasmolytika->neurologisch_dropdown) && $question_5_Spasmolytika->neurologisch_dropdown) { ?>
																	<ul>Neurologisch/psychotrop:
																		<?php echo $question_5_Spasmolytika->neurologisch_dropdown ?>
																		
																		<?php if ($question_5_Spasmolytika->neurologisch_dropdown == 'Freifeld') { ?>
																			<ul style="padding-left: 241px"><?php echo $question_5_Spasmolytika->neurologisch_dropdown_text_field; ?></ul>
																		<?php } ?>
																	</ul>
																<?php } ?>
																</li>
																</ul>
															<?php } ?>
															</li>
															</li>
														<?php } ?>
														

													</ul>
												</td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<!-- <tr>
												<td style="color: #050000;">
													9. Welche bisherige Therapie ist bei der Erkrankung unter 3. mit welchem Erfolg durchgeführt worden?
													<br><strong style="color: red">[Arztfragebogen 4/15 bis 13/15]</strong>
												</td>
											</tr> -->
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<mark>
													<?php if (!$is_exists_patient->grad) { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																8.
															<?php } else { ?>
																9.
															<?php } ?>
															
														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																9.
															<?php } else { ?>
																10.
															<?php } ?>
															
														<?php } ?>
														
													<?php } else { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																9.
															<?php } else { ?>
																10.
															<?php } ?>

														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																10.
															<?php } else { ?>
																11.
															<?php } ?>
															
														<?php } ?>
														
													<?php } ?>
													Warum stehen allgemein anerkannte, dem medizinischen Standard entsprechende alternative Behandlungsoptionen nicht zur Verfügung?</mark>
													<br>
													<br>
													Die allgemein anerkannten Standards zur Behandlung der unter 3. genannten Indikation sind ausgeschöpft.
												</td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<mark>
													<?php if (!$is_exists_patient->grad) { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																9.
															<?php } else { ?>
																10.
															<?php } ?>
															
														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																10.
															<?php } else { ?>
																11.
															<?php } ?>
															
														<?php } ?>
														
													<?php } else { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																10.
															<?php } else { ?>
																11.
															<?php } ?>
														
														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																11.
															<?php } else { ?>
																12.
															<?php } ?>
															
														<?php } ?>
														
													<?php } ?>
													Soll die bisherige Medikation parallel zur Therapie mit cannabisbasierten Medikamenten fortgeführt werden?</mark>
													<br>
													<br>
													Während der Auftitrationsphase von dem unter 2. benanntes 
													Medikament soll die bisherige Medikation beibehalten werden. 
													Nach der Auftitrationsphase soll die bisherige Medikation 
													reduziert werden.
												</td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
												 <mark><?php if (!$is_exists_patient->grad) { ?>
												 	<?php if (!$begleiterkrankungen) { ?>
												 		<?php if (!$begleitmedikation) { ?>
															10.
														<?php } else { ?>
															11.
														<?php } ?>
														
													<?php } else { ?>
														<?php if (!$begleitmedikation) { ?>
															11.
														<?php } else { ?>
															12.
														<?php } ?>
														 
													<?php } ?>
														
												<?php } else { ?>
													<?php if (!$begleiterkrankungen) { ?>
														<?php if (!$begleitmedikation) { ?>
															11.
														<?php } else { ?>
															12.
														<?php } ?>
														
													<?php } else { ?>
														<?php if (!$begleitmedikation) { ?>
															12.
														<?php } else { ?>
															13.
														<?php } ?>

													<?php } ?>
														
												<?php } ?>
													Bitte benennen Sie Literatur, aus der hervorgeht, dass 
													eine nicht ganz entfernt liegende Aussicht auf eine spürbare 
													positive Einwirkung auf den Krankheitsverlauf oder auf 
													schwerwiegende Symptome besteht.</mark>
													<br>
													<!-- <br><strong style="color: red">[siehe Excel Liste Indikation_Literatur]</strong> -->

													<?php if ($books) { ?>
														<ul>
															<?php foreach ($books as $key => $value) { ?>
																<li><strong><?php echo $value->year; ?>:</strong> <?php echo $value->book_name; ?></li>
															<?php } ?>
														</ul>
													<?php } ?>
												</td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<mark>
													<?php if (!$is_exists_patient->grad) { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																11.
															<?php } else { ?>
																12.
															<?php } ?>
																
															<?php } else { ?>
																<?php if (!$begleitmedikation) { ?>
																	12.
																<?php } else { ?>
																	13.
																<?php } ?>
																 
															<?php } ?>
														
													<?php } else { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																12.
															<?php } else { ?>
																13.
															<?php } ?>

														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																13.
															<?php } else { ?>
																14.
															<?php } ?>

														<?php } ?>
														
													<?php } ?>
													Ist eine Kontrakindikation bei der Verwendung von CannaXan während der Therapie zu erwarten?</mark>
													<br><strong style="color: red"></strong>
													<ul>
														<?php if (isset($question_6->kontraindikation) && $question_6->kontraindikation == 'Yes') { ?>
															
														<li>ja
															<ul>
																<?php if (isset($question_6) && $question_6->relative_kontraindikation == 'Relative Kontraindikation') { ?>
																	<li>relative Kontraindikation</li>
																	
																	<ul>
																		<?php if (isset($question_6) && $question_6->allergien_gegen == '1') { ?>
																			<li>Allergien gegen Cannabinoide oder einen der genannten sonstigen Bestandteile von CannaXan</li>
																		<?php } ?>
																		<?php if (isset($question_6) && $question_6->nierenerkrankung == '2') { ?>
																			<li>schwere Leber- und/oder Nierenerkrankung</li>
																		<?php } ?>
																	</ul>
																<?php } ?>
																
																<?php if (isset($question_6->absolute_kontraindikation) && $question_6->absolute_kontraindikation == 'Absolute Kontraindikation') { ?>
																	<li>absolute Kontraindikation
																		<ul>
																			<?php if (isset($question_6->schwere_personlichkeitsstorung) && $question_6->schwere_personlichkeitsstorung == '1') { ?>
																			<li>schwere Persönlichkeitsstörung (z.B. Schizophrenie, Psychose)</li>
																			<?php } ?>
																			<?php if (isset($question_6->schwere_kardiovaskulare) && $question_6->schwere_kardiovaskulare == '2') { ?>
																			<li>schwere kardiovaskuläre Erkrankung </li>
																			<?php } ?>
																			<?php if (isset($question_6->kinder) && $question_6->kinder == '3') { ?>
																			<li>Kinder und Jugendliche unter 18 Jahren </li>
																		<?php } ?>
																		<?php if (isset($question_6->schwangerschaft) && $question_6->schwangerschaft == '4') { ?>
																			<li>Schwangerschaft und Stillzeit </li>
																		<?php } ?>
																		</ul>
																	</li>
																<?php } ?>
															</ul>
														</li>
														<?php } else { ?>
														
															<li>nein</li>
														<?php } ?>
														

													</ul>
												</td>
											</tr>
											<tr>
												<td ></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<mark>
													<?php if (!$is_exists_patient->grad) { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																12.
															<?php } else { ?>
																13.
															<?php } ?>
														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																13.
															<?php } else { ?>
																14.
															<?php } ?>
																
														<?php } ?>
														
													<?php } else { ?>
														<?php if (!$begleiterkrankungen) { ?>
															<?php if (!$begleitmedikation) { ?>
																13.
															<?php } else { ?>
																14.
														<?php } ?>
														<?php } else { ?>
															<?php if (!$begleitmedikation) { ?>
																14.
															<?php } else { ?>
																15.
															<?php } ?>
														<?php } ?>
														
													<?php } ?>
													Erfolgt die Therapie im Rahmen einer klinischen Prüfung?</mark>
													<br>
													<ul>
														<li><?php echo ($question_7 && $question_7->klinischen_prufung == 'Yes')?'ja':'nein'; ?></li>
													</ul>
												</td>
											</tr>
											<tr>
												<td height="25"></td>
											</tr>
											<tr>
												<td>
													<table width="400" cellpadding="0" cellspacing="0">
														<tr>
															<td style="color: #050000;">Ort: <?php echo $ort; ?></td>
															<td>Datum: <?php echo $datum; ?></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td height="50"></td>
											</tr>
											<tr>
												<td style="color: #050000;">
													<!-- <strong>Unterschrift</strong><br>
													<img src="<?php echo base_url().'assets/uploads/unterschrift-images/'.$unterschrift; ?>">
													<br/> -->
													
													<?php echo $title.' '. $vorname.' '. $nachname; ?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</body>
</html>