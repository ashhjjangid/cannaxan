<div class="same-section min-height-high">
  <div class="container-fluid"> 
    <form method="post" action="<?php echo current_url().'?in='.$insured_number.'&qid='.$qid; ?>" class="form-horizontal ajax_form">
      <input type="hidden" name="arztlicher_hidden" value="0">
    <div class="same-heading mb-4">
      <h2>Arztfragebogen 3/6</h2>
    </div>
    <div class="radio-box normal-radio">
      <input type="checkbox" class="select-schwerwiegend-main" onclick="selectmainarztlicher()" name="arztlicher_einschatzung" <?php echo ($arztlicher_einschatzung == 'Yes')?'checked':''; ?> value="Yes" id="Ja">
      <label for="Ja">
      <span class="radio-cheak"></span>
      <strong class="same-font-size">Es ist eine medizinische Versorgung (ärztliche Behandlung, Arzneimitteltherapie) erforderlich, ohne die nach ärztlicher Einschätzung</strong>
      </label>
      <div class="normal-radio-inner" id="is_selected_arztlicher">
        <div class="checkbox-custom normal-radio">
          <input type="checkbox" class="select-schwerwiegend" onclick="selectarztlicher()" name="lebensbedrohliche_verschlimmerung" <?php echo ($eine_lebensbedrohliche_verschlimmerung)?'checked':''; ?> value="1" id="checkbox-1" data-name="lebensbedrohliche_verschlimmerung">
          <label for="checkbox-1">
          <span class="cheak"></span>
          eine lebensbedrohliche Verschlimmerung,
          </label>
        </div>
        <div class="checkbox-custom normal-radio">
          <input type="checkbox" class="select-schwerwiegend" onclick="selectarztlicher()" name="verminderung_der_lebenserwartu" <?php echo ($eine_verminderung_der_lebenserwartung)?'checked':''; ?> value="2" id="checkbox-2" data-name="verminderung_der_lebenserwartu">
          <label for="checkbox-2">
          <span class="cheak"></span>
          eine Verminderung der Lebenserwartung
          </label>
        </div>
        <div class="checkbox-custom normal-radio">
          <input type="checkbox" class="select-schwerwiegend" onclick="selectarztlicher()" name="dauerhafte_beeintrachtigung" <?php echo ($dauerhafte_beeintrachtigung)?'checked':''; ?> value="3" id="checkbox-3">
          <label for="checkbox-3" data-name="dauerhafte_beeintrachtigung">
          <span class="cheak"></span>
          eine dauerhafte Beeinträchtigung der gesundheitsbezogenen Lebensqualität
          </label>
        </div>
        <p class="nrml-txt">durch die zugrundeliegende schwerwiegende Erkrankung zu erwarten ist. Vergleiche Definition der schwerwiegenden chronischen Erkrankungen durch den Gemeinsamen Bundesausschuss (<a class="underline" href="https://www.g-ba.de/richtlinien/8/" target="_blank">https://www.g-ba.de/richtlinien/8/</a>). Die Schwere der Erkrankung wird nach SGB V begründet durch unkontrollierbare Schmerzen unter Ausschöpfung der Standardtherapien und schwere neuropsychiatrische Einschränkungen.</p>
      </div>
    </div>
    <div class="btn-group-custom text-right">
      <a class="btn btn-primary" href="<?php echo base_url('arztfragebogen/step-2?in='.$insured_number.'&qid='. $qid); ?>"><span><i class="fas fa-long-arrow-alt-left"></i> Zurück</span></a>
      <button type="submit" class="btn btn-primary"><span>Weiter <i class="fas fa-long-arrow-alt-right"></i></span></button>
    </div>
  </form>
  </div>
</div>
<script type="text/javascript">
    

    function selectarztlicher() {
        
        /*if ($($this).is(':checked')) {
            $('.select-schwerwiegend-main').prop('checked', true);
        } else {
            $('.select-schwerwiegend-main').prop('checked', false);
        }*/
        var lebensbedrohliche = $('input[name="lebensbedrohliche_verschlimmerung"]').is(':checked');
        var verminderung = $('input[name="verminderung_der_lebenserwartu"]').is(':checked');
        var dauerhafte = $('input[name="dauerhafte_beeintrachtigung"]').is(':checked');

        if (lebensbedrohliche || verminderung || dauerhafte) {
          $('.select-schwerwiegend-main').prop('checked', true);
        } else {
           $('.select-schwerwiegend-main').prop('checked', false);
        }    
    }

    function selectmainarztlicher() {
    	var arztlicher_einschatzung = $('input[name="arztlicher_einschatzung"]').is(':checked');

        if (!arztlicher_einschatzung) {
          
           $('.select-schwerwiegend').prop('checked', false);
        }
    }
</script>
