<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- <meta charset="utf-8"> -->
      <meta charset="UTF-8">
      <title>CannaXan</title>
      <link rel="icon" href="<?php echo $this->config->item('front_assets'); ?>/images/favicon.png" type="image">
      <!-- <link rel="icon" href="<?php //echo $this->config->item('front_assets'); ?>images/favicon.ico" type="image"> -->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>css/bootstrap.min.css"/>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>css/style.css"/>
      <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>css/media.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <!-- <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('front_assets'); ?>js/popper.min.js"> -->
      <script type="text/javascript" src="<?php echo $this->config->item('front_assets'); ?>js/popper.min.js"></script>
      <script type="text/javascript" src="<?php echo $this->config->item('front_assets'); ?>js/bootstrap.min.js"></script>
      <script src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
      <script src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
      <script src="https://unpkg.com/jquery-fusioncharts@1.1.0/dist/fusioncharts.jqueryplugin.js"></script>
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"/>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript">
      </script>


   </head>
   <body >
      <header>
         <div class="container-fluid">
            <div class="header-box">
               <div class="row align-items-center">
                   <div class="col">
                     <div class="nav-box">
                        <?php if($this->session->userdata('user_id')) { ?>
                        <?php if ($_GET && isset($_GET['in'])) { ?>
                        <ul>
                           <li>
                              <a href="<?php echo base_url('patients'); ?>" class="<?php echo (isset($header_menu) && ($header_menu == 'patients'))?'active':''?>" id="notification-slider-click">Patient</a>
                              <?php if ($_GET && isset($_GET['in'])) { ?>

                                <div class="notification-box-cover" id="notification-slider-box">
                                  <div class="event-list-box-cover">
                                   <ul>
                                      <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'anlegen'))?'active':''?>"><a href="<?php echo base_url('patients'); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'anlegen'))?'active':''?>">anlegen</a></li>
                                      <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'laden'))?'active':''?>"><a href="<?php echo base_url('patients/anlegen?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'laden'))?'active':''?>">laden</a></li>
                                      <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'begleiterkrankungen'))?'active':''?>"><a href="<?php echo base_url('patients/begleiterkrankungen?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'begleiterkrankungen'))?'active':''?>">Begleiterkrankungen</a></li>
                                   </ul>
                                  </div>
                                </div>
                              <?php } ?>
                           </li>
                           <li>
                              <a href="javascript:void(0)" class="<?php echo (isset($header_menu) && ($header_menu == 'therapie'))?'active':''?>" id="notification-therapie-slider-click">Therapie</a>
                              <?php if ($_GET && isset($_GET['in'])) { ?>
                                <div class="notification-box-cover  menu-box-width right" id="notification-therapie-slider-box">
                                  <div class="event-list-box-cover">
                                   <ul>
                                      <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'titration'))?'active':''?>"><a href="<?php echo base_url('therapie?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'titration'))?'active':''?>">Titration</a></li>

                                      <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'umstellung'))?'active':''?>"><a href="<?php echo base_url('therapie/umstellung?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'umstellung'))?'active':''?>">Umstellung von anderem Cannabis Med.</a></li>
                                      <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'folgeverordnung'))?'active':''?>"><a href="<?php echo base_url('therapie/folgeverordnung?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'folgeverordnung'))?'active':''?>" >Folgeverordnung</a></li>
                                   </ul>
                                  </div>
                                </div>
                              <?php } ?>
                           </li>
                           <li><a href="javascript:void(0)" class="<?php echo (isset($header_menu) && ($header_menu == 'process_control'))?'active':''?>" id="notification-verlaufskontrolle-slider-click">Verlaufskontrolle</a>
                            <?php if ($_GET && isset($_GET['in'])) { ?>
                              <div class="notification-box-cover menu-box-width right" id="notification-verlaufskontrolle-slider-box">
                                <div class="event-list-box-cover">
                                 <ul>
                          
                                    <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'Visitenplan'))?'active':''?>"><a href="<?php echo base_url('process_control/visitenplan?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'Visitenplan'))?'active':''?>">Navigator</a></li>
                                   
                                    <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'schmerz'))?'active':''?>"><a href="<?php echo base_url('process_control/schmerz?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'schmerz'))?'active':''?>">Schmerz</a></li>
                                   
                                    <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'lebensqualitat'))?'active':''?>"><a href="<?php echo base_url('process_control/lebensqualitat?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'lebensqualitat'))?'active':''?>">Lebensqualität</a></li>
                                 
                                    <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'schlafqualitat'))?'active':''?>"><a href="<?php echo base_url('process_control/schlafqualitat?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'schlafqualitat'))?'active':''?>">Schlafqualität</a></li>
                                    <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'laborwerte'))?'active':''?>"><a href="<?php echo base_url('process_control/index?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'laborwerte'))?'active':''?>">Laborwerte</a></li>
                                    
                                    <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'begleitmedikation'))?'active':''?>"><a href="<?php echo base_url('process_control/begleitmedikation?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'begleitmedikation'))?'active':''?>">Begleitmedikation</a></li>
                                    <li class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'nebenwirkung'))?'active':''?>"><a href="<?php echo base_url('nebenwirkung?in='.$_GET['in']); ?>" class="<?php echo (isset($header_sub_menu) && ($header_sub_menu == 'nebenwirkung'))?'active':''?>">Nebenwirkung</a></li>
                                 </ul>
                                </div>
                              </div>
                            <?php } ?>
                           </li>
                           <li class="<?php echo (isset($header_menu) && ($header_menu == 'arztfragebogen'))?'active':''?>">
                              <a href="<?php echo base_url('arztfragebogen/step-1?in='.$_GET['in']); ?>" class="<?php echo (isset($header_menu) && ($header_menu == 'arztfragebogen'))?'active':''?>" id="notification-slider-click">Arztfragebogen</a>
                              <?php if ($_GET && isset($_GET['in'])) { ?>

                                
                              <?php } ?>
                           </li>
                           <?php if ($this->session->userdata('user_id')) { ?>
                              <li><a href="<?php echo base_url('doctors/logout'); ?>">Abmelden</a></li>
                           <?php } ?>
                        </ul>
                      <?php } else { ?>
                        <ul>
                           <li><a href="<?php echo base_url('doctors/logout'); ?>">Abmelden</a></li>
                        </ul>
                      <?php } ?>
                        <?php } else { ?>
                        <ul>
                           <li class="<?php echo (isset($menu) && $menu == 'REGISTRIERUNG')?'active':''; ?>"><a class="<?php echo (isset($menu) && $menu == 'REGISTRIERUNG')?'active':''; ?>" href="<?php echo base_url('doctors/register/step-1'); ?>">Registrierung</a></li>
                           <li class="<?php echo (isset($menu) && $menu == 'ANMELDUNG')?'active':''; ?>"><a class="<?php echo (isset($menu) && $menu == 'ANMELDUNG')?'active':''; ?>" href="<?php echo base_url('doctors/login'); ?>">Anmeldung</a></li>
                        </ul>
                        <?php if (isset($menu) && $menu == 'REGISTRIERUNG') { ?>
                          
                          <label>*Eine Registrierung ist nur für Ärzte möglich</label>
                        <?php } ?>
                        <?php } ?>
                     </div>
                  </div>
                  <div class="col logo-width">
                     <div class="logo-box text-right">

                    	<?php if($this->session->userdata('user_id')) { ?>
                        
                        <a href="<?php echo base_url('patients') ?>">
                           <img src="<?php echo $this->config->item('front_assets'); ?>images/logo.png" alt="logo"/>
                        </a>
                       <?php } else { ?>
						            <a href="<?php echo base_url('home') ?>">
                           <img src="<?php echo $this->config->item('front_assets'); ?>images/logo.png" alt="logo"/>
                        </a>
                       <?php } ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>