<section class="same-section same-blog">
   <div class="container-fluid">
      <div class="same-heading ">
         <h2 class="text-left">ÄRZTLICHER THERAPIEPLAN FÜR UMSTELLUNG</h2>
      </div>
      <form action="<?php echo base_url('therapie/add_typumstelung?in='.$insured_number); ?>" method="post" class="ajax_form" id="umstellung_form">
        <div class="min-height-high">
           <div class="row">
              <div class="col-md-12">
                 <div class="form-group">
                    <div class="row">
                       <div class="col-sm-4">
                          <label>Bisheriges Cannabis Medikament</label>
                       </div>
                       <div class="col-sm-6">

                        <div class="dropdown select-box red-select" id="cann-box">
                         <button id="name_cannabis" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $name_cannabis_text; ?>
                           
                           <span><i class="fas fa-chevron-down"></i></span>
                         </button>
                         <ul class="dropdown-menu" id="name_cannabis" aria-labelledby="name_cannabis">
                          <?php if ($cannabis_medikament) { ?>

                             <?php foreach ($cannabis_medikament as $key => $value) { 

                                   $selected = '';

                                   if ($is_exist_patient && $value->id == $is_exist_patient->name_cannabis) {
                                      $selected = 'selected';
                                   }

                                ?>
                                <li value="<?php echo $value->id; ?>" <?php echo $selected; ?>><?php echo $value->name; ?> </li>
                             <?php } ?>
                          <?php } ?>
                          
                         </ul>
                       </div>
                       <input type="hidden" name="cannabis_medikament_id" id="name_cannabis" class="name_cannabis-field" value="<?php echo (isset($cannabis_medikament_id) && !empty($cannabis_medikament_id) && $cannabis_medikament_id)?$cannabis_medikament_id:''; ?>">
                          <!-- <div class="select-box">
                            <select class="form-control" name="cannabis_medikament_id" id="typ_umstelung" onchange="getTypumstelung()">

                                <?php if ($cannabis_medikament) { ?>

                                   <?php foreach ($cannabis_medikament as $key => $value) { 

                                         $selected = '';  

                                         if ($patientTherapyPlanUmstellung && $value->id == $patientTherapyPlanUmstellung->cannabis_medikament_id) {
                                            $selected = 'selected';
                                         }

                                      ?>
                                      <option value="<?php echo $value->id; ?>" <?php echo $selected; ?>><?php echo $value->name; ?> </option>
                                   <?php } ?>
                                <?php } ?>
                             </select>
                             <span><i class="fas fa-chevron-down"></i></span>
                          </div> -->
                       </div>
                    </div>
                 </div>
              </div>
              <div class="col-md-12">
                 <div class="row">
                    <div class="col-md-4">
                       <div class="form-group">
                          <div class="row">
                             <div class="col-sm-6">
                                <label>Dosiseinheit:</label>
                             </div>
                             <div class="col-sm-6">
                                <div class="select-box">
                                  <input type="text" class="form-control text-center" id="dosiseinheit" name="dosiseinheit" value="<?php echo $dosiseinheit; ?>" placeholder="Dosiseinheit" readonly>
                                  <!-- <select class="form-control" name="dosiseinheit">
                                      <option value="g" <?php echo ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->dosiseinheit == "g")?'selected':''; ?>>g</option>
                                      <option value="ml" <?php echo ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->dosiseinheit == "ml")?'selected':''; ?>>ml</option>
                                      <option value="Kapseln" <?php echo ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->dosiseinheit == "Kapseln")?'selected':''; ?>>Kapseln</option>
                                      <option value="Sprühstöße" <?php echo ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->dosiseinheit == "Sprühstöße")?'selected':''; ?>>Sprühstöße</option>
                                      <option value="Tropfen" <?php echo ($patientTherapyPlanUmstellung && $patientTherapyPlanUmstellung->dosiseinheit == "Tropfen")?'selected':''; ?>>Tropfen</option>
                                   </select>
                                   <span><i class="fas fa-chevron-down"></i></span> -->
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-5">
                       <div class="form-group">
                          <div class="row">
                             <div class="col-sm-7">
                                <label>Tagesdosis bisheriges </label>
                             </div>
                             <div class="col-sm-5">
                                <input type="text" class="form-control text-center red-input" name="tagesdosis_bisheriges" id="tagesdosis_bisheriges" onkeyup="calculatevalues()" value="<?php echo str_replace('.', ',', $tagesdosis_bisheriges); ?>" placeholder="Tagesdosis bisheriges">
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-3">
                       <div class="form-group">
                          <div class="row">
                             <div class="col-sm-8">
                                <label>Tagesdosis: <span>(mg THC)</span></label>
                             </div>
                             <div class="col-sm-4">
                                <input type="text" id="tagesdosis" name="tagesdosis" value="<?php echo str_replace('.', ',', round($tagesdosis,1)); ?>" class="form-control text-center" placeholder="Tagesdosis (mg THC)" readonly>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-5">
                       <div class="form-group">
                          <div class="row">    
                             <div class="col-sm-7">
                                <label>Rezepturarzneimittel:</label>
                             </div>
                             <div class="col-sm-5">
                                <input type="text" name="rezepturarzneimittel" value="CannaXan-THC" class="form-control text-center" placeholder="Rezepturarzneimittel" readonly>
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-4">
                       <div class="form-group">
                          <div class="row">
                             <div class="col-sm-4">
                                <label>Wirkstoff:</label>
                             </div>
                             <div class="col-sm-7">
                                <input type="text" name="wirkstoff" value="CannaXan-701-1.1" class="form-control " placeholder="Wirkstoff" readonly>
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-3">
                       <div class="form-group">
                          <div class="row">
                             <div class="col-sm-8">
                                <label>Anzahl Therapietage</label>
                             </div>
                             <div class="col-sm-4">
                                <input type="text" name="anzahl_therapietage" value="<?php echo ($patientTherapyPlanUmstellung)?$patientTherapyPlanUmstellung->anzahl_therapietage:''; ?>" class="form-control text-center red-input" placeholder="Anzahl Therapietage">
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-12">
                      <p class="comman-heading"><i class="text-red">Eingabewerte in rot.</i> <strong>ALLE SCHWARZEN UND <span>BLAUEN</span> WERTE WERDEN BERECHNET</strong></p>
                       <div class="umstelung-images">
                          <div class="table-box-form umstelung-content-section"></div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
         <div class="row">
            <div class="col-md-12 text-right">
               <div class="btn-mar-top">
                 <button class="btn btn-primary" type="submit"><span>Speichern &amp; weiter</span></button>
               </div>
            </div>
         </div>
      </form>
   </div>
</section>

<script type="text/javascript">
   $(function(){
      calculatevalues();
      //getTypumstelung();
   })

   function getTypumstelung()
   {
      var id = $('.name_cannabis-field').val();

      if (id == "1") {
         $('#dosiseinheit').val("Sprühstöße");
      } else if (id == "2") {
         $('#dosiseinheit').val("Tropfen");
      } else if (id == "3") {
         $('#dosiseinheit').val("g");
      } else if (id == "4") {
         $('#dosiseinheit').val("mL");
      } else if (id == "5") {
         $('#dosiseinheit').val("mL");
      } else if (id == "6") {
         $('#dosiseinheit').val("mL");
      } else if (id == "7") {
         $('#dosiseinheit').val("Tropfen");
      } else if (id == "8") {
         $('#dosiseinheit').val("Tropfen");
      } else if (id == "9") {
         $('#dosiseinheit').val("mL");
      }

      var url = "<?php echo base_url('therapie/typumstelung?in='.$insured_number); ?>";
      var tagesdosis = $('#tagesdosis').val();
      // alert(tagesdosis); 

      if (tagesdosis) {
        
        $.ajax({
           url: url,
           type: 'post',
           data: {id:id,tagesdosis:tagesdosis},
           dataType: 'json',
           success: function (data) {

              if (data.success) {
                 $('.umstelung-content-section').html(data.html);
              } else {
                 return false;
              }
           }
        });
      } else {
        return false;
      }

      
   }

   function keyUpMorgens($this) 
   {
    var field_value = $($this).val();
    /*if(field_value.match(/\./g)){      

    } 
    else {
      var field_value = field_value.replace(/\./g, ',');
    }*/
    field_value = field_value.replace(',', '.');
    var field_key = $($this).attr('data-key');
    var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
    var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
    
    if (field_value.trim()) {
      var morgens_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
      var morgens_anzahl_sps_text = morgens_anzahl_sps.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > .text-field').text(morgens_anzahl_sps_text);
      $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > input').val(morgens_anzahl_sps);
      row_sum(field_key);
    } 
   }

   function keyUpMittags($this) 
   {
    var field_value = $($this).val();
    /*if(field_value.match(/\./g)){      

    } 
    else {
      var field_value = field_value.replace(/\./g, ',');
    }*/
    field_value = field_value.replace(',', '.');
    var field_key = $($this).attr('data-key');
    var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
    var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
    
    if (field_value.trim()) {
      var mittags_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
      var mittags_anzahl_sps_text = mittags_anzahl_sps.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > .text-field').text(mittags_anzahl_sps_text);
      $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > input').val(mittags_anzahl_sps);
      row_sum(field_key);
    } 
   }

   function keyUpAbends($this) 
   {
    var field_value = $($this).val();
    /*if(field_value.match(/\./g)){      

    } 
    else {
      var field_value = field_value.replace(/\./g, ',');
    }*/
    field_value = field_value.replace(',', '.');
    var field_key = $($this).attr('data-key');
    var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
    var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
    
    if (field_value.trim()) {
      var abends_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
      var abends_anzahl_sps_text = abends_anzahl_sps.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > .text-field').text(abends_anzahl_sps_text);
      $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > input').val(abends_anzahl_sps);
     row_sum(field_key);
    } 
   }

   function keyUpNachts($this) 
   {
    var field_value = $($this).val();
    /*if(field_value.match(/\./g)){      

    } 
    else {
      var field_value = field_value.replace(/\./g, ',');
    }*/
    field_value = field_value.replace(',', '.');
    var field_key = $($this).attr('data-key');
    var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
    var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
    
    if (field_value.trim()) {
      var nachts_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
      var nachts_anzahl_sps_text = nachts_anzahl_sps.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > .text-field').text(nachts_anzahl_sps_text);
      $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > input').val(nachts_anzahl_sps);
      row_sum(field_key);
    } 
  }


   function row_sum(field_key) 
   {
      var morgens_anzahl_sps = parseFloat($('.row_'+field_key).find('.morgens_anzahl_sps > input').val());
      var mittags_anzahl_sps = parseFloat($('.row_'+field_key).find('.mittags_anzahl_sps > input').val());
      var abends_anzahl_sps = parseFloat($('.row_'+field_key).find('.abends_anzahl_sps > input').val());
      var nachts_anzahl_sps = parseFloat($('.row_'+field_key).find('.nachts_anzahl_sps > input').val());
      var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
      var total = ((parseFloat(morgens_anzahl_sps) * parseFloat(menge_thc_sps_mg)) + (parseFloat(mittags_anzahl_sps) * parseFloat(menge_thc_sps_mg)) + (parseFloat(abends_anzahl_sps) * parseFloat(menge_thc_sps_mg)) +(parseFloat(nachts_anzahl_sps) * parseFloat(menge_thc_sps_mg))).toFixed(1);
      var total_text = total.replace(/\./g, ',');
      $('.row_'+field_key).find('.transferfaktor > input').val(total);
      $('.row_'+field_key).find('.transferfaktor > .text-field').text(total_text);
      var first_row = parseFloat($('.row_'+field_key).find('.first-row >input').val());
      var eintitration = (total / first_row).toFixed(1);
      var eintitration_text = eintitration.replace(/\./g, ',');
      var id = $('#typ_umstelung option:selected').val();

      if (id != 3) {
         $('.row_'+field_key).find('.eintitration > input').val(eintitration);
         $('.row_'+field_key).find('.eintitration > .text-field').text(eintitration_text);
      }
   }

   function changeMedikament() {
      var cannabis_medikament = $('.cannabis_medikament').val();
      if (cannabis_medikament == "1") {
         $('#dosiseinheit_first').val("Sprühstöße");
      } else if (cannabis_medikament == "2") {
         $('#dosiseinheit_first').val("Tropfen");
      } else if (cannabis_medikament == "3") {
         $('#dosiseinheit_first').val("g");
      } else if (cannabis_medikament == "4") {
         $('#dosiseinheit_first').val("mL");
      } else if (cannabis_medikament == "5") {
         $('#dosiseinheit_first').val("mL");
      } else if (cannabis_medikament == "6") {
         $('#dosiseinheit_first').val("mL");
      } else if (cannabis_medikament == "7") {
         $('#dosiseinheit_first').val("Tropfen");
      } else if (cannabis_medikament == "8") {
         $('#dosiseinheit_first').val("Tropfen");
      } else if (cannabis_medikament == "9") {
         $('#dosiseinheit_first').val("mL");
      }
   }

   function calculatevalues() {
      var id = $('.name_cannabis-field').val();
      if (id == "1") {
         var tagesdosis_first = $('#tagesdosis_bisheriges').val().replace(/\,/g, '.');
         //alert(tagesdosis_first);
         var tagesdosis_first_value = 2.7*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis').val(tagesdosis_first_round_off);
         getTypumstelung();
      } else if (id == "2") {
         var tagesdosis_first = $('#tagesdosis_bisheriges').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 0.83*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis').val(tagesdosis_first_round_off);
         getTypumstelung();
         
      } else if (id == "3") {
         var tagesdosis_first = $('#tagesdosis_bisheriges').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 0.2*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis').val(tagesdosis_first_round_off);
         getTypumstelung();
      } else if (id == "4") {
         var tagesdosis_first = $('#tagesdosis_bisheriges').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 25*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis').val(tagesdosis_first_round_off);
         getTypumstelung();
      } else if (id == "5") {
         var tagesdosis_first = $('#tagesdosis_bisheriges').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 10*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis').val(tagesdosis_first_round_off);
         getTypumstelung();
      } else if (id == "6") {
         var tagesdosis_first = $('#tagesdosis_bisheriges').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 5*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis').val(tagesdosis_first_round_off);
         getTypumstelung();
      } else if (id == "7") {
         var tagesdosis_first = $('#tagesdosis_bisheriges').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 0.6*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis').val(tagesdosis_first_round_off);
         getTypumstelung();
      } else if (id == "8") {
         var tagesdosis_first = $('#tagesdosis_bisheriges').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 0.6*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis').val(tagesdosis_first_round_off);
         getTypumstelung();
      } else if (id == "9") {
         var tagesdosis_first = $('#tagesdosis_bisheriges').val().replace(/\,/g, '.');
         var tagesdosis_first_value = 50*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(1).replace(/\./g, ',');
         //alert(tagesdosis_first_value);
         $('#tagesdosis').val(tagesdosis_first_round_off);
         getTypumstelung();
      }
   }

   $('#cann-box li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).val();
      //alert(getValue);
     $('#name_cannabis').text(getText);
     $('.name_cannabis-field').val(getValue);
     $('#cann-box').addClass('not-red');
     calculatevalues();

   });

</script>