<section class="same-section rezepturarzneimittel-section">
   <div class="container-fluid">
      <div class="same-heading ">
        <div class="row align-items-center">
          <div class="col-md-7">
              <h2 class="text-left">ÄRZTLICHER THERAPIEPLAN</h2>
          </div>
        </div>
      </div>
      <form action="<?php echo base_url('therapie/add_therapie?in='.$insured_number); ?>" class="ajax_form" id="add_therapie_form" method="post">
        <div class="row ">
          <div class="col-md-12">
            <div class="form-group">
              <div class="row">
                <div class="col-sm-3">
                  <label class="blod-text">Hauptindikation:</label>
                </div>
                <div class="col-sm-9">
                  <?php if ($icd_code_zuordnung) { ?>

                    <?php foreach ($icd_code_zuordnung as $key => $value) { 
                         $selected = '';
                         //pr($is_exist_patient); die;
                        if ($is_exist_patient && ($is_exist_patient->hauptindikation == $value->id)) {
                            $selected = 'selected'; ?>
                            <input type="text" name="hauptindikation" class="form-control" placeholder="Hauptindikation" value="<?php echo $value->indikation; ?>" readonly>
                       <?php } }
                    ?>
                        <?php  } ?>
                  <!-- <div class="select-box">
                    <select class="form-control" name="hauptindikation" id="hauptindikation">
              
                    <?php if ($icd_code_zuordnung) { ?>

                       <?php foreach ($icd_code_zuordnung as $key => $value) { 
                             $selected = '';

                             if ($is_exist_patient && ($is_exist_patient->hauptindikation == $value->id)) {
                                $selected = 'selected';
                             }
                          ?>
                          <option value="<?php echo $value->id; ?>" data-icd="<?php echo $value->icd_10; ?>" <?php echo $selected; ?>><?php echo $value->indikation; ?></option>
                       <?php } ?>
                    <?php } ?>
                 </select>
                    <span><i class="fas fa-chevron-down"></i></span>
                  </div>
                </div> -->
              </div>
            </div>
            </div>
           <div class="col-md-3">
           </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-6">
                     <label>Rezepturarzneimittel:</label>
                  </div>
                  <div class="col-sm-6">
                     <input type="text" name="rezepturarzneimittel" class="form-control" placeholder="Rezepturarzneimittel:" value="CannaXan-THC" readonly="">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-6">
                     <label>Wirkstoff:</label>
                  </div>
                  <div class="col-sm-6">
                     <input type="text" name="wirkstoff" class="form-control" value="CannaXan-701-1.1" readonly="">
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-6">
                     <label class="dosistyp">Dosistyp
                       <span class="dropmenu" onmouseenter="showdosistypdetails()">?</span>
                        <div class="dropmenu-box">
                          <div class="drop-heading">
                            <h2>Erläuterung der Dosistypen</h2>
                            <p>Für Eintitrierung oder Umstellung auf CannaXan-THC <!--  -->(Wirkstoff CannaXan 701-1.1)</p>
                          </div>
                          <h3>Dosistyp 1:</h3>
                          <p>sehr langsame Eintitrierung für einen Cannabis naiven Patienten</p>
                          <h3>Dosistyp 2:</h3>
                          <p>schnellere Eintitrierung für einen Patienten, der sporadische Cannabis Erfahrungen hat</p>
                          <h3>Typ Umstellung:</h3>
                          <p> Sofortige Umstellung von Patienten von Blüten (geraucht, inhaliert), Dronabinol, Sativex oder anderen Cannabis Extrakten wie z.B. Tilray auf CannaXan-THC</p>
                        </div>
                     </label>
                  </div>
                  <div class="col-sm-6">
                    <div class="dropdown select-box red-select dropdown-select-bold" id="select_dosistyp_dropdown_box">
                     <button id="dosistyp_btn" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $dosistyp; ?>
                       
                       <span><i class="fas fa-chevron-down"></i></span>
                     </button>
                     <ul class="dropdown-menu" id="select-dosistype" aria-labelledby="dosistyp_btn">

                       
                      <li value="dosistyp_1" data-toggle="tooltip" data-placement="bottom">Dosistyp 1</li>
                       <li value="dosistyp_2" data-toggle="tooltip" data-placement="bottom">Dosistyp 2</li>
                     </ul>
                   </div>
                   <input type="hidden" name="dosistyps" id="select-dosis-type" class="dosistype-field" value="<?php echo ($arztlicher_therapieplan && $arztlicher_therapieplan->dosistyp)?$arztlicher_therapieplan->dosistyp:'dosistyp_1'; ?>">
                     <!-- <div class="select-box">
                    <select class="form-control dosistyp-field red-select" name="dosistyps" onchange="getDosistyp()">
                       <option value="dosistyp_1" <?php echo (($arztlicher_therapieplan && $arztlicher_therapieplan->dosistyp == 'dosistyp_1')?'selected':'');?> >Dosistyp 1</option>
                       <option value="dosistyp_2" <?php echo (($arztlicher_therapieplan && $arztlicher_therapieplan->dosistyp == 'dosistyp_2')?'selected':'');?> >Dosistyp 2</option>
                       
                    </select>
                    <span><i class="fas fa-chevron-down"></i></span>
                  </div> -->
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-6">
                     <label>THC Konzentration [<span>mg/mL</span>]</label>
                  </div>
                  <div class="col-sm-6">
                     <input type="text" name="konzentration_thc_mg_ml" class="form-control text-center" placeholder="<?php echo round($zuordnung_sps->konzentration_thc_mg_ml,1);?>" value="<?php echo str_replace('.', ',', round($zuordnung_sps->konzentration_thc_mg_ml,1)) ;?>" readonly>
                  </div>
               </div>
            </div>
          </div>
          <div class="col-md-3 tabel-1small">
            <div class="text-rezept">
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-rezept-event gray-bx">
                      <div class="heading-right">
                        <span>Angaben für BtM Rezept:</span>
                      </div>
                      <ul>
                         <li>CannaXan 701-1.1; <span>2,2 <i class="font-style-none">mg/mL</i> THC</span></li>
                         <li>insgesamt:<span><span class="Anzahl-20mL-Flaschen">0</span> x 20 <i class="font-style-none">mL</i> <span class="insgesamt-value"></span> <i class="font-style-none">mg</i> THC</span></li>
                      </ul>
                          <span class="dosierung"><!-- Dosierung siehe Therapieplan -->Dosierung gemäß schriftlicher Anweisung</span>
                       </div>
                    </div>
                    <div class="col-md-12 mt-3">
                       <div class="text-rezept-event gray-bx">
                        <div class="heading-right">
                          <span>Zusammenfassung:</span>
                        </div>
                          <ul>
                            <li><p>Summe THC <i class="font-style-none">[mg]</i> <input type="hidden" name="total_summe" class="total_summe_input" value="0"></p> <span class="total_summe_thc">0</span></li>
                            <li><p>Summe CannaXan 701-1.1 <i class="font-style-none">[mL]</i> <input type="hidden" name="total_cannaxan_thc" class="total_cannaxan_thc_input" value="0"></p> <span class="Summe-CannaXan-THC ">0</span></li>
                            <li><p>Verordnete Anzahl 20 <i class="font-style-none">mL</i> Flaschen CannaXan 701-1.1 <input type="hidden" name="verordnete_anzahl" class="verordnete_anzahl_input" value="0"></p><span class="Anzahl-20mL-Flaschen">0</span></li>
                            <li><p>Rechnerische Restmenge <br> CannaXan 701-1.1 <br> nach 30 Tagen <i class="font-style-none">[mL]</i></p> <input type="hidden" name="rechnerische_restmenge" class="rechnerische_restmenge_input" value="0"><span class="CannaXan-THC-nach-30-Tagen low">0</span></li>

                            <!-- <li>Summe THC [mg] <span class="total_summe_thc">2</span></li>
                            <li>Summe CannaXan-THC [mL] <span class="Summe-CannaXan-THC">134</span></li>
                            <li>Verordnete Anzahl 20mL Flaschen CannaXan-THC <span class="Anzahl-20mL-Flaschen">7</span></li>
                            <li>Rechnerische Restmenge CannaXan-THC nach 30 Tagen [mL]<span class="CannaXan-THC-nach-30-Tagen">6</span></li> -->
                          </ul>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
           <div class="col-md-9 tabel-2small">
            <div class="table-box-form">
              <span><i class="text-red">Eingabewerte in rot.</i> <strong>ALLE SCHWARZEN UND <span>BLAUEN</span> WERTE WERDEN BERECHNET</strong></span>
              <div class="curve-table-bx curve-table2-bx long-table">
                <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/table-images.png" alt="" width="100%"> -->
                <div class="append-table"></div>
                <div class="table-right-part">
                  <p>Anz. = Anzahl</p>
                  <p>SPS = Sprühstöße </p>
                </div>
                <div class="rotate-border"></div>
              </div>
            </div>
           </div>
        </div>
        <div class="btn-mar-top text-right w-100">
            <button type="submit" class="btn btn-primary"><span>Speichern &amp; weiter</span></button>
        </div>
      </form>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(function(){
    getDosistyp();
  })
  function getDosistyp()
  {
    var type = $('.dosistype-field').val();

    if (type == 'dosistyp_1'){
      var url = "<?php echo base_url('therapie/getDosisTypeTableFirst?in='.$insured_number); ?>";
    } else if (type == 'dosistyp_2'){
      var url = "<?php echo base_url('therapie/getDosisTypeTableSecond?in='.$insured_number); ?>";
    } else if (type == 'typ_umstellung'){
      var url = "<?php echo base_url('therapie/getTypUmstellungTable'); ?>";
    }

    $.ajax({
      url: url,
      type: 'get',
      dataType: 'json',
      success: function (data) {
        $('.append-table').html(data.html);
        total_summe_thc();
      }
    })
  }

  function keyUpMorgens($this) 
  {
    var field_value = $($this).val();
    
    /*if(field_value.match(/\./g)){      

    } else {
      var field_value = field_value.replace(/\./g, ',');
    }*/
    field_value = field_value.replace(',', '.');

    var field_key = $($this).attr('data-key');
    var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
    var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
    
    if (field_value.trim()) {
      var morgens_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
      var thc_morgens_mg = (parseFloat(morgens_anzahl_sps) * parseFloat(menge_thc_sps_mg)).toFixed(1);
      
      $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > input').val(morgens_anzahl_sps);
      var morgens_anzahl_sps_text = morgens_anzahl_sps.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > .text-field').text(morgens_anzahl_sps_text);
      $($this).closest('.row_'+field_key).find('.thc_morgens_mg > input').val(thc_morgens_mg);
      var thc_morgens_mg_text = thc_morgens_mg.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.thc_morgens_mg > .text-field').text(thc_morgens_mg_text);
      row_sum(field_key);
    } else {
      $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > input').val(0);
      $($this).closest('.row_'+field_key).find('.morgens_anzahl_sps > .text-field').text(0);
      $($this).closest('.row_'+field_key).find('.thc_morgens_mg > input').val(0);
      $($this).closest('.row_'+field_key).find('.thc_morgens_mg > .text-field').text(0);
      row_sum(field_key);
    }
  }

  function keyUpMittags($this) 
  {
    var field_value = $($this).val();
    
   /* if (field_value.match(/\./g)){      

    } else {
      var field_value = field_value.replace(/\./g, ',');
    }*/

    field_value = field_value.replace(',', '.');

    var field_key = $($this).attr('data-key');
    var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
    var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
    
    if (field_value.trim()) {
      var mittags_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
      var thc_mittags_mg = (parseFloat(mittags_anzahl_sps) * parseFloat(menge_thc_sps_mg)).toFixed(1);
      $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > input').val(mittags_anzahl_sps);
      var mittags_anzahl_sps_text = mittags_anzahl_sps.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > .text-field').text(mittags_anzahl_sps_text);
      $($this).closest('.row_'+field_key).find('.thc_mittags_mg > input').val(thc_mittags_mg);
      var thc_mittags_mg_text = thc_mittags_mg.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.thc_mittags_mg > .text-field').text(thc_mittags_mg_text);
      row_sum(field_key);
    } else {
      $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > input').val(0);
      $($this).closest('.row_'+field_key).find('.mittags_anzahl_sps > .text-field').text(0);
      $($this).closest('.row_'+field_key).find('.thc_mittags_mg > input').val(0);
      $($this).closest('.row_'+field_key).find('.thc_mittags_mg > .text-field').text(0);
      row_sum(field_key);
    }
  }

  function keyUpAbends($this) 
  {
    var field_value = $($this).val();
    /*if(field_value.match(/\./g)){      

    } 
    else {
      var field_value = field_value.replace(/\./g, ',');
    }*/
    field_value = field_value.replace(',', '.');
    var field_key = $($this).attr('data-key');
    var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
    var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
    
    if (field_value.trim()) {
      var abends_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
      var thc_abends_mg = (parseFloat(abends_anzahl_sps) * parseFloat(menge_thc_sps_mg)).toFixed(1);
      $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > input').val(abends_anzahl_sps);
      var abends_anzahl_sps_text = abends_anzahl_sps.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > .text-field').text(abends_anzahl_sps_text);
      $($this).closest('.row_'+field_key).find('.thc_abends_mg > input').val(thc_abends_mg);
      var thc_abends_mg_text = thc_abends_mg.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.thc_abends_mg > .text-field').text(thc_abends_mg_text);
      row_sum(field_key);
    } else {
      $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > input').val(0);
      $($this).closest('.row_'+field_key).find('.abends_anzahl_sps > .text-field').text(0);
      $($this).closest('.row_'+field_key).find('.thc_abends_mg > input').val(0);
      $($this).closest('.row_'+field_key).find('.thc_abends_mg > .text-field').text(0);
      row_sum(field_key);
    }
  }

  function keyUpNachts($this) 
  {
    var field_value = $($this).val();
    /*if(field_value.match(/\./g)){      

    } 
    else {
      var field_value = field_value.replace(/\./g, ',');
    }*/
    field_value = field_value.replace(',', '.');
    var field_key = $($this).attr('data-key');
    var anzahl_spruhstobe_durchgang = "<?php echo $zuordnung_sps->anzahl_spruhstobe_durchgang; ?>";
    var menge_thc_sps_mg = "<?php echo $zuordnung_sps->menge_thc_sps_mg; ?>";
    
    if (field_value.trim()) {
      var nachts_anzahl_sps = (parseFloat(field_value) * parseFloat(anzahl_spruhstobe_durchgang)).toFixed(0);
      var thc_nachts_mg = (parseFloat(nachts_anzahl_sps) * parseFloat(menge_thc_sps_mg)).toFixed(1);

      $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > input').val(nachts_anzahl_sps);
      var nachts_anzahl_sps_text = nachts_anzahl_sps.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > .text-field').text(nachts_anzahl_sps_text);
      $($this).closest('.row_'+field_key).find('.thc_nachts_mg > input').val(thc_nachts_mg);
      var thc_nachts_mg_text = thc_nachts_mg.replace(/\./g, ',');
      $($this).closest('.row_'+field_key).find('.thc_nachts_mg > .text-field').text(thc_nachts_mg_text);
      row_sum(field_key);
    } else {
      $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > input').val(0);
      $($this).closest('.row_'+field_key).find('.nachts_anzahl_sps > .text-field').text(0);
      $($this).closest('.row_'+field_key).find('.thc_nachts_mg > input').val(0);
      $($this).closest('.row_'+field_key).find('.thc_nachts_mg > .text-field').text(0);
      row_sum(field_key);
    }
  }

  function row_sum(field_key) 
  {
    var thc_morgens_mg = parseFloat($('.row_'+field_key).find('.thc_morgens_mg > input').val());
    var thc_mittags_mg = parseFloat($('.row_'+field_key).find('.thc_mittags_mg > input').val());
    var thc_abends_mg = parseFloat($('.row_'+field_key).find('.thc_abends_mg > input').val());
    var thc_nachts_mg = parseFloat($('.row_'+field_key).find('.thc_nachts_mg > input').val());
    var total = (thc_morgens_mg + thc_mittags_mg + thc_abends_mg + thc_nachts_mg).toFixed(1);
    //var total_text = total;     
    var total_text = total.replace(/\./g, ',');

    $('.row_'+field_key).find('.summe_thc_mg > input').val(total);
    $('.row_'+field_key).find('.summe_thc_mg > .text-field').text(total_text);
    total_summe_thc();
  }

  function total_summe_thc()
  {
    var total_sum = 0;
    var total_length = parseInt($('.summe_thc_mg').length) - 1;
    
    $('.summe_thc_mg').each(function(key, value){
     //  alert($(this).find('input').val());
      total_sum = parseFloat(total_sum) + parseFloat($(this).find('input').val());

      if (key == total_length) {
        /*$('.total_summe_thc').text((total_sum.toFixed(1)).replace('.', ','));
        $('.total_summe_input').val(total_sum.toFixed(1));*/
      }
    })
    $('.total_summe_input').val(total_sum.toFixed(1));
    var konzentration_thc_mg_ml = "<?php echo $zuordnung_sps->konzentration_thc_mg_ml; ?>";
    var menge_thc_20_ml = "<?php echo $zuordnung_sps->menge_thc_20_ml; ?>";
    var menge_thc_20_ml = parseInt(menge_thc_20_ml);

    var summe_CannaXan_THC = (total_sum / konzentration_thc_mg_ml).toFixed(1);
    var anzahl_20mL_Flaschen = (summe_CannaXan_THC / 20).toFixed(1);
    var anzahl_20mL_Flaschen = Math.ceil(anzahl_20mL_Flaschen);
    var insgesamt_value = (anzahl_20mL_Flaschen * menge_thc_20_ml).toFixed(1);
    var cannaXan_THC_nach_30_Tagen = ((anzahl_20mL_Flaschen * 20) - summe_CannaXan_THC).toFixed(1);

    $('.total_cannaxan_thc_input').val(summe_CannaXan_THC);
    $('.verordnete_anzahl_input').val(anzahl_20mL_Flaschen);
    $('.rechnerische_restmenge_input').val(cannaXan_THC_nach_30_Tagen);

    var new_total_summe = total_sum.toFixed(1);
    var new_summe_CannaXan_THC = summe_CannaXan_THC.replace('.', ',');
    var new_anzahl_20mL_Flaschen = anzahl_20mL_Flaschen;
    var new_insgesamt_value = insgesamt_value.replace('.', ',');
    var new_cannaXan_THC_nach_30_Tagen = cannaXan_THC_nach_30_Tagen.replace('.', ',');
    $('.total_summe_thc').text(new_total_summe.replace('.', ','));
    $('.Summe-CannaXan-THC').text(new_summe_CannaXan_THC);
    $('.Anzahl-20mL-Flaschen').text(new_anzahl_20mL_Flaschen);
    $('.insgesamt-value').text(new_insgesamt_value);
    $('.CannaXan-THC-nach-30-Tagen').text(new_cannaXan_THC_nach_30_Tagen);
  }

  function showdosistypdetails() {
    $('.dropmenu-box').css('display', 'block');
    
    $(".dropmenu-box").mouseleave(function(){
      $(".dropmenu-box").css('display', 'none');
    });
  }

  $('#select_dosistyp_dropdown_box li').click(function() {
     var getText = $(this).text(); 
     var getValue = $(this).attr('value');

     $('.dosistype-field').val(getValue);
     $('#dosistyp_btn').text(getText);
     $('#select_dosistyp_dropdown_box').addClass('not-red');
     getDosistyp();

   });
</script>
