<div class="curve-table-header">
  <img src="<?php echo $this->config->item('front_assets'); ?>images/table-header-1.png" alt="logo"/>
</div>
<table cellspacing="0" cellspacing="0" class="table table-striped table-bordered">
    <?php if ($dosistyp) { $i = 1; ?>

		<?php foreach ($dosistyp as $key => $value) { ?>		
			<tr class="row_<?php echo $key; ?> adjust-colum">
				<td class="no-cell"><?php echo $i; $i++; ?></td>
				<td class="no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][morgens_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->morgens_anzahl_durchgange)); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMorgens(this)" data-key="<?php echo $key; ?>"></td>

				<td class="no-cell morgens_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][morgens_anzahl_sps]" value="<?php echo $value->morgens_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->morgens_anzahl_sps)); ?></span></td>

				<td class="selected no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][mittags_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->mittags_anzahl_durchgange)); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpMittags(this)" data-key="<?php echo $key; ?>"></td>

				<td class="selected no-cell mittags_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][mittags_anzahl_sps]" value="<?php echo $value->mittags_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->mittags_anzahl_sps)); ?></span></td>

				<td class="no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][abends_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->abends_anzahl_durchgange)); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpAbends(this)" data-key="<?php echo $key; ?>"></td>

				<td class="no-cell abends_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][abends_anzahl_sps]" value="<?php echo $value->abends_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->abends_anzahl_sps)); ?></span></td>

				<td class="selected no-cell"><input type="text" name="dosistyp[dosistyp][<?php echo $key; ?>][nachts_anzahl_durchgange]" value="<?php echo str_replace('.', ',', round($value->nachts_anzahl_durchgange)); ?>" oninput="this.value = this.value.replace(/[^0-9]/g, '');" onkeyup="keyUpNachts(this)" data-key="<?php echo $key; ?>"></td>

				<td class="selected no-cell nachts_anzahl_sps"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][nachts_anzahl_sps]" value="<?php echo $value->nachts_anzahl_sps; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->nachts_anzahl_sps)); ?></span></td>

				<td class="big-cell thc_morgens_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_morgens_mg]" value="<?php echo $value->thc_morgens_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->thc_morgens_mg,1)); ?></span></td>

				<td class="big-cell thc_mittags_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_mittags_mg]" value="<?php echo $value->thc_mittags_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->thc_mittags_mg,1)); ?></span></td>

				<td class="big-cell thc_abends_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_abends_mg]" value="<?php echo $value->thc_abends_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->thc_abends_mg,1)); ?></span></td>

				<td class="big-cell thc_nachts_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][thc_nachts_mg]" value="<?php echo $value->thc_nachts_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->thc_nachts_mg,1)); ?></span></td>

				<td class="big-cell summe_thc_mg"><input type="hidden" name="dosistyp[dosistyp][<?php echo $key; ?>][summe_thc_mg]" value="<?php echo $value->summe_thc_mg; ?>"><span class="text-field"><?php echo str_replace('.', ',', round($value->summe_thc_mg,1)); ?></span></td>

			</tr>
		<?php } ?>
	<?php } ?>
  </table>