<div class="normal-tabel">
  <div class="tabel-1box tabel-3box">
    <table cellspacing="0" class="table  table-bordered">
     <tr>
        <th>Tag</th>
        <th>Anzahl Sprühstöße morgens</th>
        <th>Anzahl Sprühstöße mittags</th>
        <th>Anzahl Sprühstöße abends</th>
        <th>Anzahl Sprühstöße nachts</th>
        <th>THC morgens [mg]</th>
        <th>THC mittags [mg]</th>
        <th>THC abends [mg]</th>
        <th>THC nachts [mg]</th>
        <th>Summe THC [mg]</th>
     </tr>
      <?php if (isset($thherpieplan_folgeverord) && !empty($thherpieplan_folgeverord)) { ?>

      <?php $i = 1; foreach ($thherpieplan_folgeverord as $key => $value) { ?>
        <tr class="row_<?php echo $i; ?>">
          <td><input type="hidden" value="<?php echo $i; ?>" name="thherpieplan_folgeverord[<?php echo $i; ?>][tag_name]"><?php echo $i; ?></td>
          <td class="nocell"><p><input type="text" name="thherpieplan_folgeverord[<?php echo $i; ?>][anzahl_spruhstobe_morgens]" value="<?php echo $value->anzahl_spruhstobe_morgens; ?>" onkeyup="keyUpMorgens(this)" data-key="<?php echo $i; ?>"></p></td>

          <td class="selected nocell"><p><input type="text" name="thherpieplan_folgeverord[<?php echo $i; ?>][anzahl_spruhstobe_mittags]" value="<?php echo $value->anzahl_spruhstobe_mittags; ?>" onkeyup="keyUpMittags(this)" data-key="<?php echo $i; ?>"></p></td>

          <td class="nocell"><p><input type="text" name="thherpieplan_folgeverord[<?php echo $i; ?>][anzahl_spruhstobe_abends]" value="<?php echo $value->anzahl_spruhstobe_abends; ?>" onkeyup="keyUpAbends(this)" data-key="<?php echo $i; ?>"></p></td>

          <td class="nocell selected"><p><input type="text" name="thherpieplan_folgeverord[<?php echo $i; ?>][anzahl_spruhstobe_nachts]" value="<?php echo $value->anzahl_spruhstobe_nachts ; ?>" onkeyup="keyUpNachts(this)" data-key="<?php echo $i; ?>"></p></td>

          <td class="thc_morgens_mg"><input type="hidden" name="thherpieplan_folgeverord[<?php echo $i; ?>][thc_morgens_mg]" value="<?php echo $value->thc_morgens_mg; ?>"> <span class="text-field"><?php echo $value->thc_morgens_mg; ?></span></td>

          <td class="thc_mittags_mg"><input type="hidden" name="thherpieplan_folgeverord[<?php echo $i; ?>][thc_mittags_mg]" value="<?php echo $value->thc_mittags_mg; ?>"> <span class="text-field"><?php echo $value->thc_mittags_mg; ?></span></td>

          <td class="thc_abends_mg"><input type="hidden" name="thherpieplan_folgeverord[<?php echo $i; ?>][thc_abends_mg]" value="<?php echo $value->thc_abends_mg; ?>"> <span class="text-field"><?php echo $value->thc_abends_mg; ?></span></td>

          <td class="thc_nachts_mg"><input type="hidden" name="thherpieplan_folgeverord[<?php echo $i; ?>][thc_nachts_mg]" value="<?php echo $value->thc_nachts_mg; ?>"> <span class="text-field"><?php echo $value->thc_nachts_mg; ?></span></td>

          <td class="summe_thc_mg"><input type="hidden" name="thherpieplan_folgeverord[<?php echo $i; ?>][summe_thc_mg]" value="<?php echo $value->summe_thc_mg; ?>"> <span class="text-field"><?php echo $value->summe_thc_mg; ?></span></td>
        </tr>
      <?php $i++; } ?>
     <?php } ?>
    </table>
  </div>
</div>