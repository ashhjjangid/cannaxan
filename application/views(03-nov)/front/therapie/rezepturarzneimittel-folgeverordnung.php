 <section class="same-section rezepturarzneimittel-section pb-0">
         <div class="container-fluid">
            <div class="page-heading">
              <div class="row align-items-center">
                <div class="col-md-7">
                  <div class="custom-heading">
                    <h2>Hinweis:<br> 
                      <span>Diesen Therapieplan 2x ausdrucken<br> (1x für Patient, 1x für Abgabe durch Patient bei Apotheke)</span>
                    </h2>
                  </div>
                </div>
                <div class="col-md-5 text-right">
                  <div class="min-width-btn">
                    <button class="btn btn-primary" onclick="makepdf()"><span>Therapieplan herunterladen</span></button>
                    <a class="btn btn-primary" href="<?php echo base_url('process_control/begleitmedikation?in='.$versichertennr) ?>"><span>Weiter mit Begleitmedikamente</span></a>
                  </div>
                </div>
              </div>
            </div>
            <section class="blue-line-section pb-0" style="border-bottom: none;">
              <div class="same-heading ">
                <div class="row align-items-center">
                  <div class="col-md-7">
                      <h2 class="text-left">ÄRZTLICHER THERAPIEPLAN</h2>
                  </div>
                  <div class="col-md-5 text-right">
                   <div class="heading-logo">
                    <img src="<?php echo $this->config->item('front_assets'); ?>images/logo.png" alt="logo">
                   </div>
                  </div>
                </div>
              </div>
              <div class="row ">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-7">
                        <label class="blod-text">Lebenslange Arztnummer
                          <span><?php echo $title.' '. $first_name.' '. $last_name.'<br/>'. $strabe. ' '.$hausnr.'<br/>'. $plzl.' '.$ort; ?><!-- Dr. med. Onkel Doktor um die Ecke Musterstraße 1a 81828 Musterstadt --></span>
                        </label>
                      </div>
                      <div class="col-sm-5">
                        <input type="text" name="lanr" class="form-control text-center" placeholder="29.02.2020" readonly="" value="<?php echo $lanr; ?>">
                      </div>
                    </div>
                  </div>
                  </div>
                 <div class="col-md-4">
                 </div>
                 <div class="col-md-4"></div>
                </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                      <div class="col-sm-7">
                         <label>Rezepturarzneimittel</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="rezepturarzneimittel" class="form-control text-center" readonly="" placeholder="CannaXan-THC" value="<?php echo $rezepturarzneimittel; ?>">
                      </div>
                   </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>Versichertennr.</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="versichertennr" class="form-control text-center" placeholder="A123456789" readonly="" value="<?php echo $versichertennr; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                      <div class="col-sm-7">
                         <label>Wirkstoff:</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="wirkstoff" class="form-control text-center" readonly="" placeholder="CannaXan-701-1.1" value="<?php echo $wirkstoff; ?>">
                      </div>
                   </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>Name</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="name" class="form-control text-center" placeholder="Muster" readonly="" value="<?php echo $name; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                      <div class="col-sm-7">
                         <label>Therapiedauer (Tage):</label>
                      </div>
                      <div class="col-sm-5">
                         <input type="text" name="tag_name" class="form-control text-center" readonly="" placeholder="30" value="<?php echo round($therapiedauer, 0); ?>">
                      </div>
                   </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>VorName</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="vorname" class="form-control text-center" placeholder="Beispiel" readonly="" value="<?php echo $vorname; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-7">
                        <label>Therapietyp:</label>
                      </div>
                      <div class="col-sm-5">
                        <input type="text" name="dosistyp" class="form-control text-center" readonly="" placeholder="Folgeverordnung" value="<?php echo 'Folgeverordnung'; ?>">
                      </div>
                    </div>
                     <!-- <div class="row">
                        <div class="col-sm-7">
                           <label>Gleichbleibende Tagesdosis CBD festlegen<br> <span>(Optimum von Wirkung/ Nebenwirkung aus Titrationsphase, oder Anpassung)</span></label>
                        </div>
                        <div class="col-sm-5">
                           <div class="checkbox-custom">
                              <input type="checkbox" id="me" name="gleichbleibende_tagesdosis" <?php echo ($gleichbleibende_tagesdosis == "Yes")?'checked':''; ?>>
                              <label for="me"><span class="cheak"></span></label>
                           </div>
                        </div>
                     </div> -->
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>Geb. Datum</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="gedatum" class="form-control text-center" placeholder="TT.MM.JJJJ" readonly="" value="<?php echo $gedatum; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-7">
                        <label>Ausstellungsdatum:</label>
                      </div>
                      <div class="col-sm-5">
                        <input type="text" id="datum" name="datum" class="form-control text-center" readonly="" placeholder="18.11.2019" value="<?php echo $datum; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5 offset-md-2 align-self-center">
                           <label>Telefonr.</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="telefone" class="form-control text-center" placeholder="0165 87654321" readonly="" value="<?php echo $telefone; ?>">
                        </div>
                     </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-7">
                        <label>Dosistyp</label>
                      </div>
                      <div class="col-sm-5">
                        <input type="text" name="dosistyp" class="form-control text-center" readonly="" placeholder="Dosistyp 1" value="<?php echo $dosistyp; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-7">
                           <label>Therapiedauer für<br> gleichbleibende Tagesdosis [Tage]</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="therapiedauer" class="form-control text-center" placeholder="30" readonly="" value="<?php echo str_replace('.', ',', $therapiedauer); ?>">
                        </div>
                     </div>
                  </div>
                </div> -->
               <!--  <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-7">
                           <label>Tagesdosis CBD <span>[mg]</span></label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="tagesdosis_cbd" class="form-control text-center" readonly="" placeholder="9,2" value="<?php echo str_replace('.', ',', $tagesdosis_cbd); ?>">
                        </div>
                     </div>
                  </div>
                </div> -->
              </div>
              <div class="row">
                <div class="col-md-3 tabel-1small">
                  <div class="text-rezept">
                    <div class="text-rezept-event gray-bx">
                        <div class="heading-right">
                          <span>ANGABEN für BtM Rezept:</span>
                        </div>
                        <ul>
                           <li>CannaXan 701-1.1; <span>2,2 <i class="font-style-none">mg/mL</i> THC</span></li>
                           <li>insgesamt:<span><?php echo str_replace('.', ',', round($verordnete_anzahl,1)); ?> x 20 <i class="font-style-none">mL</i> <?php echo str_replace('.', ',', round(($verordnete_anzahl * 44),1)); ?> <i class="font-style-none">mg</i> THC</span></li>
                        </ul>
                        <span class="dosierung"><!-- Dosierung siehe Therapieplan -->Dosierung gemäß schriftlicher Anweisung</span>
                     </div>
                     <div class="text-rezept-event low-span-width mt-3 gray-bx">
                      <div class="heading-right">
                        <span>Zusammenfassung:</span>
                      </div>
                      <ul>
                         <li><p>Summe THC <i class="font-style-none">[mg]</i></p> <span class="total_summe"><?php echo str_replace('.', ',', round($total_summe,1)); ?></span></li>
                         <li><p>Summe CannaXan 701-1.1 <i class="font-style-none">[mL]</i></p> <span class="total_cannaxan_thc"><?php echo str_replace('.', ',', round($total_cannaxan_thc,1)); ?></span></li>
                         <li><p>Verordnete Anzahl 20 <i class="font-style-none">mL</i> Flaschen CannaXan 701-1.1</p> <span class="verordnete_anzahl"><?php echo str_replace('.', ',', round($verordnete_anzahl,1)); ?></span></li>
                         <li><p>Rechnerische Restmenge <br> CannaXan 701-1.1 <br> nach 30 Tagen <i class="font-style-none">[mL]</i></p> <span class="rechnerische_restmenge"><?php echo str_replace('.', ',', round($rechnerische_restmenge,1)); ?></span></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-9 tabel-2small">
                  <div class="table-box-form">
                    <!-- <span><i class="text-red">Eingabewerte in rot.</i> <strong>ALLE SCHWARZEN UND <span>BLAUEN</span> WERTE WERDEN BERECHNET</strong></span> -->
                    <div class="curve-table-bx curve-table2-bx">
                      <div class="curve-table-header">
                        <img src="<?php echo $this->config->item('front_assets'); ?>images/table-header-1.png" alt="logo"/>
                      </div>
                      <table cellspacing="0" cellspacing="0" class="table table-striped table-bordered">
                        <?php if (isset($thherpieplan_folgeverord) && !empty($thherpieplan_folgeverord)) { ?>

                        <?php $i = 1; foreach ($thherpieplan_folgeverord as $key => $value) { ?>
                          <tr class="row_<?php echo $i; ?> adjust-colum">
                            <td><?php echo $i; ?></td>
                            <td class="nocell"><p><?php echo str_replace('.', ',', round($value->anzahl_spruhstobe_morgens)); ?></p></td>
                            <td class="nocell"><p><?php echo str_replace('.', ',', round($value->anzahl_sps_morgens)); ?></p></td>

                            <td class="selected nocell"><p><?php echo str_replace('.', ',', round($value->anzahl_spruhstobe_mittags)); ?></p></td>
                            <td class="selected nocell"><p><?php echo str_replace('.', ',', round($value->anzahl_sps_mittags)); ?></p></td>

                            <td class="nocell"><p><?php echo str_replace('.', ',', round($value->anzahl_spruhstobe_abends)); ?></p></td>
                            <td class="nocell"><p><?php echo str_replace('.', ',', round($value->anzahl_sps_abends)); ?></p></td>

                            <td class="nocell selected"><p><?php echo str_replace('.', ',', round($value->anzahl_spruhstobe_nachts)) ; ?></p></td>
                            <td class="nocell selected"><p><?php echo str_replace('.', ',', round($value->anzahl_sps_nachts)) ; ?></p></td>

                            <td class="thc_morgens_mg"> <span class="text-field"><?php echo str_replace('.', ',', round($value->thc_morgens_mg,1)); ?></span></td>

                            <td class="thc_mittags_mg"> <span class="text-field"><?php echo str_replace('.', ',', round($value->thc_mittags_mg,1)); ?></span></td>

                            <td class="thc_abends_mg"> <span class="text-field"><?php echo str_replace('.', ',', round($value->thc_abends_mg,1)); ?></span></td>

                            <td class="thc_nachts_mg"> <span class="text-field"><?php echo str_replace('.', ',', round($value->thc_nachts_mg,1)); ?></span></td>

                            <td class="summe_thc_mg"> <span class="text-field"><?php echo str_replace('.', ',', round($value->summe_thc_mg,1)); ?></span></td>
                          </tr>
                        <?php $i++; } ?>
                       <?php } ?>
                      </table>
                      <div class="table-right-part">
                        <p>Anz. = Anzahl</p>
                        <p>SPS = Sprühstöße </p>
                      </div>
                      <div class="rotate-border"></div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <section class="same-section titration-event pt-0">
        <div class="container-fluid">
          <section class="blue-line-section" style="border-top: none;">
            <div class="event-blog">
              <ul>
                <li>
                  <span>Einnahmehinweis:</span>
                  <p>1. Morgens und/oder abends <b>jeweils EINEN</b> Sprühstoß in die linke Wangeninnenseite, unter die Zunge, sowie in die rechte Wangeninnenseite geben. Dieser Vorgang wird als <b>Durchgang</b> bezeichnet.</p>
                  <div class="einnah-images">
                    <img src="<?php echo $this->config->item('front_assets'); ?>images/einnahmehinweis.png" alt="">
                    <div class="row">
                      <div class="col-md-4">
                        <p>Linke Wangeninnenseite</p>
                      </div>
                      <div class="col-md-4">
                        <p>Unter die Zunge</p>
                      </div>
                      <div class="col-md-4">
                        <p>Rechte Wangeninnenseite</p>
                      </div>

                    </div>
                  </div>
                </li>
                <li>
                  <p>2. 30 Sekunden warten, dann schlucken!</p>
                </li>
                <li>
                  <p>3. Obigen Durchgang gemäß Verordnung wiederholen (Beispiel: bei jeweils 6 Sprühstößen morgens und abends: obigen Durchgang 2x wiederholen und dazwischen 30 Sekunden warten, dann schlucken).</p>
                </li>
                <li>
                  <span>Hinweise für Patienten:</span>
                  <p><!-- Die Dosissteigerung ist solange durchzuführen bis die Nebenwirkungen (Schwindel, Berauschtheit etc.) zu stark werden. Danach sollte auf die Dosis zurückgegangen werden, bei der die Nebenwirkungen noch erträglich waren. Sollten bei der niedrigen Dosis noch Nebenwirkungen bestehen, werden diese innerhalb weniger Tage bei Beibehaltung der Dosis verschwinden. --> 
                  Bitte steigern Sie die Dosis gemäß dem Therapieplan bis sich eine ausreichende Wirkung eingestellt hat. Sollten Nebenwirkungen  wie z.B. Schwindel oder Berauschtheit eintreten, kontaktieren Sie bitte Ihren behandelnden Arzt und bleiben Sie bei der bestehenden Dosis. 
                </p>
                </li>
                <li>
                  <span>Hinweise für Rezepteinreichung in Apotheke:</span>
                  <p>
                     Bitte geben Sie eine Kopie dieses Therapieplans zusammen mit Ihrem Rezept in der Apotheke ab, damit der Apotheker eine gesetzlich verpflichtete Plausibilitätsprüfung durchführen kann.
                  </p>
                </li>
              </u1>
              <div class="bottom-text">
                <p>Stempel/Unterschrift des/der verordneten Arztes/Ärztin</p>
                <?php if ($unterschrift) { ?>
                  <img height="75" width="200" src="<?php echo base_url('assets/uploads/unterschrift-images/'. $unterschrift);?>" align="right" alt="">
                <?php } ?>
              </div>
            </div>
          </section>
        </div>
      </section>
      <script>
        function makepdf() {
          window.location.href = "<?php echo base_url('therapie/generate_folgeverordnung_pdf?in='.$versichertennr); ?>"
        }
      </script>