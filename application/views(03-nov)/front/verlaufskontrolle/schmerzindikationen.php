<section class="same-section same-blog schmerz-blog">
         <div class="container-fluid">
            <div class="same-heading table-heading">
               <h2 class="text-left">Navigator für Schmerzindikationen</h2>
            </div>
            <div class="eidt-daten text-right">
               <span class="eidt-icon-text">Daten bearbeiten mit:</span>
               <span class="eidt-icon"><img src="<?php echo $this->config->item('front_assets'); ?>images/edit-icon.png" alt=""></span>
            </div>
             <form method="post" class="ajax_form" action="<?php echo base_url('process_control/add_visites'). '?in='. $insured_number; ?>">
              <div class="min-height">
               <div class="chart-schmerz text-center">
                  <div class="visit-plan-table">
                     <div class="visit-plan-table-header">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1436.933" height="171.437" viewBox="0 0 1436.933 171.437">
                           <defs>
                           <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                              <stop offset="0" stop-color="#192351"/>
                              <stop offset="0.032" stop-color="#2f3861"/>
                              <stop offset="0.111" stop-color="#5f6686"/>
                              <stop offset="0.191" stop-color="#898ea6"/>
                              <stop offset="0.273" stop-color="#abafc0"/>
                              <stop offset="0.355" stop-color="#c5c8d4"/>
                              <stop offset="0.44" stop-color="#d8dae2"/>
                              <stop offset="0.527" stop-color="#e3e5eb"/>
                              <stop offset="0.622" stop-color="#e7e9ee"/>
                              <stop offset="0.887" stop-color="#fff"/>
                           </linearGradient>
                           <filter id="Rectangle_159" x="783.252" y="113.117" width="50" height="51" filterUnits="userSpaceOnUse">
                              <feOffset dx="-2" dy="-2" input="SourceAlpha"/>
                              <feGaussianBlur stdDeviation="2" result="blur"/>
                              <feFlood flood-color="#192351" flood-opacity="0.6"/>
                              <feComposite operator="in" in2="blur"/>
                              <feComposite in="SourceGraphic"/>
                           </filter>
                           <filter id="Rectangle_159-2" x="996.252" y="113.117" width="50" height="51" filterUnits="userSpaceOnUse">
                              <feOffset dx="-2" dy="-2" input="SourceAlpha"/>
                              <feGaussianBlur stdDeviation="2" result="blur-2"/>
                              <feFlood flood-color="#192351" flood-opacity="0.6"/>
                              <feComposite operator="in" in2="blur-2"/>
                              <feComposite in="SourceGraphic"/>
                           </filter>
                           <filter id="Rectangle_159-3" x="1216.252" y="113.117" width="51" height="51" filterUnits="userSpaceOnUse">
                              <feOffset dx="-2" dy="-2" input="SourceAlpha"/>
                              <feGaussianBlur stdDeviation="2" result="blur-3"/>
                              <feFlood flood-color="#192351" flood-opacity="0.6"/>
                              <feComposite operator="in" in2="blur-3"/>
                              <feComposite in="SourceGraphic"/>
                           </filter>
                           </defs>
                           <g id="Group_470" data-name="Group 470" transform="translate(-241.748 -348.883)">
                           <path id="Path_478" data-name="Path 478" d="M1585.359,1346.678l170.306-169.527H490.947L320.642,1346.678Z" transform="translate(-77.938 -827.313)" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.91" fill="url(#linear-gradient)"/>
                           <g id="Group_253" data-name="Group 253" transform="translate(270.633 389.78)">
                              <text id="VISITENNUMMER" transform="translate(7.778 116.281) rotate(-45)" fill="#192351" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">VISITENNUMMER</tspan></text>
                              <text id="DATUM" transform="translate(80.128 114.546) rotate(-45)" fill="#192351" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">DATUM</tspan></text>
                              <text id="PATIENT_ANLEGEN" data-name="PATIENT ANLEGEN" transform="translate(163.705 116.279) rotate(-45)" fill="#192351" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">PATIENT ANLEGEN</tspan></text>
                              <text id="NRS_SCHMERZ" data-name="NRS SCHMERZ" transform="translate(219.235 116.277) rotate(-45)" fill="#192351" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">NRS SCHMERZ</tspan></text>
                              <text id="SF-12_Lebensqualität_" data-name="SF-12 (Lebensqualität)" transform="translate(265.92 116.278) rotate(-45)" fill="#192351" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">SF-12 (Lebensqualität)</tspan></text>
                              <text id="RIS_Schlafqualität_" data-name="RIS (Schlafqualität)" transform="translate(419.596 116.278) rotate(-45)" fill="#192351" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">RIS (Schlafqualität)</tspan></text>
                              <text id="LABORPARAMETER" transform="translate(552.383 116.279) rotate(-45)" fill="#9e2b5f" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">LABORPARAMETER</tspan></text>
                              <text id="Schwere_NEBENWIRKUNGEN" data-name="Schwere 
                        NEBENWIRKUNGEN" transform="translate(1061.21 116.279) rotate(-45)" fill="#9e2b5f" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">SCHWERE </tspan><tspan x="0" y="14">NEBENWIRKUNGEN</tspan></text>
                              <text id="BEGLEITMEDIKATION" transform="translate(912.37 116.278) rotate(-45)" fill="#9e2b5f" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">BEGLEITMEDIKATION</tspan></text>
                              <text id="LABORWERTE" transform="translate(853.223 116.279) rotate(-45)" fill="#9e2b5f" font-size="12" font-family="SegoeUI-Bold, Segoe UI" font-weight="700" letter-spacing="0.05em"><tspan x="0" y="0">LABORWERTE</tspan></text>
                           </g>
                           <g id="Group_254" data-name="Group 254" transform="translate(242.703 518.666)">
                              <path id="Path_481" data-name="Path 481" d="M320.642,1286.707v1.4h1265.42v-1.4H320.642" transform="translate(-320.642 -1286.707)" fill="#192351" fill-rule="evenodd"/>
                           </g>
                           <line id="Line_190" data-name="Line 190" y1="168.95" x2="169.729" transform="translate(277.854 350.415)" fill="none" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.454"/>
                           <line id="Line_193" data-name="Line 193" y1="168.95" x2="169.729" transform="translate(383.305 350.415)" fill="none" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.454"/>
                           <line id="Line_194" data-name="Line 194" y1="168.95" x2="169.729" transform="translate(452.2 350.415)" fill="none" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.454"/>
                           <line id="Line_195" data-name="Line 195" y1="168.95" x2="169.729" transform="translate(487.351 350.415)" fill="none" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.454"/>
                           <line id="Line_196" data-name="Line 196" y1="168.95" x2="169.729" transform="translate(643.419 350.415)" fill="none" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.454"/>
                           <line id="Line_197" data-name="Line 197" y1="168.95" x2="169.729" transform="translate(771.368 350.415)" fill="none" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.454"/>
                           <line id="Line_198" data-name="Line 198" y1="168.95" x2="169.729" transform="translate(1077.177 350.415)" fill="none" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.454"/>
                           <line id="Line_199" data-name="Line 199" y1="168.95" x2="169.729" transform="translate(1132.715 350.415)" fill="none" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.454"/>
                           <line id="Line_200" data-name="Line 200" y1="168.95" x2="169.729" transform="translate(1283.863 350.415)" fill="none" stroke="#192351" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.454"/>
                           <path id="Path_534" data-name="Path 534" d="M321.1,1286.707v1.4H1585.815v-1.4H321.1" transform="translate(-77.692 -768.041)" fill="#192351" fill-rule="evenodd"/>
                           <g id="Group_325" data-name="Group 325" transform="translate(1032.769 470.4)">
                              <g transform="matrix(1, 0, 0, 1, -791.02, -121.52)" filter="url(#Rectangle_159)">
                                 <a href="<?php echo base_url('process_control?in='.$insured_number); ?>">
                                 <rect id="Rectangle_159-4" data-name="Rectangle 159" width="38" height="39" transform="translate(791.25 121.12)" fill="#fff"/>
                                 </a>
                              </g>
                              <g id="Group_323" data-name="Group 323" transform="translate(7.783 4.227)">
                                 <g id="Report_icon" data-name="Report icon" transform="translate(0 0)">
                                 <path id="Path_561" data-name="Path 561" d="M162.5,161.663V171.9a.485.485,0,0,1-.484.484H139.111a.484.484,0,0,1-.484-.484V142.663a.484.484,0,0,1,.484-.484h22.906a.485.485,0,0,1,.484.484v19h-1.7V144.345a.484.484,0,0,0-.484-.484h-19.5a.484.484,0,0,0-.484.484V170.22a.485.485,0,0,0,.484.484h19.5a.485.485,0,0,0,.484-.484v-8.556" transform="translate(-138.627 -142.179)" fill="#192351" fill-rule="evenodd"/>
                                 </g>
                                 <g id="Pencil" transform="translate(4.006 5.547)">
                                 <path id="Path_562" data-name="Path 562" d="M145.007,178.4l2.717-1.76-2.093-1.505-.822,3.121A.131.131,0,0,0,145.007,178.4Z" transform="translate(-144.804 -159.308)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_563" data-name="Path 563" d="M148.895,172.943l-2.3-1.657a.193.193,0,0,1-.075-.206l1.212-4.766a.2.2,0,0,1,.181-.147l.824-.031a.574.574,0,0,1,.558.364l.372.949a.193.193,0,0,0,.18.123l.865,0a.575.575,0,0,1,.534.368l.34.883a.2.2,0,0,0,.182.125h.738a.575.575,0,0,1,.543.385l.246.7a.194.194,0,0,1-.077.227l-4.1,2.68A.2.2,0,0,1,148.895,172.943Z" transform="translate(-145.404 -156.146)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_564" data-name="Path 564" d="M160.127,152.971l1.318.948L154,163.235l-.276-.715a.349.349,0,0,0-.324-.224l-.722,0Z" transform="translate(-147.571 -151.519)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_565" data-name="Path 565" d="M149.151,160.17l-.015-.011,7.452-9.324a.247.247,0,0,1,.345-.056l1.4,1.008-7.444,9.313-.292-.745a.359.359,0,0,0-.348-.228Z" transform="translate(-146.326 -150.732)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_566" data-name="Path 566" d="M156.94,165.624l.015.011,7.414-9.279a.305.305,0,0,0-.07-.425l-1.356-.974-7.44,9.316.8.038a.359.359,0,0,1,.327.257Z" transform="translate(-148.564 -152.217)" fill="#192351" fill-rule="evenodd"/>
                                 </g>
                              </g>
                           </g>
                           <g id="Group_326" data-name="Group 326" transform="translate(1245.539 470.4)">
                              <g transform="matrix(1, 0, 0, 1, -1003.79, -121.52)" filter="url(#Rectangle_159-2)">
                                 <a href="<?php echo base_url('process_control/begleitmedikation?in='.$insured_number); ?>">
                                 <rect id="Rectangle_159-5" data-name="Rectangle 159" width="38" height="39" transform="translate(1004.25 121.12)" fill="#fff"/>
                                 </a>
                              </g>
                              <g id="Group_323-2" data-name="Group 323" transform="translate(7.783 4.227)">
                                 <g id="Report_icon-2" data-name="Report icon" transform="translate(0 0)">
                                 <path id="Path_561-2" data-name="Path 561" d="M162.5,161.663V171.9a.485.485,0,0,1-.484.484H139.111a.484.484,0,0,1-.484-.484V142.663a.484.484,0,0,1,.484-.484h22.906a.485.485,0,0,1,.484.484v19h-1.7V144.345a.484.484,0,0,0-.484-.484h-19.5a.484.484,0,0,0-.484.484V170.22a.485.485,0,0,0,.484.484h19.5a.485.485,0,0,0,.484-.484v-8.556" transform="translate(-138.627 -142.179)" fill="#192351" fill-rule="evenodd"/>
                                 </g>
                                 <g id="Pencil-2" data-name="Pencil" transform="translate(4.006 5.547)">
                                 <path id="Path_562-2" data-name="Path 562" d="M145.007,178.4l2.717-1.76-2.093-1.505-.822,3.121A.131.131,0,0,0,145.007,178.4Z" transform="translate(-144.804 -159.308)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_563-2" data-name="Path 563" d="M148.895,172.943l-2.3-1.657a.193.193,0,0,1-.075-.206l1.212-4.766a.2.2,0,0,1,.181-.147l.824-.031a.574.574,0,0,1,.558.364l.372.949a.193.193,0,0,0,.18.123l.865,0a.575.575,0,0,1,.534.368l.34.883a.2.2,0,0,0,.182.125h.738a.575.575,0,0,1,.543.385l.246.7a.194.194,0,0,1-.077.227l-4.1,2.68A.2.2,0,0,1,148.895,172.943Z" transform="translate(-145.404 -156.146)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_564-2" data-name="Path 564" d="M160.127,152.971l1.318.948L154,163.235l-.276-.715a.349.349,0,0,0-.324-.224l-.722,0Z" transform="translate(-147.571 -151.519)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_565-2" data-name="Path 565" d="M149.151,160.17l-.015-.011,7.452-9.324a.247.247,0,0,1,.345-.056l1.4,1.008-7.444,9.313-.292-.745a.359.359,0,0,0-.348-.228Z" transform="translate(-146.326 -150.732)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_566-2" data-name="Path 566" d="M156.94,165.624l.015.011,7.414-9.279a.305.305,0,0,0-.07-.425l-1.356-.974-7.44,9.316.8.038a.359.359,0,0,1,.327.257Z" transform="translate(-148.564 -152.217)" fill="#192351" fill-rule="evenodd"/>
                                 </g>
                              </g>
                           </g>
                           <g id="Group_327" data-name="Group 327" transform="translate(1466.4 470.4)">
                              <g transform="matrix(1, 0, 0, 1, -1224.65, -121.52)" filter="url(#Rectangle_159-3)">
                                 <a href="<?php echo base_url('nebenwirkung?in='.$insured_number); ?>">
                                 <rect id="Rectangle_159-6" data-name="Rectangle 159" width="39" height="39" transform="translate(1224.25 121.12)" fill="#fff"/>
                                 </a>
                              </g>
                              <g id="Group_323-3" data-name="Group 323" transform="translate(7.783 4.227)">
                                 <g id="Report_icon-3" data-name="Report icon" transform="translate(0 0)">
                                 <path id="Path_561-3" data-name="Path 561" d="M162.5,161.663V171.9a.485.485,0,0,1-.484.484H139.111a.484.484,0,0,1-.484-.484V142.663a.484.484,0,0,1,.484-.484h22.906a.485.485,0,0,1,.484.484v19h-1.7V144.345a.484.484,0,0,0-.484-.484h-19.5a.484.484,0,0,0-.484.484V170.22a.485.485,0,0,0,.484.484h19.5a.485.485,0,0,0,.484-.484v-8.556" transform="translate(-138.627 -142.179)" fill="#192351" fill-rule="evenodd"/>
                                 </g>
                                 <g id="Pencil-3" data-name="Pencil" transform="translate(4.006 5.547)">
                                 <path id="Path_562-3" data-name="Path 562" d="M145.007,178.4l2.717-1.76-2.093-1.505-.822,3.121A.131.131,0,0,0,145.007,178.4Z" transform="translate(-144.804 -159.308)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_563-3" data-name="Path 563" d="M148.895,172.943l-2.3-1.657a.193.193,0,0,1-.075-.206l1.212-4.766a.2.2,0,0,1,.181-.147l.824-.031a.574.574,0,0,1,.558.364l.372.949a.193.193,0,0,0,.18.123l.865,0a.575.575,0,0,1,.534.368l.34.883a.2.2,0,0,0,.182.125h.738a.575.575,0,0,1,.543.385l.246.7a.194.194,0,0,1-.077.227l-4.1,2.68A.2.2,0,0,1,148.895,172.943Z" transform="translate(-145.404 -156.146)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_564-3" data-name="Path 564" d="M160.127,152.971l1.318.948L154,163.235l-.276-.715a.349.349,0,0,0-.324-.224l-.722,0Z" transform="translate(-147.571 -151.519)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_565-3" data-name="Path 565" d="M149.151,160.17l-.015-.011,7.452-9.324a.247.247,0,0,1,.345-.056l1.4,1.008-7.444,9.313-.292-.745a.359.359,0,0,0-.348-.228Z" transform="translate(-146.326 -150.732)" fill="#192351" fill-rule="evenodd"/>
                                 <path id="Path_566-3" data-name="Path 566" d="M156.94,165.624l.015.011,7.414-9.279a.305.305,0,0,0-.07-.425l-1.356-.974-7.44,9.316.8.038a.359.359,0,0,1,.327.257Z" transform="translate(-148.564 -152.217)" fill="#192351" fill-rule="evenodd"/>
                                 </g>
                              </g>
                           </g>
                           </g>
                        </svg>

                     </div>
                     <!-- <div class="tabel-1box-header">
                        <div class="row">
                           <div class="col same-width subtabel-width edit-btn">
                              <div class="text-box border-left">
                                 <p>Laborparameter</p>
                                 <div class="eidt-daten space">
                                    <a href="<?php echo base_url('process_control?in='.$insured_number); ?>"><img src="<?php echo base_url('assets/front/images/eidt.jpeg'); ?>" alt=""></a>
                                 </div>
                              </div>
                           </div>
                           <div class="col same-width subtabel-width small">
                              <div class="text-box border-left">
                                 <p>Laborwerte</p>
                              </div>
                           </div>
                           <div class="col same-width address1-width edit-btn">
                              <div class="text-box border-left">
                                 <p>Begleitmedikation</p>
                                    <div class="eidt-daten">
                                    <a href="<?php echo base_url('process_control/begleitmedikation?in='.$insured_number); ?>"><img src="<?php echo base_url('assets/front/images/eidt.jpeg'); ?>" alt=""></a>
                                 </div>
                              </div>
                           </div>
                           <div class="col same-width address1-width edit-btn">
                              <div class="text-box border-left">
                                 <p>Nebenwirkungen</p>
                                    <div class="eidt-daten">
                                    <a href="<?php echo base_url('nebenwirkung?in='.$insured_number); ?>"><img src="<?php echo base_url('assets/front/images/eidt.jpeg'); ?>" alt=""></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div> -->
                     <table cellspacing="0" cellspacing="0" class="table table-striped table-bordered">
                        <tbody class="append-new-row">

                           <?php if ($patient_visits) { ?>
                           <?php foreach ($patient_visits as $key => $value) { ?>
                              <tr class="table-rows filled-row">
                                 <td class="no-cell"><?php echo $value->visite; ?></td>
                                 <td class="no-cell">
                                 <?php echo date('d.m.Y', strtotime($value->datum)); ?>
                                 </td>
                                 <!-- <td class="no-cell"></td>
                                 <td class="no-cell"></td> -->
                                 <td class="button">
                                    <div>
                                       <?php if(($key == 0) && $is_exist_patient->is_profile_complete == 'No') { ?>
                                          <a href="<?php echo base_url('patients/anlegen?in='.$insured_number); ?>"><img src="<?php echo base_url('assets/front/images/eidt.jpeg')?>" alt=""></a>
                                       <?php } else { ?>
                                          OK
                                       <?php } ?>
                                    </div>
                                 </td>
                                 <td class="no-cell"><?php echo $value->nrs_schmerz; ?></td>
                                 <td class="address1"><?php echo $value->sf_12_lebensqualitat; ?></td>
                                 <td class="address1"><?php echo $value->ris_schlafqualitat; ?></td>
                                 <td class="sub-tabel">
                                    <table cellspacing="0" cellspacing="0" border="0" width="100%">
                                       <?php if ($value->verlaufskontrolle_laborwerte) { ?>
                                          
                                          <?php if ($value->verlaufskontrolle_laborwerte->laborparameter1) { ?>
                                             <tr>
                                                <td><?php echo $value->verlaufskontrolle_laborwerte->laborparameter1; ?></td>
                                             </tr>
                                          <?php } ?>

                                          <?php if ($value->verlaufskontrolle_laborwerte->laborparameter2) { ?>
                                             <tr>
                                                <td><?php echo $value->verlaufskontrolle_laborwerte->laborparameter2; ?></td>
                                             </tr>
                                          <?php } ?>

                                          <?php if ($value->verlaufskontrolle_laborwerte->laborparameter3) { ?>
                                             <tr>
                                                <td><?php echo $value->verlaufskontrolle_laborwerte->laborparameter3; ?></td>
                                             </tr>
                                          <?php } ?>

                                          <?php if ($value->verlaufskontrolle_laborwerte->laborparameter4) { ?>
                                             <tr>
                                                <td><?php echo $value->verlaufskontrolle_laborwerte->laborparameter4; ?></td>
                                             </tr>
                                          <?php } ?>

                                       <?php } else { ?>
                                          <tr>
                                             <td></td>
                                          </tr>
                                       <?php } ?>
                                    </table>
                                 </td>
                                 <td class="sub-tabel small">
                                    <table cellspacing="0" cellspacing="0" border="0" width="100%">
                                       <?php if ($value->verlaufskontrolle_laborwerte) { ?>
                                          
                                          <?php //if ($value->verlaufskontrolle_laborwerte->wert1) { 
                                             $wert1_roundoff = $value->verlaufskontrolle_laborwerte->wert1;
                                          ?>
                                             <tr>
                                                <td><?php echo str_replace('.', ',', $wert1_roundoff); ?></td>
                                             </tr>
                                          <?php //} ?>

                                          <?php //if ($value->verlaufskontrolle_laborwerte->wert2) { 
                                             $wert2_roundoff = $value->verlaufskontrolle_laborwerte->wert2;
                                          ?>
                                             <tr>
                                                <td><?php echo str_replace('.', ',', $wert2_roundoff); ?></td>
                                             </tr>
                                          <?php //} ?>

                                          <?php //if ($value->verlaufskontrolle_laborwerte->wert3) { 
                                             $wert3_roundoff = $value->verlaufskontrolle_laborwerte->wert3;
                                          ?>
                                             <tr>
                                                <td><?php echo str_replace('.', ',', $wert3_roundoff); ?></td>
                                             </tr>
                                          <?php //} ?>

                                          <?php //if ($value->verlaufskontrolle_laborwerte->wert4) { 
                                             $wert4_roundoff = $value->verlaufskontrolle_laborwerte->wert4;
                                          ?>
                                             <tr>
                                                <td><?php echo str_replace('.', ',', $wert4_roundoff); ?></td>
                                             </tr>
                                          <?php //} ?>
                                       <?php } else { ?>
                                          <tr>
                                             <td></td>
                                          </tr>
                                       <?php } ?>
                                    </table>
                                 </td>
                                 <td class="address1"><?php echo $value->begleitmedikation; ?></td>
                                 <td class="address1"><?php echo $value->nebenwirkung; ?></td>
                              </tr>
                           <?php } ?>
                           <?php } else { ?>
                              <tr class="table-rows filled-row">
                                 <td class="no-cell">1</td>
                                 <td class="no-cell">
                                    <input type="text" name="visit[0][datum]" class="datum" value="<?php echo date('d.m.Y'); ?>" readonly>
                                    <input type="hidden" name="visit[0][visit]" class="datum" value="1">
                                 </td>
                                 <!-- <td class="no-cell"></td>
                                 <td class="no-cell"></td> -->
                                 <td class="button">
                                    <div>
                                       <?php if($is_exist_patient->is_profile_complete == 'No') { ?>
                                          <a href="<?php echo base_url('patients/anlegen?in='.$insured_number); ?>"><img src="<?php echo base_url('assets/front/images/eidt.jpeg')?>" alt=""></a>
                                       <?php } else { ?>
                                          OK
                                       <?php } ?>
                                    </div>
                                 </td>
                                 <td class="no-cell"><input type="hidden" name="visit[0][nrs_schmerz]" class="datum" value="<?php echo $schmerz; ?>"><?php echo $schmerz; ?></td>
                                 <td class="address1"><input type="hidden" name="visit[0][sf_12_lebensqualitat]" class="datum" value="<?php echo $lebensqualitat; ?>"><?php echo $lebensqualitat; ?></td>
                                 <td class="address1"><input type="hidden" name="visit[0][ris_schlafqualitat]" class="datum" value="<?php echo $schlafqualitat; ?>"><?php echo $schlafqualitat; ?></td>
                                 <td class="sub-tabel">
                                    <table cellspacing="0" cellspacing="0" border="0" width="100%">
                                       <tr>
                                       <td></td>
                                       </tr>
                                    </table>
                                 </td>
                                 <td class="sub-tabel small">
                                    <table cellspacing="0" cellspacing="0" border="0" width="100%">
                                       <tr>
                                       <td></td>
                                       </tr>
                                    </table>
                                 </td>
                                 <td class="address1"></td>
                                 <td class="address1"></td>
                              </tr>
                           <?php } ?>

                        
                        <tr>
                           <td class="no-cell cursor-pointer" onclick="addRow()">+</td>
                           <td class="no-cell">
                              
                           </td>
                           <!-- <td class="no-cell"></td>
                           <td class="no-cell"></td> -->
                           <td class="button">
                              
                           </td>
                           <td class="no-cell"></td>
                           <td class="address1"></td>
                           <td class="address1"></td>
                           <td class="sub-tabel">
                              <table cellspacing="0" cellspacing="0" border="0" width="100%">
                                 
                              </table>
                           </td>
                           <td class="sub-tabel small">
                              <table cellspacing="0" cellspacing="0" border="0" width="100%">
                                 
                              </table>
                           </td>
                           <td class="address1"></td>
                           <td class="address1"></td>
                        </tr>
                        </tbody>
                     </table>
                     <div class="rotate-border"></div>
                  </div>
                  <!-- <img src="images/visitenplan-für-schmerzindikationen.jpeg" alt="" width="100%"> -->
               </div>
              </div>
              <div class="btn-mar-top text-right">
                 <button class="btn btn-primary" type="submit"><span>Speichern</span></button>
              </div>
            </form>
         </div>
      </section>
<script type="text/javascript">
	var row_counter = '<?php echo ($patient_visits)?0:1; ?>';
  $(function(){
    console.log('<?php echo date('Y-m-d'); ?>');
    initDatepicker();
  });
  function addRow(){
    var total_row = $('.table-rows').length;
    var insured_number = "<?php echo $insured_number; ?>";
    // alert(total_row);
    $.ajax({
      url: "<?php echo base_url('process_control/add_visiteplan_row'); ?>",
      type: 'post',
      dataType: 'json',
      data: {row_number:total_row, insured_number:insured_number, row_counter:row_counter },
      success: function(data) {

      	if (data.success) {

	      	row_counter = parseInt(row_counter) + 1;
	        if (total_row < 1) {
	          $('.append-new-row').prepend(data.html);
	        } else {
	          $('.append-new-row .filled-row:last').after(data.html);
	        }

	        initDatepicker();
      	} else {
      		checkTosterResponse(data);
      	}
      }
    })
  }

  function initDatepicker(){
    /*var date = new Date();
    // var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    date.setDate(date.getDate());
    $('.datum').datepicker({
      endDate: date,
      todayHighlight : true,
      autoclose: true,
      format: "dd.mm.yyyy",
    });*/
  }
</script>