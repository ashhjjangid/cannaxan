<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
 <section class="same-section same-blog auswertung">
    <div class="container-fluid">
            <div class="same-heading table-heading">
               <h2 class="text-left">Auswertung Laborwerte</h2>
            </div>
            <div class="chart-schmerz text-center">
               <div class="row">
                  <div class="col-md-6">
                     <div class="chart-schmerz chart-bx visit-txt">
                        <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
                        <div id="chart_container_1"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="chart-schmerz chart-bx visit-txt">
                        <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
                        <div id="chart_container_2"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="chart-schmerz chart-bx visit-txt">
                        <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
                        <div id="chart_container_3"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="chart-schmerz chart-bx visit-txt">
                        <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
                        <div id="chart_container_4"></div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </section>
<script type="text/javascript">
  Highcharts.chart('chart_container_1', {
  chart: {
    zoomType: 'xy'
  },
  title: {
    text: 'AST, GOT (Aspertat-Aminotransferase)'
  },
  subtitle: {
    text: ''
  },
  xAxis: [{
    categories: <?php echo $series_1; ?>,
    crosshair: true
  }],
  yAxis: [{ // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: "#BABDCB"
      }
    },
    title: {
      text: 'AST, GOT',
      style: {
        color: "#BABDCB"
      }
    }
  }, { // Secondary yAxis
    title: {
      text: 'Dosis [mg THC]',
      style: {
        color: "#9E2B5F"
      }
    },
    labels: {
      format: '{value}',
      style: {
        color: "#9E2B5F"
      }
    },
    opposite: true
  }],
  tooltip: {
    shared: true
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 120,
    verticalAlign: 'top',
    y: 100,
    floating: true,
    backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || // theme
      'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'AST, GOT',
    type: 'column',
    color: "#BABDCB",
    data: <?php echo $column_1; ?>,
    tooltip: {
      valueSuffix: ''
    }

  }, {
    name: 'Dosis [mg THC]',
    type: 'spline',
    yAxis: 1,
    color: "#9E2B5F",
    data: <?php echo $line_1; ?>,
    tooltip: {
      valueSuffix: ''
    }
  }],
  exporting: {
	    buttons: {
	        contextButton: {
	            menuItems: [
	                'printChart',
	                'separator',
	                'downloadPNG',
	                'downloadJPEG',
	                'downloadPDF',
	                'downloadSVG'
	            ]
	        }
	    }
	}
});
</script>
<script type="text/javascript">
  Highcharts.chart('chart_container_2', {
  chart: {
    zoomType: 'xy'
  },
  title: {
    text: 'ALT, GPT (Alanin-Aminotransferase)'
  },
  subtitle: {
    text: ''
  },
  xAxis: [{
    categories: <?php echo $series_2; ?>,
    crosshair: true
  }],
  yAxis: [{ // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: "#BABDCB"
      }
    },
    title: {
      text: 'ALT, GPT',
      style: {
        color: "#BABDCB"
      }
    }
  }, { // Secondary yAxis
    title: {
      text: 'Dosis [mg THC]',
      style: {
        color: "#9E2B5F"
      }
    },
    labels: {
      format: '{value}',
      style: {
        color: "#9E2B5F"
      }
    },
    opposite: true
  }],
  tooltip: {
    shared: true
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 120,
    verticalAlign: 'top',
    y: 100,
    floating: true,
    backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || // theme
      'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'ALT, GPT',
    type: 'column',
    color: "#BABDCB",
    data: <?php echo $column_2; ?>,
    tooltip: {
      valueSuffix: ''
    }

  }, {
    name: 'Dosis [mg THC]',
    type: 'spline',
    yAxis: 1,
    color: "#9E2B5F",
    data: <?php echo $line_2; ?>,
    tooltip: {
      valueSuffix: ''
    }
  }],
  exporting: {
	    buttons: {
	        contextButton: {
	            menuItems: [
	                'printChart',
	                'separator',
	                'downloadPNG',
	                'downloadJPEG',
	                'downloadPDF',
	                'downloadSVG'
	            ]
	        }
	    }
	}
});
</script>
<script type="text/javascript">
  Highcharts.chart('chart_container_3', {
  chart: {
    zoomType: 'xy'
  },
  title: {
    text: 'AP (Alkalische Phosphatase)'
  },
  subtitle: {
    text: ''
  },
  xAxis: [{
    categories: <?php echo $series_3; ?>,
    crosshair: true
  }],
  yAxis: [{ // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: "#BABDCB"
      }
    },
    title: {
      text: 'AP',
      style: {
        color: "#BABDCB"
      }
    }
  }, { // Secondary yAxis
    title: {
      text: 'Dosis [mg THC]',
      style: {
        color: "#9E2B5F"
      }
    },
    labels: {
      format: '{value}',
      style: {
        color: "#9E2B5F"
      }
    },
    opposite: true
  }],
  tooltip: {
    shared: true
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 120,
    verticalAlign: 'top',
    y: 100,
    floating: true,
    backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || // theme
      'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'AP',
    type: 'column',
    color: "#BABDCB",
    data: <?php echo $column_3; ?>,
    tooltip: {
      valueSuffix: ''
    }

  }, {
    name: 'Dosis [mg THC]',
    type: 'spline',
    yAxis: 1,
    color: "#9E2B5F",
    data: <?php echo $line_3; ?>,
    tooltip: {
      valueSuffix: ''
    }
  }],
  exporting: {
	    buttons: {
	        contextButton: {
	            menuItems: [
	                'printChart',
	                'separator',
	                'downloadPNG',
	                'downloadJPEG',
	                'downloadPDF',
	                'downloadSVG'
	            ]
	        }
	    }
	}
});
</script>
<script type="text/javascript">
  Highcharts.chart('chart_container_4', {
  chart: {
    zoomType: 'xy'
  },
  title: {
    text: 'Bilirubin, gesamt'
  },
  subtitle: {
    text: ''
  },
  xAxis: [{
    categories: <?php echo $series_4; ?>,
    crosshair: true
  }],
  yAxis: [{ // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: "#BABDCB"
      }
    },
    title: {
      text: 'Bilirubin, gesamt',
      style: {
        color: "#BABDCB"
      }
    }
  }, { // Secondary yAxis
    title: {
      text: 'Dosis [mg THC]',
      style: {
        color: "#9E2B5F"
      }
    },
    labels: {
      format: '{value}',
      style: {
        color: "#9E2B5F"
      }
    },
    opposite: true
  }],
  tooltip: {
    shared: true
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 120,
    verticalAlign: 'top',
    y: 100,
    floating: true,
    backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || // theme
      'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'Bilirubin, gesamt',
    type: 'column',
    color: "#BABDCB",
    data: <?php echo $column_4; ?>,
    tooltip: {
      valueSuffix: ''
    }

  }, {
    name: 'Dosis [mg THC]',
    type: 'spline',
    yAxis: 1,
    color: "#9E2B5F",
    data: <?php echo $line_4; ?>,
    tooltip: {
      valueSuffix: ''
    }
  }],
  exporting: {
	    buttons: {
	        contextButton: {
	            menuItems: [
	                'printChart',
	                'separator',
	                'downloadPNG',
	                'downloadJPEG',
	                'downloadPDF',
	                'downloadSVG'
	            ]
	        }
	    }
	}
});
</script>
