<section class="same-section begleiterkrankungen-section">
         <div class="container-fluid">
            <div class="same-heading ">
              <h2 class="text-left">Nebenwirkung Auswertung</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Datum</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="29.02.2020">
                        </div>
                     </div>
                  </div>
               </div>
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Behandungstag</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="21">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4"></div>
            </div>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Visite</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="3">
                        </div>
                     </div>
                  </div>
               </div>
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Arzneimittel</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="CannaXan-THC">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4"></div>
               </div>
               <div class="col-md-12">
                <div class="row">
                  <div class="col-md-9">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-8">
                          <label>Keine kausal bedingte Nebenwirkung</label>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="" class="form-control text-center" placeholder="nein">
                          </div>
                       </div>
                    </div>
                    </div>
                    <div class="col-md-3"></div>
                  </div>
                </div>
               </div>
            </div>

            <div class="container">
              <div class="tabel-overlap">
                <table cellspacing="0"  cellpadding="0" width="100%" class="table table-striped table-bordered">
                    <tr>
                      <th>Nebenwirkung</th>
                      <th colspan="6" class="p-0">
                       <table cellpadding="0" cellspacing="0" width="100%" align="center">
                          <tr>
                             <th colspan="6"> <center>Wochen</center></th>
                          </tr>
                          <tr>
                             <th style="">0</th>
                             <th style="">2</th>
                             <th style="">12</th>
                             <th style="">24</th>
                             <th style="">36</th>
                             <th style="">48</th>
                          </tr>
                       </table></th>
                      <th style="text-align: center;">KAUSALITÄT ZUR<br> THERAPIE VORHANDEN</th>
                   </tr>
                   <tr>
                      <td>Ubelkeit</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center; "><span style="background:#F76816; display:block; height:20px;"></span></td>
                      <td style="text-align: center;"><span style="background:#FCAF16; display:block; height:20px;"></span></td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                   <tr>
                      <td>Erbrechen</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center; ">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                   <tr>
                      <td>Ubelkeit</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center; "><span style="background:#F76816; display:block; height:20px;"></span></td>
                      <td style="text-align: center;"><span style="background:#FCAF16; display:block; height:20px;"></span></td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                   <tr>
                      <td>Erbrechen</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center; ">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                    <tr>
                      <td>Erbrechen</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center; ">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                    <tr>
                      <td>Erbrechen</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;"><span style="background:#2CA61E; display:block; height:20px;"></span></td>
                      <td style="text-align: center; ">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">-</td>
                      <td style="text-align: center;">
                       <select>
                          <option>ja</option>
                          <option>ja</option>
                       </select>
                      </td>
                   </tr>
                   
                </table>
             </div> 
           <div class="tabel-view-box text-right">
             <ul>
               <li><span style="background: #2CA61E;"></span>mild</li>
               <li><span style="background:#FCAF16; "></span>moderat</li>
               <li><span style="background: #F76816;"></span>schwer</li>
               <li><span style="background:#D91313 "></span>lebensbedrohlich</li>
             </ul>
           </div>
            </div>
         </div>
      </section>