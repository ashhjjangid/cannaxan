<!DOCTYPE html>
  <html lang="en">
    <head>
    <title>Comming Soon: Welcome</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- <link rel="shortcut icon" type="img/png" href="<?php echo base_url('assets/front/images/favicon.ico')?>images/favicon.ico"/> -->
    <link rel="shortcut icon" type="img/png" href="<?php echo base_url('assets/front/images/favicon.png'); ?>"/> 

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css/bootstrap.min.css'); ?>"/>    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css/style.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css/media.css'); ?>"/>
    </head>
    <body class="comming-soon-body">
      <div class="fullwraper">
        <div class="fullwraper header">
            <div class="logo"><img src="<?php echo base_url('assets/front/images/logo.png')?>"></div>
            <div class="comming-soon-txt-h">Coming Soon <small>Frill decor coming up with lots of amazing luxury products for special homes. Frill's each product will be identical and unique to serene your home. We will surely fit with all your home furnishing needs for home, office, beach houses, clubs, lounge, Hotel needs. Our range of products will try to fulfill the needs through:</small></div>
        </div>
      

        <div class="fullwraper mid-conct">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 bdr-ri">
                        <div class="date-time" id="clock">
                            <div class="days-rw"><strong class="days">125</strong><span>/ Days</span></div>
                            <ul class="time-rw">
                                <li>
                                    <h3 class="hours">07</h3>
                                    <p>Hours</p>
                                </li>
                                <li>
                                    <h3 class="minutes">15</h3>
                                    <p>Minutes</p>
                                </li>
                                <li>
                                    <h3 class="seconds">33</h3>
                                    <p>Seconds</p>
                                </li>
                            </ul>
                            <div class="date-time-txt">Join the Beta Program</div>


                            <div class="clearfix"></div>
                        </div>

                    </div>
                    <div class="col-sm-6 bdr-le">

                        <div class="subscribe-rw">
                            <h3>We’re currently building Frill. Stay in the loop to receive updates as beta users are invited to start the testing fun.</h3>

                            <ul class="subscribe-frm">
                                <form id="subscribe-form" action="<?php echo base_url('subscribe'); ?>" class="ajax_form" method="post">
                                    <li>
                                        <input type="text" placeholder="Name" name="name" class="subscribe-fild">
                                    </li>
                                    <li>
                                        <input type="text" placeholder="Email" name="email" class="subscribe-fild">
                                    </li>
                                    <li>
                                        <input type="submit" value="Subscribe Now" class="subscribe-btn">
                                    </li>
                                </form>
                            </ul>

                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="ftr-copyright comming-soon-ftr fullwraper clearfix text-center">
          <div class="container">
            <p>Copyright 2019 © Frill, Inc. All Rights Reserved</p>
          </div>
        </div>
      </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/front/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/front/js/jquery.countdown.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/front/js/jquery.form.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/front/js/common.js'); ?>"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

    <script type="text/javascript">
        $(function(){
            $('#clock').countdown("2020/1/1", function(event) {
              //var totalHours = event.offset.totalDays * 24 + event.offset.hours;
              var totalDays = event.offset.totalDays;
               $('.days').html(totalDays);
               $('.hours').html(event.strftime('%H'));
               $('.minutes').html(event.strftime('%M'));
               $('.seconds').html(event.strftime('%S'));
             });    
        });
    </script>
    </body>
  </html>