<section class="same-section same-blog auswertung">
         <div class="container-fluid">
            <div class="same-heading table-heading">
               <h2 class="text-left">Auswertung Laborwerte</h2>
            </div>
            <div class="chart-schmerz text-center">
               <div class="row">
                  <div class="col-md-6">
                     <div class="images-chart">
                        <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
                        <div id="chart_container_1"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="images-chart">
                        <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
                        <div id="chart_container_2"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="images-chart">
                        <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
                        <div id="chart_container_3"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="images-chart">
                        <!-- <img src="<?php echo $this->config->item('front_assets'); ?>images/schmerz.png" alt=""> -->
                        <div id="chart_container_4"></div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </section>
    <script type="text/javascript">
   $(document).ready(function() {

      graphByPatientData();
   });

   function graphByPatientData() {
      /*var chart = Highcharts.chart('container', {

        title: {
          text: 'Patient Data'
        },

        subtitle: {
          text: 'Schmer'
        },

        xAxis: {
          categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },

        series: [{
          type: 'column',
          colorByPoint: false,
          data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
          showInLegend: false
        }]

      });


      $('#plain').click(function() {
        chart.update({
          chart: {
            inverted: false,
            polar: false
          },
          subtitle: {
            text: 'Plain'
          }
        });
      });*/
   
   $("#chart_container_1").insertFusionCharts({
  type: "msstackedcolumn2dlinedy",
  width: "100%",
  height: "100%",
  dataFormat: "json",
  dataSource: {
    chart: {
      drawAnchors: "0",
      caption: "Patient Dosis Data",
      subcaption: "Week 1",
      pyaxisname: "Units",
      syaxisname: " of total dosis THC",
      snumbersuffix: "mg",
      syaxismaxvalue: "25",
      theme: "fusion",
      showvalues: "0",
      drawcrossline: "1",
      divlinealpha: "20"
    },
    categories: [
      {
        category: [
          {
            label: "1"
          },
          {
            label: "2"
          },
          {
            label: "3"
          },
          {
            label: "4"
          },
          {
            label: "5"
          },
          {
            label: "6"
          }
        ]
      }
    ],
    dataset: [
      {
        dataset: [
          {
            seriesname: "Dosis THC",
            data: [
              {
                value: "2"
              },
              {
                value: "4"
              },
              {
                value: "3"
              },
              {
                value: "1"
              },
              {
                value: "3"
              },
              {
                value: "9"
              }
            ]
          }
        ]
      }
    ],
    lineset: [
      {
        seriesname: "Dosis THC %",
        plottooltext: "Dosis THC $label is <b>$dataValue</b>",
        showvalues: "0",
        data: [
          {
            value: "17.74"
          },
          {
            value: "19.23"
          },
          {
            value: "15.43"
          },
          {
            value: "12.34"
          },
          {
            value: "15.34"
          },
          {
            value: "21.17"
          }
        ]
      }
    ]
  }
});
    $("#chart_container_2").insertFusionCharts({
  type: "msstackedcolumn2dlinedy",
  width: "100%",
  height: "100%",
  dataFormat: "json",
  dataSource: {
    chart: {
      drawAnchors: "0",
      caption: "Patient Dosis Data",
      subcaption: "Week 1",
      pyaxisname: "Units",
      syaxisname: " of total dosis THC",
      snumbersuffix: "mg",
      syaxismaxvalue: "25",
      theme: "fusion",
      showvalues: "0",
      drawcrossline: "1",
      divlinealpha: "20"
    },
    categories: [
      {
        category: [
          {
            label: "1"
          },
          {
            label: "2"
          },
          {
            label: "3"
          },
          {
            label: "4"
          },
          {
            label: "5"
          },
          {
            label: "6"
          }
        ]
      }
    ],
    dataset: [
      {
        dataset: [
          {
            seriesname: "Dosis THC",
            data: [
              {
                value: "2"
              },
              {
                value: "4"
              },
              {
                value: "3"
              },
              {
                value: "1"
              },
              {
                value: "3"
              },
              {
                value: "9"
              }
            ]
          }
        ]
      }
    ],
    lineset: [
      {
        seriesname: "Dosis THC %",
        plottooltext: "Dosis THC $label is <b>$dataValue</b>",
        showvalues: "0",
        data: [
          {
            value: "17.74"
          },
          {
            value: "19.23"
          },
          {
            value: "15.43"
          },
          {
            value: "12.34"
          },
          {
            value: "15.34"
          },
          {
            value: "21.17"
          }
        ]
      }
    ]
  }
});
        $("#chart_container_3").insertFusionCharts({
  type: "msstackedcolumn2dlinedy",
  width: "100%",
  height: "100%",
  dataFormat: "json",
  dataSource: {
    chart: {
      drawAnchors: "0",
      caption: "Patient Dosis Data",
      subcaption: "Week 1",
      pyaxisname: "Units",
      syaxisname: " of total dosis THC",
      snumbersuffix: "mg",
      syaxismaxvalue: "25",
      theme: "fusion",
      showvalues: "0",
      drawcrossline: "1",
      divlinealpha: "20"
    },
    categories: [
      {
        category: [
          {
            label: "1"
          },
          {
            label: "2"
          },
          {
            label: "3"
          },
          {
            label: "4"
          },
          {
            label: "5"
          },
          {
            label: "6"
          }
        ]
      }
    ],
    dataset: [
      {
        dataset: [
          {
            seriesname: "Dosis THC",
            data: [
              {
                value: "2"
              },
              {
                value: "4"
              },
              {
                value: "3"
              },
              {
                value: "1"
              },
              {
                value: "3"
              },
              {
                value: "9"
              }
            ]
          }
        ]
      }
    ],
    lineset: [
      {
        seriesname: "Dosis THC %",
        plottooltext: "Dosis THC $label is <b>$dataValue</b>",
        showvalues: "0",
        data: [
          {
            value: "17.74"
          },
          {
            value: "19.23"
          },
          {
            value: "15.43"
          },
          {
            value: "12.34"
          },
          {
            value: "15.34"
          },
          {
            value: "21.17"
          }
        ]
      }
    ]
  }
});
   $("#chart_container_4").insertFusionCharts({
  type: "msstackedcolumn2dlinedy",
  width: "100%",
  height: "100%",
  dataFormat: "json",
  dataSource: {
    chart: {
      drawAnchors: "0",
      caption: "Patient Dosis Data",
      subcaption: "Week 1",
      pyaxisname: "Units",
      syaxisname: " of total dosis THC",
      snumbersuffix: "mg",
      syaxismaxvalue: "25",
      theme: "fusion",
      showvalues: "0",
      drawcrossline: "1",
      divlinealpha: "20"
    },
    categories: [
      {
        category: [
          {
            label: "1"
          },
          {
            label: "2"
          },
          {
            label: "3"
          },
          {
            label: "4"
          },
          {
            label: "5"
          },
          {
            label: "6"
          }
        ]
      }
    ],
    dataset: [
      {
        dataset: [
          {
            seriesname: "Dosis THC",
            data: [
              {
                value: "2"
              },
              {
                value: "4"
              },
              {
                value: "3"
              },
              {
                value: "1"
              },
              {
                value: "3"
              },
              {
                value: "9"
              }
            ]
          }
        ]
      }
    ],
    lineset: [
      {
        seriesname: "Dosis THC %",
        plottooltext: "Dosis THC $label is <b>$dataValue</b>",
        showvalues: "0",
        data: [
          {
            value: "17.74"
          },
          {
            value: "19.23"
          },
          {
            value: "15.43"
          },
          {
            value: "12.34"
          },
          {
            value: "15.34"
          },
          {
            value: "21.17"
          }
        ]
      }
    ]
  }
});

}

</script>