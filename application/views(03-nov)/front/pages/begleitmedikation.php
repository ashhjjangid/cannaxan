<section class="same-section same-blog all-tabel-event">
         <div class="container-fluid">
            <div class="same-heading table-heading">
               <h2 class="text-left">Begleitmedikation</h2>
               <p >Tragen Sie bitte alle begleitende Medikamente ein, die mit Beginn und während der CannaXan Behandlung eingenommen wurden.</p><p> Wurde die Dosierung einer Begleitmedikation geändert, dann tragen Sie bitte die neue Dosierung in eine neue Zeile ein und </p>
            </div>
            <div class="tabel-event">
               <h2>Drücken Sie auf Plus, um einen weiteren Zeile hinzuzufügen!</h2>
               <table class="table">
                 <thead>
                   <tr>
                     <th>Nr.</th>
                     <th>Wirksoff</th>
                     <th>Handelsname</th>
                     <th>Starke</th>
                     <th>Einheit</th>
                     <th>form</th>
                     <th>morgens</th>
                     <th>Mittags</th>
                     <th>abends</th>
                     <th>zur Nacht</th>
                     <th>Einheit</th>
                     <th>Tagesdosis</th>
                     <th>Grund</th>
                     <th>Startdatum</th>
                     <th>Startdatum</th>
                   </tr>
                 </thead>
                 <tbody>
                   <tr class="table-row-length">
                     <th>1</th>
                     <td>Pregabalin</td>
                     <td>Lyrica</td>
                     <td>50</td>
                     <td>
                        <div class="select-box">
                           <select class="form-control">
                              <option>mg</option>
                              <option>mg</option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div>
                     </td>
                     <td>
                        <div class="select-box">
                           <select class="form-control">
                              <option>Tabl</option>
                              <option>Tabl</option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div>
                     </td>
                     <td>1</td>
                     <td>0</td>
                     <td>1</td>
                     <td>0</td>
                     <td>Stuck</td>
                     <td>2</td>
                     <td>
                        <div class="select-box">
                           <select class="form-control">
                              <option>neuopath. Schmerz</option>
                              <option>neuopath. Schmerz</option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div>
                     </td>
                     <td>01.01.2018</td>
                     <td>01.01.2018</td>
                   </tr>
                   <tr class="table-row-length">
                     <th class="blue-color">2</th>
                     <td class="blue-color">Pregabalin</td>
                     <td class="blue-color">Lyrica</td>
                     <td class="blue-color">50</td>
                     <td class="blue-color">
                        <div class="select-box">
                           <select class="form-control blue-color">
                              <option>mg</option>
                              <option>mg</option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div>
                     </td>
                     <td class="blue-color">
                        <div class="select-box">
                           <select class="form-control blue-color">
                              <option>Tabl</option>
                              <option>Tabl</option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div>
                     </td>
                     <td class="blue-color">1</td>
                     <td class="blue-color">0</td>
                     <td class="blue-color">1</td>
                     <td class="blue-color">0</td>
                     <td class="blue-color">Stuck</td>
                     <td class="blue-color">2</td>
                     <td>
                        <div class="select-box">
                           <select class="form-control blue-color">
                              <option>neuopath. Schmerz</option>
                              <option>neuopath. Schmerz</option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div>
                     </td>
                     <td class="blue-color">01.01.2018</td>
                     <td class="blue-color">01.01.2018</td>
                   </tr>
                   <tr class="table-row-length">
                     <th>+</th>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td>
                        <div class="select-box">
                           <select class="form-control">
                              <option></option>
                              <option></option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div>
                     </td>
                     <td>
                        <div class="select-box">
                           <select class="form-control">
                              <option></option>
                              <option></option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div>
                     </td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td>
                        <div class="select-box">
                           <select class="form-control">
                              <option></option>
                              <option></option>
                           </select>
                           <span><i class="fas fa-chevron-down"></i></span>
                        </div>
                     </td>
                     <td></td>
                     <td></td>
                   </tr>
                 </tbody>
               </table>
               <div class="form-group btn-mar-top text-right">
                        <a href="<?php echo base_url('patients/begleitmedikation_verlauf');?>"class="btn btn-info"><span>Speichern &amp; weiter</span></a>
                     </div>
                  </div>
            </div>
         </div>
      </section>