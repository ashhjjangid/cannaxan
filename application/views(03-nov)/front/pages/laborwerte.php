 <section class="same-section begleiterkrankungen-section">
         <div class="container-fluid">
            <div class="same-heading ">
              <h2 class="text-left">Laborwerte</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Datum</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="29.02.2020">
                        </div>
                     </div>
                  </div>
               </div>
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Behandungstag</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="21">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4"></div>
            </div>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Visite</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="3">
                        </div>
                     </div>
                  </div>
               </div>
                <div class="col-md-4">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Arzneimittel</label>
                        </div>
                        <div class="col-sm-7">
                           <input type="text" name="" class="form-control text-center" placeholder="CannaXan-THC">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4"></div>
               </div>
            </div>

            <div class="begleiterkrankungen-blog">
              <div class="form-group">
                <label >Laborparameter</label>
              </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="col-md-12">
                        <div class="form-group">
                           <div class="row align-items-center">
                              <div class="col-sm-2">
                                 <label class="text-right">1.</label>
                              </div>
                              <div class="col-sm-10">
                                 <input type="text" name="" class="form-control" placeholder="AST, GOT (Aspertat-Aminotransferase)" readonly value="AST, GOT (Aspertat-Aminotransferase)">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <div class="row align-items-center">
                              <div class="col-sm-2">
                                 <label class="text-right">2.</label>
                              </div>
                              <div class="col-sm-10">
                                 <input type="text" name="" class="form-control" placeholder="ALT, GPT (Alanin-Aminotransferase)" value="ALT, GPT (Alanin-Aminotransferase)" readonly>
                              </div>
                           </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="row align-items-center">
                            <div class="col-sm-2">
                               <label class="text-right">3.</label>
                            </div>
                            <div class="col-sm-10">
                               <input type="text" name="" class="form-control" placeholder="AP (Alkalische Phosphatase)">
                            </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                          <div class="row align-items-center">
                            <div class="col-sm-2">
                               <label class="text-right">4.</label>
                            </div>
                            <div class="col-sm-10">
                               <input type="text" name="" class="form-control" placeholder="Bilirubin, gesamt" value="Bilirubin, gesamt" readonly>
                            </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Wert</label>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="" class="form-control text-center" placeholder="127">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="" class="form-control text-center" placeholder="28">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="" class="form-control text-center" placeholder="7">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="" class="form-control text-center" placeholder="40">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Einheit</label>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="select-box">
                                <select class="form-control">
                                   <option>µmol</option>
                                   <option>µmol</option>
                                </select>
                                <span><i class="fas fa-chevron-down"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="select-box">
                                <select class="form-control">
                                   <option>mg</option>
                                   <option>mg</option>
                                </select>
                                <span><i class="fas fa-chevron-down"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="select-box">
                                <select class="form-control">
                                   <option>L</option>
                                   <option>L</option>
                                </select>
                                <span><i class="fas fa-chevron-down"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="select-box">
                                <select class="form-control">
                                   <option>mg</option>
                                   <option>dL</option>
                                   <option>µmol</option>
                                   <option>L</option>
                                </select>
                                <span><i class="fas fa-chevron-down"></i></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Normbereich</label>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="" class="form-control text-center" placeholder="80 - 200">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="" class="form-control text-center" placeholder="15 - 30">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="" class="form-control text-center" placeholder="5 - 10">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" name="" class="form-control text-center" placeholder="55 - 70">
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="col-md-12 text-right">
                     <div class="form-group btn-mar-top">
                        <a href="<?php echo base_url('Patients/laborwerte_auswertung');?>" class="btn btn-info"><span>Speichern & weiter</span></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>