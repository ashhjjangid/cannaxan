      <section class="same-section">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-4 align-self-center">
                           <label class="mb-0">Anrede</label>
                        </div>
                        <div class="col-sm-5">
                           <div class="select-box">
                              <select class="form-control">
                                 <option>Herr</option>
                                 <option>Herr</option>
                              </select>
                              <span><i class="fas fa-chevron-down"></i></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-4 align-self-center">
                           <label class="mb-0">Name Arztpraxis</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="" class="form-control" placeholder="Onkel Doktor um die Ecke">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-4 align-self-center">
                           <label class="mb-0">Name</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="" class="form-control" placeholder="Muster">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-4 align-self-center">
                           <label class="mb-0">Fachrichtung:</label>
                        </div>
                        <div class="col-sm-5">
                           <div class="select-box">
                              <select class="form-control">
                                 <option>Herr</option>
                                 <option>Herr</option>
                              </select>
                              <span><i class="fas fa-chevron-down"></i></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-4 align-self-center">
                           <label class="mb-0">VorName</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="" class="form-control" placeholder="Beispiel">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-4 align-self-center">
                           <label class="mb-0">LANR:</label>
                        </div>
                        <div class="col-sm-5">
                           <input type="text" name="" class="form-control" placeholder="123456789">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-4 align-self-center">
                           <label class="mb-0">Titel</label>
                        </div>
                        <div class="col-sm-5">
                           <div class="select-box">
                              <select class="form-control">
                                 <option>Dr. med</option>
                                 <option>Dr. med</option>
                              </select>
                              <span><i class="fas fa-chevron-down"></i></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="other-form-box">
               <div class="row">
                   <div class="col-md-6">
                     <div class="form-group">
                        <div class="row align-items-end">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">Straße</label>
                           </div>
                           <div class="col-sm-5">
                              <input type="text" name="" class="form-control" placeholder="Musterstraße">
                           </div>
                           <div class="col-md-3">
                              <label class="mb-2">Hausnr.</label>
                              <input type="text" name="" class="form-control">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group mt-4">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">E-Mail</label>
                           </div>
                           <div class="col-sm-5">
                              <input type="text" name="" class="form-control" placeholder="arzt@onkeldoktor.de">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">PLZl</label>
                           </div>
                           <div class="col-sm-5">
                              <input type="text" name="" class="form-control" placeholder="81828">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">www.</label>
                           </div>
                           <div class="col-sm-5">
                              <input type="text" name="" class="form-control" placeholder="onkel-Doktor.de">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">Ort</label>
                           </div>
                           <div class="col-sm-5">
                              <input type="text" name="" class="form-control" placeholder="München">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">Tel. nr.</label>
                           </div>
                           <div class="col-sm-5">
                              <input type="text" name="" class="form-control" placeholder="089 12345678">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="row">
                           <div class="col-sm-4 align-self-center">
                              <label class="mb-0">Fax</label>
                           </div>
                           <div class="col-sm-5">
                              <input type="text" name="" class="form-control" placeholder="089 12345679">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-5">
                           <div class="checkbox-custom">
                               <input type="checkbox" id="me">
                               <label for="me"><span class="cheak"></span>DatenSpeicherung</label>
                            </div>
                            <a href="willkommen.html" class="btn btn-info"><span>Anmelden</span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>