<section class="same-section same-blog schmerz-blog">
         <div class="container-fluid">
            <div class="same-heading table-heading">
               <h2 class="text-left">Visitenplan für Schmerzindikationen</h2>
            </div>
            <div class="eidt-daten text-right">
               <span class="eidt-icon-text">Daten bearbeiten mit:
                  <span class="eidt-icon"><a href="#"><img src="images/eidt.jpeg" alt=""></a></span>
               </span>
            </div>
            <div class="chart-schmerz text-center">
               <img src="images/visitenplan-für-schmerzindikationen.jpeg" alt="" width="100%">
            </div>
            <div class="btn-mar-top text-right">
              <button class="btn btn-info"><span>Speichern</span></button>
            </div>
         </div>
      </section>