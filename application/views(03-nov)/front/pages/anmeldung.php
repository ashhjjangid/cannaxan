<section class="same-section">
         <div class="container-fluid">
            <div class="row align-items-end">
               <div class="col-md-8">
                  <div class="input-fill-box">
                     <div class="logo-box text-center">
                        <img src="images/logo.png" alt="logo">
                     </div>
                     <div class="row align-items-center">
                        <div class="col-4">
                           <label>Benutzername:</label>
                        </div>
                        <div class="col-8">
                           <input type="`text" name="" placeholder="arzt@onkeldoktor.de"  class="form-control">
                        </div>
                     </div>
                     <div class="row align-items-center">
                        <div class="col-4">
                           <label>password:</label>
                        </div>
                        <div class="col-8">
                           <input type="password" name="" placeholder="123" class="form-control">
                        </div>
                     </div>
                     <div class="foroget-password-cover">
                        <a href="#" class="foroget-password" id="foroget_password">Passwort vergessen?</a>
                        <div id="foroget_password_box" style="display: none;">
                           <h4>Passwort</h4>
                           <p>Bitte geben Sie hier Ihr neues Passwort ein. Zur Gewärung einer besseren Datensicherheit muss es aus minimum acht Zeichen bestehen. Das Passwort muss min. ein Sonderzeichen sowie eine Zahl enthalten.</p>
                           <div class="foroget-password-input">
                              <div class="form-group">
                                 <label>Neues Passwort</label>
                                 <input type="password" name="" class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Passwort Bestätigung</label>
                                 <input type="password" name="" class="form-control">
                              </div>
                              <div class="form-group mb-0">
                                 <button class="btn btn-info"><span>Speichern &amp; weiter</span></button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="custom-btn">
                     <a href="patient-laden.html"class="btn btn-info"><span>Anmelden</span></a>
                  </div>
               </div>
            </div>
         </div>
      </section>