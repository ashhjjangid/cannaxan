<section class="banner-section">
         <div class="container-fluid">
            <div class="banner-img">
               <img src="<?php echo $this->config->item('front_assets'); ?>images/banner-img.png">
            </div>
         </div>
      </section>

      <section class="same-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-8">
                  <div class="same-heading ">
                     <h2>Über CannaXan</h2>
                     <p> CANNAXAN GMBH IST EIN PHARMAZEUTISCHES UNTERNEHMEN MIT INNOVATIVEN CANNABIS REZEPTURARZNEIMITTELN, DIE AUSSCHLIEßLICH ÜBER APOTHEKEN BEZOGEN UND VOM ARZT VERORDNET WERDEN MÜSSEN.</p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="custom-btn custom-btn-full-btn text-center">
                     <a href="<?php echo base_url('doctors/register/step-1')?>" class="btn btn-primary"> <span>Registrierung*</span></a>
                     <a href="<?php echo base_url('doctors/login')?>" class="btn btn-primary"><span>Anmeldung</span></a>
                  </div>
                  <div class="extra-button-line text-center">
                     <p>*Eine Registrierung ist nur für Ärzte möglich</p>
                  </div>
               </div>
            </div>
         </div>
      </section>