<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/dosistypII')?>" class="tooltips" data-original-title="List of DosisTypII" data-placement="top" data-container="body">DosisTypII
        </a>
        <i class="fa fa-arrow-right">
        </i>
        <?php if($records) { ?>
        <?php echo $records->tag_name; ?>
      <?php } else { ?>

        <?php } ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/dosistypII'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
        <?php if(!empty($records)) { ?>
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>DosisTypII
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>Tag Name
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $records->tag_name; ?> 
                    </td>
                  </tr>

                  <tr>
                    <th>Morgens Anzahl Durchgänge
                    </th>
                    <td style="word-break: break-all">
                      <?php echo $records->morgens_anzahl_durchgange; ?>
                    </td>
                  </tr>  
                  <tr>  
                    <th>Mittags Anzahl Durchgänge                     
                    </th>
                    <td>
                      <?php echo $records->mittags_anzahl_durchgange; ?>
                    </td>
                  </tr>                  
                  <tr>  
                    <th>Abends Anzahl Durchgänge   
                    </th>
                    <td>
                      <?php echo $records->abends_anzahl_durchgange; ?>
                    </td>
                  </tr>
                  <tr>  
                    <th>Nachts Anzahl Durchgänge                     
                    </th>
                    <td>
                      <?php echo $records->nachts_anzahl_durchgange; ?>
                    </td>
                  </tr>

                  <tr>  
                    <th>Morgens Anzahl SPS
                    </th>
                    <td>
                      <?php echo $records->morgens_anzahl_sps; ?>
                    </td>
                  </tr>
                  <tr>  
                    <th>Mittags Anzahl SPS         
                    </th>
                    <td>
                      <?php echo $records->mittags_anzahl_sps; ?>
                    </td>
                  </tr>
                  <tr>  
                    <th>Abends Anzahl SPS          
                    </th>
                    <td>
                      <?php echo $records->abends_anzahl_sps; ?>
                    </td>
                  </tr>
                  <tr>  
                    <th>Nachts Anzahl SPS                   
                    </th>
                    <td>
                      <?php echo $records->nachts_anzahl_sps; ?>
                    </td>
                  </tr>

                  <tr>  
                    <th>THC morgens [mg]             
                    </th>
                    <td>
                      <?php echo $records->thc_morgens_mg; ?>
                    </td>
                  </tr>
                  <tr>  
                    <th>THC mittags [mg]
                    </th>
                    <td>
                      <?php echo $records->thc_mittags_mg; ?>
                    </td>
                  </tr>
                  <tr>  
                    <th>THC abends [mg]             
                    </th>
                    <td>
                      <?php echo $records->thc_abends_mg; ?>
                    </td>
                  </tr>
                  <tr>  
                    <th>THC nachts [mg]              
                    </th>
                    <td>
                      <?php echo $records->thc_nachts_mg; ?>
                    </td>
                  </tr>


                  <tr>  
                    <th>Mittags Anzahl Durchgänge                     
                    </th>
                    <td>
                      <?php echo $records->mittags_anzahl_durchgange; ?>
                    </td>
                  </tr>
                  <tr>  
                    <th>Summe THC [mg]               
                    </th>
                    <td>
                      <?php echo $records->summe_thc_mg; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Status
                    </th>
                      <td style="word-break: break-all;">
                        <?php if ($records->status == 'Active') { ?>
                          <button class="btn btn-xs btn-success status_label" type="button">Active</button>
                        <?php } else { ?>
                          <button class="btn btn-xs btn-danger stats_label" type="button">Inactive</button>
                        <?php } ?>
                      </td>
                  </tr>                  
                  <tr>
                    <th>Add Date
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo date('M d, Y h:i:s A', strtotime(getGMTDateToLocalDate($records->add_date)));?>
                    </td>
                  </tr>
                </table>
              </section>              
            </div>
          </div>
        <?php } else { ?>
          <div class="alert alert-info">No record found</div>
        <?php } ?>
        </div>
      </div>
   </div>
</div>
