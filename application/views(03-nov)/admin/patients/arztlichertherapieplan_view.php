<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/view_arztlichertherapieplan')?>" class="tooltips" data-original-title="List of Patients" data-placement="top" data-container="body"> List of Patients
        </a>
        <i class="fa fa-arrow-right">
        </i>
        <?php if($arztlicher_therapieplan)
        {
          echo $arztlicher_therapieplan->rezepturarzneimittel;
        } else 
        {

        } ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/Patients'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Arztlicher Therapieplan Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
                <?php if ($arztlicher_therapieplan) { ?>
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>
                      Hauptindikation
                    </th>
                    <td>
                      <?php echo $hauptindikation_name; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Rezepturarzneimittel
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $arztlicher_therapieplan->rezepturarzneimittel; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Wirkstoff
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $arztlicher_therapieplan->wirkstoff; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>DosisTyp
                    </th>
                    <td style="word-break: break-all;">
                      <?php if ($arztlicher_therapieplan->dosistyp == 'dosistyp_1') {
                        echo "DosisTyp 1"; 
                        } else {
                          echo "DosisTyp 2"; 
                        } ?>
                    </td>
                  </tr>
                </table>

                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <th>Tag name</th>              
                    <th>Morgens Anzahl Durchgänge</th>              
                    <th>Morgens Anzahl SPS</th>
                    <th>Mittags Anzahl Durchgänge</th>
                    <th>mittags anzahl sps</th>              
                    <th>abends anzahl durchgange</th>
                    <th>abends anzahl sps</th>              
                    <th>nachts anzahl durchgange</th>
                    <th>nachts anzahl sps</th>
                    <th>thc morgens [mg]</th>
                    <th>thc mittags [mg]</th>
                    <th>thc abends [mg]</th>              
                    <th>thc nachts [mg]</th>
                    <th>summe thc [mg]</th>
                  </thead>
                  <tbody>
                  <?php if ($patients_dosistyp_data) { ?>
                    

                    <?php foreach ($patients_dosistyp_data as $key => $value) { ?>
                      
                    <tr>
                      <td><?php echo $value->tag_name;?></td>
                      <td><?php echo $value->morgens_anzahl_durchgange;?></td>
                      <td><?php echo $value->morgens_anzahl_sps;?></td>
                      <td><?php echo $value->mittags_anzahl_durchgange;?></td>
                      <td><?php echo $value->mittags_anzahl_sps;?></td>
                      <td><?php echo $value->abends_anzahl_durchgange;?></td>
                      <td><?php echo $value->abends_anzahl_sps;?></td>
                      <td><?php echo $value->nachts_anzahl_durchgange;?></td>
                      <td><?php echo $value->nachts_anzahl_sps;?></td>
                      <td><?php echo $value->thc_morgens_mg;?></td>
                      <td><?php echo $value->thc_mittags_mg;?></td>
                      <td><?php echo $value->thc_abends_mg;?></td>
                      <td><?php echo $value->thc_nachts_mg;?></td>
                      <td><?php echo $value->summe_thc_mg;?></td>
                    </tr>

                <?php } ?>
              <?php } } else { ?>
                  <tr>
                    <td colspan="9"><div class="no-record">No record found</div></td>
                  </tr>
                  <?php }  ?>
                  </tbody>
                </table>
              </section>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

