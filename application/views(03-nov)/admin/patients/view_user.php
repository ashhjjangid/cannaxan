<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/Patients')?>" class="tooltips" data-original-title="List of Patients" data-placement="top" data-container="body"> List of Patients
        </a>
        <i class="fa fa-arrow-right">
        </i>
          <?php echo $usersdata->first_name; ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/Patients'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Patients Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>
                      Krankenvdersicherungsnummer
                    </th>
                    <td>
                      <?php echo $usersdata->insured_number; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Fallnummer
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->case_number; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Name
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->first_name. ' '. $usersdata->last_name; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Telephone Number
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->telephone_number ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Diagnosecode
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->diagnostic_code;?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Geburtsdatum
                    </th>
                    <td>
                      <?php echo date('d M Y', strtotime($usersdata->date_of_birth)); ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Geschlecht
                    </th>
                    <td>
                      <?php echo $usersdata->gender; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Gewicht
                    </th>
                    <td>
                      <?php echo $usersdata->weight; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Größe
                    </th>
                    <td>
                      <?php echo $usersdata->size; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Hauptanzeige
                    </th>
                    <td>
                      <?php echo $usersdata->main_indication; ?>
                    </td>
                  </tr>
                  <!-- <tr>
                    <th>
                      Nicht zuordenbare Hauptindikation
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $usersdata->main_indication_not_assigned; ?>
                    </td>
                  </tr> -->
                  <tr>
                    <th>
                      Moderate bis schwere Schmerzen
                    </th>
                    <td>
                      <?php echo $usersdata->pain_scale; ?>
                    </td>
                  </tr>
                  <!-- <tr>
                    <th>
                      PATIENT HAT DATENEINVERSTÄNDNISERKLÄRUNG UNTERZEICHNET
                    </th>
                    <td>
                      <?php //echo $usersdata->consent_form; ?>
                    </td>
                  </tr> -->
                  <tr>
                    <th>BESTEHENDES CANNABIS MEDIKAMENT
                    </th>
                    <td>
                      <?php echo $usersdata->bestehendes; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      NAME CANNABIS MEDIKAMENT
                    </th>
                    <td>
                      <?php echo $usersdata->cannabis_drug; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      DOSISEINHEIT
                    </th>
                    <td>
                      <?php echo $usersdata->dosiseinheit_first; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      TAGESDOSIS
                    </th>
                    <td>
                      <?php echo $usersdata->tagesdosis_first; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      TAGESDOSIS: (MG THC)
                    </th>
                    <td>
                      <?php echo $usersdata->tagesdosis_mg_first; ?>
                    </td>
                  </tr>
                  <!-- <tr>
                    <th>
                      NAME EINES NICHT ZUORDENBARES CANNABIS MEDIKAMENT:
                    </th>
                    <td>
                      <?php //echo $usersdata->name_eines_nicht; ?>
                    </td>
                  </tr> -->
                  <!-- <tr>
                    <th>
                      DOSISEINHEIT:
                    </th>
                    <td>
                      <?php //echo $usersdata->dosiseinheit_second; ?>
                    </td>
                  </tr> -->
                  <!-- <tr>
                    <th>
                      TAGESDOSIS:
                    </th>
                    <td>
                      <?php //echo $usersdata->tagesdosis_second; ?>
                    </td>
                  </tr> -->
                  <!-- <tr>
                    <th>
                      TAGESDOSIS: (MG THC)
                    </th>
                    <td>
                      <?php //echo $usersdata->tagesdosis_mg_thc_second; ?>
                    </td>
                  </tr> -->
                  <tr>
                    <th>
                      GRUND FÜR UMSTELLUNG
                    </th>
                    <td>
                      <?php echo $usersdata->grund_fur_umstellung; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Anderer Grund
                    </th>
                    <td>
                      <?php echo $usersdata->anderer_grund; ?>
                    </td>
                  </tr>
                  </table>
                  <!-- <div class="portlet portlet-sortable box green-haze">
                    <div class="portlet-title">
                      <div class="caption">
                        <span>Patient Questions Details
                        </span>
                      </div>
                      <div class="tools">
                        <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                        </a>
                      </div>
                    </div>
                    <div class="portlet-body portlet-empty">
                      <?php if ($question_data_records) { ?>
                        
                      <table class="table table-bordered table-condensed">
                      <tr>
                        <th>
                          Bitte wählen Sie aus, wie stark Ihr Schmerz im Durchschnitt der letzten Woche war.
                        </th>
                        <td>
                          <?php echo $question_data_records->severe_pain_last_week; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Ist der Schmerz erträglich?
                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->pain_bearable == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                      <tr>
                        <th style="word-break: break-all;">
                          Bitte wählen Sie aus, wie stark Ihr Schmerz wäre, wenn Sie keine Schmerzmittel einnehmen würden.
                        </th>
                        <td>
                          <?php echo $question_data_records->no_pain_reliever_severe_pain; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Wie würden Sie Ihren Gesundheitszustand im Allgemeinen beschreiben?
                        </th>
                        <td>
                          <?php echo $question_data_records->health_status; ?>
                        </td>
                      </tr>
                      <tr>
                        <th style="word-break: break-all;">
                          Sind Sie durch Ihren derzeitigen Gesundheitszustand bei diesen Tätigkeiten eingeschränkt? Wenn ja, ie stark? Mittelschwere Tätigkeiten, z.B. einen Tisch verschieben, Staubsaugen oder kegeln
                        </th>
                        <td>
                          <?php echo $question_data_records->health_activities_moderate; ?>
                        </td>
                      </tr>
                      <tr>
                        <th style="word-break: break-all;">
                          Sind Sie durch Ihren derzeitigen Gesundheitszustand bei diesen Tätigkeiten eingeschränkt? Wenn ja, ie stark? Mehrere Treppenabsätze steigen.
                        </th>
                        <td>
                          <?php echo $question_data_records->health_activities_climbing; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Hatten Sie in den vergangenen 4 Wochen aufgrund Ihrer körperlichen Gesundheit irgendwelche Schwierigkeiten bei der Arbeit oder anderen alltäglichen Tätigkeiten im Beruf bzw. zu Hause? Ich habe weniger geschafft als ich wollte
                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->physical_health_past_4_weeks_less == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Hatten Sie in den vergangenen 4 Wochen aufgrund Ihrer körperlichen Gesundheit irgendwelche Schwierigkeiten bei der Arbeit oder anderen alltäglichen Tätigkeiten im Beruf bzw. zu Hause?
                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->physical_health_past_4_weeks_certain == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Hatten Sie in den vergangenen 4 Wochen aufgrund seelischer Probleme irgendwelche Schwierigkeiten bei der Arbeit oder anderen alltäglichen Tätigkeiten im Beruf bzw. zu Hause? (z.B. weil Sie sich niedergeschlagen oder ängstlich fühlten) Ich habe weniger geschafft als ich wollte.
                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->mental_health_past_4_weeks_less == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Hatten Sie in den vergangenen 4 Wochen aufgrund seelischer Probleme irgendwelche Schwierigkeiten bei der Arbeit oder anderen alltäglichen Tätigkeiten im Beruf bzw. zu Hause? (z.B. weil Sie sich niedergeschlagen oder ängstlich fühlten) Ich konnte nicht so sorgfältig wie üblich arbeiten.
                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->mental_health_past_4_weeks_could_not_be_careful == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Inwieweit haben Schmerzen Sie in den vergangenen 4 Wochen bei der Ausübung Ihrer Alltagstätigkeiten zu Hause oder im Beruf behindert?
                        </th>
                        <td>
                          <?php echo $question_data_records->pain_in_daily_activities_past_4_weeks; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Wie oft waren Sie in den letzten 4 Wochen … Ruhig und Gelassan
                        </th>
                        <td>
                          <?php echo $question_data_records->clam_in_past_4_weeks; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Wie oft waren Sie in den letzten 4 Wochen … Voller Energie
                        </th>
                        <td>
                          <?php echo $question_data_records->full_energy_in_past_4_weeks; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Wie oft waren Sie in den letzten 4 Wochen … Entmutig und Traurig
                        </th>
                        <td>
                          <?php echo $question_data_records->discourage_sad_in_past_4_weeks; ?>
                        </td>
                      </tr>
                      <tr>
                        <th style="word-break: break-all;">
                          Wie häufig haben Ihre körperliche Gesundheit oder seelische Probleme in den vergangenen 4 Wochen Ihren Kontakt zu anderen Menschen wie z.B. Besuche bei Freunden und Bekannten beeinträchtigt?
                        </th>
                        <td>
                          <?php echo $question_data_records->contact_with_others; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Meine üblichen Bettzeiten sind:
                        </th>
                        <td>
                          <?php echo date('H:i',strtotime($question_data_records->sleep_timing_from)). ' zu '. date('H:i',strtotime($question_data_records->sleep_timing_to)); ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Wie viele Minuten brauchen Sie in der Regel, um einzuschlafen?
                        </th>
                        <td>
                          <?php echo $question_data_records->time_takes_to_fall_asleep; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Wie viele Stunden glauben Sie, durchschnittlich nachts zu schlafen?
                        </th>
                        <td>
                          <?php echo $question_data_records->avg_hours_sleep_at_night; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Ich kann nicht durchschlafen.
                        </th>
                        <td>
                          <?php echo $question_data_records->cannot_sleep_through; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Ich wache zu früh auf.
                        </th>
                        <td>
                          <?php echo $question_data_records->wake_up_early; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Ich wache schon bei leichten Geräuschen auf.
                        </th>
                        <td>
                          <?php echo $question_data_records->wake_up_with_noise; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Ich habe das Gefühl, die ganze Nacht kein Auge zugetan zu haben.
                        </th>
                        <td>
                          <?php echo $question_data_records->no_eye_all_night; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Ich denke viel über meinen Schlaf nach.
                        </th>
                        <td>
                          <?php echo $question_data_records->think_much_sleep_after; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Ich habe Angst ins Bett zu gehen, da ich befürchte, nicht schlafen zu können.
                        </th>
                        <td>
                          <?php echo $question_data_records->afraid_not_able_to_sleep; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Ich fühle mich voll leistungsfähig.
                        </th>
                        <td>
                          <?php echo $question_data_records->full_powerful; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Ich nehme Schlafmittel, um einschlafen zu können.
                        </th>
                        <td>
                          <?php echo $question_data_records->taking_sleeping_pills; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Erinnerung für Lebensqualitätsfragebogen ( alle 4 Wochen)
                        </th>
                        <td>
                          <?php echo date('H:i', strtotime($question_data_records->reminder_quality_of_life_questions)); ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Erinnerung für Schlafqualitätsbogen ( 1 Mal pro Woche)
                        </th>
                        <td>
                          <?php echo date('H:i', strtotime($question_data_records->reminder_sleep_quality)); ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          ANZAHL DURCHGÄNGE
                        </th>
                        <td>
                          <?php echo 'MORGENS: '. $question_data_records->morning_no_of_passages; ?><br/>
                          <?php echo 'MITTAGS: '. $question_data_records->lunch_no_of_passages; ?><br/>
                          <?php echo 'ABENDS: '. $question_data_records->evening_no_of_passages; ?><br/>
                          <?php echo 'NACHTS: '. $question_data_records->night_no_of_passages; ?><br/>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          SPRÜHSTOß/DURCHGANG
                        </th>
                        <td>
                          <?php echo 'MORGENS: '. $question_data_records->morning_spray_continuity; ?><br/>
                          <?php echo 'MITTAGS: '. $question_data_records->lunch_spray_continuity; ?><br/>
                          <?php echo 'ABENDS: '. $question_data_records->evening_spray_continuity; ?><br/>
                          <?php echo 'NACHTS: '. $question_data_records->night_spray_continuity; ?><br/>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          SUMME SPRÜHSTÖßE
                        </th>
                        <td>
                          <?php echo 'MORGENS: '. $question_data_records->morning_total_spraying; ?><br/>
                          <?php echo 'MITTAGS: '. $question_data_records->lunch_total_spraying; ?><br/>
                          <?php echo 'ABENDS: '. $question_data_records->evening_total_spraying; ?><br/>
                          <?php echo 'NACHTS: '. $question_data_records->night_total_spraying; ?><br/>
                        </td>
                      </tr>
                        <th>
                          Angabe der Schmerzstärke abends 30 Minuten nach Einnahme von CannaXan-THC

                        </th>
                        <td>
                          <?php echo $question_data_records->pain_after_30_min_after_thc; ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          IST DER SCHMERZ ERTRÄGLICH?
                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->pain_bearable_after_cannaxan_thc == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Sind bei Ihnen Durchbruchschmerzen aufgetreten? um Morgens

                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->morning_breakthrough_pain == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Sind bei Ihnen Durchbruchschmerzen aufgetreten? um Mittags
                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->lunch_breakthrough_pain == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Sind bei Ihnen Durchbruchschmerzen aufgetreten? um Abends
                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->evening_breakthrough_pain == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          Sind bei Ihnen Durchbruchschmerzen aufgetreten? um Nachts
                        </th>
                        <td style="word-break: break-all;">
                          <?php if ($question_data_records->night_breakthrough_pain == 'ja') { ?>
                            <button class="btn btn-xs btn-success status_label" type="button">Ja</button>
                          <?php } else { ?>
                            <button class="btn btn-xs btn-danger stats_label" type="button">Nein</button>
                          <?php } ?>
                        </td>
                      </tr>
                      <!-- <tr>
                        <th>
                          Status
                        </th>
                          <td style="word-break: break-all;">
                            <?php if ($question_data_records->status == 'Active') { ?>
                              <button class="btn btn-xs btn-success status_label" type="button">Active</button>
                            <?php } else { ?>
                              <button class="btn btn-xs btn-danger stats_label" type="button">Inactive</button>
                            <?php } ?>
                          </td>
                      </tr> -->
                      <!-- <tr>
                        <th>Add Date
                        </th>
                        <td style="word-break: break-all;">
                          <?php echo date('F d, Y',strtotime(getGMTDateToLocalDate($question_data_records->add_date)));?>
                        </td>
                      </tr> -->
                    <?php } else { ?>
                        <tr>
                          <td colspan="9"><div class="no-record">No record found</div></td>
                        </tr>
                    <?php } ?>
                    </table>
                  </div>
                </div> -->

                <!-- <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <th>Time</th>              
                    <th>Number of Passages</th>              
                    <th>Spray</th>
                    <th>Total Spray</th>
                    <th>Add Date</th>              
                  </thead>
                  <tbody>
                  <?php if ($patient_weitere_data) { ?>

                    <?php foreach ($patient_weitere_data as $key => $value) { ?>
                      
                    <tr>
                      <td><?php echo date('H:i', strtotime($value->time));?></td>
                      <td><?php echo $value->number_of_passages;?></td>
                      <td><?php echo $value->spray;?></td>
                      <td><?php echo $value->total_spray;?></td>
                      <td><?php echo date('d.m.Y', strtotime($value->add_date));?></td>
                    </tr>
                    <?php } ?> 
                  <?php } else { ?>
                      <tr>
                        <td colspan="9"><div class="no-record">No record found</div></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table> -->
              </section>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>