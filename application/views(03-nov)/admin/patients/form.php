<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js">
</script>
<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px" >
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/patients')?>" class="tooltips" data-original-title="Patients List" data-placement="top" data-container="body">Patients List
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <?php if($id != '') { ?>
        <?php echo $first_name; ?>

        <?php } else { ?>
        Add Patient
        <?php } ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/patients'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form', 'autocomplete' => 'off'));?>
        <div class="form-body">
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">VERSICHERTENNUMMER

              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'VERSICHERTENNUMMER', 'id' => "insured_number", 'name' => "insured_number", 'class' => "form-control", 'value' => "$insured_number")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Doctor
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php $doctors_id = isset($doctors->doctors_id) ? $record->doctors_id : ''; ?>
              <select class="form-control select2me" id="doctors_id" onchange="getFallnummer()" name="doctors_id">
                  <?php if ($doctors) { ?>
                    <?php foreach ($doctors as $key => $doctors) { ?>
                      <option <?php echo ($doctors->id == $selected_doctor)?'selected':''; ?> value="<?php echo $doctors->id; ?>"><?php echo $doctors->name; ?></option>
                      
                    <?php } ?>
                  <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">FALLNUMMER
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'FALLNUMMER', 'id' => "case_number", 'name' => "case_number", 'class' => "form-control", 'value' => "$case_number")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">VORNAME
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'VORNAME', 'id' => "first_name", 'name' => "first_name", 'class' => "form-control", 'value' => "$first_name")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Name
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Name', 'id' => "last_name", 'name' => "last_name", 'class' => "form-control", 'value' => "$last_name")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Telephone Number
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Enter Telephone Number", 'id' => "telephone_number", 'name' => "telephone_number", 'class' => "form-control", 'value' => "$telephone_number")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          <!-- <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Road
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Enter Road", 'type' => "text", 'id' => "road", 'name' => "road", 'class' => "form-control", 'value' => "$road")) ?>
            </div>
          </div> -->
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">DIAGNOSE CODE (ICD 10-GM)
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "ICD Code", 'type' => "text", 'id' => "diagnostic_code", 'name' => "diagnostic_code", 'class' => "form-control", 'value' => "$diagnostic_code")) ?>
            </div>
          </div>
          <!-- <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">E-mail
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Enter E-mail', 'id' => "email", 'name' => "email", 'class' => "form-control", 'value' => "$email")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div> -->
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">GEBURTSDATUM
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'GEBURTSDATUM', 'id' => "date_of_birth", 'name' => "date_of_birth", 'class' => "form-control", 'value' => "$date_of_birth")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">GEWICHT 
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "GEWICHT", 'id' => "weight", 'name' => "weight", 'class' => "form-control", 'value' => "$weight")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">GRÖSSE
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "GRÖSSE", 'id' => "size", 'name' => "size", 'class' => "form-control", 'value' => "$size")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
                <label for="form_control_title" class="control-label col-md-2">GESCHLECHT</label>
                <div class="md-radio-inline">
                  <div class="md-radio">
                    <input id="radio21" class="md-radiobtn" type="radio" name="gender" value="männlich" <?php echo ($gender == 'männlich')?'checked':''; ?>>
                    <label for="radio21">
                    <span class="inc"></span>
                    <span class="check"></span>
                    <span class="box"></span>Male</label>
                  </div>
                  <div class="md-radio has-error">
                    <input id="radio22" class="md-radiobtn" type="radio" name="gender" value="weiblich" <?php echo ($gender == 'weiblich')?'checked':''; ?>>
                    <label for="radio22">
                    <span class="inc"></span>
                    <span class="check"></span>
                    <span class="box"></span>Female</label>
                  </div>
                </div>
            </div> 
          <!-- <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Gender
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Enter Gender', 'id' => "gender", 'name' => "gender", 'class' => "form-control", 'value' => "$gender")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div> -->
          <!-- <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Main Indication
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php $main_indication = isset($record->main_indication) ? $record->main_indication : ''; ?>
              <select class="form-control" name="main_indication">
                  <?php if ($main_indication) { ?>
                    <?php foreach ($main_indication as $key => $all_main_indication) { ?>
                      <option <?php echo ($all_main_indication->id == $category)?'selected':''; ?> value="<?php echo $all_main_indication->id; ?>"><?php echo $all_main_indication->main_indication; ?></option>
                      
                    <?php } ?>
                  <?php } ?>
              </select>
            </div>
          </div> -->
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">HAUPTINDIKATION
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <!-- <?php //echo form_input(array('placeholder' => 'Enter Main Indication', 'id' => "main_indication", 'name' => "main_indication", 'class' => "form-control", 'value' => "$main_indication")); ?> -->
              <?php //$indication = isset($main_indication->id) ? $record->main_indication : ''; ?>
              <select class="form-control select2me" id="main_indication" onchange="IcdDiagnoseCode()" name="main_indication">
                <?php if ($all_indications) { ?>
                  <?php foreach ($all_indications as $key => $value) { ?>
                    <option <?php echo ($value->id == $selected_indication)?'selected':''; ?> data-icd="<?php echo $value->icd_10; ?>" value="<?php echo $value->id; ?>"><?php echo $value->indikation; ?></option>
                    
                  <?php } ?>
                <?php } ?>
              </select>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <!-- <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Nicht zuordenbare Hauptindikation
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php //echo form_input(array('placeholder' => 'Enter Main Indication', 'id' => "main_indication", 'name' => "main_indication", 'class' => "form-control", 'value' => "$main_indication")); ?>
              <textarea class="form-control" id="main_indication_not_assigned" name="main_indication_not_assigned" placeholder="Enter Main Indication that are not assigned"><?php //echo $main_indication_not_assigned; ?></textarea>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div> -->
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Moderate bis schwere Schmerzen</label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input id="radio26" class="md-radiobtn" type="radio" name="pain_scale" value="Applicable" <?php echo ($pain_scale == 'Applicable')?'checked':''; ?>>
                <label for="radio26">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>Applicable</label>
              </div>
              <div class="md-radio has-error">
                <input id="radio27" class="md-radiobtn" type="radio" name="pain_scale" value="Not Applicable" <?php echo ($pain_scale == 'Not Applicable')?'checked':''; ?>>
                <label for="radio27">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>Not Applicable</label>
              </div>
            </div>
          </div>
          <!-- <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">PATIENT HAS SIGNED CONSENT FORM: 
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <select class="form-control" name="consent_form">

                <option <?php //echo ($consent_form == 'ja')?'selected':''; ?> value="ja">Ja</option>
                <option <?php //echo ($consent_form == 'nein')?'selected':''; ?> value="nein">Nein</option>
              </select>
            </div>
          </div> -->
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Existing Cannabis Medicine</label>
            <div class="md-radio-inline existing_cannabis_med"onchange="get_existing_cannabis_med()">
              <div class="md-radio">
                <input id="radio24" class="md-radiobtn" type="radio" name="existing_cannabis_med" value="ja" <?php echo ($existing_cannabis_med == 'ja')?'checked':''; ?>>
                <label for="radio24">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>Ja</label>
              </div>
              <div class="md-radio has-error">
                <input id="radio25" class="md-radiobtn" type="radio" name="existing_cannabis_med" value="nein" <?php echo ($existing_cannabis_med == 'nein')?'checked':''; ?>>
                <label for="radio25">
                <span class="inc"></span>
                <span class="check"></span>
                <span class="box"></span>Nein</label>
              </div>
            </div>
          </div>
          
 
          <div class="cannabis_medicine_exist" id="cannabis_medicine_exist">
            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">NAME CANNABIS MEDIKAMENT 
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <?php $cannabis_drug = isset($record->cannabis_drug) ? $record->cannabis_drug : ''; ?>
                <select class="form-control cannabis_medikament" onchange="changeMedikament()" name="cannabis_drug">
                	<option value=""></option>
                    <?php if ($cannabis_medikament) { ?>
                      <?php foreach ($cannabis_medikament as $key => $all_cannabis_drug) { ?>
                        <option <?php echo ($all_cannabis_drug->id == $selected_cannabis_medikament)?'selected':''; ?> value="<?php echo $all_cannabis_drug->id; ?>"><?php echo $all_cannabis_drug->name; ?></option>
                        
                      <?php } ?>
                    <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">DOSISEINHEIT: 
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <?php echo form_input(array('placeholder' => 'Dose Unit', 'id' => "dose_unit", 'name' => "dose_unit", 'class' => "form-control dose_unit", 'value' => "$dose_unit")); ?>
                <!-- <select class="form-control" name="dose_unit">

                  <option <?php echo ($dose_unit == 'kapseln')?'selected':''; ?> value="kapseln">Kapseln</option>
                  <option <?php echo ($dose_unit == 'kapseln')?'selected':''; ?> value="kapseln">kapseln</option>
                </select> -->
              </div>
            </div>
            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">TAGESDOSIS
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <?php echo form_input(array('placeholder' => 'TAGESDOSIS', 'id' => "daily_dose", 'name' => "daily_dose", 'class' => "form-control", 'value' => "$daily_dose", "onkeyup" => "calculatevalues()")); ?>
                <div class="form-control-focus"> 
                </div>
              </div>
            </div>

            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">Tagesdosis: (mg THC)
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <?php echo form_input(array('placeholder' => 'Tagesdosis', 'id' => "daily_dose_mg", 'name' => "daily_dose_mg", 'class' => "form-control", 'value' => "$daily_dose_mg")); ?>
                <div class="form-control-focus"> 
                </div>
              </div>
            </div>
            <!-- <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">NAME EINES NICHT ZUORDENBARES CANNABIS MEDIKAMENT
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <?php //echo form_input(array('placeholder' => 'NAME EINES NICHT ZUORDENBARES CANNABIS MEDIKAMENT', 'id' => "unassigned_drug", 'name' => "unassigned_drug", 'class' => "form-control", 'value' => "$unassigned_drug")); ?>
                <div class="form-control-focus"> 
                </div>
              </div>
            </div> -->
            <!-- <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">DOSISEINHEIT: 
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <select class="form-control" name="unassigned_dose_unit">

                  <option <?php //echo ($dose_unit == 'kapseln')?'selected':''; ?> value="kapseln">Kapseln</option>
                  <option <?php //echo ($dose_unit == 'kapseln')?'selected':''; ?> value="kapseln">Kapseln</option>
                </select>
              </div>
            </div> -->
            <!-- <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">TAGESDOSIS
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <?php //echo form_input(array('placeholder' => 'TAGESDOSIS', 'id' => "unassigned_daily_dose", 'name' => "unassigned_daily_dose", 'class' => "form-control", 'value' => "$unassigned_daily_dose")); ?>
                <div class="form-control-focus"> 
                </div>
              </div>
            </div> -->
            <!-- <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">TAGESDOSIS(MG THC)
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <?php //echo form_input(array('placeholder' => 'Enter Daily Dose(MG THC)', 'id' => "unassigned_daily_dose_mg", 'name' => "unassigned_daily_dose_mg", 'class' => "form-control", 'value' => "$unassigned_daily_dose_mg")); ?>
                <div class="form-control-focus"> 
                </div>
              </div>
            </div> -->
          </div>

            <div class="cannabis_medicine_does_not_exist" id="cannabis_medicine_does_not_exist">
            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">GRUND FÜR UMSTELLUNG  
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <?php $change_reason = isset($record->change_reason) ? $record->change_reason : ''; ?>
                <select class="form-control select2me" name="change_reason[]" multiple="">
                  <!-- <option value="Nebenwirkungen">Nebenwirkungen</option>
                  <option value="Ungenügende Wirkung">Ungenügende Wirkung</option>
                  <option value="Fehlende Kostenübernahme">Fehlende Kostenübernahme</option> -->
                  
                    <?php $all_selected_reason = explode(',', $selected_grund_umstellung_reason); ?>
                      <?php foreach ($all_selected_reason as $key => $all_change_reason) { ?>
                        <option value="Nebenwirkungen" <?php echo ($all_change_reason == 'Nebenwirkungen')?'selected':''; ?>>Nebenwirkungen</option>
                        <option value="Ungenügende Wirkung" <?php echo ($all_change_reason == 'Ungenügende Wirkung')?'selected':''; ?>>Ungenügende Wirkung</option>
                        <option value="Fehlende Kostenübernahme" <?php echo ($all_change_reason == 'Fehlende Kostenübernahme')?'selected':''; ?>>Fehlende Kostenübernahme</option>
                        <!-- <option <?php echo ($all_change_reason->id == $all_selected_reason)?'selected':''; ?> value="<?php echo $all_change_reason->id; ?>"><?php echo $all_change_reason->change_reason; ?></option> -->
                        
                      <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">Anderer Grund
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <!-- <?php //echo form_input(array('placeholder' => 'Enter Other Reason', 'id' => "other_reason", 'name' => "other_reason", 'class' => "form-control", 'value' => "$other_reason")); ?> -->
                <textarea id="other_reason" name="other_reason" placeholder="Anderer Grund" class="form-control"><?php echo $other_reason; ?></textarea>
                <div class="form-control-focus"> 
                </div>
              </div>
            </div>
          </div>


          <div class="form-group form-md-line-input">
                <label for="form_control_title" class="control-label col-md-2">Status</label>
                <div class="md-radio-inline">
                  <div class="md-radio">
                    <input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" <?php echo ($status == 'Active')?'checked':''; ?>>
                    <label for="radio19">
                    <span class="inc"></span>
                    <span class="check"></span>
                    <span class="box"></span>Active</label>
                  </div>
                  <div class="md-radio has-error">
                    <input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" <?php echo ($status == 'Inactive')?'checked':''; ?>>
                    <label for="radio20">
                    <span class="inc"></span>
                    <span class="check"></span>
                    <span class="box"></span>Inactive</label>
                  </div>
                </div>
            </div> 
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/patients'); ?>" class="btn default">Cancel
                </a>
              </div>
            </div>
          </div>
        </div>
        </form>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
  $(document).ready(function(){   

    $('#date_of_birth').datepicker(
      {
        format: 'dd-mm-yyyy',     
        autoclose:true
      }
    );
    get_existing_cannabis_med();

  });

    function get_existing_cannabis_med() {
      
     var existing_cannabis_med = $('input[name="existing_cannabis_med"]:checked').val();
      if(existing_cannabis_med == 'nein') {
      //alert(existing_cannabis_med);
        $('#cannabis_medicine_does_not_exist').show();
        $('#cannabis_medicine_exist').hide();
      } else {
        //alert(existing_cannabis_med);
        $('#cannabis_medicine_does_not_exist').hide();
        $('#cannabis_medicine_exist').show();
      }
    }

    function getFallnummer() {
      var link = "<?php echo base_url('admin/patients/add'); ?>";
      var doctor = $('#doctors_id').val();
      //alert(doctor);
      $.ajax({
        url: link+"?doctor="+doctor,

        success: function(fallnummerdata) {
          //console.log(fallnummerdata);
          $('#case_number').val(fallnummerdata);
        }
      });
    }

    function changeMedikament() {
      var cannabis_medikament = $('.cannabis_medikament').val();
      if (cannabis_medikament == "1") {
         $('#dose_unit').val("Sprühstöße");
      } else if (cannabis_medikament == "2") {
         $('#dose_unit').val("Tropfen");
      } else if (cannabis_medikament == "3") {
         $('#dose_unit').val("g");
      } else if (cannabis_medikament == "4") {
         $('#dose_unit').val("ml");
      }
   }

   function IcdDiagnoseCode() {
   	var icd_code = $('#main_indication option:selected').attr('data-icd');
   	$('#diagnostic_code').val(icd_code);
   }

   function calculatevalues() {
      var cannabis_medikament = $('.cannabis_medikament').val();
      if (cannabis_medikament == "1") {
         var tagesdosis_first = $('#daily_dose').val();
         var tagesdosis_first_value = 2.7*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(2);
         //alert(tagesdosis_first_value);
         $('#daily_dose_mg').val(tagesdosis_first_round_off);
      } else if (cannabis_medikament == "2") {
         var tagesdosis_first = $('#daily_dose').val();
         var tagesdosis_first_value = 0.83*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(2);
        //alert(tagesdosis_first_round_off);
         //alert(tagesdosis_first_value);
         $('#daily_dose_mg').val(tagesdosis_first_round_off);
         
      } else if (cannabis_medikament == "3") {
         var tagesdosis_first = $('#daily_dose').val();
         var tagesdosis_first_value = 0.2*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(2);
         //alert(tagesdosis_first_value);
         $('#daily_dose_mg').val(tagesdosis_first_round_off);
      } else if (cannabis_medikament == "4") {
         var tagesdosis_first = $('#daily_dose').val();
         var tagesdosis_first_value = 25*tagesdosis_first;
         var tagesdosis_first_round_off = parseFloat(tagesdosis_first_value).toFixed(2);
         //alert(tagesdosis_first_value);
         $('#daily_dose_mg').val(tagesdosis_first_round_off);
      }
   }

</script>

