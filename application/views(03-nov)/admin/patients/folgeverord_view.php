<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/Patients')?>" class="tooltips" data-original-title="List of Patients" data-placement="top" data-container="body"> List of Patients
        </a>
        <i class="fa fa-arrow-right">
        </i>
        <?php if($folgeverord_data)
         {
            echo $folgeverord_data->rezepturarzneimittel;
         } else
         {

         }?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/Patients'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Patients Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
               <?php if ($folgeverord_data) { ?>
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>
                      Hauptindikation
                    </th>
                    <td>
                      <?php echo $folgeverord_data->hauptindikation; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Rezepturarzneimittel
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $folgeverord_data->rezepturarzneimittel; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Wirkstoff
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $folgeverord_data->wirkstoff; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>
                      thc konzentration [mg/ml]
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $folgeverord_data->thc_konzentration_mg_ml; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Tagesdosis thc [mg]
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $folgeverord_data->tagesdosis_thc_mg; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Wirkstoff
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $folgeverord_data->wirkstoff; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>
                     Therapiedauer
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $folgeverord_data->therapiedauer; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                     Tagesdosis cbd[mg]
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $folgeverord_data->tagesdosis_cbd; ?>
                    </td>
                  </tr>
                </table>
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Tag name</th>              
                      <th>Anzahl Sprühstöße morgens</th>              
                      <th>Anzahl Sprühstöße mittags</th>
                      <th>Anzahl Sprühstöße abends</th>
                      <th>Anzahl Sprühstöße nachts</th>              
                      <th>thc morgens [mg]</th>
                      <th>thc mittags [mg]</th>
                      <th>thc abends [mg]</th>              
                      <th>thc nachts [mg]</th>
                      <th>summe thc [mg]</th>
                    </tr>
                  </thead>
                  <?php if ($patient_folgeverord_data) { ?>

                    <?php foreach ($patient_folgeverord_data as $key => $value) { ?>
                  <tbody>
                    <tr>
                      <td><?php echo $value->tag_name;?></td>
                      <td><?php echo $value->anzahl_spruhstobe_morgens;?></td>
                      <td><?php echo $value->anzahl_spruhstobe_mittags;?></td>
                      <td><?php echo $value->anzahl_spruhstobe_abends;?></td>
                      <td><?php echo $value->anzahl_spruhstobe_nachts;?></td>
                      <td><?php echo $value->thc_morgens_mg;?></td>
                      <td><?php echo $value->thc_mittags_mg;?></td>
                      <td><?php echo $value->thc_abends_mg;?></td>
                      <td><?php echo $value->thc_nachts_mg;?></td>
                      <td><?php echo $value->summe_thc_mg;?></td>  
                    </tr>                      
                  </tbody>
                    <?php } ?>
                   <?php } else { ?>
                      <tr>
                        <td colspan="9"><div class="no-record">No record found</div></td>
                      </tr>
                    <?php } } else { ?>
                       <tr>
                        <td colspan="9"><div class="no-record">No record found</div></td>
                      </tr>
                    <?php } ?>
                </table>
                
              </section>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
