<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/Patients')?>" class="tooltips" data-original-title="List of Patients" data-placement="top" data-container="body"> List of Patients
        </a>
        <i class="fa fa-arrow-right">
        </i>
          <?php echo $record->first_name; ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/Patients'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Patient Nebenwirkung Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
                <?php if ($nebenwirkung_data) { ?>
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>
                      Datum
                    </th>
                    <td>
                      <?php echo date('d.m.Y', strtotime($nebenwirkung_data->datum)); ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Behandungstag
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $nebenwirkung_data->behandungstag; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Visite
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $nebenwirkung_data->visite; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Arzneimittel
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $nebenwirkung_data->arzneimittel; ?>
                    </td>
                  </tr>
                </table>
                  
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                    <th>Nebenwirkung</th>
                    <th colspan="4" class="p-0">
                     <table cellpadding="0" cellspacing="0" width="100%" align="center">
                        <tr>
                           <th colspan="4"> <center> schweregrad</center></th>
                        </tr>
                        <tr>
                           <th style="background:#2CA61E; ">mild</th>
                           <th style="background:#FCAF16; ">moderat</th>
                           <th style="background:#F76816; ">schwer</th>
                           <th style="background:#D91313; ">lebensbedrohlich</th>
                        </tr>
                     </table></th>
                    <th>KAUSALITÄT ZUR<br> THERAPIE VORHANDEN</th>
                 </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($get_nebenwirkung as $key => $value) { 

                        $mild_schweregrad = '';
                        $moderat_schweregrad = '';
                        $schwer_schweregrad = '';
                        $lebensbedrohlich_schweregrad = '';
                        $kausalitat_zur_therapie_vorhanden = '';
                    if (isset($get_nebenwirkung_data)) { 
                            foreach ($get_nebenwirkung_data as $k => $val) { 

                            if ($value->id == $val->behandlunggstag_id) {
                                $mild_schweregrad = $val->mild_schweregrad;
                                $moderat_schweregrad = $val->moderat_schweregrad;
                                $schwer_schweregrad = $val->schwer_schweregrad;
                                $lebensbedrohlich_schweregrad = $val->lebensbedrohlich_schweregrad;
                                $kausalitat_zur_therapie_vorhanden = $val->kausalitat_zur_therapie_vorhanden;
                                break;
                            }
                        }
                    } ?>
                            
                    <tr>                            
                        <td><?php echo $value->nebenwirkung_name; ?></td>
                        <td><?php echo $mild_schweregrad; ?></td>
                        <td><?php echo $moderat_schweregrad; ?></td>
                        <td><?php echo $schwer_schweregrad; ?></td>
                        <td><?php echo $lebensbedrohlich_schweregrad; ?></td>
                        <td><?php echo $kausalitat_zur_therapie_vorhanden; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <?php } else { ?>
                  <tr>
                    <td colspan="9"><div class="no-record">No record found</div></td>
                  </tr>
                <?php } ?>
              </section>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
