<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/Patients')?>" class="tooltips" data-original-title="List of Patients" data-placement="top" data-container="body"> List of Patients
        </a>
        <i class="fa fa-arrow-right">
        </i>
         <?php if($umstellung_data)
         {
            echo $umstellung_data->rezepturarzneimittel;
         } else
         {

         }?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/Patients'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Patients Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
               <?php if ($umstellung_data) { ?>
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>
                      Datum
                    </th>
                    <td>
                      <?php echo date('d.m.Y', strtotime($umstellung_data->add_date)); ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Dosiseinheit:
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $umstellung_data->dosiseinheit; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Tagesdosis bisheriges
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $umstellung_data->tagesdosis_bisheriges; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Tagesdosis (mg THC)
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $umstellung_data->tagesdosis; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Rezepturarzneimittel
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $umstellung_data->rezepturarzneimittel; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Wirkstoff
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $umstellung_data->wirkstoff; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Anzahl Therapietage
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $umstellung_data->anzahl_therapietage; ?>
                    </td>
                  </tr>
                </table>
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Tag name</th>              
                      <th>morgens anzahl durchgange</th>              
                      <th>morgens anzahl sps</th>
                      <th>mittags anzahl durchgange</th>
                      <th>mittags anzahl sps</th>              
                      <th>abends anzahl durchgange</th>
                      <th>abends anzahl sps</th>
                      <th>mittags anzahl sps</th>              
                      <th>nachts anzahl durchgange</th>
                      <th>nachts anzahl sps</th>
                      <th>Start tgl cannaxan thc dosis mg thc</th>
                      <th>Transferfaktor</th>              
                      <th>Eintitration</th>
                    </tr>
                  </thead>
                  <?php if ($patient_umstellung_data) { ?>

                    <?php foreach ($patient_umstellung_data as $key => $value) { ?>
                  <tbody>
                    <tr>
                      <td><?php echo $value->tag_name;?></td>
                      <td><?php echo $value->morgens_anzahl_durchgange;?></td>
                      <td><?php echo $value->morgens_anzahl_sps;?></td>
                      <td><?php echo $value->mittags_anzahl_durchgange;?></td>
                      <td><?php echo $value->mittags_anzahl_sps;?></td>
                      <td><?php echo $value->abends_anzahl_durchgange;?></td>
                      <td><?php echo $value->abends_anzahl_sps;?></td>
                      <td><?php echo $value->mittags_anzahl_sps;?></td>
                      <td><?php echo $value->nachts_anzahl_durchgange;?></td>
                      <td><?php echo $value->nachts_anzahl_sps;?></td>
                      <td><?php echo $value->start_tgl_cannaxan_thc_dosis_mg_thc;?></td>
                      <td><?php echo $value->transferfaktor;?></td>
                      <td><?php echo $value->eintitration;?></td>   
                    </tr>                      
                  </tbody>
                <?php } ?>
                   <?php } else { ?>
                      <tr>
                        <td colspan="9"><div class="no-record">No record found</div></td>
                      </tr>
                    <?php } } else { ?>
                       <tr>
                        <td colspan="9"><div class="no-record">No record found</div></td>
                      </tr>
                    <?php } ?>
                </table>
                
              </section>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
