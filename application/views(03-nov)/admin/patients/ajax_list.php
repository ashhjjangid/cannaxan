<?php if ($records) {?>
<?php foreach ($records as $key => $value) {?>
<tr class="my_row" data-id="<?php echo $value->id; ?>">
   <td><input type="checkbox" class="checkboxes recordcheckbox group-checkable" value="<?php echo $value->id; ?>"/></td>
   <td><?php echo $value->insured_number; ?></td>
   <td><?php echo $value->case_number; ?></td>
   <td>
      <?php echo $value->first_name. ' '. $value->last_name; ?>
   </td>
   <!-- <td><?php echo $value->name; ?></td> -->
   <td>
      <div class="btn-group">
      <?php if($value->status == 'Active') { ?>
         <button class="btn btn-xs btn-success status_label" type="button">Active</button>
      <?php } else { ?>
         <button class="btn btn-xs btn-danger status_label" type="button">Inactive</button>
      <?php } ?>
      
         <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-angle-down"></i></button>
         <ul class="dropdown-menu" role="menu" style="margin-top:-50px">
            
            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/patients/change_status'); ?>" data-status="Active" onClick="changeStatusCommon(this)">Active</a></li>

            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/patients/change_status'); ?>" data-status="Inactive" onClick="changeStatusCommon(this)">Inactive</a></li>
         </ul>
      </div>
   </td>
   <td><?php echo date('M d, Y H:i:s', strtotime($value->add_date)); ?></td>
   <td class="text-center">
      <!-- <a href="<?php echo base_url('admin/patients/update/' . $value->id) ?>" class="btn purple tooltips" data-original-title="Update this record" data-placement="top" data-container="body"><i class="fa fa-pencil"></i></a> -->
      
      <!-- <a href="<?php echo base_url('admin/patients/delete/'. $value->id); ?>" class="btn red tooltips" data-original-title="Delete this Record" data-placement="top" data-container="body"><i class="fa fa-trash"></i></a> -->
      <?php // if($_SERVER['REMOTE_ADDR']=="61.16.138.242"){ ?>
      <!-- <a href="javascript:" class="btn btn-danger tooltips" data-original-title="Delete this record" data-placement="top" data-container="body" onclick="deleteUser(this)" data-id="<?php echo $value->id;  ?>" data-customer="<?php echo $value->id;  ?>" data-url="<?php echo base_url('admin/patients/delete') ?>"><i class="fa fa-trash"></i></a> -->
      <?php  // } ?>

      <div class="btn-group">
        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Actions
        </button>
        <div class="dropdown-menu">
         <li><a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/update/' . $value->id) ?>" class="btn purple tooltips" data-original-title="Update this record" data-placement="top" data-container="body">Update <i class="fa fa-pencil"></i></a></li>

         <li><a style="margin-bottom:10px;" href="javascript:" class="btn btn-danger tooltips" data-original-title="Delete this record" data-placement="top" data-container="body" onclick="deleteUser(this)" data-id="<?php echo $value->id;  ?>" data-customer="<?php echo $value->id;  ?>" data-url="<?php echo base_url('admin/patients/delete') ?>">Delete <i class="fa fa-ban"></i></a></li>

          <li><a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">View <i class="fa fa-eye"></i></a></li>

          <li><a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view_arztlichertherapieplan/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body"> Titration<i class="fa fa-flask"></i></a></li>

          <li><a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view_umstellung/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">Umstellung von anderem Cannabis Med. <i class="fa fa-recycle"></i></a></li>

          <li><a  href="<?php echo base_url('admin/patients/view_folgeverord/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">Folgeverordnung <i class="fa fa-history"></i></a></li>
      
      </div>

      <!-- <a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body"><i class="fa fa-eye"></i></a><br/>


      <a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view_arztlichertherapieplan/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">Titration</a>
      <br/>

      <a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view_umstellung/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">Umstellung von anderem Cannabis Med.</a>
      <br/>

      <a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view_folgeverord/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">folgeverord</a>
      <br/> -->
      
      <!-- <a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view_laborwerte/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">Laborwerte</a> -->
      

      <!-- <a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view_begleitmedikation/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">Begleitmedikation</a>
      <br/> -->

      <!-- <a style="margin-bottom:10px;" href="<?php echo base_url('admin/patients/view_nebenwirkung/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body">nebenwirkung</a> -->

   
   </td>
</tr>
<?php }?>
<?php } else {?>
<tr>
   <td colspan="9"><div class="no-record">No record found</div></td>
</tr>
<?php }?>
