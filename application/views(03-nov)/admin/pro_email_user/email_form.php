<?php
	if (isset($message) && $message) {
		$alert = ($success)? 'alert-success':'alert-danger';
		echo '<div class="alert ' . $alert . '">' . $message . '</div>';
	}
?>
<div class="portlet light" style="height:45px">
	<div class="row">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
				<i class="fa fa-arrow-right"></i>
			</li>
			<li>
				<a href="<?php echo site_url('admin/Promotion_email_user')?>" class="tooltips" data-original-title="Newsletter Templates" data-placement="top" data-container="body">Send email</a>
				<i class="fa fa-arrow-right"></i>
			</li>
			<li>
				<?php if($id != '')
				 	{?>Update Promotion email User <?php }
				  else 
				  	{?>Add Promotion email User <?php }
				  ?>			
			</li>	
			<li style="float:right;">
				<a class="btn red tooltips" href="<?php echo base_url('admin/Promotion_email_user'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
				</a>
			</li>				
		</ul>			
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="fa fa-file-image"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>				
			</div>
			<div class="portlet-body form">
				<?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
					<div class="form-body">                               
                    	<div class="form-group form-md-line-input">
							<label for="form_control_title" class="control-label col-md-2">Email id<span style="color:red">*</span></label>
							<div class="col-md-10">					
								<?php echo form_input(array('placeholder' => "Enter Email id", 'id' => "email", 'name' => "promotion_email_id", 'class' => "form-control", 'value' => "$promotion_email_id")); ?>
								<div class="form-control-focus"> </div>			
							</div>
						</div>

						 <div class="form-body">                               
                          <div class="form-group form-md-line-input">
                          <label for="form_control_title" class="control-label col-md-2">User_id
                            <span style="color: red">*
                           </span>
                             </label>
                               <div class="col-md-10">
                       <select class="form-control select2me" name="user_id" id="user_id">
                          <?php if (isset($get_data)) { foreach ($get_data as $value) { ?>
                          <option value="<?php echo $value->id; ?>" <?php if ($user_id == $value->id) { ?> selected <?php } ?> >
                       <?php echo $value->id; ?>
                        </option>
                        <?php } } ?>
                       </select>
                      <div class="form-control-focus"></div>
                      </div>
                          </div>
						<div class="form-group form-md-line-input">
							<label for="form_control_title" class="control-label col-md-2">Status</label>
							<div class="md-radio-inline">
								<div class="md-radio">
									<input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" <?php echo ($status == 'Active')?'checked':''; ?>>
									<label for="radio19"><span class="inc"></span>
									<span class="check"></span>
									<span class="box"></span>Active</label>
								</div>
								<div class="md-radio has-error">
									<input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" <?php echo ($status == 'Inactive')?'checked':''; ?>>
									<label for="radio20"><span class="inc"></span>
									<span class="check"></span>
									<span class="box"></span>Inactive</label>
								</div>
							</div>
						</div>
						<div class="form-actions noborder">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
								<button type="submit" value=""class="btn green submitBTN">Submit</button>
								<a href="<?php echo base_url('admin/Promotion_email_user'); ?>" class="btn default">Cancel</a>
							</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
CKEDITOR.replace('content');
CKEDITOR.add
</script>
<script type="text/javascript">
	$('.ajax_form').submit(function(event){
		for (instance in CKEDITOR.instances)
	    {
	        CKEDITOR.instances[instance].updateElement();
	    }
	});
</script>

<script type="text/javascript">
var element = '.use_constant';
$(element).on('click', function(e)
{
	var constants_type = '.constants';
	e.preventDefault();
	var myValue = $(constants_type).val();
	var type = $(constants_type).attr('data-type');
	var New_value = '{{'+myValue+'}}';
	if(New_value == 0)
    {
        New_value = '';
        CKEDITOR.instances[type].insertText(New_value);
    }
        CKEDITOR.instances[type].insertText(New_value);

});
	</script>