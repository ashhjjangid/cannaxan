<?php if ($customers) {?>
<?php foreach ($customers as $key => $value) { ?>
<tr>
 <td style="text-align:center;">
   <input type="checkbox" class="checkboxes recordcheckbox" value="<?php echo $value->id; ?>"/>
 </td>
 <td>
  <?php if(!empty($value->image)) { ?>
  <img height="60" width="60" src="<?php echo base_url('assets/uploads/crew/profile/'.$value->image);?>" alt="">
  <?php }  else { ?>
  <img height="60" width="60" src="<?php echo base_url('assets/uploads/noimageavailable.png');?>" alt="">
  <?php } ?>
</td>
 <td>
   <?php echo $value->first_name .' '. $value->last_name; ?>
 </td>
 <td>
   <?php echo $value->email; ?>
 </td>
 <td>
   <div class="btn-group">
     <?php if($value->status == 'Active') { ?>
     <button class="btn btn-xs btn-success status_label" type="button">Active
     </button>
     <?php } else { ?>
     <button class="btn btn-xs btn-danger status_label" type="button">Inactive
     </button>
     <?php } ?>
     <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" type="button">
       <i class="fa fa-angle-down">
       </i>
     </button>
     <ul class="dropdown-menu" role="menu" style="margin-top:-50px">
       <li>
         <a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/users/changeStatus'); ?>" data-status="Active" onClick="changeStatusCommon(this)">Active
         </a>
       </li>
       <li>
         <a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/users/changeStatus'); ?>" data-status="Inactive" onClick="changeStatusCommon(this)">Inactive
         </a>
       </li>
     </ul>
   </div>
 </td>
 <td>
  <?php echo getGMTDateToLocalDate($value->add_date,'F j,Y'); ?>
 </td>

 <td class="text-center">  
   <a href="<?php echo base_url('admin/users/update/'.$value->id) ?>" class="btn purple tooltips" title="Update this record" data-original-title="Update this record" data-placement="top" data-container="body">
     <i class="fa fa-pencil">
     </i>
   </a>
   <a href="<?php echo base_url('admin/users/view/'.$value->id) ?>" class="btn yellow tooltips" title="View this record" data-original-title="View this record" data-placement="top" data-container="body">
     <i class="fa fa-eye">
     </i>
   </a>
   <!--<a href="<?php echo base_url('admin/users/favorites/'.$value->id) ?>"  class="btn btn-success tooltips" title="View Favorite Business" data-original-title="View favorite business" data-placement="top" data-container="body">
     <i class="fa fa-heart">
     </i> 
   </a>-->
   <a data-toggle="modal" data-id="<?php echo $value->id; ?>" data-url="<?php echo base_url('admin/users/delete'); ?>" class="btn btn-danger tooltips" onClick="deleteRecord(this);" data-original-title="Delete this record" title="Delete this record" data-placement="top" data-container="body">
     <i class="fa fa-remove">
     </i> 
   </a>
 </td>
</tr>                       
<?php }?>
<?php } else {?>
<tr>
   <td colspan="9"><div class="no-record">No record found</div></td>
</tr>
<?php }?>