<div class="row margin-top-10">
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
             <?php echo $users; ?>
          </h3>
          <!-- <a class="hd_title" href="javascript:void(0)">Total Doctors
          </a> -->
          <a class="hd_title" href="<?php echo base_url('admin/users'); ?>">Total Doctors
          </a>
        </div>
        <div class="icon">
          <i class="fa fa-user-md" aria-hidden="true">
          </i>
        </div>
      </div>	
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            <?php echo $patients; ?>
          </h3>
          <!-- <a class="hd_title" href="javascript:void(0)">Total Patients
          </a> -->
          <a class="hd_title" href="<?php echo base_url('admin/patients'); ?>">Total Patients
          </a>
        </div>
        <div class="icon">
          <i class="fa fa-users" aria-hidden="true">
          </i>
        </div>
      </div>  
    </div>
  </div>
</div>
  <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            0
          </h3>
          <a href="javascript:void(0)" class="hd_title">Total Payment Received
          </a>
        </div>
        <div class="icon">
          <i class="fa fa-money" aria-hidden="true">            
          </i>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            0
             <?php //echo $hunters; ?>
          </h3>
          <a href="javascript:void(0)" class="hd_title">Total Hunters
          </a>
        </div>
        <div class="icon">
          <i class="fa fa-users" aria-hidden="true">
          </i>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 sol-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            0
          </h3>
          <a href="javascript:void(0)" class="hd_title">Scooters charged Weekly
          </a>
        </div>
        <div class="icon">
          <i class="fa fa-plug">            
          </i>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            0
          </h3>
          <a href="javascript:void(0)" class="hd_title">Total Payment for Scooters charged
          </a>
        </div>
        <div class="icon">
          <i class="fa fa-money">            
          </i>
        </div>
      </div>
    </div>
  </div> -->
<!-- <div class="page-head">
  <div class="page-title">
    <h1>Gemini</h1>
  </div>
</div>
<div class="row margin-top-10">
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            USD
          </h3>
          <a class="hd_title" href="<?php echo base_url('admin/gemini_api/exchange/USD')?>">
            U.S. dollar
          </a>
        </div>
        <div class="icon">
          
          <img src="<?php echo base_url('assets/USD.png')?>" height="55px;">
        </div>
      </div>  
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            BTC
          </h3>
          <a class="hd_title" href="<?php echo base_url('admin/gemini_api/exchange/BTC')?>">
            Bitcoin
          </a>
        </div>
        <div class="icon">
          
          <img src="<?php echo base_url('assets/BTC.png')?>" height="55px;">
        </div>
      </div>  
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            ETH
          </h3>
          <a class="hd_title" href="<?php echo base_url('admin/gemini_api/exchange/ETH')?>">
            Ethereum
          </a>
        </div>
        <div class="icon">
          
          <img src="<?php echo base_url('assets/ETH.png')?>" height="55px;">
        </div>
      </div>  
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            BCH
          </h3>
          <a class="hd_title" href="<?php echo base_url('admin/gemini_api/exchange/BCH')?>">
            Bitcoin Cash
          </a>
        </div>
        <div class="icon">
          
          <img src="<?php echo base_url('assets/BCH.png')?>" height="55px;">
        </div>
      </div>  
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            LTC
          </h3>
          <a class="hd_title" href="<?php echo base_url('admin/gemini_api/exchange/LTC')?>">
            Litecoin
          </a>
        </div>
        <div class="icon">
          
          <img src="<?php echo base_url('assets/LTC.png')?>" height="55px;">
        </div>
      </div>  
    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat2">
      <div class="display">
        <div class="number">
          <h3 class="font-purple-soft">
            ZEC
          </h3>
          <a class="hd_title" href="<?php echo base_url('admin/gemini_api/exchange/ZEC')?>">
            Zcash
          </a>
        </div>
        <div class="icon">
          
          <img src="<?php echo base_url('assets/ZEC.png')?>" height="55px;">
        </div>
      </div>  
    </div>
  </div>
</div> -->