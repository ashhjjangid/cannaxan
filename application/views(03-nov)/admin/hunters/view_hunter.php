<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/hunters')?>" class="tooltips" data-original-title="List of Hunters" data-placement="top" data-container="body"> Hunters list
        </a>
        <i class="fa fa-arrow-right">
        </i>
        <?php echo $huntersdata->first_name. ' '. $huntersdata->last_name; ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/hunters'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Hunter Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>Full Name
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $huntersdata->first_name; ?> 
                      <?php echo $huntersdata->last_name; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Email
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $huntersdata->email; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Date of Birth
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo date('F d, Y',strtotime($huntersdata->date_of_birth));?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Gender
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $huntersdata->gender; ?>
                    </td>
                  </tr>
                    <tr>
                    <th>
                      Address
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $huntersdata->address; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Phone Number
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $huntersdata->phonenumber ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Profile Photo
                    </th>
                    <td style="word-break: break-all;">
                      <?php if($huntersdata->profile) { ?>
                        <img height="100" width="200" src="<?php echo base_url("./assets/uploads/hunters_images/". $huntersdata->profile); ?>">
                      <?php } else { ?>
                        <img height="100" width="100" src="<?php echo base_url('./assests/uploads/noimageavailable.png'); ?>">
                      <?php } ?>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      Status
                    </th>
                      <td style="word-break: break-all;">
                        <?php if ($huntersdata->status == 'Active') { ?>
                          <button class="btn btn-xs btn-success status_label" type="button">Active</button>
                        <?php } else { ?>
                          <button class="btn btn-xs btn-danger stats_label" type="button">Inactive</button>
                        <?php } ?>
                      </td>
                  </tr>                  
                </table>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        