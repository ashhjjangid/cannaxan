<div class="portlet light" style="height:45px">
  <ul class="page-breadcrumb breadcrumb">
    <li>
      <i class="icon-home"></i>
      <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
      <i class="fa fa-arrow-right"></i>
    </li>
    <li>
      <a href="<?php echo site_url('admin/mail_templates')?>" class="tooltips" data-original-title="List of  email templates" data-placement="top" data-container="body">List of Email Templates</a>
      <i class="fa fa-arrow-right"></i>
    </li>
    <li>
      <a href="<?php echo site_url('admin/mail_schedule/'.$template_id)?>" class="tooltips" data-original-title="List of email schedulled" data-placement="top" data-container="body">List of Email Schedulled</a>
      <i class="fa fa-arrow-right"></i>
    </li>
    <li>
      View Email schedule
    </li>
    <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/mail_schedule/'.$template_id); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
        </a>
      </li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>					
        </div>
      </div>		
      <div class="portlet-body form">		
        <div class="form-body">
          <?php if(!empty($records)) {?>
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Schedule Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty"> 
              <section style="margin-top: 20px; background-color: white; ">
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>Template
                    </th>
                    <td>
                      <?php echo $records->name; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Time Slot
                    </th>
                    <td>
                      <?php echo $records->scheduled_time; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Add Date
                    </th>
                    <td>
                      <?php echo getGMTDateToLocalDate($records->add_date,'F j,Y'); ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Status
                    </th>
                    <td> 
                      <?php if($records->status == 'Active') { ?>
                      <button class="btn btn-xs btn-success status_label" type="button">Active
                      </button>
                      <?php } else { ?>
                      <button class="btn btn-xs btn-danger status_label" type="button">Inactive
                      </button>
                      <?php } ?>
                    </td>
                  </tr>
                </table>
              </section>								                       
            </div>
          </div>						 
          <?php } else {?>
          <div class="alert alert-info">
            <?php echo $this->lang->line('No_Record_Found!') ?>
          </div>
          <?php } ?>

          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Schedule Users
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty"> 
              <section style="margin-top: 20px; background-color: white; ">
               <!--<table class="table table-bordered table-condensed">-->
              <table class="table table-striped table-bordered table-hover" id="sample_3">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Name </th>
                    <th>Email</th> 
                    <th>User type</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($selected_users)) { 
                    
                        foreach ($selected_users as $key => $value) { ?>
                    <tr>
                    <td><?php echo $key+1;?></td>
                    <td>
                      <?php echo $value->first_name; ?>
                    </td>
                    <td>
                      <?php echo $value->email; ?>
                    </td>
                    <td>
                      <?php echo $value->user_type; ?>
                    </td>      
                    <td>
                      <div class="btn-group">
                      <?php if($value->status == 'Active') { ?>
                        <button class="btn btn-xs btn-success status_label" type="button">Active</button>
                      <?php } else { ?>
                        <button class="btn btn-xs btn-danger status_label" type="button">Inactive</button>
                      <?php } ?>
                      </div>
                    </td>
                  </tr>               
                  <?php } } else { ?>
                  <tr>
                     <td colspan="5"><div class="no-record">No record found</div></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
                <!--</table>-->
              </section>                                       
            </div>
          </div>             
        </div>
      </div>
    </div>
  </div>
</div>
