<?php
	if (isset($message) && $message) {
		$alert = ($success)? 'alert-success':'alert-danger';
		echo '<div class="alert ' . $alert . '">' . $message . '</div>';
	}
?>
<div class="portlet light" style="height:45px">
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>
			<a href="<?php echo site_url('admin/newsletter_templates')?>" class="tooltips" data-original-title="List of  email templates" data-placement="top" data-container="body">List of Newsletter Templates</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>
			<a href="<?php echo site_url('admin/mail_schedule/'.$template_id)?>" class="tooltips" data-original-title="List of email schedulled" data-placement="top" data-container="body">List of Email Schedulled</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>
			<?php if($id != '') 
				{?>Update Email schedule<?php } 
			else 
				{?>Add Email schedule<?php } 
			?>			
		</li>
		<li style="float:right;">
				<a class="btn red tooltips" href="<?php echo base_url('admin/mail_schedule/'.$template_id); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
				</a>
			</li>
						
	</ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="fa fa-chain-broken"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>				
			</div>
			<div class="portlet-body form">
				<?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
					<div class="form-body">	

						<div class="form-group form-md-line-input">
							<label for="form_control_title" class="control-label col-md-2">Users<span style="color:red">*</span></label>
							<div class="col-md-10">	
								<select class="form-control select2me" name="users[]" multiple="">
								    <?php if(isset($users_list)) { foreach($users_list as $users) { 

	                                        if(in_array($users->id, $selected_users)) {
                                                $selected = 'selected';
	                                        } else {
	                                        	$selected = '';
	                                        }
								    	?>
								        <option value="<?php echo $users->id;?>" <?php echo $selected;?>><?php echo $users->first_name.' '. $users->last_name;?></option>
								    <?php }} ?>
								 </select>							
							</div>
						</div>				

						<div class="form-group form-md-line-input">
							<label for="form_control_subject" class="control-label col-md-2">Time Slots<span style="color:red">*</span></label>
							<div class="col-md-10">								
								<?php echo form_input(array('placeholder' => "Enter time slot", 'id' => "time_slot", 'name' => "time_slot", 'class' => "form-control", 'value' => "$time_slot")); ?>
								<div class="form-control-focus"> </div>
							</div>
						</div>	
					</div>
					<div class="form-actions noborder">
						<div class="row">
							<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn green">Submit</button>
							<a href="<?php echo base_url('admin/mail_schedule/'.$template_id); ?>" class="btn default">Cancel</a>
						</div>
						</div>
					</div>
				</form>
			</div>			
		</div>
	</div>
</div>
<script>
	$("#select2box").select2();

	$(function() {
	  $('#time_slot').datetimepicker({ 
	    minDate: new Date(),
	    format: 'YYYY-MM-DD HH:mm:ss',
	  });
	});
</script>	
