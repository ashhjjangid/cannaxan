<div class="portlet light" style="height:45px">
  <ul class="page-breadcrumb breadcrumb">
    <li>
      <i class="icon-home">
      </i>
      <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
      </a>
      <i class="fa fa-arrow-right">
      </i>
    </li>
    <li>			
      <a href="<?php echo site_url('admin/nebenwirkung')?>" class="tooltips" data-original-title="List of nebenwirkung" data-placement="top" data-container="body">List of nebenwirkung
      </a>
      <i class="fa fa-arrow-right">
      </i>
    </li>
    <li>View details
    </li>	
    <li style="float:right;">
      <a class="btn red tooltips" href="<?php echo base_url('admin/nebenwirkung'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
        <i class="m-icon-swapleft m-icon-white">
        </i>
      </a>
    </li>				
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>					
        </div>
      </div>		
      <div class="portlet-body form">		
        <div class="form-body">
          <?php if(!empty($record)) {?>
          <div class="">
            <div class="portlet light">
              <div class="portlet-title">
                <div class="row">
                  <div class="col-md-6">
                    <div class="caption font-red-sunglo">
                      <i class="fa fa-file-image">
                      </i>
                      <span class="caption-subject bold font-purple-plum uppercase">
                        <i class="fa fa-eye">
                        </i> 	About the nebenwirkung
                      </span>
                    </div>
                  </div>					
                </div>				
              </div>
              <div class="portlet-body portlet-empty"> 
                <section style="margin-top: 20px; background-color: white; ">
                  <table class="table table-bordered table-condensed">
                    <tr>
                      <th>Nebenwirkung
                      </th>
                      <td>
                        <?php echo $record->nebenwirkung_name; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Add Date
                      </th>
                      <td>
                        <?php echo getGMTDateToLocalDate($record->add_date,'F j,Y'); ?>
                      </td>
                    </tr>
                  </table>
                </section>								                       
              </div>
            </div>
          </div>					 
          <?php } else {?>
          <div class="alert alert-info">No record found
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>