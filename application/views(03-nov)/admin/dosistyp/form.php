<?php
	if (isset($message) && $message) {
		$alert = ($success)? 'alert-success':'alert-danger';
		echo '<div class="alert ' . $alert . '">' . $message . '</div>';
	}
?>
<div class="portlet light" style="height:45px">
	<div class="row">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
				<i class="fa fa-arrow-right"></i>
			</li>

			<li>
				<a href="<?php echo site_url('admin/dosistyp')?>" class="tooltips" data-original-title="List of dosistyp" data-placement="top" data-container="body">List of dosistyp</a>
				<i class="fa fa-arrow-right"></i>
			</li>

			<li>
				<input type="hidden" name="" id="ids" value="<?php echo $id; ?>">

				<?php if ($id != '') { ?>
				 	<?php $fa_icon  = 'fa-pencil-square-o'; ?>
				 	<?php echo $record->morgens_anzahl_durchgange; ?>

				 <?php } else { ?>
				 	Add dosistyp
				 	<?php $fa_icon  = 'fa-plus'; ?>
				 <?php } ?>
			</li>	
			<li style="float:right;">
				<a class="btn red tooltips" href="<?php echo base_url('admin/dosistyp'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
				</a>
			</li>				
		</ul>			
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="fa fa-file-image"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>				
			</div>
			<?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
				<div class="portlet light">
					<div class="portlet-title">
						<div class="row">
							<div class="col-md-6">
								<div class="caption font-red-sunglo">
									<i class="fa fa-file-image"></i>
									<span class="caption-subject bold font-purple-plum uppercase"><i class="fa <?php echo $fa_icon; ?>"></i>About the dosistyp
									</span>
								</div>
							</div>					
						</div>				
					</div>

					<!-- <div class="form-group form-md-line-input">
			            <label for="form_control_title" class="control-label col-md-2">Choose Business
			              <span style="color:red">*
			              </span>
			            </label>
			            <div class="col-md-10">
			              <?php $selected_user = isset($record->user_id) ? $record->user_id : ''; ?>
			              <select class="form-control" name="select_business">
			              	<option value="">Select Business</option>
			                  <?php if ($select_business) { ?>
			                    <?php foreach ($select_business as $key => $list_of_business) { ?>
			                      <option <?php echo ($list_of_business->id == $selected_user)?'selected':''; ?> value="<?php echo $list_of_business->id; ?>"><?php echo $list_of_business->name; ?></option>
			                      
			                    <?php } ?>
			                  <?php } ?>
			              </select>
			            </div>
			          </div> -->
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Tag name<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $tag_name = isset($record->tag_name) ? $record->tag_name : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter tag name", 'id' => "tag_name", 'name' => "tag_name", 'class' => "form-control", 'value' => "$tag_name")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							 <div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Morgens anzahl durchgange<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $morgens_anzahl_durchgange = isset($record->morgens_anzahl_durchgange) ? $record->morgens_anzahl_durchgange : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter morgens anzahl durchgange", 'id' => "morgens_anzahl_durchgange", 'name' => "morgens_anzahl_durchgange", 'class' => "form-control", 'value' => "$morgens_anzahl_durchgange")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>

							 <div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Morgens anzahl sps</label>
								<div class="col-md-10">	
									<?php  $morgens_anzahl_sps = isset($record->morgens_anzahl_sps) ? $record->morgens_anzahl_sps : ''; ?>
									<?php echo form_input(array('placeholder' => "Enter morgens anzahl sps", 'id' => "morgens_anzahl_sps", 'name' => "morgens_anzahl_sps", 'class' => "form-control", 'value' => "$morgens_anzahl_sps")); ?>
									<div class="form-control-focus"> </div>
								</div>
							</div>
							
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Mittags anzahl durchgange<span style="color:red">*</span></label>
								<div class="col-md-10">
									<?php echo form_input(array('placeholder' => "Enter mittags anzahl durchgange",'type' => "text", 'id' => "mittags_anzahl_durchgange", 'name' => "mittags_anzahl_durchgange", 'class' => "form-control", 'value' => "$mittags_anzahl_durchgange")); ?>
									<div class="form-control-focus"> </div>			
								</div>
						    </div> 
	

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Mittags anzahl sps<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $mittags_anzahl_sps = isset($record->mittags_anzahl_sps) ? $record->mittags_anzahl_sps : ''; ?>
									<?php echo form_input(array('placeholder' => "Enter mittags anzahl sps",'type' => "text", 'id' => "mittags_anzahl_sps", 'name' => "mittags_anzahl_sps", 'class' => "form-control", 'value' => "$mittags_anzahl_sps")); ?>
									<div class="form-control-focus"> </div>
								</div>
							</div>

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Abends anzahl durchgange<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $abends_anzahl_durchgange = isset($record->abends_anzahl_durchgange) ? $record->abends_anzahl_durchgange : ''; ?>
									<?php echo form_input(array('placeholder' => "Enter abends anzahl durchgange",'type' => "text", 'id' => "abends_anzahl_durchgange", 'name' => "abends_anzahl_durchgange", 'class' => "form-control", 'value' => "$abends_anzahl_durchgange")); ?>
									<div class="form-control-focus"> </div>
								</div>
							</div>

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Abends anzahl sps<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $abends_anzahl_sps = isset($abends_anzahl_sps) ? $abends_anzahl_sps : ''; ?>
									<input type="text" data-role="tagsinput" name="abends_anzahl_sps" placeholder="Enter abends anzahl sps" id="abends_anzahl_sps", class="form-control" value="<?php echo  $abends_anzahl_sps; ?>">				
									<div class="form-control-focus"> </div>			
								</div>
							</div>

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Nachts anzahl durchgange<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php $nachts_anzahl_durchgange = isset($record->nachts_anzahl_durchgange) ? $record->nachts_anzahl_durchgange : ''; ?>
									<textarea placeholder="Enter nachts anzahl durchgange" class="form-control" id="nachts_anzahl_durchgange" name="nachts_anzahl_durchgange" ><?php echo $nachts_anzahl_durchgange; ?></textarea>
									<div class="form-control-focus"> </div>
								</div>
							</div>

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Nachts anzahl sps<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $nachts_anzahl_sps = isset($nachts_anzahl_sps) ? $nachts_anzahl_sps : ''; ?>
									<input type="text"  name="nachts_anzahl_sps" placeholder="Enter nachts anzahl sps" id="nachts_anzahl_sps", class="form-control" value="<?php echo  $nachts_anzahl_sps; ?>">				
									<div class="form-control-focus"> </div>			
								</div>
							</div>

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Thc morgens mg<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $thc_morgens_mg = isset($thc_morgens_mg) ? $thc_morgens_mg : ''; ?>
									<input type="text"  name="thc_morgens_mg" placeholder="Enter thc morgens mg" id="thc_morgens_mg", class="form-control" value="<?php echo  $thc_morgens_mg; ?>">				
									<div class="form-control-focus"> </div>			
								</div>
							</div>

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Thc mittags mg<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $thc_mittags_mg = isset($thc_mittags_mg) ? $thc_mittags_mg : ''; ?>
									<input type="text"  name="thc_mittags_mg" placeholder="Enter thc mittags mg" id="thc_mittags_mg", class="form-control" value="<?php echo  $thc_mittags_mg; ?>">				
									<div class="form-control-focus"> </div>			
								</div>
							</div>

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">	Thc abends mg<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $thc_abends_mg = isset($thc_abends_mg) ? $thc_abends_mg : ''; ?>
									<input type="text"  name="thc_abends_mg" placeholder="Enter thc abends mg" id="thc_abends_mg", class="form-control" value="<?php echo  $thc_abends_mg; ?>">				
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Thc nachts mg<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $thc_nachts_mg = isset($thc_nachts_mg) ? $thc_nachts_mg : ''; ?>
									<input type="text"  name="thc_nachts_mg" placeholder="Enter thc nachts mg" id="thc_nachts_mg", class="form-control" value="<?php echo  $thc_nachts_mg; ?>">				
									<div class="form-control-focus"> </div>			
								</div>
							</div>

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Summe thc mg<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $summe_thc_mg = isset($summe_thc_mg) ? $summe_thc_mg : ''; ?>
									<input type="text"  name="summe_thc_mg" placeholder="Enter summe thc mg" id="summe_thc_mg", class="form-control" value="<?php echo  $summe_thc_mg; ?>">				
									<div class="form-control-focus"> </div>			
								</div>
							</div>

							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Display order<span style="color:red">*</span></label>
								<div class="col-md-10">	
									<?php  $display_order = isset($display_order) ? $display_order : ''; ?>
									<input type="text"  name="display_order" placeholder="Enter display order" id="display_order", class="form-control" value="<?php echo  $display_order; ?>">				
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Status</label>
								<div class="md-radio-inline">
									<div class="md-radio">
										<input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" <?php echo ($status == 'Active')?'checked':''; ?>>
										<label for="radio19">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Active</label>
									</div>
									<div class="md-radio has-error">
										<input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" <?php echo ($status == 'Inactive')?'checked':''; ?>>
										<label for="radio20">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Inactive</label>
									</div>
								</div>
							</div> 
							<div class="form-actions noborder">
								<div class="row">
									<div class="col-md-offset-2 col-md-10">
										<button type="submit" class="btn green">Submit</button>
										<a href="<?php echo base_url('admin/dosistyp'); ?>" class="btn default">Cancel</a>
								    </div>
								</div>
						    </div>
						</div>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
<script src="https://maps.googleapis.com/maps/api/js?v=3&key=<?php echo getSiteOption('google_map_api_key', true); ?>&sensor=false&libraries=places"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
		var input = document.getElementById('address');
        var places = new google.maps.places.Autocomplete(input);        
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
           var latitude = place.geometry.location.lat();
           var longitude = place.geometry.location.lng();
           $('#latitude').val(latitude);
           $('#longitude').val(longitude);
        });
    });

   	function updateLAT($this) {
   		var a = $($this).val();
   		$("#latitude").val('');
   		$("#longitude").val('');
   	}
 </script>

 <script type="text/javascript">

	$(function(){
	    $('#start_date').datetimepicker({
	       // startDate: new Date(),
		   // autoclose: true,
		   // format: "yyyy-mm-dd hh:ii",
	    });

	    $('#end_date').datetimepicker({
	       // startDate: new Date(),
		   // autoclose: true,
		   // format: "yyyy-mm-dd hh:ii",
	    });

	    $('#add_time').datetimepicker({

	    });
	});
 </script>

