<div class="portlet light" style="height:45px">
  <ul class="page-breadcrumb breadcrumb">
    <li>
      <i class="icon-home">
      </i>
      <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
      </a>
      <i class="fa fa-arrow-right">
      </i>
    </li>
    <li>			
      <a href="<?php echo site_url('admin/dosistyp')?>" class="tooltips" data-original-title="List of dosistyp" data-placement="top" data-container="body">List of dosistyp
      </a>
      <i class="fa fa-arrow-right">
      </i>
    </li>
    <li>View details
    </li>	
    <li style="float:right;">
      <a class="btn red tooltips" href="<?php echo base_url('admin/dosistyp'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
        <i class="m-icon-swapleft m-icon-white">
        </i>
      </a>
    </li>				
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>					
        </div>
      </div>		
      <div class="portlet-body form">		
        <div class="form-body">
          <?php if(!empty($record)) {?>
          <div class="">
            <div class="portlet light">
              <div class="portlet-title">
                <div class="row">
                  <div class="col-md-6">
                    <div class="caption font-red-sunglo">
                      <i class="fa fa-file-image">
                      </i>
                      <span class="caption-subject bold font-purple-plum uppercase">
                        <i class="fa fa-eye">
                        </i> 	About the dosistyp
                      </span>
                    </div>
                  </div>					
                </div>				
              </div>
              <div class="portlet-body portlet-empty"> 
                <section style="margin-top: 20px; background-color: white; ">
                  <table class="table table-bordered table-condensed">
                   
                    <tr>
                      <th>Morgens Anzahl Durchgänge
                      </th>
                      <td>
                        <?php echo $record->morgens_anzahl_durchgange; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Morgens Anzahl SPS
                      </th>
                      <td>
                        <?php echo $record->morgens_anzahl_sps; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Mittags Anzahl Durchgänge
                      </th>
                      <td>
                        <?php echo $record->mittags_anzahl_durchgange; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Mittags Anzahl SPS
                      </th>
                      <td>
                        <?php echo  $record->mittags_anzahl_sps; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Abends Anzahl Durchgänge
                      </th>
                      <td>
                        <?php echo wordwrap($record->abends_anzahl_durchgange); ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Abends Anzahl SPS
                      </th>
                      <td>
                        <?php echo $record->abends_anzahl_sps; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Nachts Anzahl Durchgänge
                      </th>
                      <td>
                        <?php echo $record->nachts_anzahl_durchgange; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Nachts Anzahl SPS
                      </th>
                      <td>
                        <?php echo $record->nachts_anzahl_sps; ?>
                      </td>
                    </tr>
                    <tr>
                       <tr>
                      <th>THC morgens [mg]
                      </th>
                      <td>
                        <?php echo wordwrap($record->thc_morgens_mg); ?>
                      </td>
                    </tr>
                      <tr>
                      <th>THC mittags [mg]
                      </th>
                      <td>
                        <?php echo wordwrap($record->thc_mittags_mg); ?>
                      </td>
                    </tr>
                    <tr>
                      <th>THC abends [mg]
                      </th>
                      <td>
                        <?php echo $record->thc_abends_mg; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>THC nachts [mg]
                      </th>
                      <td>
                        <?php echo $record->thc_nachts_mg; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Summe THC [mg]
                      </th>
                      <td>
                        <?php echo $record->summe_thc_mg; ?>
                      </td>
                    </tr>
                    <tr>
                      <th>Status
                      </th>
                      <td>
                        <div class="btn-group">
                          <?php if($record->status == 'Active') { ?>
                          <button class="btn btn-xs btn-success status_label" type="button">Active
                          </button>
                          <?php } else { ?>
                          <button class="btn btn-xs btn-danger status_label" type="button">Inactive
                          </button>
                          <?php } ?>
                        </div>
                      </td>
                    </tr>
                  </table>
                </section>								                       
              </div>
            </div>
          </div>					 
          <?php } else {?>
          <div class="alert alert-info">No record found
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>