<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/faq_category')?>" class="tooltips" data-original-title="List of FAQ" data-placement="top" data-container="body">List of FAQ
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <?php if($id != '') { ?>
        <?php echo $category_name;?>
        <?php } else { ?>
        Add FAQ Category
        <?php } ?>			
      </li>	
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/faq_category'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>				
    </ul>			
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>					
        </div>				
      </div>
      <div class="portlet-body form">
        <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
        <div class="form-body">                               
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Category Name
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">					
              <?php echo form_input(array('placeholder' => "Enter Name", 'id' => "category_name", 'name' => "category_name", 'class' => "form-control", 'value' => "$category_name")); ?>
              <div class="form-control-focus"> 
              </div>			
            </div>
          </div>						
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Status
            </label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" 
                       <?php echo ($status == 'Active')?'checked':''; ?>>
                <label for="radio19">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Active
                </label>
              </div>
              <div class="md-radio has-error">
                <input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" 
                       <?php echo ($status == 'Inactive')?'checked':''; ?>>
                <label for="radio20">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Inactive
                </label>
              </div>
            </div>
          </div> 
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/faq_category'); ?>" class="btn default">Cancel
                </a>
              </div>
            </div>
          </div>
          </form>
      </div>
    </div>
  </div>
</div>
</div>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCxhgcC9Un6YMIVL5agYr7ygNvQMt306Nc&sensor=false&libraries=places">
</script>
<script type="text/javascript">
  google.maps.event.addDomListener(window, 'load', function () {
    var input = document.getElementById('address');
    var places = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(places, 'place_changed', function () {
      var place = places.getPlace();
      var address = place.formatted_address;
      var latitude = place.geometry.location.lat();
      var longitude = place.geometry.location.lng();
      $('#latitude').val(latitude);
      $('#longitude').val(longitude);
    });
  });
  function updateLAT($this) {
    var a = $($this).val();
    $("#latitude").val('');
    $("#longitude").val('');
  }
</script>-->