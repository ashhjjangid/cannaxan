<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo base_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo base_url('admin/Credits')?>" class="tooltips" data-original-title="List of Credits" data-placement="top" data-container="body">List of Credits
        </a>
        <i class="fa fa-arrow-right"></i>
      </li>
      <li>
        <?php if($id != ''){?>
          <!--<?php echo $records->user_id; ?>-->
      <?php } else {?>
         Add Credits
     <?php } ?>     
      </li> 
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/Credits'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>       
    </ul>     
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>          
        </div>        
      </div>
      <div class="portlet-body form">
       <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
        <div class="form-body">                               
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">User_id
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
            <select class="form-control select2me" name="category_id" id="category_id">
              <?php if (isset($get_data)) { foreach ($get_data as $value) { ?>
                <option value="<?php echo $value->id; ?>" <?php if ($user_id == $value->id) { ?> selected <?php } ?> >
                  <?php echo $value->user_name; ?>
                </option>
              <?php } } ?>
            </select>
            <div class="form-control-focus"></div>
            </div>
          </div>
          <div class="portlet-body form">
        <div class="form-body">                               
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Email
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">         
              <?php echo form_input(array('placeholder' => "Enter Email id", 'type' => "text", 'id' => "recevier_mail", 'name' => "recevier_mail", 'class' => "form-control", 'value' => "$recevier_mail")); ?>
              <div class="form-control-focus"> 
              </div>      
            </div>
          </div>
           <div class="portlet-body form">
        <div class="form-body">                               
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Credits
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Enter Credits", 'type' => "text", 'id' => "credits", 'name' => "credits", 'class' => "form-control", 'value' => "$credits")); ?>
              <div class="form-control-focus"> 
              </div>      
            </div>
          </div>
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/Credits'); ?>" class="btn default">Cancel
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      </form>
  </div>
</div>
</div>
