<div class="portlet light" style="height:45px">
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>			
			<a href="<?php echo site_url('admin/Send_email')?>" class="tooltips" data-original-title="send email" data-placement="top" data-container="body">send email</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>View send email </li>	
		<li style="float:right;">
			<a class="btn red tooltips" href="<?php echo base_url('admin/Send_email'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
			</a>
		</li>				
	</ul>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="icon-globe"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>
			</div>		
			<div class="portlet-body form">		
				<div class="form-body">
					<?php if(!empty($email_details)) {?>
						<div class="portlet portlet-sortable box green-haze">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span>send email Details</span>
                                </div>
                                <div class="tools">
                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                </div>
                            </div>
                            <div class="portlet-body portlet-empty"> 
                            	<section style="margin-top: 20px; background-color: white; ">
							        <table class="table table-bordered table-condensed">
							            <tr>
							                <th>Subject</th>
							                <td><?php echo $email_details->subject; ?></td>
							            </tr>
							            <tr>
							                <th>Message</th>
							                <td><?php echo $email_details->message; ?></td>
							            </tr>
							                <th>Add Date</th>
							                <td><?php echo getGMTDateToLocalDate($email_details->add_date,'d M,Y H:i:s'); ?></td>
							            </tr>
							          </table>
							    </section>								                       
							</div>
                        </div>						 
				    <?php } else {?>
				    	<div class="alert alert-info">No record found</div>
				    <?php } ?>
				</div>
			
<div class="portlet-body form">		
				<div class="form-body">
					<?php if(!empty($user_details)) {?>
						<div class="portlet portlet-sortable box green-haze">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span>User Details</span>
                                </div>
                                <div class="tools">
                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                </div>
                            </div>
                            <div class="portlet-body portlet-empty"> 
                            	<section style="margin-top: 20px; background-color: white; ">
							        <table class="table table-bordered table-condensed">
							            <tr>
							               <th>User</th>
							               <th>Email</th>
							               <th>Phone Number</th>
							            </tr>
							            <?php foreach ($user_details as $value) { ?>

							            <tr>
							            	<td><?php echo $value->first_name?></td>
				
							            <td><?php echo $value->email?></td>
							            <td><?php echo $value->phonenumber?></td>
							        </tr>
							    <?php }?>
							          </table>
							    </section>								                       
							</div>
                        </div>						 
				    <?php } else {?>
				    	<div class="alert alert-info">No record found</div>
				    <?php } ?>
				</div>
			</div>
		</div>
		</div>
		</div>

	</div>
</div>
	</div>
</div> 
<style type="text/css">
.mix-inner{height: 200px;}
.mix-inner img{width: 100%; height:200px;}
.fancybox-overlay{z-index:9999;}
.mix-link {background-color: #DC143C !important}
.abc { text-align: center;}
</style>