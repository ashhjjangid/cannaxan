<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo base_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>Add Order</li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/Currency_management'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>       
    </ul>     
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>          
        </div>        
      </div>
      <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
      <div class="form-body">                               
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Symbol
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">         
              <select class="form-control" name="symbol">
                <?php foreach ($symbols as $key => $value) { ?>
                  <option value="<?php echo $value; ?>"><?php echo strtoupper($value); ?></option>
                <?php } ?>
              </select>
              <div class="form-control-focus"></div>      
            </div>
          </div>

          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Amount
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">         
              <input type="text" class="form-control" name="amount" placeholder="Enter amount">
              <div class="form-control-focus"></div>      
            </div>
          </div>

          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Price
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">         
              <input type="text" class="form-control" name="price" placeholder="Enter price">
              <div class="form-control-focus"></div>      
            </div>
          </div>

          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Order Type
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <select class="form-control" name="order_type">
                <option name="limit">Limit</option>
                <option name="stop-limit">Stop limit</option>
              </select>
              <div class="form-control-focus"></div>      
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Side
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <select class="form-control" name="side">
                <option name="buy">Buy</option>
                <option name="sell">Sell</option>
              </select>
              <div class="form-control-focus"></div>      
            </div>
          </div>
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit</button>
                <!-- <a href="<?php echo base_url('admin/Currency_management'); ?>" class="btn default">Cancel -->
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      </form>
  </div>
</div>
</div>
<script type="text/javascript">
  var id = $('.content-idn').val();
  CKEDITOR.replace('content');
  CKEDITOR.add
</script>
<script type="text/javascript">
  $('.ajax_form').submit(function(event){
    for (instance in CKEDITOR.instances)
    {
      CKEDITOR.instances[instance].updateElement();
    }
  });
</script>
