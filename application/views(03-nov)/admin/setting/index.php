<?php
	if ($message) {
		echo '<div class="alert alert-danger">' . $message . '</div>';
	}
?>
<div class="portlet light" style="height:45px">
	<div class="row">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="<?php echo $this->lang->line('Home'); ?>" data-placement="top" data-container="body">Home</a>
				<i class="fa fa-arrow-right"></i>
			</li>			
			<li>
				Website Setting
			</li>	
			<li style="float:right;">
				<a class="btn red tooltips" href="<?php echo base_url('admin'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
				</a>
			</li>					
		</ul>			
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-red-sunglo">
					<i class="icon-settings"></i>
					<span class="caption-subject bold uppercase">Website Setting</span>
				</div>
			</div>
			<div class="portlet-body form">				
				   <?php 
					echo form_open_multipart('', array('class'=>"ajax_form"));
					?>
					<div class="form-body">						
						<div class="portlet portlet-sortable box green-haze">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>SMTP Details</div>
                                    <div class="tools">
                                        <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                        <a class="fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                                        <a class="remove" href="javascript:;" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-empty"> 
                                	<div class="row">
										<div class="col-md-6">
											<div class="form-group form-md-line-input">
												<input type="text" name="smtp_host" class="form-control" id="smtp_host" placeholder="Enter SMTP Host" value="<?php echo $options_content->smtp_host ?>">		
												<label for="form_control_smtp_host">SMTP Host <span style="color:red">*</span></label>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-md-line-input">
												<input type="text" name="smtp_port" class="form-control" id="smtp_port" placeholder="Enter SMTP Port Number" value="<?php echo $options_content->smtp_port ?>">
												<label for="form_control_smtp_port">SMTP Port Number <span style="color:red">*</span></label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group form-md-line-input">
												<input type="text" name="smtp_user" class="form-control" id="smtp_user" placeholder="Enter SMTP Port Number" value="<?php echo $options_content->smtp_user ?>">
												<label for="form_control_smtp_user">SMTP Username <span style="color:red">*</span></label>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-md-line-input">
												<input type="text" name="smtp_pass" class="form-control" id="smtp_pass" placeholder="Enter SMTP Password" value="<?php echo $options_content->smtp_pass ?>">
												<label for="form_control_smtp_pass">SMTP Password <span style="color:red">*</span></label>
											</div>
										</div>
									</div>
                                </div>

                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings"></i>Contact Details</div>
                                    <div class="tools">
                                        <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                        <a class="fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                                        <a class="remove" href="javascript:;" data-original-title="" title=""> </a>
                                    </div>
                                </div>

                                <div class="portlet-body portlet-empty"> 
                                	<!-- <div class="row">
										<div class="col-md-12">
											<div class="form-group form-md-line-input">
												<input type="text" name="contact_us_address" class="form-control" id="address" placeholder="Enter Address" value="<?php echo $options_content->contact_us_address; ?>">		
												<label for="form_control_smtp_host">Address<span style="color:red">*</span></label>
											</div>
										</div>
									</div> -->
									<!-- <input type="hidden" name="latitude" class="form-control" id="latitude" value="<?php echo $options_content->latitude ?>">
									<input type="hidden" name="longitude" class="form-control" id="longitude" value="<?php echo $options_content->longitude ?>">
 -->
									<div class="row">
											<div class="col-md-6">
												<div class="form-group form-md-line-input">
													<input type="text" name="contact_us_tel_no" class="form-control" id="contact_us_tel_no" placeholder="Enter Email Address" value="<?php echo $options_content->contact_us_tel_no ?>">
													<label for="form_control_smtp_pass">Tel No. <span style="color:red">*</span></label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-md-line-input">
													<input type="text" name="contact_us_fax_no" class="form-control" id="contact_us_fax_no" placeholder="Enter Phone Number" value="<?php echo $options_content->contact_us_fax_no ?>">
													<label for="form_control_smtp_pass">Fax No. <span style="color:red">*</span></label>
												</div>
											</div>
									</div>
                                </div>
	                            
	                            	
	                            
	                           
	                                <div class="portlet-title">
	                                    <div class="caption">
	                                        <i class="icon-settings"></i>Copyright Text</div>
	                                    <div class="tools">
	                                        <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
	                                        <a class="fullscreen" href="javascript:;" data-original-title="" title=""> </a>
	                                        <a class="remove" href="javascript:;" data-original-title="" title=""> </a>
	                                    </div>
	                                </div>
	                                <div class="portlet-body portlet-empty"> 
	                                	<div class="row">
											<div class="col-md-6">
												<div class="form-group form-md-line-input">
												<textarea name="copyright_text" placeholder="Enter Copy Right Text" id="copyright_text" class="form-control"><?php echo $copyright_text; ?>
                            					</textarea>
                            					<label for="form_control_smtp_pass">Copyright Text<span style="color:red">*</span></label></div>
											</div>						
										</div>
	                                </div>
	                            </div>
                            </div>
                                                                    
                           
                            
						</div>
					<div class="form-actions noborder">
						<button type="submit" class="btn green">Submit</button>
					</div>
				<?php 
					echo form_close();
				?>
			</div>
		</div>
	</div>
</div>
</script>
 <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCxhgcC9Un6YMIVL5agYr7ygNvQMt306Nc&sensor=false&libraries=places"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
           var latitude = place.geometry.location.lat();
           var longitude = place.geometry.location.lng();
          
           $('#latitude').val(latitude);
           $('#longitude').val(longitude);
        });
    });

    // function updateLAT($this) {
    //   var a = $($this).val();
    //   $("#latitude").val('');
    //   $("#longitude").val('');
    // }
</script>

