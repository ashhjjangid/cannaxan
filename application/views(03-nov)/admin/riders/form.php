<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js">
</script>
<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px" >
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/riders')?>" class="tooltips" data-original-title="List of Riders" data-placement="top" data-container="body">Rider List
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <?php if($id != '') { ?>
        <?php echo $first_name. ' '. $last_name; ?>
        <?php } else { ?>
        Add New Rider
        <?php } ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/riders'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form', 'autocomplete' => 'off'));?>
        <div class="form-body">
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">First Name
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Enter First Name', 'id' => "first_name", 'name' => "first_name", 'class' => "form-control", 'value' => "$first_name")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Last Name
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Enter Last Name', 'id' => "last_name", 'name' => "last_name", 'class' => "form-control", 'value' => "$last_name")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>          
          <div class="form-group form-md-line-input">
            <label for="form-contorl_title" class="control-label col-md-2">Date of Birth
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Enter Date of Birth', 'id' => 'date_of_birth', 'name' => 'date_of_birth', 'class' => 'form-control dob select_year', 'value' => "$date_of_birth")); ?>
              <div class="form-control-focus">                
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Gender
              <span style="color:red">
              </span>
            </label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input id="radio19" class="md-radiobtn" type="radio" name="gender" value="Male" 
                <?php echo ($gender == 'Male')?'checked':''; ?>>
                <label for="radio19">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Male
                </label>
              </div>
              <div class="md-radio">
                <input id="radio20" class="md-radiobtn" type="radio" name="gender" value="Female" 
                       <?php echo ($gender == 'Female')?'checked':''; ?>>
                <label for="radio20">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Female
                </label>
              </div>
            </div>
                   
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">E-mail Address
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Enter email address', 'id' => "email", 'name' => "email", 'class' => "form-control", 'value' => "$email")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Phone Number
              <span style="color:red">
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => "Enter Phone Number", 'id' => "phonenumber", 'name' => "phonenumber", 'class' => "form-control", 'value' => "$phonenumber")); ?>
              <div class="form-control-focus">
              </div>
            </div>
          </div>
          

          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Address
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <textarea class="form-control" id="address" name="address" placeholder="Enter Address"><?php echo $address; ?></textarea>
              <div class="form-control-focus"></div>
            </div>
          </div>
          
          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Password
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Enter password', 'id' => "password", 'name' => "password", 'type' => 'password', 'class' => "form-control", 'value' => "")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <?php if ($task == 'edit') { ?>
          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Confirm Password
              <span style="color: red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Re-enter Your Password', 'id' => 'con_password', 'name' => 'con_password', 'type' => 'password', 'class' => 'form-control')); ?>
              <div class="form-control-focus"></div>
            </div>
          </div>
          <?php } else { ?>

          <?php } ?>
          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Image
            </label>
            <div class="col-md-10">
              <input type="file"  name="profile" id="profile" class="multifile form-control">
            </div>
          </div>  
          <?php if($task == 'edit') { ?>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">
            </label>
            <div class="col-md-10">   
              <img height="100" width="200" src="<?php echo base_url('./assets/uploads/rider_images/'. $record->profile);?>" alt="">
            </div>
          </div>
          <?php  } ?>
          </div>          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Status
              <span style="color:red">
              </span>
            </label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input type="radio" name="status" id="status_active" class="md-radiobtn" value="Active"
                  <?php echo ($status == 'Active')?'checked':''; ?>>
                  <label for="status_active">
                    <span class="inc">                    
                    </span>
                    <span class="check">
                    </span>
                    <span class="box">
                    </span>Active
                  </label>
              </div>
              <div class="md-radio has-error">
                <input type="radio" name="status" id="status_inactive" class="md-radiobtn" value="Inactive"
                  <?php echo ($status == 'Inactive')?'checked':''; ?>>
                  <label for="status_inactive">
                    <span class="inc">
                    </span>
                    <span class="check">
                    </span>
                    <span class="box">
                    </span>Inactive
                  </label>
              </div>
            </div>
            </div>         
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/riders'); ?>" class="btn default">Cancel
                </a>
              </div>
            </div>
          </div>
        </form>
        </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  $(function() {
    $('.dob').datepicker(
    {
      format: 'M dd yyyy',
      enddate: '-20y',
      autoclose: true
    });
  });
</script>

