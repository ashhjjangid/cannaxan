<?php
  if (isset($message) && $message) {
    $alert = ($success)? 'alert-success':'alert-danger';
    echo '<div class="alert ' . $alert . '">' . $message . '</div>';
  }
?>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo base_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo base_url('admin/rating')?>" class="tooltips" data-original-title="List of Reviews" data-placement="top" data-container="body">Reviews
        </a>
        <i class="fa fa-arrow-right"></i>
      </li>
      <li>
        <?php if($id != ''){?>
          <?php echo $records->rating_id;?>
      <?php } else {?>
         Add review
     <?php } ?>     
      </li> 
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/rating'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>       
    </ul>     
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>          
        </div>        
      </div>
      <div class="portlet-body form">
       <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
        <div class="portlet-body form">
        <div class="form-body">                               
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">E-scooter Name
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">         
              <select class="form-control select2me" name="scooter_name" id="scooter_name">
              <?php if (isset($scooters)) { foreach ($scooters as $key => $value) { ?>
                <option value="<?php echo $value->id; ?>" <?php if ($id == $value->id) { ?> selected <? } ?> >
                  <?php echo $value->maker_name.' '.$value->name; ?>
                </option>
              <?php } } ?>
            </select>
              <div class="form-control-focus"> 
              </div>      
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">User Name (Rider Name)
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">         
              <select class="form-control select2me" name="user_name" id="user_name">
              <?php if (isset($users)) { foreach ($users as $key => $value) { ?>
                <option value="<?php echo $value->id; ?>" <?php if ($id == $value->id) { ?> selected <? } ?> >
                  <?php echo $value->first_name.' '.$value->last_name; ?>
                </option>
              <?php } } ?>
            </select>
              <div class="form-control-focus"> 
              </div>      
            </div>
          </div>            
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Rating
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10"> 
              <select class="form-control" name="rating">
                <option><?php echo $records->rating ;?></option> 
                <option value=0 >0</option>
                <option value=1.0>1</option>
                <option value=2.0>2</option>
                <option value=3.0>3</option>
                <option value=4.0>4</option>
                <option value=5.0>5</option>
              </select>
            </div>
          </div>          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Review
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <textarea placeholder="Enter review" id="review" name="review" class="form-control"><?php echo $review; ?></textarea>
              <div class="form-control-focus"> 
              </div>      
            </div>
          </div>
      <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Status
              <span style="color:red">*
            </label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" 
                <?php echo ($status == 'Active')?'checked':''; ?>>
                <label for="radio19">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Active
                </label>
              </div>
              <div class="md-radio has-error">
                <input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive"
                <?php echo ($status == 'Inactive')?'checked':''; ?>>
                <label for="radio20">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Inactive
                </label>
              </div>
            </div>
          
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/rating'); ?>" class="btn default">Cancel
                </a>
              </div>
             </div>
            </div>
          </div>
        </form>
        </div>
      </div>
  </div>
</div>
</div>