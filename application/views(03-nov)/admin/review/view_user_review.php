<div class="portlet light" style="height:45px">
  <ul class="page-breadcrumb breadcrumb">
    <li>
      <i class="icon-home">
      </i>
      <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
      </a>
      <i class="fa fa-arrow-right">
      </i>
    </li>
    <li>			
      <a href="<?php echo site_url('admin/rating')?>" class="tooltips" data-original-title="List of Reviews" data-placement="top" data-container="body">E-scooter Reviews
      </a>
      <i class="fa fa-arrow-right">
      </i>
    </li>
    <li>Review Details
    </li>	
    <li style="float:right;">
      <a class="btn red tooltips" href="<?php echo base_url('admin/rating'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
        <i class="m-icon-swapleft m-icon-white">
        </i>
      </a>
    </li>				
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>					
        </div>
      </div>		
      <div class="portlet-body form">		
        <div class="form-body">
          <?php if(!empty($review_records)) {?>
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Review Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty"> 
              <section style="margin-top: 20px; background-color: white; ">
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>User Name
                    </th>
                    <td>
                      <?php echo $review_records->first_name. ' '. $review_records->last_name; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>E-scooter
                    </th>
                    <td>
                      <?php echo $review_records->maker_name. ' '. $review_records->name; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>User Rating
                    </th>
                    <td>
                      <?php echo $review_records->rating; ?>
                    </td>
                  </tr>                  
                  <tr>
                    <th>Full Review of User
                    </th>
                    <td>
                      <?php echo $review_records->review; ?>
                    </td>
                  </tr>                  
                  <tr>
                    <th>Add Date
                    </th>
                    <td>
                      <?php echo getGMTDateToLocalDate($review_records->add_date,'M d, Y h:i:s A'); ?>
                    </td>
                  </tr>
                </table>
              </section>								                       
            </div>
          </div>						 
          <?php } else {?>
          <div class="alert alert-info">No record found
          </div>
          <?php } ?>
        </div>
        
    </div>
  </div>
</div>
</div>


