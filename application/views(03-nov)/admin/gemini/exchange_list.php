<?php
	if ($this->session->flashdata('message')) {
		echo '<div class="alert alert-success">' . $this->session->flashdata('message') . '</div>';
	}
?>
<script type="text/javascript">
$(function(){
	var table = $('#sample_3');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing1 _START_ to _END_ of _TOTAL_ entries1",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            
            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [{
                "orderable": true
            }, {
                "orderable": true
            },{
                "orderable": true
            }, {
                "orderable": false
            }],
            "lengthMenu": [
                [5, 15, 20, 100, -1],
                [5, 15, 20, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "Search Anything: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [1, "desc"]
            ] // set first column as a default sort by asc
        });
	});
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box grey-cascade">			
			<div class="portlet-body">
				
			</div>
		</div>
		<div class="portlet box grey-cascade">
			<div class="portlet-title">
				<div class="caption"><i class="icon-docs"></i><?php echo $page_title; ?></div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div id="alert_area"></div>
					<div class="row">
						<div class="col-md-12">	
						</div>						
					</div>
				</div>
				<div class="">
					<table class="table table-striped table-bordered table-hover" id="sample_3">
						<thead>
							<tr>
					    		<th>Exchnage</th>
								<th>Rate (Bid)</th>
								<th>Rate (Ask)</th>
								<!-- <th>Date</th> -->
								<th>Option</th>
							</tr>
						</thead>
						<tbody>
						 <?php foreach ($currencies as $key => $value) { ?>
								<tr>
									<td><?php echo $value['name']; ?></td>
									<td><?php echo $value['bid']; ?></td>
									<td><?php echo $value['ask']; ?></td>
									<!-- <td><?php echo getDefaultToGMTDate($value['date']); ?></td> -->
									<!-- <td><?php echo date('d M Y H:i:s', strtotime($value->add_date)); ?></td> -->
									<td class="text-center">
										<a href="<?php echo base_url('admin/gemini_api/past_orders?s='.$symbol.'&es='.strtolower($value['name'])) ?>" 
											class="btn purple tooltips" data-original-title="View Orders" data-placement="top" data-container="body"><i class="fa fa-archive"></i>
										</a>
										<!--
										 <a data-toggle="modal" data-id="<?php echo $value->id; ?>" data-url="<?php echo base_url('admin/Currency_management/delete'); ?>" class="btn btn-danger tooltips" onClick="deleteRecord(this);" data-original-title="Delete this record" data-placement="top" data-container="body"><i class="fa fa-remove"></i>
										 </a>
										<a href="<?php echo base_url('admin/Currency_management/view/'.$value->id) ?>" class="btn yellow tooltips" title="View this record" data-original-title="View this record" data-placement="top" data-container="body">
        								<i class="fa fa-eye">
     									</i>
   										</a> -->
									</td>
									
								</tr>	
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>