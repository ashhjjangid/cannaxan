<style type="text/css">
  .loader-parent{
    position: relative;
  }
  .loader-bx{
    position: absolute;
    left: 12px;
    top: 51%;
    width: 100%;
    height: 47%;
    background: rgba(255,255,255,0.7);
  }
  .loader-bx>img{
    max-width: 63px;
    filter: brightness(0);
    -webkit-filter: brightness(0);
    transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    position: absolute;
    left: 50%;
    top: 50%;
  }
</style>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/manage_ride')?>" class="tooltips" data-original-title="List of Completed Rides" data-placement="top" data-container="body"> Completed Rides
        </a>
        <i class="fa fa-arrow-right">
        </i>
        <?php echo $userdata->ride_id; ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/ride'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="icon-globe">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <div class="portlet portlet-sortable box green-haze">
            <div class="portlet-title">
              <div class="caption">
                <span>Ride Details
                </span>
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title=""> 
                </a>
              </div>
            </div>
            <div class="portlet-body portlet-empty">
              <section style="margin-top: 20px; background-color: white; ">
                <table class="table table-bordered table-condensed">
                  <tr>
                    <th>
                      User Name (Rider Name)
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $userdata->first_name. ' '. $userdata->last_name;?>
                    </td>
                  </tr>
                  <tr>
                  <tr>
                    <th>E-Scooter Name
                    </th>
                    <td style="word-break: break-all;">
                      <?php echo $userdata->maker_name. ' '. $userdata->name; ?> 
                    </td>
                  </tr>
                  <tr>
                    <th>From
                    </th>                    
                    <td style="word-break: break-all;">
                      <?php echo $userdata->from_time; ?>
                    </td>                    
                  </tr>
                  <tr>
                    <th>To
                    </th>
                    <?php if ($userdata->to_time) { ?>
                    <td>
                      <?php echo $userdata->to_time ;?>
                    </td>
                    <?php } else { ?>
                    <td>NA</td>
                  <?php } ?>
                  </tr>
                  <tr>
                    <th>Total Hours
                    </th>
                    <?php if ($userdata->total_hours) { ?>
                    <td style="word-break: break-all;">
                      <?php echo $userdata->total_hours; ?>
                      Hrs
                    </td>
                  <?php } else { ?>
                    <td>NA</td>
                  <?php } ?>
                  </tr>
                  <tr>
                    <th>
                      Price
                    </th>
                    <?php if ($userdata->price) { ?>
                    <td style="word-break: break-all;">
                      $<?php echo $userdata->price; ?>
                    </td>
                    <?php } else { ?>
                      <td>NA</td>
                    <?php } ?>
                  </tr>
                  <tr>
                    <th>
                      Status of Ride
                    </th>
                      <td style="word-break: break-all;">
                        <?php if ($userdata->is_status == 'In progress') { ?>
                          <button class="btn btn-xs btn-info status_label" type="button">In Progress</button>
                        <?php } else { ?>
                          <button class="btn btn-xs btn-danger stats_label" type="button">Completed</button>
                        <?php } ?>
                      </td>
                  </tr>
                  <tr>
                    <th>
                      Is Reserved
                    </th>
                      <td style="word-break: break-all;">
                        <?php if ($userdata->is_reserved == 'Active') { ?>
                          <button class="btn btn-xs btn-success status_label" type="button">Not Reserved</button>
                        <?php } else { ?>
                          <button class="btn btn-xs btn-danger stats_label" type="button">Reserved</button>
                        <?php } ?>
                      </td>
                  </tr>
                  <tr>
                    <th>
                      Status
                    </th>
                      <td style="word-break: break-all;">
                        <?php if ($userdata->status == 'Active') { ?>
                          <button class="btn btn-xs btn-success status_label" type="button">Active</button>
                        <?php } else { ?>
                          <button class="btn btn-xs btn-danger stats_label" type="button">Inactive</button>
                        <?php } ?>
                      </td>
                  </tr>                  
                </table>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        