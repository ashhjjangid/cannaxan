<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px">
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/static_page')?>" class="tooltips" data-original-title="List of static page" data-placement="top" data-container="body">List of static page
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <?php if($id != ''){?>
        	   Update static page 
			<?php } else {?>
			   Add static page 
		 <?php } ?>			
      </li>	
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/static_page'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>				
    </ul>			
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>					
        </div>				
      </div>
      <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
      <div class="portlet light">
        <div class="portlet-title">
          <div class="row">
            <div class="col-md-6">
            </div>					
          </div>				
        </div>
        <div class="portlet-body form">						
          <div class="form-body">
            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2"> Title
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <input type="hidden" class="content-idn" value="<?php echo $id; ?>">
                <input type="text" name="title" class="form-control" placeholder="Enter title" value="<?php echo $title; ?>">
                <div class="form-control-focus">
                </div>			
              </div>
            </div>
            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2"> Description
                <span style="color:red">*
                </span>
              </label>
              <div class="col-md-10">
                <textarea name="content" class="form-control" placeholder="Enter description" id="content">
                  <?php echo $content; ?>
                </textarea>
                <div class="form-control-focus">
                </div>			
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="form-body">
          <?php if (isset($record) && !empty($record)) { ?>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Status
            </label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" 
                       <?php echo ($record->status == 'Active')?'checked':''; ?>>
                <label for="radio19">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Active
                </label>
              </div>
              <div class="md-radio has-error">
                <input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" 
                       <?php echo ($record->status == 'Inactive')?'checked':''; ?>>
                <label for="radio20">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Inactive
                </label>
              </div>
            </div>
          </div> 
          <?php } else { ?>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Status
            </label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" checked />
                <label for="radio19">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Active
                </label>
              </div>
              <div class="md-radio has-error">
                <input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" />
                <label for="radio20">
                  <span class="inc">
                  </span>
                  <span class="check">
                  </span>
                  <span class="box">
                  </span>Inactive
                </label>
              </div>
            </div>
          </div> 
          <?php } ?> 
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/static_page'); ?>" class="btn default">Cancel
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      </form>
  </div>
</div>
</div>
<script type="text/javascript">
  var id = $('.content-idn').val();
  CKEDITOR.replace('content');
  CKEDITOR.add
</script>
<script type="text/javascript">
  $('.ajax_form').submit(function(event){
    for (instance in CKEDITOR.instances)
    {
      CKEDITOR.instances[instance].updateElement();
    }
  });
</script>
