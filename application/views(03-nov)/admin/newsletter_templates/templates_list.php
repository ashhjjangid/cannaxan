<?php
	if ($this->session->flashdata('message')) {
		echo '<div class="alert alert-success">' . $this->session->flashdata('message') . '</div>';
	}
?>
<script type="text/javascript">
$(function(){
	var table = $('#sample_3');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing1 _START_ to _END_ of _TOTAL_ entries1",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            
            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            
            "lengthMenu": [
                [5, 15, 20, 100, -1],
                [5, 15, 20, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 100,            
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "Search Anything: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [1, "desc"]
            ] // set first column as a default sort by asc
        });
	});
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box grey-cascade">
			<div class="portlet-body">
			</div>
		</div>
		<div class="portlet box grey-cascade">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-envelope-o"></i>List of newsletter templates</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div id="alert_area"></div>
					<div class="row">
						<div class="col-md-12">	
							<div class="btn-group tooltips" data-original-title="Add new newsletter templates">
								<a href="<?php echo base_url('admin/newsletter_templates/add'); ?>" class="btn green add_link">ADD NEW<i class="fa fa-plus"></i></a>
							</div>
							<div style="margin-left:10px;" onClick="checkForNullChecked('Active',this)" data-taskurl="<?php echo base_url('admin/newsletter_templates/multiTaskOperation'); ?>" class="btn green tooltips" data-original-title="Active selected record">ACTIVE<i class="fa fa-check"></i></div>
							 <div style="margin-left:10px;" onClick="checkForNullChecked('Inactive',this)" data-taskurl="<?php echo base_url('admin/newsletter_templates/multiTaskOperation'); ?>" class="btn purple tooltips" data-original-title="Inactive selected record">INACTIVE<i class="fa fa-ban"></i></div>
							
						</div>						
					</div>
				</div>
				<div class="">
					<table class="table table-striped table-bordered table-hover" id="sample_3">
						<thead>
							<tr>
								<th class="table-checkbox"> <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes"/>
					    		</th>
					    		<th>Subject</th>
								<th>Message</th>
								<th>Status</th>
								<th>Add date</th>
								<th>Options</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($templates as $value) { ?>
								<tr>
								<td><input type="checkbox" class="checkboxes recordcheckbox" value="<?php echo $value->id; ?>"/></td>	
								<td><?php echo (strlen($value->subject) > 20) ? substr($value->subject,0,20).'...' : $value->subject; ?></td>
								<td><?php echo (strlen($value->message) > 40) ? substr($value->message,0,40).'...' : $value->message; ?></td>
								<td>
									<div class="btn-group">
										<?php if($value->status == 'Active') { ?>
											<button class="btn btn-xs btn-success status_label" type="button">Active</button>
										<?php } else { ?>
											<button class="btn btn-xs btn-danger status_label" type="button">Inactive</button>
										<?php } ?>

										<button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-angle-down"></i></button>
										<ul class="dropdown-menu" role="menu" style="margin-top:-50px">
											<li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/newsletter_templates/change_status'); ?>" data-status="Active" onClick="changeStatusCommon(this)">Active</a></li>
											<li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/newsletter_templates/change_status'); ?>" data-status="Inactive" onClick="changeStatusCommon(this)">Inactive</a></li>
										</ul>
									</div>
								</td>
								<td><?php echo getGMTDateToLocalDate($value->created_at,'d M,Y H:i:s'); ?></td>
								<td class="text-center">		
									<a href="<?php echo base_url('admin/newsletter_templates/update/'.$value->id) ?>" class="btn purple tooltips" data-original-title="Update this record" data-placement="top" data-container="body"><i class="fa fa-pencil"></i>
									</a>
									<a href="<?php echo base_url('admin/newsletter_templates/view/'.$value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body"><i class="fa fa-eye"></i>
									</a>
									<a href="<?php echo base_url('admin/mail_schedule/'.$value->id) ?>" class="btn green tooltips" data-original-title="send email to users" data-placement="top" data-container="body"><i class="fa fa-paper-plane"></i></a>
									<!--<a href="javascript:" class="btn btn-primary tooltips send-email" data-original-title="Send Email" data-id="<?php echo $value->id; ?>" data-placement="top" data-container="body"><i class="refresh-icon fa fa-send"></i>
									</a>-->
								</td>
								</tr>								
							<?php } ?>				
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
		
	$(document).ready(function(){
		$('.send-email').click(function(){
			var $new_btn_txt = '<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>';
			var $new_btn_txt2 = ' <i class="fa fa-send"></i>';
		    var $this = $(this);
			var id = $(this).attr('data-id');
		    $(this).html($new_btn_txt);
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url('admin/newsletter_templates/send_email_subscriber_users/') ?>'+id,
				dataType: 'json',
				success: function(data){
            		$($this).find('.refresh-icon').remove();
					$($this).html($new_btn_txt2);
					toastr.success(data.message);
				},
				error: function(data){
		            $($this).html($new_btn_txt);
		        }
			})
		})
	});
</script>