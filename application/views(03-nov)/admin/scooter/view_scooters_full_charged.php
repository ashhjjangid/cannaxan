<div class="portlet light" style="height:45px">
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>			
			<a href="<?php echo site_url('admin/scooter')?>" class="tooltips" data-original-title="List of e-scooters" data-placement="top" data-container="body">E-scooters</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>			
			<a href="<?php echo site_url('admin/scooter/list_of_fully_charged_scooters')?>" class="tooltips" data-original-title=" List of Charged E-scooters" data-placement="top" data-container="body">Charged E-scooters</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>View E-scooter Charging Details </li>	
		<li style="float:right;">
			<a class="btn red tooltips" href="<?php echo base_url('admin/scooter/list_of_fully_charged_scooters'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
			</a>
		</li>				
	</ul>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="icon-globe"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>
			</div>		
			<div class="portlet-body form">		
				<div class="form-body">
					<?php if(!empty($records)) {?>
						<div class="portlet portlet-sortable box green-haze">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span>E-scooter Charging Details</span>
                                </div>
                                <div class="tools">
                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                </div>
                            </div>
                            <div class="portlet-body portlet-empty"> 
                            	<section style="margin-top: 20px; background-color: white; ">
							        <table class="table table-bordered table-condensed">
							            <tr>
							                <th>E-scooter Name</th>
							                <td><?php echo $records->maker_name. ' '. $records->name; ?></td>
							            </tr>
							            <tr>
							                <th>Picked By(Hunter name)</th>
							                <td><?php echo $records->first_name. ' ' .$records->last_name; ?></td>
							            </tr>
							                <th>Pickup Date Time</th>
							                <td><?php echo getGMTDateToLocalDate($records->pickup_time,'d M,Y H:i:s'); ?></td>
							            </tr>
							            </tr>
							                <th>Release Date Time</th>
							                <?php if (isset($records->release_time) && $records->release_time) { ?>
							                <td><?php echo getGMTDateToLocalDate($records->release_time,'d M,Y H:i:s'); ?></td>
							            <?php } else { ?>
							            	<td>NA</td>
							            <?php } ?>
							            </tr>
							            <tr>
							            	<th>Status</th>
							            	<td>
							            		<div class="btn-group">
											      <?php if($records->status == 'Charging') { ?>
											         <button class="btn btn-xs btn-danger status_label" type="button">Charging</button>
											      <?php } else { ?>
											         <button class="btn btn-xs btn-info status_label" type="button">Charged</button>
											      <?php } ?>
											    </div>
							            	</td>
							            </tr>
							          </table>
							    </section>								                       
							</div>
                        </div>						 
				    <?php } else {?>
				    	<div class="alert alert-info">No record found</div>
				    <?php } ?>
				</div>
		 
<style type="text/css">
.mix-inner{height: 200px;}
.mix-inner img{width: 100%; height:200px;}
.fancybox-overlay{z-index:9999;}
.mix-link {background-color: #DC143C !important}
.abc { text-align: center;}
</style>