<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js">
</script>
<?php
if (isset($message) && $message) {
$alert = ($success)? 'alert-success':'alert-danger';
echo '<div class="alert ' . $alert . '">' . $message . '</div>';
}
?>
<div class="portlet light" style="height:45px" >
  <div class="row">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <i class="icon-home">
        </i>
        <a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <a href="<?php echo site_url('admin/scooter')?>" class="tooltips" data-original-title="List of E-scooters" data-placement="top" data-container="body">E-scooters
        </a>
        <i class="fa fa-arrow-right">
        </i>
      </li>
      <li>
        <?php if($id != '') { ?>
        <?php echo $maker_name.' '. $name; ?>
        <?php } else { ?>
        Add New Scooter
        <?php } ?>
      </li>
      <li style="float:right;">
        <a class="btn red tooltips" href="<?php echo base_url('admin/scooter'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back
          <i class="m-icon-swapleft m-icon-white">
          </i>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="row">
          <div class="col-md-6">
            <div class="caption font-red-sunglo">
              <i class="fa fa-file-image">
              </i>
              <span class="caption-subject bold uppercase">
                <?php echo $page_title; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form', 'autocomplete' => 'off'));?>
        <div class="form-body">
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Name of E-scooter
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Enter Name of E-scooter', 'id' => "name", 'name' => "name", 'class' => "form-control", 'value' => "$name")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Maker Name
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <?php echo form_input(array('placeholder' => 'Enter Maker Name', 'id' => "maker_name", 'name' => "maker_name", 'class' => "form-control", 'value' => "$maker_name")); ?>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>          
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Year of Made
              <span style="color:red">*
              </span>
            </label>
            <div class="col-md-10">
              <select class="form-control select2me" name="year_of_made" id="year_of_made">
                <?php echo isset($records->year_of_made) ?>
                <option value="2020">2020</option>
                <option value="2019">2019</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
              </select>
              <div class="form-control-focus"> 
              </div>
            </div>
          </div>         
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Image
            </label>
            <div class="col-md-10">
              <input type="file" name="image_name[]" id="image_name" class="multifile form-control" multiple="">
            </div>
          </div>  
          <?php if($task == 'edit') { ?>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">
            </label>
            <div class="col-md-10">
              <?php if (isset($image_data->image_name) && $image_data->image_name) { $images = explode(',', $image_data->image_name); ?>
              <?php foreach ($images as $value) { ?>
              <img height="100" width="200" src="<?php echo base_url('assets/uploads/scooter_images/'. $value);?>" alt="">
              <?php } } else { ?>
                
              <?php } ?>
            </div>
          </div>
          <?php  } ?>
          </div>
          <div class="form-group form-md-line-input">
            <label for="form_control_title" class="control-label col-md-2">Status
              <span style="color:red">
              </span>
            </label>
            <div class="md-radio-inline">
              <div class="md-radio">
                <input type="radio" name="status" id="status_active" class="md-radiobtn" value="Active"
                  <?php echo ($status == 'Active')?'checked':''; ?>>
                  <label for="status_active">
                    <span class="inc">                    
                    </span>
                    <span class="check">
                    </span>
                    <span class="box">
                    </span>Active
                  </label>
              </div>
              <div class="md-radio has-error">
                <input type="radio" name="status" id="status_inactive" class="md-radiobtn" value="Inactive"
                  <?php echo ($status == 'Inactive')?'checked':''; ?>>
                  <label for="status_inactive">
                    <span class="inc">
                    </span>
                    <span class="check">
                    </span>
                    <span class="box">
                    </span>Inactive
                  </label>
              </div>
            </div>                     
          <div class="form-actions noborder">
            <div class="row">
              <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn green">Submit
                </button>
                <a href="<?php echo base_url('admin/scooter'); ?>" class="btn default">Cancel
                </a>
              </div>
            </div>
          </div>        
        </form>
      </div>
    </div>
  </div>
</div>


