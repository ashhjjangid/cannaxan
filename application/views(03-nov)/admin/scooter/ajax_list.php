<?php if ($all_scooters) {?>
<?php foreach ($all_scooters as $key => $value) {?>
<tr class="my_row" data-id="<?php echo $value->id; ?>">
   <td><input type="checkbox" class="checkboxes recordcheckbox group-checkable" value="<?php echo $value->id; ?>"/></td>
   <td>
      <?php echo $value->maker_name; ?>
   </td>
   <td>
      <?php echo show_limited_text($value->name, 25); ?>
   </td>
   <!-- <td><?php //echo date("Y-m-d",strtotime($value->user_type)); ?></td> -->
   <td>
      <div class="btn-group">
      <?php if($value->status == 'Active') { ?>
         <button class="btn btn-xs btn-success status_label" type="button">Active</button>
      <?php } else { ?>
         <button class="btn btn-xs btn-danger status_label" type="button">Inactive</button>
      <?php } ?>
      
         <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-angle-down"></i></button>
         <ul class="dropdown-menu" role="menu" style="margin-top:-50px">
            
            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/scooter/change_status'); ?>" data-status="Active" onClick="changeStatusCommon(this)">Active</a></li>

            <li><a data-id="<?php echo $value->id; ?>" data-url="<?php echo site_url('admin/scooter/change_status'); ?>" data-status="Inactive" onClick="changeStatusCommon(this)">Inactive</a></li>
         </ul>
      </div>
   </td>
   <td><?php echo date('M d, Y H:i:s', strtotime($value->add_date)); ?></td>
   <td class="text-center">
      <a href="<?php echo base_url('admin/scooter/view/' . $value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body"><i class="fa fa-eye"></i></a>
      
      
      <a href="javascript:" class="btn btn-danger tooltips" data-original-title="Delete this record" data-placement="top" data-container="body" onclick="deleteUser(this)" data-id="<?php echo $value->id;  ?>" data-customer="<?php echo $value->id;  ?>" data-url="<?php echo base_url('admin/scooter/delete/'.$value->id) ?>"><i class="fa fa-trash"></i></a>
      
      <a href="<?php echo base_url('admin/scooter/update/' . $value->id) ?>" class="btn purple tooltips" data-original-title="Update this record" data-placement="top" data-container="body"><i class="fa fa-pencil"></i></a>
   
      <!-- <a href="<?php echo base_url('admin/scooter/delete/'. $value->id); ?>" class="btn red tooltips" data-original-title="Delete this Record" data-placement="top" data-container="body"><i class="fa fa-trash"></i></a> -->
   </td>
</tr>
<?php }?>
<?php } else {?>
<tr>
   <td colspan="9"><div class="no-record">No record found</div></td>
</tr>
<?php }?>

