<?php
	if (isset($message) && $message) {
		$alert = ($success)? 'alert-success':'alert-danger';
		echo '<div class="alert ' . $alert . '">' . $message . '</div>';
	}
?>
<div class="portlet light" style="height:45px">
	<div class="row">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
				<i class="fa fa-arrow-right"></i>
			</li>

			<li>
				<a href="<?php echo site_url('admin/cannabis_medikament')?>" class="tooltips" data-original-title="List of cannabis medikament" data-placement="top" data-container="body">List of cannabis medikament</a>
				<i class="fa fa-arrow-right"></i>
			</li>

			<li>
				<input type="hidden" name="" id="ids" value="<?php echo $id; ?>">

				<?php if ($id != '') { ?>
				 	<?php $fa_icon  = 'fa-pencil-square-o'; ?>
				 	<?php echo $record->name; ?>

				 <?php } else { ?>
				 	Add cannabis medikament
				 	<?php $fa_icon  = 'fa-plus'; ?>
				 <?php } ?>
			</li>	
			<li style="float:right;">
				<a class="btn red tooltips" href="<?php echo base_url('admin/cannabis_medikament'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
				</a>
			</li>				
		</ul>			
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="fa fa-file-image"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>				
			</div>
			<?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
				<div class="portlet light">
					<div class="portlet-title">
						<div class="row">
							<div class="col-md-6">
								<div class="caption font-red-sunglo">
									<i class="fa fa-file-image"></i>
									<span class="caption-subject bold font-purple-plum uppercase"><i class="fa <?php echo $fa_icon; ?>"></i>About the cannabis medikament
									</span>
								</div>
							</div>					
						</div>				
					</div>

					<!-- <div class="form-group form-md-line-input">
			            <label for="form_control_title" class="control-label col-md-2">Choose Business
			              <span style="color:red">*
			              </span>
			            </label>
			            <div class="col-md-10">
			              <?php $selected_user = isset($record->user_id) ? $record->user_id : ''; ?>
			              <select class="form-control" name="select_business">
			              	<option value="">Select Business</option>
			                  <?php if ($select_business) { ?>
			                    <?php foreach ($select_business as $key => $list_of_business) { ?>
			                      <option <?php echo ($list_of_business->id == $selected_user)?'selected':''; ?> value="<?php echo $list_of_business->id; ?>"><?php echo $list_of_business->name; ?></option>
			                      
			                    <?php } ?>
			                  <?php } ?>
			              </select>
			            </div>
			          </div> -->
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Name<span style="color:red">*</span></label>
					
								<div class="col-md-10">	
									<?php  $name = isset($record->name) ? $record->name : ''; ?>				
									<?php echo form_input(array('placeholder' => "Enter name", 'id' => "name", 'name' => "name", 'class' => "form-control", 'value' => "$name")); ?>
									<div class="form-control-focus"> </div>			
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label for="form_control_title" class="control-label col-md-2">Status</label>
								<div class="md-radio-inline">
									<div class="md-radio">
										<input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" <?php echo ($status == 'Active')?'checked':''; ?>>
										<label for="radio19">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Active</label>
									</div>
									<div class="md-radio has-error">
										<input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" <?php echo ($status == 'Inactive')?'checked':''; ?>>
										<label for="radio20">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Inactive</label>
									</div>
								</div>
							</div> 
							<div class="form-actions noborder">
								<div class="row">
									<div class="col-md-offset-2 col-md-10">
										<button type="submit" class="btn green">Submit</button>
										<a href="<?php echo base_url('admin/cannabis_medikament'); ?>" class="btn default">Cancel</a>
								    </div>
								</div>
						    </div>
						</div>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
<script src="https://maps.googleapis.com/maps/api/js?v=3&key=<?php echo getSiteOption('google_map_api_key', true); ?>&sensor=false&libraries=places"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
		var input = document.getElementById('address');
        var places = new google.maps.places.Autocomplete(input);        
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
           var latitude = place.geometry.location.lat();
           var longitude = place.geometry.location.lng();
           $('#latitude').val(latitude);
           $('#longitude').val(longitude);
        });
    });

   	function updateLAT($this) {
   		var a = $($this).val();
   		$("#latitude").val('');
   		$("#longitude").val('');
   	}
 </script>

 <script type="text/javascript">

	$(function(){
	    $('#start_date').datetimepicker({
	       // startDate: new Date(),
		   // autoclose: true,
		   // format: "yyyy-mm-dd hh:ii",
	    });

	    $('#end_date').datetimepicker({
	       // startDate: new Date(),
		   // autoclose: true,
		   // format: "yyyy-mm-dd hh:ii",
	    });

	    $('#add_time').datetimepicker({

	    });
	});
 </script>

