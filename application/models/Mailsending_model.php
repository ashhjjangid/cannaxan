<?php
class Mailsending_model extends CI_Model {
    var $from = "";
    //var $sendermail = "test@shopformeslc.com";
    var $sender_name    = 'Cannaxan';
    var $sitelogo       = '';
    var $site_title     = '';
    var $contact_email  = '';
    var $banner_image   = '';
    var $mailtype       = 'html';
    var $social_icons   = '';
     
    function __construct() {
        $this->load->library('email');
        $config['protocol']     = "smtp";
        $config['smtp_host']    = getSiteOption('smtp_host',true);
        $config['smtp_port']    = getSiteOption('smtp_port',true);
        $config['smtp_user']    = getSiteOption('smtp_user',true);
        $config['smtp_pass']    = getSiteOption('smtp_pass',true);          
        $config['charset']      = "utf-8";
        $this->email->initialize($config);  
    }

    /**
    * Send email using Mailsending_model Model.
    * @category   apps
    * @package    Send email
    * @subpackage Model
    * @author     Created By Reena Singh (03-04-2019)
    * @copyright  2019 Salon
    */

    function sendEmail($emailAddress,$data,$subject, $attachment = null, $cc = null) {
        
        if ($attachment) {
            $this->email->clear(TRUE);
        }
        $smtpUser = getSiteOption('smtp_user',true);
        $this->email->from($smtpUser,'Cannaxan');
        $this->email->to($emailAddress);
        
        if ($cc) {
            $this->email->cc($cc);
        }
        
        $this->email->mailtype = $this->mailtype;
        $this->email->set_newline("\r\n");
        $this->email->subject($subject);
        $this->email->message($data);
        
        if ($attachment) {
            //foreach ($attachment as $key => $value) {
             $this->email->attach($attachment);
            //}
        }
        $this->email->send();
    }   

    /**
    * Get single record by id using Mailsending_model Model.
    * @category   apps
    * @package    Get single record by id
    * @subpackage Model
    * @author     Created By Reena Singh (03-04-2019)
    * @copyright  2019 Salon
    */
    function getUserRecordById($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_users');
        $result = $query->row();
        return $result;
    }

    function getSingleUserRecordById($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_members');
        $result = $query->row();
        return $result;
    }

    /**
    * Get single record by slug using Mailsending_model Model.
    * @category   apps
    * @package    Get single record by slug
    * @subpackage Model
    * @author     Created By Reena Singh (03-04-2019)
    * @copyright  2019 Salon
    */
    function templatechoose($slug) {
        $this->db->where('slug', $slug);
        $query = $this->db->get('tbl_emails');
        return $query->row();
    }
    
    /**
    * Forgot password email for admin using Mailsending_model Model.
    * @category   apps
    * @package    Forgot password email for admin
    * @subpackage Model
    * @author     Created By Reena Singh (03-04-2019)
    * @copyright  2019 Salon
    */
    function SetForgetPasswordMail($userId, $forgotPasswordKey, $url) {
        $template = $this->templatechoose('admin-forgot-password');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $emailAddress = $user->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/logo.png" alt="logo" class="logo-default"/>';
        $resetURL = $url;
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{Forgot_Password}}', $resetURL, $str);
        $this->sendEmail($emailAddress, $str, $subject);

    }

    function SetUserForgetPasswordMail($userId, $forgotPasswordKey, $url) {
        $template = $this->templatechoose('doctor-forgot-password');
        $subject = $template->subject;
        $user = $this->getSingleUserRecordById($userId);
        $firstName = $user->first_name;
        $lastName = $user->name;
        $emailAddress = $user->email;
        $default_password = $forgotPasswordKey;
        $siteURL = '<a href="'.site_url().'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/logo.png" alt="logo" class="logo-default"/>';
        $impressum = '<a href="https://www.cannaxan.de/gmbh/impressum">Impressum</a>';
        $resetURL = $url;
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Default_Password}}', $default_password, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{Set_Password}}', $resetURL, $str);
        $str = str_replace('{{Impressum}}', $impressum, $str);
        $this->sendEmail($emailAddress, $str, $subject);

    }

    function SetUserPasswordMail($userId, $setPasswordKey) {
        $user = $this->getSingleUserRecordById($userId);
        $andre = $user->lecture;
        $dr_title = (($user->title == 'keine')?'':$user->title);
        $firstName = $user->first_name;
        $lastName = $user->name;
        $dr_emailAddress = $user->email;
        $dr_mail_to_emailAddress = '<a href="mailto:'.$user->email.'">'.$user->email.'</a>';
        $current_date = date('Y-m-d');
        $template = $this->templatechoose('user-password');
        $subject = $template->subject;
        $siteURL = '<a href="'.site_url().'">Cannaxan</a>';
        $Login_Url = '<a href="'.base_url('doctors/login').'">Hier</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/logo.png" alt="logo" class="logo-default"/>';
        $default_password = $setPasswordKey;
        $emailAddress = '<a href="mailto:info@cannaxan.de">info@cannaxan.de</a>';
        $impressum = '<a href="https://www.cannaxan.de/gmbh/impressum">Impressum</a>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Andre}}', $andre, $str);
        $str = str_replace('{{Title}}', $dr_title, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Dr_Email_Address}}', $dr_mail_to_emailAddress, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Password}}', $default_password, $str);
        $str = str_replace('{{Login_Url}}', $Login_Url, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{Impressum}}', $impressum, $str);
        $this->sendEmail($dr_emailAddress, $str, $subject);

    }

    function SendUserPasswordChangeReminderMail($userId) {
        $user = $this->getSingleUserRecordById($userId);
        $andre = $user->lecture;
        $dr_title = (($user->title == 'keine')?'':$user->title);
        $firstName = $user->first_name;
        $lastName = $user->name;
        $dr_emailAddress = $user->email;
        $current_date = date('Y-m-d');
        $password_change_date = date('Y-m-d', strtotime($user->next_password_change));

        if ($password_change_date == $current_date) {
            
            $template = $this->templatechoose('change-password');
        } else {
            $template = $this->templatechoose('change-password-before-a-week');            
        }
        $subject = $template->subject;
        $siteURL = '<a href="'.site_url().'">Cannaxan</a>';
        $New_Password_Url = '<a href="'.base_url('doctors/set_new_password').'">Hier</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/logo.png" alt="logo" class="logo-default"/>';
        $emailAddress = '<a href="mailto:info@cannaxan.de">info@cannaxan.de</a>';
        $impressum = '<a href="https://www.cannaxan.de/gmbh/impressum">Impressum</a>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Andre}}', $andre, $str);
        $str = str_replace('{{Title}}', $dr_title, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        //$str = str_replace('{{Dr_Email_Address}}', $dr_mail_to_emailAddress, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        //$str = str_replace('{{Password}}', $default_password, $str);
        $str = str_replace('{{New_Password_Url}}', $New_Password_Url, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{Impressum}}', $impressum, $str);
        $this->sendEmail($dr_emailAddress, $str, $subject);

    }

    function sendContractToUser($id, $email, $attachment = null, $verfication_key = null) {

        //$this->email->clear(TRUE);
        $template = $this->templatechoose('new-doctor-contract');
        $subject = $template->subject;
        $user = $this->getSingleUserRecordById($id);
        $file_path = ASSETSPATH. 'uploads/pdf/';
        
        if ($user->lecture == 'Herr') {
            $andre = 'Sehr geehrter '.$user->lecture;
        } else {            
            $andre = 'Sehr geehrte '.$user->lecture;
        }
        if ($user->title == 'keine') {
            $dr_title = '';            
        } else {
            $dr_title = ' '.$user->title;
        }
        $lastname = $user->name;
            
        //$pdf_name = $user->contract_file;
        //$attachment_arr = $attachment.'.pdf';
        $attachment_arr = $file_path.$attachment.".pdf";

        if ($verfication_key) {
            $verfication_url = base_url('doctors/verify?key='.$verfication_key);
        } else {
            $verfication_url = '';
        }

        $v_url = '<a href="'.$verfication_url.'">Klicken Sie Hier, um Ihr Konto zu bestätigen.</a>';
        $current_date = date('d.m.Y');
        $emailAddress = '<a href="mailto:info@cannaxan.de">info@cannaxan.de</a>';
        $impressum = '<a href="https://www.cannaxan.de/gmbh/impressum">Impressum</a>';
        $str = $template->content;
        $str = str_replace('{{Andre}}', $andre, $str);
        $str = str_replace('{{Andre}}', $andre, $str);
        $str = str_replace('{{Verification_Url}}', $v_url, $str);
        $str = str_replace('{{Last_Name}}', $lastname, $str);
        $str = str_replace('{{Title}}', $dr_title, $str);
        $str = str_replace('{{Current_Date}}', $current_date, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Impressum}}', $impressum, $str);
        //$this->email->set_newline("\r\n");
        //$this->email->attach($file_path.$pdf_name);
        $this->sendEmail($email, $str, $subject, $attachment_arr);

    }

    function sendEmailToAdminToVerifyDoctor($userId, $email) {
        // pr($userId);
        // pr($emailVerifiedKey);die;
        $template = $this->templatechoose('welcome-new-doctor');
        $subject = $template->subject;
        $user = $this->getSingleUserRecordById($userId);
        $firstName = $user->first_name;
        $lastName = $user->name;
        $emailAddress = $user->email;
        $hausno = $user->hausnr;
        $plz = $user->plzl;
        $dr_lanr = $user->lanr;
        $dr_practice = $user->name_doctor_practice;
        $dr_subject = $user->subject;
        $dr_ort = $user->ort;
        $dr_strabe = $user->road;
        $dr_phone_number = $user->phonenumber;
        $siteURL = '<a href="'.base_url('admin').'">Visit Cannaxan</a>';
        $impressumURL = '<a href="https://www.cannaxan.de/gmbh/impressum">Impressum</a>';
        $unapproved_users = '<a href="'.base_url('admin/users/unapproved_users').'">'.base_url('admin/users/unapproved_users').'</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/logo.png" alt="logo" class="logo-default"/>';
        $subject_str = $template->subject;
        $subject_str = str_replace('{{Title}}', (($user->title == 'keine')?'':$user->title), $subject_str);
        $subject_str = str_replace('{{First_Name}}', $firstName, $subject_str);
        $subject_str = str_replace('{{Last_Name}}', $lastName, $subject_str);
        $subject_str = str_replace('{{plzl}}', $plz, $subject_str);
        
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{lanr}}', $dr_lanr, $str);
        $str = str_replace('{{Title}}', (($user->title == 'keine')?'':$user->title), $str);
        $str = str_replace('{{specialization}}', $dr_subject, $str);
        $str = str_replace('{{EMAIL}}', $emailAddress, $str);
        $str = str_replace('{{phone_number}}', $dr_phone_number, $str);
        $str = str_replace('{{hausnr}}', $hausno, $str);
        $str = str_replace('{{strabe}}', $dr_strabe, $str);
        $str = str_replace('{{plzl}}', $plz, $str);
        $str = str_replace('{{ort}}', $dr_ort, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{DoctorList}}', $unapproved_users, $str);
        $str = str_replace('{{Email_Address}}', '<a href="mailto:info@cannaxan.de">info@cannaxan.de</a>', $str);
        $str = str_replace('{{Impressum}}', $impressumURL, $str);
        $this->sendEmail('app@cannaxan.de',$str,$subject_str);              
    }

     public function sendPromotionTypeMail($user_first_name, $user_last_name, $user_email, $subject, $message)
     {
        $subject = $subject;
        $siteURL = '<a href="'.base_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('admin_assets').'global/img/logo.png" alt="logo" class="logo-default"/>';

        $str = $message;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{user_first_name}}', $user_first_name, $str);
        $str = str_replace('{{user_last_name}}', $user_last_name, $str);
        $str = str_replace('{{$user_email}}', $user_email, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($user_email, $str , $subject);
     }

    /**
    * Send new service provider mail for admin using mailsending_model Model.
    * @category   apps
    * @package    Send new service provider
    * @subpackage Model
    * @author     Created By Reena Singh (03-04-2019)
    * @copyright  2019 Salon
    */ 
    function addServiceProviderMailSending($firstname,$lastname,$email,$pass_key){
        $template = $this->templatechoose('admin-welcome-new-service-provider');
        $subject = $template->subject;
        $firstName = $firstname;
        $lastName = $lastname;
        $emailAddress = $email;
        $passLink = '<a href="'.site_url('admin').'setpassword/'.$pass_key.'">Click here to generate password</a>';
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $link = '<a href="'.site_url('login').'">Redirect Link</a>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Password_URL}}', $passLink, $str);
        $str = str_replace('{{Login_Url}}', $link, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }

    function NewUserAddedByAdmin($firstname,$lastname,$email,$pass_key){
        $template = $this->templatechoose('admin-welcome-new-user');
        $subject = $template->subject;
        $firstName = $firstname;
        $lastName = $lastname;
        $emailAddress = $email;
        $passLink = $pass_key;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.site_url('admin').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $link = '<a href="'.site_url('login').'">Redirect Link</a>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Password_URL}}', $passLink, $str);
        $str = str_replace('{{Login_Url}}', $link, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        //pr($str);die;
        $this->sendEmail($emailAddress,$str,$subject);              
    }
    /**
    * Send new customer mail for admin using mailsending_model Model.
    * @category   apps
    * @package    Send new customer
    * @subpackage Model
    * @author     Created By Reena Singh (04-04-2019)
    * @copyright  2019 Salon
    */ 

    function sendNewCustomerMailSending($userId,$user_info, $password){
        $template = $this->templatechoose('admin-welcome-new-customer');
        $subject = $template->subject;
        $firstName = $user_info['first_name'];
        $lastName = $user_info['last_name'];
        $emailAddress = $user_info['email'];
        $password = $password;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $link = '<a href="'.site_url('login').'">Redirect Link</a>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Password}}', $password, $str);
        $str = str_replace('{{Login_Url}}', $link, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }

    /**
    * Send new customer mail for salon using mailsending_model Model.
    * @category   apps
    * @package    Send new customer
    * @subpackage Model
    * @author     Created By Narendra Sharma (18-06-2019)
    * @copyright  2019 Salon
    */ 

    function sendNewCustomerMailSendingBySalon($userId,$user_info, $password){
        $template = $this->templatechoose('salon-welcome-new-customer');
        $subject = $template->subject;
        $firstName = $user_info['first_name'];
        $lastName = $user_info['last_name'];
        $emailAddress = $user_info['email'];
        $password = $password;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $link = '<a href="'.site_url('login').'">Redirect Link</a>';
        $verify = '<a href="'.site_url('verify-account/'.$user_info['email_verified_key']).'">Verify</a>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Password}}', $password, $str);
        $str = str_replace('{{Login_Url}}', $link, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{Verify_URL}}', $verify, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }

    /**
    * Send new sub admin mail for sub admin using mailsending_model Model.
    * @category   apps
    * @package    Send mail
    * @subpackage Model
    * @author     Created By Reena Singh (04-04-2019)
    * @copyright  2019 Salon
    */ 
    function sendSubAdminMailSending($userId,$username,$emailAddress,$password){
        $template = $this->templatechoose('welcome-new-sub-admin');
        $subject = $template->subject;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $link = '<a href="'.site_url('admin/login').'">Redirect Link</a>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{User_Name}}', $username, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Password}}', $password, $str);
        $str = str_replace('{{Sub_Admin_Url}}', $link, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }
    
    /**
    * Send newsletter subscriber mail using mailsending_model Model.
    * @category   apps
    * @package    Send newsletter subscriber mail
    * @subpackage Model
    * @author     Created By Reena Singh (04-04-2019)
    * @copyright  2019 Salon
    */ 
    function sendNewsletterMailToSubcriberByAdmin($newsletter_message,$newsletter_subject,$user_email){
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $newsletter_message;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Subject}}', $newsletter_subject, $str);
        $str = str_replace('{{Message}}', $newsletter_message, $str);
        $str = str_replace('{{Email_Address}}', $user_email, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($user_email,$str,$newsletter_subject); 
    }

    /**
    * Send reply contact query mail using mailsending_model Model.
    * @category   apps
    * @package    Send car parts request mail
    * @subpackage Model
    * @author     Created By Reena Singh (08-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function replyContactQuery($replyMessage,$id) {
        $template = $this->templatechoose('reply-contact-query');
        $contactQuery = $this->getContactQueryById($id);
        
        if ($contactQuery) {
            $user = $this->getUserRecordById($contactQuery->user_id);
            
            if ($user) {
                $firstName = $user->first_name;
                $lastName = ($user->last_name)?$user->last_name:'';
            
            } else {
                $firstName = ($contactQuery->name)?$contactQuery->name:'';
                $lastName = "";
            }           
        }       
        $replyMessage = $replyMessage;
        $emailAddress = $contactQuery->email;   
        $replySubject = $template->subject;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $visitor_query = $contactQuery->message;
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}',$emailAddress, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{Visitor_query}}', $visitor_query, $str);
        $str = str_replace('{{Reply_Message}}', $replyMessage, $str);
        $str = str_replace('{{Reply_Subject}}', $replySubject, $str);
        $this->sendEmail($emailAddress,$str,$replySubject);
    }

    //mail ending 


    function get_user_by_id($id){
        $this->db->select('tbl_users.id,tbl_users.first_name,tbl_users.last_name,tbl_users.username,tbl_users.email,tbl_users.country_id,tbl_users.city_id,tbl_users.phone_number,tbl_users.address,tbl_users.profile_image');
        $this->db->select('tbl_country.name as country_name');
        $this->db->select('tbl_city.name as city_name');
        $this->db->join('tbl_country','tbl_country.id = tbl_users.country_id');
        $this->db->join('tbl_city','tbl_city.id = tbl_users.city_id');
        $this->db->where('tbl_users.id',$id);
        $query = $this->db->get('tbl_users');
        return $query->row();
    }
    
    function getContactQueryById($id) {
        $this->db->select('*');
        $this->db->where('id',$id);
        $query = $this->db->get('tbl_contact_us');
        $result = $query->row();
        return $result;
    }

    function get_online_booking_by_id($id){
        $this->db->select('tbl_online_booking.id,tbl_online_booking.first_name,tbl_online_booking.last_name,tbl_online_booking.state,tbl_online_booking.phone_number,tbl_online_booking.email,tbl_online_booking.year,tbl_city.name as city_name,tbl_make.name as make_name,tbl_model.name as model_name, tbl_exterior_color.name as color,tbl_mileage.mileage,tbl_option.id as option_id,tbl_option_title.title');
        $this->db->join('tbl_city','tbl_city.id = tbl_online_booking.city_id');
        $this->db->join('tbl_make','tbl_make.id = tbl_online_booking.make_id');
        $this->db->join('tbl_model','tbl_model.id = tbl_online_booking.model_id');
        $this->db->join('tbl_exterior_color','tbl_exterior_color.id = tbl_online_booking.color_id');
        $this->db->join('tbl_mileage','tbl_mileage.id = tbl_online_booking.mileage_id');
        $this->db->join('tbl_option','tbl_option.id = tbl_online_booking.option_id');
        $this->db->join('tbl_option_title','tbl_option_title.option_id = tbl_option.id');
        $this->db->where('tbl_online_booking.id',$id);
        $query = $this->db->get('tbl_online_booking');
        return $query->row();
    }

    /**
    * Get single record by id using Mailsending_model Model.
    * @category   apps
    * @package    Get single record by id
    * @subpackage Model
    * @author     Created By Reena Singh (01-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function get_request_by_id($id) {
        $this->db->select('tbl_car_parts_request.*');
        $this->db->select('tbl_users.first_name,tbl_users.last_name,tbl_users.username,tbl_users.email');
        $this->db->select('tbl_make.name as make_name');
        $this->db->select('tbl_model.name as model_name');
        $this->db->select('tbl_trim.name as trim_name');
        $this->db->select('tbl_engine.name as engine_number');
        $this->db->select('tbl_parts_color.name as parts_color');
        $this->db->select('tbl_oem.name as oem');
        $this->db->join('tbl_users','tbl_users.id = tbl_car_parts_request.user_id');
        $this->db->join('tbl_make','tbl_make.id = tbl_car_parts_request.make_id');
        $this->db->join('tbl_model','tbl_model.id = tbl_car_parts_request.model_id');
        $this->db->join('tbl_trim','tbl_trim.id = tbl_car_parts_request.trim_id');
        $this->db->join('tbl_engine','tbl_engine.id = tbl_car_parts_request.engine_number');
        $this->db->join('tbl_parts_color','tbl_parts_color.id = tbl_car_parts_request.color_id');
        $this->db->join('tbl_oem','tbl_oem.id = tbl_car_parts_request.oem_id');
        $this->db->where('tbl_car_parts_request.id',$id);
        $query = $this->db->get('tbl_car_parts_request');
        return $query->row();
    }

    // function getLeaseDetailsById($id)
    // {
    //  $this->db->select('tbl_user_lease_quotes.*');
    //  $this->db->select('current_model.model_name as current_model_name');
    //  $this->db->join('tbl_car_models as current_model','tbl_user_lease_quotes.current_model_id = current_model.id','left');
    //  $this->db->select('new_model.model_name as new_model_name');
    //  $this->db->join('tbl_car_models as new_model','tbl_user_lease_quotes.new_model_id = new_model.id','left');
    //  $this->db->select('current_maker.maker_name as current_maker_name');
    //  $this->db->join('tbl_car_makers  as current_maker','tbl_user_lease_quotes.current_maker_id = current_maker.id','left');
    //  $this->db->select('new_maker.maker_name as new_maker_name');
    //  $this->db->join('tbl_car_makers as new_maker','tbl_user_lease_quotes.new_maker_id = new_maker.id','left');
    //  $this->db->select('new_milease.milease as new_milease_name');
    //  $this->db->join('tbl_milease as new_milease','tbl_user_lease_quotes.new_lease_milease = new_milease.id','left');
    //  $this->db->select('current_year.year as current_year_name');
    //  $this->db->join('tbl_year as current_year', 'current_year.id = tbl_user_lease_quotes.current_model_year','left');
    //  $this->db->select('new_year.year as new_year_name');
    //  $this->db->join('tbl_year as new_year', 'new_year.id = tbl_user_lease_quotes.new_model_year','left');
    //  $this->db->select('new_down_payment.down_payment as new_down_payment_name');
    //  $this->db->join('tbl_down_payment as new_down_payment', ' tbl_user_lease_quotes.new_lease_down_payment = new_down_payment.id ','left');
    //  $this->db->where('tbl_user_lease_quotes.id',$id);
    //  $query = $this->db->get('tbl_user_lease_quotes');
    //  $result = $query->row();
    //  return $result;
    // }
    
    

    function sendXmlEmail($emailAddress,$data,$subject) {
        $this->email->clear(TRUE);
        $smtpUser = getSiteOption('smtp_user',true);
        $this->email->from($smtpUser,'Frill');
        $this->email->to($emailAddress);
        $this->email->mailtype = $this->mailtype;
        $this->email->set_mailtype("text");
        $this->email->subject($subject);
        $this->email->message($data);
        $this->email->send();
    }

    //Email verified link sending according to language
    function sendEmailUserVerifiedLink($userId,$emailVerifiedKey) {
        // pr($userId);
        // pr($emailVerifiedKey);die;
        $template = $this->templatechoose('welcome-new-user');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $emailAddress = $user->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $link = '<a href="'.site_url('verify-account/'. $emailVerifiedKey).'">Verified Link</a>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{Email_verified}}', $link, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }

    

    // function send_email_dealer_verified($dealerId,$emailVerifiedKey) {

    //  $template       = $this->templatechoose('welcome-new-dealer');
    //  $subject        = $template->subject;
    //  $dealer         = $this->getDealerRecordById($dealerId);
    //  $firstName      = $dealer->first_name;
    //  $lastName       = $dealer->last_name;
    //  $emailAddress   = $dealer->email;
    //  $siteURL        = '<a href="'.site_url('admin').'">Visit Boconrd</a>';
    //  // $logoURL = '';
    //  $logoURL        = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
    //  $link   = '<a href="'.site_url('dealer_register/verify_account/'. $emailVerifiedKey).'">Verified Link</a>';
    //  $str    = $template->content;
    //  $str    = str_replace('{{Logo}}', $logoURL, $str);
    //  $str    = str_replace('{{First_Name}}', $firstName, $str);
    //  $str    = str_replace('{{Last_Name}}', $lastName, $str);
    //  $str    = str_replace('{{Email_Address}}', $emailAddress, $str);
    //  $str    = str_replace('{{Website_URL}}', $siteURL, $str);
    //  $str        = str_replace('{{Email_verified}}', $link, $str);
    //  $this->sendEmail($emailAddress,$str,$subject);
    // }


    function get_admin_detail(){
        $type = 'Admin';
        $this->db->select('email,username');
        $this->db->where('user_type', $type);
        $query = $this->db->get('tbl_users');
        return $query->row();
        
    }

    // function sendDealersEmail($lease_id,$emailAddress) {
    //  $template                   = $this->templatechoose('dealer-details');
    //  $subject                    = $template->subject;
    //  $lease                      = $this->getLeaseDetailsById($lease_id);
    //  $firstName                  = $lease->first_name;
    //  $lastName                   = $lease->last_name;
    //  $email                      = $lease->email;
    //  $phoneNumber                = $lease->phone_number;
    //  $zipCode                    = $lease->zip_code;
    //  $current_year               = $lease->current_year_name;
    //  $current_maker              = $lease->current_model_name;
    //  $current_model              = $lease->current_maker_name;
    //  $current_milease            = $lease->current_milease;
    //  $current_down_payment_name  = $lease->down_payment;
    //  $year                       = $lease->new_year_name;
    //  $maker                      = $lease->new_maker_name;
    //  $model                      = $lease->new_model_name;
    //  $new_milease                = $lease->new_milease_name;
    //  $new_down_payment_name      = $lease->new_down_payment_name;
    //  $siteURL                    = '<a href="'.site_url('admin').'">Visit Boconrd</a>';
    //  $logoURL                    = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
    //  $str                        = $template->content;
    //  $str                        = str_replace('{{Logo}}', $logoURL, $str);
    //  $str                        = str_replace('{{First_Name}}', $firstName, $str);
    //  $str                        = str_replace('{{Last_Name}}', $lastName, $str);
    //  $str                        = str_replace('{{Email_Address}}', $email, $str);
    //  $str                        = str_replace('{{Phone_Number}}', $phoneNumber, $str);
    //  $str                        = str_replace('{{Zip_Code}}', $zipCode, $str);
    //  $str                        = str_replace('{{Current_Year}}', $current_year, $str);
    //  $str                        = str_replace('{{Current_Maker_Name}}', $current_maker, $str);
    //  $str                        = str_replace('{{Current_Model_Name}}', $current_model, $str);
    //  $str                        = str_replace('{{Current_Milease}}', $current_milease, $str);
    //  $str                        = str_replace('{{Current_Down_Payment}}', $current_down_payment_name, $str);
    //  $str                        = str_replace('{{New_Year}}', $year, $str);
    //  $str                        = str_replace('{{New_Maker_Name}}', $maker, $str);
    //  $str                        = str_replace('{{New_Model_Name}}', $model, $str);
    //  $str                        = str_replace('{{New_Milease}}', $new_milease, $str);
    //  $str                        = str_replace('{{New_Down_Payment}}', $new_down_payment_name, $str);
    //  $str                        = str_replace('{{Website_URL}}', $siteURL, $str);
    //  $this->sendEmail($emailAddress,$str,$subject);
    // }
    
    // function createDelaerRequestTemplate($lease_detail){
    //  $template   = $this->templatechoose('dealer-details');
    //  $subject    = $template->subject;
    //  $siteURL    = '<a href="'.site_url('admin').'">Visit Boconrd</a>';
    //  $logoURL    = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';

    //  $str        = $template->content;
    //  if($lease_detail->is_down_payment_price == "Yes") {
    //      $down_payment = $lease_detail->new_lease_down_payment_price;
    //  } else {
    //      $down_payment = $lease_detail->new_down_payment_name;
    //  }

    //  $str        = str_replace('{{Logo}}', $logoURL, $str);
    //  $str        = str_replace('{{Current_Year}}', $lease_detail->current_year_name, $str);
    //  $str        = str_replace('{{Current_Maker_Name}}', $lease_detail->current_maker_name, $str);
    //  $str        = str_replace('{{Current_Model_Name}}', $lease_detail->current_model_name, $str);
    //  $str        = str_replace('{{Current_Trim}}', $lease_detail->current_trim_name, $str);
    //  $str        = str_replace('{{Current_Months_Remaining}}', $lease_detail->current_month_remaining_name, $str);
    //  $str        = str_replace('{{Current_Monthly_Payment}}', $lease_detail->down_payment, $str);
    //  $str        = str_replace('{{Current_Milease}}', $lease_detail->current_milease, $str);
    //  $str        = str_replace('{{New_Year}}', $lease_detail->new_year_name, $str);
    //  $str        = str_replace('{{New_Maker_Name}}', $lease_detail->new_maker_name, $str);
    //  $str        = str_replace('{{New_Model_Name}}', $lease_detail->new_model_name, $str);
    //  $str        = str_replace('{{New_Trim_Name}}', $lease_detail->new_trim_name, $str);
    //  $str        = str_replace('{{New_Additional_Options}}', $lease_detail->new_additional_options, $str);
    //  $str        = str_replace('{{New_Term}}', $lease_detail->term, $str);
    //  $str        = str_replace('{{New_Milease}}', $lease_detail->new_milease_name, $str);
    //  $str        = str_replace('{{New_Down_Payment}}', $down_payment, $str);
    //  $str        = str_replace('{{Firstname}}', $lease_detail->first_name, $str);
    //  $str        = str_replace('{{Lastname}}', $lease_detail->last_name, $str);
    //  $str        = str_replace('{{Email}}', $lease_detail->email, $str);
    //  $str        = str_replace('{{Phone}}', $lease_detail->phone_number, $str);
    //  $str        = str_replace('{{Zipcode}}', $lease_detail->zip_code, $str);
    //  $str        = str_replace('{{Website_URL}}', $siteURL, $str);

    //  $data['subject'] = $subject;
    //  $data['str'] = $str;
    //  return $data;
    // }
    
    // function createDelaerWidgetRequestTemplate($lease_detail){
    //  $template   = $this->templatechoose('dealer-widget-details');
    //  $subject    = $template->subject;
    //  $siteURL    = '<a href="'.site_url('admin').'">Visit Boconrd</a>';
    //  $logoURL    = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';

    //  $str        = $template->content;
    //  if($lease_detail->is_down_payment_price == "Yes") {
    //      $down_payment = $lease_detail->new_lease_down_payment_price;
    //  } else {
    //      $down_payment = $lease_detail->new_down_payment_name;
    //  }

    //  $str        = str_replace('{{Logo}}', $logoURL, $str);
    //  $str        = str_replace('{{Current_Year}}', $lease_detail->current_year_name, $str);
    //  $str        = str_replace('{{Current_Maker_Name}}', $lease_detail->current_maker_name, $str);
    //  $str        = str_replace('{{Current_Model_Name}}', $lease_detail->current_model_name, $str);
    //  $str        = str_replace('{{Current_Trim}}', $lease_detail->current_trim_name, $str);
    //  $str        = str_replace('{{Current_Months_Remaining}}', $lease_detail->current_month_remaining_name, $str);
    //  $str        = str_replace('{{Current_Monthly_Payment}}', $lease_detail->down_payment, $str);
    //  $str        = str_replace('{{Current_Milease}}', $lease_detail->current_milease, $str);
    //  $str        = str_replace('{{New_Year}}', $lease_detail->new_year_name, $str);
    //  $str        = str_replace('{{New_Maker_Name}}', $lease_detail->new_maker_name, $str);
    //  $str        = str_replace('{{New_Model_Name}}', $lease_detail->new_model_name, $str);
    //  $str        = str_replace('{{New_Trim_Name}}', $lease_detail->new_trim_name, $str);
    //  $str        = str_replace('{{New_Additional_Options}}', $lease_detail->new_additional_options, $str);
    //  $str        = str_replace('{{New_Term}}', $lease_detail->term, $str);
    //  $str        = str_replace('{{New_Milease}}', $lease_detail->new_milease_name, $str);
    //  $str        = str_replace('{{New_Down_Payment}}', $down_payment, $str);
    //  $str        = str_replace('{{Firstname}}', $lease_detail->first_name, $str);
    //  $str        = str_replace('{{Lastname}}', $lease_detail->last_name, $str);
    //  $str        = str_replace('{{Email}}', $lease_detail->email, $str);
    //  $str        = str_replace('{{Phone}}', $lease_detail->phone_number, $str);
    //  $str        = str_replace('{{Zipcode}}', $lease_detail->zip_code, $str);
    //  $str        = str_replace('{{Website_URL}}', $siteURL, $str);

    //  $data['subject'] = $subject;
    //  $data['str'] = $str;
    //  return $data;
    // }

    
    

    /**
    * Send new user verified mail using mailsending_model Model.
    * @category   apps
    * @package    Send new user mail
    * @subpackage Model
    * @author     Created By Reena Singh (22-01-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_email_user_verified_link($userId,$emailVerifiedKey) {
        $template = $this->templatechoose('welcome-new-user');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $emailAddress = $user->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $link = '<a href="'.site_url('verify-account/'. $emailVerifiedKey).'">Verified Link</a>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{Email_verified}}', $link, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }

    /**
    * Send new user profile update mail using mailsending_model Model.
    * @category   apps
    * @package    Send new user mail
    * @subpackage Model
    * @author     Created By Reena Singh (22-01-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_email_user_profile_update($userId,$user_info,$newPassword) {
        $template = $this->templatechoose('user-profile-update');
        $subject = $template->subject;
        $user = $this->get_user_by_id($userId);
        $profileImage = $user_info['profile_image'];
        $firstName = $user_info['first_name'];
        $lastName = $user_info['last_name'];
        $userName = $user_info['username'];
        $emailAddress = $user_info['email'];
        $countryName = $user->country_name;
        $cityName = $user->city_name;
        $address = $user_info['address'];
        $phoneNumber = $user_info['phone_number'];
        $password = $newPassword;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $imageURL = '<img src="'.$this->config->item('uploads').'user_image/'.$profileImage.'" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{User_Name}}', $userName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Country_Name}}', $countryName, $str);
        $str = str_replace('{{City_Name}}', $cityName, $str);
        $str = str_replace('{{Address}}', $address, $str);
        $str = str_replace('{{Phone_Number}}', $phoneNumber, $str);
        $str = str_replace('{{Password}}', $password, $str);
        $str = str_replace('{{Profile_Image}}', $imageURL, $str);

        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }

    /**
    * Send new user mail using mailsending_model Model.
    * @category   apps
    * @package    Send new user mail
    * @subpackage Model
    * @author     Created By Reena Singh (22-01-2019)
    * @copyright  2019 Bellezayofertas
    */ 
    function forgot_password_user_mail_sending($userId, $forgotPasswordKey, $url) {
        $template = $this->templatechoose('forgot-password');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $emailAddress = $user->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $resetURL = $url;
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $str = str_replace('{{Forgot_Password}}', $resetURL, $str);
        $this->sendEmail($emailAddress, $str, $subject);
    }

    /**
    * Send car parts request mail using mailsending_model Model.
    * @category   apps
    * @package    Send car parts request mail
    * @subpackage Model
    * @author     Created By Reena Singh (1-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_car_parts_request_mail($request_id,$userId) {
        $template = $this->templatechoose('car-parts_request');
        $subject = $template->subject;
        $request = $this->get_request_by_id($request_id);
        $firstName = $request->first_name;
        $lastName = $request->last_name;
        $emailAddress = $request->email;
        $userName = $request->username;
        $makeName = $request->make_name;
        $modelName = $request->model_name;
        $trimName = $request->trim_name;
        $engineNumber = $request->engine_number;
        $vinNumber = $request->vin_number;
        $partColor = $request->parts_color;
        $oem = $request->oem;
        $admin_detail   = $this->get_admin_detail();
        $email_address  = $admin_detail->email;
        //pr($email_address);die;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{User_Name}}', $userName, $str);
        $str = str_replace('{{Make_Name}}', $makeName, $str);
        $str = str_replace('{{Model_Name}}', $modelName, $str);
        $str = str_replace('{{Trim_Name}}', $trimName, $str);
        $str = str_replace('{{Engine_Number}}', $engineNumber, $str);
        $str = str_replace('{{Vin_Number}}', $vinNumber, $str);
        $str = str_replace('{{Part_Color}}', $partColor, $str);
        $str = str_replace('{{Oem}}', $oem, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($email_address,$str,$subject);             
    }

    /**
    * Send  trading car request mail using mailsending_model Model.
    * @category   apps
    * @package    Send car parts request mail
    * @subpackage Model
    * @author     Created By Reena Singh (1-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_trading_car_request_mail($userId) {
        $template = $this->templatechoose('trading-car-request');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $emailAddress = $user->email;
        $userName = $user->username;
        $admin_detail   = $this->get_admin_detail();
        $email_address  = $admin_detail->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{User_Name}}', $userName, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($email_address,$str,$subject);             
    }

    /**
    * Send car parts request mail using mailsending_model Model.
    * @category   apps
    * @package    Send car parts request mail
    * @subpackage Model
    * @author     Created By Reena Singh (1-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_finance_application_request_mail($userId) {
        $template = $this->templatechoose('finance-application-request');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $emailAddress = $user->email;
        $userName = $user->username;
        $admin_detail   = $this->get_admin_detail();
        $email_address  = $admin_detail->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{User_Name}}', $userName, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($email_address,$str,$subject);             
    }

    /**
    * Send contact us mail using mailsending_model Model.
    * @category   apps
    * @package    Send contact us request mail
    * @subpackage Model
    * @author     Created By Reena Singh (04-06-2019)
    * @copyright  2019 Bellezayofertas
    */ 
    function sendUserConactUsEmail($user_info) {
        $template = $this->templatechoose('contact-us');
        $firstName = $user_info['name'];
        $email = $user_info['email'];
        $phoneNumber = $user_info['phone_number'];
        $subject = $user_info['subject'];
        $message = $user_info['message'];
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';      $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Name}}', $firstName, $str);
        $str = str_replace('{{Email_Address}}', $email, $str);
        $str = str_replace('{{Phone_Number}}', $phoneNumber, $str);
        $str = str_replace('{{Message}}', $message, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($email,$str,$subject);
    }

    function sendAddSubscriberMail($email,$name) {
        
        $template = $this->templatechoose('admin-welcome-new-subscriber');
        $email = $email;
        $subject = $template->subject;
        $siteURL = '<a href="'.base_url().'">Visit Cannaxan</a>';
        $logoURL = '<img style="max-width: 50px;" src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';      
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{NAME}}', $name, $str);
        $str = str_replace('{{EMAIL}}', $email, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        //$this->sendEmail('testing@mailinator.com',$str,$subject, null, 'testing12@mailinator.com');
        
        $this->sendEmail('info@xsdemo.com',$str,$subject, null, 'vikashjangid.jpr@gmail.com');
    }
    /**
    * Send admin contact us mail using mailsending_model Model.
    * @category   apps
    * @package    Send contact us request mail
    * @subpackage Model
    * @author     Created By Reena Singh (04-06-2019)
    * @copyright  2019 Bellezayofertas
    */ 
    function sendAdminContactUsEmail($user_info) {
        $template = $this->templatechoose('admin-contact-us');
        $firstName = $user_info['name'];
        $email = $user_info['email'];
        $message = $user_info['message'];
        $phoneNumber = $user_info['phone_number'];
        $subject = $user_info['subject'];
        $admin_detail = $this->get_admin_detail();
        $emailAddress = $admin_detail->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';      
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{User_Name}}', $admin_detail->username, $str);
        $str = str_replace('{{Name}}', $firstName, $str);
        $str = str_replace('{{Email_Address}}', $email, $str);
        $str = str_replace('{{Phone_Number}}', $phoneNumber, $str);
        $str = str_replace('{{Message}}', $message, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);
    }

    function send_user_newsletter_email_form_website($subcriber_info) {
        $template = $this->templatechoose('newsletter-subscriber');
        $email = $subcriber_info['email'];
        $subject = $template->subject;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';      $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Email_Address}}', $email, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        //pr($str);die;
        $this->sendEmail($email,$str,$subject);
    }

    function send_admin_newsletter_email_form_website($subcriber_info) {
        $template = $this->templatechoose('admin-newsletter-subscriber');
        $email = $subcriber_info['email'];
        $subject = $template->subject;
        $admin_detail = $this->get_admin_detail();
        $emailAddress = $admin_detail->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';      
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Email_Address}}', $email, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);
    }

    /**
    * Send car request mail using mailsending_model Model.
    * @category   apps
    * @package    Send car parts request mail
    * @subpackage Model
    * @author     Created By Reena Singh (11-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_desired_car_request_mail($userId) {
        $template = $this->templatechoose('desired-car-request');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $emailAddress = $user->email;
        $userName = $user->username;
        $admin_detail   = $this->get_admin_detail();
        $email_address  = $admin_detail->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{User_Name}}', $userName, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($email_address,$str,$subject);             
    }

    /**
    * Send car request mail using mailsending_model Model.
    * @category   apps
    * @package    Send car parts request mail
    * @subpackage Model
    * @author     Created By Reena Singh (11-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_online_booking_mail_from_website($bookingId,$online_booking) {
        $template = $this->templatechoose('online-booking');
        $subject = $template->subject;
        $onlineBooking = $this->get_online_booking_by_id($bookingId);
        $firstName = $onlineBooking->first_name;
        $lastName = $onlineBooking->last_name;
        $cityName = $onlineBooking->city_name;
        $stateName = $onlineBooking->state;
        $phoneNumber = $onlineBooking->phone_number;
        $emailAddress = $onlineBooking->email;
        $year = $onlineBooking->year;
        $makeName = $onlineBooking->make_name;
        $modelName = $onlineBooking->model_name;
        $colorName = $onlineBooking->color;
        $mileage = $onlineBooking->mileage;
        $option = $onlineBooking->title;
        $admin_detail   = $this->get_admin_detail();
        $email_address  = $admin_detail->email;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{City}}', $cityName, $str);
        $str = str_replace('{{State}}', $stateName, $str);
        $str = str_replace('{{Phone_Number}}', $phoneNumber, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Year}}', $year, $str);
        $str = str_replace('{{Make}}', $makeName, $str);
        $str = str_replace('{{Model}}', $modelName, $str);
        $str = str_replace('{{Color}}', $colorName, $str);
        $str = str_replace('{{Mileage}}', $mileage, $str);
        $str = str_replace('{{Option}}', $option, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($email_address,$str,$subject);             
    }

    function reply_online_booking($reply, $email, $attachment = null) {
        $template = $this->templatechoose('reply-online-booking');
        $subject = $template->subject;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Reply}}', $reply, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($email, $str, $subject, $attachment);
    }

    function reply_request_car($reply, $email, $attachment = null) {
        $template = $this->templatechoose('reply-request-car');
        $subject = $template->subject;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{Reply}}', $reply, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($email, $str, $subject, $attachment);
    }

    /**
    * Send order accepted mail using mailsending_model Model.
    * @category   apps
    * @package    Send order accepted mail
    * @subpackage Model
    * @author     Created By Reena Singh (19-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_order_accepted_mail($userId) {
        $template = $this->templatechoose('order-accepted');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $emailAddress = $user->email;
        $userName = $user->username;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{User_Name}}', $userName, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }

    /**
    * Send order accepted mail using mailsending_model Model.
    * @category   apps
    * @package    Send order accepted mail
    * @subpackage Model
    * @author     Created By Reena Singh (19-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_order_declined_mail($userId) {
        $template = $this->templatechoose('order-declined');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $emailAddress = $user->email;
        $userName = $user->username;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{User_Name}}', $userName, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }

    /**
    * Send order delivered mail using mailsending_model Model.
    * @category   apps
    * @package    Send order delivered mail
    * @subpackage Model
    * @author     Created By Reena Singh (19-02-2019)
    * @copyright  2019 Boconrd
    */ 
    function send_order_delivered_mail($userId) {
        $template = $this->templatechoose('order-delivered');
        $subject = $template->subject;
        $user = $this->getUserRecordById($userId);
        $emailAddress = $user->email;
        $userName = $user->username;
        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';
        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{User_Name}}', $userName, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);              
    }

    function sendDynamicCronMail($data)
    {
        $template = $this->templatechoose($data->slug);
        $subject = $template->subject;
        $emailAddress = $data->email;
        $userName = $data->username;
        $phoneNumber = $data->phone_number;
        $firstName = $data->first_name;
        $lastName = $data->last_name;

        $siteURL = '<a href="'.site_url('admin').'">Visit Cannaxan</a>';
        $logoURL = '<img src="'.$this->config->item('uploads').'logo/molha_logo.png" alt="logo" class="logo-default"/>';

        $str = $template->content;
        $str = str_replace('{{Logo}}', $logoURL, $str);
        $str = str_replace('{{User_Name}}', $userName, $str);
        $str = str_replace('{{First_Name}}', $firstName, $str);
        $str = str_replace('{{Last_Name}}', $lastName, $str);
        $str = str_replace('{{Email_Address}}', $emailAddress, $str);
        $str = str_replace('{{Phone_Number}}', $phoneNumber, $str);
        $str = str_replace('{{Website_URL}}', $siteURL, $str);
        $this->sendEmail($emailAddress,$str,$subject);         
    }
}
