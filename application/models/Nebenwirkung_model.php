<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nebenwirkung_model extends CI_Model {

	function __construct() {

		parent::__construct();
		$this->nebenwirkung = 'tbl_nebenwirkung';
		$this->nebenwirkung_keine_kausal_bedingte = 'tbl_nebenwirkung_keine_kausal_bedingte';
		$this->nebenwirkung_table_data = 'tbl_nebenwirkung_data';
	}

	function getAllData() {

		$this->db->order_by('nebenwirkung_name', 'ASC');
		$query = $this->db->get($this->nebenwirkung);
		return $query->result();
	}

	function addkausalbedingteData($data) {
		$this->db->insert($this->nebenwirkung_keine_kausal_bedingte, $data);
		return $this->db->insert_id();
	}
	
	function updatekausalbedingteData($data, $id) {
		$this->db->where('id', $id);
		$this->db->update($this->nebenwirkung_keine_kausal_bedingte, $data);
		return true;
	}

	function addnebenwirkungtabledata($data) {
		$this->db->insert_batch($this->nebenwirkung_table_data, $data);
		return $this->db->insert_id();
	}

	function deletenebenwirkungtabledata($id) {
		$this->db->where('behandlunggstag_id', $id);
		$this->db->delete($this->nebenwirkung_table_data);
	}

	function getSideEffectsTableDataById($id) {

		$this->db->select($this->nebenwirkung_keine_kausal_bedingte.'.*');
		$this->db->where('patient_id', $id);
		$this->db->order_by('add_date', 'DESC');
		$query = $this->db->get($this->nebenwirkung_keine_kausal_bedingte);
		return $query->row();
	}

	function getSideEffectsTableVisitsDataById($patient_id) {

		// $this->db->select($this->nebenwirkung_keine_kausal_bedingte');
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('visite', 'ASC');
		$query = $this->db->get($this->nebenwirkung_keine_kausal_bedingte);
		return $query->result();
	}

	function getFirstVisit($id) {

		$this->db->select($this->nebenwirkung_keine_kausal_bedingte.'.*');
		$this->db->where('patient_id', $id);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get($this->nebenwirkung_keine_kausal_bedingte);
		return $query->row();
	}

	function getSideEffectsDataByBehandlunggstagId($id) {
		$this->db->select($this->nebenwirkung_table_data.'.*');
		// $this->db->select($this->nebenwirkung.'.nebenwirkung_name');
		$this->db->where($this->nebenwirkung_table_data.'.behandlunggstag_id', $id);
		//$this->db->order_by($this->nebenwirkung_table_data.'.id', 'DESC');
		// $this->db->join($this->nebenwirkung, $this->nebenwirkung.'.id='. $this->nebenwirkung_table_data.'.nebenwirkung_name');
		$query = $this->db->get($this->nebenwirkung_table_data);
		return $query->result(); 
	}

	function getAllVisitsSideEffectsDataByBehandlunggstagId($id) {
		// $condition = "mild_schweregrad ='Yes' OR moderat_schweregrad ='Yes' OR schwer_schweregrad ='Yes' OR lebensbedrohlich_schweregrad ='Yes'";

		$this->db->select($this->nebenwirkung_table_data.'.*');
		$this->db->where($this->nebenwirkung_table_data.'.behandlunggstag_id', $id);
		// $this->db->where($condition);
		$this->db->group_start();
			$this->db->or_where('mild_schweregrad', 'Yes');
			$this->db->or_where('moderat_schweregrad', 'Yes');
			$this->db->or_where('schwer_schweregrad', 'Yes');
			$this->db->or_where('lebensbedrohlich_schweregrad', 'Yes');
		$this->db->group_end();
		$query = $this->db->get($this->nebenwirkung_table_data);
		return $query->result(); 
	}

	function getRecordByDate($patient_id, $date) {
		$this->db->where('patient_id', $patient_id);
		$this->db->where('datum', $date);
		$query = $this->db->get($this->nebenwirkung_keine_kausal_bedingte);
		return $query->row(); 
	}
}