<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_model extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->patients = 'tbl_patients';
		$this->questions = 'tbl_patient_questions';
		$this->notifications = 'tbl_notifications';
		$this->tbl_app_notification_reminder = 'tbl_app_notification_reminder';
	}

	function getNotificationReminders($patient_id = null)
	{

		if ($patient_id) { 
			$this->db->where('patient_id', $patient_id);
		}

		$query = $this->db->get($this->tbl_app_notification_reminder);
		return $query->result();
	}
	function getPatientById($id){
		$this->db->where('id', $id);
		$query = $this->db->get($this->patients);
		return $query->row();
	}
	function check_patient_questions_exists($id) {

		$this->db->where('patient_id', $id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->questions);
		//pr($this->db->last_query()); die;
		return $query->row();
	}
	function getAllPatients() {
		$this->db->select('patient_id');
		$this->db->group_by('patient_id');
		$this->db->where('is_process_complete', 'Yes');
		$query = $this->db->get($this->questions);
		//pr($this->db->last_query()); die;
		return $query->result();
	}

	function getLastQuestionDetailByPatientId($id) {

		$this->db->where('is_process_complete', 'Yes');
		$this->db->where('patient_id', $id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->questions);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function get_patient_send_notification($patient_id, $notification_type, $notification_send_date) {

		$this->db->where('patient_id', $patient_id);
		$this->db->where('notification_type', $notification_type);
		$this->db->where('notification_send_date', $notification_send_date);		
		$query = $this->db->get($this->notifications);
		//pr($this->db->last_query()); die();
		return $query->row();
	}

	function add_notification($input) {
		$this->db->insert($this->notifications, $input);
		return $this->db->insert_id(); 
	}

	function get_app_verlaufskontrolle_patients()
	{
		$this->db->group_by('patient_id');	
		$query = $this->db->get('tbl_app_verlaufskontrolle');
		return $query->result();
	}

	function getDoctors($password_date) {
		$this->db->where('next_password_change', $password_date);
		$query = $this->db->get('tbl_members');
		return $query->result();
	}

	function getDoctorById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_members');
		return $query->row();
	}
}
