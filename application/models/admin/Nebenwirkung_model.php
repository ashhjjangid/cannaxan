<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Nebenwirkung_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->tbl = 'tbl_patients';
		$this->table = 'tbl_nebenwirkung';
		$this->patient_therapy_plan_umstellung = 'tbl_patient_therapy_plan_umstellung';
		$this->patient_typ_umstellung = 'tbl_patient_typ_umstellung';
		$this->thherpieplan_folgeverord = 'tbl_thherpieplan_folgeverord';
		$this->patient_thherpieplan_folgeverord = 'tbl_patient_thherpieplan_folgeverord';
		$this->patient_thherpieplan_folgeverord_option = 'tbl_patient_thherpieplan_folgeverord_option';
		$this->tbl_verlaufskontrolle_laborwerte = 'tbl_verlaufskontrolle_laborwerte';
		$this->tbl_verlaufskontrolletbl_laborwerte_laborparameter = 'tbl_verlaufskontrolletbl_laborwerte_laborparameter';
		$this->tbl_nebenwirkung_keine_kausal_bedingte = 'tbl_nebenwirkung_keine_kausal_bedingte';
		$this->tbl_begleitmedikation = 'tbl_begleitmedikation';
		$this->tbl_begleitmedikation_1 = 'tbl_begleitmedikation_1';
		$this->tbl_arztlicher_therapieplan = 'tbl_arztlicher_therapieplan';
		$this->tbl_patient_dosistyp_1 = 'tbl_patient_dosistyp_1';
		$this->tbl_patient_dosistyp_2 = 'tbl_patient_dosistyp_2';
		$this->tbl_icd_code_zuordnung = 'tbl_icd_code_zuordnung';
	}

	public function no_of_hunters()
	{
		$query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function add_nebenwirkung($input)
	 {   
		$this->db->select($this->table.'*');
		$query = $this->db->insert($this->table, $input);
		return $this->db->insert_id();
	 }

	public function getnebenwirkung()
	{
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function getnebenwirkungById($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function changeStatusById($id,$status)
	{
		$this->db->where('id', $id);
		$this->db->set('status', $status);
		$this->db->update($this->table);

		if (!$this->db->affected_rows()) {
			return false;
		} else {
			return true;
		}
	}
	

	public function setnebenwirkungById($id, $input)
	{  
		
		$this->db->select($this->table.'.*');
		$this->db->where('id', $id);
		$this->db->update($this->table, $input);

		if (!$this->db->affected_rows()) {
			return false;
		} else
		{
			return true;
		}
	}

	function getPatientbegleitmedikationData($id) {
    	$this->db->select($this->tbl.'.*');
    	$this->db->select($this->tbl_begleitmedikation.'.id as patient_begleitmedikation_id,doctor_id');
    	$this->db->where($this->tbl.'.id' , $id);
    	$this->db->join($this->tbl_begleitmedikation, $this->tbl_begleitmedikation.'.patient_id ='. $this->tbl. '.id');
    	$query = $this->db->get($this->tbl);
    	//pr($this->db->last_query()); die;
    	return $query->row();
    }

    public function getPaitentbegleitmedikationDataByPatientId($id)
	{
        $this->db->select($this->tbl_begleitmedikation_1.'.*');
        $this->db->where($this->tbl_begleitmedikation_1.'.begleitmedikation_id',$id); 
        $query = $this->db->get($this->tbl_begleitmedikation_1);
        return $query->result();
	}

	public function getPatientarztlichertherapieplanData($id) {
    	$this->db->where('patient_id', $id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->tbl_arztlicher_therapieplan);
		return $query->row();
    	//pr($this->db->last_query()); die;
    	return $query->row();
    }
    
    public function getpatient_dosistyp_1($id)
	{
        $this->db->select($this->tbl_patient_dosistyp_1.'.*');
        $this->db->where($this->tbl_patient_dosistyp_1.'.arztlicher_therapieplan_id',$id); 
        $query = $this->db->get($this->tbl_patient_dosistyp_1);
        return $query->result();
	}

	public function getpatient_dosistyp_2($id)
	{
        $this->db->select($this->tbl_patient_dosistyp_2.'.*');
        $this->db->where($this->tbl_patient_dosistyp_2.'.arztlicher_therapieplan_id',$id); 
        $query = $this->db->get($this->tbl_patient_dosistyp_2);
        return $query->result();
	}

	public function geticd_code_zuordnung()
	{
        $this->db->select($this->tbl_icd_code_zuordnung.'.*');
        //$this->db->where($this->tbl_patient_dosistyp_1.'.arztlicher_therapieplan_id',$id); 
        $query = $this->db->get($this->tbl_icd_code_zuordnung);
        return $query->result();
	}

	
}

?>