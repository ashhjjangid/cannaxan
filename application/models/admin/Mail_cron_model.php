<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Mail_cron_model class.
 * 
 * @extends CI_Controller
 */

class Mail_cron_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
    */

	function __construct() {
		parent::__construct();
		$this->table = 'tbl_schedule_emails';
        $this->table_users = 'tbl_users';
        $this->table_template = 'tbl_emails';
        $this->table_template_users = 'tbl_schedule_email_users';
	}

   /**
    * getAllPendingScheduleEmails function.
    * 
    * @access public
    * @return result set of records
    */

	function getAllPendingScheduleEmails() {
        $current_time = getDefaultToGMTDate(time());
		$this->db->select($this->table.'.*');
        $this->db->where($this->table.'.scheduled_time<=',$current_time);
        $this->db->where($this->table.'.process_status','Pending');
        $this->db->where($this->table.'.status','Active');
		$query = $this->db->get($this->table);
		$result = $query->result();
		return $result;
	}

   /**
    * setSendSucessMail function.
    * 
    * @access public
    * @return true or false
    */

    function setSendSucessMail($id) {
        $this->db->where('id',$id);
        $this->db->set('process_status','Complete');
        $result = $this->db->update($this->table);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

}
