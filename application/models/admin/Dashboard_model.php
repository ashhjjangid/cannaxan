<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_osistyp_1';
	}


	public function getuserById($user_id)
	{
		$this->db->where('id', $user_id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function change_status_by_id($id,$status)
	{
		$this->db->where('id', $id);
		$this->db->set('status', $status);
		$this->db->update($this->table);

		if (!$this->db->affected_rows()) {
			return false;
		} else {
			return true;
		}
	}
	

	public function setzuordnungById($id, $data)
	{  
		
		$this->db->select($this->table.'.*');
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);

		if (!$this->db->affected_rows()) {
			return false;
		} else
		{
			return true;
		}
	}

   /* public function delete_user_by_id($id)
    {
       $this->db->where('id',$id); 
       $this->db->delete($this->table); 
    }*/

	public function remove_file($id)
	{
		$files = $this->get_hunter_by_id($id);
		if ($files->profile) 
		{
			$path = 'assets/uploads/hunters_images/';
			unlink($path . $files->profile);	
		}
	}
}

?>