<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**Hunters_model
 */
class Icd_code_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->icd_code = 'tbl_icd_code_zuordnung';
	}

	function getAllIcdCodes() {
		$query = $this->db->get($this->icd_code);
		return $query->result();
	}

	function addNewIcdCode($data) {
		$this->db->insert($this->icd_code, $data);
		return $this->db->insert_id();
	}

	function getIcdCodeById($id) {

		$this->db->where('id', $id);
		$query = $this->db->get($this->icd_code);
		return $query->row();
	}

	function updateIcdCode($id, $data) {
		$this->db->where('id', $id);
		$this->db->update($this->icd_code, $data);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function changeStatusById($id, $status) {

		$this->db->where('id', $id);
		$this->db->set('status', $status);
		$this->db->update($this->icd_code);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function deleteIcdCodeById($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->icd_code);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
}

?>