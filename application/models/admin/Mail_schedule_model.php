<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Mail_templates_model class.
 * 
 * @extends CI_Controller
 */

class Mail_schedule_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
    */

	function __construct() {
		parent::__construct();
		$this->table = 'tbl_schedule_emails';
        $this->table_users = 'tbl_users';
        $this->table_template = 'tbl_emails';
        $this->table_template_users = 'tbl_schedule_email_users';
	}

   /**
    * getAllScheduleEmails function.
    * 
    * @access public
    * @param mixed $templete_id
    * @return result set of scheduled templates
    */

	function getAllScheduleEmails($templete_id) {
		$this->db->select($this->table.'.*');
        $this->db->select($this->table_template.'.name');
        $this->db->where($this->table.'.template_id',$templete_id);
        $this->db->join($this->table_template,$this->table_template.'.id='.$this->table.'.template_id');
		$query = $this->db->get($this->table);
		$result = $query->result();
		return $result;
	}

   /**
    * changeStatusById function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

	function changeStatusById($id,$status) {
		$this->db->where('id',$id);
		$this->db->set('status',$status);
		$this->db->update($this->table);

		if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
	}


   /**
    * deleteSchdeuleById function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    function deleteSchdeuleById($id) {
        $this->db->where('id',$id);
        $this->db->delete($this->table);
        return true;
    }

   /**
    * getSingleScheduleById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of templetes
    */

	function getSingleScheduleById($id) {
        $this->db->select($this->table.'.*');
        $this->db->select($this->table_template.'.name');
		$this->db->where($this->table.'.id',$id);
        $this->db->join($this->table_template,$this->table_template.'.id='.$this->table.'.template_id');
		$query = $this->db->get($this->table);
		$result = $query->row();
		return $result;
	}

   /**
    * getAllScheduleUsers function.
    * 
    * @access public
    * @param mixed $schedule_id
    * @return result of scheduled users
    */

    function getAllScheduleUsers($schedule_id) {
        $this->db->select($this->table_users.'.*');
        $this->db->select($this->table_template_users.'.user_id');
        $this->db->where($this->table_template_users.'.schedule_id',$schedule_id);
        $this->db->join($this->table_users,$this->table_users.'.id='.$this->table_template_users.'.user_id');
        $query = $this->db->get($this->table_template_users);
        $result = $query->result();
        return $result;        
    }	

   /**
    * setEmailTemplate function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $time_slot
    * @return true or false
    */

	function setTimeSchedule($id, $time_slot) {
		$this->db->where('id',$id);
        $this->db->set('scheduled_time',$time_slot);

		$result = $this->db->update($this->table);

		if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
	}

   /**
    * addEmailSchedule function.
    * 
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    function addEmailSchedule($data) {

        $data['add_date'] = getDefaultToGMTDate(time());
        $data['status'] = 'Active';
        $this->db->insert($this->table,$data);
        return $this->db->insert_id();        
    }

   /**
    * addEmailScheduleUsers function.
    * 
    * @access public
    * @param mixed $data as array
    * @return insert id
    */

    function addEmailScheduleUsers($data) {
        $this->db->insert($this->table_template_users,$data);
        return $this->db->insert_id();         
    }

   /**
    * deleteEmailScheduleUsers function.
    * 
    * @access public
    * @param mixed $id
    * @return null
    */

    function deleteEmailScheduleUsers($schedule_id) {

        $this->db->where('schedule_id',$schedule_id);
        $this->db->delete($this->table_template_users);
    }
		
}
