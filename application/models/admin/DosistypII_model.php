<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DosistypII_model extends CI_Model {

	function __construct() {

		parent::__construct();
		$this->dosistypII = 'tbl_dosistyp_2';
	}

	function getAllDosisTypII() {

		$query = $this->db->get($this->dosistypII);
		return $query->result();
	}

	function addNewDosistyp2($data) {

		$this->db->insert($this->dosistypII, $data);
		return $this->db->insert_id();
	}

	function editDosistyp2ById($id, $data) {
		$this->db->where('id', $id);
		$this->db->update($this->dosistypII, $data);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function getDosisTypIIById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->dosistypII);
		return $query->row();
	}

	function deleteDosisTypIIById($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->dosistypII);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function changeStatusById($id, $status) {
		$this->db->where('id', $id);
		$this->db->set('status', $status);
		$this->db->update($this->dosistypII);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
}
