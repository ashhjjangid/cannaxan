<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Mail_templates_model class.
 * 
 * @extends CI_Controller
 */

class Mail_templates_model extends CI_Model {

   /**
    * __construct function.
    * 
    * @access public
    * @return void
    */

	function __construct() {
		parent::__construct();
		$this->table = 'tbl_emails';
	}

   /**
    * getAllTemplates function.
    * 
    * @access public
    * @return result set of templates
    */

	function getAllTemplates() {
		$this->db->select('*');
		$query = $this->db->get($this->table);
		$result = $query->result();
		return $result;
	}

   /**
    * changeStatusById function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $status
    * @return true or false
    */

	function changeStatusById($id,$status) {
		$this->db->where('id',$id);
		$this->db->set('status',$status);
		$this->db->update($this->table);

		if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
	}

   /**
    * getSingleTemplateById function.
    * 
    * @access public
    * @param mixed $id
    * @return row of templetes
    */

	function getSingleTemplateById($id) {
		$this->db->where('id',$id);
		$query = $this->db->get($this->table);
		$result = $query->row();
		return $result;
	}	

   /**
    * setEmailTemplate function.
    * 
    * @access public
    * @param mixed $id
    * @param mixed $data as array
    * @return true or false
    */

	function setEmailTemplate($id, $data) {
		$this->db->where('id',$id);
		$result = $this->db->update($this->table,$data);

		if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
	}
		
}
