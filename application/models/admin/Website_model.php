<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Website_model class.
 * 
 * @extends CI_Controller
 */

class Website_model extends CI_Model
{
    private $table = 'tbl_website_setting';

   /**
    * __construct function.
    * 
    * @access public
    * @return void
   */

    public function __construct()
    {
        $this->load->database();
    }

    /**
    * getValueBySlug function.
    * 
    * @access public
    * @param mixed $key
    * @param mixed $value as false
    * @return row or false
    */

    public function getValueBySlug($key = '', $value = false)
    {
        if ($key != '') {
            $query = $this->db->get_where($this->table, array('slug' => $key));
            $option = $query->row();

            if (!empty($option)) {

                if ($value) {
                    return $option->value;
                } else {
                    return $option;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
    * updateOptionsSetting function.
    * 
    * @access public
    * @param mixed $options
    * @return true or false
    */

    public function updateOptionsSetting($options = array())
    {
        if (!empty($options)) {

            foreach ($options as $key => $value) {

                if ($this->getValueBySlug($key)) {
                    $this->updateSingleOption($key, $value);
                } else {
                    $this->addOptionBySlug($key, $value);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
    * updateSingleOption function.
    * 
    * @access public
    * @param mixed $key
    * @param mixed $value
    * @return true or false
    */

    public function updateSingleOption($key = '', $value = '')
    {
        if (is_array($value)) {

            foreach ($value as $keys => $values) {

                foreach ($values as $k => $v) {
                    $this->db->where($this->table.'.language_abbr', $keys);
                    $this->db->where($this->table.'.slug', $k);
                    $this->db->set($this->table.'.value', $v);
                    $this->db->update($this->table);
                }
            }
        } else {
            $updated = $this->db->where(array('slug' => $key))->set(array('value' => $value))->update($this->table);
        }

        if ($key == '') {
            return false;
        }

        if ($updated) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * addOptionBySlug function.
    * 
    * @access public
    * @param mixed $key
    * @param mixed $value
    * @return true or false
    */

    public function addOptionBySlug($key = '', $value = '')
    {
        if ($key != '') {
            $data = array();
            $data['slug'] = $key;
            $data['value'] = $value;
            $result = $this->db->insert($this->table, $data);
            
            if ($result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
