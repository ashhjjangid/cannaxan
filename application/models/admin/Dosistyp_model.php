<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**Hunters_model
 */
class Dosistyp_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'tbl_dosistyp_1';
	}

	public function no_of_hunters()
	{
		$query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function add_new_dosistyp($input)
	 {   
		$this->db->select($this->table.'*');
		$query = $this->db->insert($this->table, $input);
		return $this->db->insert_id();
	 }

	 public function getdosistyp()
	{
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function getdosistypById($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	public function changeStatusById($id,$status)
	{
		$this->db->where('id', $id);
		$this->db->set('status', $status);
		$this->db->update($this->table);

		if (!$this->db->affected_rows()) {
			return false;
		} else {
			return true;
		}
	}
	

	public function setdosistypById($id, $input)
	{  
		
		$this->db->select($this->table.'.*');
		$this->db->where('id', $id);
		$this->db->update($this->table, $input);

		if (!$this->db->affected_rows()) {
			return false;
		} else
		{
			return true;
		}
	}

   /* public function delete_user_by_id($id)
    {
       $this->db->where('id',$id); 
       $this->db->delete($this->table); 
    }*/

	public function remove_file($id)
	{
		$files = $this->get_hunter_by_id($id);
		if ($files->profile) 
		{
			$path = 'assets/uploads/hunters_images/';
			unlink($path . $files->profile);	
		}
	}
}

?>