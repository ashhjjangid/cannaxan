<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Arztfragebogen_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->web_question_start = 'tbl_web_patient_questions';
		$this->web_question_1 = 'tbl_web_question_1';
		$this->web_question_2 = 'tbl_web_question_2';
		$this->web_question_3 = 'tbl_web_question_3';
		$this->web_question_4 = 'tbl_web_question_4';
		$this->web_question_5 = 'tbl_web_question_5';
		$this->web_question_6 = 'tbl_web_question_6';
		$this->web_question_7 = 'tbl_web_question_7';
		$this->tbl_who_1_options_fields = 'tbl_who_1_options_fields';
		$this->tbl_who_2_options_fields = 'tbl_who_2_options_fields';
		$this->tbl_who_3_options_fields = 'tbl_who_3_options_fields';
		$this->tbl_spasmolytika_options_fields = 'tbl_spasmolytika_options_fields';

	}

	function addingWebQuestionOfPatientStart($data) {
		$this->db->insert($this->web_question_start, $data);
		return $this->db->insert_id();
	}

	function updateWebQuestionOfPatientStart($id, $data) {
		$this->db->where('id', $id);
		$this->db->update($this->web_question_start, $data);
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	function addWebQuestion_1($data) {
		//pr($data); die;
		$this->db->insert($this->web_question_1, $data);
		return $this->db->insert_id();
	}

	function addWebQuestion_2($question_2) {
		$this->db->insert($this->web_question_2, $question_2);
		return $this->db->insert_id();
	}

	function deleteWebQuestion_2($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$this->db->delete($this->web_question_2);
	}

	function addWebQuestion_3($question_3) {
		$this->db->insert($this->web_question_3, $question_3);
		return $this->db->insert_id();
	}

	function deleteWebQuestion_3($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$this->db->delete($this->web_question_3);
	}

	function addWebQuestion_4($question_4) {
		$this->db->insert($this->web_question_4, $question_4);
		return $this->db->insert_id();
	}

	function deleteWebQuestion_4($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$this->db->delete($this->web_question_4);
	}

	function addWebQuestion_5($question_5) {
		$this->db->insert($this->web_question_5, $question_5);
		return $this->db->insert_id();
	}

	function deleteWebQuestion_5($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$this->db->delete($this->web_question_5);
	}

	function addWho1fields($data) {
		$this->db->insert($this->tbl_who_1_options_fields, $data);
		return $this->db->insert_id();
	}

	function addWho2fields($data) {
		$this->db->insert($this->tbl_who_2_options_fields, $data);
		return $this->db->insert_id();
	}

	function addWho3fields($data) {
		//pr($data); die;
		$this->db->insert($this->tbl_who_3_options_fields, $data);
		return $this->db->insert_id();
	}

	function addspasmolytikafields($data) {
		$this->db->insert($this->tbl_spasmolytika_options_fields, $data);
		return $this->db->insert_id();
	}

	function addWebQuestion_6($question_6) {
		$this->db->insert($this->web_question_6, $question_6);
		return $this->db->insert_id();
	}


	function deleteWebQuestion_6($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$this->db->delete($this->web_question_6);
	}

	function addWebQuestion_7($question_7) {
		$this->db->insert($this->web_question_7, $question_7);
		return $this->db->insert_id();
	}


	function deleteWebQuestion_7($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$this->db->delete($this->web_question_7);
	}

	function getWebQuestionDataStart($patient_id, $doctor_id, $question_id = null) {
		$this->db->where('patient_id', $patient_id);
		$this->db->where('doctor_id', $doctor_id);

		if ($question_id) {
			$this->db->where('qid', $question_id);
		}
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->web_question_start);
		return $query->row();
	}

	function deleteWebQuestion_1($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$this->db->delete($this->web_question_1);
	}

	function get_last_process_complete_answer($patient_id, $doctor_id)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->where('doctor_id', $doctor_id);
		$this->db->where('is_process_complete', 'Yes');
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->web_question_start);
		return $query->row();
	}

	function get_first_step_WebQuestion_1_Data($web_question_id) {
		
		$this->db->where('web_patient_question_id', $web_question_id);
		$this->db->where('question_no', 'First');
		$query = $this->db->get($this->web_question_1);
		return $query->row();
	}

	function get_first_step_WebQuestion_2_Data($web_question_id) {
		
		$this->db->where('web_patient_question_id', $web_question_id);
		$this->db->where('question_no', 'Second');
		$query = $this->db->get($this->web_question_1);
		return $query->result();
	}

	function checkQuestionStartDataExistsById($qid) {
		$this->db->where('qid', $qid);
		$query = $this->db->get($this->web_question_start);
		return $query->row();
	}

	function get_second_step_question_data($web_question_id) {
		$this->db->where('web_patient_question_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->web_question_2);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function get_third_step_question_data($web_question_id) {
		$this->db->where('web_patient_question_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->web_question_3);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function get_forth_step_question_data($web_question_id) {
		$this->db->where('web_patient_question_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->web_question_4);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function get_forth_step_question_2_data($web_question_id) {
		$this->db->where('web_patient_question_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->web_question_5);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function get_who_1_data($web_question_id) {
		$this->db->where('who_stufe_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->tbl_who_1_options_fields);
		//pr($this->db->last_query()); die;
		return $query->result();
	}

	function getwho1Nichtrecords($id, $option_name) {
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get($this->tbl_who_1_options_fields);
		return $query->row();
	}

	function getwho1Steroiderecords($id, $option_name) {
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get($this->tbl_who_1_options_fields);
		return $query->row();
	}

	function getwho1Neuroleptikarecords($id, $option_name) {
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get($this->tbl_who_1_options_fields);
		return $query->row();
	}

	function getwho1Antidepressivarecords($id, $option_name) {
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get($this->tbl_who_1_options_fields);
		return $query->row();
	}

	function getwho1Sedativarecords($id, $option_name) {
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get($this->tbl_who_1_options_fields);
		return $query->row();
	}

	function getwho1Antikonvulsivarecords($id, $option_name) {
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get($this->tbl_who_1_options_fields);
		return $query->row();
	}

	function getwho1Antiemetikarecords($id, $option_name) {
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get($this->tbl_who_1_options_fields);
		return $query->row();
	}

	function get_who_2_data($web_question_id) {
		$this->db->where('who_stufe_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->tbl_who_2_options_fields);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function get_who_3_data($web_question_id) {
		$this->db->where('who_stufe_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->tbl_who_3_options_fields);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function get_spasmolytika_data($web_question_id) {
		$this->db->where('who_stufe_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->tbl_spasmolytika_options_fields);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function clearwhodata($web_question_id) {
		$this->db->where('web_patient_question_id', $web_question_id);
		$this->db->delete($this->web_question_5);
	}

	function deletewho1data($who_stufe_id) {
		$this->db->where('who_stufe_id', $who_stufe_id);
		$this->db->delete($this->tbl_who_1_options_fields);
	}

	function deletewho2data($who_stufe_id) {
		$this->db->where('who_stufe_id', $who_stufe_id);
		$this->db->delete($this->tbl_who_2_options_fields);
	}

	function deletewho3data($who_stufe_id) {
		$this->db->where('who_stufe_id', $who_stufe_id);
		$this->db->delete($this->tbl_who_3_options_fields);
	}

	function deletespasmolytikadata($who_stufe_id) {
		$this->db->where('who_stufe_id', $who_stufe_id);
		$this->db->delete($this->tbl_spasmolytika_options_fields);
	}

	function get_fifth_step_question_data($web_question_id) {
		$this->db->where('web_patient_question_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->web_question_6);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function get_sixth_step_question_data($web_question_id) {
		$this->db->where('web_patient_question_id', $web_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->web_question_7);
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function getLoggedInUserById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_members');
		return $query->row();
	}

	function getQuestionByQid($qid, $patient_id)
	{
		$this->db->where('qid', $qid);
		$this->db->where('patient_id', $patient_id);
		$query = $this->db->get('tbl_web_patient_questions');
		return $query->row();
	}

	function getFirstQuestionByWebPatientQuestionId($web_patient_question_id, $is_first = false)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		
		if ($is_first) {
			$this->db->order_by('answer', 'ASC');
			$this->db->where('question_no', 'First');
			$query = $this->db->get('tbl_web_question_1');
			return $query->row();
		} else {
			$this->db->order_by('answer', 'ASC');
			$this->db->where('question_no', 'Second');
			$query = $this->db->get('tbl_web_question_1');
			return $query->result();
		}
	}

	function getSecondQuestionByWebPatientQuestionId($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$query = $this->db->get('tbl_web_question_2');
		return $query->row();
	}

	function getThirdQuestionByWebPatientQuestionId($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$query = $this->db->get('tbl_web_question_3');
		return $query->row();
	}

	function getForthQuestionByWebPatientQuestionId($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$query = $this->db->get('tbl_web_question_4');
		return $query->row();
	}

	function getFifthQuestionByWebPatientQuestionId($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('tbl_web_question_5');
		return $query->row();
	}

	function getFifthQuestionWHO1NichtById($id, $option_name)
	{
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get('tbl_who_1_options_fields');
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function getFifthQuestionWHO1SteroideById($id, $option_name)
	{
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get('tbl_who_1_options_fields');
		return $query->row();
	}

	function getFifthQuestionWHO1NeuroleptikaById($id, $option_name)
	{
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get('tbl_who_1_options_fields');
		return $query->row();
	}

	function getFifthQuestionWHO1AntidepressivaById($id, $option_name)
	{
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get('tbl_who_1_options_fields');
		return $query->row();
	}

	function getFifthQuestionWHO1SedativaById($id, $option_name)
	{
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get('tbl_who_1_options_fields');
		return $query->row();
	}

	function getFifthQuestionWHO1AntikonvulsivaById($id, $option_name)
	{
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get('tbl_who_1_options_fields');
		return $query->row();
	}

	function getFifthQuestionWHO1AntiemetikaById($id, $option_name)
	{
		$this->db->where('who_stufe_id', $id);
		$this->db->where('option_name', $option_name);
		$query = $this->db->get('tbl_who_1_options_fields');
		return $query->row();
	}

	function getFifthQuestionWHO2ById($id)
	{
		$this->db->where('who_stufe_id', $id);
		$query = $this->db->get('tbl_who_2_options_fields');
		//pr($this->db->last_query()); die;
		return $query->row();
	}

	function getFifthQuestionWHO3ById($id)
	{
		$this->db->where('who_stufe_id', $id);
		$query = $this->db->get('tbl_who_3_options_fields');
		return $query->row();
	}

	function getFifthQuestionSpasmolytikaById($id)
	{
		$this->db->where('who_stufe_id', $id);
		$query = $this->db->get('tbl_spasmolytika_options_fields');
		return $query->row();
	}

	function getSixthQuestionByWebPatientQuestionId($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('tbl_web_question_6');
		return $query->row();
	}

	function getSevenQuestionByWebPatientQuestionId($web_patient_question_id)
	{
		$this->db->where('web_patient_question_id', $web_patient_question_id);
		$query = $this->db->get('tbl_web_question_7');
		return $query->row();
	}

	function getbegleitmedikationDataByPatientId($patient_id) {
		$date = date('Y-m-d');
		$this->db->where('startdatum <=', $date);
		$this->db->group_start();
			$this->db->where('stopdatum >=', $date);
			$this->db->or_where('stopdatum', '');
			$this->db->or_where('stopdatum', null);
		$this->db->group_end();
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('tbl_begleitmedikation_1');
		return $query->result();
	}

	function getHauptindikationById($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_icd_code_zuordnung');
		return $query->row();
	}

	function getBookGroupIdByICDCode($icd_code_zuordnung_id)
	{
		$this->db->where('icd_code_zuordnung_id', $icd_code_zuordnung_id);
		$query = $this->db->get('tbl_icd_code_book_groups');
		return $query->row();
	}

	function getBooksByGroupId($book_group_id)
	{
		$this->db->where('book_group_id', $book_group_id);
		$this->db->order_by('year', 'DESC');
		$query = $this->db->get('tbl_icd_code_books');
		return $query->result();
	}
}